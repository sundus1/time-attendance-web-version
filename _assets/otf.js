﻿function addAbsence(obj, Comment, postback, isAdjustment) {
    var $obj = obj;
    var absLogID = $obj.attr('data-id');
    var userID = $obj.attr('data-userid');
    var date = $obj.attr('data-date');
    var active = $obj.attr('data-active');   

    var jsondata = "{'AbsLogID': '" + absLogID + "', 'UserID':'" + userID + "' , 'Date': '" + date + "', 'Active': '" + active + "', 'Comment': '" + Comment + "', 'isAdjustment': '" + isAdjustment + "'}";
    ActivateAlertDiv('', 'AlertDiv');
    $.ajax({
        type: "POST",
        url: "WebService.asmx/AddAbsence",
        data: jsondata,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (parseInt(absLogID) > 0) {
                $obj.parent().find('.lblText').text("No Entry");                
                $obj.parent().closest('tr').removeClass('absentRow');
                $obj.parent().closest('tr').addClass('pinkRow');
                $obj.parent().closest('tr').find(".information").html("");
                jq191($obj.parent().closest('tr').find(".information")).tooltipster().remove();
            }
            else {
                $obj.parent().find('.lblText').text("Absent");
                $obj.parent().closest('tr').removeClass('pinkRow');
                $obj.parent().closest('tr').addClass('absentRow');
                $obj.parent().closest('tr').append("<td style='fon-weight:bold;' class='information'>Absent</td>");
                jq191($obj.parent().closest('tr').find(".information")).tooltipster({
                    interactive: true,
                    content: Comment
                });
            }
            ActivateAlertDiv('none', 'AlertDiv');
            $('.txtAbsComment').val('');
            $obj.hide(200);
            if (postback) {
                location.reload(true);
            }
        },
        error: function (e) {
            ActivateAlertDiv('none', 'AlertDiv');
            alert("Sorry, an error occured while processing.");
        }
    });
}


function addAbsenceToday(obj, Comment, postback) {
    var $obj = obj;
    var absLogID = $obj.attr('data-id');
    var userID = $obj.attr('data-userid');
    var date = $obj.attr('data-date');
    var active = $obj.attr('data-active');

    var jsondata = "{'AbsLogID': '" + absLogID + "', 'UserID':'" + userID + "' , 'Date': '" + date + "', 'Active': '" + active + "', 'Comment': '" + Comment + "'}";
    $.ajax({
        type: "POST",
        url: "WebService.asmx/AddAbsenceToday",
        data: jsondata,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (parseInt(absLogID) > 0) {
                $obj.parent().find('.lblText').text("No Entry");
                //$obj.parent().removeClass('pinkRow');
                //$obj.parent().closest('tr').removeClass('pinkRow');
            }
            else {
                $obj.parent().find('.lblText').text("Absent");
            }
            $('.txtAbsComment').val('');
            $obj.hide(200);
            if (postback) {
                location.reload(true);
            }
        },
        error: function (e) {
            alert("Sorry, an error occured while processing.");
        }
    });
}


function addNewAbsence(absLogID, userID, date, active, Comment) {   
    var jsondata = "{'AbsLogID': '" + absLogID + "', 'UserID':'" + userID + "' , 'Date': '" + date + "', 'Active': '" + active + "', 'Comment': '" + Comment + "'}";
    $.ajax({ 
        type: "POST",
        url: "WebService.asmx/AddAbsence",
        data: jsondata,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {            
            location.reload(true);
        },
        error: function (e) {
            alert("Sorry, an error occured while processing.");
        }
    });
}

function AddUpdateExDaysD(obj, Comment, postback) {
    var $obj = obj;
    var ExDayID = $obj.attr('data-exday-id');
    var ExDayTypeID = $obj.attr('data-exday-typeid');
    var userID = $obj.attr('data-exday-userid');
    var date = $obj.attr('data-exday-date');
    var active = $obj.attr('data-exday-active');

    var jsondata = "{'ExDayID': '" + ExDayID + "', 'ExDayTypeID' :'" + ExDayTypeID + "', 'UserID':'" + userID + "' , 'Date': '" + date + "', 'Active': '" + active + "', 'Comment': '" + Comment + "'}";
    $.ajax({
        type: "POST",
        url: "WebService.asmx/AddUpdateExDays",
        data: jsondata,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            //alert('success');
            if (parseInt(ExDayTypeID) > 0) {
                //$obj.parent().find('.lblText').text("No Entry");
                //$obj.parent().removeClass('pinkRow');
                //$obj.parent().closest('tr').removeClass('pinkRow');
            }
            else {
                //$obj.parent().find('.lblText').text("Absent");
            }
            //$('.txtAbsComment').val('');
            //$obj.hide(200);
            if (postback) {
                location.reload(true);
            }
        },
        error: function (e) {
            alert("Sorry, an error occured while processing.");
        }
    });
}

function AddUpdateExDays(ExDayID, ExDayTypeID, userID, date, active, Comment, postback) {    
    var jsondata = "{'ExDayID': '" + ExDayID + "', 'ExDayTypeID' :'" + ExDayTypeID + "', 'UserID':'" + userID + "' , 'Date': '" + date + "', 'Active': '" + active + "', 'Comment': '" + Comment + "'}";
    $.ajax({
        type: "POST",
        url: "WebService.asmx/AddUpdateExDays",
        data: jsondata,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            //if (active == 'True') {
            //    alert('Marked as partial working');
            //}
            //else {
            //    alert('Unmarked partial working');
            //}            
            if (postback) {
                location.reload(true);
            }
        },
        error: function (e) {
            alert("Sorry, an error occured while processing.");
        }
    });
}

function AddUpdateAutoPresent(AutoPresentID, UserID, ClockInTime, ClockOutTime, Active) {
    var jsondata = "{'AutoPresentID': '" + AutoPresentID + "', 'UserID':'" + UserID + "' , 'ClockInTime': '" + ClockInTime + "', 'ClockOutTime': '" + ClockOutTime + "', 'Active': '" + Active + "'}";
    $.ajax({
        type: "POST",
        url: "WebService.asmx/AddUpdateAutoPresent",
        data: jsondata,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            alert(msg.d);
            //location.reload(true);
        },
        error: function (e) {
            alert("Sorry, an error occured while processing.");
        }
    });
}

function SetUserStatus(userid, active, reload) {
    var jsondata = "{'userid': '" + userid + "', 'active':'" + active + "'}";
    $.ajax({
        type: "POST",
        url: "WebService.asmx/SetUserStatus",
        data: jsondata,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            alert("Status has been changed successfully");
            //alert(msg.d);
            if (reload) {
                location.reload(true);
            }
            //
        },
        error: function (e) {
            alert("Sorry, an error occured while processing.");
        }
    });
}


function Clockin($obj) {
    var userID = $obj.attr('data-userid');
    var datainaddress = $obj.attr('data-inaddress');

    var jsondata = "{'userid': '" + userID + "', 'inaddress':'" + datainaddress + "'}";
    $.ajax({
        type: "POST",
        url: "WebService.asmx/SignIn",
        data: jsondata,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            //$obj.hide(200);
            //$('a[data-tr-id="' + datatr + '"]').addClass("pinkRow");
            location.reload(true);
        },
        error: function (e) {
            alert("Sorry, an error occured while processing.");
        }
    });
}

function Clockout($obj) {
    var userID = $obj.attr('data-userid');
    var datainaddress = $obj.attr('data-inaddress');

    var jsondata = "{'userid': '" + userID + "', 'inaddress':'" + datainaddress + "'}";
    $.ajax({
        type: "POST",
        url: "WebService.asmx/SignOut",
        data: jsondata,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            //$obj.hide(200);
            //$('a[data-tr-id="' + datatr + '"]').addClass("pinkRow");
            location.reload(true);
        },
        error: function (e) {
            alert("Sorry, an error occured while processing.");
        }
    });
}

function StartAutoPresent() {    
    $.ajax({
        type: "POST",
        url: "WebService.asmx/PerformAutoPresentByWorkingHours",        
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            alert('Autopresent on');
        },
        error: function (e) {
            alert("Sorry, an error occured while processing.");
        }
    });
}

function resizeiframepage200() {
    var iframe = window.parent.document.getElementById("MainContent_ifapp");
    var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
    if (innerDoc.body.offsetHeight) {
        iframe.height = innerDoc.body.offsetHeight + 300 + "px";
    } else if (iframe.Document && iframe.Document.body.scrollHeight) {
        iframe.style.height = iframe.Document.body.scrollHeight + "px";
    }
}
