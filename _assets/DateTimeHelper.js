﻿
// Format Date object to AM PM
function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}



// Gets absolute then floor 
Date.daysBetween = function (date1, date2) {
    //Get 1 day in milliseconds
    var one_day = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms / one_day);
}

Date.mintuesBetween = function (date1, date2) {
    //Get 1 day in milliseconds
    var one_minute = 60000;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms / one_minute);
}

function getAbsoluteAndFloor(value) {
    var rvalue = parseFloat(value);
    value = Math.floor(Math.abs(rvalue));
    return value;
}

function getWorkedHours(signintime, OutputLabelSelector) {
    var $signintime = new Date(signintime);
    var currtime = new Date();
    var daysdiff = Date.daysBetween(currtime, $signintime);
    var mindiff = Date.mintuesBetween(currtime, $signintime);
    var hoursDiff = mindiff / 60;
    var daysDiff = Math.ceil(hoursDiff / 24);
    var h1 = getAbsoluteAndFloor(hoursDiff);
    var h2 = (getAbsoluteAndFloor(daysDiff) * 24);
    var resulthoursDiff = h1 - h2;
    var m1 = getAbsoluteAndFloor(mindiff);
    var m2 = (getAbsoluteAndFloor(hoursDiff) * 60);
    var resultmindiff = m1 - m2;
    if (daysDiff <= 0 && resulthoursDiff > 0) {
        $(OutputLabelSelector).text(getAbsoluteAndFloor(resulthoursDiff) + " hrs " + getAbsoluteAndFloor(resultmindiff) + " mins");
    }
    else if (daysDiff <= 0 && resulthoursDiff <= 0) {
        $(OutputLabelSelector).text(getAbsoluteAndFloor(resultmindiff) + " mins");
    }
    else {
        $(OutputLabelSelector).text(getAbsoluteAndFloor(daysDiff) + " days " + getAbsoluteAndFloor(resulthoursDiff) + " hrs " + getAbsoluteAndFloor(resultmindiff) + " mins");
    }
}

function getTimeDifference(signintime, OutputLabelSelector) {
    var date = new Date(signintime);
    var currdate = new Date();
    var datediff = (currdate - date) / 1000;
    var days = Math.floor(datediff / 86400);
    var hours = Math.floor(datediff / 3600) % 24;
    var minutes = Math.floor(datediff / 60) % 60;
    var seconds = Math.floor(datediff) % 60;
    var outputstring = "";
    if (days > 0) {
        outputstring += days + " days, ";
    }
    if (hours > 0) {
        outputstring += hours + " hrs, ";
    }
    outputstring += minutes + " min";
    $(OutputLabelSelector).text(outputstring);
}

function getDateTimeDifference(signintime, OutputLabelSelector, signout) {
    var datetime1 = new Date(signintime);
    var datetime2 = new Date(signout);
    var datediff = datetime2 - datetime1
    datediff = datediff / 1000
    var days = Math.floor(datediff / 86400);
    var hours = Math.floor(datediff / 3600) % 24;
    var minutes = Math.floor(datediff / 60) % 60;
    var seconds = Math.floor(datediff) % 60;
    var outputstring = "";
    if (days > 0) {
        outputstring += days + " days ";
    }
    if (hours > 0) {
        outputstring += hours + " hrs ";
    }
    if (minutes > 0) {
        outputstring += minutes + " mins ";
    }
    else {
        outputstring += "0 mins ";
    }
    $(OutputLabelSelector).text(outputstring);
}

function getTimeDifference(starttime, endtime, OutputLabelSelector) {
    //2/19/2014 9:09:00 AM
    var daydate = new Date();
    var starthr = starttime.split(' ')[0].split(':')[0];
    var startmin = starttime.split(' ')[0].split(':')[1];
    var ampm = starttime.split(' ')[1];
    var date = new Date(daydate.getMonth() + "/" + daydate.getDay() + "/" + daydate.getYear() + " " + starthr + ":" + startmin + ":00 " + ampm);
    starthr = endtime.split(' ')[0].split(':')[0];
    startmin = endtime.split(' ')[0].split(':')[1];
    ampm = endtime.split(' ')[1];
    daydate = new Date();
    var currdate = new Date(daydate.getMonth() + "/" + daydate.getDay() + "/" + daydate.getYear() + " " + starthr + ":" + startmin + ":00 " + ampm);
    var datediff = (currdate - date) / 1000;
    var days = Math.floor(datediff / 86400);
    var hours = Math.floor(datediff / 3600) % 24;
    var minutes = Math.floor(datediff / 60) % 60;
    var seconds = Math.floor(datediff) % 60;
    var outputstring = "";
    if (days > 0) {
        outputstring += days + " days, ";
    }
    if (hours > 0) {
        outputstring += hours + " hrs, ";
    }
    outputstring += minutes + " min";
    $(OutputLabelSelector).text(outputstring);
}

function getTimeFromAMPM(time) {
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AMPM = time.match(/\s(.*)$/)[1];
    if (AMPM == "PM" && hours < 12) hours = hours + 12;
    if (AMPM == "AM" && hours == 12) hours = hours - 12;
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    if (hours < 10) sHours = "0" + sHours;
    if (minutes < 10) sMinutes = "0" + sMinutes;
    return sHours + ":" + sMinutes;
}


Date.MilliSecondsToDHMs = function (t) {
    var cd = 24 * 60 * 60 * 1000,
        ch = 60 * 60 * 1000,
        d = Math.floor(t / cd),
        h = Math.floor((t - d * cd) / ch),
        m = Math.round((t - d * cd - h * ch) / 60000),
        pad = function (n) { return n < 10 ? '0' + n : n; };
    if (m === 60) {
        h++;
        m = 0;
    }
    if (h === 24) {
        d++;
        h = 0;
    }
    return [d, pad(h), pad(m)].join(':');
}

function GetDHMs(minutes) {
    var h = parseInt(minutes / 60);
    var m = parseInt(minutes - (h * 60));
    return ((h > 0) ? h.toString() + " hrs " + m.toString() + " mins" : m.toString() + " mins");
}

function GetDHMWithSec(minutes) {
    var h = parseInt(minutes / 60);
    var m = parseInt(minutes - (h * 60));
    var s = parseInt((minutes * 60) - ((h * 3600) + (m * 60)));
    return ((h > 0) ? h.toString() + " hrs " + m.toString() + " mins" : m.toString() + " mins" + " " + s + " sec");
}
