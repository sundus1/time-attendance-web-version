﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PagerControl.ascx.cs" Inherits="CustomControls_PagerControl" %>
<style type="text/css">
    .auto-style1 {
        
    }
</style>
<table id="tblPaging" runat="server">
    <tr>
        <td style="text-align:left;">
            <asp:Label ID="lblStatus" runat="server" Font-Name="verdana" Font-Size="9pt"/>
        </td>
        <td style="width:10px">
            
        </td>
        <td class="auto-style1">
            <asp:LinkButton ID="lbtnShowFirst" runat="server" Text="<< First" Font-Size="9pt" OnClick="lbtnPageChanged_Click" CommandName="First" Enabled="false"></asp:LinkButton>
        </td>
        <td class="auto-style1">
            <asp:LinkButton ID="lbtnShowPrevious" runat="server" Text="< Prev" Font-Size="9pt" OnClick="lbtnPageChanged_Click" CommandName="Prev" Enabled="false"></asp:LinkButton>
        </td>
        <td class="auto-style1">
            <asp:LinkButton ID="lbtnShowNext" runat="server" Text="Next >" Font-Size="9pt" OnClick="lbtnPageChanged_Click" CommandName="Next" Enabled="false"></asp:LinkButton>
        </td>
        <td class="auto-style1">
            <asp:LinkButton ID="lbtnShowLast" runat="server" Text="Last >>" Font-Size="9pt" OnClick="lbtnPageChanged_Click" CommandName="Last" Enabled="false"></asp:LinkButton>
        </td>
    </tr>
</table>
