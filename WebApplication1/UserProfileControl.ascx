﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserProfileControl.ascx.cs" Inherits="WebApplication1_UserProfileControl" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <style>
            input[type=text], select {
                display: inline-block;
                padding: 6px;
                border: 1px solid #b7b7b7;
                border-radius: 3px;
                font: arial, helvetica, sans-serif;
            }

            formCaptionTd {
                width: 200px !important;
            }

            .readonly {
                color: #808080 !important;
            }
        </style>

        <script type="text/javascript" src="../js/jquery.timeentry.js"></script>
        <script>
            jQuery(document).ready(function ($) {
                timebox('in');
                timebox('out');
                ApplyClass();
            });
            
            function ApplyClass() {
                $(".check-time").timeEntry({
                    defaultTime: "",
                    placeholder: "HH:MM AM",
                    ampmPrefix: ' '
                });
            }

            function timebox(type) {
                if (type == 'in') {
                    if ($("[id$=chk_autoclockin]").is(":checked"))
                        $("[id$=txt_AutoClockinTime]").show(200);
                    else
                        $("[id$=txt_AutoClockinTime]").hide(200);
                }

                if (type == 'out') {
                    if ($("[id$=chk_autoclockout]").is(":checked"))
                        $("[id$=txt_AutoClockoutTime]").show(200);
                    else
                        $("[id$=txt_AutoClockoutTime]").hide(200);
                }
            }

            function validate() {
                
                if ($("[id$=chk_autoclockin]").is(":checked"))
                    if ($("[id$=txt_AutoClockinTime]").val() == "") {
                        $("[id$=txt_AutoClockinTime]").css("border", "1px solid red");
                        return false;
                    }


                if ($("[id$=chk_autoclockout]").is(":checked"))
                    if ($("[id$=txt_AutoClockoutTime]").val() == "") {
                        $("[id$=txt_AutoClockoutTime]").css("border", "1px solid red");
                        return false;
                    }

                return true;
            }

            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        </script>

        <div id="mainContentDiv">
            <div id="generrortop" runat="server"></div>
            <div class="breadCrumb" id="divgridRelatedMenu" runat="server">
                <asp:Label ID="lblpage" runat="server" />
            </div>
            <div class="formBodyDiv">
                <asp:Label runat="server" ForeColor="red" ID="lblexist"></asp:Label>
                <div class="formDiv">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td colspan="2">
                                <div class="formTitleDiv">Contact Information</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="formCaptionTd" style="width: 200px !important;">Full Name:
                            </td>
                            <td class="formFieldTd">
                                <asp:TextBox ID="txtfullname" runat="server" CssClass="standardField" MaxLength="200" />
                                <asp:RequiredFieldValidator ID="rfvtxtfullname" runat="server"
                                    ErrorMessage="Required" ControlToValidate="txtfullname"
                                    SetFocusOnError="true" ValidationGroup="info" ForeColor="Red">Required</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="formCaptionTd" style="width: 200px !important;">Email:
                            </td>
                            <td class="formFieldTd">
                                <asp:TextBox ID="txtemail" runat="server" CssClass="standardField readonly" MaxLength="50" ReadOnly="true" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtemail" Display="Dynamic" ErrorMessage="Invalid Email" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="asdwer">Invalid Email</asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="formCaptionTd" style="width: 200px !important;">Timezone:
                            </td>
                            <td class="formFieldTd">
                                <asp:DropDownList runat="server" ID="ddltimezone" Enabled="false" CssClass="ddl f-2">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvtimezone" runat="server"
                                    ErrorMessage="Required" ControlToValidate="ddltimezone"
                                    SetFocusOnError="true" ValidationGroup="info" ForeColor="Red">Required</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <asp:Literal ID="lt_cweb" runat="server"><br /></asp:Literal>
                        <tr runat="server" id="trHead">
                            <td colspan="2">
                                <div class="formTitleDiv">Attendance settings</div>
                            </td>
                        </tr>
                        <tr runat="server" id="trHead_options">
                            <td class="formCaptionTd"></td>
                            <td class="formFieldTd">
                                <%--<asp:CheckBoxList ID="chkAttendanceList" runat="server" ClientIDMode="Static">
                                    <asp:ListItem Text="User will manage his/her clockin" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Auto Clock-in" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Auto Clock-out" Value="2"></asp:ListItem>
                                </asp:CheckBoxList>--%>

                                <asp:CheckBox runat="server" ID="chk_ismanaged" Text=" User will manage his/her clockin" ClientIDMode="Static" />
                                <br />
                                <br />
                                <asp:CheckBox runat="server" ID="chk_autoclockin" Text=" Auto Clock-in" ClientIDMode="Static" onchange="timebox('in');" />
                                &nbsp;&nbsp;<asp:TextBox runat="server" ID="txt_AutoClockinTime" ClientIDMode="Static" CssClass="check-time" placeholder="hh:mm AM/PM" onfocus="this.placeholder = ''" onblur="this.placeholder = 'hh:mm AM/PM'"></asp:TextBox>
                                <br />
                                <asp:CheckBox runat="server" ID="chk_autoclockout" Text=" Auto Clock-out" ClientIDMode="Static" onchange="timebox('out');" />
                                <asp:TextBox runat="server" ID="txt_AutoClockoutTime" ClientIDMode="Static" CssClass="check-time" placeholder="hh:mm AM/PM" onfocus="this.placeholder = ''" onblur="this.placeholder = 'hh:mm AM/PM'"></asp:TextBox>
                            </td>
                        </tr>
                        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                        <tr>
                            <td class="formCaptionTd">&nbsp;</td>
                            <td class="formFieldTd">
                                <br />
                                <asp:Button runat="server" ID="btnupdateprofile" Text=" Update " CssClass="ui-state-default" OnClick="btnupdateprofile_Click" ToolTip="Save"
                                    ValidationGroup="info" OnClientClick="return validate();" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="generrorbottom" runat="server"></div>
        </div>
        <asp:HiddenField ID="hfuserid" runat="server" />
        <asp:HiddenField ID="hfoldpassword" runat="server" />
        <asp:HiddenField ID="hfGen" runat="server" />
        <asp:HiddenField ID="hffullname" runat="server" />
        <asp:HiddenField ID="hftimezone" runat="server" />
        <asp:HiddenField ID="hfwid" runat="server" />
        <asp:HiddenField ID="hfinsID" runat="server" />

    </ContentTemplate>
</asp:UpdatePanel>
