﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CustomControls_PagerControl : System.Web.UI.UserControl
{
    private const string CurrentPage_Key = "CurrentPage";
    private const string TotalRecords_Key = "TotalRecords";
    private const string PageSize_Key = "PageSize";
    public bool changeText { get; set; }
    #region Public Properties
    public int CurrentPage
    {
        get
        {
            if (this.ViewState[CurrentPage_Key] == null)
                return 0;
            else
                return Convert.ToInt32(this.ViewState[CurrentPage_Key].ToString());
        }
        set
        {
            this.ViewState[CurrentPage_Key] = value;
        }
    }

    public int TotalRecords
    {
        get
        {
            if (this.ViewState[TotalRecords_Key] == null)
                return 0;
            else
                return Convert.ToInt32(this.ViewState[TotalRecords_Key].ToString());
        }
        set
        {
            this.ViewState[TotalRecords_Key] = value;
        }
    }

    public int PageSize
    {
        get
        {
            if (this.ViewState[PageSize_Key] == null)
                return 0;
            else
                return Convert.ToInt16(this.ViewState[PageSize_Key].ToString());
        }
        set
        {
            this.ViewState[PageSize_Key] = value;
        }
    }

    public int LastPage
    {
        get
        {
            if (TotalRecords > 0)
                return (int)Math.Ceiling(Convert.ToDouble(TotalRecords) / PageSize) - 1;
            return 0;
        }
    }
    #endregion

    #region Custom Events

    public delegate void PageIndex_Changed(object sender, EventArgs e);

    public event PageIndex_Changed OnPageIndex_Changed;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void lbtnPageChanged_Click(object sender, EventArgs e)
    {
        LinkButton lbtn = sender as LinkButton;
        if (lbtn.CommandName == "First")
        {
            this.CurrentPage = 0;
        }
        if (lbtn.CommandName == "Prev")
        {
            if (this.CurrentPage > 0)
                this.CurrentPage--;
        }
        if (lbtn.CommandName == "Next")
        {
            if (this.CurrentPage < this.LastPage)
                this.CurrentPage++;
        }
        if (lbtn.CommandName == "Last")
        {
            this.CurrentPage = this.LastPage;
        }
        LoadStatus();
        if (OnPageIndex_Changed != null)
            OnPageIndex_Changed(sender, e);
    }

    public void LoadStatus()
    {
        ShowHidePagingButtons();
        if(changeText)
        {
            lblStatus.Text = "Total records found: " + this.TotalRecords + " - Showing Page: " + (CurrentPage + 1) + " of " + (LastPage + 1);
        }
        else
        {
            //lblStatus.Text = "Total workers in this department: " + this.TotalRecords + " - Showing Page: " + (CurrentPage + 1) + " of " + (LastPage + 1);
            lblStatus.Text = "Total records found: " + this.TotalRecords + " - Showing Page: " + (CurrentPage + 1) + " of " + (LastPage + 1);

        }
    }

    protected void ShowHidePagingButtons()
    {
        if (this.TotalRecords > 0)
            tblPaging.Visible = true;
        if (TotalRecords > PageSize)
        {
            if (CurrentPage != 0)
            {
                lbtnShowFirst.Enabled = true;
                lbtnShowFirst.Visible = true;
            }
            else
            {
                lbtnShowFirst.Enabled = false;
                lbtnShowFirst.Visible = false;
            }
            if (CurrentPage != LastPage)
            {
                lbtnShowLast.Enabled = true;
                lbtnShowLast.Visible = true;
            }
            else
            {
                lbtnShowLast.Enabled = false;
                lbtnShowLast.Visible = false;
            }
            if (CurrentPage < LastPage)
            {
                lbtnShowNext.Enabled = true;
                lbtnShowLast.Visible = true;
                lbtnShowLast.Enabled = true;
                lbtnShowNext.Visible = true;
            }
            else
            {
                lbtnShowNext.Enabled = false;
                lbtnShowNext.Visible = false;
            }
            if (CurrentPage > 0)
            {
                lbtnShowPrevious.Enabled = true;
                lbtnShowPrevious.Visible = true;
            }
            else
            {
                lbtnShowPrevious.Enabled = false;
                lbtnShowPrevious.Visible = false;
            }
        }
        else
        {
            lbtnShowNext.Visible = false;
            lbtnShowLast.Visible = false;
            lbtnShowPrevious.Visible = false;
            lbtnShowFirst.Visible = false;

        }

    }
}