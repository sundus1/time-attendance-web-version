﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebApplication1_userprofile : System.Web.UI.UserControl
{
    public string InstanceID
    {
        get { return hfinsID.Value; }
        set
        {
            if (!string.IsNullOrEmpty(value))
                hfinsID.Value = value;
        }
    }
    AvaimaUserProfile objwebser;
    AvaimaEmailAPI objemail;
    public string wid
    {
        get { return hfwid.Value; }
        set
        {
            if (!string.IsNullOrEmpty(value))
                hfwid.Value = value;
        }
    }

    public string userprofileid
    {
        get { return hfuserid.Value; }
        set
        {
            if (!string.IsNullOrEmpty(Request["userid"]))
                hfuserid.Value = Request["userid"];
            else
            {
                hfuserid.Value = value;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        objwebser = new AvaimaUserProfile();
        ddlcountry.DataSource = objwebser.GetCountries();
        ddlcountry.DataBind();
        ddlcountry1.DataSource = objwebser.GetCountries();
        ddlcountry1.DataBind();
        ddlcountry2.DataSource = objwebser.GetCountries();
        ddlcountry2.DataBind();
        if (!IsPostBack)
        {
            bind_dates();
            bind_data();
        }
    }

    public string ddlrole_SelectedValue { get { return ddlrole.SelectedValue; } set { ddlrole.SelectedValue = value; } }

    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsValid)
            {
                string text = insertUser();
                if (text == "notadded")
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "mainscript12",
                                                        "alert('user already exist..')", true);
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "mainscript12",
                                                        "alert('user added sucessfully..')", true);
                    Response.Redirect("add_worker.aspx?id=" + wid + "&instanceid=" + this.InstanceID);
                }
            }
        }
        catch (Exception se)
        {
            throw se;
        }
    }

    public void Updateworkername(string workername, string email)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("updatedworkername", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", wid);
                    cmd.Parameters.AddWithValue("@title", workername);
                    cmd.Parameters.AddWithValue("@email", email);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    public string Checkuserexist(string email)
    {
        string exist;
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("SELECT * FROM attendence_management WHERE email=@email and instanceId = @ins", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@ins", InstanceID);
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                    exist = "exist";
                else
                    exist = "";
            }
            return exist;
        }
    }

    public void Insertuserrole(string userid, string email)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("adduserrole", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", wid);
                    cmd.Parameters.AddWithValue("@role", "worker");
                    cmd.Parameters.AddWithValue("@userid", userid);
                    cmd.Parameters.AddWithValue("@email", email);
                    cmd.Parameters.AddWithValue("@instanceid", InstanceID);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }

        }
        catch (Exception ex)
        {

            throw;
        }
    }

    private string insertUser()
    {
        //objuser.insertcompanyUser();
        string usercheck = Checkuserexist(txtemail.Text);

        if (string.IsNullOrEmpty(usercheck))
        {
            AddinstanceWS objaddinst = new AddinstanceWS();
            string userid = objaddinst.AddInstance(txtemail.Text, "6e815b00-6e13-4839-a57d-800a92809f21", InstanceID, HttpContext.Current.User.Identity.Name, txtfirstname.Text, txtlastname.Text);
            //userid = new Guid().ToString();
            //string userid = "714fcd17";

            Hashtable objuser = new Hashtable();
            objwebser = new AvaimaUserProfile();
            string aemail = "";
            string aphone = "";
            string acell = "";
            string afax = "";
            string awebsite = "";
            string acwebsite = "";
            IEnumerator en = Request.Form.GetEnumerator();
            while (en.MoveNext())
            {
                if (en.Current.ToString().Contains("Alternate"))
                {
                    aemail += Request.Form[en.Current.ToString()] + "|";
                }
                else if (en.Current.ToString().Contains("Phone"))
                {
                    aphone += Request.Form[en.Current.ToString()] + "|";
                }
                else if (en.Current.ToString().Contains("Cell"))
                {
                    acell += Request.Form[en.Current.ToString()] + "|";
                }
                else if (en.Current.ToString().Contains("Fax"))
                {
                    afax += Request.Form[en.Current.ToString()] + "|";
                }
                else if (en.Current.ToString().Contains("Website"))
                {
                    awebsite += Request.Form[en.Current.ToString()] + "|";
                }
                else if (en.Current.ToString().Contains("Company"))
                {
                    acwebsite += Request.Form[en.Current.ToString()] + "|";
                }
            }

            string phone = aphone.Trim('|');

            objuser.Add("userid", userid);
            objuser.Add("username", "");

            //objuser.Email = txtemail.Text == "" ? "" : AvaimaUtilities.EncryptData(txtemail.Text);
            objuser.Add("email", txtemail.Text == "" ? "" : txtemail.Text);

            //objuser.AlternatEemail = aemail.Trim('|');
            objuser.Add("AlternatEemail", aemail.Trim('|'));

            //objuser.Phone = phone;
            objuser.Add("Phone", phone);

            //objuser.Company = txtorganization.Text;
            objuser.Add("Company", txtorganization.Text);

            //objuser.Designation = txtdesignation.Text;
            objuser.Add("Designation", txtdesignation.Text);

            //objuser.FirstName = txtfirstname.Text;
            objuser.Add("FirstName", txtfirstname.Text);

            //objuser.LastName = txtlastname.Text;
            objuser.Add("LastName", txtlastname.Text);

            //objuser.Gender = ddlgender.SelectedValue;
            objuser.Add("Gender", ddlgender.SelectedValue);

            //objuser.Industry = ddlindustry.SelectedValue;
            objuser.Add("Industry", ddlindustry.SelectedValue);

            string Address = txtaddress.Text;
            if (txtaddress1.Text.Length > 0)
                Address = Address + "|" + txtaddress1.Text;
            if (txtaddress2.Text.Length > 0)
                Address = Address + "|" + txtaddress2.Text;

            //objuser.Address = Address;
            objuser.Add("Address", Address);

            string PostalCode = txtzipcode.Text;
            if (txtzipcode1.Text.Length > 0)
                PostalCode = PostalCode + "|" + txtzipcode1.Text;
            if (txtzipcode2.Text.Length > 0)
                PostalCode = PostalCode + "|" + txtzipcode2.Text;

            //objuser.PostalCode = PostalCode;
            objuser.Add("PostalCode", PostalCode);

            string City = txtcity.Text;
            if (txtcity1.Text.Length > 0)
                City = City + "|" + txtcity1.Text;
            if (txtcity2.Text.Length > 0)
                City = City + "|" + txtcity2.Text;

            //objuser.City = City;
            objuser.Add("City", City);

            string Province = txtstate.Text;
            if (txtstate1.Text.Length > 0)
                Province = Province + "|" + txtstate1.Text;
            if (txtstate2.Text.Length > 0)
                Province = Province + "|" + txtstate2.Text;

            //objuser.Province = Province;
            objuser.Add("state", Province);

            string Country = ddlcountry.Text;
            if (ddlcountry1.Text.Length > 0)
                Country = Country + "|" + ddlcountry1.Text;
            if (ddlcountry2.Text.Length > 0)
                Country = Country + "|" + ddlcountry2.Text;

            //objuser.Country = Country;
            objuser.Add("Country", Country);

            string cellphone = acell.Trim('|');
            //objuser.CellNo = cellphone;
            objuser.Add("CellNo", cellphone);

            string fax = afax.Trim('|');
            string cweb = acwebsite.Trim('|');


            //objuser.Fax = fax;
            objuser.Add("Fax", fax);

            //objuser.DOB = Convert.ToDateTime(String.Format("{0}/{1}/{2}", ddlmonth.SelectedValue, ddlday.SelectedValue, ddlyear.SelectedValue));
            objuser.Add("DOB", Convert.ToDateTime(String.Format("{0}/{1}/{2}", ddlmonth.SelectedValue, ddlday.SelectedValue, ddlyear.SelectedValue)));
            //objuser.Website = awebsite.Trim('|');
            objuser.Add("Website", awebsite.Trim('|'));

            //objuser.Vender_id = hfuserid.Value;
            objuser.Add("workerid", wid);

            objwebser.insertUserFull(objuser["userid"].ToString(), objuser["username"].ToString(), objuser["email"].ToString(), objuser["AlternatEemail"].ToString(), objuser["Phone"].ToString(), objuser["Country"].ToString(),
                            objuser["City"].ToString(), objuser["Company"].ToString(), objuser["Designation"].ToString(), objuser["FirstName"].ToString(), objuser["LastName"].ToString(), objuser["Gender"].ToString(), objuser["Industry"].ToString(),
                            objuser["state"].ToString(), objuser["PostalCode"].ToString(), objuser["Address"].ToString(), objuser["CellNo"].ToString(), objuser["Fax"].ToString(), objuser["Website"].ToString(), objuser["workerid"].ToString(), InstanceID, objuser["DOB"].ToString());

            Insertuserrole(objuser["userid"].ToString(), objuser["email"].ToString());
            objemail = new AvaimaEmailAPI();
            objemail.verifyinfoemail(HttpContext.Current.User.Identity.Name, objuser["userid"].ToString(), objuser["email"].ToString(), "Attendance Management");
            string workername = objuser["FirstName"].ToString() + " " + objuser["LastName"].ToString();
            string email = objuser["email"].ToString();
            Updateworkername(workername, email);
            return "added";
        }
        else
        {
            return "notadded";
            //ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('User already Exist...')", true);
            //lblexist.Text="User already Exist...";
            //lblexist.Focus();
        }
    }

    protected void cvdob_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime dt;

        args.IsValid = DateTime.TryParse(String.Format("{0}/{1}/{2}", ddlmonth.SelectedValue, ddlday.SelectedValue, ddlyear.SelectedValue), out dt);
    }
    private void bind_dates()
    {
        for (int i = 1; i < 13; i++)
        {
            ddlmonth.Items.Add(new DateTime(1, i, 1).ToString("MMMM"));
            ddlmonth.Items[i - 1].Value = i.ToString();
        }
        for (int i = 1; i < 32; i++)
            ddlday.Items.Add(i.ToString());
        for (int i = DateTime.Now.Year - 5; i > DateTime.Now.Year - 106; i--)
            ddlyear.Items.Add(i.ToString());


    }


    private void bind_data()
    {
        objwebser = new AvaimaUserProfile();
        DataTable dtuser = new DataTable();
        string template = @"<tr><td class=""formCaptionTd"">#type#:</td>
                            <td class=""formFieldTd"">                                          
                                <input type=""text"" id=""#type##count#"" name=""#type##count#"" value=""#value#"" class=""standardField"" MaxLength=""50"" /></td></tr>";

        string template2 = @"&nbsp;<a id=""a#type##count#"" href=""javascript:addrow('#type##count#','#count1#','#type#');"">Add Row</a>&nbsp;<a id=""d#type##count#"" href=""javascript:delrow('#type##count#','#type##count2#','#count2#','#type#');"">Delete Row</a>";

        if (!string.IsNullOrEmpty(hfuserid.Value))
        {
            dtuser = objwebser.getuserbyidandInstanceid(userprofileid, InstanceID);
        }

        if (dtuser.Rows.Count > 0)
        {
            hfoldpassword.Value = dtuser.Rows[0]["password"].ToString() == "" ? "" : dtuser.Rows[0]["password"].ToString();
            //txtusername.Text = objuser.UserName;
            txtemail.Text = dtuser.Rows[0]["email"].ToString() == "" ? "" : dtuser.Rows[0]["email"].ToString();



            txtorganization.Text = dtuser.Rows[0]["company"].ToString();
            txtdesignation.Text = dtuser.Rows[0]["designation"].ToString();



            txtfirstname.Text = dtuser.Rows[0]["firstname"].ToString();
            txtlastname.Text = dtuser.Rows[0]["lastname"].ToString();
            ddlgender.SelectedValue = dtuser.Rows[0]["gender"].ToString();
            ddlindustry.SelectedValue = dtuser.Rows[0]["industry"].ToString();

            if (!dtuser.Rows[0]["city"].ToString().Contains("|"))
                txtcity.Text = dtuser.Rows[0]["city"].ToString();
            else
            {
                string[] City = dtuser.Rows[0]["city"].ToString().Split('|');
                if (City.Length > 2)
                {
                    txtcity.Text = City[0];
                    txtcity1.Text = City[1];
                    txtcity2.Text = City[2];
                    tr_add1.Style.Add("display", "block");
                    tr_add1.Style.Add("display", "");
                    tr_add2.Style.Add("display", "block");
                    tr_add2.Style.Add("display", "");
                }
                else if (City.Length > 1)
                {
                    txtcity.Text = City[0];
                    txtcity1.Text = City[1];
                    tr_add1.Style.Add("display", "block");
                    tr_add1.Style.Add("display", "");
                }

            }

            if (!dtuser.Rows[0]["country"].ToString().Contains("|"))
                ddlcountry.SelectedValue = dtuser.Rows[0]["country"].ToString();
            else
            {
                string[] Country = dtuser.Rows[0]["country"].ToString().Split('|');
                if (Country.Length > 2)
                {
                    ddlcountry.SelectedValue = Country[0];
                    ddlcountry1.SelectedValue = Country[1];
                    ddlcountry2.SelectedValue = Country[2];

                }
                else if (Country.Length > 1)
                {
                    ddlcountry.SelectedValue = Country[0];
                    ddlcountry1.SelectedValue = Country[1];

                }

            }
            if (!dtuser.Rows[0]["state"].ToString().Contains("|"))
                txtstate.Text = dtuser.Rows[0]["state"].ToString();
            else
            {
                string[] Province = dtuser.Rows[0]["state"].ToString().Split('|');
                if (Province.Length > 2)
                {
                    txtstate.Text = Province[0];
                    txtstate1.Text = Province[1];
                    txtstate2.Text = Province[2];
                    tr_add1.Style.Add("display", "block");
                    tr_add1.Style.Add("display", "");
                    tr_add2.Style.Add("display", "block");
                    tr_add2.Style.Add("display", "");
                }
                else if (Province.Length > 1)
                {
                    txtstate.Text = Province[0];
                    txtstate1.Text = Province[1];
                    tr_add1.Style.Add("display", "block");
                    tr_add1.Style.Add("display", "");
                }

            }
            if (!dtuser.Rows[0]["postalcode"].ToString().Contains("|"))
                txtzipcode.Text = dtuser.Rows[0]["postalcode"].ToString();
            else
            {
                string[] PostalCode = dtuser.Rows[0]["postalcode"].ToString().Split('|');
                if (PostalCode.Length > 2)
                {
                    txtzipcode.Text = PostalCode[0];
                    txtzipcode1.Text = PostalCode[1];
                    txtzipcode2.Text = PostalCode[2];
                    tr_add1.Style.Add("display", "block");
                    tr_add1.Style.Add("display", "");
                    tr_add2.Style.Add("display", "block");
                    tr_add2.Style.Add("display", "");
                }
                else if (PostalCode.Length > 1)
                {
                    txtzipcode.Text = PostalCode[0];
                    txtzipcode1.Text = PostalCode[1];
                    tr_add1.Style.Add("display", "block");
                    tr_add1.Style.Add("display", "");
                }

            }
            if (!dtuser.Rows[0]["address"].ToString().Contains("|"))
                txtaddress.Text = dtuser.Rows[0]["address"].ToString();
            else
            {
                string[] Address = dtuser.Rows[0]["address"].ToString().Split('|');
                if (Address.Length > 2)
                {
                    txtaddress.Text = Address[0];
                    txtaddress1.Text = Address[1];
                    txtaddress2.Text = Address[2];
                    tr_add1.Style.Add("display", "block");
                    tr_add1.Style.Add("display", "");
                    tr_add2.Style.Add("display", "block");
                    tr_add2.Style.Add("display", "");
                }
                else if (Address.Length > 1)
                {
                    txtaddress.Text = Address[0];
                    txtaddress1.Text = Address[1];
                    tr_add1.Style.Add("display", "block");
                    tr_add1.Style.Add("display", "");
                }

            }


            if (!dtuser.Rows[0]["alternateemail"].ToString().Contains("|"))
            {
                Alternateemail0.Text = dtuser.Rows[0]["alternateemail"].ToString();
                lt_Emails.Text = "";
            }
            else
            {
                StringBuilder altemail = new StringBuilder();
                string[] AlternatEemail = dtuser.Rows[0]["alternateemail"].ToString().Split('|');
                Alternateemail0.Text = AlternatEemail[0];
                for (int email = 1; email < AlternatEemail.Length; email++)
                {
                    if (!String.IsNullOrEmpty(AlternatEemail[email]))
                        altemail.Append(template.Replace("#type#", "Alternateemail").Replace("#count#", email.ToString()).Replace("#value#", AlternatEemail[email]));
                }
                if (altemail.Length > 0)
                    lt_Emails.Text = altemail.Replace("</td></tr>", "", altemail.Length - 10, 10).Append(template2.Replace("#type#", "Alternateemail").Replace("#count#", (Convert.ToInt32(AlternatEemail.Length) - 1).ToString()).Replace("#count1#", (Convert.ToInt32(AlternatEemail.Length)).ToString()).Replace("#count2#", (Convert.ToInt32(AlternatEemail.Length) - 2).ToString())).ToString();


            }

            if (!dtuser.Rows[0]["phone"].ToString().Contains("|"))
            {
                Phone0.Text = dtuser.Rows[0]["phone"].ToString();
                lt_phone.Text = "";
            }

            else
            {
                string[] phones = dtuser.Rows[0]["phone"].ToString().Split('|');
                StringBuilder sbphone = new StringBuilder();
                Phone0.Text = phones[0];
                for (int phone = 1; phone < phones.Length; phone++)
                {
                    if (!String.IsNullOrEmpty(phones[phone]))
                        sbphone.Append(template.Replace("#type#", "Phone").Replace("#count#", phone.ToString()).Replace("#value#", phones[phone]));
                }
                if (sbphone.Length > 0)
                    lt_phone.Text = sbphone.Replace("</td></tr>", "", sbphone.Length - 10, 10).Append(template2.Replace("#type#", "Phone").Replace("#count#", (Convert.ToInt32(phones.Length) - 1).ToString()).Replace("#count1#", (Convert.ToInt32(phones.Length)).ToString()).Replace("#count2#", (Convert.ToInt32(phones.Length) - 2).ToString())).ToString();

            }
            if (!dtuser.Rows[0]["cellphone"].ToString().Contains("|"))
            {
                Cellphone0.Text = dtuser.Rows[0]["cellphone"].ToString();
                lt_cphone.Text = "";
            }

            else
            {
                StringBuilder sbcphone = new StringBuilder();
                string[] cellphones = dtuser.Rows[0]["cellphone"].ToString().Split('|');
                Cellphone0.Text = cellphones[0];
                for (int ce = 1; ce < cellphones.Length; ce++)
                {
                    if (!String.IsNullOrEmpty(cellphones[ce]))
                        sbcphone.Append(template.Replace("#type#", "Cell phone").Replace("#count#", ce.ToString()).Replace("#value#", cellphones[ce]));
                }
                if (sbcphone.Length > 0)
                    lt_cphone.Text = sbcphone.Replace("</td></tr>", "", sbcphone.Length - 10, 10).Append(template2.Replace("#type#", "Cell phone").Replace("#count#", (Convert.ToInt32(cellphones.Length) - 1).ToString()).Replace("#count1#", (Convert.ToInt32(cellphones.Length)).ToString()).Replace("#count2#", (Convert.ToInt32(cellphones.Length) - 2).ToString())).ToString();

            }
            if (!dtuser.Rows[0]["fax"].ToString().Contains("|"))
            {
                Fax0.Text = dtuser.Rows[0]["fax"].ToString();
                lt_fax.Text = "";
            }

            else
            {
                StringBuilder sbfax = new StringBuilder();
                string[] fax = dtuser.Rows[0]["fax"].ToString().Split('|');
                Fax0.Text = fax[0];
                for (int ce = 1; ce < fax.Length; ce++)
                {
                    if (!String.IsNullOrEmpty(fax[ce]))
                        sbfax.Append(template.Replace("#type#", "Fax").Replace("#count#", ce.ToString()).Replace("#value#", fax[ce]));
                }
                if (sbfax.Length > 0)
                    lt_fax.Text = sbfax.Replace("</td></tr>", "", sbfax.Length - 10, 10).Append(template2.Replace("#type#", "Fax").Replace("#count#", (Convert.ToInt32(fax.Length) - 1).ToString()).Replace("#count1#", (Convert.ToInt32(fax.Length)).ToString()).Replace("#count2#", (Convert.ToInt32(fax.Length) - 2).ToString())).ToString();

            }

            if (!dtuser.Rows[0]["website"].ToString().Contains("|"))
            {
                Website0.Text = dtuser.Rows[0]["website"].ToString();
                lt_web.Text = "";
            }

            else
            {
                StringBuilder sbweb = new StringBuilder();
                string[] Website = dtuser.Rows[0]["website"].ToString().Split('|');
                Website0.Text = Website[0];
                for (int ce = 1; ce < Website.Length; ce++)
                {
                    if (!String.IsNullOrEmpty(Website[ce]))
                        sbweb.Append(template.Replace("#type#", "Website").Replace("#count#", ce.ToString()).Replace("#value#", Website[ce]));
                }
                if (sbweb.Length > 0)
                    lt_web.Text = sbweb.Replace("</td></tr>", "", sbweb.Length - 10, 10).Append(template2.Replace("#type#", "Website").Replace("#count#", (Convert.ToInt32(Website.Length) - 1).ToString()).Replace("#count1#", (Convert.ToInt32(Website.Length)).ToString()).Replace("#count2#", (Convert.ToInt32(Website.Length) - 2).ToString())).ToString();

            }

            if (!dtuser.Rows[0]["Cwebsites"].ToString().Contains("|"))
            {
                Companywebsite0.Text = dtuser.Rows[0]["Cwebsites"].ToString();
                lt_cweb.Text = "";
            }

            else
            {
                StringBuilder sbcweb = new StringBuilder();
                string[] cwebsite = dtuser.Rows[0]["Cwebsites"].ToString().Split('|');
                Companywebsite0.Text = cwebsite[0];
                for (int ce = 1; ce < cwebsite.Length; ce++)
                {
                    if (!String.IsNullOrEmpty(cwebsite[ce]))
                        sbcweb.Append(template.Replace("#type#", "Campany website").Replace("#count#", ce.ToString()).Replace("#value#", cwebsite[ce]));
                }
                if (sbcweb.Length > 0)
                    lt_cweb.Text = sbcweb.Replace("</td></tr>", "", sbcweb.Length - 10, 10).Append(template2.Replace("#type#", "Company website").Replace("#count#", (Convert.ToInt32(cwebsite.Length) - 1).ToString()).Replace("#count1#", (Convert.ToInt32(cwebsite.Length)).ToString()).Replace("#count2#", (Convert.ToInt32(cwebsite.Length) - 2).ToString())).ToString();

            }

            ddlmonth.SelectedValue = Convert.ToDateTime(dtuser.Rows[0]["dob"].ToString()).Month.ToString();
            ddlday.SelectedValue = Convert.ToDateTime(dtuser.Rows[0]["dob"].ToString()).Day.ToString();
            ddlyear.SelectedValue = Convert.ToDateTime(dtuser.Rows[0]["dob"].ToString()).Year.ToString();



            btnupdate.Visible = false;
            btnupdateprofile.Visible = true;
            //btncancel.Visible = false;
        }


        //hlcname.NavigateUrl = Request.Url.ToString();
        //hlcname.Text = "Contact Information";
        //lblpage.Visible = false;



    }
    protected void btnupdateprofile_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(userprofileid))
            updateprofile();
    }
    private void updateprofile()
    {
        string userid = userprofileid;
        //string userid = "714fcd17";

        Hashtable objuser = new Hashtable();
        objwebser = new AvaimaUserProfile();
        string aemail = "";
        string aphone = "";
        string acell = "";
        string afax = "";
        string awebsite = "";
        string acwebsite = "";
        IEnumerator en = Request.Form.GetEnumerator();
        while (en.MoveNext())
        {
            if (en.Current.ToString().Contains("Alternate"))
            {
                aemail += Request.Form[en.Current.ToString()] + "|";
            }
            else if (en.Current.ToString().Contains("Phone"))
            {
                aphone += Request.Form[en.Current.ToString()] + "|";
            }
            else if (en.Current.ToString().Contains("Cell"))
            {
                acell += Request.Form[en.Current.ToString()] + "|";
            }
            else if (en.Current.ToString().Contains("Fax"))
            {
                afax += Request.Form[en.Current.ToString()] + "|";
            }
            else if (en.Current.ToString().Contains("Website"))
            {
                awebsite += Request.Form[en.Current.ToString()] + "|";
            }
            else if (en.Current.ToString().Contains("Company"))
            {
                acwebsite += Request.Form[en.Current.ToString()] + "|";
            }
        }

        string phone = aphone.Trim('|');

        objuser.Add("userid", userid);
        objuser.Add("username", "");

        //objuser.Email = txtemail.Text == "" ? "" : AvaimaUtilities.EncryptData(txtemail.Text);
        objuser.Add("email", txtemail.Text == "" ? "" : txtemail.Text);

        //objuser.AlternatEemail = aemail.Trim('|');
        objuser.Add("AlternatEemail", aemail.Trim('|'));

        //objuser.Phone = phone;
        objuser.Add("Phone", phone);

        //objuser.Company = txtorganization.Text;
        objuser.Add("Company", txtorganization.Text);

        //objuser.Designation = txtdesignation.Text;
        objuser.Add("Designation", txtdesignation.Text);

        //objuser.FirstName = txtfirstname.Text;
        objuser.Add("FirstName", txtfirstname.Text);

        //objuser.LastName = txtlastname.Text;
        objuser.Add("LastName", txtlastname.Text);

        //objuser.Gender = ddlgender.SelectedValue;
        objuser.Add("Gender", ddlgender.SelectedValue);

        //objuser.Industry = ddlindustry.SelectedValue;
        objuser.Add("Industry", ddlindustry.SelectedValue);

        string Address = txtaddress.Text;
        if (txtaddress1.Text.Length > 0)
            Address = Address + "|" + txtaddress1.Text;
        if (txtaddress2.Text.Length > 0)
            Address = Address + "|" + txtaddress2.Text;

        //objuser.Address = Address;
        objuser.Add("Address", Address);

        string PostalCode = txtzipcode.Text;
        if (txtzipcode1.Text.Length > 0)
            PostalCode = PostalCode + "|" + txtzipcode1.Text;
        if (txtzipcode2.Text.Length > 0)
            PostalCode = PostalCode + "|" + txtzipcode2.Text;

        //objuser.PostalCode = PostalCode;
        objuser.Add("PostalCode", PostalCode);

        string City = txtcity.Text;
        if (txtcity1.Text.Length > 0)
            City = City + "|" + txtcity1.Text;
        if (txtcity2.Text.Length > 0)
            City = City + "|" + txtcity2.Text;

        //objuser.City = City;
        objuser.Add("City", City);

        string Province = txtstate.Text;
        if (txtstate1.Text.Length > 0)
            Province = Province + "|" + txtstate1.Text;
        if (txtstate2.Text.Length > 0)
            Province = Province + "|" + txtstate2.Text;

        //objuser.Province = Province;
        objuser.Add("state", Province);

        string Country = ddlcountry.Text;
        if (ddlcountry1.Text.Length > 0)
            Country = Country + "|" + ddlcountry1.Text;
        if (ddlcountry2.Text.Length > 0)
            Country = Country + "|" + ddlcountry2.Text;

        //objuser.Country = Country;
        objuser.Add("Country", Country);

        string cellphone = acell.Trim('|');
        //objuser.CellNo = cellphone;
        objuser.Add("CellNo", cellphone);

        string fax = afax.Trim('|');
        string cweb = acwebsite.Trim('|');


        //objuser.Fax = fax;
        objuser.Add("Fax", fax);

        //objuser.DOB = Convert.ToDateTime(String.Format("{0}/{1}/{2}", ddlmonth.SelectedValue, ddlday.SelectedValue, ddlyear.SelectedValue));
        objuser.Add("DOB", Convert.ToDateTime(String.Format("{0}/{1}/{2}", ddlmonth.SelectedValue, ddlday.SelectedValue, ddlyear.SelectedValue)));
        //objuser.Website = awebsite.Trim('|');
        objuser.Add("Website", awebsite.Trim('|'));

        //objuser.Vender_id = hfuserid.Value;
        objuser.Add("workerid", wid);

        objwebser.UpdateUser(objuser["userid"].ToString(), objuser["username"].ToString(), objuser["email"].ToString(), objuser["AlternatEemail"].ToString(), objuser["Phone"].ToString(), objuser["Country"].ToString(),
                        objuser["City"].ToString(), objuser["Company"].ToString(), objuser["Designation"].ToString(), objuser["FirstName"].ToString(), objuser["LastName"].ToString(), objuser["Gender"].ToString(), objuser["Industry"].ToString(),
                        objuser["state"].ToString(), objuser["PostalCode"].ToString(), objuser["Address"].ToString(), objuser["CellNo"].ToString(), objuser["Fax"].ToString(), objuser["Website"].ToString(), objuser["workerid"].ToString(), objuser["DOB"].ToString());
        string workername = objuser["FirstName"].ToString() + " " + objuser["LastName"].ToString();
        string email = objuser["email"].ToString();
        Updateworkername(workername, email);
        bind_dates();
        bind_data();
        lblexist.ForeColor = System.Drawing.Color.Green;
        lblexist.Text = "Sucessfully Updated!";
        lblexist.Visible = true;
        lblexist.Focus();
    }
}