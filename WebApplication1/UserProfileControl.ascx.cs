﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebApplication1_UserProfileControl : System.Web.UI.UserControl
{
    Attendance atd = new Attendance();
    DataAccessLayer dalAvaima = new DataAccessLayer(true);
    string UserId = "", InstanceID = "", UserAvaimaID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        UserId = Request.QueryString["uid"].ToString();
        InstanceID = Request.QueryString["instanceid"].ToString();
        UserAvaimaID = atd.Timezone_datetime(UserId, "", "");

        if (!IsPostBack)
            BindData();
    }

    public void BindData()
    {
        txtfullname.Text = SP.GetWorkerName(UserId);
        foreach (KeyValuePair<string, string> entry in atd.TimeZone)
        {
            ddltimezone.Items.Add(new ListItem(entry.Key, entry.Value));
        }

        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["aviamaConn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("Select * from cms_user where userid = @userid", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", UserAvaimaID);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    txtemail.Text = dt.Rows[0]["useremail"].ToString();
                    ddltimezone.SelectedValue = dt.Rows[0]["timezone"].ToString();
                }
            }
        }

        //Get User Role and hide detail properties
        if (atd.getParent(UserId) != Request.QueryString["id"].ToString())
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_getuserrole", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@userid", UserId);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        if (Convert.ToString(dt.Rows[0]["role"]) != "superuser" || Convert.ToString(dt.Rows[0]["role"]) != "admin")
                        {
                            trHead.Visible = trHead_options.Visible = false;
                        }
                        else
                            trHead.Visible = trHead_options.Visible = true;
                    }
                }
            }
        }

        //Check if user is Admin & employee(UserId) is assigned to him than enable detail properties
        if (atd.isAdmin(Request.QueryString["id"].ToInt32()))
            trHead.Visible = trHead_options.Visible = true;


        //User Attendance settings
        dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_UserAttendanceSettings", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", UserId);
                cmd.Parameters.AddWithValue("@action", "get");
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    if (Convert.ToBoolean(dt.Rows[0]["isManaged"]))
                        chk_ismanaged.Checked = true;

                    if (Convert.ToBoolean(dt.Rows[0]["AutoClockin"]))
                    {
                        chk_autoclockin.Checked = true;
                        txt_AutoClockinTime.Text = dt.Rows[0]["AutoClockin_Time"].ToString();
                    }

                    if (Convert.ToBoolean(dt.Rows[0]["AutoClockout"]))
                    {
                        chk_autoclockout.Checked = true;
                        txt_AutoClockoutTime.Text = dt.Rows[0]["AutoClockout_Time"].ToString();
                    }
                }
            }
        }

        ScriptManager.RegisterStartupScript(this, GetType(), "timebox", "timebox('in');timebox('out');ApplyClass();", true);

    }

    protected void btnupdateprofile_Click(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_UserAttendanceSettings", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", UserId);
                cmd.Parameters.AddWithValue("@InstanceId", InstanceID);
                cmd.Parameters.AddWithValue("@title", txtfullname.Text.Trim());
                cmd.Parameters.AddWithValue("@isManaged", chk_ismanaged.Checked);
                if (chk_autoclockin.Checked)
                {
                    cmd.Parameters.AddWithValue("@AutoClockin", chk_autoclockin.Checked);
                    cmd.Parameters.AddWithValue("@AutoClockin_Time", (txt_AutoClockinTime.Text != "") ? txt_AutoClockinTime.Text : null);
                }
                if (chk_autoclockout.Checked)
                {
                    cmd.Parameters.AddWithValue("@AutoClockout", chk_autoclockout.Checked);
                    cmd.Parameters.AddWithValue("@AutoClockout_Time", (txt_AutoClockoutTime.Text != "") ? txt_AutoClockoutTime.Text : null);
                }
                cmd.Parameters.AddWithValue("@action", "update");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        //Save Timezone
        Hashtable ht = new Hashtable();
        ht["@userid"] = UserAvaimaID;
        ht["@timezone"] = ddltimezone.SelectedValue;
        //dalAvaima.ExecuteIUDQuery("Update cms_user SET timezone = @timezone  where userid = @userid", ht);

        ScriptManager.RegisterStartupScript(this, GetType(), "reloadControl", "jq191('#divworkeradd').dialog('close');window.parent.location.reload(true);", true);

        Response.Redirect(Request.Url.AbsoluteUri);
    }
}