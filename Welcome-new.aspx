﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Welcome-new.aspx.cs" Inherits="Welcome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Time And Attendance</title>

    <script>

        function removeLastChar(value, char) {
            var lastChar = value.slice(-1);
            if (lastChar == char) {
                value = value.slice(0, -1);
            }
            return value;
        }


        function GetParameterValues(param) {
            var url = removeLastChar(window.parent.location.href, "#");
            url = url.slice(url.indexOf('?') + 1).split('&');

            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].slice(url[i].indexOf('=') + 1);
                if (url[i].slice(0, url[i].indexOf('=')) == param) {
                    return urlparam;
                }
            }
        }


        function resizeIframe() {
            var iframe = document.getElementById("MainContent_ifapp");
            var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
            if (innerDoc.body.offsetHeight) {

                iframe.height = iframe.contentWindow.document.body.scrollHeight;

                //    iframe.height = innerDoc.body.offsetHeight + 32 + "px";
            }
            else if (iframe.Document && iframe.Document.body.scrollHeight) {
                iframe.style.height = iframe.Document.body.scrollHeight + "px";
            }
        }

        function LoadPage() {
            resizeIframe();
            document.getElementById("newlink").style.display = "none";
            document.getElementById("OldUserLink").style.display = "none";
            document.getElementById("MainContent_ifapp").style.display = "block";
        }
        function resizeIframe(obj) {
            //  obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
            obj.style.height = document.body.scrollHeight + 'px';
        }


        //function openLink(link)
        //{
        //    var newlink = link.replace(/&amp;/g, '&');
        //    document.getElementById("link").href = newlink;
        //    setTimeout(function () {
        //        var win = window.open(newlink, '_blank');
        //        if (win) {
        //            //Browser has allowed it to be opened
        //            win.focus();
        //        } else {
        //            //Browser has blocked it
        //            alert('Please allow popups for this website OR Click the link to open the application');
        //           
        //        }
        //    }, 5000);
        //}        

        function openLink(link) {
            var newlink = link.replace(/&amp;/g, '&');
            setTimeout(function () {
                //window.location.href = newlink;
                window.parent.location.replace(newlink);
            }, 5000);
        }

    </script>
</head>

<body id="mainBody">
    <form runat="server" id="form1">
        <asp:HiddenField ID="hdnUSERR" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnAdminEmail" runat="server" />
        <asp:HiddenField ID="hdnAdminName" runat="server" />
        <asp:HiddenField ID="hdnAppUserID" runat="server" />
        <asp:HiddenField ID="hdnInstanceID" runat="server" />
        <asp:HiddenField runat="server" ID="_link" ClientIDMode="Static" />
        <%-- <p id="newlink">
            You will be automatically redirected to new version of <b>Time and Attendance</b> in 5 seconds. If it takes longer than please click <a id="link" runat="server" target="_blank">here</a></p>
        <p id="OldUserLink" runat="server">To keep using the old version, please click <a id="oldlink" runat="server" onclick="LoadPage();" href="#">here</a></p>
     
         <iframe id="MainContent_ifapp" runat="server" style="height:1177px !important;  display:none;" ></iframe>--%>
       
         <div style="display:none;">
            <p id="newlink" runat="server" style="font-size: 30px;">
                Time and Attendance will automatically launch in a new window in 5 seconds.
                <br />
                OR <b><a id="link" runat="server" target="_blank" clientidmode="static">Click here</a></b> to open it now. 
            </p>
            <br />
            <p style="font-size: 20px;"><b>NOTE: The new window will not open in case you have the pop-ups disabled. If this is the case, click here to open it now.</b></p>
        </div>
        
            <p style="font-size: 20px;text-align:center;"><b>Redirecting... Please wait</b></p>


        <p id="OldUserLink" runat="server" visible="false"><b>Time and Attendance</b> has a new version. To use please click<a id="link2" runat="server" target="_blank">here</a></p>

<%--        <iframe id="MainContent_ifapp" runat="server"></iframe>--%>

        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </form>

</body>
</html>

