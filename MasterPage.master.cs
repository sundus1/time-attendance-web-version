﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //+ "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString()
        navbar.HRef = "Default.aspx?id=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
        dashboard.HRef = "Default.aspx?id=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
        settings.HRef = "settings.aspx?id=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
        users.HRef = "#";
        lblYear.Text = System.DateTime.Now.Year.ToString();
    }
}
