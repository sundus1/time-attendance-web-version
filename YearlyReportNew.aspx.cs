﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Globalization;

public partial class YearlyReportNew : System.Web.UI.Page
{
    int userId = 0;
    string instanceID = "";
    public int appAssignedId { get; set; }
    private bool isAdmin = false;
    private bool IsSimpleClock = true;
    public string OwnerId { get; set; }
    public bool isWorker { get; set; }
    public List<User> Users { get; set; }
    List<AttUser> users = AttUser.GetAll();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.QueryString["id"] != null)
        {
            userId = Convert.ToInt16(Request.QueryString["id"]);
        }
        if (Request.QueryString["instanceid"] != null)
        {
            instanceID = Request.QueryString["instanceid"];
        }
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("select * from attendence_management a where a.Id = @id and a.InstanceId = @insId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", userId);
                    cmd.Parameters.AddWithValue("@insId", this.instanceID);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];
                        lblUserName.Text = dr["Title"].ToString();
                    }
                    else
                    {
                        Response.Redirect("Default.aspx?instanceid=" + this.instanceID);
                    }
                }

            }
        }
        else
        {
            Response.Redirect("Default.aspx?instanceid=" + this.instanceID);
        }
        if (!IsPostBack)
        {
            BindUsers();
            BindYears();
        }
    }
    private void BindUsers()
    {
        AddinstanceWS objinst = new AddinstanceWS();
        OwnerId = objinst.GetUserID(HttpContext.Current.User.Identity.Name);
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("getuserrole", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", OwnerId.ToString());
                cmd.Parameters.AddWithValue("@instanceid", instanceID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    if (Convert.ToString(dt.Rows[0]["role"]) == "admin")
                    {
                        isAdmin = true;
                        isWorker = false;
                    }
                    else
                    {
                        isAdmin = false;
                        isWorker = true;
                    }
                    appAssignedId = Convert.ToInt32(dt.Rows[0]["userid"]);
                }

            }
        }
        if (!isWorker)
        {
            FillUsers();
            ddlUsers.SelectedIndex = Users.FindIndex(u => u.userID == userId);
            lblUserName.Text = Users.Where(u => u.userID == userId).SingleOrDefault().userName;
            trSelUser.Visible = true;
        }
        else
        {
            AttUser user = AttUser.GetUserByID(userId);
            if (user == null)
            {
                ddlUsers.Items.Add(new ListItem("User", userId.ToString()));
            }
            else
            {
                ddlUsers.Items.Add(new ListItem(user.UserName, userId.ToString()));
            }
            ddlUsers.SelectedIndex = 1;
            trSelUser.Style.Add("display", "none");
        }
    }
    private void FillUsers()
    {
        PopulateUsersAccord(0, isAdmin);
        hfparentid.Value = "0";
    }
    private void FetchUsers(Int32 parentID)
    {
        List<AttUser> filteredUsers = users.FindAll(u => u.ParentID == parentID);
        foreach (AttUser user in filteredUsers)
        {
            if (user.Category == false)
            {
                Users.Add(new User() { userID = user.UserID, userName = user.UserName });
            }
            else
            {
                FetchUsers(user.ParentID);
            }
        }
    }
    private void BindYears()
    {
        ddlYear.Items.Clear();
        DateTime startDate = new DateTime();
        DateTime endDate = new DateTime();
        if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out userId))
        {
            startDate = SP.GetWorkerFirstSignInn(userId.ToString());
            endDate = SP.GetWorkerLastSignIn(userId.ToString());
        }
        for (int i = endDate.Year; i >= startDate.Year; i--)
        {
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }
    protected void PopulateUsersAccord(int pid, bool isAdmin = false, bool ShowFiltered = false, int status = 1)
    {
        DataTable dtstudentall = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            string q = "getallattendencerecordbyid";
            if (ShowFiltered)
            {
                if (status == 1)
                    q = "getallactiveusers";
                else
                {
                    q = "getallinactiveusers";
                }
            }
            else
            {
                if (isAdmin)
                {
                    q = "getfilteredattendancerecordbyid";
                }
            }

            using (SqlCommand cmd = new SqlCommand(q, con))
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", pid);
                cmd.Parameters.AddWithValue("@InstanceId", this.instanceID);
                if (isAdmin)
                {
                    if (q == "getfilteredattendancerecordbyid")
                    {
                        cmd.Parameters.AddWithValue("@userid", appAssignedId);
                    }
                }
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dtstudentall);
                cmd.CommandText = "getclocktype";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@instanceId", this.instanceID);
                bool i = (bool)cmd.ExecuteScalar();
                if (i)
                {
                    IsSimpleClock = false;
                }
                Users = new List<User>();
                if (dtstudentall.Rows.Count > 0)
                {
                    foreach (DataRow row in dtstudentall.Rows)
                    {
                        if (Convert.ToBoolean(row["category"].ToString()) == false)
                        {
                            Users.Add(new User() { userID = Convert.ToInt32(row[0].ToString()), userName = row[1].ToString() });
                        }
                        else
                        {
                            FetchUsers(Convert.ToInt32(row[0].ToString()));
                        }
                    }
                }
                ddlUsers.DataSource = Users;
                ddlUsers.DataTextField = "userName";
                ddlUsers.DataValueField = "userID";
                ddlUsers.DataBind();
            }
        }
    }
    protected void bk_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            Response.Redirect("Reports.aspx?id=" + userId + "&instanceid=" + this.instanceID);
        }
    }
  
    public DataTable GetYearlyAbsences(Int32 UserID, DateTime start, DateTime end)
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_GetYearlyAbsences", con))
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("@userID", UserID);
                cmd.Parameters.AddWithValue("@startDate", start);
                cmd.Parameters.AddWithValue("@endDate", end);
                adp.Fill(dt);

            }
        }
        return dt;
    }
    public DataTable GetYearlyAttendance(Int32 UserID, DateTime start, DateTime end)
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_GetYearlyHistory", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@UserId", UserID);
                cmd.Parameters.AddWithValue("@startDate", start);
                cmd.Parameters.AddWithValue("@endDate", end);
                adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }
        }
        return dt;
    }
    private string ConvertTimeSpan(TimeSpan WorkedHours)
    {
        string strWorkingHours = String.Empty;
        if (WorkedHours.Days > 0)
        {
            strWorkingHours = WorkedHours.Days + " Days";
        }
        if (WorkedHours.Hours > 0)
        {
            strWorkingHours += " " + WorkedHours.Hours + " Hours";
        }
        if (WorkedHours.Minutes > 0)
        {
            strWorkingHours += " " + WorkedHours.Minutes + " Minutes";
        }
        //if (WorkedHours.Seconds > 0)
        //{
        //    strWorkingHours += " " + WorkedHours.Seconds + " Seconds";
        //}
        return strWorkingHours;
    }
    private string WorkedHour(DataTable dt)
    {
        TimeSpan WorkedHours = new TimeSpan();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["lastout"].ToString() == null || dt.Rows[i]["lastout"].ToString() == "")
            {
                WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(DateTime.Now).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
            }
            else
            {
                WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastout"].ToString())).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
            }
        }

        return UtilityMethods.getFormatedTimeByMinutes(WorkedHours.TotalMinutes.ToInt32());
    }
    protected void btnGR_Click(object sender, EventArgs e)
    {
        string year = ddlYear.SelectedValue.ToString();
        //string month = ddlMonth.SelectedValue.ToString();
        int monthI = DateTime.ParseExact("January", "MMMM", CultureInfo.InvariantCulture).Month;
        DateTime startDate = new DateTime(Convert.ToInt16(year), 1, 1, 0, 0, 0);
        if (startDate > DateTime.Now)
        {
            //trMsg.Visible = true;
            hdnData.Value = "false";
        }
        else
        {
            //trMsg.Visible = false;
            hdnData.Value = "true";
            DateTime endDate = new DateTime(Convert.ToInt16(year), 12, DateTime.DaysInMonth(startDate.Year, startDate.Month), 23, 59, 59);
            if (endDate > DateTime.Now)
            {
                endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            }
           // DataTable dtHolidays = GetYearlyHolidays(startDate, endDate);
            DataTable dtAbsences = GetYearlyAbsences(userId, startDate, endDate);
            DataTable dtAttendance = GetYearlyAttendance(userId, startDate, endDate);
            DataTable uniqueCols = dtAttendance.DefaultView.ToTable(true, "FormatedDate");

           // List<DateTime> lst = GetDatesBetween(startDate, endDate);
            lblWHours.Text = WorkedHour(dtAttendance);
           // CreateTable(dtAttendance, dtHolidays, dtAbsences, lst);
           
            lblTotalAbsences.Text = dtAbsences.Rows.Count.ToString();
            lblTotDays.Text = uniqueCols.Rows.Count.ToString();
            lblUserName.Text = ((ListItem)ddlUsers.SelectedItem).Text;
        }
    }
}