﻿
function UpdateMenu(active) {
    var instanceid = $("input[id^='hdnInstanceId']").val();
    var userid = $("input[id^='hdnUserID']").val();

    $.ajax({
        url: "../W/Default.aspx/CheckEmployees",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: "{'userid':'" + userid + "','instanceid':'" + instanceid + "','active':'" + active + "'}",
        async: true,
        complete: function (xhr, status) {
        },
        success: function (data) {
            console.log(data.d);
            $("#formerusers_count").html(data.d);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}


// checks if one day has passed. return "true" is so
function hasOneDayPassed() {
    var date = new Date().toLocaleDateString();

    if (localStorage.yourapp_date == date)
        return false;

    localStorage.yourapp_date = date;
    return true;
}


function checkUserStandartTime(timezone) {

    if (!hasOneDayPassed() && (document.cookie.indexOf("IgnoreDST=") >= 0))
        return false;
    else {
        jQuery.ajax({
            url: "Add_worker.aspx/CurrentTime",
            type: "POST",
            data: "{'userid':'" + $("[id$=hdnUserID]").val() + "','timezone':'" + $('[id$=hdntimezone]').val() + "','dlsaving':'" + $('[id$=hdndlsaving]').val() + "','dlsavinghour':'" + $('[id$=hdndlsavinghour]').val() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (response) {
                //var _zoneStandardTime = moment.tz(timezone).format('h:mm A');
                var _zoneStandardTime = zoneStandardTime(timezone);
                //console.log("Timezone: " + $('[id$=hdntimezone]').val() + ",  DST Enabled: " + $('[id$=hdndlsaving]').val() + ",  DST Difference: " + $('[id$=hdndlsavinghour]').val());
                //console.log("User Time: " + response.d);
                //console.log(timezone + " Time: " + _zoneStandardTime);

                var useravaimatime = "01/01/2001 " + response.d;
                var standardtime = "01/01/2001 " + _zoneStandardTime;
                
                //Convert milliseconds to minutes
                //var time_difference = (moment.duration(moment(standardtime).diff(moment(useravaimatime)))._milliseconds) / 60000;
                var time_difference = datetime_difference(standardtime, useravaimatime);
                localStorage.dstDifference = time_difference;
                // console.log("Difference in minutes:" + time_difference);

                var hd_timetick1 = new Date(useravaimatime);
                var hd_timetick2 = new Date(standardtime);

                //5 minutes (300000 milliseconds) difference is ignorable 
                if (time_difference > 5) {
                    $(".isa_warning").removeClass("hide");
                    $(".isa_warningText").html("<span class='hd_timetick1 hide'>" + hd_timetick1 + "</span>Your Profile Time:<span class='timetick1'>" + response.d + "</span> (current settings)<br/> <span class='hd_timetick2 hide'>" + hd_timetick2 + "</span>Correct Time: <span class='timetick2'>" + _zoneStandardTime + "</span>");
                    startTimer();
                }
                else {
                    clearInterval(myTimer);
                }
            },
            error: function (response) {
                console.log("error: " + response.d);
            }
        });
    }
}

function updateTimeZoneSettings_FirstTime(timezone) {
    jQuery.ajax({
        url: "Add_worker.aspx/CurrentTime",
        type: "POST",
        data: "{'userid':'" + $("[id$=hdnUserID]").val() + "','timezone':'" + $('[id$=hdntimezone]').val() + "','dlsaving':'" + $('[id$=hdndlsaving]').val() + "','dlsavinghour':'" + $('[id$=hdndlsavinghour]').val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (response) {
            var _zoneStandardTime = zoneStandardTime(timezone);
            var useravaimatime = "01/01/2001 " + response.d;
            var standardtime = "01/01/2001 " + _zoneStandardTime;

            //Convert milliseconds to minutes
            var time_difference = datetime_difference(standardtime, useravaimatime);

            //5 minutes (300000 milliseconds) difference is ignorable 
            if (time_difference > 5) {
                console.log("Timezone: " + $('[id$=hdntimezone]').val() + ",  DST Enabled: " + $('[id$=hdndlsaving]').val() + ",  DST Difference: " + $('[id$=hdndlsavinghour]').val());
                console.log("User Time: " + response.d);
                console.log(timezone + " Time: " + _zoneStandardTime);
                console.log("Difference in minutes:" + time_difference);

                var dlsaving = 0;

                if (dst_difference > 0)
                    dlsaving = 1;
                else if (dst_difference < 0)
                    dlsaving = 2;
                else
                    dlsaving = 0;

                dst_difference = Math.abs(dst_difference);

                //(string UserId, string Timezone, int dlsaving, int dlsavinghour)
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/SettingsUpdate",
                    data: "{'UserId':'" + $('[id$=hdnAdminID]').val() + "','Timezone':" + $('[id$=hdnLastInID]').val() + ",'dlsaving':'" + dlsaving + "','dlsavinghour':" + dst_difference + ",'sendEmail':'false'}",
                    dataType: "json",
                    beforeSend: function (e) {
                        ActivateAlertDiv('', 'AlertDiv');
                    },
                    contentType: "application/json; charset=utf-8",
                    success: function (result) {
                        console.log("success");
                    },
                    error: function (e) {
                        console.log("error");
                    }
                });
            }
        },
        error: function (response) {
            console.log("error: " + response.d);
        }
    });
}

function datetime_difference(date1, date2) {
    var time_difference = (moment.duration(moment(date1).diff(moment(date2)))._milliseconds) / 60000;
    localStorage.setItem("temp_DSTdifference", time_difference);
    return time_difference

}

function zoneStandardTime(timezone) {
    var _zoneStandardTime = moment.tz(timezone).format('h:mm A');
    localStorage.setItem("temp_timezone", _zoneStandardTime);
    return _zoneStandardTime
}