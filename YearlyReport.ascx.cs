﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_YearlyReport : System.Web.UI.UserControl
{
    private bool IsSimpleClock = true;
    List<RecordModel> records = new List<RecordModel>();
    private int UserId;
    public String InstanceID { get; set; }
    DateTime signInDate;

    protected void Page_Load(object sender, EventArgs e)
    {
        signInDate = SP.GetWorkerFirstSignInn(UserId.ToString());
        if (!IsPostBack)
        {
            GenerateDate();
        }
        if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out UserId))
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("select * from attendence_management a where a.Id = @id and a.InstanceId = @insId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", UserId);
                    cmd.Parameters.AddWithValue("@insId", this.InstanceID);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];
                        lblUserName.Text = dr["Title"].ToString();
                    }
                    else
                    {
                        Response.Redirect("Default.aspx?instanceid=" + this.InstanceID);
                    }
                }

            }
        }
        else
        {
            Response.Redirect("Default.aspx?instanceid=" + this.InstanceID);
        }

        AddinstanceWS objinst = new AddinstanceWS();
        OwnerId = objinst.GetUserID(HttpContext.Current.User.Identity.Name);
        //OwnerId = "e8a92994-352a-4319-838d-882e8dc1bdeb";

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("getuserrole", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", OwnerId.Trim());
                cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    //AdminFirstSignIn = SP.GetWorkerFirstSignInn(dt.Rows[0][1].ToString()).Date;
                    if (Convert.ToString(dt.Rows[0]["role"]) == "worker" || Convert.ToString(dt.Rows[0]["role"]) == "admin")
                    {
                        if (Convert.ToString(dt.Rows[0]["role"]) == "admin")
                        {
                            isAdmin = true;
                            isWorker = false;
                            trSelUser.Visible = true;
                            appAssignedId = Convert.ToInt32(dt.Rows[0]["userid"]);
                            hfparentid.Value = SP.GetAdminCatID(Convert.ToInt32(dt.Rows[0]["userid"].ToString())).ToString();
                        }
                        else
                        {
                            isWorker = true;
                            //Response.Redirect("Add_worker.aspx?id=" + dt.Rows[0]["userid"]);
                        }
                    }
                    else
                    {
                        isWorker = false;
                        appAssignedId = Convert.ToInt32(dt.Rows[0]["userid"]);
                    }
                }
            }
            if (!IsPostBack)
            {
                if (ddlYear.Items.Count > 0)
                {
                    ddlYear.SelectedIndex = 1;
                }
                if (!isWorker)
                {
                    FillUsers();
                    //ddlUsers.SelectedIndex = Users.FindIndex(u => u.userID == UserId);
                    trSelUser.Visible = true;
                }
                else
                {
                    ddlUsers.Items.Add(new ListItem("User", UserId.ToString()));
                    ddlUsers.SelectedIndex = 1;
                    trSelUser.Visible = false;
                    FillandCreateHistory(ddlUsers.SelectedValue, con);
                    CreateYearlyReport(ddlUsers.SelectedValue, con);
                }
            }
        }
    }

    private void HoursCalculation(int totHrs, int totMins, int wtotHrs, int wtotMins, int btotHrs, int btotMins)
    {
        #region Estimated total hours calculations
        List<Absence> absences = Absence.GetAbsences(Convert.ToInt32(ddlUsers.SelectedValue)).Where(u => u.Active == true).ToList();
        Int32 totHours = 0;
        Int32 totWorkHours = 0;
        Boolean weekend = false;
        DateTime startDateTime = SP.GetWorkerFirstSignInThisYear(ddlUsers.SelectedValue, Convert.ToInt32(ddlYear.SelectedValue));
        DateTime endDateTime = SP.GetWorkerLastSignInThisYear(ddlUsers.SelectedValue.ToString(), Convert.ToInt32(ddlYear.SelectedValue));
        lblTest.Text = startDateTime.ToShortDateString() + " - " + endDateTime.ToShortDateString();
        for (DateTime i = startDateTime; i <= endDateTime; i = i.AddDays(1))
        {
            DateTime currDateTime = i.Date;
            if (absences.Where(u => u.CrtDate == currDateTime.Date).ToList().Count < 1)
            {
                Int32 userWorkHours = SP.GetTodayUserWorkHours(this.InstanceID, Convert.ToInt32(ddlUsers.SelectedValue), currDateTime);
                Int32 userTotHours = SP.GetTodayUserTotHours(this.InstanceID, Convert.ToInt32(ddlUsers.SelectedValue), currDateTime);
                weekend = false;

                if (userTotHours < 0)
                {
                    lblTest.Text += "<0";
                    totWorkHours += (-(userWorkHours) + 12 - (SP.GetUserBreakTimeDiff(Convert.ToInt32(ddlUsers.SelectedValue), this.InstanceID, currDateTime) * 2));
                    totHours += (-(userTotHours) + 12);
                }
                else
                {
                    lblTest.Text += "<br> " + currDateTime.ToString("MMM dd yyyy") + "  " + totWorkHours.ToString();
                    totWorkHours += userWorkHours;
                    totHours += userTotHours;
                }

            }
        }
        #endregion
        lblEstWHrs.Text = totWorkHours.ToString() + " hrs (" + lblTotDays.Text + ")";
        //lblEstWHrs.Text = totWorkHours + " hrs";
        //lblTotalHours.Text = totHrs + " hrs " + totMins + " mins should be " + totHours + " hrs";
        lblWHours.Text = wtotHrs + " hrs " + wtotMins + " mins";
        lblTotBreakHrs.Text = btotHrs + " hrs " + btotMins + " mins";

        string[] wHourMin = lblWHours.Text.Split(' ');
        try
        {
            int exHr = 0;
            int exMin = 0;
            if (wHourMin.Count() > 3)
            {
                exHr = Convert.ToInt32(wHourMin[0]);
                exMin = Convert.ToInt32(wHourMin[2]);
            }
            else
            {
                exMin = Convert.ToInt32(wHourMin[0]);
            }
            exHr = exHr - Convert.ToInt32(lblEstWHrs.Text.Split(' ')[0]);
            if (exHr > 0)
            {
                lblExtraHours.Text = exHr + " hrs " + exMin + " min";
            }
            else
            {
                lblExtraHours.Text = "0 hrs";
            }

        }
        catch (Exception)
        { }
    }

    private void FillUsers()
    {
        PopulateUsersAccord(0, isAdmin);
        hfparentid.Value = "0";
    }


    protected void PopulateUsersAccord(int pid, bool isAdmin = false, bool ShowFiltered = false, int status = 1)
    {
        DataTable dtstudentall = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            string q = "getallattendencerecordbyid";
            if (ShowFiltered)
            {
                if (status == 1)
                    q = "getallactiveusers";
                else
                {
                    q = "getallinactiveusers";
                }
            }
            else
            {
                if (isAdmin)
                {
                    q = "getfilteredattendancerecordbyid";
                }
            }

            using (SqlCommand cmd = new SqlCommand(q, con))
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", pid);
                cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                if (isAdmin)
                {
                    if (q == "getfilteredattendancerecordbyid")
                    {
                        cmd.Parameters.AddWithValue("@userid", appAssignedId);
                    }
                }
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dtstudentall);
                cmd.CommandText = "getclocktype";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@instanceId", this.InstanceID);
                bool i = (bool)cmd.ExecuteScalar();
                if (i)
                {
                    IsSimpleClock = false;
                }
                Users = new List<User>();
                if (dtstudentall.Rows.Count > 0)
                {
                    foreach (DataRow row in dtstudentall.Rows)
                    {
                        if (Convert.ToBoolean(row["category"].ToString()) == false)
                        {
                            Users.Add(new User() { userID = Convert.ToInt32(row[0].ToString()), userName = row[1].ToString() });
                        }
                    }
                }
                ddlUsers.DataSource = Users;
                ddlUsers.DataTextField = "userName";
                ddlUsers.DataValueField = "userID";
                ddlUsers.DataBind();
            }
        }
    }

    private void GenerateDate()
    {
        ddlYear.Items.Clear();
        DateTime startDate = new DateTime();
        DateTime endDate = new DateTime();
        if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out UserId))
        {
            startDate = SP.GetWorkerFirstSignInn(UserId.ToString());
            endDate = SP.GetWorkerLastSignIn(UserId.ToString());
        }
        DateTime tempDt = new DateTime(DateTime.Now.Year, 1, 1);
        String strDate = "";
        ddlYear.Items.Add(new ListItem("Select Year", "0"));
        ddlYear.Items.Add(new ListItem(endDate.ToString("yyyy"), endDate.ToString("yyyy")));
        int diff = endDate.Year - startDate.Year;
        for (int i = 0; i < diff; i++)
        {
            strDate = startDate.AddYears(-i).ToString("yyyy");
            ddlYear.Items.Add(new ListItem(strDate, strDate));
        }
    }

    private void FillandCreateHistory(string userid, SqlConnection con)
    {
        if (Convert.ToInt32(ddlYear.SelectedValue) > 0)
        {
            using (SqlCommand cmd = new SqlCommand("GetYearlyReport", con))
            {
                DataTable dt = new DataTable();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Year", ddlYear.SelectedValue);
                cmd.Parameters.AddWithValue("@Id", userid);
                cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);

                List<Absence> _absences = Absence.GetAbsences(Convert.ToInt32(UserId)).Where(u => u.Active == true && u.CrtDate.Year == Convert.ToInt32(ddlYear.SelectedValue)).ToList();
                //lblTotalHour.Text = dt.AsEnumerable().Sum(u => u.Field<int>(1)).ToString() + " days";
                //lblTotalAbsence.Text = _absences.Count().ToString() + " days";
                //lblTotWeekend.Text = dt.AsEnumerable().Sum(u => u.Field<int>(3)).ToString() + " days";
            }
            hdnData.Value = "true";
        }
        else
        {
            hdnData.Value = "false";
        }
    }
    private static void CalculateTime(ref int bTotShouldHrs, ref int bTotShouldMins, string[] workedhours)
    {
        if (workedhours.Count() > 3)
        {
            bTotShouldHrs += Convert.ToInt32(workedhours[0]);
            bTotShouldMins += Convert.ToInt32(workedhours[2]);
            if (bTotShouldMins >= 60)
            {
                bTotShouldHrs += Convert.ToInt32((bTotShouldMins / 60));
                bTotShouldMins -= 60;
            }
        }
        else
        {
            if (Convert.ToInt32(workedhours[0]) > 0)
            {
                bTotShouldMins += Convert.ToInt32(workedhours[0]);
                if (bTotShouldMins >= 60)
                {
                    bTotShouldHrs += Convert.ToInt32((bTotShouldMins / 60));
                    bTotShouldMins -= 60;
                }
            }
        }
    }
    private void CreateYearlyReport(string userid, SqlConnection con)
    {
        hdnData.Value = "true";
        DateTime startDate = SP.GetWorkerFirstSignInThisYear(userid, Convert.ToInt32(ddlYear.SelectedValue));
        DateTime endDate = SP.GetWorkerLastSignInThisYear(userid, Convert.ToInt32(ddlYear.SelectedValue));
        lblStartDate.Text = startDate.ToString("ddd, MMM dd yyyy");
        lblEndDate.Text = endDate.ToString("ddd, MMM dd yyyy");
        int days = 0;
        int totalAbs = 0;
        int weekendHours = 0;
        int homeHours = 0;
        int officeHours = 0;
        int totHrs = 0;
        int totMins = 0;
        int wtotHrs = 0;
        int wtotMins = 0;
        int btotHrs = 0;
        int btotMins = 0;
        int totDays = 0;
        int exDays = 0;
        int totExtDays = 0;
        int totExtWeekendsHrs = 0;
        int totExtWeekendsMins = 0;
        List<Absence> absences = Absence.GetAbsences(Convert.ToInt32(userid)).Where(u => u.Active == true).ToList();

        if (startDate.Year <= Convert.ToInt32(ddlYear.SelectedValue) && endDate.Year >= Convert.ToInt32(ddlYear.SelectedValue))
        {
            for (int i = startDate.Month; i <= endDate.Month; i++)
            {
                DateTime qstartDate = new DateTime(Convert.ToInt32(ddlYear.SelectedValue), i, 1);
                DateTime qendDate = qstartDate.AddMonths(1).AddDays(-1);
                using (SqlCommand cmd = new SqlCommand("getworkerrecordbydate", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@sDate", qstartDate.Date);
                    cmd.Parameters.AddWithValue("@eDate", qendDate.Date);
                    cmd.Parameters.AddWithValue("@Id", userid);
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    DataSet ds = new DataSet();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                    records = UtilityMethods.getRecords(ds);
                    tblHistory.Rows.Clear();
                    foreach (RecordModel record in records)
                    {
                        // Extra days, total days and weekend hours
                        if (record.Records.Count > 0)
                        {

                            if (record.DayTitle != null)
                            {
                                totExtDays++;
                                string[] weekendWorkHours;
                                // Weekend Worked hours
                                weekendWorkHours = record.TotalWorkedHours.Split(' ');
                                CalculateTime(ref totExtWeekendsHrs, ref totExtWeekendsMins, weekendWorkHours);
                            }
                            else
                            {
                                totDays++;
                            }
                        }

                        officeHours = record.Records.Where(x => x.InLocation == "Office").Sum(x => x.RowTime);

                        homeHours = record.Records.Where(x => x.InLocation == "Home").Sum(x => x.RowTime);

                        string[] workedhours = record.TotalTime.Split(' ');
                        if (workedhours.Count() > 3)
                        {
                            totHrs += Convert.ToInt32(workedhours[0]);
                            totMins += Convert.ToInt32(workedhours[2]);
                            if (totMins >= 60)
                            {
                                totHrs += Convert.ToInt32((totMins / 60));
                                totMins -= 60;
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(workedhours[0]) < 0)
                            {
                                totMins += Convert.ToInt32(workedhours[0]);
                                if (totMins >= 60)
                                {
                                    totHrs += Convert.ToInt32((totMins / 60));
                                    totMins -= 60;
                                }
                            }
                        }

                        // Total Break hours
                        if (record.Records.Count > 0)
                        {
                            workedhours = record.TotalBreak.Split(' ');
                            if (workedhours.Count() > 3)
                            {
                                btotHrs += Convert.ToInt32(workedhours[0]);
                                btotMins += Convert.ToInt32(workedhours[2]);
                                if (btotMins >= 60)
                                {
                                    btotHrs += Convert.ToInt32((btotMins / 60));
                                    btotMins -= 60;
                                }
                            }
                            else
                            {
                                if (Convert.ToInt32(workedhours[0]) < 0)
                                {
                                    btotMins += Convert.ToInt32(workedhours[0]);
                                    if (btotMins >= 60)
                                    {
                                        btotHrs += Convert.ToInt32((btotMins / 60));
                                        btotMins -= 60;
                                    }
                                }
                            }
                        }


                        // Worked hours without breaks
                        workedhours = record.TotalWorkedHours.Split(' ');
                        if (workedhours.Count() > 3)
                        {
                            wtotHrs += Convert.ToInt32(workedhours[0]);
                            wtotMins += Convert.ToInt32(workedhours[2]);
                            if (wtotMins >= 60)
                            {
                                wtotHrs += Convert.ToInt32((wtotMins / 60));
                                wtotMins -= 60;
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(workedhours[0]) < 0)
                            {
                                wtotMins += Convert.ToInt32(workedhours[0]);
                                if (wtotMins >= 60)
                                {
                                    wtotHrs += Convert.ToInt32((wtotMins / 60));
                                    wtotMins -= 60;
                                }
                            }
                        }
                    }
                }
                totalAbs += absences.Where(u => u.CrtDate.Date.Month == i).ToList().Count;
            }
        }

        lblExtraDays.Text = exDays + " days";
        lblTotDays.Text = totDays + " days";
        HoursCalculation(totHrs, totMins, wtotHrs, wtotMins, btotHrs, btotMins);
        lblTotalAbsences.Text = totalAbs.ToString();
        //lblTotalHours.Text = totHrs + " hrs " + totMins + " mins";
        //lblWHours.Text = wtotHrs + " hrs " + wtotMins + " mins";
        //lblTotBreakHrs.Text = btotHrs + " hrs " + btotMins + " mins";
    }

    protected void btnGR_Click(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            //FillandCreateHistory(UserId.ToString(), con);
            CreateYearlyReport(ddlUsers.SelectedValue, con);
        }
    }
    protected void bk_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out UserId))
        {
            Response.Redirect("Reports.aspx?id=" + UserId + "&instanceid=" + this.InstanceID);
        }
    }

    public string OwnerId { get; set; }

    public bool isAdmin { get; set; }

    public bool isWorker { get; set; }

    public int appAssignedId { get; set; }

    public List<User> Users { get; set; }
}