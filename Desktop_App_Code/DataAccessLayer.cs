﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for DataAccessLayer
/// </summary>
public class DataAccessLayer
{
    SqlConnection con;
    public DataAccessLayer()
    {
        //
        // TODO: Add constructor logic here
        //
        con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString);
    }
    private DataTable GetDataTable(string spName, Hashtable hts)
    {
        DataTable dt = new DataTable();
        SqlCommand cmd = new SqlCommand(spName, con);
        con.Open();
        foreach (DictionaryEntry de in hts)
        {
            cmd.Parameters.Add(new SqlParameter(de.Key.ToString(), de.Value.ToString()));
        }
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        return dt;
    }
    private bool ExecuteNonQuery(string spName, Hashtable hts)
    {
        SqlCommand cmd = new SqlCommand(spName, con);
        con.Open();
        foreach (DictionaryEntry de in hts)
        {
            cmd.Parameters.Add(new SqlParameter(de.Key.ToString(), de.Value.ToString()));
        }
        cmd.CommandType = CommandType.StoredProcedure;
        if (cmd.ExecuteNonQuery() > 0)
            return true;
        else
            return false;
    }
}