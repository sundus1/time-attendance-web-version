﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AvaimaThirdpartyTool;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Text;
using MyAttSys;
using System.Text;
using System.Security.Cryptography;
using System.Web.Security;

public partial class Welcome : AvaimaWebPage
{
    Attendance atd = new Attendance();
    private string Email = "";
    private string Password = "";

    protected void Page_Load(object sender, EventArgs e)
    {

        hdnUSERR.Value = HttpContext.Current.User.Identity.Name;
        //hdnUSERR.Value = "9d23a470-13f0-41da-b137-c2bf4fa86030";    //test 
        //hdnUSERR.Value = "648dec41-d961-4a78-a615-d9aac688c3ad";  // On local execution
        //hdnUSERR.Value = "e8a92994-352a-4319-838d-882e8dc1bdeb";
         //hdnUSERR.Value = "b05a5dc0-aa96-4495-bafc-bd8e79063b86";

        if (Request.QueryString["instanceid"].ToString() != "")
            hdnInstanceID.Value = Request.QueryString["instanceid"].ToString();
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "", "document.getElementById('hdnInstanceID').value = GetParameterValues('iid')", true);

        string Link = "";

        if (!IsPostBack)
        {
            /////////////////
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("getuserrole", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@userid", hdnUSERR.Value.Trim());
                    cmd.Parameters.AddWithValue("@instanceid", hdnInstanceID.Value);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count == 0)
                    {
                        AvaimaUserProfile objwebser = new AvaimaUserProfile();
                        //string[] user = objwebser.Getonlineuserinfo(this.Context.User.Identity.Name).Split('#');
                        string[] user = objwebser.Getonlineuserinfo(hdnUSERR.Value).Split('#');
                        using (SqlCommand cmd1 = new SqlCommand("insertworker", con))
                        {
                            string title = user[0] + " " + user[1];
                            cmd1.Parameters.AddWithValue("@title", title);
                            cmd1.Parameters.AddWithValue("@parentid", 0);
                            cmd1.Parameters.AddWithValue("@InstanceId", hdnInstanceID.Value);
                            cmd1.Parameters.AddWithValue("@datefield", Convert.ToDateTime(DateTime.Now));
                            cmd1.Parameters.AddWithValue("@category", 0);
                            cmd1.Parameters.AddWithValue("@email", user[2]);
                            cmd1.Parameters.AddWithValue("@active", true);

                            cmd1.CommandType = CommandType.StoredProcedure;
                            string id = cmd1.ExecuteScalar().ToString();
                            atd.Insertuserrole(hdnUSERR.Value, "superuser", user[2], hdnInstanceID.Value, id);

                            try
                            {
                                objwebser.insertUserFull(hdnUSERR.Value, user[3], user[2], "", "", "", "", "", "", user[0], user[1], "", "", "", "", "", "", "", "", id, hdnInstanceID.Value, "");
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }

            }
            /////////////////
        }

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["aviamaConn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("Select * from cms_user where userid = @userid", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", hdnUSERR.Value);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    Email = atd.Decrypt(dt.Rows[0]["email"].ToString(), true);
                    Password = atd.Decrypt(dt.Rows[0]["password"].ToString(), true);
                }
            }
        }


        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("Select * from attendence_management  where email = @email and InstanceId =  @instanceid", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@email", Email);
                cmd.Parameters.AddWithValue("@instanceid", hdnInstanceID.Value);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    hdnAppUserID.Value = dt.Rows[0]["ID"].ToString();
                }
            }

            //office and foreign user
            //if (InstanceID == "f425f912-8d79-4415-ba72-89a5309acb10" || InstanceID == "363250bb-1293-4ddf-9fa7-482b1e1f13f8")
            //{
            //MainContent_ifapp.Visible = false;
            newlink.Visible = true;
            //}
            //else
            //{
            //    MainContent_ifapp.Visible = true;
            //    newlink.Visible = false;
            //}


            //MainContent_ifapp.Attributes.Add("src", "http://www.avaima.com/6e815b00-6e13-4839-a57d-800a92809f21/Default.aspx?f=" + FormsAuthentication.GetAuthCookie(User.Identity.Name, false).Value + "&s=" + Session.SessionID + "&instanceid=" + hdnInstanceID.Value);

            //MainContent_ifapp.Attributes.Add("onload", "resizeIframe(this)");
            //MainContent_ifapp.Attributes.Add("width", "100%");
            //MainContent_ifapp.Attributes.Add("frameborder", "0");
            //MainContent_ifapp.Attributes.Add("scrolling", "no");
            Link = "http://www.avaima.com/6e815b00-6e13-4839-a57d-800a92809f21/W/Add_worker.aspx?uid=" + hdnAppUserID.Value.ToString() + "&id=" + hdnAppUserID.Value.ToString() + "&instanceid=" + hdnInstanceID.Value + "&e=" + Email.Encrypt() + "&p=" + Password.Encrypt();
            link.HRef = Link;
        }

        //if (InstanceID == "f425f912-8d79-4415-ba72-89a5309acb10")
        ScriptManager.RegisterStartupScript(this, GetType(), "", "openLink('" + Link + "');", true);

    }




}