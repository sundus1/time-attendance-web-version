﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class MonthlyReport : AvaimaThirdpartyTool.AvaimaWebPage
{
    int id = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            if (DateTime.Now.Month > 1)
            {
                year.Items.Add(DateTime.Now.Year.ToString());
            }
            year.Items.Add((DateTime.Now.Year - 1).ToString());
        }
        if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out id))
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("select * from attendence_management a where a.Id = @id and a.InstanceId = @insId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@insId", this.InstanceID);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];
                        name.Text = dr["Title"].ToString();
                    }
                    else
                    {
                        this.Redirect("Default.aspx");
                    }
                }
            }
            if (!string.IsNullOrEmpty(Request.QueryString["my"]))
            {
                string[] y = Request.QueryString["my"].Split(":".ToCharArray());
                int m = Convert.ToInt32(y[0]);
                DateTime sDate = DateTime.ParseExact(y[1] + "-" + ((m < 10) ? "0" + m.ToString() : m.ToString()) + "-01", "yyyy-MM-dd", null);
                year.SelectedValue = y[1];
                month.SelectedValue = m.ToString();
                monthName.Text = sDate.ToString("MMMM yyyy");
                genReport(sDate);
            }
        }
        else
        {
            this.Redirect("Default.aspx");
        }
    }

    private void genReport(DateTime sDate)
    {
        using (
                    SqlConnection con =
                        new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString)
                    )
        {
            using (SqlCommand cmd = new SqlCommand("getworkerrecordbydate", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DateTime eDate = sDate.AddMonths(1).AddDays(-1);
                if (sDate.Month == DateTime.Now.Month && sDate.Year == DateTime.Now.Year)
                {
                    eDate = DateTime.Now;
                }
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@sDate", sDate);
                cmd.Parameters.AddWithValue("@eDate", eDate);
                cmd.Parameters.AddWithValue("@Id", id);
                cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                List<RecordModel> records = UtilityMethods.getRecords(ds);
                if (records.Count > 0)
                {
                    filtersContainer.Visible = true;
                    records.Reverse();
                    reportR.DataSource = records;
                    reportR.DataBind();
                    rpContainer.Visible = true;
                }
            }
        }
    }

    protected void btnGR_Click(object sender, EventArgs e)
    {
        int m = 0, y = 0;
        bool r = int.TryParse(month.SelectedValue, out m);
        bool yr = int.TryParse(year.SelectedValue, out y);
        if (r && yr && y > 0 && m > 0 && m <= 12)
        {
            DateTime sDate =DateTime.ParseExact(y.ToString() + "-" + ((m < 10) ? "0" + m.ToString() : m.ToString()) + "-01", "yyyy-MM-dd", null);
            //if (sDate.Month <= DateTime.Now.Month && sDate.Year <= DateTime.Now.Year)
            {
                monthName.Text = sDate.ToString("MMMM yyyy");
                genReport(sDate);
            }
            //else
            //{
            //    rpContainer.Visible = false;
            //}
        }
        else
        {
            
        }
    }

    private int absent = 0;
    private int totalWork = 0;
    protected void reportR_OnDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            RecordModel container = (RecordModel)e.Item.DataItem;
            if (container.Date.Date <= DateTime.Now.Date)
            {
                AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
                Label date = e.Item.FindControl("lblDate") as Label;
                Label loginTime = e.Item.FindControl("lblInTime") as Label;
                Label logoutTime = e.Item.FindControl("lblOutTime") as Label;
                Label TotalHours = e.Item.FindControl("lblTotal") as Label;
                Label dtitle = e.Item.FindControl("lblDayTitle") as Label;
                HtmlControl dateTitleTD = e.Item.FindControl("dateTitleTD") as HtmlControl;
                HtmlControl dateTD = e.Item.FindControl("dateTD") as HtmlControl;

                date.Text = objATZ.GetDate(container.Date.ToString(), HttpContext.Current.User.Identity.Name);
                dtitle.Text = container.DayTitle;
                totalWork++;
                if (date.Text.ToLower() != "today")
                {
                    date.Text =
                        DateTime.Parse(
                            objATZ.GetDate(container.Date.ToString(), HttpContext.Current.User.Identity.Name), null).
                            ToString("dd");
                }

                if (container.Records.Count > 0)
                {
                    if (
                        objATZ.GetDate(container.Records.Last().LoginTime, HttpContext.Current.User.Identity.Name).
                            ToLower() != "today")
                    {
                        loginTime.Text =
                            DateTime.Parse(
                                objATZ.GetDate(container.Records.Last().LoginTime,
                                               HttpContext.Current.User.Identity.Name),
                                null).ToString("dd-MM-yyyy") + " " +
                            objATZ.GetTime(container.Records.Last().LoginTime, HttpContext.Current.User.Identity.Name);
                    }
                    else
                    {
                        loginTime.Text =
                            objATZ.GetDate(container.Records.Last().LoginTime, HttpContext.Current.User.Identity.Name) +
                            " " +
                            objATZ.GetTime(container.Records.Last().LoginTime, HttpContext.Current.User.Identity.Name);
                    }
                    if (!string.IsNullOrEmpty(container.Records.First().LogoutTime))
                    {
                        if (objATZ.GetDate(container.Records.First().LogoutTime, HttpContext.Current.User.Identity.Name).ToLower() != "today")
                        {
                            logoutTime.Text =
                                DateTime.Parse(
                                    objATZ.GetDate(container.Records.First().LogoutTime,
                                                   HttpContext.Current.User.Identity.Name), null).ToString("dd-MM-yyyy") +
                                " " +
                                objATZ.GetTime(container.Records.First().LogoutTime, HttpContext.Current.User.Identity.Name);
                        }
                        else
                        {
                            logoutTime.Text =
                            objATZ.GetDate(container.Records.First().LogoutTime, HttpContext.Current.User.Identity.Name) +
                            " " +
                            objATZ.GetTime(container.Records.First().LogoutTime, HttpContext.Current.User.Identity.Name);
                        }
                    }
                    else
                    {
                        logoutTime.Text = "-";
                    }
                    TotalHours.Text = container.TotalTime;
                    if (container.DayType.HasValue)
                    {
                        totalWork--;
                        if (string.IsNullOrEmpty(container.DayTitle))
                        {
                            dtitle.Text = "Holiday";
                        }
                        else
                        {
                            dtitle.Text = container.DayTitle;
                        }
                        if (container.DayType.Value == 0)
                        {
                            dateTD.Attributes.Add("class", "SatSunHolidays");
                            dateTitleTD.Attributes.Add("class", "SatSunHolidays");
                        }
                        else
                        {
                            dateTD.Attributes.Add("class", "NormalHoliday");
                            dateTitleTD.Attributes.Add("class", "NormalHoliday");
                        }
                    }
                }
                else
                {
                    if (container.DayType.HasValue)
                    {
                        totalWork--;
                        if (string.IsNullOrEmpty(container.DayTitle))
                        {
                            dtitle.Text = "Holiday";
                        }
                        else
                        {
                            dtitle.Text = container.DayTitle;
                        }
                        if (container.DayType.Value == 0)
                        {
                            dateTD.Attributes.Add("class", "SatSunHolidays");
                            dateTitleTD.Attributes.Add("class", "SatSunHolidays");
                        }
                        else
                        {
                            dateTD.Attributes.Add("class", "NormalHoliday");
                            dateTitleTD.Attributes.Add("class", "NormalHoliday");
                        }
                    }
                    else
                    {

                        dateTitleTD.Attributes.Add("class", "absent");
                        dtitle.Text = "Absent";
                        dtitle.ForeColor = System.Drawing.Color.White;
                        dateTD.Style.Add("background-color", "red");
                        dateTD.Style.Add("color", "#FFFFFF");
                        absent++;
                    }
                }
                dateTitleTD.Attributes.Add("iTime", loginTime.Text.Replace("Today", DateTime.Now.ToString("dd-MM-yyyy")));
                dateTitleTD.Attributes.Add("oTime", logoutTime.Text.Replace("Today", DateTime.Now.ToString("dd-MM-yyyy")));
                dateTitleTD.Attributes.Add("wTime", TotalHours.Text.Replace(" hour(s) ", "-").Replace(" minute(s)", ""));
            }
        }

        if(e.Item.ItemType == ListItemType.Footer)
        {
            Label lblTWD = e.Item.FindControl("lblTWD") as Label;
            Label lblTA = e.Item.FindControl("lblTA") as Label;
            lblTWD.Text = totalWork.ToString();
            lblTA.Text = absent.ToString();
        }
    }

    protected void bk_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out id))
        {
            this.Redirect("Add_worker.aspx?id=" + id);
        }
    }
}