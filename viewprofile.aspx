﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="viewprofile.aspx.cs" Inherits="viewprofile" %>

<%@ Register Src="~/WebApplication1/userprofile.ascx" TagName="contractuserprofile" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" media="screen" href="_assets/style.css" />
    <link rel="stylesheet" href="_assets/jquery-ui.css" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
       <div class="space">
            <asp:ScriptManager ID="scmang" runat="server"></asp:ScriptManager>
        
        <div class="formTitleDiv">
            Woker Profile
        </div>
        <div>
            <uc1:contractuserprofile ID="contractuserprofile" runat="server" />
        </div>
       </div>
    </form>
</body>
</html>
