﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class viewprofile : AvaimaThirdpartyTool.AvaimaWebPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        contractuserprofile.wid = Request["id"];
    }
    protected void lnkhome_Click(object sender, EventArgs e)
    {
        this.Redirect("Default.aspx");
    }

    protected void LnkworkerClick(object sender, EventArgs e)
    {
        this.Redirect("worker.aspx?id="+Request["id"]);
    }
}