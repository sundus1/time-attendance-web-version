﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Globalization;

public partial class MonthlyReportNew : System.Web.UI.Page
{
    int userId = 0;
    string instanceID = "";
    public int appAssignedId { get; set; }
    private bool isAdmin = false;
    private bool IsSimpleClock = true;
    public string OwnerId { get; set; }
    public bool isWorker { get; set; }

    //public string OwnerId { get; set; }

    public List<User> Users { get; set; }
    List<AttUser> users = AttUser.GetAll();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
            userId = Convert.ToInt16(Request.QueryString["id"]);
        }
        if (Request.QueryString["instanceid"] != null)
        {
            instanceID = Request.QueryString["instanceid"];
        }
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            //WorkerLastSignIn = SP.GetWorkerLastSignIn(UserId.ToString()).Date.Date;
            //WorkerFirstSignIn = SP.GetWorkerFirstSignInn(UserId.ToString()).Date.Date;
            //hdnerid.Value = UserId.ToString();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("select * from attendence_management a where a.Id = @id and a.InstanceId = @insId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", userId);
                    cmd.Parameters.AddWithValue("@insId", this.instanceID);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];
                        lblUserName.Text = dr["Title"].ToString();
                    }
                    else
                    {
                        Response.Redirect("Default.aspx?instanceid=" + this.instanceID);
                    }
                }

            }
        }
        else
        {
            Response.Redirect("Default.aspx?instanceid=" + this.instanceID);
        }
        
        if (!IsPostBack)
        {
            BindMonths();
            BindYears(); BindUsers();
        }
    }
    private void FetchUsers(Int32 parentID)
    {
        List<AttUser> filteredUsers = users.FindAll(u => u.ParentID == parentID);
        foreach (AttUser user in filteredUsers)
        {
            if (user.Category == false)
            {
                Users.Add(new User() { userID = user.UserID, userName = user.UserName });
            }
            else
            {
                FetchUsers(user.ParentID);
            }
        }
    }
    private void FillUsers()
    {
        PopulateUsersAccord(0, isAdmin);
        hfparentid.Value = "0";
    }
    protected void PopulateUsersAccord(int pid, bool isAdmin = false, bool ShowFiltered = false, int status = 1)
    {
        DataTable dtstudentall = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            string q = "getallattendencerecordbyid";
            if (ShowFiltered)
            {
                if (status == 1)
                    q = "getallactiveusers";
                else
                {
                    q = "getallinactiveusers";
                }
            }
            else
            {
                if (isAdmin)
                {
                    q = "getfilteredattendancerecordbyid";
                }
            }

            using (SqlCommand cmd = new SqlCommand(q, con))
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", pid);
                cmd.Parameters.AddWithValue("@InstanceId", this.instanceID);
                if (isAdmin)
                {
                    if (q == "getfilteredattendancerecordbyid")
                    {
                        cmd.Parameters.AddWithValue("@userid", appAssignedId);
                    }
                }
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dtstudentall);
                cmd.CommandText = "getclocktype";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@instanceId", this.instanceID);
                bool i = (bool)cmd.ExecuteScalar();
                if (i)
                {
                    IsSimpleClock = false;
                }
                Users = new List<User>();
                if (dtstudentall.Rows.Count > 0)
                {
                    foreach (DataRow row in dtstudentall.Rows)
                    {
                        if (Convert.ToBoolean(row["category"].ToString()) == false)
                        {
                            Users.Add(new User() { userID = Convert.ToInt32(row[0].ToString()), userName = row[1].ToString() });
                        }
                        else
                        {
                            FetchUsers(Convert.ToInt32(row[0].ToString()));
                        }
                    }
                }
                ddlUsers.DataSource = Users;
                ddlUsers.DataTextField = "userName";
                ddlUsers.DataValueField = "userID";
                ddlUsers.DataBind();
            }
        }
    }
    private void BindUsers()
    {
        AddinstanceWS objinst = new AddinstanceWS();
        OwnerId = objinst.GetUserID(HttpContext.Current.User.Identity.Name);
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("getuserrole", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", OwnerId.ToString());
                cmd.Parameters.AddWithValue("@instanceid", instanceID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    if (Convert.ToString(dt.Rows[0]["role"]) == "admin")
                    {
                        isAdmin = true;
                        isWorker = false;
                    }
                    else
                    {
                        isAdmin = false;
                        isWorker = true;
                    }
                    appAssignedId = Convert.ToInt32(dt.Rows[0]["userid"]);
                }

            }
        }
        if (!isWorker)
        {
            FillUsers();
            ddlUsers.SelectedIndex = Users.FindIndex(u => u.userID == userId);
            lblUserName.Text = Users.Where(u => u.userID == userId).SingleOrDefault().userName;
            trSelUser.Visible = true;
        }
        else
        {
            AttUser user = AttUser.GetUserByID(userId);
            if (user == null)
            {
                ddlUsers.Items.Add(new ListItem("User", userId.ToString()));
            }
            else
            {
                ddlUsers.Items.Add(new ListItem(user.UserName, userId.ToString()));
            }
            ddlUsers.SelectedIndex = 1;
            trSelUser.Style.Add("display", "none");
        }
    }
    private void BindYears()
    {
        ddlYear.Items.Clear();
        DateTime startDate = new DateTime();
        DateTime endDate = new DateTime();
        if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out userId))
        {
            startDate = SP.GetWorkerFirstSignInn(userId.ToString());
            endDate = SP.GetWorkerLastSignIn(userId.ToString());
        }
        for (int i = endDate.Year; i >= startDate.Year; i--)
        {
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }
    private void BindMonths()
    {
        ddlMonth.Items.Clear();
        for (int i = 0; i < 12; i++)
        {
            ddlMonth.Items.Add(CultureInfo.CurrentCulture.DateTimeFormat.MonthNames.GetValue(i).ToString());
        }
        //ddlMonth.Items.FindByValue(System.DateTime.Now.Month.ToString()).Selected = true; 
    }
    private DataTable GetMonthlyHolidays(DateTime start, DateTime end)
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_GetMonthlyHolidays", con))
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("@instanceId", instanceID);
                cmd.Parameters.AddWithValue("@startDate", start);
                cmd.Parameters.AddWithValue("@endDate", end);
                adp.Fill(dt);

            }
        }
        return dt;
    }
    public DataTable GetMonthlyAbsences(Int32 UserID, DateTime start, DateTime end)
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_GetMonthlyAbsences", con))
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("@userID", UserID);
                cmd.Parameters.AddWithValue("@startDate", start);
                cmd.Parameters.AddWithValue("@endDate", end);
                adp.Fill(dt);

            }
        }
        return dt;
    }
    public DataTable GetMonthlyAttendance(Int32 UserID, DateTime start, DateTime end)
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_GetMonthlyHistory", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@UserId", UserID);
                cmd.Parameters.AddWithValue("@startDate", start);
                cmd.Parameters.AddWithValue("@endDate", end);
                adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }
        }
        return dt;
    }
    private string ConvertTimeSpan(TimeSpan WorkedHours)
    {
        string strWorkingHours = String.Empty;
        if (WorkedHours.Days > 0)
        {
            strWorkingHours = WorkedHours.Days + " Days";
        }
        if (WorkedHours.Hours > 0)
        {
            strWorkingHours += " " + WorkedHours.Hours + " Hours";
        }
        if (WorkedHours.Minutes > 0)
        {
            strWorkingHours += " " + WorkedHours.Minutes + " Minutes";
        }
        //if (WorkedHours.Seconds > 0)
        //{
        //    strWorkingHours += " " + WorkedHours.Seconds + " Seconds";
        //}
        return strWorkingHours;
    }
    private void CreateTable(DataTable dtAttendance, DataTable dtHolidays, DataTable dtAbsences, List<DateTime> lst)
    {
        DataColumn newColumn = new DataColumn("ManualRecord", typeof(System.String));
        newColumn.DefaultValue = "0";
        dtAttendance.Columns.Add(newColumn);
        AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
        tblHistory.Rows.Clear();
        tblHistory.CssClass = "smallGrid";
        TableRow trow = new TableRow();
        trow.CssClass = "smallGridHead";
        #region Table Headers
        trow.Cells.Add(new TableCell() { Text = "" });
        trow.Cells.Add(new TableCell() { Text = "Clock In" });
        trow.Cells.Add(new TableCell() { Text = "Clock Out" });
        trow.Cells.Add(new TableCell() { Text = "Worked" });
        tblHistory.Rows.Add(trow);
        #endregion
        for (int j = 0; j < lst.Count; j++)
        {
            DataRow[] drs = dtAttendance.Select("FormatedDate = '" + lst[j].ToString("MM/dd/yyyy") + "'");
            if (drs.Length == 0)
            {
                DataRow dr = dtAttendance.NewRow();
                dr["lastin"] = lst[j];
                dr["ManualRecord"] = 1;
                dr["FormatedDate"] = lst[j].ToString("MM/dd/yyyy");
                dtAttendance.Rows.Add(dr);
            }
        }
        int idS = 0;
        dtAttendance.DefaultView.Sort = "lastin desc";
        dtAttendance = dtAttendance.DefaultView.ToTable();
        int count = 0, totalrows = 0;
        DataRow[] drMultipleClockin = null;
        for (int i = 0; i < dtAttendance.Rows.Count; i++)
        {

            if (count == totalrows)
            {
                totalrows = 0;
                count = 0;
            }
            if (count == 0)
            {
                drMultipleClockin = dtAttendance.Select("FormatedDate='" + Convert.ToDateTime(dtAttendance.Rows[i]["lastin"]).ToString("MM/dd/yyyy") + "'");
                totalrows = drMultipleClockin.Length;
            }
            else
            {
                drMultipleClockin = null;

            }
            TableRow tRowRecord = new TableRow();
            string todaysDate = "";
            if (Convert.ToDateTime(dtAttendance.Rows[i]["lastin"].ToString()).Date == DateTime.Now.Date)
            {
                todaysDate = "Today";
            }
            else
            {
                todaysDate = Convert.ToDateTime(dtAttendance.Rows[i]["lastin"].ToString()).ToString("ddd, MMM d yyy");
            }

            if (todaysDate == "Today")
            {
                tRowRecord.Cells.Add(new TableCell() { Text = todaysDate });
            }
            else //Except Today
            {
                if (drMultipleClockin != null)
                {
                    if (drMultipleClockin.Length > 1)
                    {
                        tRowRecord.Cells.Add(new TableCell() { Text = todaysDate, RowSpan = drMultipleClockin.Length });
                    }
                    else
                    {
                        tRowRecord.Cells.Add(new TableCell() { Text = todaysDate });
                    }
                }
                count++;
            }
            if (dtAttendance.Rows[i]["ManualRecord"].ToString() == "1") //Means No Record Entry, added when mapping
            {
                DataRow[] drholdays = dtHolidays.Select("date='" + Convert.ToDateTime(dtAttendance.Rows[i]["lastin"]).ToString("M/d/yyyy") + " 12:00:00 AM" + "' and Type='1'");
                if (drholdays.Length > 0) //Holiday
                {
                    tRowRecord.Attributes.Add("class", "yellowRow");
                    tRowRecord.Cells.Add(new TableCell() { Text = drholdays[0]["Title"].ToString(), ColumnSpan = 4 });
                    tRowRecord.Cells[1].Font.Bold = true;
                }
                else
                {
                    DataRow[] drWeekned = dtHolidays.Select("Title='" + Convert.ToDateTime(dtAttendance.Rows[i]["lastin"].ToString()).DayOfWeek + "' and Type='0'");
                    if (drWeekned.Length > 0) //Weekned
                    {
                        tRowRecord.Attributes.Add("class", "greenRow");
                        tRowRecord.Cells.Add(new TableCell() { Text = "Weekend", ColumnSpan = 4, CssClass = "greenRow" });
                        tRowRecord.Cells[1].Font.Bold = true;
                    }
                    else
                    {
                        DataRow[] drAbsences = dtAbsences.Select("CrtDate='" + Convert.ToDateTime(dtAttendance.Rows[i]["lastin"]).Date + "'");
                        if (drAbsences.Length > 0) //Absent
                        {
                            TableCell tcellhw = new TableCell();
                            Label lblText = new Label();
                            lblText.ID = "lblText" + ++idS;
                            lblText.CssClass = "lblText";
                            lblText.Text = "Absent - (" + drAbsences[0]["Comment"].ToString() + ") ";
                            tcellhw.Controls.Add(lblText);
                            LinkButton lnkMarkAbsence = new LinkButton();
                            lnkMarkAbsence.ID = "lnkMarkPresent" + idS;
                            lnkMarkAbsence.CssClass = "lnkMarkAbsence";
                            lnkMarkAbsence.CommandName = "lnkMarkAbsence";
                            lnkMarkAbsence.Text = "Remove absence";
                            lnkMarkAbsence.CommandArgument = Convert.ToDateTime(dtAttendance.Rows[i]["lastin"]).Date.ToString();
                            lnkMarkAbsence.Attributes.Add("data-id", drAbsences[0]["AbsLogID"].ToString());
                            lnkMarkAbsence.Attributes.Add("data-userid", drAbsences[0]["UserID"].ToString());
                            lnkMarkAbsence.Attributes.Add("data-date", drAbsences[0]["CrtDate"].ToString());
                            lnkMarkAbsence.Attributes.Add("data-active", "False");
                            lnkMarkAbsence.PostBackUrl = "";
                            tcellhw.Controls.Add(lblText);
                            if (isAdmin)
                            {
                                tcellhw.Controls.Add(lnkMarkAbsence);
                            }
                            tRowRecord.Attributes.Add("class", "pinkRow");
                            tcellhw.ColumnSpan = 4;
                            tcellhw.Font.Bold = true;
                            tRowRecord.Cells.Add(tcellhw);
                        }
                        else
                        {
                            TableCell tcellhw = new TableCell();
                            Label lblText = new Label();
                            lblText.ID = "lblText" + ++idS;
                            lblText.CssClass = "lblText";
                            lblText.Text = "No Entry - ";
                            LinkButton lnkMarkAbsence = new LinkButton();
                            lnkMarkAbsence.ID = "lnkMarkAbsence" + idS;
                            lnkMarkAbsence.CssClass = "lnkMarkAbsence";
                            lnkMarkAbsence.CommandName = "lnkMarkAbsence";
                            lnkMarkAbsence.Text = "Mark as absent";
                            lnkMarkAbsence.CommandArgument = Convert.ToDateTime(dtAttendance.Rows[i]["lastin"]).Date.ToString();
                            lnkMarkAbsence.Attributes.Add("data-id", "0");
                            lnkMarkAbsence.Attributes.Add("data-userid", userId.ToString());
                            lnkMarkAbsence.Attributes.Add("data-date", Convert.ToDateTime(dtAttendance.Rows[i]["lastin"]).Date.ToString());
                            lnkMarkAbsence.Attributes.Add("data-active", "true");
                            lnkMarkAbsence.PostBackUrl = "";
                            tcellhw.Controls.Add(lblText);
                            tcellhw.Controls.Add(lnkMarkAbsence);
                            tRowRecord.Attributes.Add("class", "pinkRow");
                            tcellhw.ColumnSpan = 4;
                            tcellhw.Font.Bold = true;
                            tRowRecord.Cells.Add(tcellhw);
                        }

                    }
                }
            }
            else
            {
                tRowRecord.Cells.Add(new TableCell() { Text = objATZ.GetTime(dtAttendance.Rows[i]["lastin"].ToString(), HttpContext.Current.User.Identity.Name) });
                if (dtAttendance.Rows[i]["lastout"].ToString() == null || dtAttendance.Rows[i]["lastout"].ToString() == "")
                {
                    tRowRecord.Cells.Add(new TableCell() { Text = "" });
                }
                else
                {
                    tRowRecord.Cells.Add(new TableCell() { Text = objATZ.GetTime(dtAttendance.Rows[i]["lastout"].ToString(), HttpContext.Current.User.Identity.Name) });
                }
                TimeSpan WorkedHours = new TimeSpan();

                if (todaysDate == "Today")//If clockout is not performed show individual records
                {
                    if (dtAttendance.Rows[i]["lastout"].ToString() == null || dtAttendance.Rows[i]["lastout"].ToString() == "")
                    {
                        WorkedHours = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dtAttendance.Rows[i]["lastin"].ToString())));
                        tRowRecord.Cells.Add(new TableCell() { Text = ConvertTimeSpan(WorkedHours) });
                    }
                    else
                    {
                        WorkedHours = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dtAttendance.Rows[i]["lastout"].ToString())).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dtAttendance.Rows[i]["lastin"].ToString())));
                        tRowRecord.Cells.Add(new TableCell() { Text = ConvertTimeSpan(WorkedHours) });
                    }
                }
                else
                {
                    if (drMultipleClockin != null)
                    {
                        if (drMultipleClockin.Length > 1) //
                        {
                            for (int p = 0; p < drMultipleClockin.Length; p++)
                            {
                                WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(drMultipleClockin[p]["lastout"].ToString())).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(drMultipleClockin[p]["lastin"].ToString())));
                            }

                            tRowRecord.Cells.Add(new TableCell() { Text = ConvertTimeSpan(WorkedHours), RowSpan = drMultipleClockin.Length });
                        }
                        else
                        {
                            if (dtAttendance.Rows[i]["lastout"].ToString() == null || dtAttendance.Rows[i]["lastout"].ToString() == "")
                            {
                                WorkedHours = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dtAttendance.Rows[i]["lastin"].ToString())));
                                tRowRecord.Cells.Add(new TableCell() { Text = ConvertTimeSpan(WorkedHours) });
                            }
                            else
                            {
                                WorkedHours = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dtAttendance.Rows[i]["lastout"].ToString())).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dtAttendance.Rows[i]["lastin"].ToString())));
                                tRowRecord.Cells.Add(new TableCell() { Text = ConvertTimeSpan(WorkedHours) });
                            }
                        }
                    }

                }
            }
            tblHistory.Rows.Add(tRowRecord);

        }
    }
    public List<DateTime> GetDatesBetween(DateTime startDate, DateTime endDate)
    {
        List<DateTime> allDates = new List<DateTime>();
        for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
            allDates.Add(date);
        return allDates;

    }
    private string WorkedHour(DataTable dt)
    {
        TimeSpan WorkedHours = new TimeSpan();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["lastout"].ToString() == null || dt.Rows[i]["lastout"].ToString() == "")
            {
                WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(DateTime.Now).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
            }
            else
            {
                WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastout"].ToString())).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
            }
        }

        return UtilityMethods.getFormatedTimeByMinutes(WorkedHours.TotalMinutes.ToInt32());
    }
    protected void btnGR_Click(object sender, EventArgs e)
    {
        string year = ddlYear.SelectedValue.ToString();
        string month = ddlMonth.SelectedValue.ToString();
        int monthI = DateTime.ParseExact(month, "MMMM", CultureInfo.InvariantCulture).Month;
        DateTime startDate = new DateTime(Convert.ToInt16(year), monthI, 1, 0, 0, 0);
        if (startDate > DateTime.Now)
        {
            trMsg.Visible = true;
            hdnData.Value = "false";
        }
        else
        {
            trMsg.Visible = false;
            hdnData.Value = "true";
            DateTime endDate = new DateTime(Convert.ToInt16(year), monthI, DateTime.DaysInMonth(startDate.Year, startDate.Month), 23, 59, 59);
            if (endDate > DateTime.Now)
            {
                endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            }
            DataTable dtHolidays = GetMonthlyHolidays(startDate, endDate);
            DataTable dtAbsences = GetMonthlyAbsences(userId, startDate, endDate);
            DataTable dtAttendance = GetMonthlyAttendance(userId, startDate, endDate);
            DataTable uniqueCols = dtAttendance.DefaultView.ToTable(true, "FormatedDate");
            List<DateTime> lst = GetDatesBetween(startDate, endDate);
            lblWHours.Text = WorkedHour(dtAttendance);
            CreateTable(dtAttendance, dtHolidays, dtAbsences, lst);
            lblTotalAbsences.Text = dtAbsences.Rows.Count.ToString();
            lblTotDays.Text = uniqueCols.Rows.Count.ToString();
            lblUserName.Text = ((ListItem)ddlUsers.SelectedItem).Text;
        }
        //  int month
        //int userId= dd
    }



}