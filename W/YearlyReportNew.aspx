﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="YearlyReportNew.aspx.cs" Inherits="YearlyReportNew" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Yearly Report</title>
    <link rel="stylesheet" type="text/css" media="screen" href="_assets/style.css" />
    <link rel="stylesheet" href="_assets/jquery-ui.css" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="_assets/css/tooltipster.css" />
    <link href="_assets/tablestyles.css" rel="stylesheet" />
    <script type="text/javascript" src="_assets/js/jquery.tooltipster.js"></script>
    <link rel="stylesheet" href="_assets/jquery.timepicker.css" />
    <script src="_assets/jquery.timepicker.min.js"></script>
    <script src="_assets/DateTimeHelper.js"></script>
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <script>
        $(document).ready(function () {
            $('.tooltip').tooltipster({
                interactive: true
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <section id="secprofile" style="margin-top: 5px">
                <h1 class="username">
                    <asp:Label ID="lblUserName" runat="server" Text="Derp" Style="margin-bottom: 5px;"></asp:Label>
                    &nbsp;<asp:LinkButton runat="server" ID="bk" Text="Back to reports" Style="font-size: 12px;" OnClick="bk_Click"></asp:LinkButton>
                </h1>
                <div style="clear: both">
                </div>
                <div style="clear: both">
                    <div style="text-align: left">
                        <div id="filtersContainer" runat="server" visible="false">
                            <span id="tmC" style="display: none">Start Time:
                    <input type="text" id="time" name="time" style="width: 85px" /><input type="button" value="Show" id="btntC" style="margin-top: 2px" />
                            </span>
                        </div>
                    </div>
                </div>

                <table class="profile" style="margin-top: 12px">
                    <tr id="trSelUser" runat="server">
                        <td colspan="2">Select Employee: 
                <asp:DropDownList runat="server" ID="ddlUsers">
                    <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">Please select year
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlYear" Height="21px" Width="148px">
                                <asp:ListItem Text="Select Year" Value="0" Enabled="true"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnGR" runat="server" Text="Generate Report" Width="190px" Style="margin-top: -2px" class="ui-state-default" OnClick="btnGR_Click" />
                        </td>
                    </tr>
                <%--    <tr class="trdata">
                        <td colspan="2" style="font-weight: bold; padding: 7px 0px">From
                <asp:Label ID="lblStartDate" runat="server"></asp:Label>&nbsp
                to &nbsp<asp:Label ID="lblEndDate" runat="server"></asp:Label>
                        </td>
                    </tr>--%>
                    <tr class="trdata">
                        <td class="caption">Absences this year: 
                        </td>
                        <td>
                            <asp:Label ID="lblTotalAbsences" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                    <tr class="trdata">
                        <td class="caption">Total days worked: 
                        </td>
                        <td>
                            <asp:Label ID="lblTotDays" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>

                    <%--<tr class="trdata">
                        <td class="caption">Working hrs should be: 
                        </td>
                        <td>
                            <asp:Label ID="lblEstWHrs" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>--%>
                    <tr class="trdata">
                        <td class="caption">Worked this year:
                        </td>
                        <td>
                            <asp:Label ID="lblWHours" Text="0 Minute" runat="server" Style="float: left"></asp:Label>

                        </td>
                    </tr>
                    <%-- <tr class="trdata">
                        <td class="caption">Breaks this year:
                        </td>
                        <td>
                            <asp:Label ID="lblTotBreakHrs" runat="server" Style="float: left"></asp:Label>

                        </td>
                    </tr>--%>
                    <%--<tr class="trdata">
                        <td class="caption">Extra days worked:
                        </td>
                        <td>
                            <asp:Label ID="lblExtraDays" runat="server" Style="float: left"></asp:Label>

                        </td>
                    </tr>--%>
                </table>
                <div>
                    <asp:Label ID="lblTest" runat="server" Visible="false"></asp:Label>
                </div>
                <asp:HiddenField ID="hdnData" ClientIDMode="Static" Value="false" runat="server" />
                <asp:HiddenField ID="hfparentid" runat="server" />
                <asp:Table ID="tblHistory" runat="server">
                </asp:Table>
            </section>
        </div>
    </form>

</body>
</html>
