﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Absence.aspx.cs" Inherits="W_Absence" MasterPageFile="~/W/MasterPage.master" %>

<asp:Content runat="server" ContentPlaceHolderID="head">

    <style type="text/css">
        .auto-style1 {
            width: 159px;
        }

        .auto-style2 {
            width: 137px;
        }

        .Clear {
            clear: both;
        }
    </style>

    <script>
        jQuery(document).ready(function ($) {
            $("input[id$='txtDate']").datepicker();

            $('.lnkadd_worker, .lnkAdd_worker1').click(function () {
                openDialog();
            });

            function openDialog() {
                $('#txttitle').val('');

                var dlg = $("#divAbs").dialog({
                    show: {
                    },
                    top: 80,
                    left: 330,
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                $('.txtEmail').focus();
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                dlg.parent().css("width", "455px");
                dlg.parent().css("top", "50px");
            }

        });

        HTMLInputElement.prototype.startsWith = function () { return false; };

    </script>

    <asp:ScriptManager runat="server" ID="scriptmanager1"></asp:ScriptManager>
    <asp:HiddenField runat="server" ID="Option" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnUserID" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnParentID" ClientIDMode="Static" />

    <div class="space">
        <div class="Clear"></div>
        <div class="Tab AttendenceSettingDiv3">
            <h2 class="username">
                <asp:Label ID="lblUserName" runat="server" Text="Unmarked Days"></asp:Label>
            </h2>
            <br />
            <div>
                <table style="width: 314px" class="table-responsive">
                    <tr class="smallGrid" id="">
                        <td class="auto-style3">Select Year </td>
                        <td class="auto-style1">
                            <asp:DropDownList runat="server" ID="ddlYear" CssClass="ddl" ClientIDMode="Static" Style="width: 100px;" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>

            <div>
                <asp:UpdatePanel runat="server" ID="absence_panel">
                    <ContentTemplate>
                        <asp:Repeater runat="server" ID="rpthd" OnItemDataBound="rpthd_ItemDataBound" OnItemCommand="rpthd_ItemCommand">
                            <HeaderTemplate>
                                <table class="table-grid-sample1 table-responsive">
                                    <tr>
                                        <td>Absence Reason
                                        </td>
                                        <td>Date
                                        </td>
                                        <td></td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("AbsLogID") %>' />
                                        <asp:Label runat="server" ID="hdtitle" Text='<%# Eval("Comment") %>'></asp:Label>
                                        <asp:TextBox runat="server" ID="txtTitle" Text='<%# Eval("Comment") %>' Visible="false" ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="hdDate" Text='<%# Eval("CrtDate") %>'></asp:Label>
                                        <asp:TextBox runat="server" ID="txtDate" Text='<%# Eval("CrtDate") %>' Visible="false" ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:LinkButton runat="server" ID="edit" CommandName="edit" Text="Edit"> <i class="fa fa-fw fa-pencil"></i></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="save" CommandName="save" Text="Save" Visible="false"> <i class="fa fa-fw fa-save"></i></asp:LinkButton>&nbsp;
                                        <asp:LinkButton runat="server" ID="cancel" CommandName="cancel" Visible="false"> <i class="fa fa-fw fa-close"></i></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="del" CommandName="del" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete?')"> <i class="fa fa-fw fa-trash"></i></asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <br />
                        <asp:Label runat="server" Text="No Record Found." Visible="False" ID="hdmsg" Font-Bold="true"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlYear" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>

        <div class="Clear"></div>
    </div>
</asp:Content>
