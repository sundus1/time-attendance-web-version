﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class YearlyReport : AvaimaThirdpartyTool.AvaimaWebPage
{
    int id = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            year.Items.Add(DateTime.Now.Year.ToString());
            year.Items.Add((DateTime.Now.Year - 1).ToString());
        }
        if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out id))
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("select * from attendence_management a where a.Id = @id and a.InstanceId = @insId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@insId", this.InstanceID);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];
                        name.Text = dr["Title"].ToString();
                    }
                    else
                    {
                        Response.Redirect("Default.aspx");
                    }
                }
            }
        }
        else
        {
            Response.Redirect("Default.aspx");
        }
    }

    protected void btnGenRpt_Click(object sender, EventArgs e)
    {

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("GetYearlyReport", con))
            {
                DataTable dt = new DataTable();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Year", year.SelectedValue);
                cmd.Parameters.AddWithValue("@Id", id);
                cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    if (DateTime.Now.Year == Convert.ToInt32(year.SelectedValue))
                    {
                        rptReport.DataSource = dt.Select().Where(x => x.Field<int>("month") < DateTime.Now.Month).ToArray();
                    }
                    else
                    {
                        rptReport.DataSource = dt.Select();
                    }
                    rptReport.DataBind();
                    rmaindiv.Visible = true;
                }
                else
                {
                    rmaindiv.Visible = false;
                }
            }
        }
    }
    
    protected void rptReport_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRow container = (DataRow)e.Item.DataItem;
            Label m = e.Item.FindControl("m") as Label;
            Label w = e.Item.FindControl("w") as Label;
            Label a = e.Item.FindControl("a") as Label;
            Label h = e.Item.FindControl("h") as Label;
            Label ew = e.Item.FindControl("e") as Label;
            LinkButton d = e.Item.FindControl("d") as LinkButton;
            m.Text = DateTime.ParseExact("1900-" + (container.Field<int>("month") < 10 ? "0" + container["month"].ToString() : container["month"].ToString()) + "-01", "yyyy-MM-dd", null).ToString("MMMM");
            w.Text = container["TotalWorkingDays"].ToString();
            a.Text = container["TotalAbsences"].ToString();
            h.Text = container["TotalHolidays"].ToString();
            ew.Text = container["ExtraDaysWork"].ToString();
            d.CommandArgument = container["month"].ToString() + ":" + year.SelectedValue;
        }
    }

    protected void rptReport_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "det")
        {
            Response.Redirect("MonthlyReport.aspx?id=" + id.ToString() + "&my=" + e.CommandArgument);
        }
    }
}