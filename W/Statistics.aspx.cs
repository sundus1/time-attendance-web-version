﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Statistics : AvaimaThirdpartyTool.AvaimaWebPage
{
    int userId = 0;
    string instanceID = "";
    string Appid = "";
    Attendance atd = new Attendance();
    DefaultSettings _defaultSettings = new DefaultSettings();
    DataAccessLayer dal = new DataAccessLayer(false);

    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Write(Test());
        //Response.End();

        if (Request.QueryString["id"] != null)
        {
            userId = Convert.ToInt16(Request.QueryString["id"]);
        }
        if (Request.QueryString["instanceid"] != null)
        {
            instanceID = Request.QueryString["instanceid"];
        }

        if (!Page.IsPostBack)
        {
            string isVerified = atd.VerifyLogin(Request.QueryString["e"].Decrypt(), Request.QueryString["p"].Decrypt());
            if (isVerified != "TRUE")
                this.Redirect("Info.aspx");

            LoadData();
            lnkStatsPage.PostBackUrl = atd.WebURL("settings", userId.ToString(), instanceID, Request.QueryString["e"].Decrypt(), Request.QueryString["p"].Decrypt());

            ////Load default Report Setting 
            //Hashtable ht = new Hashtable();
            //ht["@userid"] = userId;
            //ht["@action"] = "get";
            //ReportSettings(ht);
        }

        DataTable userSettings = JsonConvert.DeserializeObject<DataTable>(atd.GetSettingInfo(Request.QueryString["e"].ToString().Decrypt(), Request.QueryString["p"].ToString().Decrypt()));
        Appid = userSettings.Rows[0]["userid"].ToString();

    }

    public void LoadData()
    {
        LoadUsers();

        //Get First Clockin DateTime of this Instance; to bind Year Dropdown
        DataTable instanceFirst_clockin = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            con.Open();
            using (SqlCommand cmd = new SqlCommand("Select top 1 lastin from workertrans where p_pid in (Select ID from attendence_management where instanceid = @InstnaceID)", con))
            {
                cmd.Parameters.AddWithValue("@InstnaceID", instanceID);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(instanceFirst_clockin);
            }
        }

        DateTime date = DateTime.Now;
        DateTime InstanceFirstClockinDateTime = (instanceFirst_clockin.Rows.Count > 0) ? Convert.ToDateTime(instanceFirst_clockin.Rows[0]["lastin"]) : date;

        for (int i = InstanceFirstClockinDateTime.Year; i <= date.Year; i++)
        {
            ddlYear.Items.Add(new ListItem(i.ToString()));
            ddlYearly.Items.Add(new ListItem(i.ToString()));
        }

        ddlYear.SelectedValue = ddlYearly.SelectedValue = date.Year.ToString();

        //ddlYear.Items.Add(new ListItem(date.AddYears(-2).Year.ToString(), date.AddYears(-2).Year.ToString()));
        //ddlYear.Items.Add(new ListItem(date.AddYears(-1).Year.ToString(), date.AddYears(-1).Year.ToString()));
        //ddlYear.Items.Add(new ListItem(date.Year.ToString(), date.Year.ToString()));
        //ddlYear.SelectedValue = date.Year.ToString();

        //ddlYearly.Items.Add(new ListItem(date.AddYears(-2).Year.ToString(), date.AddYears(-2).Year.ToString()));
        //ddlYearly.Items.Add(new ListItem(date.AddYears(-1).Year.ToString(), date.AddYears(-1).Year.ToString()));
        //ddlYearly.Items.Add(new ListItem(date.Year.ToString(), date.Year.ToString()));
        //ddlYearly.SelectedValue = date.Year.ToString();

        ddlMonth.SelectedValue = date.Month.ToString();

        //Hours Dropdown
        for (int i = 1; i <= 12; i++)
        {
            ddlHours.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        ddlHours.SelectedValue = "8";


        //Minutes Dropdown
        for (int i = 0; i < 60; i++)
        {
            ddlMinutes.Items.Add(new ListItem(i.ToString(), i.ToString()));
            ddlBrkMinutes.Items.Add(new ListItem(i.ToString(), i.ToString()));
            ddlAdditionalBrkMin.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }

    }

    public void LoadUsers()
    {
        DataTable dt = new DataTable();

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            con.Open();
            SqlTransaction transaction = con.BeginTransaction(IsolationLevel.ReadUncommitted);

            //using (SqlCommand cmd = new SqlCommand("sp_GetAllUsers_BACKUP", con))
            using (SqlCommand cmd = new SqlCommand("GetUsers", con))
            {
                cmd.Parameters.AddWithValue("@instanceid", instanceID);
                cmd.Parameters.AddWithValue("@userid", userId);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = transaction;
                cmd.CommandTimeout = 90;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);


                dt.DefaultView.Sort = "ID ASC";
                dt = dt.DefaultView.ToTable();

                if (dt.Rows.Count > 0)
                {
                    ddlUsers.DataValueField = "ID";
                    ddlUsers.DataTextField = "Title";
                    ddlUsers.DataSource = dt;
                    ddlUsers.DataBind();
                }

                for (int i = 0; i < ddlUsers.Items.Count; i++)
                {
                    ListItem item = ddlUsers.Items[i];
                    item.Attributes["parentid"] = dt.Select("ID=" + ddlUsers.Items[i].Value)[0]["parentid"].ToString();
                }
            }

            ddlUsers.Items.Insert(0, new ListItem(" Select ", "-1"));
        }

        if (Request.QueryString["uid"] != null)
        {
            ddlUsers.SelectedValue = Request.QueryString["uid"].ToString();
        }
        else
        {
            if (dt.Rows.Count == 1)
            {
                ddlUsers.SelectedIndex = 1;
                ddlUsers.Enabled = false;
            }
        }
    }

    public void GenerateStats123(string type)
    {
        result_table.Visible = true;
        string html = "";
        int id = ddlUsers.SelectedValue.ToInt32();
        //string Appid = "648dec41-d961-4a78-a615-d9aac688c3ad";
        string Appid = HttpContext.Current.User.Identity.Name;

        string report_type = rbtnReportType.SelectedValue;
        int month = ddlMonth.SelectedValue.ToInt32();
        int year = (rbtnReportType.SelectedValue == "yearly") ? DateTime.Now.Year : ddlYear.SelectedValue.ToInt32();
        //string report_type = (rbtnYearly.Checked) ? "yearly" : "monthly";
        //int month = ddlMonth.SelectedValue.ToInt32();
        //int year = (rbtnYearly.Checked) ? DateTime.Now.Year : ddlYear.SelectedValue.ToInt32();


        Hashtable stats = JsonConvert.DeserializeObject<Hashtable>(atd.CalculateStatistics(userId, year, month, id, instanceID, report_type));
        string heading = "";

        if (report_type == "yearly")
        {
            heading = year.ToString();
            html = "<div id='reportDiv'><h4>Performance Report - " + heading + "</h4>";
            html += "<div><div><b>Working Days " + heading + " <span title='Any day which is not a weekend or a holiday is a working day.'><i class='fa fa-fw fa-exclamation-circle' class='red'></i></span>: </b>" + stats["YearlyWorkingDays"].ToString() + " days" + "<br>";
            TotalWorkingDays.Value = stats["YearlyWorkingDays"].ToString();
        }
        else if (report_type == "monthly")
        {
            heading = ddlMonth.SelectedItem.Text + " " + year;
            html = "<div id='reportDiv'><h4>Performance Report - " + heading + "</h5>";
            html += "<div><div><b>Working Days " + heading + " <span title='Any day which is not a weekend or a holiday is a working day.'><i class='fa fa-fw fa-exclamation-circle' ></i></span>: </b>" + stats["MonthlyWorkingDays"].ToString() + " days" + "<br>";
            TotalWorkingDays.Value = stats["MonthlyWorkingDays"].ToString();
        }

        WorkedDays.Value = stats["TotalDaysWorked"].ToString();
        WorkedHours.Value = stats["TotalWorkedHours"].ToString();

        string url = atd.WebURL("settings", userId.ToString(), instanceID, Request.QueryString["e"].Decrypt(), Request.QueryString["p"].Decrypt());
        string absences_url = atd.WebURL("absences", Request.QueryString["id"].ToString(), instanceID, Request.QueryString["e"].Decrypt(), Request.QueryString["p"].Decrypt(), id.ToString());
        string unmarked_url = atd.WebURL("unmarked", userId.ToString(), instanceID, Request.QueryString["e"].Decrypt(), Request.QueryString["p"].Decrypt(), id.ToString(), stats["startDate"].ToString(), stats["endDate"].ToString());

        //Summarized Stats Table
        string sum_table = html;
        sum_table += "<b>Public Holidays " + heading + " <span title='These are days that you manually mark as holidays. You can change the settings, add/remove holidays and refresh the statistics to reflect those changes in these stats.'><i class='fa fa-fw fa-exclamation-circle' ></i></span>: </b>" + stats["TotalHolidays"].ToString() + " days (<a href='" + url + "' target='_blank'>View setting</a>)" + "<br>";
        sum_table += "<b>Weekends " + heading + " <span title='Days that you mark as a weekly holiday. You can change the settings, add/remove/change weekend definition and refresh the statistics to reflect those changes in these stats.'><i class='fa fa-fw fa-exclamation-circle' ></i></span>: </b>" + stats["TotalWeekends"].ToString() + " days (<a href='" + url + "' target='_blank'>Update</a>)</div>";
        sum_table += "<div><b>Absences " + heading + " <span title='These are days that are manually marked as absent by the employee or yourself.'><i class='fa fa-fw fa-exclamation-circle'></i></span>: </b>" + stats["TotalAbsences"].ToString() + " days (<a href='" + absences_url + "' target='_blank'>View</a>)" + "<br>";
        sum_table += "<b>Un-marked Days " + heading + " <span title='Days when the user did not clock-in, was not marked absent, is not a weekend, or a holiday.'><i class='fa fa-fw fa-exclamation-circle' ></i></span>: </b>" + stats["UnmarkedDays"].ToString() + " days (<a href='" + unmarked_url + "' target='_blank'>View</a>)" + "<br>";
        sum_table += "<b>Worked Days " + heading + " <span title='Any day in which the employee clocked-in is counted as a worked-day. Even if a day is marked as absent and the employee clocks-in then it is included as a worked day.'><i class='fa fa-fw fa-exclamation-circle' ></i></span>: </b>" + stats["TotalDaysWorked"].ToString() + "<br>";
        sum_table += "<b>Total Hours Clocked " + heading + " <span title='The total time in hours/minutes that the user clocked.'><i class='fa fa-fw fa-exclamation-circle' ></i></span>: </b>" + stats["TotalWorkedHours"].ToString() + "</div>";


        //sum_table += "<tr><td><b>Public Holidays " + heading + ": </b> </td><td>" + stats["TotalHolidays"].ToString() + " days (<a href='" + url + "' target='_blank'>View days</a>)</td></tr>";
        //sum_table += "<tr><td><b>Weekends " + heading + ": </b> </td><td>" + stats["TotalWeekends"].ToString() + " days (<b>" + stats["WeekendDays"] + "</b> <a href='" + url + "' target='_blank'>Change days</a>)</td></tr>";
        //sum_table += "<tr><td><b>Absences " + heading + ": </b></td><td>" + stats["TotalAbsences"].ToString() + " days</td></tr>";
        //sum_table += "<tr><td><b>Worked Days " + heading + ": </b></td><td>" + stats["TotalDaysWorked"].ToString() + " days</td></tr>";
        //sum_table += "<tr><td><b>Total Hours Clocked " + heading + ": </b></td><td>" + stats["TotalWorkedHours"].ToString() + " </td></tr>";
        sum_table += "</div><div><br/>";

        if (type == "detail_stats")
        {
            result.InnerHtml = "";
        }
        else if (type == "summarized_stats")
        {
            result.InnerHtml = sum_table;
        }
    }


    //public void GenerateStats(string type)
    //{
    //    result_table.Visible = true;
    //    string html = "";
    //    int id = ddlUsers.SelectedValue.ToInt32();
    //    int year = DateTime.Now.Year;
    //    //string Appid = "648dec41-d961-4a78-a615-d9aac688c3ad";
    //    string Appid = HttpContext.Current.User.Identity.Name;

    //    string report_type = (rbtnYearly.Checked) ? "yearly" : "monthly";
    //    int month = ddlMonth.SelectedValue.ToInt32();

    //    Hashtable stats = JsonConvert.DeserializeObject<Hashtable>(atd.CalculateStatistics(userId, year, month, id, instanceID, report_type));
    //    string heading = "";

    //    if (report_type == "yearly")
    //    {
    //        heading =  year.ToString();
    //        html = "<div id='reportDiv' ><h5 style='text-align: center;font-weight: bold;'>Performance Report - " + heading + "</h5><table class='table table-responsive table-sm'>";
    //        html += "<tr><td><b>Working Days <i class='fa fa-fw fa-exclamation-circle'></i>" + heading + ": </b></td><td>" + stats["YearlyWorkingDays"].ToString() + " days</td></tr>";

    //    }
    //    else if (report_type == "monthly")
    //    {
    //        heading = ddlMonth.SelectedItem.Text + " " + year;
    //        html = "<div id='reportDiv' ><h5 style='text-align: center;font-weight: bold;'>Performance Report - " + heading + "</h5><table class='table table-responsive table-sm'>";
    //        html += "<tr><td><b>Working Days " + heading + ": </b></td><td>" + stats["MonthlyWorkingDays"].ToString() + " days</td></tr>";

    //    }

    //    string url = atd.WebURL("settings", userId.ToString(), instanceID, Request.QueryString["e"].Decrypt(), Request.QueryString["p"].Decrypt());

    //    //Summarized Stats Table
    //    string sum_table = html;
    //    sum_table += "<tr><td><b>Public Holidays " + heading + ": </b> </td><td>" + stats["TotalHolidays"].ToString() + " days (<a href='" + url + "' target='_blank'>View days</a>)</td></tr>";
    //    sum_table += "<tr><td><b>Weekends " + heading + ": </b> </td><td>" + stats["TotalWeekends"].ToString() + " days (<b>"+stats["WeekendDays"] +"</b> <a href='" + url + "' target='_blank'>Change days</a>)</td></tr>";
    //    sum_table += "<tr><td><b>Absences " + heading + ": </b></td><td>" + stats["TotalAbsences"].ToString() + " days</td></tr>";
    //    sum_table += "<tr><td><b>Worked Days " + heading + ": </b></td><td>" + stats["TotalDaysWorked"].ToString() + " days</td></tr>";
    //    sum_table += "<tr><td><b>Total Hours Clocked " + heading + ": </b></td><td>" + stats["TotalWorkedHours"].ToString() + " </td></tr>";
    //    sum_table += "</table></div>";

    //    if (type == "detail_stats")
    //    {
    //        result.InnerHtml = "";
    //    }
    //    else if (type == "summarized_stats")
    //    {
    //        result.InnerHtml = sum_table;
    //    }
    //}


    //public void GenerateStats1(string type)
    //{
    //    //HtmlTableRow htmlrow = (HtmlTableRow)this.Master.FindControl("head").FindControl("filter_tr");
    //    //htmlrow.Attributes.Remove("style");
    //    // Attributes.Add("Style", "display:block");

    //    result_table.Visible = true;

    //    int id = ddlUsers.SelectedValue.ToInt32();
    //    int hours = (ddlHours.SelectedValue != "") ? ddlHours.SelectedValue.ToInt32() : _defaultSettings.WorkingHour;
    //    int year = DateTime.Now.Year;
    //    int breakHr = (ddlBrkHour.SelectedValue != "") ? ddlBrkHour.SelectedValue.ToInt32() : _defaultSettings.BreakTime;
    //    string StrtHr = (ddlBrkHour.SelectedValue != "") ? txtStartHour.Text : _defaultSettings.StartTime;
    //    string EndHr = (txtEndHour.Text != "") ? txtEndHour.Text : _defaultSettings.EndTime;
    //    //string Appid = "648dec41-d961-4a78-a615-d9aac688c3ad";
    //    string Appid = HttpContext.Current.User.Identity.Name;

    //    Hashtable stats = JsonConvert.DeserializeObject<Hashtable>(atd.CalculateStatistics(userId, hours, year, id, instanceID, breakHr, StrtHr, EndHr, Appid));
    //    string html = "<div id='reportDiv' ><h5 style='text-align: center;font-weight: bold;'>Performance Report - "+System.DateTime.Now.Year.ToString()+"</h5><table class='table table-responsive table-sm'>";


    //    //Detailed Stats Table
    //    string table = html;
    //    table += "<tr><td><b>Total Absences:  </b></td><td>" + stats["TotalAbsences"].ToString() + " days</td></tr>";
    //    table += "<tr ><td rowspan='2'><b>Worked Days: </b> </td><td> With break: " + stats["TotalDaysWorked"].ToString() + " (Should be: " + stats["WorkingDays"].ToString() + " days)</td><tr/>";
    //    table += "<tr><td></td><td>Without break: " + stats["TotalDaysWorkedB"].ToString() + " (Should be: " + stats["WorkingDaysB"].ToString() + " days)</td></tr>";
    //    table += "<tr><td rowspan='2'><b>Worked Hours: </b> </td><td> With break: " + stats["TotalWorkedHours"].ToString() + " (Should be: " + stats["WorkingHours"].ToString() + " hours)</td></tr>";
    //    table += "<tr></tr>";
    //    table += "<tr><td></td><td>Without break: " + stats["TotalWorkedHoursB"].ToString() + " (Should be: " + stats["WorkingHoursB"].ToString() + " hours)</td></tr>";
    //    table += "<tr><td><b>Extra Worked Hours: </b> </td><td>" + stats["ExtraHours"].ToString() + " hours</td></tr>";
    //    table += "<tr><td><b>Under Worked Hours: </b> </td><td>" + stats["UnderHours"].ToString() + " hours</td></tr>";
    //    table += "</table></div>";

    //    //Summarized Stats Table
    //    string sum_table = html;
    //    sum_table += "<tr><td><b>Total Absences: </b></td><td>" + stats["TotalAbsences"].ToString() + " days</td></tr>";
    //    sum_table += "<tr><td><b>Worked Days: </b> </td><td>" + stats["TotalDaysWorked"].ToString() + " days</td></tr>";
    //    sum_table += "<tr><td><b>Worked Hours: </b></td><td>" + stats["TotalWorkedHours"].ToString() + " hours</td></tr>";
    //    sum_table += "</table></div>";


    //    if (type == "detail_stats")
    //    {
    //        result.InnerHtml = table;
    //    }
    //    else if (type == "summarized_stats")
    //    {
    //        result.InnerHtml = sum_table;
    //    }

    //    //ScriptManager.RegisterStartupScript(this, GetType(), "", "    $('html, body').animate({ scrollTop: $('#reportDiv').offset().top}, 2000);", true);

    //}

    protected void btnGenerateReportTemp_Click(object sender, EventArgs e)
    {
        LinkButton clickedButton = (LinkButton)sender;
        int id = ddlUsers.SelectedValue.ToInt32();

        ddlUsers.Items.Clear();
        LoadUsers();
        ddlUsers.SelectedValue = id.ToString();


        #region General

        string parent = "";
        int parentid = ddlUsers.SelectedItem.Attributes["parentid"].ToInt32();
        if (parentid == 0)
        {
            parentid = userId;
        }

        if (parentid != userId)
        {
            parent = "class='hide'";
        }

        int TotalAbsences = 0;
        int hours = 1;
        hdn_isworkdefine.Value = "False";
        bool Chophours = false;
        string ChophoursFrom = "";

        Hashtable ht = new Hashtable();

        ht["@action"] = "get";
        ht["@userid"] = parentid;
        DataTable reportsettings = dal.GetDataTable("sp_ReportSettings", ht);
        if (reportsettings.Rows.Count > 0)
        {
            TotalAbsences = (reportsettings.Rows[0]["AbsencesAllowed"] != DBNull.Value) ? reportsettings.Rows[0]["AbsencesAllowed"].ToInt32() : TotalAbsences;
            hours = (reportsettings.Rows[0]["working_hours"] != DBNull.Value) ? reportsettings.Rows[0]["working_hours"].ToString().Split(',')[0].ToInt32() : hours;
            hdn_isworkdefine.Value = (reportsettings.Rows[0]["ApplyWorkingHours"] != DBNull.Value) ? reportsettings.Rows[0]["ApplyWorkingHours"].ToString() : hdn_isworkdefine.Value;
            Chophours = (reportsettings.Rows[0]["Chophours"] != DBNull.Value) ? Convert.ToBoolean(reportsettings.Rows[0]["Chophours"]) : Chophours;
            ChophoursFrom = (reportsettings.Rows[0]["ChophoursFrom"] != DBNull.Value) ? Convert.ToDateTime(reportsettings.Rows[0]["ChophoursFrom"]).ToShortTimeString() : ChophoursFrom;
        }

        string text = "";

        if (hdn_isworkdefine.Value == "True")
        {
            text += "<table class='table-grid-sample1'><tr class='headerRow'><th>Week Day</th><th>Clock-in Time</th><th>Clock-out Time</th><th>Break Start Time</th><th>Break End Time</th><th>Working Hours</th></tr>";

            DataTable dt = atd.GetWorkingHours(parentid);
            foreach (DataRow dr in dt.Rows)
            {
                text += "<tr><td>" + dr["DayTitle"] + "</td><td>" + Convert.ToDateTime(dr["Clockin"]).ToShortTimeString() + "</td><td>" + Convert.ToDateTime(dr["Clockout"]).ToShortTimeString() + "</td><td>" + Convert.ToDateTime(dr["BreakStart"]).ToShortTimeString() + "</td><td>" + Convert.ToDateTime(dr["BreakEnd"]).ToShortTimeString() + "</td><td>";
                text += (!string.IsNullOrEmpty(dr["TotalWorkingHours"].ToString())) ? dr["TotalWorkingHours"].ToString() + " Hours" : "" + "</td></tr>";
            }

            text += "</table><br/>";
            if (TotalAbsences > 0)
                text += "<b>Absences allowed in a year: </b>" + TotalAbsences.ToString() + "<br/>";

            if (Chophours && (!string.IsNullOrEmpty(ChophoursFrom)))
                text += "<b><u style='color:red;'>Note:</u> Time spent in morning from " + ChophoursFrom + " to defined Clock-in Time will be chopped.</b><br/>";

            if (instanceID == "f425f912-8d79-4415-ba72-89a5309acb10")
                text += "<b>Also time spent during break will be removed from total hours worked.</b>";

            InfoDetails.InnerHtml = text;
        }
        else
        {
            if (hours > 1)
                InfoDetails.InnerText = "Working hours in a day should be " + hours.ToString();
        }
        if (ddlreports.SelectedValue == "1")
        {
            //int hours = ddlHours.SelectedValue.ToInt32() + Math.Abs((ddlMinutes.SelectedValue != "0") ? (ddlMinutes.SelectedValue.ToInt32() / 60) : 0);
            decimal breakHr = ddlBrkHour.SelectedValue.ToInt32() + Math.Abs((ddlBrkMinutes.SelectedValue != "0") ? (ddlBrkMinutes.SelectedValue.ToInt32() / 60) : 0);
            string additionalbreak = ddlAdditionalBrk.SelectedValue;
            decimal additionalbreakHr = ddlAdditionalBrkHr.SelectedValue.ToInt32() + Math.Abs((ddlAdditionalBrkMin.SelectedValue != "0") ? (ddlAdditionalBrkMin.SelectedValue.ToInt32() / 60) : 0);
            string StrtHr = txtStartHour.Text;
            string EndHr = txtEndHour.Text;

            //  string Appid = "648dec41-d961-4a78-a615-d9aac688c3ad";
            //string Appid = HttpContext.Current.User.Identity.Name;

            string report_type = rbtnReportType.SelectedValue;
            int month = ddlMonth.SelectedValue.ToInt32();
            int year = (rbtnReportType.SelectedValue == "yearly") ? ddlYearly.SelectedValue.ToInt32() : ddlYear.SelectedValue.ToInt32();

            string report_for = rbtnReportFor.SelectedValue;

            string heading = "Yearly Report: " + year;

            if (report_type == "monthly")
            {
                heading = "Monthly Report: " + ddlMonth.SelectedItem.Text + " " + ddlYear.SelectedItem.Text;
            }
            else if (report_type == "custom")
            {
                heading = "Report: " + Convert.ToDateTime(dateTo.Text).ToString("dd MMM yyyy") + " - " + Convert.ToDateTime(dateFrom.Text).ToString("dd MMM yyyy");
            }

            Hashtable stats = JsonConvert.DeserializeObject<Hashtable>(atd.AdvanceCalculateStatistics(parentid, hours, year, month, id, instanceID, TotalAbsences, breakHr, StrtHr, EndHr, Appid, report_type, additionalbreak, additionalbreakHr, dateTo.Text, dateFrom.Text));

            //Managed User
            string c = "";
            if (id == userId)
            {
                if (!atd.UserManaged(userId.ToInt32(), instanceID, "get"))
                {
                    c = "class='hide'";
                }
            }

            string sum_table = "<div id='reportDiv'><h5 class='imgInfo'>" + heading + "</h5><hr/>";

            if (report_for == "fulltime" || instanceID == "ae5eb924-def7-461e-8097-c503ec893bfc")
            {
                string absences_url = atd.WebURL("absences", Request.QueryString["id"].ToString(), instanceID, Request.QueryString["e"].Decrypt(), Request.QueryString["p"].Decrypt(), id.ToString());
                string unmarked_url = atd.WebURL("unmarked", userId.ToString(), instanceID, Request.QueryString["e"].Decrypt(), Request.QueryString["p"].Decrypt(), id.ToString(), stats["startDate"].ToString(), stats["endDate"].ToString());

                sum_table += "<span><b>Unmarked Days:  </b>" + stats["UnmarkedDays"] + " days <span " + c + ">(<a href='" + unmarked_url + "' target='_blank'>View</a>)</span><br></span>";
                sum_table += "<b>Absences:  </b>" + stats["Absences"] + " days <span " + c + ">(<a href='" + absences_url + "' target='_blank'>View</a>)</span><br>";

                if (instanceID == "ae5eb924-def7-461e-8097-c503ec893bfc")
                {
                    string adjustments_url = atd.WebURL("adjustments", Request.QueryString["id"].ToString(), instanceID, Request.QueryString["e"].Decrypt(), Request.QueryString["p"].Decrypt(), id.ToString());
                    sum_table += "<b>Adjustments:  </b>" + stats["Adjustments"] + " days <span " + c + ">(<a href='" + adjustments_url + "' target='_blank'>View</a>)</span><br>";
                }

                if (stats["AdvanceSttings"].ToString() == "1" || instanceID == "ae5eb924-def7-461e-8097-c503ec893bfc") sum_table += "<b>Worked Hours should be:</b>" + stats["WorkingHours"] + "</br>";
                sum_table += "<b>Employee Worked: </b>" + stats["TotalWorkedHoursB"] + "</br>";

                if (stats["AdvanceSttings"].ToString() == "1" || hours > 0)
                {
                    if (!string.IsNullOrEmpty(stats["info"].ToString()))
                        stats["info"] = "<b>(" + stats["info"] + ")</b>";

                    sum_table += "<hr>";
                    if (stats["ExtraHours"].ToString() != "0")
                        sum_table += "<b>Balance Time:  </b><span style='color:green;'>" + stats["ExtraHours"].ToString().Replace('-', ' ') + " Overtime</span> " + stats["info"] + "<br>";
                    else if (stats["UnderHours"].ToString() != "0")
                        sum_table += "<b>Balance Time:  </b><span style='color:red;'>" + stats["UnderHours"].ToString().Replace('-', ' ') + " Undertime</span> " + stats["info"] + "<br>";
                    else
                        sum_table += "<b>Balance Time:  </b>0 hours " + stats["info"] + "<br><br>";
                }

                if (report_type != "yearly" || (userId == 11025 || userId == 9844 || userId == 9845 || userId == 14779)) //Allow Detail Report view to Sir Fahad & Avaima Developer & Test Avaima ID User & Haris Khan for Night Shift Instance
                {
                    sum_table += "<br/><b><a href='#' class='detailReport' onclick='detailreport()'>View Detail Report</a></b><br><br>";
                }
            }
            else
            {
                sum_table += "<b>Employee Worked: </b>" + stats["TotalWorkedHoursB"].ToString() + "<br><br>";
            }

            sum_table += "</div><br/>";

            result_table.Visible = true;
            advance_stats.InnerHtml = sum_table;
            advance_stats.Visible = true;
            DivTags.Visible = false;

            hdnstartDate.Value = stats["startDate"].ToString();
            hdnendDate.Value = stats["endDate"].ToString();
            hdnReportUserid.Value = stats["userid"].ToString();

            ScriptManager.RegisterStartupScript(this, GetType(), "end", "office_instance();managedUser();timeValidate();", true);
            #endregion
        }
        else if (ddlreports.SelectedValue == "2")
        {
            int year = (rbtnReportType.SelectedValue == "yearly") ? ddlYearly.SelectedValue.ToInt32() : ddlYear.SelectedValue.ToInt32();
            int month = ddlMonth.SelectedValue.ToInt32();
            DateTime user_joindate = SP.GetWorkerFirstSignInThisYear(id.ToString(), year);
            DateTime startDate = new DateTime(year, 1, 1, 0, 0, 0);
            DateTime endDate = new DateTime();
            DateTime currenttime = Convert.ToDateTime(App.GetTime(DateTime.UtcNow.TimeOfDay.ToString(), Appid));
            string heading = "Yearly Report: " + year;
            if (startDate < currenttime)
            {
                string report_type = rbtnReportType.SelectedValue;
                if (report_type == "monthly")
                {
                    heading = "Monthly Report: " + ddlMonth.SelectedItem.Text + " " + ddlYear.SelectedItem.Text;
                }
                else if (report_type == "custom")
                {
                    heading = "Report: " + Convert.ToDateTime(dateTo.Text).ToString("dd MMM yyyy") + " - " + Convert.ToDateTime(dateFrom.Text).ToString("dd MMM yyyy");
                }


                if (report_type == "monthly")
                {
                    startDate = new DateTime(year, month, 1);

                    if (user_joindate > startDate && (startDate.Month == user_joindate.Month))
                    {
                        startDate = user_joindate;
                    }

                    if (startDate.Month == currenttime.Month)
                    {
                        if (startDate.Year == currenttime.Year)
                            endDate = new DateTime(currenttime.Year, currenttime.Month, currenttime.Day, 23, 59, 59);
                        else
                            endDate = new DateTime(startDate.Year, startDate.Month, DateTime.DaysInMonth(startDate.Year, startDate.Month), 23, 59, 59);
                    }
                    else
                    {
                        if (user_joindate > startDate && (startDate.Month == user_joindate.Month))
                        {
                            endDate = new DateTime(currenttime.Year, startDate.Month, DateTime.DaysInMonth(year, user_joindate.Month), 23, 59, 59);
                        }
                        else
                            //endDate = startDate.AddMonths(1).AddDays(-1);
                            endDate = new DateTime(startDate.Year, startDate.Month, DateTime.DaysInMonth(year, startDate.Month), 23, 59, 59);

                    }
                    //endDate = startDate.AddMonths(1).AddDays(-1);
                    //endDate = new DateTime(currenttime.Year, startDate.Month, DateTime.DaysInMonth(year, user_joindate.Month), 23, 59, 59);
                }
                else if (report_type == "yearly")
                {
                    startDate = SP.GetWorkerFirstSignInThisYear(id.ToString(), year);
                    endDate = SP.GetWorkerLastSignInThisYear(id.ToString(), year);
                }

                StringBuilder objstr = new StringBuilder();
                string query = rbtntagReport.SelectedValue;
                DataTable dt = FillTagTable(startDate, endDate, id, query);

                if (dt.Rows.Count > 0)
                {
                    lbltagreport.InnerHtml = heading;

                    objstr.Append(@"<table class=""smallGrid""><tr class=""smallGridHead""><td>Tags</td><td>Counts</td>");
                    if (query == "TagDayWiseReport")
                        objstr.Append(@"<td>Date</td>");
                    objstr.Append("</tr>");
                    foreach (DataRow drrow in dt.Rows)
                    {
                        objstr.Append(@"<tr class=""smallGrid""><td >" + drrow["Tags"] + "</td ><td >" + drrow["num"] + "</td >");
                        if (query == "TagDayWiseReport")
                            objstr.Append(@"<td>" + Convert.ToDateTime(drrow["lastin"]).ToString("MMM dd, yyyy") + "</td>");
                        objstr.Append("</tr>");
                    }
                    objstr.Append(@"</table>");
                    if (report_type != "yearly" || (userId == 11025 || userId == 9844 || userId == 9845 || userId == 14779)) //Allow Detail Report view to Sir Fahad & Avaima Developer & Test Avaima ID User & Haris Khan for Night Shift Insta
                    {
                        objstr.Append("<br/><b><a href='#' class='detailReport' onclick='detailTagReport()'>View Detail Report</a></b><br><br>");
                    }
                    divtagreportdetail.InnerHtml = objstr.ToString();
                }
                else
                {
                    divtagreportdetail.InnerHtml = "";
                }

                result_table.Visible = false;
                DivTags.Visible = true;

                hdnstartDate.Value = startDate.ToString();
                hdnendDate.Value = endDate.ToString();
                hdnReportUserid.Value = id.ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "end", "office_instance();managedUser();timeValidate();", true);

            }
        }

    }


    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        LinkButton clickedButton = (LinkButton)sender;

        int id = ddlUsers.SelectedValue.ToInt32();

        ddlUsers.Items.Clear();
        LoadUsers();
        ddlUsers.SelectedValue = id.ToString();

        string parent = "";
        int parentid = ddlUsers.SelectedItem.Attributes["parentid"].ToInt32();
        if (parentid == 0)
        {
            parentid = userId;
        }

        if (parentid != userId)
        {
            parent = "class='hide'";
        }

        int hours = ddlHours.SelectedValue.ToInt32() + Math.Abs((ddlMinutes.SelectedValue != "0") ? (ddlMinutes.SelectedValue.ToInt32() / 60) : 0);
        decimal breakHr = ddlBrkHour.SelectedValue.ToInt32() + Math.Abs((ddlBrkMinutes.SelectedValue != "0") ? (ddlBrkMinutes.SelectedValue.ToInt32() / 60) : 0);
        string additionalbreak = ddlAdditionalBrk.SelectedValue;
        decimal additionalbreakHr = ddlAdditionalBrkHr.SelectedValue.ToInt32() + Math.Abs((ddlAdditionalBrkMin.SelectedValue != "0") ? (ddlAdditionalBrkMin.SelectedValue.ToInt32() / 60) : 0);
        string StrtHr = txtStartHour.Text;
        string EndHr = txtEndHour.Text;
        int TotalAbsences = 15;
        //  string Appid = "648dec41-d961-4a78-a615-d9aac688c3ad";
        string Appid = HttpContext.Current.User.Identity.Name;

        if (clickedButton.ClientID == "head_btnGenerateReport")
        {
            Hashtable ht = new Hashtable();
            ht["@userid"] = userId;
            ht["@working_hours"] = ddlHours.SelectedValue + ',' + ddlMinutes.SelectedValue;
            ht["@startTime"] = StrtHr;
            ht["@endTime"] = EndHr;
            ht["@breakTime"] = ddlBrkHour.SelectedValue + ',' + ddlBrkMinutes.SelectedValue;
            ht["@additional_break"] = ddlAdditionalBrk.SelectedValue;
            ht["@additional_breakhours"] = ddlAdditionalBrkHr.SelectedValue + ',' + ddlAdditionalBrkMin.SelectedValue;
            ht["@additional_breakday"] = (ddlAdditionalBrk.SelectedValue == "week") ? ddlAdditionalBrkDay.SelectedValue : "";
            ht["@CreatedDate"] = DateTime.Now.ToString();
            ht["@ModifiedDate"] = DateTime.Now.ToString();
            ht["@action"] = hdntype.Value;

            //ReportSettings(ht);
        }

        string report_type = rbtnReportType.SelectedValue;
        int month = ddlMonth.SelectedValue.ToInt32();
        int year = (rbtnReportType.SelectedValue == "yearly") ? DateTime.Now.Year : ddlYear.SelectedValue.ToInt32();

        string report_for = rbtnReportFor.SelectedValue;

        //string report_type = (rbtnYearly.Checked) ? "yearly" : "monthly";
        //int month = ddlMonth.SelectedValue.ToInt32();
        //int year = (rbtnYearly.Checked) ? DateTime.Now.Year : ddlYear.SelectedValue.ToInt32();


        string heading = "Yearly Statistics: " + year;
        if (report_type == "monthly")
            heading = "Monthly Statistics: " + ddlMonth.SelectedItem.Text + " " + ddlYear.SelectedItem.Text;
        else if (report_type == "custom")
            heading = "Statistics: " + Convert.ToDateTime(dateTo.Text).ToString("dd MMM yyyy") + " - " + Convert.ToDateTime(dateFrom.Text).ToString("dd MMM yyyy");

        Hashtable stats = JsonConvert.DeserializeObject<Hashtable>(atd.AdvanceCalculateStatistics(parentid, hours, year, month, id, instanceID, TotalAbsences, breakHr, StrtHr, EndHr, Appid, report_type, additionalbreak, additionalbreakHr, dateTo.Text, dateFrom.Text));

        //Managed User
        string c = "";
        if (id == userId)
        {
            if (!atd.UserManaged(userId.ToInt32(), instanceID, "get"))
            {
                c = "class='hide'";
            }
        }

        string sum_table = "<div id='reportDiv'><h4>" + heading;
        sum_table += "<a href='#' onclick='return ApplyFilter(this);' title ='Advance Calculator' class='btn-edit office'><i class='fa fa-fw fa-calculator'></i></a></h4>";

        if (report_for == "fulltime")
        {
            //string absences_url = atd.WebURL("absences", id.ToString(), instanceID, Request.QueryString["e"].Decrypt(), Request.QueryString["p"].Decrypt()); ;
            string absences_url = atd.WebURL("absences", Request.QueryString["id"].ToString(), instanceID, Request.QueryString["e"].Decrypt(), Request.QueryString["p"].Decrypt(), id.ToString());
            string unmarked_url = atd.WebURL("unmarked", userId.ToString(), instanceID, Request.QueryString["e"].Decrypt(), Request.QueryString["p"].Decrypt(), id.ToString(), stats["startDate"].ToString(), stats["endDate"].ToString());

            sum_table += "<span " + parent + "'><b>Unmarked Days:  </b>" + stats["UnmarkedDays"] + " days <span " + c + ">(<a href='" + unmarked_url + "' target='_blank'>View</a>)</span><br></span>";
            sum_table += "<b>Absences:  </b>" + stats["Absences"] + " days <span " + c + ">(<a href='" + absences_url + "' target='_blank'>View</a>)</span><br>";
            sum_table += "<b>Worked Hours should be:</b></td><td>" + stats["WorkingHours"].ToString() + " hours <span class='office'> <span class='hourperday'><b>(<span class='updatedhourperday'>" + ddlHours.SelectedValue + " hours " + ddlMinutes.SelectedValue + " minutes</span> /day)</b>";
            sum_table += "<a href='#' onclick='showhide(\"show\")' title ='Edit hours per day' " + c + "><i class='fa fa-fw fa-pencil'></i></a></span></span>";
            sum_table += "<span class='addhourperday hide'><input type='text' id='txthourperday' placeholder='Hours' maxlength='2'>&nbsp;<input type='text' id='txtminperday' placeholder='Minutes'  maxlength='2'><a href='#' onclick='showhide(\"hide\")' title ='Update'><i class='fa fa-fw fa-remove'></i></a></span><br>";
            sum_table += "<b>Employee Worked: </b></td><td>" + stats["TotalWorkedHoursB"].ToString() + " <span class='office'><b>(<span class='updatedhourperday'>" + ddlHours.SelectedValue + " hours " + ddlMinutes.SelectedValue + " minutes</span> /day)</b></span>";
            //if (instanceID == "f425f912-8d79-4415-ba72-89a5309acb10" || id == 10192)
            //{
            //    sum_table += "<br/><b>UnderTime: </b> " + stats["UnderTime"].ToString() + "<br>";
            //}
            sum_table += "<hr>";
            if (stats["ExtraHours"].ToString() != "0")
                sum_table += "<b>Balance Time:  </b><span style='color:green;'>" + stats["ExtraHours"].ToString() + " Overtime</span>" + "<br>";
            else if (stats["UnderHours"].ToString() != "0")
                sum_table += "<b>Balance Time:  </b><span style='color:red;'>" + stats["UnderHours"].ToString() + " Undertime</span>" + "<br>";
            else
                sum_table += "<b>Balance Time:  </b>0 hours" + "<br><br>";

            if (report_type != "yearly")
            {
                sum_table += "<b><a href='#' class='detailReport' onclick='detailreport()'>View Detail Report</a></b><br><br>";
            }
        }
        else
        {
            sum_table += "<b>Employee Worked: </b></td><td>" + stats["TotalWorkedHoursB"].ToString() + " <b>(" + ddlHours.SelectedValue + " (hours-minutes)/day)</b><br><br>";
        }

        sum_table += "</div>";

        result_table.Visible = true;
        advance_stats.InnerHtml = sum_table;
        advance_stats.Visible = true;
        //ht["startDate"] = startDate;
        //ht["endDate"] = endDate;
        //ht["userid"] = userid;
        hdnstartDate.Value = stats["startDate"].ToString();
        hdnendDate.Value = stats["endDate"].ToString();
        hdnReportUserid.Value = stats["userid"].ToString();

        ScriptManager.RegisterStartupScript(this, GetType(), "", "office_instance();managedUser();timeValidate();  $('#detail_stats').hide();", true);


        // detail_stats.InnerHtml = stats["DetailReport"].ToString();

        //int id = ddlUsers.SelectedValue.ToInt32();
        //int hours = ddlHours.SelectedValue.ToInt32();
        //int year = DateTime.Now.Year;
        //int breakHr = ddlBrkHour.SelectedValue.ToInt32();
        //string StrtHr = txtStartHour.Text;
        //string EndHr = txtEndHour.Text;
        ////string Appid = "648dec41-d961-4a78-a615-d9aac688c3ad";
        //string Appid = HttpContext.Current.User.Identity.Name;

        //Hashtable stats = JsonConvert.DeserializeObject<Hashtable>(atd.CalculateStatistics(userId,hours, year, id, instanceID, breakHr, StrtHr, EndHr, Appid));

        //string table = "<div id='reportDiv' ><h5 style='text-align: center;font-weight: bold;'>Report</h5><table class='table table-responsive table-sm'>";
        //table += "<tr><td><b>Absences:  </b></td><td>" + stats["TotalAbsences"].ToString() + "</td></tr>";
        //table += "<tr ><td rowspan='2'><b>Worked Days: </b> </td><td> With break: " + stats["TotalDaysWorked"].ToString() + " (Should be: " + stats["WorkingDays"].ToString() + " days)</td><tr/>";
        //table += "<tr><td></td><td>Without break: " + stats["TotalDaysWorkedB"].ToString() + " (Should be: " + stats["WorkingDaysB"].ToString() + " days)</td></tr>";
        //table += "<tr><td rowspan='2'><b>Worked Hours: </b> </td><td> With break: " + stats["TotalWorkedHours"].ToString() + " (Should be: " + stats["WorkingHours"].ToString() + " hours)</td></tr>";
        //table += "<tr></tr>";
        //table += "<tr><td></td><td>Without break: " + stats["TotalWorkedHoursB"].ToString() + " (Should be: " + stats["WorkingHoursB"].ToString() + " hours)</td></tr>";
        //table += "<tr><td><b>Extra Worked Hours: </b> </td><td>" + stats["ExtraHours"].ToString() + "</td></tr>";
        //table += "<tr><td><b>Under Worked Hours: </b> </td><td>" + stats["UnderHours"].ToString() + "</td></tr>";
        //table += "</table></div>";

        //result.InnerHtml = table;

        //ScriptManager.RegisterStartupScript(this, GetType(), "", "    $('html, body').animate({ scrollTop: $('#reportDiv').offset().top}, 2000);", true);

    }

    [WebMethod]
    public static string DetailReport(int userid, DateTime startDate, DateTime endDate, string Appid, string ParentId, DateTime dateTo, string isworkdefine, string instanceid)
    {
        Attendance atd = new Attendance();
        DataTable dtHistory = new DataTable();
        dtHistory.Columns.Add("DateTime", typeof(DateTime));
        dtHistory.Columns.Add("Day");
        dtHistory.Columns.Add("Clock-in");
        dtHistory.Columns.Add("Clock-out");

        DataRow dr = null;
        DateTime user_joindate = SP.GetWorkerFirstSignInThisYear(userid.ToString(), DateTime.Now.Year);

        string text = "";
        string table = "";
        string extra_head = "";
        string colspan_table = "3";
        string colspan_full = "5";
        bool check = false;
        int parentid = atd.getParent(userid.ToString()).ToInt32();
        DataTable workingHours = new DataTable();
        DataRow tempRow = null;

        if (isworkdefine == "True")
        {
            dtHistory.Columns.Add("Break Time");
            //text = "(Break excluded)";
            table = "<th>Break Time</th>";

            if (ParentId == "9844")
            {
                extra_head = "<th>Hours</th><th>Min</th>";
                colspan_table = "6";
                colspan_full = "8";
            }
            else
            {
                colspan_table = "4";
                colspan_full = "6";
            }
            check = true;
            workingHours = atd.GetWorkingHours(parentid);
        }

        dtHistory.Columns.Add("Time Spent");
        dtHistory.Columns.Add("totalminutes");

        dtHistory.Merge(atd.FormatDataTable(atd.GetYearlyAttendance(userid, startDate, endDate), "attendance", Appid, instanceid));
        if (instanceid == "ae5eb924-def7-461e-8097-c503ec893bfc")
            dtHistory.Merge(atd.FormatDataTable(atd.GetAllYearlyAbsences(userid, startDate, endDate), "absence", Appid));
        else
            dtHistory.Merge(atd.FormatDataTable(atd.GetYearlyAbsences(userid, startDate, endDate), "absence", Appid));

        string historyTable = "<h5 class='detail_heading'>Detailed Report: " + startDate.ToString("dd MMM yyyy") + " to " + endDate.ToString("dd MMM yyyy") + "</h5>";
        if (startDate < user_joindate)
            historyTable += "<p>(Time clocked from: " + user_joindate.ToString("dd MMM yyyy") + ")</p>";

        historyTable += "<table class='table table-bordered table-hover table-history' >";
        historyTable += "<thead><tr class='headerRow'><th>Day</th><th>Date</th><th>Clock-in</th><th>Clock-out</th>" + table + "<th>Worked " + text + "</th>" + extra_head + "</tr></thead><tbody>";

        //UnMarked Days
        string UnMarkedDates = atd.GetUnmarkedDays(dtHistory, startDate, endDate, atd.getParent(ParentId).ToInt32(), endDate.Year);

        if (UnMarkedDates != "" && (startDate >= user_joindate))
        {
            foreach (string days in UnMarkedDates.Split(','))
            {
                dr = dtHistory.NewRow();
                dr["DateTime"] = Convert.ToDateTime(days).ToString("dd-MMM-yyyy");
                dr["Day"] = Convert.ToDateTime(days).ToString("dd-MMM-yyyy");
                dr["Clock-in"] = "unmarked";
                dr["Clock-out"] = "";
                if (check)
                    dr["Break Time"] = "";
                dr["Time Spent"] = "";
                dr["totalminutes"] = "0";
                dtHistory.Rows.Add(dr);
            }
        }

        DataView view = dtHistory.DefaultView;
        view.Sort = "DateTime ASC";
        dtHistory = view.ToTable();

        if (dtHistory.Rows.Count > 0)
        {
            TimeSpan total_timespent = new TimeSpan();
            TimeSpan total_break = new TimeSpan();
            string Last_row = "";
            Last_row += "<tr style='font-weight: bold;'><td colspan='4'>Total</td>";
            List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(atd.getParent(userid.ToString()).ToInt32(), instanceid);

            foreach (DataRow row in dtHistory.Rows)
            {
                string day = Convert.ToDateTime(row["Day"]).ToString("dddd");

                if (row["Clock-in"].ToString() == "absent")
                {
                    string abtext = "Absent: " + row["Clock-out"];
                    if (row["Clock-out"].ToString().Contains("Adjustment:"))
                        abtext = row["Clock-out"].ToString();

                    historyTable += "<tr class='absentRow'><td>" + day + "</td><td>" + row["Day"] + "</td><td colspan='" + colspan_table + "'>" + abtext + "</td></tr>";
                }
                else if (row["Clock-in"].ToString() == "unmarked")
                {
                    historyTable += "<tr class='unmarkedRow'><td>" + day + "</td><td>" + row["Day"] + "</td><td colspan='" + colspan_table + "'>Unmarked Day</td></tr>";
                }
                else
                {
                    if (check)
                    {
                        if (workingHours.Rows.Count > 0)
                        {
                            tempRow = workingHours.Select("DayTitle = '" + day + "'").FirstOrDefault();
                            List<ExceptionalDay> exDay = exDays.Where(u => u.Date.Date.ToShortDateString() == Convert.ToDateTime(row["Day"]).ToShortDateString()).ToList();

                            if (tempRow != null && exDay.Count == 0)
                            {
                                DateTime bStart = Convert.ToDateTime(tempRow["BreakStart"]);
                                DateTime bEnd = Convert.ToDateTime(tempRow["BreakEnd"]);
                                DateTime clockin = Convert.ToDateTime(row["Clock-in"]);
                                DateTime clockout = DateTime.UtcNow;
                                string breakDuration = "";

                                if (row["Clock-out"].ToString() != " Still Clocked-in ")
                                {
                                    clockout = Convert.ToDateTime(row["Clock-out"]);
                                    //if ((bStart.TimeOfDay >= clockin.TimeOfDay && bStart.TimeOfDay <= clockout.TimeOfDay) && (bEnd.TimeOfDay >= clockin.TimeOfDay && bEnd.TimeOfDay <= clockout.TimeOfDay))
                                    //if ((bStart.TimeOfDay >= clockin.TimeOfDay && bEnd.TimeOfDay <= clockout.TimeOfDay))
                                    //{
                                    //    breakDuration = atd.ConvertTimeSpan(bEnd.Subtract(bStart));
                                    //    total_break += bEnd.Subtract(bStart);

                                    //    historyTable += "<tr><td>" + day + "</td><td>" + row["Day"] + "</td><td>" + row["Clock-in"] + "</td><td>" + row["Clock-out"] + "</td><td>" + Convert.ToDateTime(tempRow["BreakStart"]).ToShortTimeString() + " to " + Convert.ToDateTime(tempRow["BreakEnd"]).ToShortTimeString() + " (<b>" + breakDuration + "</b>)</td><td>" + row["Time Spent"] + "</td></tr>";
                                    //}
                                    //else if ((bStart.TimeOfDay <= clockin.TimeOfDay && bEnd.TimeOfDay >= clockin.TimeOfDay))
                                    //{
                                    //    breakDuration = atd.ConvertTimeSpan(bEnd.TimeOfDay.Subtract(clockin.TimeOfDay));
                                    //    total_break += bEnd.TimeOfDay.Subtract(clockin.TimeOfDay);

                                    //    historyTable += "<tr><td>" + day + "</td><td>" + row["Day"] + "</td><td>" + row["Clock-in"] + "</td><td>" + row["Clock-out"] + "</td><td>" + clockin.ToShortTimeString() + " to " + Convert.ToDateTime(tempRow["BreakEnd"]).ToShortTimeString() + " (<b>" + breakDuration + "</b>)</td><td>" + row["Time Spent"] + "</td></tr>";
                                    //}
                                    //else
                                    //    historyTable += "<tr><td>" + day + "</td><td>" + row["Day"] + "</td><td>" + row["Clock-in"] + "</td><td>" + row["Clock-out"] + "</td><td><b>Break not included</b></td><td>" + row["Time Spent"] + "</td></tr>";


                                    //9AM - 6PM
                                    if (bStart.TimeOfDay >= clockin.TimeOfDay && bEnd.TimeOfDay <= clockout.TimeOfDay)
                                    {
                                        breakDuration = atd.ConvertTimeSpan(bEnd.Subtract(bStart));
                                        total_break += bEnd.Subtract(bStart);

                                        historyTable += "<tr><td>" + day + "</td><td>" + row["Day"] + "</td><td>" + row["Clock-in"] + "</td><td>" + row["Clock-out"] + "</td><td>" + Convert.ToDateTime(tempRow["BreakStart"]).ToShortTimeString() + " to " + Convert.ToDateTime(tempRow["BreakEnd"]).ToShortTimeString() + " (<b>" + breakDuration + "</b>)</td><td>" + row["Time Spent"] + "</td>";
                                    }
                                    //9AM - 1.30PM
                                    else if (bStart.TimeOfDay >= clockin.TimeOfDay && (bStart.TimeOfDay <= clockout.TimeOfDay && bEnd.TimeOfDay >= clockout.TimeOfDay))
                                    {
                                        breakDuration = atd.ConvertTimeSpan(clockout.TimeOfDay.Subtract(bStart.TimeOfDay));
                                        total_break += clockout.TimeOfDay.Subtract(bStart.TimeOfDay);

                                        historyTable += "<tr><td>" + day + "</td><td>" + row["Day"] + "</td><td>" + row["Clock-in"] + "</td><td>" + row["Clock-out"] + "</td><td>" + Convert.ToDateTime(tempRow["BreakStart"]).ToShortTimeString() + " to " + clockout.ToShortTimeString() + " (<b>" + breakDuration + "</b>)</td><td>" + row["Time Spent"] + "</td>";
                                    }
                                    //1PM - 6PM
                                    else if ((bStart.TimeOfDay <= clockin.TimeOfDay && bEnd.TimeOfDay >= clockin.TimeOfDay) && bEnd.TimeOfDay <= clockout.TimeOfDay)
                                    {
                                        breakDuration = atd.ConvertTimeSpan(bEnd.TimeOfDay.Subtract(clockin.TimeOfDay));
                                        total_break += bEnd.TimeOfDay.Subtract(clockin.TimeOfDay);

                                        historyTable += "<tr><td>" + day + "</td><td>" + row["Day"] + "</td><td>" + row["Clock-in"] + "</td><td>" + row["Clock-out"] + "</td><td>" + clockin.ToShortTimeString() + " to " + Convert.ToDateTime(tempRow["BreakEnd"]).ToShortTimeString() + " (<b>" + breakDuration + "</b>)</td><td>" + row["Time Spent"] + "</td>";
                                    }
                                    //1PM - 2PM
                                    else if (bStart.TimeOfDay <= clockin.TimeOfDay && bEnd.TimeOfDay >= clockout.TimeOfDay)
                                    {
                                        breakDuration = atd.ConvertTimeSpan(clockout.TimeOfDay.Subtract(clockin.TimeOfDay));
                                        total_break += clockout.TimeOfDay.Subtract(clockin.TimeOfDay);

                                        historyTable += "<tr><td>" + day + "</td><td>" + row["Day"] + "</td><td>" + row["Clock-in"] + "</td><td>" + row["Clock-out"] + "</td><td>" + clockin.ToShortTimeString() + " to " + clockout.ToShortTimeString() + " (<b>" + breakDuration + "</b>)</td><td>" + row["Time Spent"] + "</td>";
                                    }
                                    else
                                        historyTable += "<tr><td>" + day + "</td><td>" + row["Day"] + "</td><td>" + row["Clock-in"] + "</td><td>" + row["Clock-out"] + "</td><td><b>Break not included</b></td><td>" + row["Time Spent"] + "</td>";

                                }
                                else
                                    historyTable += "<tr><td>" + day + "</td><td>" + row["Day"] + "</td><td>" + row["Clock-in"] + "</td><td>" + row["Clock-out"] + "</td><td><b>Break not included</b></td><td>" + row["Time Spent"] + "</td>";

                            }
                            else
                            {
                                if (exDay.Count != 0)
                                {
                                    historyTable += "<tr><td>" + day + "</td><td>" + row["Day"] + "</td><td>" + row["Clock-in"] + "</td><td>" + row["Clock-out"] + "</td><td><b>" + exDay[0].GroupName.ToString() + "</b></td><td>" + row["Time Spent"] + "</td>";
                                }
                                else
                                    historyTable += "<tr><td>" + day + "</td><td>" + row["Day"] + "</td><td>" + row["Clock-in"] + "</td><td>" + row["Clock-out"] + "</td><td><b>Break not included</b></td><td>" + row["Time Spent"] + "</td>";

                            }
                            //historyTable += "<tr><td>" + day + "</td><td>" + row["Day"] + "</td><td>" + row["Clock-in"] + "</td><td>" + row["Clock-out"] + "</td><td>" + Convert.ToDateTime(tempRow["BreakStart"]).ToShortTimeString() + " to " + Convert.ToDateTime(tempRow["BreakEnd"]).ToShortTimeString() + "</td><td>" + row["Time Spent"] + "</td></tr>";
                        }
                    }
                    else
                        historyTable += "<tr><td>" + day + "</td><td>" + row["Day"] + "</td><td>" + row["Clock-in"] + "</td><td>" + row["Clock-out"] + "</td><td>" + row["Time Spent"] + "</td>";

                    string extra_work = "";
                    if (ParentId == "9844")
                    {
                        if (row["Time Spent"].ToString().Contains(" Hours") || row["Time Spent"].ToString().Contains(" hrs"))
                            extra_work += "<td>" + row["Time Spent"].ToString().Split(' ')[1] + "</td>";
                        else
                            extra_work += "<td>0</td>";

                        if (row["Time Spent"].ToString().Contains(" Minutes") || row["Time Spent"].ToString().Contains(" mins"))
                        {
                            if ((row["Time Spent"].ToString().Contains(" Hours") || row["Time Spent"].ToString().Contains(" hrs")) && row["Time Spent"].ToString().Split(' ').Count() > 3)
                                extra_work += "<td>" + row["Time Spent"].ToString().Split(' ')[3] + "</td>";
                            else if (row["Time Spent"].ToString().Contains(" Minutes") || row["Time Spent"].ToString().Contains(" mins"))
                                extra_work += "<td>" + row["Time Spent"].ToString().Split(' ')[1] + "</td>";
                            else
                                extra_work += "<td>0</td>";
                        }
                        else
                            extra_work += "<td>0</td>";

                        historyTable += extra_work;
                    }

                    historyTable += "</tr>";

                    total_timespent += TimeSpan.FromMinutes(Convert.ToDouble(row["totalminutes"]));
                }
            }

            if (check)
                Last_row += "<td>" + atd.GetTotalHours(total_break) + " Hours " + ((total_break.Minutes != 0) ? total_break.Minutes + " Mins" : "") + "</td>";

            Last_row += "<td>" + atd.GetTotalHours(total_timespent) + " Hours " + ((total_timespent.Minutes != 0) ? total_timespent.Minutes + " Mins" : "") + "</td>";
            Last_row += "</tr>";

            historyTable += Last_row;
        }
        else
        {
            historyTable += "<tr class='noentryRow'><td colspan='" + colspan_full + "'> No entry available</td></tr>";
        }

        historyTable += "</tbody></table>";

        //TEST
        //historyTable = atd.WorkingHoursShouldBe_table(startDate,endDate,parentid,userid,15, "f425f912-8d79-4415-ba72-89a5309acb10", 8);

        return JsonConvert.SerializeObject(historyTable);
    }

    [WebMethod]
    public static string DetailReportTags(int userid, DateTime startDate, DateTime endDate, string Appid, string ParentId, DateTime dateTo, string isworkdefine, string instanceid)
    {
        Attendance atd = new Attendance();
        DataTable dtHistory = new DataTable();
        dtHistory.Columns.Add("DateTime", typeof(DateTime));
        dtHistory.Columns.Add("Day");
        dtHistory.Columns.Add("Clock-in");
        dtHistory.Columns.Add("Clock-out");

        DataRow dr = null;
        DateTime user_joindate = SP.GetWorkerFirstSignInThisYear(userid.ToString(), DateTime.Now.Year);

        string text = "";
        string table = "";
        string extra_head = "";
        string colspan_table = "3";
        string colspan_full = "5";
        bool check = false;
        int parentid = atd.getParent(userid.ToString()).ToInt32();
        DataTable workingHours = new DataTable();
        DataRow tempRow = null;

        if (isworkdefine == "True")
        {
            dtHistory.Columns.Add("Tags");
            //text = "(Break excluded)";
            table = "<th>Tag</th>";

            if (ParentId == "9844")
            {
                extra_head = "<th>Hours</th><th>Min</th>";
                colspan_table = "6";
                colspan_full = "8";
            }
            else
            {
                colspan_table = "4";
                colspan_full = "6";
            }
            check = true;
            workingHours = atd.GetWorkingHours(parentid);
        }

        dtHistory.Columns.Add("Time Spent");
        dtHistory.Columns.Add("totalminutes");

        dtHistory.Merge(atd.FormatDataTagTable(atd.GetYearlyAttendance("sp_GetYearlyHistoryTagReport", userid, startDate, endDate), "attendance", Appid, instanceid));

        string historyTable = "<h5 class='detail_heading'>Detailed Report: " + startDate.ToString("dd MMM yyyy") + " to " + endDate.ToString("dd MMM yyyy") + "</h5>";
        if (startDate < user_joindate)
            historyTable += "<p>(Time clocked from: " + user_joindate.ToString("dd MMM yyyy") + ")</p>";

        historyTable += "<table class='table table-bordered table-hover table-history' >";
        historyTable += "<thead><tr class='headerRow'><th>Day</th><th>Date</th><th>Clock-in</th><th>Clock-out</th>" + table + "<th>Worked " + text + "</th>" + extra_head + "</tr></thead><tbody>";

        DataView view = dtHistory.DefaultView;
        view.Sort = "DateTime ASC";
        dtHistory = view.ToTable();

        if (dtHistory.Rows.Count > 0)
        {
            TimeSpan total_timespent = new TimeSpan();
            foreach (DataRow row in dtHistory.Rows)
            {
                string day = Convert.ToDateTime(row["Day"]).ToString("dddd");


                if (check)
                {
                    if (workingHours.Rows.Count > 0)
                    {
                        tempRow = workingHours.Select("DayTitle = '" + day + "'").FirstOrDefault();
                        DateTime clockin = Convert.ToDateTime(row["Clock-in"]);
                        DateTime clockout = DateTime.UtcNow;

                        if (row["Clock-out"].ToString() != " Still Clocked-in ")
                        {
                            clockout = Convert.ToDateTime(row["Clock-out"]);

                            historyTable += "<tr><td>" + day + "</td><td>" + row["Day"] + "</td><td>" + row["Clock-in"] + "</td><td>" + row["Clock-out"] + "</td><td>" + row["Tags"] + "</td><td>" + row["Time Spent"] + "</td>";

                        }
                        else
                            historyTable += "<tr><td>" + day + "</td><td>" + row["Day"] + "</td><td>" + row["Clock-in"] + "</td><td>" + row["Clock-out"] + "</td><td>" + row["Tags"] + "</td><td>" + row["Time Spent"] + "</td>";

                    }
                }
                else
                    historyTable += "<tr><td>" + day + "</td><td>" + row["Day"] + "</td><td>" + row["Clock-in"] + "</td><td>" + row["Clock-out"] + "</td><td>" + row["Time Spent"] + "</td>";

                string extra_work = "";
                if (ParentId == "9844")
                {
                    if (row["Time Spent"].ToString().Contains(" Hours") || row["Time Spent"].ToString().Contains(" hrs"))
                        extra_work += "<td>" + row["Time Spent"].ToString().Split(' ')[1] + "</td>";
                    else
                        extra_work += "<td>0</td>";

                    if (row["Time Spent"].ToString().Contains(" Minutes") || row["Time Spent"].ToString().Contains(" mins"))
                    {
                        if ((row["Time Spent"].ToString().Contains(" Hours") || row["Time Spent"].ToString().Contains(" hrs")) && row["Time Spent"].ToString().Split(' ').Count() > 3)
                            extra_work += "<td>" + row["Time Spent"].ToString().Split(' ')[3] + "</td>";
                        else if (row["Time Spent"].ToString().Contains(" Minutes") || row["Time Spent"].ToString().Contains(" mins"))
                            extra_work += "<td>" + row["Time Spent"].ToString().Split(' ')[1] + "</td>";
                        else
                            extra_work += "<td>0</td>";
                    }
                    else
                        extra_work += "<td>0</td>";

                    historyTable += extra_work;
                }

                historyTable += "</tr>";

                total_timespent += TimeSpan.FromMinutes(Convert.ToDouble(row["totalminutes"]));
            }

            //if (check)
            //    Last_row += "<td>" + atd.GetTotalHours(total_break) + " Hours " + ((total_break.Minutes != 0) ? total_break.Minutes + " Mins" : "") + "</td>";

            //Last_row += "<td>" + atd.GetTotalHours(total_timespent) + " Hours " + ((total_timespent.Minutes != 0) ? total_timespent.Minutes + " Mins" : "") + "</td>";
            //Last_row += "</tr>";

            //historyTable += Last_row;
        }
        else
        {
            historyTable += "<tr class='noentryRow'><td colspan='" + colspan_full + "'> No entry available</td></tr>";
        }

        historyTable += "</tbody></table>";

        //TEST
        //historyTable = atd.WorkingHoursShouldBe_table(startDate,endDate,parentid,userid,15, "f425f912-8d79-4415-ba72-89a5309acb10", 8);

        return JsonConvert.SerializeObject(historyTable);
    }

    public static string DetailReport_Old(int userid, DateTime startDate, DateTime endDate, string Appid, string ParentId, DateTime dateTo, string isworkdefine, string instanceid)
    {
        Attendance atd = new Attendance();
        DataTable dtHistory = new DataTable();
        dtHistory.Columns.Add("DateTime", typeof(DateTime));
        dtHistory.Columns.Add("Day");
        dtHistory.Columns.Add("Clock-in");
        dtHistory.Columns.Add("Clock-out");
        dtHistory.Columns.Add("Time Spent");
        DataRow dr = null;
        DateTime user_joindate = SP.GetWorkerFirstSignInThisYear(userid.ToString(), DateTime.Now.Year);


        //DataTable user_joindate = new DataTable();
        //using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        //{
        //    con.Open();
        //    using (SqlCommand cmd = new SqlCommand("Select top 1 * from schema_6e815b00_6e13_4839_a57d_800a92809f21.workertrans where p_pid=@userid", con))
        //    {
        //        cmd.Parameters.AddWithValue("@userid", userid);
        //        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        //        adp.Fill(user_joindate);
        //    }
        //}

        dtHistory.Merge(atd.FormatDataTable(atd.GetYearlyAttendance(userid, startDate, endDate), "attendance", Appid, instanceid));
        dtHistory.Merge(atd.FormatDataTable(atd.GetYearlyAbsences(userid, startDate, endDate), "absence", Appid));

        //DataTable dtAbsences = atd.FormatDataTable(atd.GetYearlyAbsences(userid, startDate, endDate), "absent", Appid);

        //AvaimaTimeZoneAPI atp = new AvaimaTimeZoneAPI();
        //string lastin;
        //string lastout;

        //string historyTable = "<h5>Detailed Report: " + dateTo.ToString("dd MMM yyyy") + " to " + endDate.ToString("dd MMM yyyy") + "</h5>";
        //if (Convert.ToDateTime(dateTo) < Convert.ToDateTime(user_joindate.Rows[0]["lastin"]))
        //    historyTable += "<p>(Time clocked from: "+ Convert.ToDateTime(user_joindate.Rows[0]["lastin"]).ToString("dd MMM yyyy") +")</p>";

        //string historyTable = "<h5>Detailed Report: " + startDate.ToString("dd MMM yyyy") + " to " + endDate.ToString("dd MMM yyyy") + "</h5>";
        //if (startDate < Convert.ToDateTime(user_joindate.Rows[0]["lastin"]))
        //    historyTable += "<p>(Time clocked from: " + Convert.ToDateTime(user_joindate.Rows[0]["lastin"]).ToString("dd MMM yyyy") + ")</p>";

        string historyTable = "<h5>Detailed Report: " + startDate.ToString("dd MMM yyyy") + " to " + endDate.ToString("dd MMM yyyy") + "</h5>";
        if (startDate < user_joindate)
            historyTable += "<p>(Time clocked from: " + user_joindate.ToString("dd MMM yyyy") + ")</p>";

        string text = (isworkdefine == "True") ? "(Break excluded)" : "";
        //historyTable += "<table class='table table-bordered table-hover table-responsive' >";
        historyTable += "<table class='table table-bordered table-hover' >";
        historyTable += "<thead><tr class='headerRow'><th>Day</th><th>Date</th><th>Clock-in</th><th>Clock-out</th><th>Time Spent " + text + "</th></th></thead><tbody>";

        ////Clocked-in/out days
        //if (dtAttendance.Rows.Count > 0)
        //{
        //    foreach (DataRow row in dtAttendance.Rows)
        //    {
        //        lastin = atp.GetTime(row["lastin"].ToString(), Appid);
        //        lastout = atp.GetTime(row["lastout"].ToString(), Appid);

        //        dr = dtHistory.NewRow();
        //        dr["Day"] = Convert.ToDateTime(row["FormatedDate"]).ToString("dd-MMM-yyyy");
        //        dr["Clock-in"] = Convert.ToDateTime(lastin).ToShortTimeString();
        //        dr["Clock-out"] = Convert.ToDateTime(lastout).ToShortTimeString();
        //        dr["Time Spent"] = row["hours"];
        //        dtHistory.Rows.Add(dr);
        //    }
        //}

        ////Absences
        //if (dtAbsences.Rows.Count > 0)
        //{
        //    foreach (DataRow row in dtAbsences.Rows)
        //    {
        //        dr = dtHistory.NewRow();
        //        dr["Day"] = Convert.ToDateTime(row["CrtDate"]).ToString("dd-MMM-yyyy");
        //        dr["Clock-in"] = "absent";
        //        dr["Clock-out"] = row["Comment"].ToString();
        //        dr["Time Spent"] = "";
        //        dtHistory.Rows.Add(dr);
        //    }
        //}

        //UnMarked Days
        string UnMarkedDates = atd.GetUnmarkedDays(dtHistory, startDate, endDate, atd.getParent(ParentId).ToInt32(), endDate.Year);

        if (UnMarkedDates != "" && (startDate >= user_joindate))
        {
            foreach (string days in UnMarkedDates.Split(','))
            {
                dr = dtHistory.NewRow();
                dr["DateTime"] = Convert.ToDateTime(days).ToString("dd-MMM-yyyy");
                dr["Day"] = Convert.ToDateTime(days).ToString("dd-MMM-yyyy");
                dr["Clock-in"] = "unmarked";
                dr["Clock-out"] = "";
                dr["Time Spent"] = "";
                dtHistory.Rows.Add(dr);
            }
        }

        DataView view = dtHistory.DefaultView;
        view.Sort = "DateTime ASC";
        dtHistory = view.ToTable();

        if (dtHistory.Rows.Count > 0)
        {
            foreach (DataRow row in dtHistory.Rows)
            {
                string day = Convert.ToDateTime(row["Day"]).ToString("dddd");

                if (row["Clock-in"].ToString() == "absent")
                {
                    historyTable += "<tr class='absentRow'><td>" + day + "</td><td>" + row["Day"] + "</td><td colspan='3'>Absent: " + row["Clock-out"] + "</td></tr>";
                }
                else if (row["Clock-in"].ToString() == "unmarked")
                {
                    historyTable += "<tr class='unmarkedRow'><td>" + day + "</td><td>" + row["Day"] + "</td><td colspan='3'>Unmarked Day</td></tr>";
                }
                else
                    historyTable += "<tr><td>" + day + "</td><td>" + row["Day"] + "</td><td>" + row["Clock-in"] + "</td><td>" + row["Clock-out"] + "</td><td>" + row["Time Spent"] + "</td></tr>";
            }
        }
        else
        {
            historyTable += "<tr class='noentryRow'><td colspan='5'> No entry available</td></tr>";
        }

        historyTable += "</tbody></table>";

        return JsonConvert.SerializeObject(historyTable);
    }

    protected void ddlUsers_SelectedIndexChanged(object sender, EventArgs e)
    {
        int user = ddlUsers.SelectedValue.ToInt32();

        //using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        //{
        //    using (SqlCommand cmd = new SqlCommand("Select * from tblholidays where userid = @id", con))
        //    {
        //        con.Open();
        //        cmd.Parameters.AddWithValue("@id", user);
        //        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        //        DataTable dt = new DataTable();
        //        adp.Fill(dt);

        //        if (dt.Rows.Count > 0)
        //        {
        //            rptHolidayWeekend.DataSource = dt;
        //            rptHolidayWeekend.DataBind();

        //            //rptHolidayWeekend.Visible = true;
        //            //lblText.Visible = false;

        //            lnkEmpSetting.Visible = true;
        //        }
        //        else
        //        {
        //            //rptHolidayWeekend.Visible = false;
        //            //lblText.Text = "Selected employee has no personal settings defined!";
        //            //lblText.Visible = true;

        //            lnkEmpSetting.Visible = false;
        //        }
        //    }
        //}
        advance_stats.Visible = false;
        lnkGenerateStats.Enabled = true;
        //GenerateStats("summarized_stats");

        //filter_tr.Attributes.Remove("display");
    }

    protected void rptHolidayWeekend_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label type = e.Item.FindControl("lblType") as Label;
            Label date = e.Item.FindControl("lblDate") as Label;

            if (type.Text == "0")
            {
                type.Text = "Weekend";
                date.Text = "weekly";
            }
            else
            {
                type.Text = "Holiday";
                date.Text = Convert.ToDateTime(date.Text).ToString("dd-MM-yyyy");
            }
        }

    }


    //protected void lnkEmpSetting_Click(object sender, EventArgs e)
    //{
    //    LinkButton btn = (LinkButton)sender;

    //    if (btn.Attributes["data-type"] == "view")
    //    {
    //        btn.Attributes["data-type"] = "hide";
    //        lnkEmpSetting.Text = "Close";
    //        rptHolidayWeekend.Visible = true;
    //    }
    //    else
    //    {
    //        btn.Attributes["data-type"] = "view";
    //        lnkEmpSetting.Text = "View employee personal settings";
    //        rptHolidayWeekend.Visible = false;
    //    }
    //}


    public void GetYearStats(int userid, int year, string instanceID)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            List<RecordModel> records = new List<RecordModel>();
            DateTime startDate = SP.GetWorkerFirstSignInThisYear(userid.ToString(), year);
            DateTime endDate = SP.GetWorkerLastSignInThisYear(userid.ToString(), year);
            #region Table Headers weekends
            List<RecordModel> recordList = new List<RecordModel>();
            List<DateRange> DateRanges = new List<DateRange>();
            #endregion
            List<Absence> absences = Absence.GetAbsences(Convert.ToInt32(userid)).Where(u => u.Active == true).ToList();
            List<WorkingHour> userWorkingHours = WorkingHour.GetWorkingHours(userid);
            if (userWorkingHours.Count <= 0)
            {
                userWorkingHours = WorkingHour.GetWorkingHours(0);
                userWorkingHours = userWorkingHours.Where(u => u.InstanceID == instanceID).ToList();
            }
            TimeSpan TotalOvertime = new TimeSpan(0);
            TimeSpan Totalundertime = new TimeSpan(0);
            TimeSpan tsTotalOvertime = new TimeSpan(0);
            TimeSpan tsTotalUndertime = new TimeSpan(0);
            TimeSpan tsworkingHoursShouldBe = new TimeSpan(0);
            TimeSpan tsworkingHours = new TimeSpan(0);
            TimeSpan tsbreakhours = new TimeSpan(0);
            TimeSpan tsbreakhoursShouldBe = new TimeSpan(0);
            TimeSpan tsOvertimeAuto = new TimeSpan(0);
            //TimeSpan tsWeekendWorkedHours = new TimeSpan(0);
            TimeSpan tsAbsenceHours = new TimeSpan(0);
            int totalAbs = 0;
            int totAbsDays = 0;
            int iDaysWorked = 0;

            if (startDate.Year <= year && endDate.Year >= year)
            {
                for (int i = startDate.Month; i <= endDate.Month; i++)
                {
                    DateTime qstartDate = new DateTime(year, i, 1);
                    DateTime qendDate = qstartDate.AddMonths(1).AddDays(-1);
                    using (SqlCommand cmd = new SqlCommand("getworkerrecordbydate", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@sDate", qstartDate.Date);
                        cmd.Parameters.AddWithValue("@eDate", qendDate.Date);
                        cmd.Parameters.AddWithValue("@Id", userid);
                        cmd.Parameters.AddWithValue("@InstanceId", instanceID);
                        DataSet ds = new DataSet();
                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        adp.Fill(ds);
                        records = UtilityMethods.getRecords(ds);
                        records = records.OrderBy(u => u.Date).ToList();
                        DateRanges.Add(new DateRange() { EndDate = qendDate.Date, StartDate = qstartDate.Date });

                        foreach (var record in records)
                        {
                            if (record.DayTitle != null && record.Records.Count > 0)
                            {
                                tsTotalOvertime += record.TotalOverTime;
                                ++iDaysWorked;
                            }
                            else if (record.Records.Count > 0 && record.DayTitle == null)
                            {
                                tsworkingHours += record.TotalWorkedHoursTS;
                                tsworkingHoursShouldBe += record.WorkingHours;
                                tsTotalOvertime += record.TotalOverTime;
                                tsTotalUndertime += record.TotalUnderTimeWithoutLate;
                            }
                        }
                    }
                }
                foreach (Absence absence in absences.Where(u => u.CrtDate.Year == year).ToList()) { totAbsDays += SP.GetTodayUserWorkHours(instanceID, userId, absence.CrtDate.Date); }
                totalAbs += absences.Where(u => u.CrtDate.Date.Year == year).ToList().Count;
            }

            tsAbsenceHours = new TimeSpan(totAbsDays, 0, 0);
            //Response.Write(tsOvertimeAuto.ToString() + "<br />");
            if (tsOvertimeAuto >= new TimeSpan(0))
            {
                TimeSpan tsOTAllTotal = tsTotalOvertime + tsOvertimeAuto;
                //lblTotalOvertimeR.ForeColor = System.Drawing.Color.Green;
                // Overtime including weekend and all
                //usmanlblTotalOvertimeR.Text = Math.Ceiling(Convert.ToDecimal(iDaysWorked)) + " weekend work days [" + UtilityMethods.getFormatedTimeByMinutes(tsOTAllTotal.TotalMinutes.ToInt32()) + "]";
                // lblTotalOvertimeR.Text = UtilityMethods.getFormatedTimeByMinutes(tsOTAllTotal.TotalMinutes.ToInt32());
                TotalOvertime = tsOTAllTotal;
            }
            else
            {
                TimeSpan tsOTAllTotal = tsTotalOvertime;
                //lblTotalOvertimeR.ForeColor = System.Drawing.Color.Green;
                // Overtime including weekend and all
                //usmanlblTotalOvertimeR.Text = Math.Ceiling(Convert.ToDecimal(iDaysWorked)) + " weekend work days [" + UtilityMethods.getFormatedTimeByMinutes(tsOTAllTotal.TotalMinutes.ToInt32()) + "]";
                //lblTotalOvertimeR.Text = UtilityMethods.getFormatedTimeByMinutes(tsOTAllTotal.TotalMinutes.ToInt32());
                TotalOvertime = tsOTAllTotal;
            }

            if (tsOvertimeAuto < new TimeSpan(0))
            {
                //Undertime exluding Late undertime
                TimeSpan tsUTAllTotal = tsTotalUndertime + (-tsOvertimeAuto);
                //lblTotalUndertimeR.ForeColor = System.Drawing.Color.Red;
                // lblTotalUndertimeR.Text = UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32((tsUTAllTotal.TotalMinutes)));
                //Response.Write((tsTotalUndertime).ToString() + "<br /> +");
                //Response.Write(tsOvertimeAuto.ToString() + "<br /> +");
                //lblOvertimeLabel.Text = "Missing time(Hours not completed): ";
                Totalundertime = tsUTAllTotal;
            }
            else
            {
                TimeSpan tsUTAllTotal = tsTotalUndertime;
                //lblTotalUndertimeR.ForeColor = System.Drawing.Color.Red;
                //lblTotalUndertimeR.Text = UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32((tsUTAllTotal.TotalMinutes)));
                //Response.Write(tsUTAllTotal.ToString() + "<br />");
                Totalundertime = tsUTAllTotal;
            }

            //usmanlblTotalAbsencesR.Text = totalAbs.ToString() + " day(s) (" + totAbsDays.ToString() + " hours)";
            ////lblTotalAbsencesR.Text = totalAbs.ToString() + " day(s)";
            ////lnkViewAbs.HRef = "Absence.aspx?id=" + Request.QueryString["uid"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
            ////lnkViewStats.HRef = "Statistics.aspx?id=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
            //lblEstWHrs.Text = UtilityMethods.getFormatedTimeByMinutes(tsworkingHoursShouldBe.TotalMinutes.ToInt32());
            //lblWHours.Text = UtilityMethods.getFormatedTimeByMinutes(tsworkingHours.TotalMinutes.ToInt32()) + " (Extra/Weekend Work Hours Excluded)";
            //lblTotBreakHrs.Text = UtilityMethods.getFormatedTimeByMinutes(tsbreakhours.TotalMinutes.ToInt32()) + " (should be " + UtilityMethods.getFormatedTimeByMinutes(tsbreakhoursShouldBe.TotalMinutes.ToInt32()) + ")";


            //lblTotDays.Text = iDaysWorked.ToString();

            TimeSpan Totalbalancetime = new TimeSpan(0);

            Totalbalancetime = TotalOvertime - Totalundertime;

            if (Totalbalancetime.TotalMinutes < 0)
            {
                //lblbalancetime.ForeColor = System.Drawing.Color.Red;
                //  lblbalancetime.ToolTip = "Absence not included";
                // lblbalancetime.Text = UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32((Totalbalancetime.TotalMinutes * -1))) + " Undertime";
            }
            else if (Totalbalancetime.TotalMinutes >= 0)
            {
                //  lblbalancetime.ForeColor = System.Drawing.Color.Green;
                //  lblbalancetime.Text = UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32((Totalbalancetime.TotalMinutes))) + " Overtime";
                //  lblbalancetime.ToolTip = "Absence not included";
            }


        }
    }


    protected void lnkGenerateStats_Click(object sender, EventArgs e)
    {
        int id = ddlUsers.SelectedValue.ToInt32();
        int hours = ddlHours.SelectedValue.ToInt32() + Math.Abs((ddlMinutes.SelectedValue != "0") ? (ddlMinutes.SelectedValue.ToInt32() / 60) : 0);
        int breakHr = ddlBrkHour.SelectedValue.ToInt32();
        string additionalbreak = ddlAdditionalBrk.SelectedValue;
        int additionalbreakHr = ddlAdditionalBrkHr.SelectedValue.ToInt32() + Math.Abs((ddlAdditionalBrkMin.SelectedValue != "0") ? (ddlAdditionalBrkMin.SelectedValue.ToInt32() / 60) : 0);
        string StrtHr = txtStartHour.Text;
        string EndHr = txtEndHour.Text;
        //string Appid = "648dec41-d961-4a78-a615-d9aac688c3ad";
        string Appid = HttpContext.Current.User.Identity.Name;
        int TotalAbsences = 15;
        string report_type = rbtnReportType.SelectedValue;
        int month = ddlMonth.SelectedValue.ToInt32();
        int year = (rbtnReportType.SelectedValue == "yearly") ? DateTime.Now.Year : ddlYear.SelectedValue.ToInt32();

        //string report_type = (rbtnYearly.Checked) ? "yearly" : "monthly";
        //int month = ddlMonth.SelectedValue.ToInt32();
        //int year = (rbtnYearly.Checked) ? DateTime.Now.Year : ddlYear.SelectedValue.ToInt32();

        Hashtable stats = JsonConvert.DeserializeObject<Hashtable>(atd.AdvanceCalculateStatistics(userId, hours, year, month, id, instanceID, TotalAbsences, breakHr, StrtHr, EndHr, Appid, report_type, additionalbreak, additionalbreakHr));
        string sum_table = "<div id='reportDiv'><h4>Detailed Statistics</h4>";

        sum_table += "<b>Worked Hours should be: </b></td><td>" + stats["WorkingHoursB"].ToString() + "<br>";
        sum_table += "<b>Employee Worked: </b> </td><td>" + stats["TotalWorkedHoursB"].ToString() + "<br>";
        sum_table += "<hr>";
        if (stats["ExtraHours"].ToString() != "0")
            sum_table += "<b>Balance Time:  </b><span style='color:green;'>" + stats["ExtraHours"].ToString() + " hours</span>" + "<br>";
        else if (stats["UnderHours"].ToString() != "0")
            sum_table += "<b>Balance Time:  </b><span style='color:red;'>" + stats["UnderHours"].ToString() + " hours</span>" + "<br>";
        else
            sum_table += "<b>Balance Time:  </b>0 hours" + "<br><br>";

        sum_table += "</div>";

        result_table.Visible = true;
        advance_stats.InnerHtml = sum_table;
        advance_stats.Visible = true;
    }

    protected void ReportSettings(Hashtable ht)
    {
        DataAccessLayer dal = new DataAccessLayer(false);
        bool res = false;
        DataTable dt = new DataTable();

        if (ht["@action"].ToString() == "insert")
            res = dal.ExecuteNonQuery("sp_ReportSettings", ht);

        if (ht["@action"].ToString() == "get")
        {
            dt = dal.GetDataTable("sp_ReportSettings", ht);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string[] time = row["working_hours"].ToString().Split(',');
                    string[] brktime = row["breakTime"].ToString().Split(',');
                    string[] additional_breakhours = row["additional_breakhours"].ToString().Split(',');
                    ddlHours.SelectedValue = time[0];
                    ddlMinutes.SelectedValue = (time.Length > 1) ? time[1] : "0";
                    txtStartHour.Text = row["startTime"].ToString();
                    txtEndHour.Text = row["endTime"].ToString();
                    ddlBrkHour.SelectedValue = brktime[0];
                    ddlBrkMinutes.SelectedValue = (brktime.Length > 1) ? brktime[1] : "0";
                    ddlAdditionalBrk.SelectedValue = row["additional_break"].ToString();
                    ddlAdditionalBrkHr.SelectedValue = additional_breakhours[0];
                    ddlAdditionalBrkMin.SelectedValue = (brktime.Length > 1) ? additional_breakhours[1] : "0";
                    ddlAdditionalBrkDay.SelectedValue = row["additional_breakday"].ToString();
                }
                hdntype.Value = "update";
            }
            else
            {
                hdntype.Value = "insert";
            }
        }

        if (ht["@action"].ToString() == "update")
            res = dal.ExecuteNonQuery("sp_ReportSettings", ht);
    }

    public string Test()
    {
        //DateTime s = Convert.ToDateTime("2018-07-10 08:42:00.000");
        //DateTime e = Convert.ToDateTime("2018-07-10 18:00:00.000");
        DateTime s = Convert.ToDateTime("2018-07-10 13:38:00.000");
        DateTime e = Convert.ToDateTime("2018-07-10 18:00:00.000");

        TimeSpan start = TimeSpan.Parse(s.TimeOfDay.ToString());
        TimeSpan end = TimeSpan.Parse(e.TimeOfDay.ToString());

        TimeSpan sRange = TimeSpan.Parse(Convert.ToDateTime("1:00 PM").TimeOfDay.ToString()); // 1 PM
        TimeSpan eRange = TimeSpan.Parse(Convert.ToDateTime("2:00 PM").TimeOfDay.ToString());   // 2 pm
        string str = "";

        if (start <= end)
        {
            //if (startTime <= ci.EndTime && endTime >= ci.StartTime)

            // start and stop times are in the same day
            if ((sRange >= start && sRange <= end) && (eRange >= start && eRange <= end))
            {
                // current time is between start and stop
                str = "yes, took break in mins:       ";
                str += eRange.Subtract(sRange).TotalMinutes;

            }
            else if ((start >= sRange) && (start <= eRange))
            {
                str = "clock in break, took break in mins:   ";
                str += eRange.Subtract(start).TotalMinutes;
            }
            else
                str = "no";
        }
        else
        {
            // start and stop times are in different days
            if ((sRange >= start || sRange <= end) && (eRange >= start || eRange <= end))
            {
                // current time is between start and stop
                str = "yes2";
            }
            else
                str = "no2";
        }

        return str;
    }

    public DataTable FillTagTable(DateTime startdate, DateTime enddate, int userid, string SP)
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(SP, con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@startdate", startdate);
                cmd.Parameters.AddWithValue("@enddate", enddate);
                cmd.Parameters.AddWithValue("@userid", userid);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                return dt;
            }
        }
    }

}

class DefaultSettings
{
    public Int32 WorkingHour { get; set; }
    public Int32 BreakTime { get; set; }
    public String StartTime { get; set; }
    public String EndTime { get; set; }

    public DefaultSettings()
    {
        WorkingHour = 8;
        BreakTime = 60;
        StartTime = "09:00 AM";
        EndTime = "06:00 PM";
    }
}

class DateRange
{
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
}