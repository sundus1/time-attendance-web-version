﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Add_worker.aspx.cs" Inherits="attendence.Add_worker" MasterPageFile="~/W/MasterPage.master" %>

<%--<%@ Register Src="~/WebApplication1/userprofile.ascx" TagName="contractuserprofile" TagPrefix="uc1" %>--%>
<%@ Register Src="~/WebApplication1/UserProfileControl.ascx" TagName="contractuserprofile" TagPrefix="uc1" %>


<asp:Content runat="server" ContentPlaceHolderID="head" ID="Content1">

    <link href="../_assets/jquery-ui-timepicker-addon.css" rel="stylesheet" />
    <script src="../_assets/jquery-ui-timepicker-addon.js"></script>

    <script type="text/javascript">

        function openPausedDialog() {
            if ($('#hdnPauseTimeID').val() != "0") {
                var dlg = $('#divPauseDialog').dialog({
                    modal: true,
                    hide: { effect: "blind" },
                    width: 'auto',
                    height: 'auto'
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                var iframe = window.parent.document.getElementById("MainContent_ifapp");
                $('.ui-widget-overlay').css("opacity", ".8");
                $('.lnkPlay').click(function () {
                    $(this).fadeOut();
                });
            }
        }

        function timeAtOffset(offset) {
            var d = new Date();
            var sign = /^\-/.test(offset) ? -1 : 1;
            offset = offset.match(/\d\d/g) || [0, 0];
            d.setMinutes(d.getMinutes() + d.getTimezoneOffset() + sign * (offset[0] * 60 + offset[1] * 1));
            //  return d;
            return d.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
        }

        jQuery(document).ready(function ($) {

            //Absence Adjustment
            if (GetParameterValues("instanceid") == 'ae5eb924-def7-461e-8097-c503ec893bfc')
                $("#tr_absence").show();
            else
                $("#tr_absence").hide();


            $('#btnApplyToAll').click(function () {
                var CI = $('.txtMonCI').val();
                var CO = $('.txtMonCO').val();
                $('.datetimeCI').val(CI);
                $('.datetimeCO').val(CO);
                return false;
            });
            $('.btnCanceWH').click(function () {
                $('#divWorkingHours').dialog('close');
                return false;
            });

            $('.imgDeleteAbs').click(function () {
                var $obj = $(this);
                var userid = $(this).attr('data-userid');
                var id = $(this).attr('data-id');
                var date = $(this).attr('data-date');
                $('#divDialog').attr('title', 'Delete leave?');
                $('#divDialog').html('Are you sure?');
                $('#divDialog').dialog({
                    modal: true,
                    hide: {
                        effect: "blind"
                    },
                    buttons: {
                        "Yes": function () {
                            addNewAbsence(id, userid, date, "False", "Deleting");
                            $obj.closest('tr').hide(200);
                            $(this).dialog('close');
                        },
                        "No": function () {
                            $(this).dialog('close');
                        },
                        "Cancel": function () {
                            $(this).dialog('close');
                        }
                    }
                });
            });

            $('.btnLeaveCancel').click(function () {
                $('.divAbs').dialog('close');
                return false;
            });
            $('.btnAddLeave').click(function () {
                $('.divAbs').dialog('close');
            });

            $('.lnkAddAbs').click(function () {
                var userid = $(this).attr('data-userid');
                var date = new Date();
                $('.txtAbsDateFrom').val('');
                $('.txtAbsDateTo').val('');
                $('.txtAbsComments').val('');
                var dlg = $('.divAbs').dialog({
                    width: 380,
                    modal: true,
                    hide: {
                        effect: "blind"
                    }
                    //buttons: {
                    //    "Save": function () {
                    //        addNewAbsence('0', $('#hdnUserID').val(), $('.txtAbsDate').val(), "True", $('.txtAbsComments').val());
                    //        $(this).dialog('close');
                    //    },
                    //    "Cancel": function () {
                    //        $(this).dialog('close');
                    //    }
                    //}
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;
            });

            $('.lnkMarkNPW').click(function () {
                var dataid = $(this).attr('data-id');
                var datatype = $(this).attr('data-ex-type');
                var datauserid = $(this).attr('data-userid');
                var datadate = $(this).attr('data-date');
                var dataactive = $(this).attr('data-active');
                var $obj = $(this);

                $('#Dialog').attr('title', $(this).attr('tooltip'));
                $('#Dialog').html('Are you sure you want to ' + $(this).attr('tooltip') + "?");
                $('#Dialog').dialog({
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            AddUpdateExDays(dataid, datatype, datauserid, datadate, dataactive, 'Partial Working', true);
                            $(this).dialog('close');
                            if (dataactive == "False") {
                                $obj.closest('tr').removeClass('eggshellRow');
                            }
                            else {
                                $obj.closest('tr').addClass('eggshellRow');
                            }

                        },
                        "No": function () {
                            $(this).dialog('close');
                        },
                        "Cancel": function () {
                            $(this).dialog('close');
                        }
                    }
                });
                return false;
            });


            //$("#chkAP").switchButton({
            //    on_label: 'On',
            //    off_label: 'Off'
            //});

            jQuery(document).on('change', '#chkAP', function ($) {
                //alert('change');
                if ($('#chkAP').is(":checked")) {
                    $('.trAPC').show(200);
                    //$
                }
                else {
                    $('.trAPC').hide(200);
                }


                //$('.tooltip').tooltipster({
                //    interactive: true
                //});


            });

            //$('.datetime').timepicker({
            //    timeFormat: 'hh:mm tt',
            //    controlType: 'select'
            //});


            $('.lnkWorkingHours').click(function () {
                //var AutoPresentID = $(this).attr('data-ap-id');
                //var UserID = $(this).attr('data-ap-userid');
                //var ClockInTime = $(this).attr('data-ap-clockin');
                //var ClockOutTime = $(this).attr('data-ap-clockout');
                //$('.txtWClockIn').val(ClockInTime);
                //$('.txtWClockOut').val(ClockOutTime);
                //var Active = $(this).attr('data-ap-active');
                var dlg = $('#divWorkingHours').dialog({
                    modal: true,
                    width: 400,
                    hide: {
                        effect: "blind",
                    }

                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;
            });

            $('.lblShowExDays').click(function () {
                //var AutoPresentID = $(this).attr('data-ap-id');
                //var UserID = $(this).attr('data-ap-userid');
                //var ClockInTime = $(this).attr('data-ap-clockin');
                //var ClockOutTime = $(this).attr('data-ap-clockout');
                //$('.txtWClockIn').val(ClockInTime);
                //$('.txtWClockOut').val(ClockOutTime);
                //var Active = $(this).attr('data-ap-active');
                var dlg = $('.ExpectionalDays').dialog({
                    modal: true,
                    width: 'auto',
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;
            });

            $('.lbltimesetting').click(function () {
                //var AutoPresentID = $(this).attr('data-ap-id');
                //var UserID = $(this).attr('data-ap-userid');
                //var ClockInTime = $(this).attr('data-ap-clockin');
                //var ClockOutTime = $(this).attr('data-ap-clockout');
                //$('.txtWClockIn').val(ClockInTime);
                //$('.txtWClockOut').val(ClockOutTime);
                //var Active = $(this).attr('data-ap-active');
                var dlg = $('.TimeSetting').dialog({
                    modal: true,
                    width: 'auto',
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;
            });


            $('.timesettings').click(function () {
                $('.TimeSetting').dialog('close');
            });

            $('.timesettingcancel').click(function () {
                $('.TimeSetting').dialog('close');
                return false;
            });


            $('.btnSaveWH').click(function () {
                $('#divWorkingHours').dialog('close');
            });

            $('.btnAddExceptionalDays').click(function () {
                $('.ExpectionalDays').dialog('close');
            });

            $('.btnCancelExDays').click(function () {
                $('.ExpectionalDays').dialog('close');
                return false;
            });

            $('.lnkExNew').click(function () {
                $('.txtExTitle').val('');
                $('.txtExFrom').val('');
                $('.txtExTo').val('');
                $('.txtExClockIn').val('');
                $('.txtExClockOut').val('');
                $('.chkEliminateBreaks').find('input').prop('checked', false);
                $('#hdnGroupID').val('0');
                $('.btnAddExceptionalDays').val('Add');
                return false;
            });

            $('.lnkEdit').click(function () {
                try {
                    $('.txtExTitle').val($(this).parent().parent().find('.lblGroupName').text());
                    $('.txtExFrom').val($(this).parent().parent().find('.lblStartDate').text());
                    $('.txtExTo').val($(this).parent().parent().find('.lblEndDate').text());
                    $('.txtExClockIn').val($(this).parent().parent().find('.lblClockIn').text());
                    $('.txtExClockOut').val($(this).parent().parent().find('.lblClockOut').text());
                    if ($(this).parent().parent().find('.lblClockOut').val() == 'No') {
                        $('.chkEliminateBreaks').find('input').prop('checked', false);
                    }
                    else {
                        $('.chkEliminateBreaks').find('input').prop('checked', true);
                    }
                    $('#hdnGroupID').val($(this).attr('data-groupid'));
                    $('.btnAddExceptionalDays').val('Save');
                    return false;
                } catch (e) {
                    alert(e.message);
                }
            });


            $('.lblAutoPresent').click(function () {
                var AutoPresentID = $(this).attr('data-ap-id');
                var UserID = $(this).attr('data-ap-userid');
                var ClockInTime = $(this).attr('data-ap-clockin');
                var ClockOutTime = $(this).attr('data-ap-clockout');
                $('.txtOPStartTime').val(ClockInTime);
                $('.txtOPEndTime').val(ClockOutTime);
                var Active = $(this).attr('data-ap-active');
                $('#divAutoPresent').dialog({
                    modal: true,
                    buttons: {
                        "Save": function () {
                            ClockInTime = $('.txtOPStartTime').val();
                            ClockOutTime = $('.txtOPEndTime').val();
                            try {
                                if ($('.trAPC').css('display') == 'none') {
                                    Active = 'False';
                                }
                                else {
                                    Active = 'True';
                                }
                                AddUpdateAutoPresent(AutoPresentID, UserID, ClockInTime, ClockOutTime, Active);
                                $('.lblAutoPresent').attr('data-ap-clockin', $('.txtOPStartTime').val());
                                $('.lblAutoPresent').attr('data-ap-clockout', $('.txtOPEndTime').val());
                                $('.lblAutoPresent').attr('data-ap-active', Active);
                                if (Active == "True") {
                                    $('.lblAutoPresent').text('On');
                                }
                                else {
                                    $('.lblAutoPresent').text('Off');
                                }

                                $(this).dialog("close");
                            } catch (e) {

                            }
                        },
                        "Cancel": function () {
                            //alert('close');
                            $(this).dialog("close");
                        }
                    },
                    show: {

                    }
                });
            })

            $('.lblactive').click(function () {
                var userid = $(this).attr('data-userid');
                var status = $(this).attr('data-active');
                $('#divActive').attr('title', "Set status");
                if (status == "True") {
                    $('#divActive').html("<p>Do you want to deactivate employee?</p>");
                    $('#divActive').dialog({
                        modal: true,
                        buttons: {
                            "Yes": function () {
                                SetUserStatus(userid, status, false);
                                $('.lblactive').text("Inactive");
                                $('.lblactive').attr("data-active", "False");
                                $(this).dialog('close');
                            },
                            "No": function () {
                                $(this).dialog('close');
                            },
                            "Cancel": function () {
                                $(this).dialog('close');
                            }
                        }
                    });
                }
                else {
                    $('#divActive').html("<p>Do you want to activate employee?</p>");
                    $('#divActive').dialog({
                        modal: true,
                        buttons: {
                            "Yes": function () {
                                SetUserStatus(userid, status, false);
                                $('.lblactive').text("Active");
                                $('.lblactive').attr("data-active", "True");
                                $(this).dialog('close');
                            },
                            "No": function () {
                                $(this).dialog('close');
                            },
                            "Cancel": function () {
                                $(this).dialog('close');
                            }
                        }
                    });
                }
            });

            $('.chkAutoPresent').click(function () {
                if ($(this).is(":checked")) {
                    var returnVal = confirm("Are you sure?");
                    $(this).attr("checked", returnVal);
                    alert(returnVal);
                }
            });
            //$('#btnUpdateTime').click(function () {
            $(document).on('click', '#<%=btnUpdateTime.ClientID%>', function () {
                $("#Eerror").hide();
                //$('#<%=btnUpdateTime.ClientID%>').click(function () {
                if ($('.txtEFrmTime').val() == 'Select' || $('.txtEToTime').val() == 'Select') {
                    alert('Please select valid time range');
                    return false;
                }

                //if ((Date.parse($('.txtEToTime').val()) <= Date.parse($('.txtEFrmTime').val()))) {
                if ($('.txtEToTime').attr("disabled") == undefined) {
                    if ((Date.parse("1-1-2000 " + $('.txtEToTime').val()) <= Date.parse("1-1-2000 " + $('.txtEFrmTime').val()))) {
                        alert('Invalid Range - Clock-Out time before Clock-In time');
                        return false;
                    }
                }

                if ($("#txteComments").val() == "") {
                    $("#Eerror").show();
                    return false;
                }

                jq191("#divEditTime").dialog("close");
                ActivateAlertDiv('', 'AlertDiv');

            });

            $(document).on('click', '#<%=btnaddtags.ClientID%>', function () {
                jq191("#divAddTags").dialog("close");
                ActivateAlertDiv('', 'AlertDiv');
            });

            //Add Hours
            $(document).on('click', '#<%=btnAddTime.ClientID%>', function () {
                $("#Aerror").hide();
                //$('#<%=btnAddTime.ClientID%>').click(function () {
                if ($('.txtAFrmTime').val() == 'Select' || $('.txtAToTime').val() == 'Select') {
                    alert('Please select valid time range');
                    return false;
                }

                if ((Date.parse("1-1-2000 " + $('.txtAToTime').val()) <= Date.parse("1-1-2000 " + $('.txtAFrmTime').val()))) {
                    alert('Please select valid time range');
                    return false;
                }

                if ($("#txtaComments").val() == "") {
                    $("#Aerror").show();
                    return false;
                }

                jq191("#divAddTime").dialog("close");
                ActivateAlertDiv('', 'AlertDiv');

            });

            //Delete Hours
            $(document).on('click', '#<%=btnDelTime.ClientID%>', function () {
                $("#Derror").hide();

                if ($("#txtdComments").val() == "") {
                    $("#Derror").show();
                    return false;
                }

                jq191("#divDeleteTime").dialog("close");
                ActivateAlertDiv('', 'AlertDiv');

            });

            //$('#imgEditRole1').click(function () {
            //    var dlg = $('#divEditRole').dialog({
            //        modal: true,
            //        hide: {
            //            effect: "blind",
            //        }
            //    });
            //    dlg.parent().appendTo($("form:first"));
            //    dlg.parent().css("z-index", "1000");
            //});
            //jQuery(document).on('click', '.btnRole', function ($) {
            //    $('#divEditRole').dialog().hide(200);
            //});
            $('#btnDeleteTime').click(function () {
                var frmTime = $('.txtfrmTime').val();
                var toTime = $('.txtToTime').val();
                if (frmTime == 'Select' || toTime == 'Select') {
                    alert('Please select a time range');
                    return false;
                }
                //$('#revHr').dialog('close');
                getTimeDifference(frmTime, toTime, '#lblHoursToDel');
                var dlg = $('#divConfirmDeleteTime').dialog({
                    model: true
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "2000");
                var iframe = window.parent.document.getElementById("MainContent_ifapp");
                //if (iframe != null)
                //    iframe.height = "600px";
                return false;
            });
            $('#btnNo, #btnCancel').click(function () {
                //$('#divConfirmDeleteTime').hide(200);
                $('#divConfirmDeleteTime').dialog("close");
                return false;
            });
            // Refresh Worked hour
            $('.lblWorkedhours').attr('data-incdate', $($('span[data-incdate]')[0]).attr('data-incdate'))
            setInterval(function () {
                $('span[data-incdate],label[data-incdate]').each(function (index) {
                    //alert($(this).text());
                    var $target = '#' + $(this).attr('id');
                    var signintime = $($target).attr('data-incdate');

                    //getworkedhours
                    getDateTimeDifference(signintime, $target, new Date(new Date().toLocaleDateString() + ' ' + $('#hdnCITime').val()));
                });
            }, 1000);

            //string AbsLogID, string UserID, string Date, string Active
            $('.lnkMarkAbsence').click(function () {
                //$(document).on('click', ".lnkMarkAbsence", function () {
                var label;
                //$('.lnkMarkAbsence').each(function () {
                label = $(this).text();
                //});
                showDialog("Are you sure?", "<p>Are you sure you want to " + label + "?</p>", $(this));
                return false;
            });

            function showDialog(title, message, obj) {
                $('#divClockedIn').attr("title", title);
                $('#divClockedIn').html(message);
                jq191('#divClockedIn').dialog({
                    model: true,
                    buttons: {
                        "Yes": function () {
                            showAbsCommentBox(obj);
                            jq191(this).dialog("close");
                        },
                        "No": function () {
                            jq191(this).dialog("close");
                        },
                        "Cancel": function () {
                            jq191(this).dialog("close");
                        }
                    }
                });
            }

            function showAbsCommentBox(obj) {
                jq191('#absComment').dialog({
                    width: 375,
                    modal: true,
                    buttons: {
                        "Save": function () {
                            if ($('#txtAbsComment').val() == "") {
                                $("#abs_reason").css("display", "block");
                            }
                            else {

                                $("#abs_reason").css("display", "none");
                                addAbsence(obj, $('#txtAbsComment').val(), false, $('[id$=chkIsAdjustment]').is(":checked"));
                                jq191(this).dialog('close');
                                //jq191('#divClockedIn').dialog("close");
                            }
                        },
                        "Cancel": function () {
                            jq191(this).dialog('close');
                        }
                    }
                });

                setTimeout(function () {
                    $('#txtAbsComment').focus();
                }, 50);

            }

            $('.imgDeleteHours').click(function () {
                //alert('delete hours');
                var $obj = $(this);
                var frmTime = $obj.attr('data-frmTime');
                var toTime = $obj.attr('data-toTime');
                var recordID = $obj.attr('data-recordID');
                var breakTime = "Break Time: " + frmTime + " - " + toTime;
                var dataday = $obj.attr('data-day');
                var datadaytime = $obj.attr('data-daytime');
                $('#lbldataday').text(dataday);
                $('#lblsWorked').text(datadaytime);


                $('.txtfrmTime').val('Select');
                $('.txtToTime').val('Select');
                $('#rID').val(recordID);

                var dlg = $("#revHr").dialog({
                    height: 270,
                    width: 380,
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                var iframe = window.parent.document.getElementById("MainContent_ifapp");
                //if (iframe != null)
                //    iframe.height = "600px";
                return false;
            });

            //Add Tags
            $(document).on('click', ".imgAddTag", function () {
                var $obj = $(this);
                var recordID = $obj.attr('data-recordID');
                $('#hdnerid').val(recordID);

                var dlg = jq191("#divAddTags").dialog({
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });

            //Edit Hours
            $(document).on('click', ".imgEditHours", function () {
                var $obj = $(this);
                var frmTime = $obj.attr('data-frmTime');
                var toTime = $obj.attr('data-toTime');
                var frmDate = $obj.attr('data-frmdate');
                var toDate = $obj.attr('data-todate');
                var autoclockin = $obj.attr("data-autoclockin");
                var recordID = $obj.attr('data-recordID');

                if ($(this).hasClass('imgEditInHour')) {
                    $('.trout').hide();
                }
                else {
                    $('.trout').show();
                }

                $("#lblEDate").text("Date: " + moment(frmDate).format("ddd, MMM Do YYYY"));
                $("#hdnEDate").val(frmDate);
                $('#txteComments').val('');
                $('#hdnOFrmTime').val(frmDate + ' ' + frmTime);
                $('#hdnOToTime').val(toDate + ' ' + toTime);
                $('.txtEFrmTime').val(frmTime);
                $('.txtEToTime').val(toTime);
                $('.lblEFrmTime').text(frmTime);

                if (autoclockin == "1") {
                    $('.txtEFrmTime').hide();
                    $('.lblEFrmTime').show();
                }
                else {
                    $('.lblEFrmTime').hide();
                    $('.txtEFrmTime').show();
                }

                if (toTime == "")
                    $('.txtEToTime').attr('disabled', 'disabled');
                else
                    $('.txtEToTime').removeAttr('disabled');

                $('#hdnerid').val(recordID);

                var dlg = jq191("#divEditTime").dialog({
                    width: 393,
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });

            //Add Hours
            $(document).on('click', ".imgAddHours", function () {
                var $obj = $(this);
                var frmTime = $obj.attr('data-frmTime');
                var toTime = $obj.attr('data-toTime');
                var frmDate = $obj.attr('data-frmdate');
                var toDate = $obj.attr('data-todate');

                if ($(this).hasClass('imgEditInHour')) {
                    $('.trout').hide();
                }
                else {
                    $('.trout').show();
                }
                $('#txtaComments').val('');
                $('#hdnAFrmTime').val(frmDate);
                $('#hdnAToTime').val(toDate);
                $('.txtAFrmTime').val(frmTime);
                $('.txtAToTime').val(toTime);

                //Cannot add Futures Dates entry while being clocked-in              
                var d1 = new Date($(".lblSigninDateTime").text().split(' ')[0]);
                var d2 = new Date($("#hdnAFrmTime").val())
                if ((d1 < d2) && ($("[id$=lblSigninStatus]").text() == "Clocked in at"))
                    openConfirmBox("You cannot add future date entry while being clocked-in. Please clock-out first to add future date entry.");
                else {
                    if (toTime == "")
                        $('.txtAToTime').attr('disabled', 'disabled');
                    else
                        $('.txtAToTime').removeAttr('disabled');

                    var dlg = jq191("#divAddTime").dialog({
                        width: 393,
                        modal: true,
                        hide: {
                            effect: "blind",
                        }
                    });
                    dlg.parent().appendTo($("form:first"));
                    dlg.parent().css("z-index", "1000");
                    return false;
                }
            });

            //Delete Hours
            $(document).on('click', ".imgDelHours", function () {
                var $obj = $(this);
                var frmTime = $obj.attr('data-frmTime');
                var toTime = $obj.attr('data-toTime');
                var frmDate = $obj.attr('data-frmdate');
                var toDate = $obj.attr('data-todate');
                var recordID = $obj.attr('data-recordID');

                $("#hdndrid").val(recordID);
                $('#hdnDFrmTime').val(frmDate + ' ' + frmTime);
                $('#hdnDToTime').val(toDate + ' ' + toTime);
                $('#txtdComments').val('');
                $('.txtDFrmTime').text(moment(frmDate).format("ddd, MMM Do YYYY") + ' ' + frmTime);
                $('.txtDToTime').text(moment(toDate).format("ddd, MMM Do YYYY") + ' ' + toTime);

                var dlg = jq191("#divDeleteTime").dialog({
                    width: 393,
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;
            });

            //Add Absence -  Grid Admin option
            $(document).on('click', ".imgAddAbsence", function () {
                var $obj = $(this);
                var userid = $obj.attr('data-userid');
                var date = $obj.attr('data-date');

                jQuery("[id$=lblcustommsg]").text("Are you sure you want to mark Absent?");
                var dlg = jq191("#divCustomBox").dialog({
                    resizable: false,
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            jq191(this).dialog("close");
                            jq191('#absComment').dialog({
                                width: 375,
                                modal: true,
                                buttons: {
                                    "Save": function () {
                                        if ($('#txtAbsComment').val() == "") {
                                            $("#abs_reason").css("display", "block");
                                        }
                                        else {
                                            $("#abs_reason").css("display", "none");
                                             var isAdjustment = $('[id$=chkIsAdjustment]').is(":checked");
                                            jq191('#absComment').dialog("close");
                                            //
                                            ActivateAlertDiv('', 'AlertDiv');
                                            jQuery.ajax({
                                                url: "Attendance.asmx/MarkAbsent",
                                                type: "POST",
                                                data: "{'userid':'" + userid + "','comment':'" + $('#txtAbsComment').val() + "','date':'" + date + "','isAdjustment':'" + isAdjustment + "'}",
                                                contentType: "application/json; charset=utf-8",
                                                dataType: "json",
                                                async: true,
                                                success: function () {
                                                    //Master page function
                                                    openConfirmBox("Employee marked Absent successfully!");
                                                    location.reload(true);
                                                },
                                                error: function (response) {
                                                    ActivateAlertDiv('none', 'AlertDiv');
                                                    openConfirmBox('Sorry an error has occurred. Please try again.');
                                                }
                                            });
                                            return true;
                                            //end

                                            $('#txtAbsComment').val("");
                                            jq191(this).dialog('close');
                                        }
                                    },
                                    "Cancel": function () {
                                        $('#txtAbsComment').val("");
                                        jq191(this).dialog('close');
                                    }
                                }
                            });
                        },
                        "No": function () {
                            jq191(this).dialog("close");
                            return false;
                        }
                    }
                });
                dlg.parent().appendTo(jQuery("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });


            //$('.tooltip').tooltipster({
            //    interactive: true
            //});

        });

        function showVerifiedSignedOut() {
            $('.pverifyout').show();
            $('#divClockedOut').dialog({
                modal: true,
                buttons: {
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }

        function showSignedOut() {
            $('.pverifyout').hide();
            $('#divClockedOut').dialog({
                modal: true,
                buttons: {
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
        function showVerificationSignedIn() {
            $('.pverifyin').show();
            $('#divClockedIn').dialog({
                modal: true,
                buttons: {
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
        function showSignedIn() {
            $('.pverifyin').hide();
            $('#divClockedIn').dialog({
                modal: true,
                buttons: {
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }


        jQuery(document).ready(function ($) {
            //$('.btnSignNow').click(function () {
            //    $('#divSigninProcess').slideUp(800);
            //});


            <%--            $(window).bind("beforeunload", function () {
                if ($("#<%=btnsignin.ClientID%>").hasClass('hide'))
                    return confirm("Do you really want to close?");
            });--%>


            // Sets hdnSomeEvent val to noevent to provide page leave confirmation
            $('#hdnSomeEvent').val("noevent");
            // Sets hdnSomeEvent val to event in order to avoid page leave confirmation
            $('.raisesevent').click(function () {
                $('#hdnSomeEvent').val("event")
            });
            // Checks if user leaves the page without clocking out
            <%--  window.onunload = function (e) {
                e = e || window.event;
                var someevent = $('#hdnSomeEvent').val();
                var btnsignin = $("#<%=btnsignin.ClientID%>").hasClass('hide');
                if (btnsignin) {
                    // For IE and Firefox prior to version 4
                    if (e) {
                        e.returnValue = alert('You are still clocked in!');
                    }

                    // For Safari
                    return alert('You are still clocked in!');
                }
            };--%>

            //window.onunload = WindowCloseHanlder;


            $('.btnSignoutProcess').click(function () {
                $("#<%=btnsignin.ClientID%>").removeClass('btnSignInDisable');
            });

            $('#rpth_tr_0').css("display", "none");




            setInterval(function () {
                //getWorkedHours
                //getDateTimeDifference($('.lblSigninDateTime').text(), '.lblWorkedhours');
            }, 2000);



            //$('.btnsignout').click(function () {
            //    //if (confirm('Are you sure you want to clock out?')) {
            //    //    return true;
            //    //}
            //    return true;
            //});


            $("#lnkcomment").click(function () {
                $('#lblerror').text("");

                var dlg = $("#addcomment").dialog({
                    height: 160,
                    width: 450,
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                var textbox = document.getElementById('txtcomment');
                textbox.focus();
                textbox.value = textbox.value;
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                var iframe = window.parent.document.getElementById("MainContent_ifapp");
                //if (iframe != null)
                //    iframe.height = "600px";
                return false;
            });

        <%--    $("#<%=btnsignin.ClientID%>").click(function () {
                //alert(updateMsg);
                $(this).attr("onclick", "");
            });--%>

            $(".undo").click(function (e) {
                if (confirm("Are you sure you want to undo?")) {
                    $(this).attr("onclick", "");
                } else {
                    e.preventDefault();
                }
            });


            $("#<%=btnsignout.ClientID%>").click(function () {
                var btnObj = $(this);

                //If User is set to Clockout automatically 
                if ($("#hdnIsAutoClockout").val() == "true") {
                    if ((new Date($('#hdnCOTime').val())) <= (new Date(new Date().toLocaleDateString() + ' ' + $('#hdnCITime').val()))) {
                        return true;
                    }
                    else {
                        $("#hdnIsAutoClockout").val('false');
                    }
                }


                if ($("#hdnAutoCO").val() == "false") {

                    var clockout_datetime;
                    //Auto-timezone update
                    if ($('[id$=hdnESigninDateTime]').val() != "") {
                        clockout_datetime = $('[id$=hdnESigninDateTime]').val();
                    }
                    else
                        clockout_datetime = $('[id$=lblSigninDateTime]').text();

                    //Check if clockin timespan is more than 10 hours, Ask to clockout or enter as Auto-clockin entry
                    jQuery.ajax({
                        url: "Add_worker.aspx/CheckClockInOut_Timespan",
                        type: "POST",
                        data: "{'userid':'" + $("[id$=hdnUserID]").val() + "','timezone':'" + $('[id$=ddltimezone] option:selected').val() + "','dlsaving':'" + $('[id$=hdndlsaving]').val() + "','dlsavinghour':'" + $('[id$=hdndlsavinghour]').val() + "','clockin_datetime':'" + clockout_datetime + "'}",
                        //data: "{'userid':'" + $("[id$=hdnUserID]").val() + "','timezone':'" + $('[id$=ddltimezone] option:selected').val() + "','dlsaving':'" + $('[id$=hdndlsaving]').val() + "','dlsavinghour':'" + $('[id$=hdndlsavinghour]').val() + "','clockin_datetime':'" + $('[id$=lblSigninDateTime]').text() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        beforeSend: function () {
                            ActivateAlertDiv('', 'AlertDiv');
                        },
                        success: function (response) {
                            $("#hdnAutoCO").val("true");
                            if (response.d == "yes") {
                                //$("#lblAClockoutDate").text("Date: " + moment($('[id$=lblSigninDateTime]').text()).format("DD-MM-YYYY"));
                                $("#lblAClockoutDate").text("Date: " + moment(clockout_datetime).format("DD-MM-YYYY"));

                                $('#divClockedIn').attr("title", "Are you sure?");
                                $('#divClockedIn').html("<p style='text-align:center;'>Did you forget to clockout?</p>");
                                var dlg = jq191('#divClockedIn').dialog({
                                    model: true,
                                    closeOnEscape: false,
                                    open: function (event, ui) {
                                        $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                                    },
                                    buttons: {
                                        "Yes": function () {
                                            SelfClockOut(btnObj);
                                            jq191('#divClockedIn').dialog("close");
                                        },
                                        "No": function () {
                                            ClockOut(btnObj);

                                            jq191('#divClockedIn').dialog("close");
                                        }
                                    }
                                });
                                dlg.parent().appendTo($("form:first"));
                                dlg.parent().css("z-index", "1000");

                                ActivateAlertDiv('none', 'AlertDiv');
                            }
                            else {
                                ClockOut(btnObj);
                            }
                        },
                        error: function (response) {
                            $("#hdnAutoCO").val("false");
                            console.log("time: " + response.d);
                        },
                        complete: function () {
                            ActivateAlertDiv('none', 'AlertDiv');
                        }
                    });
                    //End
                    return false;
                }


                //else if (clockoutTime > ) {
                //    console.log('hello')
                //}              
            });



            $('.btnAddPause').click(function () {
                $('.divPauseAsk').hide('blind');
            })

            $('.lnkAddOvertime, .lnkAddUndertime').click(function () {
                $('#lblerrormsg').text("");
                var dialogTitle = "";
                var UOTitle = "";
                if ($(this).hasClass('lnkAddOvertime')) {
                    $('#hdnUOTime').val('true');
                    //dialogTitle = "Overtime" + " " + (new Date()).toLocaleDateString();
                    dialogTitle = "Overtime" + " ";
                    UOTitle = "Add overtime";
                }
                else {
                    $('#hdnUOTime').val('false');
                    //dialogTitle = "Undertime" + " " + (new Date()).toLocaleDateString();
                    dialogTitle = "Undertime" + " ";
                    UOTitle = "Add undetime";
                }
                var dlg = $('#divAddOvertime').dialog({
                    title: dialogTitle, modal: true, hide: { effect: "blind" }
                });
                $('.btnCancelOvertime').click(function () {
                    $('#divAddOvertime').dialog('close');
                    return false;
                });
                //$('.btnAddOvertime').click(function () {
                //    if ($('#lblerrormsg').text() != "Error: Range Invalid")
                //       
                //});

                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                var iframe = window.parent.document.getElementById("MainContent_ifapp");

                return false;
            });

            $(window).load(function () {
                if ($("#hdnAdmin").val() != "admin") {
                    $('.lnkeditO').remove();
                    $('.lnkdeleteO').remove();
                }
            });


            $('.lnkShowOvertime').click(function () {
                $('.trOvertime').toggle();
                return false;
            });
            $('.lnkShowUndertime').click(function () {
                $('.trUndertime').toggle();
                return false;
            });

            $('.lnkeditO').click(function () {
                $(this).parent().parent().find('span').hide();
                $(this).parent().parent().find('input[type="text"], textarea').show();
                $(this).parent().find('.lnkupdateO').show();
                $(this).parent().find('.lnkcancelO').show();
                $(this).hide();
                $(this).parent().parent().find('input[type="text"]').first().focus();
                return false;
            });

            $('.lnkcancelO').click(function () {
                $(this).parent().parent().find('span').show();
                $(this).parent().parent().find('input[type="text"], textarea').hide();
                $(this).parent().find('.lnkupdateO').hide();
                $(this).hide();
                $(this).parent().find('.lnkeditO').show();
                return false;
            });

            var deleteoflag = false;

            //$('.tooltip').tooltip({
            //    offset: [45, 170],
            //    delay: 4000,
            //    effect: 'slide'
            //});


            $("[id$=link]").click(function () {
                openpopup();
                return false;
            });

            $(".DatePick").datepicker({
                dateFormat: 'mm/dd/yy',
                changeMonth: true,
                changeYear: true
            });
            $('.txtdatetime').datepicker();

            //$('.txttime').timepicker({
            //    timeFormat: 'hh:mm tt',
            //    controlType: 'select'
            //});

            $('.bedit').click(function () {
                var frmTime = $(this).attr('data-bfrom');
                var toTime = $(this).attr('data-bto');
                setTimePicker(frmTime, toTime, $(this));
            });

            $('.imgDeleteHours').click(function () {
                var frmTime = $(this).attr('data-frmTime');
                var toTime = $(this).attr('data-toTime');
                setTimePicker(frmTime, toTime);
            });

            function setTimePicker(from, to) {
                var f = $("#hftimeFormat").val();
                var fromTime = parseInt(getTimeFromAMPM(from).split(':')[0]);
                var toTime = parseInt(getTimeFromAMPM(to).split(':')[0]);
                <%-- $(".TimePick").timepicker({
                    //'timeFormat': f, 'step': 1, 'forceRoundTime': true,
                    //'minTime': '<%=f%>',
                    //'maxTime': '<%=t%>',
                    hourMin: fromTime,
                    hourMax: toTime,
                    //controlType: 'select',
                    //'minTime': from,
                    //'maxTime': to,
                    timeFormat: 'hh:mm tt',
                    controlType: 'select'
                    //'showDuration': true
                });--%>
            }

            <%-- $(".TimePick").timepicker({
                //'timeFormat': f, 'step': 1, 'forceRoundTime': true,
                //'minTime': '<%=f%>',
                //'maxTime': '<%=t%>',
                //hourMin: fromTime,
                //hourMax: toTime,
                //controlType: 'select',
                //'minTime': from,
                //'maxTime': to,
                timeFormat: 'hh:mm tt',
                controlType: 'select'
                //'showDuration': true
            });--%>

            function setTimePicker(from, to, $obj) {
                //var f = $("#hftimeFormat").val();
                var fromTime = parseInt(getTimeFromAMPM(from).split(':')[0]);
                var toTime = parseInt(getTimeFromAMPM(to).split(':')[0]);
                <%--   $obj.timepicker({
                    //'timeFormat': f, 'step': 1, 'forceRoundTime': true,
                    //'minTime': '<%=f%>',
                    //'maxTime': '<%=t%>',
                    hourMin: fromTime,
                    hourMax: toTime,
                    //controlType: 'select',
                    //'minTime': from,
                    //'maxTime': to,
                    timeFormat: 'hh:mm tt',
                    controlType: 'select'
                    //'showDuration': true
                });--%>
            }

            //$(".DateTimePick").datetimepicker({

            $(".DateTimePick").timepicker({
                timeFormat: "hh:mm tt",
                controlType: 'select',
                autoclose: true,
            });

            $(".DateTimePickN").timepicker({
                timeFormat: "hh:mm tt",
                controlType: 'select',
                autoclose: true,
            });

            //Tick hdnClockIn time 
            $(function () {
                setInterval(function () {
                    var d1 = $('#hdnCITime').val().split(' '); var d = $('#hdnCITime').val().split(' ')[0].split(':'); if (d[1] == "59") { d[1] = "00"; if (d[0] == "12") d[0] = "01"; else d[0] = parseInt(d[0]) + 1; } else d[1] = parseInt(d[1]) + 1; if (parseInt(d[1]) < 10) d[1] = "0" + d[1]; $('#hdnCITime').val(d[0] + ":" + d[1] + " " + d1[1])
                }, 60000);
            });


            //Check for Auto Clockout
            if ($("#<%=btnsignin.ClientID%>").attr("disabled") == "disabled") {

                $("#<%=btnsignin.ClientID%>").removeClass("btnSignInEnable");
                $("#<%=btnsignin.ClientID%>").addClass("btnSignInDisable");

                $("#<%=btnsignout.ClientID%>").addClass("btnSignOutEnable");
                $("#<%=btnsignout.ClientID%>").removeClass("btnSignOutDisable");
                $("#<%=btnsignin.ClientID%>").addClass("hide");
                $("#<%=btnsignout.ClientID%>").removeClass("hide");
                $('.trProfile').each(function () {
                    $(this).addClass('uncheck');
                    $(this).removeClass('hide');
                });

                if ($('#hdnIsAutoClockout').val() != "false") {
                    var coInterval = setInterval(function () {
                        if ((new Date($('#hdnCOTime').val())) <= (new Date(new Date().toLocaleDateString() + ' ' + $('#hdnCITime').val()))) {
                            clearInterval(coInterval);
                            //Show Msg                            
                            var dlg = jq191('#divAutoClockoutSimple').dialog({
                                width: 400,
                                modal: true,
                                hide: { effect: "blind", }
                            });
                            dlg.parent().appendTo($("form:first"));
                            dlg.parent().css("z-index", "2000");
                            //End
                            $('#hdnACO').val('true');
                            $("#<%=btnsignout.ClientID%>").addClass('raisesevent');
                            ActivateAlertDiv('', 'AlertDiv');
                            $("#<%=btnsignout.ClientID%>").click();
                        }
                    }, 1000)
                }
            } else {
                $("#<%=btnsignin.ClientID%>").addClass("btnSignInEnable");
                $("#<%=btnsignin.ClientID%>").removeClass("btnSignInDisable");

                $("#<%=btnsignout.ClientID%>").removeClass("btnSignOutDisable");
                $("#<%=btnsignout.ClientID%>").addClass("btnSignOutEnable");
                $("#<%=btnsignout.ClientID%>").addClass("hide");
                $("#<%=btnsignin.ClientID%>").removeClass("hide");
                $('.trProfile').each(function () {
                    $(this).addClass('check');
                    $(this).addClass('hide');
                });
            }

            setTimeout("try{document.getElementById('UndoMsg').style.display = 'none';}catch(e){}", 60000);

            jq191('.statusDetails, .information').tooltipster({
                interactive: true,
            });


            //Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            //Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            $('[id$=ddlemployees]').change(function () {
                $("#width_tmp_option").html($('[id$=ddlemployees] option:selected').text());
                $(this).width($("#width_tmp_select").width());
            });


            $('[id$=ddltimezone]').change(function () {
                ActivateAlertDiv('', 'AlertDiv');
                getCurrentTime();
            });

            $(".cross").on('click', function () {
                clearInterval(myInterval);
                $("#div_timezone").hide();
            });

        });

        jQuery(document).on('click', '.imgAction', function ($)
        //jQuery('.imgAction').click(function ($) 
        {
            try {

                var dataautoclockin = jQuery(this).attr('data-autoclockin');
                var datafrmTime = jQuery(this).attr('data-frmTime');
                var datatoTime = jQuery(this).attr('data-toTime');
                var datafrmdate = jQuery(this).attr('data-frmdate');
                var datatodate = jQuery(this).attr('data-todate');
                var datarecordID = jQuery(this).attr('data-recordID');

                //New
                var pos = jQuery(this).offset();
                var elPos = { X: pos.left, Y: pos.top };

                jQuery('#menu').css('left', elPos.X);
                jQuery('#menu').css('top', elPos.Y);
                jQuery('#menu').css('position', 'absolute');


                jQuery('#menu').show(200);
                GenerateMenu(dataautoclockin, datafrmTime, datatoTime, datafrmdate, datatodate, datarecordID);
                jQuery('#menu').menu();
            } catch (e) {
                //alert(e.message);
                openConfirmBox(e.message);
            }
        });

        function GenerateMenu(dataautoclockin, datafrmTime, datatoTime, datafrmdate, datatodate, datarecordID) {
            $('.imgEditHours').attr('data-autoclockin', "dataautoclockin");
            $('.imgEditHours').attr('data-frmTime', datafrmTime);
            $('.imgEditHours').attr('data-toTime', datatoTime);
            $(".imgEditHours").attr('data-frmdate', datafrmdate);
            $(".imgEditHours").attr('data-todate', datatodate);
            $('.imgEditHours').attr('data-recordID', datarecordID);


            $('.imgDelHours').attr('data-frmTime', datafrmTime);
            $('.imgDelHours').attr('data-toTime', datatoTime);
            $(".imgDelHours").attr('data-frmdate', datafrmdate);
            $(".imgDelHours").attr('data-todate', datatodate);
            $('.imgDelHours').attr('data-recordID', datarecordID);

            $('.imgAddTag').attr('data-recordID', datarecordID);

            if ($("#hdnTagCount").val() != "0") {
                $('.imgAddTag').parent().show();
            }
            else {
                $('.imgAddTag').parent().hide();
            }


        }



        $('body').click(function (event) {
            if (event.target.nodeName != 'IMG') {
                $('#menu').hide(200);
            }
        });

        jQuery(document).on('click', 'img.bedit', function ($) {
            try {
                var $edit = $(this);
                var $parenttr = $edit.parent();
                var dataflag = $edit.attr("data-flag");
                var databid = $edit.attr("data-bid");
                var bfrom = $edit.attr("data-bfrom");
                var bto = $edit.attr("data-bto");
                var bdate = $edit.attr("data-bdate");
                $('.bfrmTime').val(bfrom);
                $('.btoTime').val(bto);
                $('#beDate').val(bdate);
                $('#bID').val(databid);
                $('#bModified').val(dataflag);
                $('.lblBreakTime').text('Break Time: ' + bfrom + " - " + bto);
                var dlg = $("#edtHr").dialog({
                    height: 226,
                    width: 291,
                    top: 60,
                    show: {
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

            } catch (e) {
                alert(e.message);
            }
            return false;
        });

        //var signoutclickable = false;
        function ClockOut(btnObj) {

            //if (!signoutclickable) {
            var dlg = jq191('#divDialogUOT').dialog({
                modal: true,
                closeOnEscape: false,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                },
                hide: {
                    effect: "blind"
                },
                buttons: {
                    "Clockout": function () {
                        //signoutclickable = true;
                        //ClockOut(btnObj)
                        $(btnObj).click();
                        //jq191(this).hide('blind');
                        jq191(this).dialog('close');
                        ActivateAlertDiv('', 'AlertDiv');
                    },
                    "Cancel": function () {
                        $("#hdnAutoCO").val("false");
                        $('#hdnIsUnderTime').val('false');
                        jq191(this).dialog('close');
                    }
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            $(".txtUOTReason").val("");

            return false;
            //}
        }

        function SelfClockOut(btnObj) {
            $("#AClockouterror").hide();
            $("#txtAClockout").val("");
            $("#txtAClockoutReason").val("");
            var dlg = jq191('#divAutoClockout').dialog({
                width: 375,
                modal: true,
                closeOnEscape: false,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                },
                buttons: {
                    "Save": function () {
                        $("#AClockouterror").hide();

                        if ($("#txtAClockout").val() == "" || $("#txtAClockoutReason").val() == "") {
                            $("#AClockouterror").show();
                            return false;
                        }
                        else if ((Date.parse("1-1-2000 " + $('.txtAClockout').val()) <= Date.parse("1-1-2000 " + $('.lblSigninTime').text()))) {
                            alert('Please select valid time');
                            return false;
                        }
                        else {
                            $(btnObj).click();
                            // ClockOut(btnObj);
                            jq191(this).dialog('close');
                            ActivateAlertDiv('', 'AlertDiv');
                        }
                    },
                    "Cancel": function () {
                        $("#hdnAutoCO").val("false");
                        jq191(this).dialog('close');
                    }
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
        }

        function getCurrentTime() {
            jQuery.ajax({
                url: "Add_worker.aspx/CurrentTime",
                type: "POST",
                data: "{'userid':'" + $("[id$=hdnUserID]").val() + "','timezone':'" + $('[id$=ddltimezone] option:selected').val() + "','dlsaving':'" + $('[id$=hdndlsaving]').val() + "','dlsavinghour':'" + $('[id$=hdndlsavinghour]').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: function (response) {
                    $("#lbltime").text(response.d);
                    ActivateAlertDiv('none', 'AlertDiv');
                },
                error: function (response) {
                    ActivateAlertDiv('none', 'AlertDiv');
                    console.log("time: " + response.d);
                }
            });
        }

        function editTime() {
            var bID = "";
            var bModified = "";
            var bfrmTime = $('.bfrom').val();
            var btoTime = $('.bto').val();
            var bDate = $('#bDate').val();


            var dlg = $("#edtHr").dialog({
                height: 226,
                width: 291,
                show: {
                },
                modal: true,
                hide: {
                    effect: "blind",
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            //if (iframe != null)
            //    iframe.height = "600px";
            return false;
        }

        function openpopup() {
            $('#lblerror').text("");
            var dlg = jq191("#divworkeradd").dialog({
                height: 'auto',
                width: 800,
                show: {
                },
                modal: true,
                hide: {
                    effect: "blind",
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            //var iframe = window.parent.document.getElementById("MainContent_ifapp");
            //if (iframe != null)
            //    iframe.height = "600px";
            return false;
        }

        function editdiv() {
            var dlg = jq191("#editworker").dialog({
                height: 255,
                width: 450,
                modal: true,
                hide: {
                    effect: "blind",
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            //if (iframe != null)
            //    iframe.height = "600px";
            return false;

        }

        function delTime() {
            var dlg = $("#revHr").dialog({
                height: 140,
                width: 200,
                modal: true,
                hide: {
                    effect: "blind",
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            //if (iframe != null)
            //    iframe.height = "600px";
            return false;
        }

        function Checkovertimehours() {
            var fromDate = new Date((new Date()).toDateString() + ' ' + $('.divAddOvertime .txtOverTimeFrom').val());
            var toDate = new Date((new Date()).toDateString() + ' ' + $('.divAddOvertime .txtOverTimeTo').val());
            if (toDate < fromDate) {
                $('#divAddOvertime').show('blind');
                $('#lblerrormsg').text("Error: Range Invalid");
                return false;

            }
            else {
                if ((((toDate.getTime() - fromDate.getTime()) / 60) / 1000) < $('#hfotlimit').val() && $('#hdnUOTime').val() != "false") {
                    $('#lblerrormsg').text("Error: You cannot enter overtime less than " + $('#hfotlimit').val() + " minutes. Contact the relevant person for details.");
                    return false;
                }
                else {
                    $('#divAddOvertime').hide('blind');
                    $('#lblerrormsg').text("");
                    return true;
                }
            }
        }

        function InstructNewUser() {
            $('#divNewUser').dialog({
                modal: true,
                hide: {
                    effect: "blind",
                },
                buttons: {
                    "Ok": function () {
                        $(this).dialog('close');
                    }
                }
            });
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            iframe.height = "500px";
            iframe.onload = null;
            //iframe.style.height = "600px !important";
        }




        function WindowCloseHanlder() {
            var btnsignin = $("#<%=btnsignin.ClientID%>").hasClass('hide');
            if (btnsignin) {
                //return 'You are still clocked in!';

            }
        }

        //  window.addEventListener("beforeunload", function (e) {
        //    var confirmationMessage = "\o/";
        //  var btnsignin = $("#<%=btnsignin.ClientID%>").hasClass('hide');
        //if (btnsignin) {
        //  (e || window.event).returnValue = confirmationMessage; //Gecko + IE
        //return confirmationMessage;                            //Webkit, Safari, Chrome

        //    }

        //});

        HTMLInputElement.prototype.startsWith = function () { return false; };

        //function timedRefresh(timeoutPeriod) {
        //    setTimeout("location.reload(true);", timeoutPeriod);
        //}

        //window.onload = timedRefresh(60000);

    </script>

    <style type="text/css">
        .ui-widget-content a {
            color: #222222;
            cursor: pointer;
        }

        .auto-style2 {
            width: 62px;
        }

        #tblDetail {
            font-size: 14px;
            text-align: left;
        }

            #tblDetail td {
                padding: 5px 1px 0 4px;
            }

                #tblDetail td:first-child {
                    text-align: right;
                    font-weight: bold;
                }

        .bedit {
            display: none;
        }

            .bedit:last-of-type {
                display: inline;
            }

        h1.username {
            font: 32px/20px arial, helvetica, sans-serif;
            color: #333;
            margin: 6px 0 0 0;
        }


        .comment {
            line-height: 12px;
            margin-bottom: 0px;
        }

        .btnsign {
            float: left;
            background-repeat: no-repeat;
            width: 132px;
            height: 28px;
            padding: 0px 0 0 14px;
            color: #fff;
            font: 16px/ 16px arial, helvetica, sans-serif;
            text-align: left;
        }

        td.btnNeighbor {
            padding: 2px 0 0 0;
            vertical-align: middle;
        }

        table.tblBreaks td {
            padding: 0px;
            margin: 0px;
        }

            table.tblBreaks td:last-child {
                padding: 0 0 0 10px;
            }

        big {
            color: green;
            font-weight: bold;
        }

        #secprofile {
            padding: 1px 0 0 8px;
            line-height: 16px;
        }

        #tblDlg td, #tblDlg th {
            padding: 2px;
        }

        #tblDlg th {
            padding: 2px;
            text-align: right;
            font-weight: bold;
        }

        input[type='radio'] label {
            padding: 5px;
        }

        #width_tmp_select {
            display: none;
        }

        .cross {
            font-weight: bold;
            cursor: pointer;
            float: right;
        }

        .lbltxticon {
            cursor: pointer;
        }

        .align {
            vertical-align: middle !important;
        }

        #rbtn_choice {
            margin-left: 170px !important;
        }
    </style>

    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <link href="../_assets/InfobarStyle.css" rel="stylesheet" />
    <%--</head>
<body>
    <form id="form1" runat="server">--%>
    <asp:HiddenField ID="hdnAdminEmail" runat="server" />
    <asp:HiddenField ID="hdnAdminName" runat="server" />
    <asp:HiddenField ID="hdnUserID" runat="server" />
    <asp:HiddenField ID="hdnParentid" runat="server" />
    <asp:HiddenField ID="hdnIsVerified" runat="server" Value="true" />
    <asp:HiddenField ID="hdn_SessionUpdate" runat="server" />

    <select id="width_tmp_select">
        <option id="width_tmp_option"></option>
    </select>

    <asp:ScriptManager runat="server" ID="scriptmanager" AsyncPostBackTimeout="360000" />
    <%--    <asp:Timer ID="timerForRepeater" runat="server" OnTick="timerForRepeater_Tick" Interval="100000" Enabled="true"></asp:Timer>--%>

    <div id="divMain" runat="server">
        <section id="secprofile">
            <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12 btns-txt hide" id="div_timezone">
                <div class="alert alert-info">
                    <%-- <div>
                        <p>
                            <span class="cross">&times;</span>
                            <i class="fa fa-fw fa-exclamation-circle" style="color: #0085da;"></i>
                            Your Time Zone is
                            <asp:Label runat="server" ID="lbltimezone" Font-Bold="true"></asp:Label>
                            <br />
                            <span style="margin-left: 20px;">
                                <span style="font-size: 14px;">Current Time: 
                            <label id="lbltime" class="clock"><%=System.DateTime.Now.ToShortTimeString() %></label></span>
                                <br />
                                <br />
                                <b>NOTE: <a href="#" runat="server" id="lnk_settingpage"><b>Change timezone</b></a></b>
                            </span>
                        </p>
                        <p style="display: none;">
                            Select Time Zone: 
                                <asp:DropDownList runat="server" ID="ddltimezone" ClientIDMode="Static"></asp:DropDownList>
                            <asp:HiddenField runat="server" ID="timezone_offset" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="hdndlsaving" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="hdndlsavinghour" ClientIDMode="Static" />
                        </p>
                    </div>--%>
                    <div>
                        <p>
                            <span class="cross">&times;</span>
                            <i class="fa fa-fw fa-exclamation-circle" style="color: #0085da;"></i>
                            <b>Please Verify Your Time</b>
                            <br />
                            <br />
                            <asp:Label runat="server" ID="lbltimezone" Font-Bold="true" Visible="false"></asp:Label>
                            <span>
                                <span style="font-size: 14px;">Your current time is: 
                            <label id="lbltime" class="clock"><%=System.DateTime.Now.ToShortTimeString() %></label>
                                    <br />
                                    This time is auto-detected using your IP address and relevant updates have been made in your <a href="#" runat="server" id="lnk_settingpage">settings</a>.
                                    <br />
                                    You can manually update your time from the <a href="#" runat="server" id="lnk_settingpage1">settings</a> page.
                                </span>
                            </span>
                        </p>
                        <p style="display: none;">
                            Select Time Zone: 
                                <asp:DropDownList runat="server" ID="ddltimezone" ClientIDMode="Static"></asp:DropDownList>
                            <asp:HiddenField runat="server" ID="timezone_offset" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="hdndlsaving" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="hdndlsavinghour" ClientIDMode="Static" />
                        </p>
                    </div>
                </div>
            </div>
            <div class="isa_info" id="divInfo" runat="server" visible="false">
                <%--                <i class="fa fa-info-circle"></i>--%>
                    Replace this text with your own text.            
            </div>
            <div class="isa_success" id="divSuccess" runat="server" visible="false">
                <%--<i class="fa fa-check"></i>--%>
                    Replace this text with your own text.
            </div>
            <div class="isa_warning1" id="divWarning" runat="server" visible="false">
                <%--<i class="fa fa-warning"></i>--%>
            </div>
            <div class="isa_error" id="divError" runat="server" visible="false">
                <%--<i class="fa fa-times-circle"></i>--%>
                <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
            </div>
            <br />
            <div>
                <asp:HiddenField runat="server" ID="hfdate" />
                <div class="space" runat="server" id="showProfile">
                    <%--                    <asp:ScriptManager ID="scmang" runat="server"></asp:ScriptManager>--%>
                    <asp:HiddenField runat="server" ID="HiddenField1" />
                    <div id="divworkeradd" style="display: none; width: 600px">
                        <uc1:contractuserprofile ID="workeruserprofile" runat="server" />
                    </div>
                    <a runat="server" id="anctab" target="_blank" style="display: none">a</a>
                </div>
                <%--background-color: #1a87cc;--%>

                <div style="display: inline-flex;">
                    <h1 class="username">
                        <asp:Label ID="lblUserName" runat="server" Text="My Profile"></asp:Label>
                        <span style="font-size: 14px; vertical-align: middle;" runat="server" id="userprofile_link">[<asp:LinkButton runat="server" Text="My Profile" ID="link" Style="vertical-align: middle;"></asp:LinkButton>]
                        </span>
                    </h1>
                    &nbsp;  &nbsp;
                    <div style="display: inline-flex;" id="div_emp" runat="server">
                        <asp:DropDownList ID="ddlemployees" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlemployees_SelectedIndexChanged" CssClass="custom-dropdown">
                        </asp:DropDownList>
                    </div>
                </div>

                <table class="profile table-responsive" cellspacing="0" cellpadding="0">
                    <tr class="managedUser">
                        <td class="caption">
                            <div style="width: 165px !important;">
                                <!--Auto update timezone Exceptional-case Hidden field-->
                                <asp:HiddenField runat="server" ID="hdnErowID" />
                                <asp:HiddenField runat="server" ID="hdnESigninDateTime" />
                                <asp:HiddenField runat="server" ID="hdnEcheck" />
                                <!--End-->

                                <asp:Button ID="btnsignin" CssClass="btnsignin btnsign raisesevent" runat="server" Text="Clock In" OnClick="BtnsigninClick" />
                                <asp:Button ID="btnsignout" CssClass="btnsignout btnsign" Text="Clock Out" runat="server" OnClick="BtnSignoutClick" />
                                <%--<asp:Button ID="lnkAddOvertime" CssClass="lnkAddOvertime lnkAddOvertimeButton btnsign" runat="server" OnClick="lnkAddOvertime_Click" Text="+ Overtime" Visible="false"></asp:Button>--%>
                            </div>
                        </td>
                        <td class="btnNeighbor" id="divWorkedTime" style="padding: 0px" runat="server">
                            <asp:Label ID="lblSigninStatus" runat="server" Text="Clocked in at "></asp:Label>
                            <big>
                            <asp:Label ID="lblSigninTime" CssClass="lblSigninTime" runat="server">00:00 am</asp:Label>
                                </big>
                            <asp:Label ID="lblSignDate" runat="server"></asp:Label>
                            <asp:Label ID="lblSigninDateTime" CssClass="lblSigninDateTime hide" runat="server">00:00 am</asp:Label>
                            <asp:Image ID="imgEditInHour" runat="server" Width="12px" ImageUrl="../images/Controls/pencil.png" ToolTip="Edit Clock-in time" title="Edit Clock-in time" Visible="false" CssClass="tooltip imgEditInHour" Style="margin-left: 5px" />
                        </td>
                        <%-- <td>
                            
                        </td>--%>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                        <td style="padding: 0px; min-height: 10px;">

                            <div runat="server" id="UndoMsg" visible="false">
                                You have been signed
                        <asp:Label runat="server" ID="ttype"></asp:Label>
                                successfully
                    <asp:LinkButton runat="server" CssClass="event" ID="lnkUndo" Text="Undo" OnClick="lnkUndo_Click"></asp:LinkButton><asp:HiddenField runat="server" ID="hrid" />
                            </div>

                        </td>
                    </tr>

                    <%--  <tr class="trProfile" id="trVerifStatus" runat="server">
                        <td class="caption">Verified:
                        </td>
                        <td style="padding-bottom: 6px">
                            <asp:Label ID="lblVerStatus" runat="server"></asp:Label>
                        </td>
                    </tr>--%>

                    <tr class="trProfile" style="display: none">
                        <td></td>
                        <td>
                            <asp:LinkButton runat="server" PostBackUrl="#" ID="lnkDeleteHours" CssClass="lnkDeleteHours">I took additional break</asp:LinkButton>
                        </td>
                    </tr>
                    <tr class="trProfile" id="comments" runat="server" style="display: none">
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td class="caption">Comments:
                                    </td>
                                    <td>
                                        <%--Comments Repeater--%>
                                        <asp:Repeater ID="rptComments" runat="server" OnItemDataBound="rptComments_ItemDataBound">
                                            <HeaderTemplate>
                                                <table>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr id="trComment" runat="server">
                                                    <td>
                                                        <div class="comment">
                                                            <asp:Label runat="server" ID="lblComments" Text='<%# DataBinder.Eval(Container.DataItem, "Comments") %>'></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>

                                                <tfooter>                                                        
                            </tfooter>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:LinkButton runat="server" ID="lnkcomment" Text="Add Comment" Style="text-decoration: none;"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="caption"></td>
                        <td style="width: 100%" id="tdHistory" runat="server">

                            <asp:UpdatePanel ID="updatePanel" runat="server">
                                <ContentTemplate>
                                    <asp:Table ID="tblHistory" runat="server" EnableViewState="false">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                    Empty
                                            </asp:TableCell>
                                            <asp:TableCell>
                                    &nbsp;
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </ContentTemplate>
                                <Triggers>
                                    <%--                                    <asp:AsyncPostBackTrigger ControlID="timerForRepeater" EventName="Tick" />--%>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td class="caption">Reports:
                        </td>
                        <td>
                            <asp:LinkButton runat="server" ID="rpt" OnClick="rpt_Click" CssClass="raisesevent">Monthly Report</asp:LinkButton>
                            | 
                            <asp:LinkButton ID="lnkGetYearStats" runat="server" OnClick="yrpt_Click">Yearly Report</asp:LinkButton>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2"></td>
                    </tr>

                    <tr class="trdataR managedUser" id="tr1" runat="server" visible="false">
                        <td class="caption align">Total Absences in <%=DateTime.Now.Year %>:
                        </td>
                        <td>
                            <asp:Label ID="lblTotalAbsencesR" runat="server" Text="0"></asp:Label>&nbsp;<asp:LinkButton ID="lnkAddAbs" runat="server" CssClass="lnkAddAbs" Text="Add leave(s)" Style="padding-left: 0px" Visible="false"></asp:LinkButton>
                            <%--                            <asp:LinkButton ID="lnkViewAbs" runat="server" CssClass="lnkViewAbs" Text="View Absence(s)" Style="padding-left: 0px" ></asp:LinkButton>--%>
                            <a id="lnkViewAbs" runat="server" style="padding-left: 0px" target="_blank">View Absence(s)</a>
                        </td>
                    </tr>
                    <tr class="trdataR managedUser" id="tr2" runat="server" visible="false">
                        <td class="caption align">Absences in <%=DateTime.Now.ToString("MMM") %>&nbsp;<%=DateTime.Now.Year %>:
                        </td>
                        <td>
                            <asp:Label ID="lblTotalAbsencesM" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                    <tr class="trdataR managedUser" id="tr3" runat="server" visible="false">
                        <td class="caption align">Unmarked in <%=DateTime.Now.ToString("MMM") %>&nbsp;<%=DateTime.Now.Year %>:
                        </td>
                        <td>
                            <asp:Label ID="lblTotalUnmarkedDaysM" runat="server" Text="0"></asp:Label>&nbsp;
                           <a id="lnkViewUnmark" runat="server" class="information" title="Current day not inlcuded" style="padding-left: 0px; font-weight: bold; color: red;" target="_blank"><i class="fa fa-exclamation-circle"></i>&nbsp;Check</a>
                        </td>
                    </tr>
                    <tr class="trdataR">
                        <td class="caption align">Time Spent in <%=DateTime.Now.ToString("MMM") %> <%=DateTime.Now.Year %> :
                        </td>
                        <td>
                            <asp:Label ID="lblTotalWorkedHourM" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <a id="lnkmorestats" href="#" style="padding-left: 0px">More Stats</a>
                        </td>
                    </tr>
                    <tr class="trdataR tr_morestats hide">
                        <td class="caption align">Time Spent in <%=DateTime.Now.Year %>:
                        </td>
                        <td>
                            <asp:Label ID="lblTotalWorkedHourY" runat="server" Text="0"></asp:Label>
                            <a id="lnkViewStats" runat="server" style="padding-left: 0px" target="_blank">(View Statistics)</a>
                        </td>
                    </tr>
                    <tr class="trdataR tr_morestats hide">
                        <td class="caption align">Unmarked days in <%=DateTime.Now.Year %>:
                        </td>
                        <td>
                            <asp:Label ID="lblTotalUnmarkedDays" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>

                    <%-- <tr class="trdataR" id="tr2" runat="server" visible="false">
                            <td class="caption">Overtime 2015: 
                            </td>
                            <td>
                                <asp:Label ID="lblTotalOvertimeR" runat="server" Text="0"></asp:Label>
                            </td>
                        </tr>--%>
                    <%-- <tr class="trdataR" id="tr3" runat="server" visible="false">
                            <td class="caption" style="text-align: right">
                                <asp:Label ID="lbl12" CssClass="lblOvertimeLabel" runat="server" Text="Undertime 2015: "></asp:Label>
                            </td>

                            <td style="">
                                <asp:Label ID="lblTotalUndertimeR" CssClass="divworker defaulte" runat="server" data-text="As the time is recorded" data-label="" Style="float: left"></asp:Label>
                                <div style="background-color: black; width: 20px">
                                </div>
                            </td>
                        </tr>--%>
                    <%-- <tr class="trdataR" id="tr4" runat="server" visible="false">
                            <td class="caption" style="text-align: right; display: block; margin: 6px 0 0 0;">
                                <asp:Label ID="Label3" CssClass="lblOvertimeLabel" runat="server" Text="Balance Time 2015: "></asp:Label>
                            </td>
                            <td style="">
                                <asp:Label ID="lblbalancetime" CssClass="divworker defaulte" runat="server" data-text="As the time is recorded" data-label="" Style="float: left; border: 1px; border-top-style: solid; border-top-color: #000; padding-top: 5px;"></asp:Label>
                                <div style="background-color: black; width: 20px">
                                </div>
                            </td>
                        </tr>--%>

                    <%-- <tr id="imgEditRole" runat="server">
                            <td class="caption">Role:
                            </td>
                            <td>
                                <asp:Label ID="lblUserRole" runat="server">
                            
                                </asp:Label>
                                <img src="images/Controls/edit.png" runat="server" id="imgEditRole1" title="Use this option to change role of employee" alt="Edit" class="imgEditRole tooltip" tooltip="Use this option to change role of employee" style="cursor: pointer; width: 12px;" />
                            </td>
                        </tr>--%>
                    <tr style="display: none">
                        <td class="caption">Autopresent
                        </td>
                        <td>
                            <asp:Label ID="lblAutoPresent" runat="server" CssClass="lblAutoPresent" ForeColor="Blue" Style="cursor: pointer"></asp:Label>
                            <asp:Label ID="lblAutoPresentU" runat="server" CssClass="lblAutoPresentU"></asp:Label>
                        </td>
                    </tr>
                    <tr id="trStatus" runat="server">
                        <td class="caption">Deactive/active Status
                        </td>
                        <td>
                            <asp:Label ID="lblactive" CssClass="lblactive" runat="server" Style="color: blue; cursor: pointer"></asp:Label>
                            <asp:Label ID="lblActiveU" CssClass="lblActiveU" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">
                            <asp:Label ID="lblLeaves" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Repeater ID="rptAbscenses" runat="server">
                                <HeaderTemplate>
                                    <table class="smallGrid" id="tblAdAbs">
                                        <thead>
                                            <tr class="smallGridHead">
                                                <td>Date
                                                </td>
                                                <td style="padding-left: 10px">Comments
                                                </td>
                                                <td style="padding-left: 10px">Action
                                                </td>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr data-id="">
                                        <td>
                                            <asp:Label ID="lblAbsences" runat="server" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "CrtDate")).ToString("ddd, MMM dd yyyy") %>'></asp:Label>
                                        </td>
                                        <td style="padding-left: 10px">
                                            <asp:Label ID="lblComments" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Comment") %>'></asp:Label>
                                        </td>
                                        <td style="padding-left: 10px">
                                            <img style="cursor: pointer; color: blue" src="../images/Controls/cross.png" alt="Delete" id="imgDeleteAbs" class="imgDeleteAbs" width="12px" data-id="<%# DataBinder.Eval(Container.DataItem, "AbsLogID") %>" data-userid="<%# DataBinder.Eval(Container.DataItem, "UserID") %>" data-date="<%# DataBinder.Eval(Container.DataItem, "CrtDate") %>" data-active="<%# DataBinder.Eval(Container.DataItem, "Active") %>" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                </table>


            </div>
            <%--  <div id="divEditRole" title="Employee role" class="hide" runat="server">
                    <table class='profile' style='margin-top: 10px'>
                        <tr>
                            <td style='padding: 5px;'>Select role:
                            </td>
                            <td style='padding: 5px;'>
                                <asp:RadioButtonList ID="rblRole" runat="server">
                                    <asp:ListItem Text="Admin" Value="admin" />
                                    <asp:ListItem Text="Employee" Value="worker" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style='padding: 5px; text-align: right'>
                                <asp:Button ID="btnRole" CssClass="btnRole ui-state-default raisesevent" runat="server" Text="Update" OnClick="btnRole_Click" />

                            </td>
                        </tr>
                    </table>


                </div>--%>
            <div id="divSigninProcess" title="Clock" class="hide">
                <center>
                <table style="margin-top:20px;" cellpadding="0px" cellspacing="0px">
                    <tr runat="server" id="LocationContainer">
                        <td class="caption" style="min-width:0px">
                            <span style="margin-left: 10px;">Location:</span>
                        </td>
                        <td >
                            <asp:DropDownList runat="server" ID="location"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption" style="min-width:0px">Current time:
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblCurrentTime" CssClass="lblCurrentTime">00:00 am</asp:Label>
                        </td>                        
                    </tr>
                    <tr id="trLateClockIn" runat="server" visible="false">
                        <td class="caption" style="min-width:0px">Undertime:
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblUnderTime" CssClass="lblUnderTime"></asp:Label>
                        </td>                                                
                    </tr>
                    <tr id="trOverClockIn" runat="server" visible="false">
                        <td class="caption" style="min-width:0px">Overtime:
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblOvertime" CssClass="lblOvertime"></asp:Label>
                        </td>                                                
                    </tr>
                    <tr id="trLateComment" runat="server" visible="false">
                        <td class="caption" style="min-width:0px">
                            <asp:Label ID="lblUndetimeReason" text="Undertime Reason" runat="server" />                            
                        </td>
                        <td>
                            <asp:TextBox TextMode="MultiLine" runat="server" ID="txtUndertimeComment" CssClass="txtUndertimeComment"></asp:TextBox>
                        </td>                                                
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:center">
                            <asp:Button ID="btnSigninProcess" CssClass="btnSigninProcess btnSignNow btnSignInEnable" runat="server" Text="Clock-In Now" OnClick="BtnsigninClick" style="padding:0 0 0 4px;height:28px;width:130px;text-align:left"/>
                            <asp:Button ID="btnSignoutProcess" CssClass="btnSignoutProcess btnSignNow btnSignOutEnable" runat="server" Text="Clock-Out Now" OnClick="BtnSignoutClick"  style="padding:0 0 0 4px;height:28px;width:130px;text-align:left"/>
                        </td>
                    </tr>
                </table>
                    </center>
            </div>
        </section>
        <asp:HiddenField runat="server" ID="hftimeFormat" />
        <div style="margin-left: 11px;" class="BreakTimeDiv">
            <asp:Label runat="server" ID="noBreak" ForeColor="Red" Text="No record found." Visible="false"></asp:Label>
            <div id="edtHr" title="Edit" style="display: none; padding-left: 10px;">
                <asp:HiddenField runat="server" ID="bID" />
                <asp:HiddenField runat="server" ID="bModified" />
                <asp:HiddenField runat="server" ID="beDate" />
                <div style="padding: 10px 0 10px 28px">
                    <asp:Label ID="lblBreakTime" CssClass="lblBreakTime" runat="server" Style="margin-bottom: 12px;"></asp:Label>
                    <br />
                    <br />
                    <asp:Label runat="server" ID="h" Text="I took break from:" Width="120px"></asp:Label>
                    <asp:TextBox ID="bfrmTime" runat="server" CssClass="TimePick bfrmTime" ValidationGroup="bt" Width="65px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="bfrmTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="bt">Required</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="bfrmTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="(0?[1-9]|(10|11|12))(:[0-5][0-9])( )*(AM|PM|am|pm)" ValidationGroup="bt">Invalid Time</asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="bfrmTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="([01]?[0-9]|2[0-3]):[0-5][0-9]" Visible="False" ValidationGroup="bt">Invalid Time</asp:RegularExpressionValidator>
                </div>
                <div style="padding: 5px 0 10px 28px">
                    <asp:Label runat="server" ID="m" Text="to:" Width="120px"></asp:Label>
                    <asp:TextBox ID="btoTime" runat="server" CssClass="TimePick btoTime" ValidationGroup="bt" Width="65px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="btoTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="bt">Required</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="btoTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="(0?[1-9]|(10|11|12))(:[0-5][0-9])( )*(AM|PM|am|pm)" ValidationGroup="bt">Invalid Time</asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="btoTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="([01]?[0-9]|2[0-3]):[0-5][0-9]" Visible="False" ValidationGroup="bt">Invalid Time</asp:RegularExpressionValidator>
                </div>
                <div style="text-align: right; border-top: 1px solid gray; padding: 10px 0 10px 0px; margin-top: 10px">
                    <asp:Button runat="server" ID="editBreak" CssClass="raisesevent" Text="Save" OnClick="editBreak_Click" ValidationGroup="bt" />
                </div>
            </div>
            <asp:Label ID="bmsgs" runat="server" Text="No breaks" Visible="false"></asp:Label>
        </div>
        <div style="margin-left: 11px;" class="hide">
            <h3 class="SevenDayHHEading">Seven Days History</h3>
        </div>

        <div>
        </div>
        <div id="divActive" class="hide">
        </div>
        <div id="revHr" title="Additional break" style="display: none">
            <asp:HiddenField runat="server" ID="rID" />
            <p>
                Use this option to delete time from your recorded history in order to fix any excess time you recorded by mistake.
            </p>
            <p style="padding: 12px 32px; font-weight: bold; text-align: center">
                <label id="lbldataday"></label>
                <br />
                <label id="lblsWorked"></label>
            </p>
            <div class="divDelHours">
                <asp:Label runat="server" ID="Label1" CssClass="Label1" Text="From:" Width="57px"></asp:Label>
                <asp:TextBox ID="txtfrmTime" runat="server" CssClass="TimePick txtfrmTime" ValidationGroup="dt" Width="65px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtfrmTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="dt">Required</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtfrmTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="(0?[1-9]|(10|11|12))(:[0-5][0-9])( )*(AM|PM|am|pm)" ValidationGroup="dt">Invalid Time</asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtfrmTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="([01]?[0-9]|2[0-3]):[0-5][0-9]" Visible="False" ValidationGroup="dt">Invalid Time</asp:RegularExpressionValidator>
            </div>
            <div class="divDelHours">
                <asp:Label runat="server" ID="Label2" Text="To:" Width="57px"></asp:Label>
                <asp:TextBox ID="txtToTime" runat="server" CssClass="TimePick txtToTime" ValidationGroup="dt" Width="65px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtToTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="dt">Required</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtToTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="(0?[1-9]|(10|11|12))(:[0-5][0-9])( )*(AM|PM|am|pm)" ValidationGroup="dt">Invalid Time</asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtToTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="([01]?[0-9]|2[0-3]):[0-5][0-9]" Visible="False" ValidationGroup="dt">Invalid Time</asp:RegularExpressionValidator>
            </div>
            <div class="divDialogButton">
                <button value="Add Break Time" id="btnDeleteTime" class="btnDeleteTime ui-state-default">Add Break Time</button>
            </div>
        </div>
        <div id="divDeleteTime" title="Delete Time" style="display: none">
            <asp:HiddenField runat="server" ID="hdndrid" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hdnDFrmTime" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hdnDToTime" ClientIDMode="Static" />

            <table class="tblDeleteDlg" id="tblDeleteDlg">
                <tr>
                    <th style="text-align: left" colspan="2">Use this option to delete time from your recorded history.
                    </th>
                </tr>
                <tr>
                    <th>&nbsp;
                    </th>
                    <td>
                        <span id="Derror" class=" formError hide">Please provide a reason in comment box!</span>
                    </td>
                </tr>
                <tr>
                    <th>From:
                    </th>
                    <td>
                        <asp:Label runat="server" ID="txtDFrmTime" ClientIDMode="Static" CssClass="txtDFrmTime"></asp:Label>
                    </td>
                </tr>
                <tr class="trout">
                    <th>To:
                    </th>
                    <td>
                        <asp:Label runat="server" ID="txtDToTime" ClientIDMode="Static" CssClass="txtDToTime"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <th>Comments:
                    </th>
                    <td>
                        <asp:TextBox ID="txtdComments" runat="server" TextMode="MultiLine" ClientIDMode="Static" ValidationGroup="deltime">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="deltime" ControlToValidate="txtdComments" ErrorMessage="Required" SetFocusOnError="true" ForeColor="Red" InitialValue=""></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <div class="divDialogButton">
                <asp:Button ID="btnDelTime" runat="server" Text="Delete Time" CssClass="ui-state-default btnDelTime raisesevent" OnClick="btnDelTime_Click" ValidationGroup="deltime" />
            </div>
        </div>
        <div id="divAddTime" title="Add Time" style="display: none">
            <asp:HiddenField runat="server" ID="hdnarid" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hdnAFrmTime" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hdnAToTime" ClientIDMode="Static" />
            <table class="tblAddDlg" id="tblAddDlg">
                <tr>
                    <th style="text-align: left" colspan="2">Use this option to add clock-in / clock-out time.
                    </th>
                </tr>
                <tr>
                    <%--<td>&nbsp;
                            <asp:Label ID="Label5" CssClass="lblRowID" runat="server"></asp:Label>
                    </td>--%>
                    <th>&nbsp;
                    </th>
                    <td>
                        <span id="Aerror" class=" formError hide">Please provide a reason in comment box!</span>
                    </td>
                </tr>
                <tr>
                    <th>From:
                    </th>
                    <td>
                        <asp:TextBox ID="txtAFrmTime" runat="server" CssClass="DateTimePickN txtAFrmTime" onkeydown="event.preventDefault()" ValidationGroup="addtime" Width="160px" ClientIDMode="Static"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtAFrmTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="addtime">Required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="trout">
                    <th>To:
                    </th>
                    <td>
                        <asp:TextBox ID="txtAToTime" runat="server" CssClass="DateTimePickN txtAToTime" onkeydown="event.preventDefault()" ValidationGroup="addtime" Width="160px" ClientIDMode="Static"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtAToTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="addtime">Required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <th>Comments:
                    </th>
                    <td>
                        <asp:TextBox ID="txtaComments" runat="server" TextMode="MultiLine" ClientIDMode="Static" ValidationGroup="addtime">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="addtime" ControlToValidate="txtaComments" ErrorMessage="Required" SetFocusOnError="true" ForeColor="Red" InitialValue=""></asp:RequiredFieldValidator>
                    </td>
                </tr>

            </table>
            <div class="divDialogButton">
                <asp:Button ID="btnAddTime" runat="server" Text="Add Time" CssClass="ui-state-default btnAddTime raisesevent" OnClick="btnAddTime_Click" ValidationGroup="addtime" />
            </div>
        </div>

        <div id="divEditTime" title="Edit Time" style="display: none">
            <asp:HiddenField runat="server" ID="hdnerid" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hdnOFrmTime" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hdnOToTime" ClientIDMode="Static" />
            <table class="tblDlg" id="tblDlg">
                <tr>
                    <th style="text-align: left" colspan="2">Use this option to edit time from your recorded history.
                    </th>
                </tr>
                <tr>
                    <%-- <td>&nbsp;
                            <asp:Label ID="lblRowID" CssClass="lblRowID" runat="server"></asp:Label>
                    </td>--%>
                    <th></th>
                    <td>
                        <span id="Eerror" class=" formError hide">Please provide a reason in comment box!</span>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: center !important;" colspan="2">
                        <asp:Label ID="lblEDate" CssClass="lblEDate" runat="server" ClientIDMode="Static"></asp:Label>
                        <asp:HiddenField runat="server" ID="hdnEDate" ClientIDMode="Static" />
                    </th>
                </tr>
                <tr>
                    <th>From:
                    </th>
                    <td>
                        <asp:Label runat="server" ID="lblEFrmTime" ClientIDMode="Static" CssClass="lblEFrmTime information" ToolTip="You cannot edit auto clock-in time"></asp:Label>
                        <asp:TextBox ID="txtEFrmTime" runat="server" CssClass="DateTimePick txtEFrmTime" onkeydown="event.preventDefault()" ValidationGroup="edittime" Width="160px" ClientIDMode="Static"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEFrmTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="edittime">Required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="trout">
                    <th>To:
                    </th>
                    <td>
                        <asp:TextBox ID="txtEToTime" runat="server" CssClass="DateTimePick txtEToTime" onkeydown="event.preventDefault()" ValidationGroup="edittime" Width="160px" ClientIDMode="Static"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtEToTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="edittime" InitialValue="0">Required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <th>Comments:
                    </th>
                    <td>
                        <asp:TextBox ID="txteComments" runat="server" TextMode="MultiLine" ClientIDMode="Static" ValidationGroup="edittime">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="edittime" ControlToValidate="txteComments" ErrorMessage="Required" SetFocusOnError="true" ForeColor="Red" InitialValue=""></asp:RequiredFieldValidator>
                    </td>
                </tr>

            </table>
            <div class="divDialogButton">
                <asp:Button ID="btnUpdateTime" runat="server" Text="Update Time" CssClass="ui-state-default btnUpdateTime raisesevent" OnClick="btnUpdateTime_Click" ValidationGroup="edittime" />
            </div>
        </div>
        <div id="divConfirmDeleteTime" class="hide" title="Are you sure?">
            <p style="margin-bottom: 5px">
                <b>Warning:</b> Once the hours are deleted you will not be able to restore them.
            </p>
            <p style="margin-bottom: 5px">
                You are deleting <b>
                    <label id="lblHoursToDel" class="lblHoursToDel">#</label></b> from your work log. 
            </p>
            <p style="margin-bottom: 5px">
                Are you sure you want to do delete this time?
            </p>
            <div class="divDialogButton">
                <asp:Button runat="server" ID="deleHrs" Text="Yes" OnClick="deleHrs_Click" CssClass="raisesevent ui-state-default" />
                <button id="btnNo" class="btnclosedlg ui-state-default" value="No">No</button>
                <button id="btnCancel" class="btnclosedlg ui-state-default" value="Cancel">Cancel</button>
            </div>
        </div>
        <div id="editworker" style="display: none" title="Edit employee time" runat="server">
            <table class="smallGrid">
                <tr>
                    <td style="width: 100px">Signin </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hfeditid" />
                        <input type="text" value="" id="j" style="width: 0px; height: 0px; margin-left: -10px; border-color: transparent; color: transparent" />
                        <asp:TextBox runat="server" ID="txteditsignin" Width="150px" CssClass="DatePick" TabIndex="1"></asp:TextBox>
                        <asp:TextBox runat="server" ID="txteditintime" Width="100px" CssClass="TimePick"></asp:TextBox>
                        <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator9" runat="server" ControlToValidate="txteditsignin" Display="Dynamic" ErrorMessage="Invalid Date" ValidationExpression="(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}" ValidationGroup="ghj">Invalid Date</asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator10" runat="server" ControlToValidate="txteditintime" Display="Dynamic" ErrorMessage="Invalid Time" ValidationExpression="(0?[1-9]|(10|11|12))(:[0-5][0-9])( )*(AM|PM|am|pm)">Invalid Time</asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator11" runat="server" ControlToValidate="txteditintime" Display="Dynamic" ErrorMessage="Invalid Time" ValidationExpression="([01]?[0-9]|2[0-3]):[0-5][0-9]" Visible="False">Invalid Time</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlsigninstatus">
                            <asp:ListItem Text="" Value=""></asp:ListItem>
                            <asp:ListItem Text="Verified" Value="Verified"></asp:ListItem>
                            <asp:ListItem Text="Not Verified" Value="Not Verified"></asp:ListItem>
                        </asp:DropDownList>

                    </td>
                </tr>
                <tr>
                    <td>Signout</td>
                    <td>
                        <input type="text" value="" id="q" style="width: 0px; height: 0px; margin-left: -10px; border-color: transparent; color: transparent" />
                        <asp:TextBox runat="server" ID="txteditsignout" CssClass="DatePick" Width="150px" TabIndex="2"></asp:TextBox>
                        <asp:TextBox runat="server" ID="txteditoutitme" CssClass="TimePick" Width="100px"></asp:TextBox>
                        <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator12" runat="server" ControlToValidate="txteditsignout" Display="Dynamic" ErrorMessage="Invalid Date" ValidationExpression="(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}" ValidationGroup="ghj">Invalid Date</asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator13" runat="server" ControlToValidate="txteditoutitme" Display="Dynamic" ErrorMessage="Invalid Time" ValidationExpression="(0?[1-9]|(10|11|12))(:[0-5][0-9])( )*(AM|PM|am|pm)">Invalid Time</asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator14" runat="server" ControlToValidate="txteditoutitme" Display="Dynamic" ErrorMessage="Invalid Time" ValidationExpression="([01]?[0-9]|2[0-3]):[0-5][0-9]" Visible="False">Invalid Time</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlsignoutstatus">
                            <asp:ListItem Text="" Value=""></asp:ListItem>
                            <asp:ListItem Text="Verified" Value="Verified"></asp:ListItem>
                            <asp:ListItem Text="Not Verified" Value="Not Verified"></asp:ListItem>
                        </asp:DropDownList>

                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:TextBox runat="server" TextMode="MultiLine" ID="txteditcomment" Width="247px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button runat="server" Text="Update" ID="btnupdate" class="buttonstyle" OnClick="BtnupdateClick" ValidationGroup="ghj" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divClockedIn" title="Clocked In Successfully!" style="display: none">
            <p>
                You have successfully clocked in. 
            </p>
            <br />
            <p class="pverifyin">
                Please verify your location by clicking the link in your verification email sent to 
                <asp:Label runat="server" ID="lblInEmail"></asp:Label>
            </p>
        </div>
        <div id="divClockedOut" title="Clocked Out Successfully!" style="display: none">
            <p>
                You have successfully clocked out. 
            </p>
            <br />
            <p class="pverifyout">
                Please verify your location by clicking the link in your verification email sent to 
                <asp:Label runat="server" ID="lblOutEmail"></asp:Label>
            </p>
        </div>

        <div id="divDialog" title="" style="display: none">
        </div>
        <div id="addcomment" title="Comment" style="display: none">
            <asp:HiddenField runat="server" ID="hfcommentstatus" />
            <asp:TextBox runat="server" ID="txtcomment" TextMode="MultiLine" Height="50px"></asp:TextBox>
            <br />
            <br />

            <asp:Button runat="server" ID="btnin" CssClass="raisesevent ui-state-default" Text="Post Comment" placeholder="Please provide your comments..." Style="float: right" />
        </div>
        <div id="addcomentout" title="Signout Comment" style="display: none">
            <asp:TextBox runat="server" ID="txtcommentout" TextMode="MultiLine" Height="50px"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="btnout" runat="server" Text="Post Comment" Style="float: right" />
        </div>
        <div id="absComment" class="hide" title="Comment">
            <table>
                <tr>
                    <td>
                        <textarea id="txtAbsComment" class="txtAbsComment" style="width: 333px; height: 57px;" placeholder="Please add your comments here..."></textarea>
                    </td>
                </tr>
                <tr id="tr_absence">
                    <td>
                        <asp:CheckBox runat="server" ID="chkIsAdjustment" ClientIDMode="Static" /> Mark as Adjustment                      
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="color: red; display: none;" id="abs_reason">Please provide a reason</label>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divAutoPresent" title="Auto Present" style="display: none">
            <table>
                <tr>
                    <td colspan="2">
                        <p>
                            Use this option to mark present automatically by specifying clock in and clock out time.
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>Auto Present:
                    </td>
                    <td>
                        <input type="checkbox" id="chkAP" value="1" />
                    </td>
                </tr>
                <tr class="trAPC">
                    <td>Clock in Time: 
                    </td>
                    <td>
                        <input id="txtOPStartTime" type="text" class="txtOPStartTime datetime" value="Select" />
                    </td>
                </tr>
                <tr class="trAPC">
                    <td>Clock out Time: 
                    </td>
                    <td>
                        <input id="txtOPEndTime" type="text" class="txtOPEndTime datetime" value="Select" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divWorkingHours" title="Working hours" style="display: none">
            <center>
                <h3>
                    <asp:Label ID="lblWorkingHourMessage" CssClass="lblWorkingHourMessage" runat="server"></asp:Label>
                </h3>   
                    </center>
            <table class="profile" id="tblWorkingHours" runat="server" style="margin-top: 10px; padding: 0 13px">
                <tr>
                    <td colspan="3" style="text-align: center">
                        <p>
                            Provide your working hours here
                        </p>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <th>Clock in 
                    </th>
                    <th>Clock out
                    </th>
                </tr>
                <tr>
                    <td class="captionauto">Monday
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hdnMon" Value="0" />
                        <asp:HiddenField runat="server" ID="hdnMonID" Value="0" />
                        <asp:TextBox ID="txtMonCI" runat="server" CssClass="txtMonCI datetime datetimeCI"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtMonCO" runat="server" CssClass="txtMonCO datetime datetimeCO"></asp:TextBox>
                    </td>
                    <td>
                        <img id="btnApplyToAll" class="btnApplyToAll" src="" alt="Apply to all" style="float: right; margin-left: 5px; color: blue; cursor: pointer" />
                    </td>
                </tr>
                <tr class="trAPC">
                    <td class="captionauto">Tuesday
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hdnTue" Value="0" />
                        <asp:HiddenField runat="server" ID="hdnTueID" Value="0" />
                        <asp:TextBox ID="txtTuesCI" runat="server" CssClass="txtTuesCI datetime datetimeCI"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtTuesCO" runat="server" CssClass="txtTuesCO datetime datetimeCO"></asp:TextBox>
                    </td>
                </tr>
                <tr class="trAPC">
                    <td class="captionauto">Wednesday
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hdnWed" Value="0" />
                        <asp:HiddenField runat="server" ID="hdnWedID" Value="0" />
                        <asp:TextBox ID="txtWedCI" runat="server" CssClass="txtWedCI datetime datetimeCI"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtWedCO" runat="server" CssClass="txtWedCO datetime datetimeCO"></asp:TextBox>
                    </td>
                </tr>
                <tr class="trAPC">
                    <td class="captionauto">Thursday
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hdnThu" Value="0" />
                        <asp:HiddenField runat="server" ID="hdnThuID" Value="0" />
                        <asp:TextBox ID="txtThuCI" runat="server" CssClass="txtThuCI datetime datetimeCI"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtThuCO" runat="server" CssClass="txtThuCO datetime datetimeCO"></asp:TextBox>
                    </td>
                </tr>
                <tr class="trAPC">
                    <td class="captionauto">Friday
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hdnFri" Value="0" />
                        <asp:HiddenField runat="server" ID="hdnFriID" Value="0" />
                        <asp:TextBox ID="txtFriCI" runat="server" CssClass="txtFriCI datetime datetimeCI"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFriCO" runat="server" CssClass="txtFriCO datetime datetimeCO"></asp:TextBox>
                    </td>
                </tr>
                <tr class="trAPC">
                    <td class="captionauto">Saturday
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hdnSat" Value="0" />
                        <asp:HiddenField runat="server" ID="hdnSatID" Value="0" />
                        <asp:TextBox ID="txtSatCI" runat="server" CssClass="txtSatCI datetime datetimeCI"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtSatCO" runat="server" CssClass="txtSatCO datetime datetimeCO"></asp:TextBox>
                    </td>
                </tr>
                <tr class="trAPC">
                    <td class="captionauto">Sunday
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hdnSun" Value="0" />
                        <asp:HiddenField runat="server" ID="hdnSunID" Value="0" />
                        <asp:TextBox ID="txtSunCI" runat="server" CssClass="txtSunCI datetime datetimeCI"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtSunCO" runat="server" CssClass="txtSunCO datetime datetimeCO"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="captionauto">Autopresent: 
                    </td>
                    <td>
                        <asp:CheckBox ID="chkAutoPresent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right">
                        <asp:Button ID="btnSaveWH" CssClass="btnSaveWH ui-state-default raisesevent" runat="server" Text="Update" OnClick="btnSaveWH_Click" />
                        <asp:Button ID="btnCanceWH" CssClass="btnCanceWH ui-state-default" runat="server" Text="Cancel" />
                    </td>
                </tr>
            </table>
            <asp:LinkButton ID="lblShowExDays" runat="server" CssClass="lblShowExDays" Text="Days with different working hours"></asp:LinkButton><br />
            <br />
            <asp:LinkButton ID="lbltimesetting" runat="server" CssClass="lbltimesetting" Text="Use flexible hours for this worker (Ex: 7hrs /day)"></asp:LinkButton><br />
            <br />

        </div>
        <div class="Tab ExpectionalDays" title="Days with different working hours" id="ExpectionalDays" runat="server" style="display: none; margin-top: 5px !important">
            <asp:LinkButton ID="lnkExNew" CssClass="lnkExNew" runat="server" Text="Add new"></asp:LinkButton>

            <table style="width: 314px">
                <tr class="smallGrid">
                    <td class="auto-style3">Title </td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtExTitle" CssClass="txtExTitle" runat="server" MaxLength="150" ValidationGroup="l" Width="124px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtExTitle" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="5">Required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="smallGrid">
                    <td class="auto-style3">Start Date</td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtExFrom" CssClass="txtExFrom txtdatetime" runat="server" MaxLength="150" ValidationGroup="l" Width="124px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtExFrom" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="5">Required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="smallGrid">
                    <td class="auto-style3">End Date</td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtExTo" CssClass="txtExTo txtdatetime" runat="server" MaxLength="150" ValidationGroup="l" Width="124px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtExTo" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="5">Required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="smallGrid">
                    <td class="auto-style3">Clock In</td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtExClockIn" CssClass="txtExClockIn txttime" runat="server" MaxLength="150" ValidationGroup="l" Width="124px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtExTo" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="5">Required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="smallGrid">
                    <td class="auto-style3">Clock Out</td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtExClockOut" CssClass="txtExClockOut txttime" runat="server" MaxLength="150" ValidationGroup="l" Width="124px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtExTo" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="5">Required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="smallGrid">
                    <td class="auto-style3">Eliminate Breaks</td>
                    <td class="auto-style1">
                        <asp:CheckBox ID="chkEliminateBreaks" CssClass="chkEliminateBreaks" runat="server" Text="" />
                    </td>
                </tr>
                <tr class="smallGrid">
                    <td class="auto-style3"></td>
                    <td class="auto-style1">
                        <asp:Button ID="btnAddExceptionalDays" CssClass="btnAddExceptionalDays" runat="server" Text="Add" ValidationGroup="5" Width="50px" OnClick="btnAddExceptionalDays_Click" />
                        <%--<asp:Button ID="btnClearExDays" runat="server" Text="Use standard only" ValidationGroup="5" Width="50px" OnClick="btnClearExDays_Click" />--%>
                        <asp:Button ID="btnCancelExDays" CssClass="btnCancelExDays" runat="server" Text="Cancel" ValidationGroup="5" Width="50px" />
                        <asp:HiddenField ID="hdnGroupID" ClientIDMode="Static" runat="server" Value="0" />
                    </td>
                </tr>
            </table>
            <asp:Repeater runat="server" ID="rptExceptionalDays" OnItemCommand="rptExceptionalDays_ItemCommand">
                <HeaderTemplate>
                    <table class="smallGrid">
                        <tr class="smallGridHead">
                            <td>Title</td>
                            <td>From</td>
                            <td>To</td>
                            <td>Clockin</td>
                            <td>Clockout</td>
                            <td>CrtDate</td>
                            <td>ModDate</td>
                            <td>Avoid Breaks</td>
                            <td></td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="smallGrid">
                        <td>
                            <asp:Label runat="server" ID="lblGroupName" Text='<%# DataBinder.Eval(Container.DataItem, "GroupName").ToString() %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblStartDate" Text='<%# GetDate(DataBinder.Eval(Container.DataItem, "StartDate").ToString()) %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblEndDate" Text='<%# GetDate(DataBinder.Eval(Container.DataItem, "EndDate").ToString()) %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblClockIn" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "ClockIn")).ToShortTimeString() %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblClockOut" Text='<%# Convert.ToDateTime((DataBinder.Eval(Container.DataItem, "ClockOut"))).ToShortTimeString()%>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblCrtDate" Text='<%# GetDate(DataBinder.Eval(Container.DataItem, "CrtDate").ToString()) %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblModDate" Text='<%# GetDate(DataBinder.Eval(Container.DataItem, "ModDate").ToString())%>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="chkAvoidBreaks" Text='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "AvoidBreaks").ToString())?"Yes" : "No" %>'></asp:Label>
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" data-groupid='<%# DataBinder.Eval(Container.DataItem, "GroupID").ToString()%>' CommandName="Edit" CssClass="raisesevent lnkEdit" Visible='<%# GetExVisiblity(DataBinder.Eval(Container.DataItem, "UserID").ToString()) %>'></asp:LinkButton>
                            <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" data-groupid='<%# DataBinder.Eval(Container.DataItem, "GroupID").ToString()%>' CommandArgument='<%# DataBinder.Eval(Container.DataItem, "GroupID").ToString()%>' CommandName="Delete" CssClass="raisesevent" Visible='<%# GetExVisiblity(DataBinder.Eval(Container.DataItem, "UserID").ToString()) %>'></asp:LinkButton>
                            <asp:Label ID="lblExInfo" runat="server" CssClass="raisesevent" Text='<%# GetExInfo(DataBinder.Eval(Container.DataItem, "UserID").ToString()) %>'></asp:Label>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <div class="Tab TimeSetting" title="Use flexible hours for this worker (Ex: 7hrs /day)" id="Div1" runat="server" style="display: none; margin-top: 5px !important">
            <p>To Save this Setting, also change this user fixed time setting to flexible time setting </p>
            <table style="width: 405px;">
                <tr class="smallGrid">
                    <td>Hours per day (Ex: 8 hrs /day):
                    </td>
                    <td>
                        <asp:TextBox ID="txthours" runat="server" CssClass="standardField"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server" ErrorMessage="Only Number" ControlToValidate="txthours" ForeColor="Red" ValidationExpression="^\d$" ValidationGroup="flex"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr class="smallGrid">
                    <td>Break:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlflexbreak" runat="server">
                            <asp:ListItem Value="False">No</asp:ListItem>
                            <asp:ListItem Value="True">Yes</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr class="smallGrid">
                    <td></td>
                    <td>
                        <asp:Button ID="btnflexsave" runat="server" CssClass="standardButton timesettings" Text="Save" OnClick="btnflexsave_Click" />&nbsp;
                                <asp:Button ID="Button1" CssClass="timesettingcancel" runat="server" Text="Cancel" ValidationGroup="5" Width="50px" />

                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField runat="server" ID="hdnAdmin" />
        <asp:HiddenField runat="server" ID="hdnSomeEvent" />
    </div>
    <h1 id="h1Inactive" runat="server" style="padding-top: 10px;" visible="false">
        <span style="color: red">Sorry!</span> your account has been deactivated by admin.
    </h1>
    <div id="divAbs" class="divAbs" style="display: none" title="Add leaves">
        <asp:HiddenField runat="server" ID="hdfAbsLogID" />
        <asp:HiddenField runat="server" ID="hdfActive" />
        <asp:HiddenField runat="server" ID="hdfUserID" />
        <table class="profile" style="margin-top: 10px">
            <tr>
                <td colspan="2" style="text-align: center"><b>Please add a leave</b>
                </td>
            </tr>
            <tr>
                <td class="captionauto">Taking leave from: 
                </td>
                <td>
                    <asp:TextBox type="text" ID="txtAbsDateFrom" runat="server" CssClass="txtAbsDateFrom DateTimePick"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="captionauto">To: 
                </td>
                <td>
                    <asp:TextBox type="text" ID="txtAbsDateTo" runat="server" CssClass="txtAbsDateTo DateTimePick"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="captionauto">Comment: 
                </td>
                <td>
                    <asp:TextBox ID="txtAbsComments" CssClass="txtAbsComments" runat="server" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center"><b>Your request will be emailed to your administrators for approval</b>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: right">
                    <asp:Button ID="btnAddLeave" CssClass="btnAddLeave raisesevent ui-state-default" runat="server" Text="Add" OnClick="btnAddLeave_Click" />
                    <asp:Button ID="btnLeaveCancel" CssClass="btnLeaveCancel ui-state-default" runat="server" Text="Cancel" />
                </td>
            </tr>
        </table>
    </div>
    <div id="Dialog" title="">
    </div>
    <div id="divNewUser" class="hide" title="Welcome">
        <div>
            <p>
                Welcome to avaima Time & Attendance.
                    <br />
                <br />
                Start using Time & Attendance by clocking in before you start your start. 
                    <br />
                <br />
                After work, you can clock out to record your working statistics.                    
            </p>
        </div>
    </div>

    <div id="divAutoClockout" title="Clock Out" style="display: none">
        <table>
            <tr>
                <td colspan="2" style="text-align: left;">
                    <p>
                        Use this option to update clock out time in your recorded history, incase you forgot to clock out.                     
                    </p>
                </td>
            </tr>
            <tr>
                <th colspan="2" style="text-align: center;">
                    <span id="AClockouterror" class=" formError hide">Please fill all the fields below!</span>
                </th>
            </tr>
            <tr>
                <th colspan="2" style="text-align: center;">
                    <asp:Label runat="server" ID="lblAClockoutDate" ClientIDMode="Static"></asp:Label>
                </th>
            </tr>
            <tr>
                <th>Clock-out Time: 
                </th>
                <td>
                    <asp:TextBox ID="txtAClockout" runat="server" CssClass="txtAClockout DateTimePickN" ClientIDMode="Static"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <th>Comment: 
                </th>
                <td>
                    <asp:TextBox ID="txtAClockoutReason" runat="server" TextMode="MultiLine" CssClass="txtAClockoutReason" ClientIDMode="Static"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>

    <div id="divDialogUOT" title="Clock-out" style="display: none">
        <div id="divmsg">
        </div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblUOTTitle" runat="server" CssClass="lblUOTTitle"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblUOTDelay" runat="server" CssClass="lblUOTDelay"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Reason:
                </td>
                <td>
                    <asp:TextBox ID="txtUOTReason" runat="server" TextMode="MultiLine" CssClass="txtUOTReason"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="divDialogIN" title="Clock-in" style="display: none">
        <div id="divmsg1">
        </div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" CssClass="lblUOTTitle"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label4" runat="server" CssClass="lblUOTDelay"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Reason:
                </td>
                <td>
                    <asp:TextBox ID="TXTINReason" runat="server" TextMode="MultiLine" CssClass="txtUOTReason"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>

    <ul id="menu" style="display: none;">
        <li><a class="imgDelHours menuitem" id="lnkDelete" href="#">Delete Record</a></li>
        <li><a class="imgEditHours menuitem" id="lnkEditTime" href="#" runat="server">Edit Time</a></li>
        <li><a class="imgAddTag menuitem" id="lnkTag" href="#" runat="server">Add Tags</a></li>
    </ul>
    <div id="divAddTags" title="Add Tags" style="display: none">
        <asp:Repeater runat="server" ID="reptTags">
            <HeaderTemplate>
                <table class="tblAddDlg">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:CheckBox ID="chktag" runat="server" />
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lbliptitle" Text='<%# Eval("Tags") %>'></asp:Label>
                        <asp:HiddenField ID="hftagID" runat="server" Value='<%# Eval("ID") %>' />
                    </td>
                </tr>
            </ItemTemplate>

            <FooterTemplate>
                </table>
                                   
            </FooterTemplate>
        </asp:Repeater>
        <div class="divDialogButton">
            <asp:Button ID="btnaddtags" runat="server" Text="Add Tags" CssClass="ui-state-default btnaddtags raisesevent" OnClick="btnaddtags_Click" />
        </div>
        <asp:Label runat="server" ForeColor="red" Text="No Record Found." Visible="False" ID="lblTags"></asp:Label>
    </div>

    <asp:HiddenField ID="hdnClockinTime" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnClockinServerTime" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnClockoutTime" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnIsUnderTime" ClientIDMode="Static" runat="server" Value="false" />
    <asp:HiddenField ID="hdnIsOverTime" ClientIDMode="Static" runat="server" Value="false" />
    <asp:HiddenField ID="hdnCOTime" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnCITime" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnIsAutoClockin" runat="server" ClientIDMode="Static" Value="false" />
    <asp:HiddenField ID="hdnIsAutoClockout" runat="server" ClientIDMode="Static" Value="false" />
    <asp:HiddenField ID="hdnCOServerTime" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnUndertimeLateClockIn" runat="server" ClientIDMode="Static" />
    <!--More than 10 hours clocked-in hidden field-->
    <asp:HiddenField ID="hdnAutoCO" runat="server" ClientIDMode="Static" Value="false" />
    <!--Auto Clockout hidden field-->
    <asp:HiddenField ID="hdnACO" runat="server" ClientIDMode="Static" Value="false" />
    <asp:HiddenField ID="hdnAnotherCO" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnMessageFlag" runat="server" ClientIDMode="Static" Value="." />
    <asp:HiddenField ID="hdnIsWeekend" runat="server" ClientIDMode="Static" Value="false" />
    <asp:HiddenField ID="hdnIsEditAllowed" runat="server" ClientIDMode="Static" Value="true" />
    <asp:HiddenField ID="hdnTagCount" runat="server" ClientIDMode="Static" Value="0" />

    <div>
    </div>

    <script>
        function OpenDialog(id) {
            jq191(id).dialog({
                width: 400,
                modal: true,
                hide: {
                    effect: "blind",
                },
                buttons: {
                    'Ok': function () {
                        jq191(this).dialog('close');
                    }
                }
            });
        }



        function OpenDialogWB(id) {
            var dlg = $(id).dialog({
                width: 400,
                modal: true,
                hide: { effect: "blind", }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");

            $('.btnClose').click(function () {
                $(id).dialog('close');
                return false;
            });
        }

        function OpenEditUOT() {
            var dlg = $('#divUOT').dialog({
                width: 360,
                modal: true,
                hide: { effect: "blind" },
                buttons: {
                    'Cancel': function () {
                        $(this).dialog('close');
                    }
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
        }

        function OpenEditOvertime() {

            $('#divOvertimeEdit').attr('title', 'Edit Over-time');
            var dlg = $('#divOvertimeEdit').dialog({
                width: 360,
                modal: true,
                hide: { effect: "blind" },
                buttons: {
                    'Cancel': function () {
                        $(this).dialog('close');
                    }
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
        }

        $(function () {
            $('#lblClockintime').text($('#hdnClockinTime').val());

            var div = $('#' + $('#hdnMessageFlag').val());
            if (div != "") {
                OpenDialog(div);
            }

            $('.divAddOvertime .txtOverTimeTo, .divAddOvertime  .txtOverTimeFrom').change(function () {
                var fromDate = new Date((new Date()).toDateString() + ' ' + $('.divAddOvertime .txtOverTimeFrom').val());
                var toDate = new Date((new Date()).toDateString() + ' ' + $('.divAddOvertime .txtOverTimeTo').val());
                var duration = GetDHMs((toDate - fromDate) / 60000);
                $('.lblUOTDuration').text(duration);
            })

            var signin = false;
            var auto_signin = false;
            $('.btnsignin').click(function () {

                if ($("#hdnIsAutoClockin").val() == "true") {
                    //ActivateAlertDiv('', 'AlertDiv');
                    //return true;
                    if (auto_signin) {
                        return true;
                    }

                    if (!auto_signin) {
                        if (new Date($('#hdnClockinTime').val()) >= (new Date(new Date().toLocaleDateString() + ' ' + $('#hdnCITime').val()))) {
                            var timeToCIInterval = setInterval(function () {
                                var difference = new Date($('#hdnClockinTime').val()) - new Date(new Date().toLocaleDateString() + ' ' + $('#hdnCITime').val());
                                if (difference <= 0) { auto_signin = true; clearInterval(timeToCIInterval); jq191('#divAutoClockIn').dialog('close'); ActivateAlertDiv('', 'AlertDiv'); $('.btnsignin').click(); }
                                $('.lblTimeToCI').text(GetDHMWithSec(difference / 60000));
                            }, 1000);

                            var dlg = jq191('#divAutoClockIn').dialog({
                                width: 400,
                                modal: true,
                                hide: { effect: "blind", },
                                buttons: {
                                    'Cancel Auto Clock-in': function () {
                                        clearInterval(timeToCIInterval);
                                        auto_signin = false;
                                        jq191('#divAutoClockIn').dialog('close');
                                    }
                                }
                            });
                            dlg.parent().appendTo($("form:first"));
                            dlg.parent().css("z-index", "2000");

                            return false;
                        }
                    }
                }

                if (!signin) {

                    var btnObj = $(this);
                    var dlg = jq191('#divDialogIN').dialog({
                        modal: true,
                        hide: {
                            effect: "blind"
                        },
                        buttons: {
                            "Clockin": function () {
                                signin = true;
                                $(btnObj).click();
                                //jq191(this).hide('blind');
                                jq191(this).dialog('close');
                                ActivateAlertDiv('', 'AlertDiv');
                            },
                            "Cancel": function () {
                                $('#hdnIsUnderTime').val('false');
                                jq191(this).dialog('close');
                            }
                        }
                    });
                    dlg.parent().appendTo($("form:first"));
                    dlg.parent().css("z-index", "1000");
                    var iframe = window.parent.document.getElementById("MainContent_ifapp");
                    $(".txtUOTReason").val("");
                    return false;

                }
            });

            $('#lnkmorestats').click(function () {
                var obj = $(this);
                if ($(obj).text() != "Back") {
                    ActivateAlertDiv('', 'AlertDiv');
                    jQuery.ajax({
                        url: "Add_worker.aspx/LoadMoreStats",
                        type: "POST",
                        //data: "{'userid':'" + $("[id$=hdnUserID]").val() + "','parentid':'" + $("[id$=hdnParentid]").val() + "','appid':'" + $("[id$=hdnAppId]").val() + "'}",
                        data: "{'userid':'" + $("[id$=hdnUserID]").val() + "','appid':'" + $("[id$=hdnAppId]").val() + "','instanceid':'" + $("[id$=hdnInstanceId]").val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        success: function (response) {

                            var data = response.d.split(',');
                            $("[id$=lblTotalWorkedHourY]").text(data[0]);
                            $("[id$=lblTotalUnmarkedDays]").text(data[1]);

                            $(".tr_morestats").removeClass("hide");
                            $("#lnkmorestats").text("Back");
                            ActivateAlertDiv('none', 'AlertDiv');
                        },
                        error: function (response) {
                            ActivateAlertDiv('none', 'AlertDiv');
                            console.log(response);
                        }
                    });
                }
                else {
                    $(".tr_morestats").addClass("hide");
                    $("#lnkmorestats").text("More Stats");
                }

            });

            $("[id$=rbtn_choice]").on('change', function () {
                var val = $("[id$=rbtn_choice ] :checked").val();
                if (val == "yes") {
                    $("#tab2").removeClass("hide");
                }
                else {
                    $("#tab2").addClass("hide");
                }
            });

        });
    </script>
    <div id="divEarlyClockIn" title="Early Clock-In" class="hide">
        <p>
            You signed in earlier than your start time defined in your working hours
        </p>
        <p>
            If you are intentionally doing over-time then adjust time via time-edit to manually record time.
            As time spent in morning, before defined working hours will be chopped.               
        </p>
    </div>
    <div id="divLateClockIn" title="Late Clock-In" class="hide">
        <%-- <p>
                <b>Subject:</b> You will automatically CLOCK-IN at
                <label id="lblClockintime"></label>
            </p>--%>
        <%--<br />--%>
        <p>
            Your start time is
                <asp:Label ID="lblcitime1" runat="server"></asp:Label>
            and you are
                <asp:Label ID="lblMinutes" runat="server"></asp:Label>
            late!
        </p>
    </div>
    <div id="divAutoClockoutSimple" title="Auto Clock-out" class="hide">
        <p>
            Your working hours end at
                <asp:Label ID="lblClockouttime" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
            and hence you have been clocked out automatically.
        </p>
        <p>
            <%--            You have to manually add overtime in case you are working extra.--%>
                You have to clockin again or you may edit time in case you are working extra.
        </p>
    </div>
    <div id="divAutoClockoutAfter" title="Auto Clock-out" class="hide">
        <p>
            Your working hours are complete. Any extra work can be manually recorded using over-time feature.
        </p>
        <br />
        <p>
            You have to manually add overtime in case you are working extra.
        </p>
    </div>
    <div id="divAlreadyClockedIn" title="Already Clocked-in" class="hide">
        <p>
            You are already clocked-in!
        </p>
    </div>
    <div id="divClockinAfterClockoutTimeWithNoClockIn" title="Already Clocked-in" class="hide">
        <p>
            You can only clock-in during your work hours. You can add Over-time if you are working 
        </p>
    </div>
    <div id="divErr" title="Error" class="hide">
        <p>
            An error has occurred.
        </p>
        <br />
        <p>
            <b>Error:</b>
            <asp:Label ID="lblErrTitle" runat="server"></asp:Label>
        </p>
        <br />
        <p>
            <b>Detail:</b>
            <asp:Label ID="lblErrDetail" runat="server"></asp:Label>
        </p>
        <br />
        <p>
            <asp:Button ID="btnEmailToDev" runat="server" CssClass="btnAddPause raisesevent ui-state-default" Text="Email Support" OnClick="btnEmailToDev_Click" />
            <asp:Button ID="btnClose" CssClass="btnClose ui-state-default" runat="server" Text="Close" />
        </p>
        <%-- <p>
                <b>Stack Overflow:</b> <asp:Label ID="lblErrStack" runat="server"></asp:Label>
            </p>--%>
    </div>
    <div id="divErrUOT" title="Invalid Attempt" class="hide">
        <br />
        <p>
            <b>Error:</b>
            <asp:Label ID="lblUOTMessage" runat="server"></asp:Label>
        </p>
        <%-- <p>
                <b>Stack Overflow:</b> <asp:Label ID="lblErrStack" runat="server"></asp:Label>
            </p>--%>
    </div>
    <div id="divAutoClockIn" class="divAutoClockIn hide" title="Auto Clock-in">
        <p>
            You are set to automatically clock-in at
                <asp:Label runat="server" ID="lblAutoCITime" CssClass="lblAutoCITime"></asp:Label>
            <br />
            <br />
            Time to clock-in:
                <asp:Label ForeColor="#cc0000" Font-Size="Large" ID="lblTimeToCI" CssClass="lblTimeToCI" runat="server"></asp:Label>
        </p>
    </div>
    <%--<div id="divOnClockin" class="hide" title="NOTE">
            <p>
                If you dont clock in within your office work hours. You will be undertimed with late minutes which you have to compensate afterwards in order to complete your work hours.
            </p>
        </div>
        <div id="divOnClockout" class="hide" title="NOTE">
            <p>
                Make sure you compensate your undertime before clocking out. If you clock out before completing your work hours, you will be unable to complete your work hours.
            </p>
        </div>
        <div id="divOnAfterClockin" class="hide" title="Time & Attendance Updates">
            <h3>Time & Attendance Updates:</h3>
            <p>
                Six important Updates (features) to have a look before proceeding:
            </p>
            <ol>
                <li>
                    <b>Pause Time: </b>You can now pause and play your time on while taking certain breaks like Smoking, taking a long break, attending a phone call etc. Depending on your organization's policy. This time slice will also be undertimed from working hours
                </li>
                <li>
                    <b>Add Overtime: </b>You can add overtime during working hour. i.e. Working during break timing/s.
                </li>
                <li>
                    <b>Add Under-time: </b>You can add undertime during working hour while taking any kind of extra breaks like Smoking, taking a long break, attending a phone call etc. Depending on your organization's policy. This time slice will also be undertimed from total work hours.
                    <ul>
                        <li>Pause time is a type of undertime.</li>
                        <li>Every undertime will increase working hours i.e. employee needs to compensate his/her undertime by either adding overtime or by working more than defined working hours</li>
                        <li>Employee has to compensate undertime to complete his working hours. Or he can add overtime afterwards to balance working hours.</li>
                    </ul>
                </li>
                <li>
                    <b>Automated Clock-out: </b>Employee will be automatically clocked out once after he completes his defined work hours. System is intelligent enough to calculate your clock out time through compensating your undertime automatically.
                </li>
                <li>
                    <b>Total Work Hours: </b>are calculation by using three parameters. Working hours, Undertime and overtime. 
                </li>
                <li>
                    <b>Late Clock-in: </b>If employee clocks in after defined clock-out time. Certain time slice will be undertimed which he has to compensate by extra work (late clock-in) and his automatic clock-out time will increase.
                </li>
            </ol>
        </div>
        <div id="divOnAfterAutoClockout" class="hide" title="NOTE">
            You have completed your work hours. You can now add overtime if you are doing overtime.
        </div>--%>


    <%--<div>
            Time & Attendance has been updated. Following are the list of feature that we are changing in Time & Attendance Application.

            <section>
                <h3>Automatic Overtime/Undertime Calculation: </h3>                
                In early version we were calculating Employee's Overtime/Undertime automatically according to their working hours.
                    However, Organizations face the following problems:
                    <ol>
                        <li>Employees that come late, used to clockout after certain duration of time that was compensating their late hours without any permission of administrator.
                        </li>
                        <li>There was an advantage for employees to clockout every day after 5 or 10 minutes later which was appearing as overtime in reports.
                        </li>
                        <li>There were very less amount of traces to track the Undertime and Overtime of employees accurately.
                        </li>
                    </ol>
                <br >/
                We came up with the solution of Manual Overtime/Undertime in order to provide accurate statistics.<br />

            </section>

            face. You may be experiencing some hard time figuring out what's gone wrong with the work hour calculation. 

        </div>--%>

    <div id="WelcomeDiv" title="Welcome!" style="margin-left: 4px; display: none;">
        <div class="breadCrumb" style="margin-left: 4px; text-align: center;">
            Welcome to Time & Attendance
        </div>
        <%--  <div style="text-align: center;" id="tab1">
            <p>
                Please help us improve by providing the information below.
                <br />
                <br />
                <b>Choose your industry:</b>
                <br />
                <asp:DropDownList runat="server" ID="ddl_userIndustry" CssClass="select-dropdown" ClientIDMode="Static">
                    <asp:ListItem Value="" Text="---- Select ----" Selected="True"></asp:ListItem>
                </asp:DropDownList>
            </p>
        </div>--%>
        <div style="text-align: center;" id="tab3">
            <p>
                Almost Done... just a few questions to help us improve your experience.
                <br />
                Do you intend to add employees?
                <br />
                <asp:RadioButtonList ID="rbtn_choice" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                    <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                    <asp:ListItem Text="No" Value="no"></asp:ListItem>
                </asp:RadioButtonList>
                <%--                <br />--%>
                <span id="tab2" class="hide">Time & Attendance of how many of your employees will be tracked through this software?<br />
                    <asp:DropDownList runat="server" ID="ddl_totalEmployees" ClientIDMode="Static">
                        <asp:ListItem Text="1-10" Value="1-10"></asp:ListItem>
                        <asp:ListItem Text="11-25" Value="11-25"></asp:ListItem>
                        <asp:ListItem Text="26-50" Value="26-50"></asp:ListItem>
                        <asp:ListItem Text="51-100" Value="51-100"></asp:ListItem>
                        <asp:ListItem Text="101-200" Value="101-200"></asp:ListItem>
                        <asp:ListItem Text="200 above" Value="200 above"></asp:ListItem>
                    </asp:DropDownList>
                </span>
            </p>
        </div>
    </div>

    <asp:HiddenField runat="server" ID="hfbfrom" Value="0" />
    <asp:HiddenField runat="server" ID="hfbto" Value="0" />
    <asp:HiddenField runat="server" ID="hfotlimit" Value="0" ClientIDMode="Static" />

    <%--   </form>
</body>
</html>--%>
</asp:Content>

