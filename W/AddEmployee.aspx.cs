﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class W_AddEmployee : System.Web.UI.Page
{
    Attendance atd;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            for (int i = 0; i < 3; i++)
                AddRow();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "initialize", "$('#divCustomBox').find('.errorResponse').remove(); $('[id$=lblcustommsg]').text('');", true);

        atd = new Attendance();
        string result = "<ul style=\"margin-left:5px; list-style:none;\" class=\"errorResponse\">";
        bool check = false;

        foreach (RepeaterItem item in rpt_AddEmployeeForm.Items)
        {
            HiddenField insertRow = item.FindControl("hdnRowCheck") as HiddenField;

            if (insertRow.Value == "1") {
                TextBox fname = item.FindControl("txtFirstName") as TextBox;
                TextBox lname = item.FindControl("txtLastName") as TextBox;
                TextBox email = item.FindControl("txtEmail") as TextBox;
                CheckBox ismanaged = item.FindControl("chk_manage") as CheckBox;

                //URL parameters
                string userID = Request.QueryString["id"].ToString();
                string instnaceID = Request.QueryString["instanceid"].ToString();
                string AppID = atd.Timezone_datetime(userID, "", "");       //Avaima userid

                //string response = "Employee added successfully !";        //Hard coded response used in Testing on Local
                string response = atd.AddEmployee(fname.Text, lname.Text, email.Text, userID, AppID, instnaceID, Convert.ToBoolean(ismanaged.Checked));
                if (response != "Employee added successfully !")
                {
                    check = true;
                    result += "<li><b><i class=\"fa fa-close small-txt\"></i> " + email.Text + " <br/>(<span class=\"small-txt\"><u>Reason:</u> " + response + ")</span></b><br/></li>";
                }
            }           
        }

        result += "</ul>";

        //Re-initialize Repeater
        //List<MyData> dataList = new List<MyData>();
        //dataList.Add(new MyData());

        ////-- bind repeater
        //rpt_AddEmployeeForm.DataSource = dataList;
        //rpt_AddEmployeeForm.DataBind();

        //ViewState["DataList"] = dataList;        
        //End


        if (check)
            ScriptManager.RegisterStartupScript(this, GetType(), "response", "$('#divCustomBox').append('" + result + "'); $('#divCustomBox').css('width','250px'); openConfirmBox('Following employee(s) cannot be added')", true);
        else
        {
            //ScriptManager.RegisterStartupScript(this, GetType(), "response", "$('#divCustomBox').append('<span>An email has been sent with the link to access their personal time & attendance dashboard on their own computer or mobile devices.<span>'); $('#divCustomBox').css('width','250px');openConfirmBox('Employee(s) added succesfully! ')", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "reLoad", "localStorage.setItem('successMsg','yes'); window.location.href = window.location.href.split('&n')[0]", true);
        }
    }


    protected void btnAdd_Click(object sender, EventArgs e)
    {
        AddRow();
    }

    public void AddRow()
    {
        List<MyData> dataList = new List<MyData>();

        //-- add all existing values to a list  
        foreach (RepeaterItem item in rpt_AddEmployeeForm.Items)
        {
            dataList.Add(
                            new MyData()
                            {
                                ID = (item.FindControl("hdnrowID") as HiddenField).Value.ToInt32() + 1,
                                FirstName = (item.FindControl("txtFirstName") as TextBox).Text,
                                LastName = (item.FindControl("txtLastName") as TextBox).Text,
                                Email = (item.FindControl("txtEmail") as TextBox).Text,
                                isManaged = (item.FindControl("chk_manage") as CheckBox).Checked.ToString(),
                                lnkRemove = (item.FindControl("lnkRemove") as LinkButton).Text,
                                RowCheck = (item.FindControl("hdnRowCheck") as HiddenField).Value,
                            });
        }

        //-- add a blank row to list to show a new row added
        dataList.Add(new MyData());

        //-- bind repeater
        rpt_AddEmployeeForm.DataSource = dataList;
        rpt_AddEmployeeForm.DataBind();

        ViewState["DataList"] = JsonConvert.SerializeObject(dataList);

        ScriptManager.RegisterStartupScript(this, GetType(), "tooltip", "cursorTooltip();lastRowCSS();", true);

        if ((JsonConvert.DeserializeObject<List<MyData>>(ViewState["DataList"].ToString())).Count == 10)
        {
            btnAdd.Enabled = false;
            btnAdd.CssClass = "notallowed";
            lblMsg.Visible = true;
        }
    }

    public List<MyData> getRepeaterItemsList()
    {
        List<MyData> dataList = new List<MyData>();

        foreach (RepeaterItem item in rpt_AddEmployeeForm.Items)
        {
            dataList.Add(
                            new MyData()
                            {
                                ID = (item.FindControl("hdnrowID") as HiddenField).Value.ToInt32(),
                                FirstName = (item.FindControl("txtFirstName") as TextBox).Text,
                                LastName = (item.FindControl("txtLastName") as TextBox).Text,
                                Email = (item.FindControl("txtEmail") as TextBox).Text,
                                isManaged = (item.FindControl("chk_manage") as CheckBox).Checked.ToString(),
                                lnkRemove = (item.FindControl("lnkRemove") as LinkButton).Text,
                                RowCheck = (item.FindControl("hdnRowCheck") as HiddenField).Value,
                            });
        }

        return dataList;
    }

    protected void rpt_AddEmployeeForm_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        List<MyData> dataList = getRepeaterItemsList();

        if (dataList.Count != 1)
        {
            if (e.CommandName == "removeRow")
            {
                dataList.Remove(dataList.Find(x => x.ID == e.CommandArgument.ToInt32()));

                //-- bind repeater
                rpt_AddEmployeeForm.DataSource = dataList;
                rpt_AddEmployeeForm.DataBind();

                ViewState["DataList"] = JsonConvert.SerializeObject(dataList);

                ScriptManager.RegisterStartupScript(this, GetType(), "tooltip", "cursorTooltip();lastRowCSS();", true);

                if (dataList.Count < 10)
                {
                    lblMsg.Visible = false;
                    btnAdd.CssClass = "";
                    btnAdd.Enabled = true;
                }
            }
        }

            ScriptManager.RegisterStartupScript(this, GetType(), "singleRowCheck", "singleRowCheck();", true);

    }

    protected void rpt_AddEmployeeForm_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        //Inside ItemCreatedEvent
        ScriptManager scriptMan = ScriptManager.GetCurrent(this);
        LinkButton btn = e.Item.FindControl("lnkRemove") as LinkButton;
        if (btn != null)
        {
            scriptMan.RegisterAsyncPostBackControl(btn);
        }
    }
}

[Serializable]
public class MyData
{
    string managedBy = "true";

    public int ID { get; set; }
    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string Email { get; set; }
    public string isManaged {
        get { return this.managedBy; }
        set { this.managedBy = value; }
    }

    public string lnkRemove { get; set; }

    public string RowCheck { get; set; }
}
