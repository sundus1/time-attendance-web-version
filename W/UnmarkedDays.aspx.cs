﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class W_UnmarkedDays : AvaimaThirdpartyTool.AvaimaWebPage
{
    Attendance atd = new Attendance();
    string Appid = ""; string instanceid = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnUserID.Value = Request.QueryString["uid"].ToString();
            hfparentid.Value = Request.QueryString["id"].ToString();
            hdnUserEmail.Value = Request.QueryString["e"].Decrypt();
            hdnUserPassword.Value = Request.QueryString["p"].Decrypt();
            instanceid = Request.QueryString["instanceid"].ToString();

            string isVerified = atd.VerifyLogin(hdnUserEmail.Value, hdnUserPassword.Value);
            if (isVerified != "TRUE")
                this.Redirect("Info.aspx");

            //Managed User
            if (hdnUserID.Value == hfparentid.Value)
            {
                if (!atd.UserManaged(hfparentid.Value.ToInt32(), instanceid, "get"))
                {
                    this.Redirect("Info.aspx");
                }
            }

            // Get User Name
            String userName = SP.GetWorkerName(hdnUserID.Value);
            lblUserName.Text = userName;

            LoadDays();
        }
    }

    public void LoadDays()
    {
        DateTime startDate = Request.QueryString["sdate"].ToDateTime();
        DateTime endDate = Request.QueryString["edate"].ToDateTime();

        Attendance atd = new Attendance();
        DataTable dtHistory = new DataTable();
        dtHistory.Columns.Add("DateTime", typeof(DateTime));
        dtHistory.Columns.Add("Day");
        dtHistory.Columns.Add("Clock-in");
        dtHistory.Columns.Add("Clock-out");
        dtHistory.Columns.Add("Time Spent");

        dtHistory.Merge(atd.FormatDataTable(atd.GetYearlyAttendance(hdnUserID.Value.ToInt32(), startDate, endDate), "attendance", ""));
        if (instanceid == "ae5eb924-def7-461e-8097-c503ec893bfc")
            dtHistory.Merge(atd.FormatDataTable(atd.GetAllYearlyAbsences(hdnUserID.Value.ToInt32(), startDate, endDate), "absence", Appid));
        else
            dtHistory.Merge(atd.FormatDataTable(atd.GetYearlyAbsences(hdnUserID.Value.ToInt32(), startDate, endDate), "absence", Appid));

        string UnMarkedDates = atd.GetUnmarkedDays(dtHistory, startDate, endDate, atd.getParent(hfparentid.Value).ToInt32(), endDate.Year);
        dtHistory = new DataTable();
        dtHistory.Columns.Add("DateTime", typeof(DateTime));
        dtHistory.Columns.Add("Day");
        dtHistory.Columns.Add("Clock-in");
        dtHistory.Columns.Add("Clock-out");
        dtHistory.Columns.Add("Time Spent");
        DataRow row = null;

        if (UnMarkedDates != "")
        {
            foreach (string days in UnMarkedDates.Split(','))
            {
                row = dtHistory.NewRow();
                row["DateTime"] = Convert.ToDateTime(days).ToString("dd-MMM-yyyy");
                row["Day"] = Convert.ToDateTime(days).ToString("dd-MMM-yyyy");
                row["Clock-in"] = "unmarked";
                row["Clock-out"] = "";
                row["Time Spent"] = "";
                dtHistory.Rows.Add(row);
            }
        }

        if (dtHistory.Rows.Count > 0)
        {
            DataView view = dtHistory.DefaultView;
            view.Sort = "DateTime ASC";
            dtHistory = view.ToTable();
            hdnCheckdata.Value = "true";
            lblcount.Text = dtHistory.Rows.Count + " unmarked days";
        }
        else
        {
            row = dtHistory.NewRow();
            row["DateTime"] = DateTime.Now;
            row["Day"] = "No Unmarked Day Available";
            row["Clock-in"] = "";
            row["Clock-out"] = "";
            row["Time Spent"] = "";
            dtHistory.Rows.Add(row);
            hdnCheckdata.Value = "false";
            lblcount.Text = "0 unmarked days";
        }

        rpt_unmarkeddays.DataSource = dtHistory;
        rpt_unmarkeddays.DataBind();
    }

    [WebMethod]
    public static void MarkAbsent(int userid, string dateArray)
    {
        string[] dates = JsonConvert.DeserializeObject<string[]>(dateArray);
        Attendance atd = new Attendance();

        foreach (string value in dates)
        {
            atd.MarkAbsent(userid, "Marked absent on Unmarked Day", value.ToDateTime().Date);
        }
    }

    protected void rpt_unmarkeddays_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (hdnCheckdata.Value == "false")
            {
                CheckBox selecteditem = e.Item.FindControl("chkselecteditem") as CheckBox;
                selecteditem.Enabled = false;
            }
        }

        if (e.Item.ItemType == ListItemType.Header)
        {
            if (hdnCheckdata.Value == "false")
            {
                CheckBox selectall = e.Item.FindControl("chkselectall") as CheckBox;
                selectall.Enabled = false;
            }
        }
    }
}