﻿<%@ Page Title="" Language="C#" MasterPageFile="~/W/MasterPage.master" AutoEventWireup="true" CodeFile="AdminDashboard.aspx.cs" Inherits="W_AdminDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style>
        /*Tabbed Layout*/
        .container {
            width: 1500px;
            margin: 0 auto;
        }

        ul.tabs {
            margin: 0px;
            padding: 0px;
            list-style: none;
        }

            ul.tabs li {
                border-radius: 3px;
                border: aliceblue 1px solid;
                background: #e8e7e7;
                color: #222;
                display: inline-block;
                padding: 10px 50px;
                cursor: pointer;
                font-weight: bold;
            }

                ul.tabs li.current {
                    font-weight: bold;
                    background: #3e8ad2;
                    color: white;
                }

        .tab-content {
            display: none;
            padding: 15px;
        }

            .tab-content.current {
                display: inherit;
            }
        /*Tabbed Layout*/

        input[type=text] {
            display: inline-block;
            padding: 6px;
            border: 1px solid #b7b7b7;
            border-radius: 3px;
            font: arial, helvetica, sans-serif;
        }

        textarea {
            height: 80px;
            width: 350px;
            display: inline-block;
            padding: 6px;
            border: 1px solid #b7b7b7;
            border-radius: 3px;
            font: arial, helvetica, sans-serif;
            margin-top: 5px;
        }

        .labels {
            font-weight: bold;
        }

        .btn {
            font-size: inherit !important;
            line-height: normal !important;
            cursor: pointer;
        }

        #tab-1 label {
            width: 80px !important;
            text-align: left !important;
        }

        .notification_table {
            width: 900px;
        }

            .notification_table > thead > tr {
                background: lightgrey;
            }

        .table-grid-sample1 tbody tr:first-child {
            background: none !important;
        }

            .table-grid-sample1 tbody tr:first-child td {
                font-weight: normal !important;
            }

        /*checkbox*/
        input[type="checkbox"] {
            cursor: pointer;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            outline: 0;
            background: lightgray;
            height: 16px;
            width: 16px;
            border: 1px solid white;
        }

            input[type="checkbox"]:checked {
                background: #4c8a3f;
            }

            input[type="checkbox"]:hover {
                filter: brightness(90%);
            }

            input[type="checkbox"]:disabled {
                background: #e6e6e6;
                opacity: 0.6;
                pointer-events: none;
            }

            input[type="checkbox"]:after {
                content: '';
                position: relative;
                left: 40%;
                top: 20%;
                width: 15%;
                height: 40%;
                border: solid #fff;
                border-width: 0 2px 2px 0;
                transform: rotate(45deg);
                display: none;
            }

            input[type="checkbox"]:checked:after {
                display: block;
            }

            input[type="checkbox"]:disabled:after {
                border-color: #7b7b7b;
            }
    </style>

    <script>
        jQuery(document).ready(function ($) {

            //Tabbed layout
            $('ul.tabs li').click(function () {
                var tab_id = $(this).attr('data-tab');

                $('ul.tabs li').removeClass('current');
                $('.tab-content').removeClass('current');

                $(this).addClass('current');
                $("#" + tab_id).addClass('current');

            });

            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

        });
    </script>

    <asp:ScriptManager runat="server" ID="scriptmanager1"></asp:ScriptManager>

    <div class="container">
        <ul class="tabs">
            <li class="tab-link current" data-tab="tab-1">Email Notifications</li>
            <li class="tab-link" data-tab="tab-2" onclick="window.open('AdminPanel.aspx', '_blank');">Graph</li>
        </ul>

        <div id="tab-1" class="tab-content current">
            <asp:UpdatePanel runat="server" ID="notifications_panel">
                <ContentTemplate>
                    <h6>List</h6>
                    <hr />
                    <div id="div_addnotifications">
                        <label class="labels">Name</label>
                        <asp:TextBox ID="name" runat="server" MaxLength="150" ValidationGroup="hd" CssClass="f-2" ClientIDMode="Static"
                            placeholder="Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Name'"></asp:TextBox>
                        <br />
                        <label class="labels">Description</label>
                        <asp:TextBox ID="desc" runat="server" ValidationGroup="hd" CssClass="f-2" ClientIDMode="Static" TextMode="MultiLine"
                            placeholder="Description" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Description'"></asp:TextBox>
                        <br />
                        <br />
                        <asp:Button ID="Add" runat="server" Text="Add" ValidationGroup="hd" OnClick="Add_Click" ClientIDMode="Static" CssClass="btn btn-primary" />
                    </div>
                    <br />

                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12" style="padding: 0px;" id="div_notifications">
                        <asp:Repeater runat="server" ID="rpt_notifications" OnItemCommand="rpt_notifications_ItemCommand">
                            <HeaderTemplate>
                                <table class="table-grid-sample1 notification_table">
                                    <thead>
                                        <tr>
                                            <td>Name</td>
                                            <td>Description</td>
                                            <td>Active</td>
                                             <td></td>
                                        </tr>
                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ID") %>' ClientIDMode="Static" />
                                        <asp:Label runat="server" ID="lblName" Text='<%# Eval("Name") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblDesc" Text='<%# Eval("Description") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:CheckBox runat="server" ID="chk_active" ClientIDMode="Static" data-id='<%# Eval("ID") %>' Checked='<%# Convert.ToBoolean(Eval("Active")) %>' OnCheckedChanged="chk_active_CheckedChanged" AutoPostBack="true" />
                                    </td>
                                    <td>
                                        <asp:LinkButton runat="server" ID="lnkDelete" ClientIDMode="Static" Text="<i class='fa fa-trash' style='color:red;font-size:15px;'></i>" CommandArgument='<%# Eval("ID") %>' CommandName="delete" OnClientClick="return confirm('Are you sure?')"/>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:Label runat="server" ForeColor="red" Text="No Record Found." Visible="False" ID="hdmsg"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
