﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="settings.aspx.cs" Inherits="settings" MasterPageFile="~/W/MasterPage.master" EnableEventValidation="false" %>

<asp:Content runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript" src="../js/jquery.timeentry.js"></script>

    <link href="../css/sumoselect.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.sumoselect.js" type="text/javascript"></script>

    <script>

        jQuery(document).ready(function ($) {
            $('body').find("input[id$='hdate']").datepicker();

            ApplyClass();

            $('body').on('click', '#addholiday', function () {
                $("#div_addholidays").show('slow');
                $("#hideholiday").show();
                $("#addholiday").hide();
                $('html, body').animate({ scrollTop: $('#div_addholidays').offset().top }, 2000);
            });

            $('body').on('click', '#hideholiday', function () {
                $("#div_addholidays").hide('slow');
                $("#addholiday").show();
                $("#hideholiday").hide();
                $("#htitle, #hdate").val("");
            });

            $(".holiday11").click(function () {
                event.preventDefault();

                var obj = $(this);
                var holiday_id = obj.closest('tr').find('[id$=hdnID]').val();

                $("[id$=lblmsg]").text("Are you sure you want to remove this holiday?");
                jq191('#divConfirmBox').dialog({
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            jq191(this).dialog("close");
                            IUD_Holiday(holiday_id);
                        },
                        "No": function () {
                            jq191(this).dialog("close");
                        }
                    }
                });
            })

            //Tabbed layout
            $('ul.tabs li').click(function () {
                var tab_id = $(this).attr('data-tab');

                $("[id$=btnSave]").css("display", "inline-block");

                if (tab_id == "tab-6" || tab_id == "tab-7")
                    //if (tab_id == "tab-4" || tab_id == "tab-6")
                    $("[id$=btnSave]").css("display", "none");

                $('ul.tabs li').removeClass('current');
                $('.tab-content').removeClass('current');

                $(this).addClass('current');
                $("#" + tab_id).addClass('current');

                sessionStorage.setItem("tabID", this);
            });

            //Sumo Select 
            <%--   var last_valid_selection = null;
            $(<%=lstEmployee.ClientID%>).change(function (event) {
                if ($(this).val().length > 1) {
                    alert('You can only choose 1 admin at a time!');
                    var $this = $(this);
                    $this[0].sumo.unSelectAll();
                    $.each(last_valid_selection, function (i, e) {
                        $this[0].sumo.selectItem($this.find('option[value="' + e + '"]').index());
                    });
                } else {
                    last_valid_selection = $(this).val();
                }
            });--%>

            $('body').find('#<%=lstEmployee.ClientID%>').change(function () {
                ////$(<%=lstAssignEmployee.ClientID%>,<%=lstEmployee.ClientID%>).change(function (event) {

                setEmployeeIds();
            });

            $('body').find('#<%=lstAssignEmployee.ClientID%>').change(function () {
                setEmployeeIds();
            });

            //End

            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        });

        function SumoSelectList() {
            $(<%=lstEmployee.ClientID%>).SumoSelect({ csvDispCount: 4, placeholder: 'Select Admin Employee' });
            $(<%=lstAssignEmployee.ClientID%>).SumoSelect({ selectAll: true, captionFormatAllSelected: "All selected!", palceholder: 'Select Employees' });
            $(<%=lstAssignEmployee.ClientID%>)[0].sumo.toggSelAll(true);
        }

        function setEmployeeIds() {
            $("[id$=hdnSelectedEmp]").val("");
            $("[id$=hdnAllEmp]").val("");

            var ids = "";

            //Assign selected values to Hidden field
            $.each($(lstAssignEmployee).val(), function (i, e) {
                ids += e + ",";
            });
            $("[id$=hdnSelectedEmp]").val(ids.slice(0, ids.length - 1));

            ids = "";
            //Get all employees option values 
            $("#lstAssignEmployee option").each(function () {
                ids += $(this).val() + ",";
            });
            $("[id$=hdnAllEmp]").val(ids.slice(0, ids.length - 1));
        }

        var last_valid_selection = null;
        var ischanged = '0';
        function SelectUnselect() {
            var $this = $(<%=lstEmployee.ClientID%>);

            if ($this.val() != null) {

                //if ($this.val().length > 1) {
                   <%-- alert('You can only choose 1 admin at a time!');
                    $this[0].sumo.unSelectAll();
                   
                    //$.each(last_valid_selection, function (i, e) {
                    //    $this[0].sumo.selectItem($this.find('option[value="' + e + '"]').index());
                    //});
                    ischanged = '1';
                    //Re-initialize sumo select & Initialize
                    reinitializeSumoselectList($(<%=lstAssignEmployee.ClientID%>));
                    $(<%=lstAssignEmployee.ClientID%>)[0].sumo.disable();--%>
                //}
                //else {
                last_valid_selection = $this.val();
                //EmployeeSelectionChange
                jQuery.ajax({
                    url: "settings.aspx/EmployeeSelectionChange",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{'id':'" + last_valid_selection + "','parentid':'" + GetParameterValues("id") + "'}",
                    async: true,
                    success: function (data) {

                        //Enable dropdown as it was disable on page load
                        $(<%=lstAssignEmployee.ClientID%>)[0].sumo.enable();

                        var recorddata = $.parseJSON(data.d);

                        //Re-initialize sumo select & Initialize
                        reinitializeSumoselectList($(<%=lstAssignEmployee.ClientID%>));

                              $.each(recorddata, function (key, value) {
                                  $(<%=lstAssignEmployee.ClientID%>)[0].sumo.add(value.ID, value.Title);
                            });

                            $(<%=lstAssignEmployee.ClientID%>)[0].sumo.selectAll();

                    },
                    error: function (response) {
                        openConfirmBox('Sorry an error has occurred. Please try again.');
                    }
                });
                //}
            }
            else {
                ischanged = '0';
                //Re-initialize sumo select & Initialize
                reinitializeSumoselectList($(<%=lstAssignEmployee.ClientID%>));
                      $(<%=lstAssignEmployee.ClientID%>)[0].sumo.disable();
            }

            return false;
        }


        function SelectUnselect111111() {

            //Re-initialize sumo select & Initialize
            reinitializeSumoselectList($(<%=lstAssignEmployee.ClientID%>));

            if (ischanged == '1')
                return false;

            var last_valid_selection = null;
            var $this = $(<%=lstEmployee.ClientID%>);

                  if ($this.val() != null) {
                      if ($this.val().length > 1) {
                          alert('You can only choose 1 admin at a time!');
                          ischanged = '1';
                          //$('[id$=lstAssignEmployee]')[0].sumo.toggSelAll(false);
                          //$('[id$=lstAssignEmployee]')[0].sumo.disable();
                          $this[0].sumo.unSelectAll();

                          //Re-initialize sumo select & Initialize
                   <%-- reinitializeSumoselectList($(<%=lstAssignEmployee.ClientID%>));--%>
                        $(<%=lstAssignEmployee.ClientID%>)[0].sumo.disable();

                        //$.each(last_valid_selection, function (i, e) {
                        //    $this[0].sumo.selectItem($this.find('option[value="' + e + '"]').index());
                        //});
                        return false;
                    }
                    else {
                        ischanged = '2';
                        last_valid_selection = $this.val();
                        $this[0].sumo.selectItem(last_valid_selection);
                        //EmployeeSelectionChange
                        ActivateAlertDiv('', 'AlertDiv');
                        jQuery.ajax({
                            url: "settings.aspx/EmployeeSelectionChange",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: "{'id':'" + last_valid_selection + "','parentid':'" + GetParameterValues("id") + "'}",
                            async: true,
                            success: function (data) {
                                ActivateAlertDiv('none', 'AlertDiv');
                                //Enable dropdown as it was disable on page load
                                $(<%=lstAssignEmployee.ClientID%>)[0].sumo.enable();

                                var recorddata = $.parseJSON(data.d);

                                //Re-initialize sumo select & Initialize
                                //reinitializeSumoselectList($(<%=lstAssignEmployee.ClientID%>));

                                $.each(recorddata, function (key, value) {
                                    $(<%=lstAssignEmployee.ClientID%>)[0].sumo.add(value.ID, value.Title);
                            });

                            $(<%=lstAssignEmployee.ClientID%>)[0].sumo.selectAll();

                            },
                            error: function (response) {
                                ActivateAlertDiv('none', 'AlertDiv');
                                openConfirmBox('Sorry an error has occurred. Please try again.');
                            }
                        });
                    }
                }

                return true;
            }

            function reinitializeSumoselectList(element) {
                var num = element[0].length;
                for (var i = num; i >= 1; i--) {
                    element[0].sumo.remove(i - 1);
                }

                //element.SumoSelect({ selectAll: true, captionFormatAllSelected: "All selected!", palceholder: 'Select Employees' });
                //element[0].sumo.toggSelAll(true);
                SumoSelectList();
            }

            function checkChange_msg() {
                if ($("[id*=chk_autoclock] input:checked").length !== 0) {
                    $('#divCustomBox').find('.custom_msg').remove();
                    $('#divCustomBox').append("<span class='custom_msg'><b>Note: You can set individual auto clock-in / out of each employee by going to their respective profiles.</b></span>");
                    openConfirmBox('Please save settings using the button at the bottom of the page to enable this feature.This feature automatically applies to all employees based on the work schedule you have defined above.');
                }
            }

            function ipVerification() {
                if (!$('[id$=chk_ipverification]').is(":checked")) {
                    $("[id$=lblmsg]").text("Allow Clock-in's on these IP Addresses only?");
                    jq191('#divConfirmBox').dialog({
                        modal: true,
                        buttons: {
                            "Yes": function () {
                                $('[id$=chk_ipverification]').prop('checked', true);
                                $('[id$=btnSave]').trigger('click');
                                jq191(this).dialog("close");
                            },
                            "No": function () {
                                jq191(this).dialog("close");
                            }
                        }
                    });
                }
            }

            function tabClick(tabId) {
                $('ul.tabs li').removeClass('current');
                $('.tab-content').removeClass('current');

                var tab_id = $(tabId).attr('data-tab');

                $(tabId).addClass('current');
                $("#" + tab_id).addClass('current');
            }

            function IUD_Holiday(Id) {
                jQuery.ajax({
                    url: "settings.aspx/IUD_Holidays",
                    type: "POST",
                    data: "{'action':'del','Id':'" + Id + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    success: function () {
                        //master page function
                        //$("#div_holidays").load(window.location.href);
                        __doPostBack('holidays', '');
                        //jQuery.ajax({
                        //    url: "settings.aspx/reloadHolidays",
                        //    type: "POST",
                        //    contentType: "application/json; charset=utf-8",
                        //    dataType: "json",
                        //    async: true,
                        //    success: function (res) {                           
                        //        console.log(res);
                        //    },
                        //    error: function (response) {
                        //        openConfirmBox('Sorry an error has occurred. Please try again.');
                        //    }
                        //});
                        openConfirmBox('Holiday removed!');
                        $('ul.tabs li').trigger('click');
                    },
                    error: function (response) {
                        openConfirmBox('Sorry an error has occurred. Please try again.');
                    }
                });
            }

            function ApplyClass() {
                $(".check-time").timeEntry({
                    defaultTime: "",
                    placeholder: "HH:MM AM",
                    ampmPrefix: ' '
                });
                $('.txtdatetime').datepicker();
            }

            function showhide(Id) {
                var check = $("#<%=chkclockbefore.ClientID %>").is(':checked');
            if (check)
                $("#" + Id).show();
            else
                $("#" + Id).hide();
        }

        function validate() {

            if ($("[id$=lstEmployee]").val() != null)
                if ($("[id$=lstAssignEmployee]").val() == null) {
                    openConfirmBox("Assign Employees to Admin");
                    return false;
                }

            return true;
        }

        HTMLInputElement.prototype.startsWith = function () { return false; };

    </script>

    <style>
        .SumoSelect .select-all {
            height: inherit !important;
        }

        .holidays-form {
            margin: 20px 0;
            padding: 5px;
            background: #f4f4f4;
            width: 360px;
        }

            .holidays-form label {
                margin: 0px;
            }

            .holidays-form .f-2 {
                margin: 0px;
                height: 25px;
                max-width: 400px;
                padding: 2px 5px;
                margin-bottom: 10px;
            }

            .holidays-form .btn {
                margin-top: 10px;
            }

            .holidays-form #cye {
                margin: 2px 5px 0 0;
                float: left;
            }

        .labels {
            font-weight: bold;
        }

        /*Tabbed Layout*/
        .container {
            width: 1500px;
            margin: 0 auto;
        }

        ul.tabs {
            margin: 0px;
            padding: 0px;
            list-style: none;
        }

            ul.tabs li {
                border-radius: 3px;
                border: aliceblue 1px solid;
                background: #e8e7e7;
                color: #222;
                display: inline-block;
                padding: 10px 40px;
                cursor: pointer;
                font-weight: bold;
            }

                ul.tabs li.current {
                    font-weight: bold;
                    background: #3e8ad2;
                    color: white;
                }

        .tab-content {
            display: none;
            padding: 15px;
        }

            .tab-content.current {
                display: inherit;
            }
        /*Tabbed Layout*/
        .btn-primary {
            cursor: pointer !important;
        }

        .btn-save {
            font-size: 0.9rem !important;
            margin: 20px 0px 0px 25px;
        }

        .btn-add {
            margin-left: 20px;
        }

        .smallGrid {
            width: 310px !important;
        }

        body hr {
            font-weight: bold;
            width: 75% !important;
        }

        #tab-1 label, .sumoselect_label {
            width: 120px !important;
            text-align: left !important;
        }

        input[type=text], select {
            display: inline-block;
            padding: 6px;
            border: 1px solid #b7b7b7;
            border-radius: 3px;
            font: arial, helvetica, sans-serif;
        }

        input[type="checkbox"] {
            cursor: pointer;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            outline: 0;
            background: lightgray;
            height: 16px;
            width: 16px;
            border: 1px solid white;
        }

            input[type="checkbox"]:checked {
                background: #4c8a3f;
            }

            input[type="checkbox"]:hover {
                filter: brightness(90%);
            }

            input[type="checkbox"]:disabled {
                background: #e6e6e6;
                opacity: 0.6;
                pointer-events: none;
            }

            input[type="checkbox"]:after {
                content: '';
                position: relative;
                left: 40%;
                top: 20%;
                width: 15%;
                height: 40%;
                border: solid #fff;
                border-width: 0 2px 2px 0;
                transform: rotate(45deg);
                display: none;
            }

            input[type="checkbox"]:checked:after {
                display: block;
            }

            input[type="checkbox"]:disabled:after {
                border-color: #7b7b7b;
            }

        #chkWeekend label {
            margin: 6px 8px;
        }

        .addbtn {
            height: 35px;
            margin-left: 280px;
            cursor: pointer;
            background: #3d7bb5;
            color: white;
        }

        .mycheckbox input[type="checkbox"] {
            margin-right: 5px;
        }
    </style>

    <asp:ScriptManager runat="server" ID="scriptmanager1"></asp:ScriptManager>
    <asp:HiddenField runat="server" ID="Option" ClientIDMode="Static" />
    <div class="space">
        <div class="Clear"></div>


        <!--container-->
        <div class="container">
            <ul class="tabs">
                <li class="tab-link current" data-tab="tab-1">Basic</li>
                <li class="tab-link" data-tab="tab-2">Holidays & Weekends</li>
                <li class="tab-link" data-tab="tab-3">Advance</li>
                <li class="tab-link" data-tab="tab-4">Admin</li>
                <li class="tab-link" data-tab="tab-5">Restrictions</li>
                <li class="tab-link" data-tab="tab-6">Notifications</li>
                <li class="tab-link" data-tab="tab-7">Tags</li>
            </ul>

            <div id="tab-1" class="tab-content current">
                <asp:UpdatePanel runat="server" ID="updatepanel1">
                    <ContentTemplate>
                        <h6>Personal Info</h6>
                        <hr />
                        <label class="labels">Full Name</label>
                        <asp:TextBox ID="txtTitle" CssClass="f-2" runat="server" MaxLength="150" ValidationGroup="setting" ClientIDMode="Static"></asp:TextBox>
                        <br />
                        <div class="information" style="float: left !important;" id="info" runat="server" clientidmode="static">
                            <div style="display: inline-flex !important; padding-top: 5px !important; padding-bottom: 5px !important;">
                                <label class="labels">Timezone</label>
                                <asp:DropDownList runat="server" ID="ddlTimzone" ValidationGroup="setting" CssClass="ddl f-2" ClientIDMode="Static" Style="width: auto !important; margin-left: 44px !important;" OnSelectedIndexChanged="ddlTimzone_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:HiddenField runat="server" ID="hdntimezone_check" />
                                <span class="about-block" runat="server" style="margin-left: 7px !important;">Time is calculated based on your timezone. Edit your timezone to update time used for clocking-in and out.</span>
                            </div>
                            <br />
                            <label class="labels">Daylight Savings </label>
                            <asp:DropDownList ID="ddldlsaving" runat="server" CssClass="standardSelect" AutoPostBack="True" OnSelectedIndexChanged="ddldlsaving_SelectedIndexChanged">
                                <asp:ListItem Selected="True" Text="Not Set" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Add" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Subtract" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddldlshours" runat="server" CssClass="standardSelect" AutoPostBack="True" Enabled="False">
                                <asp:ListItem Selected="True" Value="30">30 Minutes</asp:ListItem>
                                <asp:ListItem Value="60">1 Hour</asp:ListItem>
                                <asp:ListItem Value="90">1 Hour 30 Minutes</asp:ListItem>
                                <asp:ListItem Value="120">2 Hours</asp:ListItem>
                                <asp:ListItem Value="150">2 Hours 30 Minutes</asp:ListItem>
                                <asp:ListItem Value="180">3 Hours</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ddldlsaving" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

            <div id="tab-2" class="tab-content">
                <asp:UpdatePanel runat="server" ID="holidays_panel">
                    <ContentTemplate>
                        <h6>Weekend</h6>
                        <hr />
                        <asp:CheckBoxList runat="server" ID="chkWeekend" ClientIDMode="Static" RepeatLayout="Flow" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Monday" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Tuesday" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Wednesday" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Thursday" Value="4"></asp:ListItem>
                            <asp:ListItem Text="Friday" Value="5"></asp:ListItem>
                            <asp:ListItem Text="Saturday" Value="6"></asp:ListItem>
                            <asp:ListItem Text="Sunday" Value="0"></asp:ListItem>
                        </asp:CheckBoxList>

                        <br />
                        <br />
                        <br />
                        <br />

                        <h6>Holidays&nbsp;<a href="#" id="addholiday" style="font-size: small !important;"><i class="fa fa-fw fa-plus-circle"></i>Add</a>
                            <a href="#" id="hideholiday" style="font-size: small !important; display: none;"><i class="fa fa-fw fa-minus-circle"></i>Hide</a>
                        </h6>
                        <hr />

                        <div id="div_addholidays" style="display: none;">
                            <div class="holidays-form">
                                <div>
                                    <label>Title</label>
                                    <asp:TextBox ID="htitle" runat="server" MaxLength="150" ValidationGroup="hd" CssClass="f-2" ClientIDMode="Static"></asp:TextBox>
                                </div>
                                <div id="hdateContainer">
                                    <label>Date</label>
                                    <asp:TextBox ID="hdate" runat="server" ValidationGroup="hd" CssClass="f-2" ClientIDMode="Static"></asp:TextBox>
                                </div>
                                <div id="hyc">
                                    <asp:CheckBox runat="server" ID="cye" Text="Every Year" ClientIDMode="Static" CssClass="mycheckbox" />
                                </div>
                                <div>
                                    <asp:Button ID="hAdd" runat="server" Text="Add" ValidationGroup="hd" OnClick="hAdd_Click" ClientIDMode="Static" CssClass="btn addbtn" />
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12" style="padding: 0px;" id="div_holidays">
                            <asp:Repeater runat="server" ID="rpthd" OnItemDataBound="rpthd_ItemDataBound" OnItemCommand="rpthd_ItemCommand">
                                <HeaderTemplate>
                                    <table class="table-grid-sample1 table-responsive">
                                        <tr class="smallGridHead">
                                            <td>Title</td>
                                            <td>Date/Day</td>
                                            <td>Every Year</td>
                                            <td></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="smallGrid">
                                        <td>
                                            <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("id") %>' ClientIDMode="Static" />
                                            <asp:Label runat="server" ID="hdtitle" Text='<%# Eval("title") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="hdDate" Text='<%# Eval("date") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="ey" Text='<%# Eval("Yearly") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <%--<asp:LinkButton runat="server" ID="del" CommandName="del" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete?')"><i class="fa fa-fw fa-trash"></i></asp:LinkButton>--%>
                                            <asp:LinkButton runat="server" ID="del" CommandName="del" Text="Delete" CssClass="holiday"><i class="fa fa-fw fa-trash red"></i></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                               
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:Label runat="server" ForeColor="red" Text="No Record Found." Visible="False" ID="hdmsg"></asp:Label>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <%--  <asp:AsyncPostBackTrigger ControlID="hAdd" EventName="Click" />
                        <asp:PostBackTrigger ControlID="rpthd"/>--%>
                    </Triggers>
                </asp:UpdatePanel>
            </div>

            <div id="tab-3" class="tab-content">
                <span class="about-block" runat="server">These settings can be useful for assessing if an employee has completed his hours based on your office timings or if they are doing undertime.<br />
                    Once these settings are enabled, each employee will see a balance time which is either in red (undertime) or green (overtime)
                   
                    <br />
                    based on metrics you provide in the forms here.</span>
                <br />
                <br />
                <label class="labels">Absences allowed in a year </label>
                <asp:DropDownList runat="server" ID="ddlAbsences" ValidationGroup="setting" CssClass="ddl f-2" ClientIDMode="Static" Style="width: auto !important;">
                </asp:DropDownList>
                <br />
                <asp:UpdatePanel runat="server" ID="UpdatePanel3" ChildrenAsTriggers="true">
                    <ContentTemplate>
                        <label class="labels">Working hours in a day&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
                        <asp:DropDownList runat="server" ID="ddlWorkinghours" ValidationGroup="setting" CssClass="ddl f-2" ClientIDMode="Static" Style="width: auto !important;">
                        </asp:DropDownList>
                        <br />

                        <pre>
                            <label class="labels">OR</label></pre>
                        <asp:CheckBox runat="server" ID="chk_enableworkhours" Text=" Enable working hours definition?" AutoPostBack="true" OnCheckedChanged="chk_enableworkhours_CheckedChanged" CssClass="mycheckbox" />

                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12" style="padding: 0px;" id="div_workinghours" runat="server" visible="false">
                            <br />
                            <h6>Working Hours Definition </h6>
                            <asp:Repeater runat="server" ID="rpt_workinghours" OnItemDataBound="rpt_workinghours_ItemDataBound">
                                <HeaderTemplate>
                                    <table class="table-grid-sample1">
                                        <tr class="smallGridHead">
                                            <td>Day</td>
                                            <td colspan="2">Working Hours</td>
                                            <td colspan="2">Break Hours</td>
                                        </tr>
                                        <tr class="smallGridHead" style="font-weight: bold;">
                                            <td></td>
                                            <td>Clockin</td>
                                            <td>Clockout</td>
                                            <td>To</td>
                                            <td>From</td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="smallGrid">
                                        <td>
                                            <asp:HiddenField runat="server" ID="hdn_check" />
                                            <asp:Label runat="server" ID="lblDay" Text='<%# DataBinder.Eval(Container.DataItem, "DayTitle").ToString() %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtClockin" CssClass="check-time" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "Clockin").ToString()).ToString("hh:mm tt") %>'></asp:TextBox>
                                            <%--   <asp:Label runat="server" ID="lblClockIn" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "Clockin").ToString()).ToString("hh:mm tt") %>'></asp:Label>--%>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtClockout" CssClass="check-time" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "Clockout").ToString()).ToString("hh:mm tt") %>'></asp:TextBox>
                                            <%--     <asp:Label runat="server" ID="ClockOut" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "Clockout").ToString()).ToString("hh:mm tt") %>'></asp:Label>--%>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtBreakin" CssClass="check-time" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "BreakStart").ToString()).ToString("hh:mm tt") %>'></asp:TextBox>
                                            <%--   <asp:Label runat="server" ID="Label2" Text='<%#  Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "BreakStart").ToString()).ToString("hh:mm tt") %>'></asp:Label>--%>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtBreakout" CssClass="check-time" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "BreakEnd").ToString()).ToString("hh:mm tt") %>'></asp:TextBox>
                                            <%--   <asp:Label runat="server" ID="Label3" Text='<%#  Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "BreakEnd").ToString()).ToString("hh:mm tt") %>'></asp:Label>--%>
                                        </td>
                                        <%--<td>
                                                        <asp:LinkButton ID="lnkEdit" runat="server"></asp:LinkButton>
                                                    </td>--%>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                               
                                </FooterTemplate>
                            </asp:Repeater>

                            <br />

                            <%--                                        <div runat="server" ID="div_chophours" visible="true">--%>
                            <%--                                        <asp:Label runat="server" ID="lblclockbefore" Font-Bold="true"></asp:Label>--%>
                            <asp:CheckBox runat="server" ID="chkclockbefore" Font-Bold="true" Text="Donot allow clock-in before defined clock-in time above." onchange="showhide('Exclock');" CssClass="mycheckbox" />
                            <br />
                            <div style="display: inline-flex; display: none;" id="Exclock">
                                <label>Chop hours from: </label>
                                <asp:TextBox runat="server" ID="txtclockbefore" CssClass="check-time"></asp:TextBox>
                                <span class="about-block">HH:MM AM/PM</span>
                            </div>
                            <br />
                            <%--                                        </div>--%>
                        </div>

                        <div runat="server" id="div_autoclock">
                            <h6>Auto Clock-in/out</h6>
                            <hr />
                            <span class="about-block" runat="server">You can enable auto clock-in / out if you set your working hours definition.</span>
                            <br />
                            <br />
                            <asp:CheckBoxList ID="chk_autoclock" runat="server" ClientIDMode="Static" CssClass="mycheckbox" onchange="checkChange_msg();">
                                <asp:ListItem Text=" Enable Auto Clock-in" Value="1"></asp:ListItem>
                                <asp:ListItem Text=" Enable Auto Clock-out" Value="2"></asp:ListItem>
                            </asp:CheckBoxList>
                        </div>

                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="chk_enableworkhours" EventName="CheckedChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <br />

                <asp:UpdatePanel runat="server" ID="UpdatePanel4">
                    <ContentTemplate>
                        <div class="Tab ExpectionalDays" id="ExpectionalDays" runat="server" style="margin-top: 5px !important; margin-left: 0px !important;">
                            <asp:Label ID="h5ExceptionalDays" runat="server" Style="font-weight: bold;">Days with different working hours (then the ones defined above)
                        -
                       
                                <asp:LinkButton ID="lnkExNew" runat="server" OnClick="lnkExNew_Click" Text="Add new"></asp:LinkButton>
                            </asp:Label>
                            <table style="width: 314px; border: 1px solid #999;">
                                <tbody>
                                    <tr class="smallGrid">
                                        <td class="auto-style3">Title </td>
                                        <td class="auto-style1">
                                            <asp:TextBox ID="txtExTitle" runat="server" MaxLength="150" ValidationGroup="l" Width="124px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtExTitle" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="5" InitialValue="">Required</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr class="smallGrid">
                                        <td class="auto-style3">Start Date</td>
                                        <td class="auto-style1">
                                            <asp:TextBox ID="txtExFrom" CssClass="txtdatetime" runat="server" MaxLength="150" ValidationGroup="l" Width="124px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtExFrom" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="5" InitialValue="">Required</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr class="smallGrid">
                                        <td class="auto-style3">End Date</td>
                                        <td class="auto-style1">
                                            <asp:TextBox ID="txtExTo" CssClass="txtdatetime" runat="server" MaxLength="150" ValidationGroup="l" Width="124px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtExTo" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="5" InitialValue="">Required</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr class="smallGrid">
                                        <td class="auto-style3">Clock In</td>
                                        <td class="auto-style1">
                                            <asp:TextBox ID="txtExClockIn" CssClass="check-time" runat="server" MaxLength="150" ValidationGroup="l" Width="124px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtExClockIn" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="5" InitialValue="">Required</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr class="smallGrid">
                                        <td class="auto-style3">Clock Out</td>
                                        <td class="auto-style1">
                                            <asp:TextBox ID="txtExClockOut" CssClass="check-time" runat="server" MaxLength="150" ValidationGroup="l" Width="124px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtExClockOut" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="5" InitialValue="">Required</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <%-- <tr class="smallGrid">
                                        <td class="auto-style3">Eliminate Breaks</td>
                                        <td class="auto-style1">
                                            <asp:CheckBox ID="chkEliminateBreaks" runat="server" Text="" />
                                        </td>
                                    </tr>--%>
                                    <tr class="smallGrid">
                                        <td class="auto-style3"></td>
                                        <td class="auto-style1">
                                            <asp:Button ID="btnAddExceptionalDays" runat="server" Text="Add" ValidationGroup="5" Width="50px" OnClick="btnAddExceptionalDays_Click" />
                                            <asp:HiddenField ID="hdnGroupID" runat="server" Value="0" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <br />
                            <asp:Repeater runat="server" ID="rptExceptionalDays" OnItemCommand="rptExceptionalDays_ItemCommand">
                                <HeaderTemplate>
                                    <table class="table-grid-sample1">
                                        <tr class="smallGridHead">
                                            <td>Title</td>
                                            <td>From</td>
                                            <td>To</td>
                                            <td>Clockin</td>
                                            <td>Clockout</td>
                                            <td>CrtDate</td>
                                            <td>ModDate</td>
                                            <td></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="smallGrid">
                                        <td>
                                            <asp:Label runat="server" ID="lblGroupName" Text='<%# DataBinder.Eval(Container.DataItem, "GroupName").ToString() %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblStartDate" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "StartDate").ToString()).ToShortDateString() %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblEndDate" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "EndDate").ToString()).ToShortDateString() %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblClockIn" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "ClockIn")).ToShortTimeString() %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblClockOut" Text='<%# Convert.ToDateTime((DataBinder.Eval(Container.DataItem, "ClockOut"))).ToShortTimeString()%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblCrtDate" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "CrtDate").ToString()).ToShortDateString() %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblModDate" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "ModDate").ToString()).ToShortDateString() %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "GroupID").ToString()%>' CommandName="Edit"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "GroupID").ToString()%>' CommandName="Delete"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                               
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <br />
                <br />

                <div style="display: none">
                    <asp:Label ID="h1" runat="server" Style="font-weight: bold;">Extra break -
                       
                        <asp:LinkButton ID="lnkExBrk" runat="server" Text="Add new"></asp:LinkButton></asp:Label>
                    <br />
                    <asp:Label runat="server" Text="Additional break per "></asp:Label>
                    <asp:DropDownList runat="server" ID="ddlAdditionalBrk">
                        <asp:ListItem Text="Month" Value="month"></asp:ListItem>
                        <asp:ListItem Text="Week" Value="week"></asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;of&nbsp;
                                   
                    <asp:DropDownList runat="server" ID="ddlAdditionalBreak">
                    </asp:DropDownList>
                    <span class="help-block" runat="server">Minutes</span>
                    <br />
                </div>
            </div>

            <div id="tab-4" class="tab-content">

                <!--additional email grid-->
                <div runat="server" id="divadditionalemails">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel2">
                        <ContentTemplate>
                            <div>
                                <h6>Additional emails for notifications</h6>
                                <hr />
                                <div style="display: inline-flex;">
                                    <asp:TextBox runat="server" ID="txt_additionalemails" Width="250" ValidationGroup="setting"></asp:TextBox>
                                    <asp:Button ID="btn_additionalemails" runat="server" Text="Add" OnClick="btn_additionalemails_Click" ValidationGroup="setting" CssClass="btn btn-primary btn-add" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hdn_additionalemails"></asp:HiddenField>
                                    <br />
                                    <asp:RegularExpressionValidator runat="server" ID="validator" SetFocusOnError="true" ErrorMessage=" Please enter valid pattern" Font-Bold="true" ForeColor="Red" ValidationExpression="(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?))*" ControlToValidate="txt_additionalemails" ValidationGroup="setting"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div>
                                <asp:Repeater runat="server" ID="rpt_additionalemails" OnItemCommand="rpt_additionalemails_ItemCommand">
                                    <HeaderTemplate>
                                        <table class="smallGrid">
                                            <tr class="smallGridHead">
                                                <td>Email
                                                </td>
                                                <td></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class="smallGrid">
                                            <td>
                                                <asp:Label runat="server" ID="lbliptitle" Text='<%# Eval("email") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" ID="lnkdel" CommandName="del" OnClientClick="return  confirm('Are you sure you want to delete')" CommandArgument='<%# Eval("email") %>' Text="Delete"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                   
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                            <br />
                            <div id="div_adminemployees" runat="server">
                                <h6>Make an admin</h6>
                                <hr />
                                <span class="about-block" runat="server">You can select employee(s) from to make them as Admin & assign them All or selective employee to manage.<br />
                                    There can be multiple admins but only one can be selected at a time.
                                </span>
                                <br />
                                <br />
                                <label class="labels sumoselect_label">Select Admin</label>
                                <asp:ListBox ID="lstEmployee" runat="server" SelectionMode="Multiple" ClientIDMode="Static" onchange="return SelectUnselect();"></asp:ListBox>
                                <br />
                                <label class="labels sumoselect_label">Assign Employees</label>
                                <asp:ListBox ID="lstAssignEmployee" runat="server" SelectionMode="Multiple" ClientIDMode="Static"></asp:ListBox>
                                <asp:HiddenField runat="server" ID="hdnSelectedEmp" ClientIDMode="Static" />
                                <asp:HiddenField runat="server" ID="hdnAllEmp" ClientIDMode="Static" />
                                <%-- <asp:DropDownList runat="server" ID="ddlemployees"></asp:DropDownList>
                                <asp:CheckBoxList runat="server" ID="assignEmployeesList"></asp:CheckBoxList>--%>
                                <br />
                                <br />
                                <asp:Repeater ID="rpt_adminEmployee" runat="server" OnItemCommand="rpt_adminEmployee_ItemCommand" OnItemDataBound="rpt_adminEmployee_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="smallGrid">
                                            <tr class="smallGridHead">
                                                <td>Admin
                                                </td>
                                                <td>Employee(s) Assigned
                                                </td>
                                                <td></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class="smallGrid">
                                            <td>
                                                <asp:Label runat="server" ID="lbladminTitle" Text='<%# Eval("adminTitle") %>'></asp:Label>

                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblemployees"></asp:Label>
                                                <asp:HiddenField runat="server" ID="hdnemployeesID" />
                                            </td>
                                            <td>
                                                <asp:HiddenField runat="server" ID="hdnadminID" Value='<%# Eval("adminID") %>' />
                                                <asp:HiddenField runat="server" ID="hdnadminEmail" Value='<%# Eval("adminEmail") %>' />
                                                <asp:LinkButton runat="server" ID="lnkdel" CommandName="del" OnClientClick="return  confirm('Are you sure you want to revoke admin rights?')" CommandArgument='<%# Eval("adminID") %>' Text="Delete"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                   
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Label ID="lblempty_adminEmployee" runat="server" Text="No Admin Added"></asp:Label>

                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btn_additionalemails" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="lstEmployee" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>

            <div id="tab-5" class="tab-content">
                <h6>Clock-in/out time edit</h6>
                <hr />
                <asp:RadioButtonList ID="rbtn_allowedit" runat="server" ClientIDMode="Static">
                    <asp:ListItem Text=" Allow employees to edit their clock-in/out time entries" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text=" Do not allow employees to edit their clock-in/out time entries" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
                <br />
                <div runat="server" id="divaddipaddress" visible="false">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel5">
                        <ContentTemplate>
                            <div>
                                <h6>Verification through IP information</h6>
                                <hr />
                                <asp:CheckBox ID="chk_ipverification" runat="server" ClientIDMode="Static" Text="Allow Clock-in's on these IP Addresses only" onchange="openConfirmBox('Please Save Settings to apply the changes.');" CssClass="mycheckbox" />
                                <br />
                                <table>
                                    <tr class="smallGrid">
                                        <td>Title </td>
                                        <td class="auto-style2">
                                            <asp:TextBox runat="server" ID="ipTitle" MaxLength="150"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ipTitle" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="ip"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr class="smallGrid">
                                        <td>Add Dedicated IP </td>
                                        <td class="auto-style2">
                                            <asp:TextBox runat="server" ID="txtipaddress" ValidationGroup="ip"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtipaddress" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="ip"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr class="smallGrid">
                                        <td></td>
                                        <td class="auto-style2">
                                            <asp:Button runat="server" ID="btnsubmit" Text="Add" OnClick="BtnaddClick" Width="50px" ValidationGroup="ip" />

                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                                <asp:Repeater runat="server" ID="rptripaddress" OnItemDataBound="RptrItemdatabound" OnItemCommand="RptrItemcommand">
                                    <HeaderTemplate>
                                        <table class="smallGrid">
                                            <tr class="smallGridHead">
                                                <td>Title
                                                </td>
                                                <td>IP Address
                                                </td>
                                                <td></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class="smallGrid">
                                            <td>
                                                <asp:Label runat="server" ID="lbliptitle" Text='<%# Eval("title") %>'></asp:Label>

                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblipaddress" Text='<%# Eval("ipaddress") %>'></asp:Label>

                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" ID="lnkdel" CommandName="del" OnClientClick="return  confirm('Are you sure you want to delete')" CommandArgument='<%# Eval("ID") %>' Text="Delete"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        </table>
                                   
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Label runat="server" ForeColor="red" Text="No Record Found." Visible="False" ID="lblempty"></asp:Label>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>

            <div id="tab-6" class="tab-content">
                <asp:UpdatePanel runat="server" ID="updatepanel6">
                    <ContentTemplate>
                        <h6>Email Notifications</h6>
                        <hr />
                        <asp:Repeater ID="rpt_notifications" runat="server">
                            <HeaderTemplate>
                                <table style="width: 800px;" class="table-grid-sample1">
                                    <tbody>
                                        <tr>
                                            <td colspan="2">Type</td>
                                            <td>Enabled</td>
                                        </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:HiddenField runat="server" ID="hdn_EmailNotifications_ID" Value='<%# Eval("EmailNotifications_ID") %>' />
                                        <asp:Label runat="server" ID="lbl_notificationName" Font-Bold="true" CssClass="checkbox-inline" Text='<%# Eval("Name") %>'>                                          
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lnk_notificationDesc" Text='<%# Eval("Description") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:CheckBox runat="server" ID="chk_notificationEnabled" Checked='<%# Convert.ToBoolean(Eval("isEnabled")) %>' data-id='<%# Eval("ID") %>' OnCheckedChanged="chk_notificationEnabled_CheckedChanged" AutoPostBack="true" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody> </table>
                           
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:Label runat="server" ForeColor="red" Text="No Record Found." Visible="False" ID="notificatio_msg"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

            <div id="tab-7" class="tab-content">
                <asp:UpdatePanel runat="server" ID="UpdatePanel7">
                    <ContentTemplate>
                        <div>
                            <h6>Add Tags</h6>
                            <hr />
                            <span class="about-block" runat="server">
                                Add Tags here, which is reflect in employee Attendence.
                            </span>
                            <br />
                            <br />
                            <table>
                                <tr class="smallGrid">
                                    <td>Tag </td>
                                    <td class="auto-style2">
                                        <asp:TextBox runat="server" ID="txtTag" MaxLength="150"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTag" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="tag"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr class="smallGrid">
                                    <td></td>
                                    <td class="auto-style2">
                                        <asp:Button runat="server" ID="btnAddTag" Text="Add" OnClick="btnAddTag_Click" Width="50px" ValidationGroup="tag" />

                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <asp:Repeater runat="server" ID="reptTags" OnItemCommand="reptTags_ItemCommand">
                                <HeaderTemplate>
                                    <table class="smallGrid">
                                        <tr class="smallGridHead">
                                            <td>Tags
                                            </td>
                                            <td></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="smallGrid">
                                        <td>
                                            <asp:Label runat="server" ID="lbliptitle" Text='<%# Eval("Tags") %>'></asp:Label>

                                        </td>
                                        <td>
                                            <asp:LinkButton runat="server" ID="lnkdel" CommandName="del" OnClientClick="return  confirm('Are you sure you want to delete')" CommandArgument='<%# Eval("ID") %>' Text="Delete"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>

                                <FooterTemplate>
                                    </table>
                                   
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:Label runat="server" ForeColor="red" Text="No Record Found." Visible="False" ID="lblTags"></asp:Label>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <br />
            <br />
            <asp:Button ID="btnSave" runat="server" Text="Save Settings" ValidationGroup="setting" CssClass="btn btn-primary btn-save" OnClick="btnSave_Click" ClientIDMode="Static" OnClientClick="return validate();" />
            <!-- container -->
            <%--</ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddldlsaving" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="chk_enableworkhours" EventName="CheckedChanged" />
                </Triggers>
            </asp:UpdatePanel>--%>
        </div>
    </div>

    <div id="divConfirmBox" title="Confirm" style="display: none">
        <p>
            <asp:Label runat="server" ID="lblmsg" Text=""></asp:Label>
        </p>
    </div>
    <asp:HiddenField runat="server" ID="hdntype" />

</asp:Content>
