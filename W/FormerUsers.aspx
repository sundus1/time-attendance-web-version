﻿<%@ Page Title="" Language="C#" MasterPageFile="~/W/MasterPage.master" AutoEventWireup="true" CodeFile="FormerUsers.aspx.cs" Inherits="W_FormerUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script>
        function RestoreUsers() {
            event.preventDefault();
            var i = 0;
            $('.cb_select').each(function () {
                if ($(this).find('[name$=chkselecteditem]').is(':checked'))
                { i++; }
            });

            if (i > 0) {
                jQuery("[id$=lblcustommsg]").text("Are you sure you want to restore selected employee(s)?");
                var dlg = jq191("#divCustomBox").dialog({
                    resizable: false,
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            jq191(this).dialog("close");
                            __doPostBack("lnkRestoreUser", 'OnClick');
                        },
                        "No": function () {
                            jq191(this).dialog("close");
                            return false;
                        }
                    }
                });
                dlg.parent().appendTo(jQuery("form:first"));
                dlg.parent().css("z-index", "1000");
            }
            else {
                jQuery("[id$=lblcustommsg]").text("Please select an employee to restore.");
                var dlg = jq191("#divCustomBox").dialog({
                    resizable: false,
                    modal: true,
                    buttons: {
                        "Ok": function () {
                            jq191(this).dialog("close");
                            return false;
                        }
                    }
                });
                dlg.parent().appendTo(jQuery("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            }
        }

        function confirmBox() {
            jQuery("[id$=lblcustommsg]").text("Employee restored successfully!");
            var dlg = jq191("#divCustomBox").dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "Close": function () {
                        jq191(this).dialog("close");
                        return false;
                    }
                }
            });
            dlg.parent().appendTo(jQuery("form:first"));
            dlg.parent().css("z-index", "1000");

            return false;
        }
    </script>

    <asp:ScriptManager runat="server" ID="scriptmanager1"></asp:ScriptManager>

    <ol class="breadcrumb bread-list">
        <li class="breadcrumb-item" runat="server" id="restoreuser_link">
            <asp:LinkButton runat="server" ID="lnkRestoreUser" ClientIDMode="Static" Text="Restore Employee(s)" OnClientClick="return RestoreUsers();" OnClick="lnkRestoreUser_Click"></asp:LinkButton>
        </li>
    </ol>

    <div id="UsersDiv" runat="server">
        <h4></h4>
        <asp:UpdatePanel runat="server" ID="users_panel">
            <ContentTemplate>
                <asp:Repeater runat="server" ID="rpthd" OnItemDataBound="rpthd_ItemDataBound" OnItemCommand="rpthd_ItemCommand">
                    <HeaderTemplate>
                        <table class="table-grid-sample1 table-responsive">
                            <tr class="smallGridHead">
                                <td></td>
                                <td>Title
                                </td>
                                <td>Email
                                </td>
                                <td>Removed On
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="smallGrid">
                            <td>
                                <asp:CheckBox ID="chkselecteditem" runat="server" CssClass="cb_select" ClientIDMode="Static" />
                                <asp:HiddenField runat="server" ID="hf_ID" Value='<%# Eval("ID") %>' ClientIDMode="Static" />
                                <asp:HiddenField runat="server" ID="hf_parentid" Value='<%# Eval("parentid") %>' ClientIDMode="Static" />
                            </td>
                            <td>
                                <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ID") %>' ClientIDMode="Static" />
                                <asp:LinkButton runat="server" ID="lblTitle" Text='<%# Eval("Title") %>' CommandArgument='<%# Eval("ID") %>' CommandName="openpopup" ClientIDMode="Static"></asp:LinkButton>
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblEmail" Text='<%# Eval("email") %>' ClientIDMode="Static"></asp:Label>
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblDateAdded" Text='<%# Eval("datefield") %>' ClientIDMode="Static"></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <br />
                <asp:Label runat="server" Text="No Record Found." Visible="False" ID="hdmsg" Font-Bold="true"></asp:Label>
            </ContentTemplate>
            <Triggers>
                <%--                <asp:AsyncPostBackTrigger ControlID="AddUser" EventName="Click" />--%>
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <div id="divCustomBox" title="Confirm" style="display: none">
        <p>
            <asp:Label runat="server" ID="lblcustommsg" Text=""></asp:Label>
        </p>
    </div>

    <asp:HiddenField runat="server" ID="hdnTitle" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnUserID" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnInstanceId" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnUserEmail" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnUserPassword" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnIsActive" ClientIDMode="Static" />
</asp:Content>

