﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="YearlyReport.ascx.cs" Inherits="Controls_YearlyReport" %>
<script>
    $(document).ready(function () {
        if ($('#hdnData').val() != "true") {
            $('.trdata').hide();
        }
        else {
            $('.trdata').show();
        }
    });
</script>
<section id="secprofile" style="margin-top: 5px">
    <h1 class="username">
        <asp:Label ID="lblUserName" runat="server" Text="Derp" Style="margin-bottom: 5px;"></asp:Label>
        &nbsp;<asp:LinkButton runat="server" ID="bk" Text="Back to reports" Style="font-size: 12px;" OnClick="bk_Click"></asp:LinkButton>
    </h1>
    <div style="clear: both">
    </div>
    <div style="clear: both">
        <div style="text-align: left">
            <div id="filtersContainer" runat="server" visible="false">
                <span id="tmC" style="display: none">Start Time:
                    <input type="text" id="time" name="time" style="width: 85px" /><input type="button" value="Show" id="btntC" style="margin-top: 2px" />
                </span>
            </div>
        </div>
    </div>

    <table class="profile" style="margin-top: 12px">       
         <tr id="trSelUser" runat="server" >
            <td colspan="2">
                Select Employee: 
                <asp:dropdownlist runat="server" ID="ddlUsers">
                    <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                </asp:dropdownlist>
            </td>
        </tr>
        <tr>
            <td colspan="2">Please select month and year
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList runat="server" ID="ddlYear" Height="21px" Width="148px">
                    <asp:ListItem Text="Select Year" Value="0" Enabled="true"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="btnGR" runat="server" Text="Generate Report" Width="190px" Style="margin-top: -2px" OnClick="btnGR_Click" class="ui-state-default"/>
            </td>
        </tr>
        <tr class="trdata">
            <td colspan="2" style="font-weight: bold; padding: 7px 0px">From
                <asp:Label ID="lblStartDate" runat="server"></asp:Label>&nbsp
                to &nbsp<asp:Label ID="lblEndDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="trdata">
            <td class="caption">Absences this year: 
            </td>
            <td>
                <asp:Label ID="lblTotalAbsences" runat="server" Text="0"></asp:Label>             
            </td>
        </tr>
        <tr class="trdata">
            <td class="caption">Total days worked: 
            </td>
            <td>
                <asp:Label ID="lblTotDays" runat="server" Text="0"></asp:Label>             
            </td>
        </tr>
        <%--<tr class="trdata">
            <td class="caption">Hours this year: 
            </td>
            <td>
                <asp:Label ID="lblTotalHours" runat="server" Text=""></asp:Label>
            </td>
        </tr>--%>
        <tr class="trdata">
            <td class="caption">Working hrs should be: 
            </td>
            <td>
                <asp:Label ID="lblEstWHrs" runat="server" Text="0"></asp:Label>             
            </td>
        </tr>
        <tr class="trdata">
            <td class="caption">Worked this year:
            </td>
            <td>
                <asp:Label ID="lblWHours" runat="server" Style="float: left"></asp:Label>
                <%--<img alt="Info" src="images/controls/question.png" class="tooltip" title="Hours Info" runat="server" id="imgWorkedHourInfo" style="padding-left: 4px" />--%>
            </td>
        </tr>
        <tr class="trdata">
            <td class="caption">Breaks this year:
            </td>
            <td>
                <asp:Label ID="lblTotBreakHrs" runat="server" Style="float: left"></asp:Label>
                <%--<img alt="Info" src="images/controls/question.png" class="tooltip" title="Hours Info" runat="server" id="imgBreakHourInfo" style="padding-left: 4px" />--%>
            </td>
        </tr>
        <tr class="trdata">
            <td class="caption">Extra days worked:
            </td>
            <td>
                <asp:Label ID="lblExtraDays" runat="server" Style="float: left"></asp:Label>
                <%--<img alt="Info" src="images/controls/question.png" class="tooltip" title="Hours Info" runat="server" id="imgBreakHourInfo" style="padding-left: 4px" />--%>
            </td>
        </tr>
        <tr class="trdata">
            <td class="caption">Extra hours worked:
            </td>
            <td>
                <asp:Label ID="lblExtraHours" runat="server" Style="float: left" text="0 days"></asp:Label>
                <%--<img alt="Info" src="images/controls/question.png" class="tooltip" title="Hours Info" runat="server" id="imgBreakHourInfo" style="padding-left: 4px" />--%>
            </td>
        </tr>
    </table>
    <div>
        <asp:Label ID="lblTest" runat="server"></asp:Label>
    </div>
    <asp:HiddenField ID="hdnData" ClientIDMode="Static" Value="false" runat="server" />
    <asp:HiddenField ID="hfparentid" runat="server" />
    <asp:Table ID="tblHistory" runat="server">
    </asp:Table>
</section>
