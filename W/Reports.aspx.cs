﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports : AvaimaThirdpartyTool.AvaimaWebPage
{
    public String _InstanceID { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (_InstanceID == "0") { _InstanceID = "678d7728-915e-4e81-ac5b-d536e1a44fb9"; }
        else { _InstanceID = this.InstanceID; }
        AddinstanceWS objinst = new AddinstanceWS();
        OwnerId = objinst.GetUserID(HttpContext.Current.User.Identity.Name);
        //OwnerId = "e8a92994-352a-4319-838d-882e8dc1bdeb";

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("getuserrole", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", OwnerId.Trim());
                cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    if (Convert.ToString(dt.Rows[0]["role"]) == "worker" || Convert.ToString(dt.Rows[0]["role"]) == "admin")
                    {
                        if (Convert.ToString(dt.Rows[0]["role"]) == "admin")
                        {                            
                            appAssignedId = Convert.ToInt32(dt.Rows[0]["userid"]);                            
                        }
                        else
                        {
                            appAssignedId = Convert.ToInt32(dt.Rows[0]["userid"]);
                        }
                    }
                    else
                    {                        
                        appAssignedId = Convert.ToInt32(dt.Rows[0]["userid"]);
                    }
                }                
            }
        }
    }

    protected void lblMonthlyReport_Click(object sender, EventArgs e)
    {
        this.Redirect("MonthlyReportN.aspx?id=" + appAssignedId.ToString() + "&instanceid=" + this.InstanceID);
    }
    protected void lblYearlyReport_Click(object sender, EventArgs e)
    {

        this.Redirect("YearlyReportNew.aspx?id=" + appAssignedId.ToString() + "&instanceid=" + this.InstanceID);

    }
    protected void lblLateReport_Click(object sender, EventArgs e)
    {

        this.Redirect("LateReport.aspx?id=" + appAssignedId.ToString() + "&instanceid=" + this.InstanceID);

    }

    public string OwnerId { get; set; }
    public int appAssignedId { get; set; }
    protected void lnkEDAL_Click(object sender, EventArgs e)
    {
        this.Redirect("EDALReport.aspx?id=" + appAssignedId.ToString() + "&instanceid=" + this.InstanceID);
    }
}