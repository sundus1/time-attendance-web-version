﻿<%@ Page Title="" Language="C#" MasterPageFile="~/W/MasterPage.master" AutoEventWireup="true" CodeFile="Users.aspx.cs" Inherits="W_Users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .breadcrumb {
            background: none;
        }

        .breadcrumb-item + .breadcrumb-item::before {
            content: normal !important;
            padding-right: 0rem !important;
        }

        .breadcrumb-item a {
            color: #000;
            text-decoration: none;
            font-weight: normal;
            background: #d1edfe;
            padding: 7px 17px;
            margin: 0 5px;
        }

            .breadcrumb-item a:hover {
                text-decoration: underline;
            }

        #head_reuslt_panel {
            background: #ddd;
            max-width: 70% !important;
        }

        #head_reuslt_panel, #head_list_panel {
            background: #ddd;
            max-width: 70% !important;
        }

        .auto-style1 {
            width: 159px;
        }

        .auto-style2 {
            width: 137px;
        }

        .Clear {
            clear: both;
        }

        .modal {
            position: fixed;
            z-index: 999;
            height: 100%;
            width: 100%;
            top: 0;
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
            -moz-opacity: 0.8;
        }

        .center {
            z-index: 1000;
            margin: 300px auto;
            padding: 10px;
            width: 130px;
            background-color: White;
            border-radius: 10px;
            filter: alpha(opacity=100);
            opacity: 1;
            -moz-opacity: 1;
        }

            .center img {
                height: 128px;
                width: 128px;
            }
    </style>

    <script>
        jQuery(document).ready(function ($) {
            $("input[id$='txtDate']").datepicker();

            $("#lnkAddUser").click(function () {
                $("#div_adduser").show();
                $("#lnkHideUser").show();
            });

            $("#lnkHideUser").click(function () {
                $("#div_adduser").hide();
                $("#lnkHideUser").hide();
            });

            jq191('.tooltip').tooltipster({
                interactive: true,
            });

            $(".btnSigninLink").click(function () {
                event.preventDefault();
                var userid = $(this).closest('tr').find('#hf_ID').val();

                jQuery("[id$=lblcustommsg]").text("This option will email a Sign-in URL to user. Do you really want to send Sign-in link?");
                var dlg = jq191("#divCustomBox").dialog({
                    resizable: false,
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            jq191(this).dialog("close");
                            __doPostBack("btnSigninLink", userid);
                        },
                        "No": function () {
                            jq191(this).dialog("close");
                            return false;
                        }
                    }
                });
                dlg.parent().appendTo(jQuery("form:first"));
                dlg.parent().css("z-index", "1000");
            });


            $(".chk_managed").change(function () {
                event.preventDefault();
                var obj = $(this).find('#chk_managed');
                var msg = "";

                if (obj.is(":checked")) {
                    msg = "This will enable an employee to manage his/her clock-in/out. Are you sure?";
                }
                else {
                    msg = "This will disable an employee to manage his/her clock-in/out. Are you sure?";
                }

                var userid = obj.closest('tr').find('#hf_ID').val();
                var managed = obj.is(":checked");
                var instanceid = $("input[id$='hdnInstanceId']").val();

                jQuery("[id$=lblcustommsg]").text(msg);
                var dlg = jq191("#divCustomBox").dialog({
                    resizable: false,
                    modal: true,
                    open: function (event, ui) {
                        $(".ui-dialog-titlebar-close").hide();
                    },
                    buttons: {
                        "Yes": function () {
                            jq191(this).dialog("close");

                            var jsondata = "{'userid': " + userid + ",  'instanceid': '" + instanceid + "', 'action': 'set', 'managed': " + managed + "}";

                            $.ajax({
                                type: "POST",
                                url: "Attendance.asmx/UserManaged",
                                data: jsondata,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                beforeSend: function () {
                                    // Show image container
                                    ActivateAlertDiv('', 'AlertDiv');
                                },
                                success: function (result) {
                                    //Master Page function
                                    openConfirmBox('Status changed successfully!');
                                    location.reload();
                                },
                                complete: function (data) {
                                    // Hide image container
                                    ActivateAlertDiv('none', 'AlertDiv');
                                },
                                error: function (e) {
                                    openConfirmBox('Sorry an error has occurred. Please try again.');

                                }
                            });

                        },
                        "No": function () {
                            if (obj.is(":checked")) {
                                obj.prop('checked', false);
                            }
                            else {
                                obj.prop('checked', true);
                            }
                            jq191(this).dialog("close");
                            return false;
                        }
                    }
                });
                dlg.parent().appendTo(jQuery("form:first"));
                dlg.parent().css("z-index", "1000");
            });
        });

        function LoadRequests() {
            setGetParameter('req', 'yes');
        }

        function HideRequest() {
            window.location.replace(removeURLParam(window.location.href, 'req'));

            return false;
        }

        function setGetParameter(paramName, paramValue) {
            var url = window.location.href;
            var hash = location.hash;
            url = url.replace(hash, '');
            if (url.indexOf(paramName + "=") >= 0) {
                var prefix = url.substring(0, url.indexOf(paramName));
                var suffix = url.substring(url.indexOf(paramName));
                suffix = suffix.substring(suffix.indexOf("=") + 1);
                suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
                url = prefix + paramName + "=" + paramValue + suffix;
            }
            else {
                if (url.indexOf("?") < 0)
                    url += "?" + paramName + "=" + paramValue;
                else
                    url += "&" + paramName + "=" + paramValue;
            }
            window.location.href = url + hash;
        }

        function ProcessRequest(ev, permission) {

            var c = $(ev.parentNode.parentElement.children);
            var userid = $('[id$=hdnUserID]').val();
            var accessTo = $(c.find('[id$=userID]')).val();
            var accessDate = "<%=System.DateTime.Now.ToString()%>";
            var instanceid = $("input[id$='hdnInstanceId']").val();
            var rowid = $(c.find('[id$=rowID]')).val();

            var jsondata = "{'userid': " + userid + ", 'accessTo':" + accessTo + " , 'permission': '" + permission + "', 'accessDate': '" + accessDate + "',  'instanceid': '" + instanceid + "', 'rowid': " + rowid + "}";

            console.log(jsondata);

            $.ajax({
                type: "POST",
                url: "Attendance.asmx/ApproveDenyAdminRights_Email",
                data: jsondata,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    // Show image container
                    ActivateAlertDiv('', 'AlertDiv');
                },
                success: function (result) {
                    if (result) {
                        $(c).hide();
                    }
                },
                complete: function (data) {
                    // Hide image container
                    ActivateAlertDiv('none', 'AlertDiv');
                },
                error: function (e) {
                    openConfirmBox('Sorry an error has occurred. Please try again.');

                }
            });

            return false;
        }

        function Access(ev) {
            var c = $(ev.parentNode.parentElement.children);
            var userid = $('[id$=hdnUserID]').val();
            var access_user = $(c.find('[id$=hf_ID]')).val();
            var instanceid = $("input[id$='hdnInstanceId']").val();
            var title = $("input[id$='hdnTitle']").val();
            var ischecked;

            var chkBox = $(ev).attr('data-type');

            var obj;
            if (chkBox == "grant_access") {
                obj = $(c.find('[id$=chkGrtAccess]'));
            }
            else if (chkBox == "request_access") {
                obj = $(c.find('[id$=chkReqAccess]'));
            }

            var jsondata = "";
            var url = "";
            var response = "";

            jQuery("[id$=lblcustommsg]").text("Are you sure?");
            var dlg = jq191("#divCustomBox").dialog({
                resizable: false,
                modal: true,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                },
                buttons: {
                    "Yes": function () {
                        jq191(this).dialog("close");

                        if (chkBox == "request_access") {
                            ischecked = $(c.find('[id$=chkReqAccess]')).is(':checked');

                            if (ischecked) {
                                url = "RequestAdminRights_Email";
                                jsondata = "{'userid': '" + access_user + "', 'accessTo':'" + userid + "' , 'Title': '" + title + "', 'permission': 'pending', 'instanceid': '" + instanceid + "', 'access_taken': true, 'access_given': false}";
                                response = "Request sent successfully!";
                            }
                            else {
                                url = "RevokeAdminRights_Email";
                                jsondata = "{'userid': '" + access_user + "', 'accessTo':'" + userid + "' , 'Title': '" + title + "', 'permission': 'access_taken', 'instanceid': '" + instanceid + "', 'access_taken': true, 'access_given': false}";
                                response = "Rights revoked successfully!";
                            }
                        }
                        else if (chkBox == "grant_access") {
                            ischecked = $(c.find('[id$=chkGrtAccess]')).is(':checked');

                            if (ischecked) {
                                url = "RequestAdminRights_Email";
                                jsondata = "{'userid': '" + access_user + "', 'accessTo':'" + userid + "' , 'Title': '" + title + "', 'permission': 'allow', 'instanceid': '" + instanceid + "', 'access_taken': false, 'access_given': true}";
                                response = "Rights given successfully!";
                            }
                            else {
                                url = "RevokeAdminRights_Email";
                                jsondata = "{'userid': '" + userid + "', 'accessTo':'" + access_user + "' , 'Title': '" + title + "', 'permission': 'access_given', 'instanceid': '" + instanceid + "', 'access_taken': false, 'access_given': true}";
                                response = "Rights revoked successfully!";
                            }
                        }
                        console.log(response + "\n  " + url + "\n   " + jsondata);

                        $.ajax({
                            type: "POST",
                            url: "Attendance.asmx/" + url,
                            data: jsondata,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function () {
                                // Show image container
                                ActivateAlertDiv('', 'AlertDiv');
                            },
                            success: function (result) {
                                if (result) {
                                    //Master Page function
                                    openConfirmBox(response);
                                }
                            },
                            complete: function (data) {
                                // Hide image container
                                ActivateAlertDiv('none', 'AlertDiv');
                            },
                            error: function (e) {
                                openConfirmBox('Sorry an error has occurred. Please try again.');
                            }
                        });
                    },
                    "No": function () {
                        jq191(this).dialog("close");
                        if (obj.is(":checked")) {
                            obj.prop('checked', false);
                        }
                        else {
                            obj.prop('checked', true);
                        }
                        return false;
                    }
                }
            });
            dlg.parent().appendTo(jQuery("form:first"));
            dlg.parent().css("z-index", "1000");
        }


        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

        function BeginRequestHandler(sender, args) {
            var elem = args.get_postBackElement();
            ActivateAlertDiv('', 'AlertDiv');
        }
        function EndRequestHandler(sender, args) {
            ActivateAlertDiv('none', 'AlertDiv');
        }
        function ActivateAlertDiv(visstring, elem) {
            var adiv = $get(elem);

            var objLeft = (document.body.clientWidth) / 2 - 50;
            var objTop = (document.body.clientHeight) / 2 - 20;

            objLeft = objLeft + document.body.scrollLeft;
            objLeft = objLeft + "px";
            objTop = objTop + document.body.scrollTop;
            objTop = objTop + "px";

            adiv.style.display = visstring;
            adiv.style.position = "absolute";
            adiv.style.left = objLeft;
            adiv.style.top = objTop;
        }

        function removeURLParam(url, param) {
            var urlparts = url.split('?');
            if (urlparts.length >= 2) {
                var prefix = encodeURIComponent(param) + '=';
                var pars = urlparts[1].split(/[&;]/g);
                for (var i = pars.length; i-- > 0;)
                    if (pars[i].indexOf(prefix, 0) == 0)
                        pars.splice(i, 1);
                if (pars.length > 0)
                    return urlparts[0] + '?' + pars.join('&');
                else
                    return urlparts[0];
            }
            else
                return url;
        }

        HTMLInputElement.prototype.startsWith = function () { return false; };

    </script>

    <asp:ScriptManager runat="server" ID="scriptmanager1"></asp:ScriptManager>
    <asp:HiddenField runat="server" ID="Option" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnTitle" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnUserID" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnInstanceId" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnUserEmail" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnUserPassword" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnIsActive" ClientIDMode="Static" />

    <!-- Submenu -->
    <ol class="breadcrumb bread-list">
        <li class="breadcrumb-item" runat="server" id="accessrequest_link" visible="false">
            <asp:LinkButton runat="server" ID="lnkAccessRequest" Text="Admin Rights Requests" OnClientClick="LoadRequests();"></asp:LinkButton>
        </li>
    </ol>

    <div class="space">
        <div class="Clear"></div>
        <div class="Tab AttendenceSettingDiv3">

            <div id="div_adduser" style="display: none;">
                <div class="add-user-form">

                    <%--<tr class="smallGrid">                       
                    </tr>--%>
                    <div class="holidays-form">
                        <label>First Name:</label>
                        <asp:TextBox ID="txtFirstName" runat="server" EnableViewState="false" CssClass="standardField txtFirstName required f-2"
                            placeholder="First Name..."></asp:TextBox>
                        <label>Last Name:</label>
                        <asp:TextBox ID="txtLastName" runat="server" CssClass="standardField txtLastName required f-2"
                            EnableViewState="false" placeholder="Last Name..."></asp:TextBox>
                        <label>Email:</label>
                        <asp:TextBox runat="server" ID="txtEmail" placeholder="Email Address" CssClass="standardField txtEmail f-2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="Required"
                            ForeColor="Red" ValidationGroup="gg" Display="Dynamic">Required</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtEmail"
                            Display="Dynamic" ErrorMessage="Invalid Email" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ValidationGroup="gg"></asp:RegularExpressionValidator>
                        <asp:CheckBox ID="chk_manage" runat="server" Text="Employee manages his/her own clock-in/out." Checked="true" ClientIDMode="Static" />
                        <asp:HiddenField runat="server" ID="id" />
                        <asp:Button runat="server" ID="AddUser" Text="Add" CssClass="btn btn-primary b-21 right-gap"
                            ValidationGroup="gg" OnClick="AddUser_Click" ClientIDMode="Static" />
                        <asp:Button Text="Cancel" ClientIDMode="Static" ID="lnkHideUser" runat="server" CssClass="btn btn-primary b-21" OnClientClick="return false" />
                    </div>
                </div>
                <asp:Label runat="server" ID="lblexception"></asp:Label>
            </div>

            <div id="UsersDiv" runat="server">
                <h4></h4>
                <asp:UpdatePanel runat="server" ID="users_panel">
                    <ContentTemplate>
                        <asp:Repeater runat="server" ID="rpthd" OnItemDataBound="rpthd_ItemDataBound" OnItemCommand="rpthd_ItemCommand">
                            <HeaderTemplate>
                                <table class="table-grid-sample1 table-responsive">
                                    <tr class="smallGridHead">
                                        <td>Title
                                        </td>
                                        <td>Email
                                        </td>
                                        <td>Added On
                                        </td>
                                        <td>Last Clock-in
                                        </td>
                                        <td id="rqaccess" runat="server">Request for Access
                                        </td>
                                        <td id="gtaccess" runat="server">Grant your Access
                                        </td>
                                        <td>Employee Manage<br />
                                            his/her clock-in/out</td>
                                        <td></td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="smallGrid">
                                    <td>
                                        <asp:HiddenField runat="server" ID="hf_ID" Value='<%# Eval("ID") %>' ClientIDMode="Static" />
                                        <asp:HiddenField runat="server" ID="hf_parentid" Value='<%# Eval("parentid") %>' ClientIDMode="Static" />
                                        <asp:HiddenField runat="server" ID="hf_isManaged" Value='<%# Eval("isManaged") %>' ClientIDMode="Static" />

                                        <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ID") %>' ClientIDMode="Static" />
                                        <asp:LinkButton runat="server" ID="lblTitle" Text='<%# Eval("Title") %>' CommandArgument='<%# Eval("ID") %>' CommandName="openpopup" ClientIDMode="Static" CssClass="tooltip"></asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblEmail" Text='<%# Eval("email") %>' ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblDateAdded" Text='<%# Eval("datefield") %>' ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblLastClockin" Text='<%# Eval("lastin") %>' ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td id="tdrqaccess" runat="server">
                                        <asp:HiddenField runat="server" ID="hdnReqAccess" Value='<%# Eval("access_taken") %>' ClientIDMode="Static" />
                                        <asp:HiddenField runat="server" ID="hdnPermissionTaken" Value='<%# Eval("permission_taken") %>' ClientIDMode="Static" />
                                        <asp:CheckBox runat="server" ID="chkReqAccess" onchange="Access(this);" ClientIDMode="Static" data-type="request_access" />

                                    </td>
                                    <td id="tdgtaccess" runat="server">
                                        <asp:HiddenField runat="server" ID="hdnGrtAccess" Value='<%# Eval("access_given") %>' ClientIDMode="Static" />
                                        <asp:HiddenField runat="server" ID="hdnPermissionGiven" Value='<%# Eval("permission_given") %>' ClientIDMode="Static" />
                                        <asp:CheckBox runat="server" ID="chkGrtAccess" onchange="Access(this);" ClientIDMode="Static" data-type="grant_access" />
                                    </td>
                                    <td id="tdblank" runat="server" visible="false" style="background: #f3f0f0 !important; text-align: center; font-weight: bold;">
                                        <asp:Label runat="server" ID="lblText" Text=""></asp:Label>
                                    </td>
                                    <td id="tdmanaged" runat="server">
                                        <asp:CheckBox runat="server" ID="chk_managed" CssClass="chk_managed" ClientIDMode="Static" />
                                    </td>
                                    <td id="tdlink" runat="server">
                                        <asp:Button ID="btnSigninLink" runat="server" Text="Send Sign-in Link" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <br />
                        <asp:Label runat="server" Text="No Record Found." Visible="False" ID="hdmsg" Font-Bold="true"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="AddUser" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

            <!--Request Div-->
            <div id="RequestDiv" title="Admin Rights Requests" runat="server">
                <div style="display: inline-flex;">
                    <h4>Admin Rights Requests <a id="lnkHideReq" class="small-txt" onclick="return HideRequest();" href="#"><i class="fa fa-fw fa-minus-circle"></i>Close</a>
                    </h4>
                </div>
                <asp:UpdatePanel runat="server" ID="request_panel">
                    <ContentTemplate>
                        <asp:Repeater runat="server" ID="rptRequests" OnItemCommand="rptRequests_ItemCommand" OnItemDataBound="rptRequests_ItemDataBound">
                            <HeaderTemplate>
                                <table class="smallGrid table-responsive">
                                    <tr class="smallGridHead">
                                        <td colspan="3" style="font-weight: bold;">Please take an action to following requests before proceeding
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="smallGrid">
                                    <td>
                                        <asp:HiddenField runat="server" ID="rowID" Value='<%# Eval("rowid") %>' ClientIDMode="Static" />
                                        <asp:HiddenField runat="server" ID="userID" Value='<%# Eval("accessTo") %>' ClientIDMode="Static" />
                                        <asp:Label runat="server" ID="title" Text='<%# Eval("Title") %>' ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td></td>
                                    <td>
                                        <asp:LinkButton runat="server" ID="lnkApprove" ToolTip="Approve" CommandName="allow" OnClientClick="return ProcessRequest(this,'allow');"><i class="fa fa-fw fa-check" style="color:green;font-size: larger;"></i></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lnkDeny" ToolTip="Deny" CommandName="deny" OnClientClick="return ProcessRequest(this,'deny');"><i class="fa fa-fw fa-remove" style="color:red;font-size: larger;"></i></asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <br />
                        <asp:Label runat="server" Text="No Request Available." Visible="False" ID="lblNoReq" Font-Bold="true"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <%--                        <asp:AsyncPostBackTrigger ControlID="lnkAccessRequest" EventName="Click" />--%>
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="Clear"></div>
    </div>

    <div id="divCustomBox" title="Confirm" style="display: none">
        <p>
            <asp:Label runat="server" ID="lblcustommsg" Text=""></asp:Label>
        </p>
    </div>

    <div id="AlertDiv" style="display: none; height: 100%; width: 100%; min-width: 100%; min-height: 100%; z-index: 999999 !important;" class="overlay">
        <div class="overlay-content">
            <img src="../images/loader.gif" alt='' height="70px" width="70px" />
        </div>
    </div>

</asp:Content>

