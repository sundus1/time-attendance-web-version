﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_EDALReport : System.Web.UI.UserControl
{
    private bool IsSimpleClock = true;
    List<RecordModel> records = new List<RecordModel>();
    private int UserId;
    public String InstanceID { get; set; }
    DateTime signInDate;
    List<AttUser> users = AttUser.GetAll();

    protected void Page_Load(object sender, EventArgs e)
    {
        signInDate = SP.GetWorkerFirstSignInn(UserId.ToString());
        if (!IsPostBack)
        {
            GenerateDate();
        }
        if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out UserId))
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("select * from attendence_management a where a.Id = @id and a.InstanceId = @insId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", UserId);
                    cmd.Parameters.AddWithValue("@insId", this.InstanceID);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];
                        lblUserName.Text = dr["Title"].ToString();
                    }
                    else
                    {
                        Response.Redirect("Default.aspx?instanceid=" + this.InstanceID);
                    }
                }

            }
        }
        else
        {
            Response.Redirect("Default.aspx?instanceid=" + this.InstanceID);
        }

        AddinstanceWS objinst = new AddinstanceWS();
        OwnerId = objinst.GetUserID(HttpContext.Current.User.Identity.Name);
        OwnerId = "e8a92994-352a-4319-838d-882e8dc1bdeb";

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("getuserrole", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", OwnerId.Trim());
                cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    //AdminFirstSignIn = SP.GetWorkerFirstSignInn(dt.Rows[0][1].ToString()).Date;
                    if (Convert.ToString(dt.Rows[0]["role"]) == "worker" || Convert.ToString(dt.Rows[0]["role"]) == "admin")
                    {
                        if (Convert.ToString(dt.Rows[0]["role"]) == "admin")
                        {
                            isAdmin = true;
                            isWorker = false;
                            trSelUser.Visible = true;
                            appAssignedId = Convert.ToInt32(dt.Rows[0]["userid"]);
                            hfparentid.Value = SP.GetAdminCatID(Convert.ToInt32(dt.Rows[0]["userid"].ToString())).ToString();
                        }
                        else
                        {
                            isWorker = true;
                            //Response.Redirect("Add_worker.aspx?id=" + dt.Rows[0]["userid"]);
                        }
                    }
                    else
                    {
                        isWorker = false;
                        appAssignedId = Convert.ToInt32(dt.Rows[0]["userid"]);
                    }
                }
            }
            if (!IsPostBack)
            {
                if (ddlYear.Items.Count > 0)
                {
                    ddlYear.SelectedIndex = 1;
                }
                if (!isWorker)
                {
                    FillUsers();
                    ddlUsers.SelectedIndex = Users.FindIndex(u => u.userID == UserId);
                    lblUserName.Text = Users.Where(u => u.userID == UserId).SingleOrDefault().userName;
                    trSelUser.Visible = true;
                    FillandCreateHistory(ddlUsers.SelectedValue, con);
                    CreateYearlyReport(ddlUsers.SelectedValue, con);
                    CreateReport();
                }
                else
                {
                    AttUser user = AttUser.GetUserByID(UserId);
                    if (user == null)
                    {
                        ddlUsers.Items.Add(new ListItem("User", UserId.ToString()));
                    }
                    else
                    {
                        ddlUsers.Items.Add(new ListItem(user.UserName, UserId.ToString()));
                    }
                    ddlUsers.SelectedIndex = 1;
                    trSelUser.Style.Add("display", "none");
                    FillandCreateHistory(ddlUsers.SelectedValue, con);
                    CreateYearlyReport(ddlUsers.SelectedValue, con);
                    CreateReport();
                }
            }
            //else
            //{
            //    CreateYearlyReport(ddlUsers.SelectedValue, con);
            //    CreateReport();
            //}
        }
    }

    private void HoursCalculation(int totHrs, int totMins, int wtotHrs, int wtotMins, int btotHrs, int btotMins, int bTotShouldHrs)
    {
        #region Estimated total hours calculations
        List<Absence> absences = Absence.GetAbsences(Convert.ToInt32(ddlUsers.SelectedValue)).Where(u => u.Active == true).ToList();
        Int32 totHours = 0;
        Int32 totWorkHours = 0;
        Boolean weekend = false;
        DateTime startDateTime = SP.GetWorkerFirstSignInThisYear(ddlUsers.SelectedValue, Convert.ToInt32(ddlYear.SelectedValue));
        DateTime endDateTime = SP.GetWorkerLastSignInThisYear(ddlUsers.SelectedValue.ToString(), Convert.ToInt32(ddlYear.SelectedValue));
        //lblTest.Text = startDateTime.ToShortDateString() + " - " + endDateTime.ToShortDateString();
        for (DateTime i = startDateTime; i <= endDateTime; i = i.AddDays(1))
        {
            DateTime currDateTime = i.Date;
            //lblTest.Text += "<br><br>" + i.Date.ToShortDateString() + "<br><br>";
            if (absences.Where(u => u.CrtDate == currDateTime.Date).ToList().Count < 1)
            {
                Int32 userWorkHours = SP.GetTodayUserWorkHours(this.InstanceID, Convert.ToInt32(ddlUsers.SelectedValue), currDateTime);
                Int32 userTotHours = SP.GetTodayUserTotHours(this.InstanceID, Convert.ToInt32(ddlUsers.SelectedValue), currDateTime);
                weekend = false;

                if (userTotHours < 0)
                {
                    //lblTest.Text += "<0";
                    totWorkHours += (-(userWorkHours) + 12 - (SP.GetUserBreakTimeDiff(Convert.ToInt32(ddlUsers.SelectedValue), this.InstanceID, currDateTime) * 2));
                    totHours += (-(userTotHours) + 12);
                }
                else
                {
                    //lblTest.Text += "<br> " + currDateTime.ToString("MMM dd yyyy") + "  " + totWorkHours.ToString();
                    totWorkHours += userWorkHours;
                    totHours += userTotHours;
                }

            }
        }
        #endregion
        lblEstWHrs.Text = totWorkHours.ToString() + " hrs (" + lblTotDays.Text + ")";
        //lblEstWHrs.Text = totWorkHours + " hrs";
        //lblTotalHours.Text = totHrs + " hrs " + totMins + " mins should be " + totHours + " hrs";
        lblWHours.Text = wtotHrs + " hrs " + wtotMins + " mins (" + lblTotDays.Text + ")";
        lblTotBreakHrs.Text = btotHrs + " hrs " + btotMins + " mins " + " should be " + bTotShouldHrs + " hrs";

        //string[] wHourMin = lblWHours.Text.Split(' ');
        //try
        //{
        //    int exHr = 0;
        //    int exMin = 0;
        //    if (wHourMin.Count() > 3)
        //    {
        //        exHr = Convert.ToInt32(wHourMin[0]);
        //        exMin = Convert.ToInt32(wHourMin[2]);
        //    }
        //    else
        //    {
        //        exMin = Convert.ToInt32(wHourMin[0]);
        //    }
        //    exHr = exHr - Convert.ToInt32(lblEstWHrs.Text.Split(' ')[0]);
        //    if (exHr > 0)
        //    {
        //        lblExtraHours.Text = exHr + " hrs " + exMin + " min";
        //    }
        //    else
        //    {
        //        lblExtraHours.Text = "0 hrs";
        //    }

        //}
        //catch (Exception)
        //{ }
    }

    private void FillUsers()
    {
        PopulateUsersAccord(0, isAdmin);
        hfparentid.Value = "0";
    }

    protected void PopulateUsersAccord(int pid, bool isAdmin = false, bool ShowFiltered = false, int status = 1)
    {
        DataTable dtstudentall = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            string q = "getallattendencerecordbyid";
            if (ShowFiltered)
            {
                if (status == 1)
                    q = "getallactiveusers";
                else
                {
                    q = "getallinactiveusers";
                }
            }
            else
            {
                if (isAdmin)
                {
                    q = "getfilteredattendancerecordbyid";
                }
            }

            using (SqlCommand cmd = new SqlCommand(q, con))
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", pid);
                cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                if (isAdmin)
                {
                    if (q == "getfilteredattendancerecordbyid")
                    {
                        cmd.Parameters.AddWithValue("@userid", appAssignedId);
                    }
                }
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dtstudentall);
                cmd.CommandText = "getclocktype";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@instanceId", this.InstanceID);
                bool i = (bool)cmd.ExecuteScalar();
                if (i)
                {
                    IsSimpleClock = false;
                }
                Users = new List<User>();
                if (dtstudentall.Rows.Count > 0)
                {
                    foreach (DataRow row in dtstudentall.Rows)
                    {
                        if (Convert.ToBoolean(row["category"].ToString()) == false)
                        {
                            Users.Add(new User() { userID = Convert.ToInt32(row[0].ToString()), userName = row[1].ToString() });
                        }
                        else
                        {                            
                            users = AttUser.GetAll().Where(u => u.ParentID == parentID).ToList();
                            FetchUsers(Convert.ToInt32(row[0].ToString()));
                        }
                    }
                }
                ddlUsers.DataSource = Users;
                ddlUsers.DataTextField = "userName";
                ddlUsers.DataValueField = "userID";
                ddlUsers.DataBind();
            }
        }
    }

    private void FetchUsers(Int32 parentID)
    {
        AttUser filteredUsers = users.Find(u => u.ParentID == parentID);
        foreach (AttUser user in filteredUsers)
        {
            if (user.Category == false)
            {
                Users.Add(new User() { userID = user.UserID, userName = user.UserName });
            }
            else
            {
                FetchUsers(user.ParentID);
            }
        }
    }

    private void GenerateDate()
    {
        ddlYear.Items.Clear();
        DateTime startDate = new DateTime();
        DateTime endDate = new DateTime();
        if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out UserId))
        {
            startDate = SP.GetWorkerFirstSignInn(UserId.ToString());
            endDate = SP.GetWorkerLastSignIn(UserId.ToString());
        }
        DateTime tempDt = new DateTime(DateTime.Now.Year, 1, 1);
        String strDate = "";
        ddlYear.Items.Add(new ListItem("Select Year", "0"));
        ddlYear.Items.Add(new ListItem(endDate.ToString("yyyy"), endDate.ToString("yyyy")));
        int diff = endDate.Year - startDate.Year;
        for (int i = 0; i < diff; i++)
        {
            strDate = startDate.AddYears(-i).ToString("yyyy");
            ddlYear.Items.Add(new ListItem(strDate, strDate));
        }
    }

    private void FillandCreateHistory(string userid, SqlConnection con)
    {
        if (Convert.ToInt32(ddlYear.SelectedValue) > 0)
        {
            using (SqlCommand cmd = new SqlCommand("GetYearlyReport", con))
            {
                DataTable dt = new DataTable();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Year", ddlYear.SelectedValue);
                cmd.Parameters.AddWithValue("@Id", userid);
                cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);

                List<Absence> _absences = Absence.GetAbsences(Convert.ToInt32(UserId)).Where(u => u.Active == true && u.CrtDate.Year == Convert.ToInt32(ddlYear.SelectedValue)).ToList();
                //lblTotalHour.Text = dt.AsEnumerable().Sum(u => u.Field<int>(1)).ToString() + " days";
                //lblTotalAbsence.Text = _absences.Count().ToString() + " days";
                //lblTotWeekend.Text = dt.AsEnumerable().Sum(u => u.Field<int>(3)).ToString() + " days";
            }
            hdnData.Value = "true";
        }
        else
        {
            hdnData.Value = "false";
        }
    }
    private static void CalculateTime(ref int bTotShouldHrs, ref int bTotShouldMins, string[] workedhours)
    {
        if (workedhours.Count() > 3)
        {
            bTotShouldHrs += Convert.ToInt32(workedhours[0]);
            bTotShouldMins += Convert.ToInt32(workedhours[2]);
            if (bTotShouldMins >= 60)
            {
                bTotShouldHrs += Convert.ToInt32((bTotShouldMins / 60));
                bTotShouldMins -= 60;
            }
        }
        else
        {
            if (Convert.ToInt32(workedhours[0]) > 0)
            {
                bTotShouldMins += Convert.ToInt32(workedhours[0]);
                if (bTotShouldMins >= 60)
                {
                    bTotShouldHrs += Convert.ToInt32((bTotShouldMins / 60));
                    bTotShouldMins -= 60;
                }
            }
        }
    }
    class DateRange
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    private void CreateYearlyReport(string userid, SqlConnection con)
    {
        hdnData.Value = "true";
        DateTime startDate = SP.GetWorkerFirstSignInThisYear(userid, Convert.ToInt32(ddlYear.SelectedValue));
        DateTime endDate = SP.GetWorkerLastSignInThisYear(userid, Convert.ToInt32(ddlYear.SelectedValue));
        lblStartDate.Text = startDate.ToString("ddd, MMM dd yyyy");
        lblEndDate.Text = endDate.ToString("ddd, MMM dd yyyy");
        int days = 0;
        int totalAbs = 0;
        int weekendHours = 0;
        int homeHours = 0;
        int officeHours = 0;
        int totHrs = 0;
        int totMins = 0;
        int wtotHrs = 0;
        int wtotMins = 0;
        int btotHrs = 0;
        int btotMins = 0;
        int totDays = 0;
        int exDays = 0;
        int totExtDays = 0;
        int totExtWeekendsHrs = 0;
        int totExtWeekendsMins = 0;
        int bTotShouldHrs = 0;
        int totalWeekendDaysWork = 0;
        #region Table Headers weekends
        tblWeekendWork.CssClass = "smallGrid";
        TableRow troww = new TableRow();
        troww.CssClass = "smallGridHead";

        troww.Cells.Add(new TableCell() { Text = "" });
        troww.Cells.Add(new TableCell() { Text = "Clock In" });
        troww.Cells.Add(new TableCell() { Text = "Clock Out" });
        troww.Cells.Add(new TableCell() { Text = "Worked" });
        troww.Cells.Add(new TableCell() { Text = "Breaks" });
        troww.Cells.Add(new TableCell() { Text = "" });
        troww.Cells.Add(new TableCell() { Text = "" });
        tblWeekendWork.Rows.Add(troww);
        List<RecordModel> recordList = new List<RecordModel>();
        List<DateRange> DateRanges = new List<DateRange>();
        #endregion
        List<Absence> absences = Absence.GetAbsences(Convert.ToInt32(userid)).Where(u => u.Active == true).ToList();
        int totAbsDays = 0;
        int totalHoursShouldBe = 0;
        if (startDate.Year <= Convert.ToInt32(ddlYear.SelectedValue) && endDate.Year >= Convert.ToInt32(ddlYear.SelectedValue))
        {
            for (int i = startDate.Month; i <= endDate.Month; i++)
            {
                DateTime qstartDate = new DateTime(Convert.ToInt32(ddlYear.SelectedValue), i, 1);
                DateTime qendDate = qstartDate.AddMonths(1).AddDays(-1);
                using (SqlCommand cmd = new SqlCommand("getworkerrecordbydate", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@sDate", qstartDate.Date);
                    cmd.Parameters.AddWithValue("@eDate", qendDate.Date);
                    cmd.Parameters.AddWithValue("@Id", userid);
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    DataSet ds = new DataSet();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                    records = UtilityMethods.getRecords(ds);
                    records = records.OrderBy(u => u.Date).ToList();
                    DateRanges.Add(new DateRange()
                    {
                        EndDate = qendDate.Date,
                        StartDate = qstartDate.Date
                    });
                    //recordList.AddRange(records);
                    //for (int ii = 0; ii < 25; ii++)
                    //{
                    //    TableCell cell = new TableCell();
                    //    cell.Text = ii.ToString();
                    //    TableRow row = new TableRow();
                    //    row.Cells.Add(cell);
                    //    tblWeekendWork.Rows.Add(row);
                    //}
                    //tblWeekendWork.Rows.Clear();
                    foreach (RecordModel record in records)
                    {
                        // Extra days, total days and weekend hours
                        if (record.Records.Count > 0)
                        {

                            if (record.DayTitle != null)
                            {
                                totExtDays++;
                                string[] weekendWorkHours;
                                // Weekend Worked hours
                                totalWeekendDaysWork++;
                                weekendWorkHours = record.TotalWorkedHours.Split(' ');
                                CalculateTime(ref totExtWeekendsHrs, ref totExtWeekendsMins, weekendWorkHours);
                            }
                            else
                            {
                                totDays++;
                            }

                        }

                        officeHours = record.Records.Where(x => x.InLocation == "Office").Sum(x => x.RowTime);

                        homeHours = record.Records.Where(x => x.InLocation == "Home").Sum(x => x.RowTime);

                        string[] workedhours = record.TotalTime.Split(' ');
                        if (workedhours.Count() > 3)
                        {
                            totHrs += Convert.ToInt32(workedhours[0]);
                            totMins += Convert.ToInt32(workedhours[2]);
                            if (totMins >= 60)
                            {
                                totHrs += Convert.ToInt32((totMins / 60));
                                totMins -= 60;
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(workedhours[0]) < 0)
                            {
                                totMins += Convert.ToInt32(workedhours[0]);
                                if (totMins >= 60)
                                {
                                    totHrs += Convert.ToInt32((totMins / 60));
                                    totMins -= 60;
                                }
                            }
                        }

                        if (record.DayTitle == null)
                        {
                            // Total Break hours
                            if (record.Records.Count > 0)
                            {
                                workedhours = record.TotalBreak.Split(' ');
                                bTotShouldHrs += SP.GetUserBreakTimeDiff(Convert.ToInt32(ddlUsers.SelectedValue), this.InstanceID, record.Date.Date);
                                if (workedhours.Count() > 3)
                                {
                                    btotHrs += Convert.ToInt32(workedhours[0]);
                                    btotMins += Convert.ToInt32(workedhours[2]);
                                    if (btotMins >= 60)
                                    {
                                        btotHrs += Convert.ToInt32((btotMins / 60));
                                        btotMins -= 60;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToInt32(workedhours[0]) > 0)
                                    {
                                        btotMins += Convert.ToInt32(workedhours[0]);
                                        if (btotMins >= 60)
                                        {
                                            btotHrs += Convert.ToInt32((btotMins / 60));
                                            btotMins -= 60;
                                        }
                                    }
                                }

                                if (!String.IsNullOrEmpty(record.Records[record.Records.Count - 1].LogoutTime) || record.Records[record.Records.Count - 1].LogoutTime != "")
                                {
                                    workedhours = record.TotalWorkedHours.Split(' ');
                                    if (workedhours.Count() > 3)
                                    {
                                        wtotHrs += Convert.ToInt32(workedhours[0]);
                                        wtotMins += Convert.ToInt32(workedhours[2]);
                                        if (wtotMins >= 60)
                                        {
                                            wtotHrs += Convert.ToInt32((wtotMins / 60));
                                            wtotMins -= 60;
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(workedhours[0]) > 0)
                                        {
                                            wtotMins += Convert.ToInt32(workedhours[0]);
                                            if (wtotMins >= 60)
                                            {
                                                wtotHrs += Convert.ToInt32((wtotMins / 60));
                                                wtotMins -= 60;
                                            }
                                        }
                                    }
                                    totalHoursShouldBe += SP.GetTodayUserWorkHours(this.InstanceID, Convert.ToInt32(ddlUsers.SelectedValue), record.Date.Date);
                                }

                            }
                        }
                    }
                }


                foreach (Absence absence in absences.Where(u => u.CrtDate.Date.Month == i).ToList())
                {
                    totAbsDays += SP.GetTodayUserWorkHours(this.InstanceID, Convert.ToInt32(ddlUsers.SelectedValue), absence.CrtDate.Date);
                }
                totalAbs += absences.Where(u => u.CrtDate.Date.Month == i).ToList().Count;
            }
        }
        //totExtDays.ToString() + " days 
        long totExtWeekHrs = totExtWeekendsHrs * 3600;
        long totExtWeekMint = totExtWeekendsMins * 60;
        long totWeekendMilliseconds = totExtWeekHrs + totExtWeekMint;
        long totWWorkHrsMilli = wtotHrs * 3600;
        long totWWorkMinMilli = wtotMins * 60;
        long totWMilliSeconds = totWWorkHrsMilli + totWWorkMinMilli;
        long totWShouldBeMilli = totalHoursShouldBe * 3600;
        Boolean overtime = false;
        long ExtDayWork = 0;
        if (totWMilliSeconds >= totWShouldBeMilli)
        {
            ExtDayWork = totWMilliSeconds - totWShouldBeMilli;
            overtime = true;
            lblExTime.ForeColor = System.Drawing.Color.Green;
            lblExTime.Text = "+ " + UtilityMethods.GetFormattedDifference(ExtDayWork);
        }
        else
        {
            ExtDayWork = totWShouldBeMilli - totWMilliSeconds;
            overtime = false;
            lblExTime.ForeColor = System.Drawing.Color.Red;
            lblExTime.Text = "- " + UtilityMethods.GetFormattedDifference(ExtDayWork);
        }
        lblWeekendWorkDays.Text = ((totalWeekendDaysWork > 1)) ? totalWeekendDaysWork + " days" : totalWeekendDaysWork + " day";
        lblWeekendExtraDays.Text = String.IsNullOrEmpty(UtilityMethods.GetFormattedDifference(totWeekendMilliseconds)) ? "Not found" : UtilityMethods.GetFormattedDifference(totWeekendMilliseconds);

        lblTotalTimeDays.Text = "";
        if (overtime)
        {
            lblTotalTime.Text = UtilityMethods.GetFormattedDifference(totWeekendMilliseconds + ExtDayWork);
            lblTotalTimeDays.Text = " (" + UtilityMethods.GetFormattedHours(totWeekendMilliseconds + ExtDayWork, SP.GetTodayUserWorkHours(this.InstanceID, Convert.ToInt32(ddlUsers.SelectedValue), new DateTime(2014, 4, 21))) + ")";
            lblOvertimeLabel.Text = "Extra work on weekdays (Overtime):";
        }
        else
        {
            lblTotalTime.Text = UtilityMethods.GetFormattedDifference(totWeekendMilliseconds - ExtDayWork);
            lblOvertimeLabel.Text = "Hours not completed (Missing time):";
        }
        lblTotalTime.ForeColor = System.Drawing.Color.Green;
        if (!overtime)
        {
            if ((totWeekendMilliseconds - ExtDayWork) < 0)
            {
                lblTotalTime.ForeColor = System.Drawing.Color.Red;
            }
        }
        else if (overtime)
        {
            if ((totWeekendMilliseconds + ExtDayWork) < 0)
            {
                lblTotalTime.ForeColor = System.Drawing.Color.Red;
            }
        }
        lblTotDays.Text = totDays + " days";
        HoursCalculation(totHrs, totMins, wtotHrs, wtotMins, btotHrs, btotMins, bTotShouldHrs);
        lblTotalAbsences.Text = totalAbs.ToString() + " days (" + totAbsDays.ToString() + " hours)";
        DateRanges = DateRanges.OrderByDescending(u => u.StartDate).ToList();
        foreach (DateRange item in DateRanges)
        {
            using (SqlCommand cmd = new SqlCommand("getworkerrecordbydate", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@sDate", item.StartDate);
                cmd.Parameters.AddWithValue("@eDate", item.EndDate);
                cmd.Parameters.AddWithValue("@Id", userid);
                cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                DataSet ds = new DataSet();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                records = UtilityMethods.getRecords(ds);
                records = records.OrderByDescending(u => u.Date).ToList();
                CreateAdminTable(records);
            }
        }
        //recordList = recordList.OrderByDescending(u => u.Date.Date).ToList();
        //CreateAdminTable(records);
        //lblTotalAbsences.Text = totalAbs.ToString();

        //lblTotalHours.Text = totHrs + " hrs " + totMins + " mins";
        //lblWHours.Text = wtotHrs + " hrs " + wtotMins + " mins";
        //lblTotBreakHrs.Text = btotHrs + " hrs " + btotMins + " mins";
    }

    protected void btnGR_Click(object sender, EventArgs e)
    {
        CreateReport();
    }

    private void CreateReport()
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            //FillandCreateHistory(UserId.ToString(), con);            
            tblWeekendWork.Rows.Clear();
            CreateYearlyReport(ddlUsers.SelectedValue, con);
            //FillandCreateGrid(ddlUsers.SelectedValue, con);
            lblUserName.Text = ((ListItem)ddlUsers.SelectedItem).Text;
        }
        if (tblWeekendWork.Rows.Count > 1)
        {
            trtblextrawork.Visible = true;
            trextrawork.Visible = true;
            trabsence.Visible = true;
        }
        else
        {
            trtblextrawork.Visible = false;
            trextrawork.Visible = false;
            trabsence.Visible = false;
        }

    }
    protected void bk_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out UserId))
        {
            Response.Redirect("Reports.aspx?id=" + UserId + "&instanceid=" + this.InstanceID);
        }
    }


    private void CreateAdminTable(List<RecordModel> records)
    {
        DateTime WorkerLastSignIn = SP.GetWorkerLastSignInThisYear(ddlUsers.SelectedValue, Convert.ToInt32(ddlYear.SelectedValue));
        DateTime WorkerFirstSignIn = SP.GetWorkerFirstSignInThisYear(ddlUsers.SelectedValue, Convert.ToInt32(ddlYear.SelectedValue));
        AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();

        List<Absence> _absences = Absence.GetAbsences(Convert.ToInt32(ddlUsers.SelectedValue)).Where(u => u.Active == true).ToList();
        List<ExDay> _exDays = ExDay.GetExDays(Convert.ToInt32(ddlUsers.SelectedValue)).Where(u => u.Active == true).ToList();
        int iTotalNE = 0;
        Boolean lastSignIn = false;
        #region Generate Records
        foreach (RecordModel record in records)
        {
            //&& !record.DayType.HasValue
            if (WorkerLastSignIn < record.Date.Date && _absences.Where(u => u.CrtDate.Date == record.Date && u.Active == true).ToList().Count <= 0)
            {
                continue;
            }
            if (WorkerFirstSignIn > record.Date.Date)
            {
                continue;
            }
            if ((record.Records.Count > 0 && !record.DayType.HasValue))
            {
                continue;
            }
            if ((record.Records.Count == 0 && record.DayType.HasValue))
            {
                continue;
            }

            TableRow tRowRecord = new TableRow();
            string todaysDate = objATZ.GetDate(record.Date.ToString(), HttpContext.Current.User.Identity.Name);

            tRowRecord.Cells.Add(new TableCell() { Text = todaysDate, RowSpan = record.Records.Count });

            #region Handle weekends, holidays etc and set trow class accordingly for first record
            TableCell tcellhw = new TableCell();
            if (record.DayType.HasValue)
            {
                if (record.Records.Count <= 0)
                {
                    //imgMDetails.Visible = false;
                    //subRecordsClockIn.Visible = false;
                }
                if (string.IsNullOrEmpty(record.DayTitle))
                {
                    tcellhw.Text = "Holiday";
                }
                else
                {
                    tcellhw.Text = record.DayTitle;
                }
                tcellhw.Font.Bold = true;
                tcellhw.ColumnSpan = 6;
                tRowRecord.Cells[0].RowSpan = record.Records.Count + 1;
                tRowRecord.Cells.Add(tcellhw);
                tblWeekendWork.Rows.Add(tRowRecord);

                //dtitle.Visible = true;
                if (record.DayType.Value == 0)
                {
                    tRowRecord.Attributes.Add("class", "greenRow");
                }
                //rContainer.Attributes.Add("class", "SatSunHolidays");
                else
                {
                    //rContainer.Attributes.Add("class", "NormalHoliday");
                    tRowRecord.Attributes.Add("class", "yellowRow");
                    //trDayOff.Visible = true;
                }
                tRowRecord = new TableRow();
            }
            else if (record.Records.Count <= 0)
            {
                //Absence absence = new Absence();                                        
                if (_absences.Where(u => u.CrtDate.Date == record.Date && u.Active == true).ToList().Count <= 0)
                {
                    Label lblText = new Label();
                    lblText.ID = "lblText" + ++idS;
                    lblText.CssClass = "lblText";
                    lblText.Text = "No Entry - ";
                    ++iTotalNE;
                    //lblTotalNE.Text = iTotalNE.ToString();
                    LinkButton lnkMarkAbsence = new LinkButton();
                    lnkMarkAbsence.ID = "lnkMarkAbsence" + idS;
                    lnkMarkAbsence.CssClass = "lnkMarkAbsence";
                    lnkMarkAbsence.CommandName = "lnkMarkAbsence";
                    lnkMarkAbsence.Text = "Mark as absent";
                    lnkMarkAbsence.CommandArgument = record.Date.ToString();
                    lnkMarkAbsence.Attributes.Add("data-id", "0");
                    lnkMarkAbsence.Attributes.Add("data-UserId", ddlUsers.SelectedValue.ToString());
                    lnkMarkAbsence.Attributes.Add("data-date", record.Date.ToString());
                    lnkMarkAbsence.Attributes.Add("data-active", "true");
                    lnkMarkAbsence.PostBackUrl = "";
                    tcellhw.Controls.Add(lblText);
                    tcellhw.Controls.Add(lnkMarkAbsence);
                }
                else
                {
                    Label lblText = new Label();
                    lblText.ID = "lblText" + ++idS;
                    lblText.CssClass = "lblText";
                    lblText.Text = "";
                    tcellhw.Controls.Add(lblText);
                    LinkButton lnkMarkAbsence = new LinkButton();
                    lnkMarkAbsence.ID = "lnkMarkPresent" + idS;
                    lnkMarkAbsence.CssClass = "lnkMarkAbsence";
                    lnkMarkAbsence.CommandName = "lnkMarkAbsence";
                    lnkMarkAbsence.Text = "Remove absence";
                    lnkMarkAbsence.CommandArgument = record.Date.ToString();
                    Absence abs = _absences.SingleOrDefault(u => u.CrtDate.Date == record.Date && u.Active == true);
                    lnkMarkAbsence.Attributes.Add("data-id", abs.AbsLogID.ToString());
                    lnkMarkAbsence.Attributes.Add("data-UserId", abs.UserID.ToString());
                    lnkMarkAbsence.Attributes.Add("data-date", abs.CrtDate.ToString());
                    lnkMarkAbsence.Attributes.Add("data-active", "False");
                    lnkMarkAbsence.PostBackUrl = "";
                    Label lblComment = new Label();
                    lblComment.ID = "lblComment" + idS;
                    lblComment.CssClass = "lblComment";
                    lblComment.Text = (abs.Comment.ToString().Length > 15) ? abs.Comment.Substring(0, 15) : abs.Comment;
                    if (String.IsNullOrEmpty(abs.Comment) || abs.Comment == "")
                    {
                        lblComment.Text = "Absent";
                    }
                    Label lblCommentPopup = new Label();
                    lblCommentPopup.ID = "lblCommentPopup" + idS;
                    lblCommentPopup.CssClass = "lblCommentPopup";
                    lblCommentPopup.Text = " ... ";
                    lblCommentPopup.ToolTip = abs.Comment;
                    lblCommentPopup.Attributes.Add("Title", abs.Comment);
                    lblCommentPopup.CssClass = "tooltip";
                    lblCommentPopup.Style.Add("cursor", "pointer");
                    Image imgCommentDetail = new Image();
                    //tRowRecord.Cells.Add(new TableCell() { Text = "<img src=\"images/Controls/icon_info.png\" alt=\"\" title=\"" + details + "\" Class=\"tooltip\" ToolTip=\"" + details + "\" />", RowSpan = record.Records.Count });
                    imgCommentDetail.ImageUrl = "../images/Controls/icon_info.png";
                    imgCommentDetail.CssClass = "tooltip";
                    imgCommentDetail.Attributes.Add("title", abs.Comment);
                    imgCommentDetail.ToolTip = abs.Comment;
                    imgCommentDetail.Style.Add("float", "right");
                    tcellhw.Controls.Add(lblComment);
                    tcellhw.Controls.Add(lblCommentPopup);
                    tcellhw.Controls.Add(lblText);
                    lnkMarkAbsence.Style.Add("float", "right");
                    if (!isWorker)
                    {
                        tcellhw.Controls.Add(lnkMarkAbsence);
                    }
                    //tcellhw.Controls.Add(imgCommentDetail);

                }

                DateTime workerRequestDate = SP.GetWorkerFirstSignInn(ddlUsers.SelectedValue.ToString());
                tRowRecord.Attributes.Add("class", "pinkRow");
                tcellhw.Font.Bold = true;
                tcellhw.ColumnSpan = 6;
                tRowRecord.Cells[0].RowSpan = record.Records.Count + 1;
                tRowRecord.Cells.Add(tcellhw);
                tblWeekendWork.Rows.Add(tRowRecord);
                tRowRecord = new TableRow();
            }
            else
            {
                if (_absences.Where(u => u.CrtDate.Date == record.Date && u.Active == true && u.UserID == Convert.ToInt32(ddlUsers.SelectedValue)).ToList().Count > 0)
                {
                    Absence abs = _absences.SingleOrDefault(u => u.CrtDate.Date == record.Date && u.Active == true);
                    Label lblText = new Label();
                    lblText.ID = "lblText" + ++idS;
                    lblText.CssClass = "lblText";
                    lblText.Text = "Absent - ";
                    LinkButton lnkMarkAbsence = new LinkButton();
                    lnkMarkAbsence.ID = "lnkMarkPresent" + idS;
                    lnkMarkAbsence.CssClass = "lnkMarkAbsence";
                    lnkMarkAbsence.CommandName = "lnkMarkAbsence";
                    lnkMarkAbsence.Text = "Remove absence";
                    lnkMarkAbsence.CommandArgument = record.Date.ToString();
                    lnkMarkAbsence.Attributes.Add("data-id", abs.AbsLogID.ToString());
                    lnkMarkAbsence.Attributes.Add("data-UserId", abs.UserID.ToString().ToString());
                    lnkMarkAbsence.Attributes.Add("data-date", abs.CrtDate.ToString());
                    lnkMarkAbsence.Attributes.Add("data-active", "False");
                    lnkMarkAbsence.PostBackUrl = "";
                    tcellhw.Controls.Add(lblText);
                    if (!isWorker)
                    {
                        tcellhw.Controls.Add(lnkMarkAbsence);
                    }



                    Image imgCommentDetail = new Image();
                    //tRowRecord.Cells.Add(new TableCell() { Text = "<img src=\"images/Controls/icon_info.png\" alt=\"\" title=\"" + details + "\" Class=\"tooltip\" ToolTip=\"" + details + "\" />", RowSpan = record.Records.Count });
                    imgCommentDetail.ImageUrl = "~/images/Controls/icon_info.png";
                    imgCommentDetail.CssClass = "tooltip";
                    imgCommentDetail.Attributes.Add("title", abs.Comment);
                    imgCommentDetail.ToolTip = abs.Comment;
                    imgCommentDetail.Style.Add("float", "right");

                    tRowRecord.Attributes.Add("class", "pinkRow");
                    tcellhw.Font.Bold = true;
                    tcellhw.ColumnSpan = 6;

                    tRowRecord.Cells[0].RowSpan = record.Records.Count + 1;
                    tRowRecord.Cells.Add(tcellhw);
                    tcellhw.Controls.Add(imgCommentDetail);

                    tblWeekendWork.Rows.Add(tRowRecord);
                    tRowRecord = new TableRow();
                }

            }
            #endregion

            if (record.Records.Count > 0)
            {
                if (record.DayType.HasValue)
                {
                    if (record.DayType.Value == 0)
                    {
                        tRowRecord.Attributes.Add("class", "greenRow");
                    }
                    else
                    {
                        tRowRecord.Attributes.Add("class", "yellowRow");
                    }
                }

                if ((String.IsNullOrEmpty(record.Records[0].LogoutTime) || record.Records[0].LogoutTime == "") && record.Records.Count < 1)
                {
                    tRowRecord.CssClass = "hide";
                }

                // Add Clock-In time
                if (!String.IsNullOrEmpty(record.Records[0].LoginTime) || record.Records[0].LoginTime != "")
                {
                    if (record.Date.Date == Convert.ToDateTime(record.Records[0].LoginTime).Date)
                    {
                        tRowRecord.Cells.Add(new TableCell() { Text = objATZ.GetTime(record.Records[0].LoginTime, HttpContext.Current.User.Identity.Name) });
                    }
                    else
                    {
                        tRowRecord.Cells.Add(new TableCell() { Text = objATZ.GetTime(record.Records[0].LoginTime, HttpContext.Current.User.Identity.Name) + " - " + Convert.ToDateTime(record.Records[0].LoginTime).Date.ToString("MMM dd yyyy") });
                    }
                }

                // Add Clock-Out time
                Label lblTotalTime = new Label();
                if (!String.IsNullOrEmpty(record.Records[0].LogoutTime) || record.Records[0].LogoutTime != "")
                {
                    #region Delete Hours for Record[0]
                    Image imgDeleteHours = new Image();
                    imgDeleteHours.AlternateText = " - Delete Hours";
                    imgDeleteHours.ImageUrl = "~/images/Controls/Minusicon.png";

                    imgDeleteHours.ID = "imgDeleteHours" + ++idS + "" + record.Records[0].Id;


                    imgDeleteHours.CssClass = "imgDeleteHours tooltip";
                    imgDeleteHours.ToolTip = "Use this option to delete time from your recorded history in order to fix any excess time you recorded by mistake.";
                    imgDeleteHours.Attributes.Add("Title", "Delete Hours");

                    string intime = objATZ.GetTime(record.Records[0].LoginTime, ddlUsers.SelectedValue.ToString());
                    intime = ((!IsSimpleClock) ? Convert.ToDateTime(intime).ToString("HH:mm") : intime);
                    imgDeleteHours.Attributes.Add("data-frmTime", intime);

                    String outtime = objATZ.GetTime(record.Records[0].LogoutTime, ddlUsers.SelectedValue.ToString());
                    outtime = ((!IsSimpleClock) ? Convert.ToDateTime(outtime).ToString("HH:mm") : outtime);

                    string inDate = objATZ.GetDate(record.Records[0].LoginTime, HttpContext.Current.User.Identity.Name);
                    if (inDate.Contains("Today"))
                    {
                        inDate = DateTime.Now.ToString();
                    }
                    string outDate = objATZ.GetDate(record.Records[0].LogoutTime, HttpContext.Current.User.Identity.Name);
                    if (outDate.Contains("Today"))
                    {
                        outDate = DateTime.Now.ToString();
                    }
                    imgDeleteHours.Attributes.Add("data-toTime", outtime);

                    imgDeleteHours.Attributes.Add("data-day", record.Date.ToString("ddd, MMM dd yyyy") + " from " + intime + " to " + outtime);
                    imgDeleteHours.Attributes.Add("data-recordID", record.Records[0].Id.ToString());
                    imgDeleteHours.Style.Add("float", "right");
                    #endregion

                    #region Edit Hours Record[0] -- pencil.png

                    Image imgEditHours = new Image();
                    imgEditHours.AlternateText = " - Edit Hours";
                    imgEditHours.ImageUrl = "~/images/Controls/pencil.png";
                    imgEditHours.ID = "imgEditHours" + ++idS + "" + record.Records[0].Id;
                    imgEditHours.CssClass = "imgEditHours tooltip";
                    imgEditHours.ToolTip = "Use this option to edit time. Admin only";
                    imgEditHours.Attributes.Add("Title", "Edit Time");
                    imgEditHours.Attributes.Add("data-frmTime", intime);
                    imgEditHours.Attributes.Add("data-toTime", outtime);
                    imgEditHours.Attributes.Add("data-frmdate", Convert.ToDateTime(inDate).ToShortDateString() + " " + intime);
                    imgEditHours.Attributes.Add("data-todate", Convert.ToDateTime(outDate).ToShortDateString() + " " + outtime);
                    imgEditHours.Attributes.Add("data-recordID", record.Records[0].Id.ToString());
                    imgEditHours.Style.Add("width", "12px");
                    imgEditHours.Style.Add("margin-left", "15px");
                    imgEditHours.Style.Add("cursor", "pointer");
                    imgEditHours.Style.Add("float", "right");
                    #endregion

                    TableCell tblCellLnkDelHours = new TableCell();
                    Label lblLogout = new Label();
                    if (record.Date.Date == Convert.ToDateTime(record.Records[0].LogoutTime).Date)
                    {
                        lblLogout.Text = objATZ.GetTime(record.Records[0].LogoutTime, HttpContext.Current.User.Identity.Name);
                        tblCellLnkDelHours.Controls.Add(lblLogout);
                        tblCellLnkDelHours.Controls.Add(imgDeleteHours);
                        if (!isWorker)
                        {
                            tblCellLnkDelHours.Controls.Add(imgEditHours);
                        }
                        tRowRecord.Cells.Add(tblCellLnkDelHours);
                    }
                    else
                    {
                        lblLogout.Text = objATZ.GetTime(record.Records[0].LogoutTime, HttpContext.Current.User.Identity.Name) + " - " + Convert.ToDateTime(record.Records[0].LogoutTime).Date.ToString("MMM dd yyyy");
                        tblCellLnkDelHours.Controls.Add(lblLogout);
                        tblCellLnkDelHours.Controls.Add(imgDeleteHours);
                        if (!isWorker)
                        {
                            tblCellLnkDelHours.Controls.Add(imgEditHours);
                        }
                        tRowRecord.Cells.Add(tblCellLnkDelHours);
                    }
                }
                else
                {
                    //lnkDeleteHours.CssClass = "imgDeleteHours";
                    //lnkDeleteHours.ToolTip = "Use this option to delete time from your recorded history in order to fix any excess time you recorded by mistake.";

                    //string time = objATZ.GetTime(record.Records[0].LoginTime, UserId.toString());
                    //time = ((!IsSimpleClock) ? Convert.ToDateTime(time).ToString("HH:mm") : time);
                    //lnkDeleteHours.Attributes.Add("data-frmTime", time);

                    //time = objATZ.GetTime(DateTime.Now.ToString(), UserId.toString());
                    //time = ((!IsSimpleClock) ? Convert.ToDateTime(time).ToString("HH:mm") : time);
                    //lnkDeleteHours.Attributes.Add("data-toTime", time);
                    //lnkDeleteHours.Attributes.Add("data-recordID", record.Records[0].Id.ToString());

                    //imgDeleteHours.Attributes.Add("data-day", record.Date.ToString("ddd, MM dd yyyy") + " from " + intime + " to " + outtime);

                    string intime = objATZ.GetTime(record.Records[0].LoginTime, UserId.ToString());
                    intime = ((!IsSimpleClock) ? Convert.ToDateTime(intime).ToString("HH:mm") : intime);
                    //lnkDeleteHours.Attributes.Add("data-frmTime", intime);

                    String outtime = objATZ.GetTime(record.Records[0].LogoutTime, UserId.ToString());
                    outtime = ((!IsSimpleClock) ? Convert.ToDateTime(outtime).ToString("HH:mm") : outtime);

                    string inDate = objATZ.GetDate(record.Records[0].LoginTime, HttpContext.Current.User.Identity.Name);
                    if (inDate.Contains("Today"))
                    {
                        inDate = DateTime.Now.ToString();
                    }
                    string outDate = objATZ.GetDate(record.Records[0].LogoutTime, HttpContext.Current.User.Identity.Name);
                    if (outDate.Contains("Today"))
                    {
                        outDate = DateTime.Now.ToString();
                    }
                    //lnkDeleteHours.Attributes.Add("data-toTime", outtime);

                    if (String.IsNullOrWhiteSpace(outtime) || outtime.Contains("&nbsp;"))
                    {
                        //lnkDeleteHours.Attributes.Add("data-day", record.Date.ToString("ddd, MMM dd yyyy") + " from " + intime + " to till now");
                    }
                    else
                    {
                        //lnkDeleteHours.Attributes.Add("data-day", record.Date.ToString("ddd, MMM dd yyyy") + " from " + intime + " to " + outtime);
                    }


                    //lnkDeleteHours.Attributes.Add("data-title", "");
                    TableCell tblCellLnkDelHours = new TableCell();
                    Label lblLogout = new Label();
                    tRowRecord.Cells.Add(new TableCell() { Text = "..." });

                    lastSignIn = true;
                    lblTotalTime.Attributes.Add("data-incdate", UtilityMethods.getClientDateTime(Convert.ToDateTime(record.Records[0].LoginTime)).ToString());
                    lblTotalTime.ID = "lblTotalTime" + idS + "" + record.Records[0];
                }

                // Add Totaltime with breaks
                lblTotalTime.Text = record.TotalWorkedHours;
                TableCell cellTotalTime = new TableCell();
                cellTotalTime.Controls.Add(lblTotalTime);
                cellTotalTime.RowSpan = record.Records.Count;
                tRowRecord.Cells.Add(cellTotalTime);

                // Add Total break time                    
                TableCell cellTotalBreak = new TableCell();
                cellTotalBreak.Text = record.TotalBreak;
                cellTotalBreak.RowSpan = record.Records.Count;
                tRowRecord.Cells.Add(cellTotalBreak);

                #region --- Create detail information ---
                String details = "<table id='tblDetail'><tr>";
                details += "<td>Clock-In Location:</td>";
                int breakcolspanIn = 0;
                int breakcolspanOut = 0;
                // Check if location in is same or not
                bool sameLocationIn = true;
                bool sameLocationOut = true;
                foreach (SubRecordModel subRecord in record.Records)
                {
                    if (subRecord.LoginStatus != record.Records[0].LoginStatus)
                    {
                        sameLocationIn = false;
                    }
                    if (subRecord.LogoutStatus != record.Records[0].LogoutStatus)
                    {
                        sameLocationOut = false;
                    }
                }

                bool execIn = true;
                foreach (SubRecordModel subRecord in record.Records)
                {
                    if (sameLocationIn)
                    {
                        if (execIn)
                        {
                            breakcolspanIn++;
                            details += "<td>" + subRecord.InLocation;
                            if (!String.IsNullOrEmpty(subRecord.InLocation))
                            {
                                details += ((subRecord.LoginStatus == "Notverified") ? "<img src='images/Controls/cross.png' style='margin-left:4px' alt='Notverified' width='13px'/>" : "<img src='images/Controls/tick.png' width='13px' alt='Verified' class='tooltip' style='margin-left:4px' />") + "</td>";
                            }
                            execIn = false;
                        }
                    }
                    else
                    {
                        breakcolspanIn++;
                        details += "<td>" + subRecord.InLocation;
                        if (!String.IsNullOrEmpty(subRecord.InLocation))
                        {
                            details += ((subRecord.LoginStatus == "Notverified") ? "<img src='images/Controls/cross.png' style='margin-left:4px' alt='Notverified' width='13px'/>" : "<img src='images/Controls/tick.png' width='13px' alt='Verified' class='tooltip' style='margin-left:4px' />") + "</td>";
                        }
                    }
                }
                details += "</tr><tr><td>Clock-Out Location:</td>";
                bool execOut = true;
                foreach (SubRecordModel subRecord in record.Records)
                {
                    if (sameLocationOut)
                    {
                        if (execOut)
                        {
                            breakcolspanOut++;
                            details += "<td>" + subRecord.OutLocation;
                            if (!String.IsNullOrEmpty(subRecord.OutLocation))
                            {
                                details += ((subRecord.LogoutStatus == "Notverified") ? "<img src='images/Controls/cross.png' style='margin-left:4px' alt='Notverified' width='13px'/>" : "<img src='images/Controls/tick.png' width='13px' alt='Verified' class='tooltip' style='margin-left:4px' />") + "</td>";
                            }
                            else
                            {
                                details += "... </td>";
                            }
                            execOut = false;
                        }
                    }
                    else
                    {
                        breakcolspanOut++;
                        details += "<td>" + subRecord.OutLocation;
                        if (!String.IsNullOrEmpty(subRecord.OutLocation))
                        {
                            details += ((subRecord.LogoutStatus == "Notverified") ? "<img src='images/Controls/cross.png' style='margin-left:4px' alt='Notverified' width='13px'/>" : "<img src='images/Controls/tick.png' width='13px' alt='Verified' class='tooltip' style='margin-left:4px' />") + "</td>";
                        }
                    }
                }
                details += "</tr>";

                foreach (BreakModel breakModel in record.Breaks)
                {
                    details += string.Format("<tr><td>Break:</td><td colspan='" + ((breakcolspanIn > breakcolspanOut) ? breakcolspanIn + 1 : breakcolspanOut + 1) + "'>From: {0} To: {1}", Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + breakModel.BreakStartTime.ToString()).ToShortTimeString(), Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + breakModel.BreakEndTime.ToString()).ToShortTimeString()) + "</td>";
                }
                details += "</tr><tr><td>Total Time:</td><td colspan='" + ((breakcolspanIn > breakcolspanOut) ? breakcolspanIn + 1 : breakcolspanOut + 1) + "'>" + record.TotalTime + "</td></table>";
                // Info Image
                tRowRecord.Cells.Add(new TableCell() { Text = "<img src=\"images/Controls/icon_info.png\" alt=\"\" title=\"" + details + "\" Class=\"tooltip\" ToolTip=\"" + details + "\" />", RowSpan = record.Records.Count });

                #region --- Mark as Partial ---
                TableCell tbcell = new TableCell();
                if (_exDays.Where(u => u.CrtDate.Date.Date == record.Date.Date && u.Active == true).ToList().Count > 0)
                {
                    ExDay exDay = new ExDay();
                    try
                    {
                        exDay = _exDays.Last();
                    }
                    catch (Exception)
                    {
                    }

                    //Label lblText = new Label();
                    //lblText.ID = "lblText" + ++idS;
                    //lblText.CssClass = "lblText";
                    //lblText.Text = "Partial Day - ";
                    Image lnkMarkNPW = new Image();
                    lnkMarkNPW.ID = "lnkMarkNPW" + idS;
                    lnkMarkNPW.CssClass = "lnkMarkNPW tooltip";
                    lnkMarkNPW.ImageUrl = "~/images/controls/piechart.png";
                    lnkMarkNPW.Attributes.Add("tooltip", "Mark as not partial working");
                    lnkMarkNPW.Attributes.Add("Title", "Mark as not partial working");
                    //lnkMarkNPW.Attributes.Add("class", "tooltip");
                    lnkMarkNPW.Style.Add("width", "16px");
                    lnkMarkNPW.Style.Add("cursor", "pointer");
                    lnkMarkNPW.Attributes.Add("data-id", exDay.ExDayID.ToString());
                    lnkMarkNPW.Attributes.Add("data-ex-type", Convert.ToInt32(exDay.ExDayTypeID).ToString());
                    lnkMarkNPW.Attributes.Add("data-UserId", exDay.UserID.ToString().ToString());
                    lnkMarkNPW.Attributes.Add("data-date", exDay.CrtDate.ToString());
                    lnkMarkNPW.Attributes.Add("data-active", "False");
                    //tbcell.Controls.Add(lblText);
                    tbcell.Controls.Add(lnkMarkNPW);

                    //Image imgCommentDetail = new Image();
                    ////tRowRecord.Cells.Add(new TableCell() { Text = "<img src=\"images/Controls/icon_info.png\" alt=\"\" title=\"" + details + "\" Class=\"tooltip\" ToolTip=\"" + details + "\" />", RowSpan = record.Records.Count });
                    //imgCommentDetail.ImageUrl = "images/Controls/icon_info.png";
                    //imgCommentDetail.CssClass = "tooltip";
                    //imgCommentDetail.Attributes.Add("title", exDay.Comment);
                    //imgCommentDetail.ToolTip = exDay.Comment;
                    //imgCommentDetail.Style.Add("float", "right");

                    tRowRecord.Attributes.Add("class", "eggshellRow");
                    tbcell.Font.Bold = true;
                    tRowRecord.Cells[0].RowSpan = record.Records.Count;
                    tbcell.RowSpan = record.Records.Count;
                    tRowRecord.Cells.Add(tbcell);
                    //tcellhw.Controls.Add(imgCommentDetail);                        
                }
                else
                {
                    Label lblText = new Label();
                    lblText.ID = "lblText" + ++idS;
                    lblText.CssClass = "lblText";
                    lblText.Text = "";
                    Image lnkMarkNPW = new Image();
                    lnkMarkNPW.ID = "lnkMarkNPW" + idS;
                    lnkMarkNPW.CssClass = "lnkMarkNPW tooltip";
                    lnkMarkNPW.ImageUrl = "~/images/controls/piechart.png";
                    lnkMarkNPW.Attributes.Add("tooltip", "Mark as partial working");
                    lnkMarkNPW.Attributes.Add("Title", "Mark as partial working");
                    //lnkMarkNPW.Attributes.Add("class", "tooltip");
                    lnkMarkNPW.Style.Add("width", "16px");
                    lnkMarkNPW.Style.Add("cursor", "pointer");
                    lnkMarkNPW.Attributes.Add("data-id", "0");
                    lnkMarkNPW.Attributes.Add("data-ex-type", "1");
                    lnkMarkNPW.Attributes.Add("data-UserId", ddlUsers.SelectedValue.ToString());
                    lnkMarkNPW.Attributes.Add("data-date", record.Date.ToString());
                    lnkMarkNPW.Attributes.Add("data-active", "True");
                    tbcell.Controls.Add(lblText);
                    tbcell.Controls.Add(lnkMarkNPW);
                    tbcell.RowSpan = record.Records.Count;
                    tRowRecord.Cells.Add(tbcell);
                }
                #endregion

                tblWeekendWork.Rows.Add(tRowRecord);
                #endregion



                #region --- Create SubRecords ---
                for (int i = 1; i < record.Records.Count; i++)
                {
                    TableRow subRecord = new TableRow();
                    if (record.DayType.HasValue)
                    {
                        if (record.DayType.Value == 0)
                        {
                            subRecord.Attributes.Add("class", "greenRow");
                        }
                        else
                        {
                            subRecord.Attributes.Add("class", "yellowRow");
                        }
                    }
                    TableCell cellClockInTime = new TableCell();
                    TableCell cellClockOutTime = new TableCell();
                    if (!String.IsNullOrEmpty(record.Records[i].LoginTime) || record.Records[i].LoginTime != "")
                    {
                        if (record.Date.Date == Convert.ToDateTime(record.Records[i].LoginTime).Date)
                        {
                            cellClockInTime.Text = objATZ.GetTime(record.Records[i].LoginTime, HttpContext.Current.User.Identity.Name);
                        }
                        else
                        {
                            cellClockInTime.Text = objATZ.GetTime(record.Records[i].LoginTime, HttpContext.Current.User.Identity.Name) + "-" + Convert.ToDateTime(record.Records[i].LoginTime).Date.ToShortDateString();
                        }
                    }
                    if (!String.IsNullOrEmpty(record.Records[i].LogoutTime) || record.Records[i].LogoutTime != "")
                    {
                        #region Delete Hours for Record[i]
                        Image imgDeleteHours2 = new Image();
                        imgDeleteHours2.AlternateText = " - Delete Hours";
                        imgDeleteHours2.ImageUrl = "../images/Controls/Minusicon.png";
                        imgDeleteHours2.ID = "imgDeleteHours2" + i + "" + record.Records[i].Id;
                        imgDeleteHours2.CssClass = "imgDeleteHours tooltip";
                        imgDeleteHours2.ToolTip = "Use this option to delete time from your recorded history in order to fix any excess time you recorded by mistake.";
                        imgDeleteHours2.Attributes.Add("Title", "Delete Hours");

                        string intime = objATZ.GetTime(record.Records[i].LoginTime, ddlUsers.SelectedValue.ToString());
                        intime = ((!IsSimpleClock) ? Convert.ToDateTime(intime).ToString("HH:mm") : intime);
                        imgDeleteHours2.Attributes.Add("data-frmTime", intime);

                        String outtime = objATZ.GetTime(record.Records[i].LogoutTime, ddlUsers.SelectedValue.ToString());
                        outtime = ((!IsSimpleClock) ? Convert.ToDateTime(outtime).ToString("HH:mm") : outtime);

                        string inDate = objATZ.GetDate(record.Records[i].LoginTime, HttpContext.Current.User.Identity.Name);
                        if (inDate.Contains("Today"))
                        {
                            inDate = DateTime.Now.ToString();
                        }
                        string outDate = objATZ.GetDate(record.Records[i].LogoutTime, HttpContext.Current.User.Identity.Name);
                        if (outDate.Contains("Today"))
                        {
                            outDate = DateTime.Now.ToString();
                        }
                        imgDeleteHours2.Attributes.Add("data-toTime", outtime);

                        imgDeleteHours2.Attributes.Add("data-day", record.Date.ToString("ddd, MMM dd yyyy") + " from " + intime + " to " + outtime);
                        imgDeleteHours2.Attributes.Add("data-recordID", record.Records[i].Id.ToString());
                        imgDeleteHours2.Style.Add("float", "right");
                        Label lblLogout = new Label();
                        #endregion

                        #region Edit Hours Record[i] -- pencil.png
                        Image imgEditHours2 = new Image();
                        imgEditHours2.AlternateText = " - Edit Hours";
                        imgEditHours2.ImageUrl = "~/images/Controls/pencil.png";
                        imgEditHours2.ID = "imgEditHours2" + i + "" + record.Records[i].Id;
                        imgEditHours2.CssClass = "imgEditHours tooltip";
                        imgEditHours2.ToolTip = "Use this option to edit time. Admin only";
                        imgEditHours2.Attributes.Add("Title", "Edit Time");
                        imgEditHours2.Attributes.Add("data-frmTime", intime);
                        imgEditHours2.Attributes.Add("data-toTime", outtime);
                        imgEditHours2.Attributes.Add("data-frmDate", inDate);
                        imgEditHours2.Attributes.Add("data-toDate", outDate);
                        imgEditHours2.Attributes.Add("data-recordID", record.Records[i].Id.ToString());
                        imgEditHours2.Style.Add("width", "12px");
                        imgEditHours2.Style.Add("cursor", "pointer");
                        imgEditHours2.Style.Add("float", "right");
                        imgEditHours2.Style.Add("margin-left", "15px");
                        #endregion

                        if (record.Date.Date == Convert.ToDateTime(record.Records[i].LogoutTime).Date)
                        {
                            lblLogout.Text = objATZ.GetTime(record.Records[i].LogoutTime, HttpContext.Current.User.Identity.Name);
                            cellClockOutTime.Controls.Add(lblLogout);
                            cellClockOutTime.Controls.Add(imgDeleteHours2);
                            if (!isWorker)
                            {
                                cellClockOutTime.Controls.Add(imgEditHours2);
                            }
                            tRowRecord.Cells.Add(cellClockOutTime);
                        }
                        else
                        {
                            lblLogout.Text = objATZ.GetTime(record.Records[i].LogoutTime, HttpContext.Current.User.Identity.Name) + " - " + Convert.ToDateTime(record.Records[i].LogoutTime).Date.ToString("MMM dd yyyy");
                            cellClockOutTime.Controls.Add(lblLogout);
                            cellClockOutTime.Controls.Add(imgDeleteHours2);
                            if (!isWorker)
                            {
                                cellClockOutTime.Controls.Add(imgEditHours2);
                            }
                            tRowRecord.Cells.Add(cellClockOutTime);
                        }
                    }
                    else
                    {
                        lastSignIn = true;
                    }

                    subRecord.Cells.Add(cellClockInTime);
                    subRecord.Cells.Add(cellClockOutTime);
                    tblWeekendWork.Rows.Add(subRecord);
                }
                #endregion
            }
            else
            {

                if (record.DayType.HasValue)
                {
                    if (record.Records.Count <= 0)
                    {
                        //imgMDetails.Visible = false;
                        //subRecordsClockIn.Visible = false;
                    }
                    if (string.IsNullOrEmpty(record.DayTitle))
                    {
                        tcellhw.Text = "Holiday";
                    }
                    else
                    {
                        tcellhw.Text = record.DayTitle;
                    }
                }
                else
                {
                    //imgMDetails.Visible = true;
                    //subRecordsClockIn.Visible = true;
                }
            }

        }
        #endregion
    }



    protected void deleHrs_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(rID.Value))
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("getWorkerRecordByRowId", con))
                {
                    AvaimaTimeZoneAPI obj = new AvaimaTimeZoneAPI();
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", rID.Value);
                    cmd.Parameters.AddWithValue("@ppid", ddlUsers.SelectedValue);
                    DataTable dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        DateTime frmDateTime = Convert.ToDateTime(dt.Rows[0].Field<DateTime>("lastin").ToString("g"));
                        DateTime toDateTime = Convert.ToDateTime(dt.Rows[0].Field<DateTime>("lastout").ToString("g"));

                        DateTime tfdt = Convert.ToDateTime(frmDateTime.ToShortDateString() + " " + obj.GetTimeReverse(txtfrmTime.Text, ddlUsers.SelectedValue.ToString()));
                        DateTime ttdt = Convert.ToDateTime(toDateTime.ToShortDateString() + " " + obj.GetTimeReverse(txtToTime.Text, ddlUsers.SelectedValue.ToString()));
                        if (tfdt < frmDateTime)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('From time must be greater than or equal to current saved login time')", true);
                            return;
                        }
                        else if (tfdt >= toDateTime)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('From time must be less than current saved logout time')", true);
                            return;
                        }
                        else if (ttdt > toDateTime)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('To time must be less than or equal to current saved logout time')", true);
                            return;
                        }
                        else if (ttdt <= frmDateTime)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('To time must be greater than current saved login time')", true);
                            return;
                        }
                        else if (tfdt >= ttdt)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('From time must be less than To time')", true);
                            return;
                        }
                        else
                        {
                            if (tfdt > frmDateTime && ttdt == toDateTime)
                            {
                                cmd.CommandText = "updateWorkerTimeById";
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@Id", rID.Value);
                                cmd.Parameters.AddWithValue("@ppId", ddlUsers.SelectedValue);
                                cmd.Parameters.AddWithValue("@lastin", frmDateTime);
                                cmd.Parameters.AddWithValue("@lastout", tfdt);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.ExecuteNonQuery();
                            }
                            else if (tfdt == frmDateTime && ttdt < toDateTime)
                            {
                                cmd.CommandText = "updateWorkerTimeById";
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@Id", rID.Value);
                                cmd.Parameters.AddWithValue("@ppId", ddlUsers.SelectedValue);
                                cmd.Parameters.AddWithValue("@lastin", ttdt);
                                cmd.Parameters.AddWithValue("@lastout", toDateTime);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.ExecuteNonQuery();
                            }
                            else if (tfdt > frmDateTime && ttdt < toDateTime)
                            {
                                cmd.CommandText = "updateWorkerTimeById";
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@Id", rID.Value);
                                cmd.Parameters.AddWithValue("@ppId", ddlUsers.SelectedValue);
                                cmd.Parameters.AddWithValue("@lastin", frmDateTime);
                                cmd.Parameters.AddWithValue("@lastout", tfdt);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.ExecuteNonQuery();

                                cmd.CommandText = "insertWorkerTime";
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@ppId", ddlUsers.SelectedValue);
                                cmd.Parameters.AddWithValue("@userid", ddlUsers.SelectedValue);
                                cmd.Parameters.AddWithValue("@lastin", ttdt);
                                cmd.Parameters.AddWithValue("@lastout", toDateTime);
                                cmd.Parameters.AddWithValue("@address", Request.UserHostAddress);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.ExecuteNonQuery();
                            }
                            else if (tfdt == frmDateTime && ttdt == toDateTime)
                            {
                                cmd.CommandText = "updateWorkerTimeById";
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@Id", rID.Value);
                                cmd.Parameters.AddWithValue("@ppId", ddlUsers.SelectedValue);
                                cmd.Parameters.AddWithValue("@lastin", frmDateTime);
                                cmd.Parameters.AddWithValue("@lastout", frmDateTime);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
        }
    }

    protected void btnUpdateTime_Click(object sender, EventArgs e)
    {
        //hdnerid.Value = UserId.ToString();
        if (Page.IsValid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                if (!String.IsNullOrEmpty(txtEFrmTime.Text) && !String.IsNullOrEmpty(txtEToTime.Text))
                {

                    if (Convert.ToDateTime(txtEFrmTime.Text).ToString() == Convert.ToDateTime(txtEToTime.Text).ToString())
                    {
                        using (SqlCommand cmd = new SqlCommand("DELETE workertrans WHERE rowID = @rowID", con))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@rowID", hdnerid.Value.ToString());
                            con.Open();
                            cmd.ExecuteNonQuery();
                            //SetUserStatus();
                            //FillandCreateHistory(UserId, con);
                            FillandCreateHistory(ddlUsers.SelectedValue.ToString(), con);
                            return;
                        }
                    }
                }


                using (SqlCommand cmd = new SqlCommand("updateworkertime", con))
                {
                    AvaimaTimeZoneAPI objtime = new AvaimaTimeZoneAPI();
                    DateTime? timein = null;
                    if (!string.IsNullOrEmpty(txtEFrmTime.Text))
                    {
                        //timein = Convert.ToDateTime(DateTime.ParseExact(txteditsignin.Text, "dd/MM/yyyy", null).ToShortDateString() + " " + objtime.GetTimeReverse(txteditintime.Text, HttpContext.Current.User.Identity.Name));
                        timein = Convert.ToDateTime(txtEFrmTime.Text);
                        string time = objtime.GetTimeReverse(timein.Value.ToShortTimeString(), HttpContext.Current.User.Identity.Name);
                        timein = Convert.ToDateTime(timein.Value.Date.ToShortDateString() + " " + time);
                    }
                    DateTime? timeout = null;
                    if (!string.IsNullOrEmpty(txtEToTime.Text))
                    {
                        timeout = Convert.ToDateTime(txtEToTime.Text);
                        string time = objtime.GetTimeReverse(timeout.Value.ToShortTimeString(), HttpContext.Current.User.Identity.Name);
                        timeout = Convert.ToDateTime(timeout.Value.Date.ToShortDateString() + " " + time);
                    }
                    con.Open();
                    if (timein.HasValue)
                    {
                        cmd.Parameters.AddWithValue("@lastin", timein);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@lastin", DBNull.Value);
                    }

                    if (timeout.HasValue)
                    {
                        cmd.Parameters.AddWithValue("@lastout", timeout);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@lastout", DBNull.Value);
                    }
                    cmd.Parameters.AddWithValue("@id", hdnerid.Value);
                    cmd.Parameters.AddWithValue("@comment", txteComments.Text);
                    cmd.Parameters.AddWithValue("@instatus", ddlCIV.SelectedValue);
                    cmd.Parameters.AddWithValue("@outstatus", ddlCOV.SelectedValue);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
                //SetUserStatus();
                //FillandCreateHistory(UserId, con);
                FillandCreateHistory(ddlUsers.SelectedValue.ToString(), con);
            }
        }
    }

    protected void BtncommentClick(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            con.Open();
            using (SqlCommand cmd = new SqlCommand("insertcomments", con))
            {
                cmd.Parameters.AddWithValue("@id", ddlUsers.SelectedValue);
                cmd.Parameters.AddWithValue("@comment", txtcomment.Text);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }
    }

    public string OwnerId { get; set; }

    public bool isAdmin { get; set; }

    public bool isWorker { get; set; }

    public int appAssignedId { get; set; }

    public List<User> Users { get; set; }

    public int idS { get; set; }

    public int aidS { get; set; }
}
