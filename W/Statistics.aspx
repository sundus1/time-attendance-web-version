﻿<%@ Page Title="" Language="C#" MasterPageFile="~/W/MasterPage.master" AutoEventWireup="true" CodeFile="Statistics.aspx.cs" Inherits="Statistics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>--%>
    <%-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>
    <script type="text/javascript" src="../js/jquery.timeentry.js"></script>
    <script>

        $(document).ready(function () {
            var date = new Date();
            var currentMonth = date.getMonth();
            var currentDate = date.getDate();
            var currentYear = date.getFullYear();

            $("#dateFrom").datepicker({
                numberOfMonths: 1,
                //minDate: new Date(currentYear, currentMonth - 1, currentDate),
                maxDate: new Date,
                autoclose: true,
                onSelect: function (dateText) {
                    // console.log("Selected date: " + dateText + "; input's current value: " + this.value);
                    var newdate = new Date(this.value);
                    var newMonth = newdate.getMonth();
                    var newDate = newdate.getDate();
                    var newYear = newdate.getFullYear();
                    $('#dateTo').datepicker('destroy');
                    $("#dateTo").datepicker({
                        numberOfMonths: 1,
                        minDate: new Date(newYear, newMonth - 1, newDate),
                        // minDate : "-1M " + "-" + (date.getDate() - 1) + "D",
                        maxDate: newdate,
                        autoclose: true
                    });

                }
            });

            $("#dateTo").datepicker({
                numberOfMonths: 1,
                minDate: "-1M",
                //minDate: new Date(currentYear, currentMonth - 1, currentDate),
                maxDate: new Date,
                autoclose: true
            });



            //$("#dateFrom, #dateTo").datepicker({
            //    numberOfMonths: 1,
            //    maxDate: new Date,
            //    autoclose: true
            //});

            //$(".check-time").timeEntry({
            //    defaultTime: "00:00 AM",
            //    ampmPrefix: ' '
            //});


<%--            $("#<%=txtHours.ClientID%>,#<%=txtBrkHour.ClientID%>").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });--%>


            $('#rbtnReportType input').change(function () {

                var type = $(this).val();
                $("#month_tr").hide();
                $("#custom_tr").hide();
                $("#year_tr").hide();

                if (type == "yearly") {
                    $("#year_tr").show('slow');
                }
                else if (type == "monthly") {
                    $("#month_tr").show('slow');
                }
                else if (type == "custom") {
                    $("#custom_tr").show('slow');
                }

            });


            $('body').find('#<%=ddlAdditionalBrk.ClientID%>').change(function () {
                var type = $(this).val();

                if (type == "week") {
                    $('.brkday').show();
                }
                else {
                    $('.brkday').hide();
                }
            });

            //$('.detailReport').on('click', function () {
            //    $("[id$=detail_stats]").show();
            //});

            $(document).on('click', '.imgInfo', function () {
                var dlg = jq191("#InfoDiv").dialog({
                    width: 550,
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });


            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);


            $("#ddlreports").change(function () {
                if (this.value == 1) {
                    $("#ltgeneral").show();
                    $(".general").show();
                    $(".tags").hide();
                    $(".DivRemove").hide();
                    $(".DivTags").hide();
                    $("#reportDiv").hide();
                    
                }
                else if (this.value == 2) {
                    $("#ltgeneral").show();
                    $(".tags").show();
                    $(".general").hide();
                    $(".DivRemove").hide();
                    $(".DivTags").hide();
                    $("#reportDiv").hide();
                }
                else {
                    $("#ltgeneral").hide();
                    $(".DivRemove").hide();
                    $(".DivTags").hide();
                    $("#reportDiv").hide();
                }
            });
            //validate("list");
        });



        //function ApplyFilter(ev) {
        //    if (ev.text == "Advance Calculator") {
        //        $(".filter").removeClass('hide');
        //        ev.text = "Close";
        //    }
        //    else {
        //        $(".filter").addClass('hide');
        //        ev.text = "Advance Calculator";
        //    }

        //    ApplyClass();
        //    return false;
        //}

        function ApplyFilter(ev) {
            if (ev.text != "Close") {
                $(".filter").removeClass('hide');
                $(".timeEntry_control").remove();
                $("#lnkApplyFilter").show();
                //ev.text = "Close";
            }
            else {
                $(".filter").addClass('hide');
                $("#lnkApplyFilter").hide();
                //ev.text = "Advance Calculator";
            }

            ApplyClass();
            return false;
        }

  <%--      function EmpSetting(ev) {
            if ($(ev).attr("data-type") == "view") {
                $(ev).attr("data-type", "hide");
                $("#<%=rptHolidayWeekend.ClientID%>").show();
                ev.text = "Hide";
            }
            else {
                $(ev).attr("data-type", "view");
                $("#<%=rptHolidayWeekend.ClientID%>").hide();
                ev.text = "View employee personal settings";
            }
            return false;
        }--%>

        function ApplyClass() {
            $(".check-time1").timeEntry({
                //defaultTime: "09:00 AM",
                defaultTime: "",
                placeholder: "HH:MM AM",
                ampmPrefix: ' '
            });

            $(".check-time2").timeEntry({
                //defaultTime: "06:00 PM",
                defaultTime: "",
                placeholder: "HH:MM PM",
                ampmPrefix: ' '
            });


       <%--     $("#<%=txtHours.ClientID%>,#<%=txtBrkHour.ClientID%>, <%=txtMinutes.ClientID%>").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });--%>
        }

        function report(type) {

            $("#month_tr").hide();
            $("#custom_tr").hide();
            $("#year_tr").hide();

            if (type == "yearly") {
                $("#year_tr").show('slow');
            }
            else if (type == "monthly") {
                $("#month_tr").show('slow');
            }
            else if (type == "custom") {
                $("#custom_tr").show('slow');
            }
        }
        function validate(type) {
            if (type == "list") {
                if ($("#ddlUsers").val() != "-1") {
                    //$("#lnkGenerateStats").removeClass('hide');
                    //$("#lnkGenerateStats").css("pointer-events", "");
                    $("#lblError").hide();
                    return true;
                    $("#reportDiv").hide();

                }
                else {
                    //$("#lnkGenerateStats").css("pointer-events", "none");
                    $("#lblError").show();
                    return false;
                    //$("#lnkGenerateStats").addClass('hide');
                }

                $(".detail_stats").hide();
                $('.info-div').hide();

                return false;
            }

            if (type == "form") {

                $("#lblError").hide();
                $("#detail_stats").hide();

                //new
                if ($("#ddlUsers").val() != "-1") {
                    $("#lblError").hide();
                    $("#reportDiv").hide();
                    $("#detail_stats").hide();
                    $('.info-div').hide();
                    return true;
                }
                else {
                    $("#lblError").show();
                    return false;
                }
                //

                if ($("#rbtnReportType :checked").val() == "custom") {
                    if ($("#dateTo").val() == "" || $("#dateFrom").val() == "") {
                        $("#lblError").show();
                        return false;
                    }
                    else {
                        $("#lblError").hide();
                        return true;
                    }
                }
            }

        }

        function detailreport() {
            event.preventDefault();
            $(".info-div").hide();
            $.ajax({
                url: "Statistics.aspx/DetailReport",
                type: "POST",
                //data: "{'userid':'" + $('#hdnReportUserid').val() + "','startDate':'" + $('#hdnstartDate').val() + "','endDate':'" + $('#hdnendDate').val() + "','Appid':'" + $('#hdnAppId').val() + "','ParentId':'" + $('#hdnUserID').val() + "','dateTo':'" + $('#dateTo').val() + "','reportType':'" + $('#<%=rbtnReportType.ClientID %> input:checked').val() + "'}",
                data: "{'userid':'" + $('#hdnReportUserid').val() + "','startDate':'" + $('#hdnstartDate').val() + "','endDate':'" + $('#hdnendDate').val() + "','Appid':'" + $('#hdnAppId').val() + "','ParentId':'" + $('#hdnUserID').val() + "','dateTo':'" + $('#dateTo').val() + "','isworkdefine':'" + $('#hdn_isworkdefine').val() + "','instanceid':'" + $('#hdnInstanceId').val() + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                beforeSend: function () {
                    ActivateAlertDiv('', 'AlertDiv');
                },
                async: true,
                success: function (response) {
                    $("#detail_stats").html($.parseJSON(response.d));
                    $(".detail_heading").text("Detailed Report: " + $(".imgInfo").text().split(':')[1].trim());
                    $("#detail_stats").show();
                    $(".info-div").show();
                    $('html, body').animate({
                        scrollTop: $(".detail_stats").offset().top
                    }, 500);
                },
                complete: function () {
                    ActivateAlertDiv('none', 'AlertDiv');
                },
                error: function (response) {
                    //alert('Sorry an error has occurred. Please try again.');
                    openConfirmBox('Sorry an error has occurred. Please try again.');
                }
            });

        }

        function detailTagReport() {
            event.preventDefault();
            $(".info-div").hide();
            $.ajax({
                url: "Statistics.aspx/DetailReportTags",
                type: "POST",
                //data: "{'userid':'" + $('#hdnReportUserid').val() + "','startDate':'" + $('#hdnstartDate').val() + "','endDate':'" + $('#hdnendDate').val() + "','Appid':'" + $('#hdnAppId').val() + "','ParentId':'" + $('#hdnUserID').val() + "','dateTo':'" + $('#dateTo').val() + "','reportType':'" + $('#<%=rbtnReportType.ClientID %> input:checked').val() + "'}",
                data: "{'userid':'" + $('#hdnReportUserid').val() + "','startDate':'" + $('#hdnstartDate').val() + "','endDate':'" + $('#hdnendDate').val() + "','Appid':'" + $('#hdnAppId').val() + "','ParentId':'" + $('#hdnUserID').val() + "','dateTo':'" + $('#dateTo').val() + "','isworkdefine':'" + $('#hdn_isworkdefine').val() + "','instanceid':'" + $('#hdnInstanceId').val() + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                beforeSend: function () {
                    ActivateAlertDiv('', 'AlertDiv');
                },
                async: true,
                success: function (response) {
                    $("#detail_stats").html($.parseJSON(response.d));
                    $(".detail_heading").text("Detailed Report: " + $(".imgInfo").text().split(':')[1].trim());
                    $("#detail_stats").show();
                    $(".info-div").show();
                    $('html, body').animate({
                        scrollTop: $(".detail_stats").offset().top
                    }, 500);
                },
                complete: function () {
                    ActivateAlertDiv('none', 'AlertDiv');
                },
                error: function (response) {
                    //alert('Sorry an error has occurred. Please try again.');
                    openConfirmBox('Sorry an error has occurred. Please try again.');
                }
            });

        }


        function showhide(display) {
            event.preventDefault();
            if (display == "show") {
                $(".hourperday").addClass('hide');
                $(".addhourperday").removeClass('hide');
            }
            else if (display == "hide") {
                $(".addhourperday").addClass('hide');
                $(".hourperday").removeClass('hide');
                if ($("#txthourperday").val() != "") {
                    ($("#txtminperday").val() == "") ? $("#txtminperday").val("0") : $("#txtminperday").val();
                    $(".updatedhourperday").html($("#txthourperday").val() + " hours " + $("#txtminperday").val() + " minutes ");
                    $("[id$=ddlHours]").val($("#txthourperday").val());
                    $("[id$=ddlMinutes]").val($("#txtminperday").val());
                }
            }
        }

        function enable(action, id) {
            event.preventDefault();
            if (id == "start") {
                if (action == "yes") {
                    $('.chopstrtedit').addClass('hide');
                    $('.chopstrtcheck').removeClass('hide');
                    $("[id$=txtStartHour]").removeAttr("disabled");
                }
                else if (action == "no") {
                    $('.chopstrtcheck').addClass('hide');
                    $('.chopstrtedit').removeClass('hide');
                    $("[id$=txtStartHour]").attr("disabled", "disabled");
                }
            }

            if (id == "end") {
                if (action == "yes") {
                    $('.chopendedit').addClass('hide');
                    $('.chopendcheck').removeClass('hide');
                    $("[id$=txtEndHour]").removeAttr("disabled");

                }
                else if (action == "no") {
                    $('.chopendcheck').addClass('hide');
                    $('.chopendedit').removeClass('hide');
                    $("[id$=txtEndHour]").attr("disabled", "disabled");
                }
            }
        }

        function timeValidate() {
            $("#txthourperday,#txtminperday").on('keydown', function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });


            $("#txthourperday").keyup(function (event) {
                var number = parseFloat($(this).val());
                if (number > 12) {
                    $(this).val("");
                }
            });

            $("#txtminperday").keyup(function (event) {
                var number = parseFloat($(this).val());
                if (number > 60) {
                    $(this).val("");
                }
            });
        }

        function office_instance() {
            var user = '<%= Session["superadmin"] %>';

            if (user == 'no')
                $('.office').hide();
            else
                $('.office').show();
        }

        HTMLInputElement.prototype.startsWith = function () { return false; };

    </script>
    <style>
        .hide {
            display: none;
        }

        .form-control {
            font-size: 14px !important;
        }

        .borderless td {
            border: none;
            border-top: none;
        }

        .help-block {
            font-size: smaller;
            color: #929090;
        }

        #tbl_stats td {
            padding: 0px 10px !important;
        }

        .close-btn {
            margin: 10px 0;
            text-decoration: none;
            font-size: 15px;
            text-transform: capitalize;
        }

        .help-block {
            display: inline-block;
            margin-bottom: 10px;
        }

        #reportDiv {
            margin: 0px auto;
            padding: 15px 15px 0 15px;
        }

            #reportDiv b {
                margin: 3px 0;
                display: inline-block;
                padding-right: 10px;
            }

        .radio-2 input {
            display: inline-block;
            margin: 3px 0 0 5px;
            position: absolute;
        }

        .radio-2 label {
            display: inline-block;
            padding-left: 22px;
        }

        .btn-edit {
            font-size: 18px !important;
        }

        .info-div {
            /*border: 1px solid;*/
            /*top: 400px;
            position: absolute;
            right: 0;*/
            width: auto;
            margin: 0px 250px;
            text-align: -webkit-center;
        }
    </style>

    <asp:ScriptManager runat="server" ID="scriptmanager1" AsyncPostBackTimeout="50000"></asp:ScriptManager>
    <div>
        <div class="col-sm-12">
            <div class="settings-form">
                <asp:Label runat="server" ID="lbluser" Text="Select Employee: " Font-Bold="true"></asp:Label>
                <asp:DropDownList runat="server" ID="ddlUsers" onchange="validate('list')" CssClass="f-2" ValidationGroup="stats1" ClientIDMode="Static"></asp:DropDownList>
                <%--<asp:RequiredFieldValidator runat="server" ID="rqUsers" ValidationGroup="stats1" ControlToValidate="ddlUsers" SetFocusOnError="true" ErrorMessage="Please select an employee" Font-Bold="true" ForeColor="Red" InitialValue="-1"></asp:RequiredFieldValidator>--%>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="settings-form">
                <asp:Label runat="server" ID="Label2" Text="Select Report: " Font-Bold="true"></asp:Label>
                <asp:DropDownList runat="server" ID="ddlreports" CssClass="f-2" ClientIDMode="Static">
                    <asp:ListItem Value="0">Select Report</asp:ListItem>
                    <asp:ListItem Value="1">General Report</asp:ListItem>
                    <asp:ListItem Value="2">Tag Report</asp:ListItem>
                </asp:DropDownList>
                <%--<asp:RequiredFieldValidator runat="server" ID="rqUsers" ValidationGroup="stats1" ControlToValidate="ddlUsers" SetFocusOnError="true" ErrorMessage="Please select an employee" Font-Bold="true" ForeColor="Red" InitialValue="-1"></asp:RequiredFieldValidator>--%>
            </div>
        </div>
        <asp:Panel ID="ltgeneral" runat="server" ClientIDMode="Static" Style="display: none;">
            <div class="col-sm-12 general">
                <div class="settings-form">
                    <asp:Label runat="server" ID="lblType" Text="View " Font-Bold="true"></asp:Label>
                    <asp:RadioButtonList ID="rbtnReportFor" runat="server" ClientIDMode="Static">
                        <asp:ListItem Text=" Detailed Report" Value="fulltime" Selected="True"></asp:ListItem>
                        <asp:ListItem Text=" Total Worked Hours" Value="parttime"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="col-sm-12 tags">
                <div class="settings-form">
                    <asp:Label runat="server" ID="Label3" Text="View " Font-Bold="true"></asp:Label>
                    <asp:RadioButtonList ID="rbtntagReport" runat="server" ClientIDMode="Static">
                        <asp:ListItem Text="Report Tag Wise" Value="TagYearlyReport" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Report Day Wise" Value="TagDayWiseReport"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="settings-form">
                    <asp:Label runat="server" ID="lblReports" Text="Reports" Font-Bold="true"></asp:Label>
                    <asp:RadioButtonList ID="rbtnReportType" runat="server" ClientIDMode="Static">
                        <asp:ListItem Text="Yearly Report" Value="yearly" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Monthly Report" Value="monthly"></asp:ListItem>
                        <%--                                        <asp:ListItem Text="Custom Statistics" Value="custom"></asp:ListItem>--%>
                    </asp:RadioButtonList>
                </div>
            </div>

            <div class="col-sm-12" id="year_tr">
                <div class="settings-form">
                    <asp:Label runat="server" ID="lblYearly" Text="Please select year: " Font-Bold="true"></asp:Label>
                    <asp:DropDownList runat="server" ID="ddlYearly" CssClass="f-2">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="col-sm-12 hide" id="month_tr">
                <div class="settings-form">
                    <asp:Label runat="server" ID="lblMonthly" Text="Please select month and year: " Font-Bold="true"></asp:Label>
                    <asp:DropDownList runat="server" ID="ddlMonth" CssClass="f-2">
                        <asp:ListItem Value="1" Text="January"></asp:ListItem>
                        <asp:ListItem Value="2" Text="February"></asp:ListItem>
                        <asp:ListItem Value="3" Text="March"></asp:ListItem>
                        <asp:ListItem Value="4" Text="April"></asp:ListItem>
                        <asp:ListItem Value="5" Text="May"></asp:ListItem>
                        <asp:ListItem Value="6" Text="June"></asp:ListItem>
                        <asp:ListItem Value="7" Text="July"></asp:ListItem>
                        <asp:ListItem Value="8" Text="August"></asp:ListItem>
                        <asp:ListItem Value="9" Text="September"></asp:ListItem>
                        <asp:ListItem Value="10" Text="October"></asp:ListItem>
                        <asp:ListItem Value="11" Text="November"></asp:ListItem>
                        <asp:ListItem Value="12" Text="December"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList runat="server" ID="ddlYear" CssClass="f-2">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="col-sm-12" id="custom_tr" style="display: none;">
                <asp:Label runat="server" ID="Label1" Text="Select Date Range" Font-Bold="true"></asp:Label>
                <div class="settings-form">
                    <asp:Label runat="server" ID="lblDateFrom" Text="Date From:" Font-Bold="true"></asp:Label>
                    <asp:TextBox runat="server" ID="dateFrom" ClientIDMode="Static"></asp:TextBox>
                    &nbsp;&nbsp;
                <asp:Label runat="server" ID="lblDateTo" Text="Date To:" Font-Bold="true"></asp:Label>
                    <asp:TextBox runat="server" ID="dateTo" ClientIDMode="Static"></asp:TextBox>
                </div>
            </div>

            <asp:Label ID="lblError" runat="server" ClientIDMode="Static" Text="Please fill all the fields above" Font-Bold="true" ForeColor="Red" Style="display: none;"></asp:Label>
            <br />
            <asp:LinkButton data-type="view" runat="server" ID="lnkGenerateStats" OnClick="btnGenerateReportTemp_Click" ClientIDMode="Static" CssClass="close-btn b-21 btn btn-primary btn-sm" Text="View Report" OnClientClick="return validate('form');"></asp:LinkButton>


            <asp:UpdatePanel ID="reuslt_panel" runat="server">
                <ContentTemplate>
                    <div class="col-sm-12 DivTags" runat="server" id="DivTags">
                        <h5 ID="lbltagreport" class="imgInfo" runat="server"></h5>                       
                        <div id="divtagreportdetail" runat="server">
                        </div>
                    </div>
                    <asp:HiddenField runat="server" ID="hdnstartDate" ClientIDMode="Static" />
                    <asp:HiddenField runat="server" ID="hdnendDate" ClientIDMode="Static" />
                    <asp:HiddenField runat="server" ID="hdnReportUserid" ClientIDMode="Static" />
                    <div id="result_table" runat="server" visible="false">

                        <asp:UpdatePanel runat="server" ID="stats_panel">
                            <ContentTemplate>
                                <div id="result" runat="server">
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnGenerateReport" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <div style="display: -webkit-box;">
                            <div runat="server" id="advance_stats"></div>
                            <div class="col-lg-12 stats-form">
                                <%--                        <a href="#" id="lnkApplyFilter" class="btn-info b-21 btn" onclick="return ApplyFilter(this);">Advance Calculator</a>--%>

                                <div class="settings-form">
                                    <div class="filter hide">
                                        <asp:Label runat="server" Text="Working Hours/day"></asp:Label>
                                        <%--                                    <asp:TextBox runat="server" ID="txtHours" CssClass="f-2" ValidationGroup="stats" MaxLength="2" Text="8"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlHours" runat="server" ClientIDMode="Static">
                                        </asp:DropDownList>

                                        <span class="help-block" runat="server">Hours</span>
                                        <%--                                    <asp:TextBox runat="server" ID="txtMinutes" CssClass="f-2" ValidationGroup="stats" MaxLength="2" Text="0"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlMinutes" runat="server">
                                        </asp:DropDownList>
                                        <span class="help-block" runat="server">Minutes</span>
                                        <div class="clearfix"></div>
                                        <span class="help-block" runat="server">How many hours a day employee suppossed to work. Example: 8 hours 0 minutes.</span>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlHours" SetFocusOnError="true" ErrorMessage="Please enter working hours per day" Font-Bold="true" ForeColor="Red" ValidationGroup="stats"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="filter hide">
                                        <div>
                                            <asp:Label runat="server" ID="lblStartHour" Text="Chop hours before: "></asp:Label>
                                            <asp:TextBox runat="server" ID="txtStartHour" CssClass="f-2 check-time1" placeholder="09:00 AM" ClientIDMode="Static" Enabled="false"></asp:TextBox>
                                            <a class="chopstrtedit" href='#' onclick="enable('yes','start')"><i class='fa fa-fw fa-pencil'></i></a>
                                            <a class="chopstrtcheck hide" href='#' onclick="enable('no','start')"><i class='fa fa-fw fa-check'></i></a>
                                            <%--                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtStartHour" SetFocusOnError="true" ErrorMessage="Please enter Start Time" Font-Bold="true" ForeColor="Red" ValidationGroup="stats"></asp:RequiredFieldValidator>--%>
                                        </div>
                                        <%--                                    <div class="clearfix"></div>--%>
                                        <div>
                                            <asp:Label runat="server" ID="lblEndHour" Text="Chop hours after: "></asp:Label>
                                            <asp:TextBox runat="server" ID="txtEndHour" CssClass="f-2 check-time2" placeholder="06:00 PM" ClientIDMode="Static" Enabled="false"></asp:TextBox>
                                            <a class="chopendedit" href='#' onclick="enable('yes','end')"><i class='fa fa-fw fa-pencil'></i></a>
                                            <a class="chopendcheck hide" href='#' onclick="enable('no','end')"><i class='fa fa-fw fa-check'></i></a>
                                            <%--                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtEndHour" SetFocusOnError="true" ErrorMessage="Please enter End Time" Font-Bold="true" ForeColor="Red" ValidationGroup="stats"></asp:RequiredFieldValidator>--%>
                                        </div>
                                    </div>
                                    <div class="filter hide">
                                        <asp:Label runat="server" Text="Break /day"></asp:Label>
                                        <%--                                    <asp:TextBox runat="server" ID="txtBrkHour" placeholder="" CssClass="f-2" MaxLength="1" Text="1"></asp:TextBox>--%>
                                        <asp:DropDownList runat="server" ID="ddlBrkHour">
                                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                        </asp:DropDownList>
                                        <span class="help-block" runat="server">Hours</span>
                                        <asp:DropDownList runat="server" ID="ddlBrkMinutes">
                                        </asp:DropDownList>
                                        <span class="help-block" runat="server">Minutes</span>
                                        <div class="clearfix"></div>
                                        <%--                                    <span class="help-block" runat="server">Enter break time an employee took in a day. Example: 1 hour</span>--%>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="ddlBrkHour" SetFocusOnError="true" ErrorMessage="Please enter Break Time" Font-Bold="true" ForeColor="Red" ValidationGroup="stats"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="filter hide">
                                        <asp:Label runat="server" Text="Additional break per "></asp:Label>
                                        <asp:DropDownList runat="server" ID="ddlAdditionalBrk">
                                            <asp:ListItem Text="Month" Value="month"></asp:ListItem>
                                            <asp:ListItem Text="Week" Value="week"></asp:ListItem>
                                        </asp:DropDownList>
                                        &nbsp;&nbsp;
                                    <asp:DropDownList runat="server" ID="ddlAdditionalBrkHr">
                                        <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                    </asp:DropDownList>
                                        <span class="help-block" runat="server">Hours</span>
                                        <asp:DropDownList runat="server" ID="ddlAdditionalBrkMin">
                                        </asp:DropDownList>
                                        <span class="help-block" runat="server">Minutes</span>
                                        <span class="brkday">&nbsp;&nbsp;
                                    <asp:DropDownList runat="server" ID="ddlAdditionalBrkDay">
                                        <asp:ListItem Text="Monday" Value="Monday"></asp:ListItem>
                                        <asp:ListItem Text="Tuesday" Value="Tuesday"></asp:ListItem>
                                        <asp:ListItem Text="Wednesday" Value="Wednesday"></asp:ListItem>
                                        <asp:ListItem Text="Thursday" Value="Thursday"></asp:ListItem>
                                        <asp:ListItem Text="Friday" Value="Friday"></asp:ListItem>
                                    </asp:DropDownList>
                                            <span class="help-block" runat="server">Day</span>
                                        </span>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="filter hide">
                                        <asp:Label ID="lblStatsPage" runat="server" Text="To view/update holidays and weekend settings please click " Font-Bold="true"></asp:Label>
                                        <asp:LinkButton runat="server" ID="lnkStatsPage" Text="here"></asp:LinkButton>
                                    </div>
                                    <br />
                                    <div class="filter hide">
                                        <asp:HiddenField runat="server" ID="hdntype" />
                                        <asp:LinkButton runat="server" ID="btnGenerateReport" OnClick="btnGenerateReport_Click" Text="Calculate" CssClass="btn btn-primary b-21 btn-add" ValidationGroup="stats"></asp:LinkButton>
                                        <a href="#" id="lnkApplyFilter" class="btn-info b-21 btn" onclick="return ApplyFilter(this);" style="display: none;">Close</a>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                    <div runat="server" id="detail_stats" clientidmode="static" style="display: none;" class="detail_stats DivRemove"></div>
                    <asp:HiddenField runat="server" ID="TotalWorkingDays" ClientIDMode="Static" />
                    <asp:HiddenField runat="server" ID="WorkedDays" ClientIDMode="Static" />
                    <asp:HiddenField runat="server" ID="WorkedHours" ClientIDMode="Static" />
                    <asp:HiddenField runat="server" ID="hdn_isworkdefine" ClientIDMode="Static" />

                    <div id="InfoDiv" title="Admin Settings" runat="server" class="alert alert-info info-div hide DivRemove">
                        <div class="breadCrumb" style="text-align: center; font-size: 14px; font-weight: bold;">
                            Admin Settings
                        </div>
                        <div runat="server" id="InfoDetails">
                            Information!                           
                        </div>
                    </div>

                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnGenerateReport" />
                    <asp:AsyncPostBackTrigger EventName="Click" ControlID="lnkGenerateStats" />
                </Triggers>
            </asp:UpdatePanel>


        </asp:Panel>
    </div>

</asp:Content>

