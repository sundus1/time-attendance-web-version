﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AvaimaThirdpartyTool;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data;
using Newtonsoft.Json;

public partial class W_Absence : AvaimaWebPage
{
    Attendance atd = new Attendance();
    string Email = "";
    string Password = "";
    string InstanceId = "";
    bool isAdjustment = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        hdnParentID.Value = Request.QueryString["id"].ToString();
        hdnUserID.Value = Request.QueryString["uid"].ToString();
        Email = Request.QueryString["e"].Decrypt();
        Password = Request.QueryString["p"].Decrypt();
        InstanceId = Request.QueryString["instanceid"].ToString();
        isAdjustment = (Request.QueryString["isAdjustment"] != null) ? Convert.ToBoolean(Request.QueryString["isAdjustment"]) : isAdjustment;

        string IsVerified = atd.VerifyLogin(Email, Password);

        if (IsVerified == "FALSE")
            this.Redirect("Info.aspx");

        if (hdnParentID.Value == hdnUserID.Value)
        {
            if (!atd.UserManaged(hdnParentID.Value.ToInt32(), InstanceId, "get"))
            {
                this.Redirect("Info.aspx");
            }
        }

        if (!IsPostBack)
        {
            // Get User Name
            String userName = SP.GetWorkerName(hdnUserID.Value);
            lblUserName.Text = userName;

            LoadYear();
            LoadData();
        }
    }

    public void LoadYear()
    {
        string year = DateTime.Now.Year.ToString();

        ddlYear.Items.Add(new ListItem(year, year));


        for (int i = 1; i < 3; i++)
        {
            ddlYear.Items.Add(new ListItem(DateTime.Now.AddYears(-+i).Year.ToString(), DateTime.Now.AddYears(-+i).Year.ToString()));

        }
    }

    public void LoadData()
    {
        int year = Convert.ToInt32(ddlYear.SelectedValue);

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            List<Absence> absences = Absence.GetAbsences(Convert.ToInt32(hdnUserID.Value)).Where(u => u.Active == true && u.CrtDate.Year == year && u.isAdjustment == isAdjustment).ToList();

            if (absences.Count > 0)
            {
                hdmsg.Visible = false;
                rpthd.Visible = true;
                rpthd.DataSource = absences;
                rpthd.DataBind();
            }
            else
            {
                hdmsg.Visible = true;
                rpthd.Visible = false;
            }
        }
    }

    protected void rpthd_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField AbslogID = e.Item.FindControl("hdnID") as HiddenField;
            Label date = e.Item.FindControl("hdDate") as Label;
            Label comment = e.Item.FindControl("hdtitle") as Label;

            TextBox txtTitle = e.Item.FindControl("txtTitle") as TextBox;
            TextBox txtDate = e.Item.FindControl("txtDate") as TextBox;

            LinkButton btnEdit = e.Item.FindControl("edit") as LinkButton;
            LinkButton btnSave = e.Item.FindControl("save") as LinkButton;
            LinkButton btnCancel = e.Item.FindControl("cancel") as LinkButton;
            LinkButton btnDel = e.Item.FindControl("del") as LinkButton;

            if (e.CommandName == "del")
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("UPDATE [Absences]  SET [Active] = 0 where AbsLogID = @AbsLogID", con))
                    {
                        con.Open();
                        command.Parameters.AddWithValue("@AbsLogID", AbslogID.Value);
                        command.ExecuteNonQuery();

                        atd.SendAbsentNotification(hdnUserID.Value, Convert.ToDateTime(date.Text), comment.Text, "delete");
                    }
                }

                LoadData();
            }

            if (e.CommandName == "edit")
            {
                txtTitle.Visible = true;
                comment.Visible = false;

                txtDate.Visible = true;
                date.Visible = false;

                btnSave.Visible = true;
                btnCancel.Visible = true;
                btnEdit.Visible = false;
                btnDel.Visible = false;
            }
            if (e.CommandName == "cancel")
            {
                txtTitle.Visible = false;
                comment.Visible = true;

                txtDate.Visible = false;
                date.Visible = true;

                btnSave.Visible = false;
                btnCancel.Visible = false;
                btnEdit.Visible = true;
                btnDel.Visible = true;
            }

            if (e.CommandName == "save")
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("UPDATE [Absences] SET" +
              "[CrtDate] = @CrtDate,[ModDate] = @ModDate,[Comment] = @Comment WHERE AbsLogID = @AbsLogID", con))
                    {
                        con.Open();
                        command.Parameters.AddWithValue("@AbsLogID", AbslogID.Value);
                        command.Parameters.AddWithValue("@CrtDate", Convert.ToDateTime(txtDate.Text));
                        command.Parameters.AddWithValue("@ModDate", Convert.ToDateTime(txtDate.Text));
                        command.Parameters.AddWithValue("@Comment", txtTitle.Text);
                        command.ExecuteNonQuery();

                        atd.SendAbsentNotification(hdnUserID.Value, Convert.ToDateTime(txtDate.Text), txtTitle.Text, "update");
                    }
                }

                txtTitle.Visible = false;
                comment.Visible = true;

                txtDate.Visible = false;
                date.Visible = true;

                btnSave.Visible = false;
                btnCancel.Visible = false;
                btnEdit.Visible = true;
                btnDel.Visible = true;

                LoadData();
            }

        }
    }

    protected void rpthd_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label date = e.Item.FindControl("hdDate") as Label;
            date.Text = Convert.ToDateTime(date.Text).ToString("dd MMMM yyyy");

            TextBox txtDate = e.Item.FindControl("txtDate") as TextBox;
            txtDate.Text = date.Text;
        }
    }

    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadData();
    }
}