﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="AdminPanel.aspx.cs" Inherits="W_AdminPanel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>T&A Admin Panel</title>
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script>
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];;
        var date = new Date();
        var chartArray = [];
        var chartArray_week = [];
        var chartArray_activeusers = [];
        var barChartArray_activeusers = [];
        var chartArray_empcount = [];
        var text = '';
        var totalActiveUsers = 0;

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
              ['Task', 'Hours per Day'],
              ['Total admin sign ups: ' + document.getElementById("lbl_adminCount").innerHTML, parseInt(document.getElementById("lbl_adminCount").innerHTML)],
              ['Total employees added: ' + document.getElementById("lbl_empCount").innerHTML, parseInt(document.getElementById("lbl_empCount").innerHTML)],
              ['Total employees that signed up: ' + document.getElementById("lbl_empaccessCount").innerHTML, parseInt(document.getElementById("lbl_empaccessCount").innerHTML)],
              ['Total clock-ins today: ' + document.getElementById("lbl_clockinTodayCount").innerHTML, parseInt(document.getElementById("lbl_clockinTodayCount").innerHTML)],
            ]);

            var formatter = new google.visualization.NumberFormat(
                { negativeColor: 'red', negativeParens: true, pattern: '###,###' });
            formatter.format(data, 1);

            var options = {
                title: 'Time & Attendance Usage',
                is3D: true,
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }

        function drawChart_ActiveEmpAdmin() {

            var data = google.visualization.arrayToDataTable(barChartArray_activeusers);

            var options = {
                chart: {
                    title: 'Number of users in the last 3 months with minimum 20 clock-ins and have clocked-in once in the past 20 days.',
                    subtitle: 'Month-wise',
                },
                bars: 'horizontal' // Required for Material Bar Charts.
            };

            var chart = new google.charts.Bar(document.getElementById('clockinchart_activeAdminEmp'));
            chart.draw(data, google.charts.Bar.convertOptions(options));
            google.visualization.events.addListener(chart, 'onmouseover', barMouseOver);
            google.visualization.events.addListener(chart, 'onmouseout', barMouseOut);

        }

        function drawActiveUsersChart() {
            var data = google.visualization.arrayToDataTable(chartArray_activeusers);

            var options = {
                title: 'Number of users in the last 3 months with minimum 20 clock-ins and have clocked-in once in the past 20 days. \r\n \r\nTotal Active Users: ' + totalActiveUsers,
            };

            var chart = new google.visualization.PieChart(document.getElementById('clockinchart_activeusers'));
            chart.draw(data, options);
        }

        var barsVisualization;

        function drawMouseoverVisualization() {
            var data = google.visualization.arrayToDataTable([
               ['Month', 'Total Sign up', { role: 'style' }, { role: 'annotation' }],
               [months[date.getMonth() - 1], parseFloat(document.getElementById("lbl_last").innerHTML), 'green', document.getElementById("lbl_last").innerHTML],
               [months[date.getMonth()], parseFloat(document.getElementById("lbl_this").innerHTML), 'green', document.getElementById("lbl_this").innerHTML]
            ]);

            var options = {
                title: 'Total signups of admins and employees'
            };

            barsVisualization = new google.visualization.ColumnChart(document.getElementById('mouseoverdiv'));
            barsVisualization.draw(data, options);

            // Add our over/out handlers.
            google.visualization.events.addListener(barsVisualization, 'onmouseover', barMouseOver);
            google.visualization.events.addListener(barsVisualization, 'onmouseout', barMouseOut);
        }

        function ClockinsDataArray(year, data, type) {
            if (type == 1) {
                text = year;
                chartArray.push(['Month', 'Total Sign up', { role: 'style' }, { role: 'annotation' }]);
                var arr = JSON.parse(data);
                arr.forEach(function (el, index) {
                    chartArray.push([arr[index]["Day"], parseFloat(arr[index]["Signups"]), 'red', arr[index]["Signups"]]);
                });
            }
            else if (type == 2) {
                chartArray_week.push(['Day', 'No. of Users', { role: 'annotation' }]);
                var arr = JSON.parse(data);
                arr.forEach(function (el, index) {
                    chartArray_week.push([arr[index]["Day"], parseFloat(arr[index]["Clockins"]), arr[index]["Clockins"]]);
                });
            }
            else if (type == 3) {
                chartArray_activeusers.push(['User', 'Total', { role: 'annotation' }]);
                var arr = JSON.parse(data);
                arr.forEach(function (el, index) {
                    chartArray_activeusers.push([arr[index]["User"] + ': ' + arr[index]["Total"], parseFloat(arr[index]["Total"]), arr[index]["Total"]]);
                    totalActiveUsers += parseInt(arr[index]["Total"]);
                });
            }
            else if (type == 4) {
                barChartArray_activeusers.push(['Month', 'Total Admins', 'Total Employees']);
                var arr = JSON.parse(data);
                arr.forEach(function (el, index) {
                    barChartArray_activeusers.push([arr[index]["Month"], parseFloat(arr[index]["adminCount"]), parseFloat(arr[index]["employeeCount"])]);
                });
            }
            else if (type == 5) {
                chartArray_empcount.push(["# of Employee", "Count", { role: "style" }]);
                var arr = JSON.parse(data);
                arr.forEach(function (el, index) {
                    chartArray_empcount.push([arr[index]["label"], parseFloat(arr[index]["total"]), "gold"]);
                });
                console.log(chartArray_empcount);

            }
        }

        function drawClockinChart() {
            var data = google.visualization.arrayToDataTable(chartArray);

            var options = {
                title: 'Total signups of admins and employees in past 8 months'
            };

            barsVisualization = new google.visualization.ColumnChart(document.getElementById('signupchart'));
            barsVisualization.draw(data, options);

            // Add our over/out handlers.
            google.visualization.events.addListener(barsVisualization, 'onmouseover', barMouseOver);
            google.visualization.events.addListener(barsVisualization, 'onmouseout', barMouseOut);
        }

        function drawClockinChart_week() {
            var data = google.visualization.arrayToDataTable(chartArray_week);

            var options = {
                title: ' Number of people (distinct) who clocked in a given day (multiple clock-ins are ignored)',
                bar: { groupWidth: "80%" }
            };

            barsVisualization = new google.visualization.BarChart(document.getElementById('clockinchart_week'));
            barsVisualization.draw(data, options);

            // Add our over/out handlers.
            google.visualization.events.addListener(barsVisualization, 'onmouseover', barMouseOver);
            google.visualization.events.addListener(barsVisualization, 'onmouseout', barMouseOut);
        }

        function drawChart() {
            var data = google.visualization.arrayToDataTable(chartArray_empcount);

            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
                             {
                                 calc: "stringify",
                                 sourceColumn: 1,
                                 type: "string",
                                 role: "annotation"
                             },
                             2]);

            var options = {
                title: "User's employee count",
                width: 1200,
                height: 500,
                //bar: { groupWidth: "95%" },
                legend: { position: "none" },
            };
            var chart = new google.visualization.BarChart(document.getElementById("chart_EmpCount"));
            chart.draw(view, options);
        }

        function barMouseOver(e) {
            barsVisualization.setSelection([e]);
        }

        function barMouseOut(e) {
            barsVisualization.setSelection([{ 'row': null, 'column': null }]);
        }


    </script>
</head>
<body>
    <form id="form1" runat="server">
        <a href="http://www.avaima.com/6e815b00-6e13-4839-a57d-800a92809f21/W/Default.aspx?id=9844&instanceid=f425f912-8d79-4415-ba72-89a5309acb10&e=cwB1AHAAcABvAHIAdABAAGEAdgBhAGkAYgAuAGMAbwBtAA==&p=YQBzAGQAcwBhAGQAYQBhAGkAbgB0AA==" target="_blank"><i class="fa fa-fw fa-home"></i>Back to Home</a>
        <br />

        <%-- <div style="display: inline-flex;">
            <div id="piechart" style="width: 700px; height: 500px;"></div>
            <div id="clockinchart_activeusers" style="width: 700px; height: 500px; align-content: center;"></div>
        </div>--%>

        <br />
        <div id="clockinchart_activeusers" style="width: 700px; height: 500px; align-content: center;"></div>
        <br />

        <div id="clockinchart_activeAdminEmp" style="width: 1200px; height: 800px; align-content: center;"></div>
        <br />
        <div id="clockinchart_week" style="width: 1200px; height: 800px; align-content: center;"></div>
        <br />
        <div id="signupchart" style="width: 1200px; height: 500px;"></div>
        <br />
        <div id="chart_EmpCount" style="width: 1200px; height: 500px;"></div>
        <div style="display: none;">
            <label># of SignUps</label>
            <br />
            <div style="display: inline-flex;">
                <div>
                    <asp:Label runat="server" ID="Label1" Text=" Total admin sign ups: " Font-Bold="true"></asp:Label>
                    <asp:Label runat="server" ID="lbl_adminCount" ClientIDMode="Static"></asp:Label>
                </div>
                &nbsp; &nbsp; &nbsp;
                <div>
                    <asp:Label runat="server" ID="Label2" Text=" Total employees added: " Font-Bold="true"></asp:Label>
                    <asp:Label runat="server" ID="lbl_empCount" ClientIDMode="Static"></asp:Label>
                </div>
                &nbsp; &nbsp; &nbsp;
                <div>
                    <asp:Label runat="server" ID="Label3" Text=" Total employees that accessed their account: " Font-Bold="true"></asp:Label>
                    <asp:Label runat="server" ID="lbl_empaccessCount" ClientIDMode="Static"></asp:Label>
                </div>
            </div>
            <br />
            <br />
            <label># of Clock-ins</label>
            <br />
            <div style="display: inline-flex;">

                <div>
                    <asp:Label runat="server" ID="Label4" Text=" Total clock-ins today: " Font-Bold="true"></asp:Label>
                    <asp:Label runat="server" ID="lbl_clockinTodayCount" ClientIDMode="Static"></asp:Label>
                </div>
                &nbsp; &nbsp; &nbsp;
                <div>
                    <asp:Label runat="server" ID="Label5" Text=" Total clock-ins this month: " Font-Bold="true"></asp:Label>
                    <asp:Label runat="server" ID="lbl_clockinCount" ClientIDMode="Static"></asp:Label>
                </div>
            </div>

            <asp:Label runat="server" ID="lbl_this" ClientIDMode="Static"></asp:Label>
            <asp:Label runat="server" ID="lbl_last" ClientIDMode="Static"></asp:Label>

        </div>
    </form>
</body>
</html>


