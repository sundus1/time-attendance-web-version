﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AvaimaThirdpartyTool;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Text;
using MyAttSys;
using Newtonsoft.Json;
using AuthModel;
using System.Web.Services;

public partial class _Default : AvaimaWebPage
{
    private Dictionary<string, string> _breadcrumdict = new Dictionary<string, string>();
    private bool IsSimpleClock = true;
    private bool IsAdmin = false;
    private string OwnerId = "";
    private int appAssignedId = 0;
    private Boolean isNew = false;
    public static string AppId = "";

    Attendance atd = new Attendance();
    AvaimaTimeZoneAPI atp = new AvaimaTimeZoneAPI();

    public String _InstanceID { get; set; }

    private void UpdateFirstTime(int userId, string instanceId)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_UpdateUserSettings", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@InstanceId", instanceId);
                cmd.Parameters.AddWithValue("@popup", false);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }
    }
    private bool UserFirstTime(int userId, string instanceId)
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_GetUserSettings", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@InstanceId", instanceId);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }
        }
        if (dt == null)
        { return true; }
        else
        {
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["popup"] == null || dt.Rows[0]["popup"].ToString() == "")
                {
                    return true;
                }
                else
                {
                    return Convert.ToBoolean(dt.Rows[0]["popup"].ToString());//False==show screen
                }
            }
            else
            {
                return true;
            }
        }
    }

    public void UpdateTimeZone(string userId)
    {
        string parentUserID = atd.getParent(userId);
        DataTable dt = atd.GetTimezone(parentUserID);

        atd.SetTimezone(atd.Timezone_datetime(userId, DateTime.Now.ToString(), ""), dt.Rows[0]["timezone"].ToString(), dt.Rows[0]["dlsaving"].ToInt32(), dt.Rows[0]["dlsavinghour"].ToInt32());
    }

    private bool UserFirstTime(int userId)
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("Select * from attendence_management where ID = @UserId", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@UserId", userId);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }
        }
        if (dt == null)
        { return true; }
        else
        {
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["Accessed"] == null || dt.Rows[0]["Accessed"].ToString() == "0")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // ScriptManager.RegisterStartupScript(this, GetType(), "", "openHowToAddEmployee();", true);
        //hdnCITime.Value = App.GetTime(DateTime.Now.TimeOfDay.ToString(), HttpContext.Current.User.Identity.Name);
        hdnCITime.Value = DateTime.Now.TimeOfDay.ToString();

        if (_InstanceID == "0") { _InstanceID = App.InstanceID; }
        else { _InstanceID = this.InstanceID; }

        lblDate.Text = DateTime.Now.ToString();

        AddinstanceWS objinst = new AddinstanceWS();
        OwnerId = App.GetAVAIMAUserID(App.Roles.SUPER_ADMIN);
        lbl.Text = OwnerId;

        //List<WorkingHour> whList = WorkingHour.GetWorkingHours(0);
        //whList = whList.Where(u => u.InstanceID == this.InstanceID).ToList();
        //if (whList.Count <= 0)
        //{
        //    this.Redirect("Welcome.aspx?InstanceID=" + InstanceID);
        //    ScriptManager.RegisterStartupScript(this, GetType(), "Opentheworkingdiv", "Workinghour();", true);
        //}


        //workeruserprofile.InstanceID = _InstanceID;
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("getuserrole", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", OwnerId.Trim());
                cmd.Parameters.AddWithValue("@instanceid", _InstanceID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    hdnAdminName.Value = SP.GetWorkerName(dt.Rows[0][1].ToString());
                    hdnAdminEmail.Value = SP.GetEmailByUserID(Convert.ToInt32(dt.Rows[0][1].ToString()));

                    if (Convert.ToString(dt.Rows[0]["role"]) == "worker" || Convert.ToString(dt.Rows[0]["role"]) == "admin")
                    {
                        if (Convert.ToString(dt.Rows[0]["role"]) == "admin")
                        {

                            IsAdmin = true;
                            appAssignedId = Convert.ToInt32(dt.Rows[0]["userid"]);
                            hfparentid.Value = SP.GetAdminCatID(Convert.ToInt32(dt.Rows[0]["userid"].ToString())).ToString();
                            if (UserFirstTime(appAssignedId, _InstanceID) == true)
                            {
                                //if (AllowSplashScreen(appAssignedId, _InstanceID) == false)
                                //{
                                //    Response.Redirect("StartUp.aspx?UserId=" + appAssignedId + "&InstanceId=" + _InstanceID);
                                //}
                                //ScriptManager.RegisterStartupScript(this, GetType(), "", "showFirstTime();", true);
                                UpdateFirstTime(appAssignedId, _InstanceID);

                            }
                        }
                        //else
                        //{
                        //    attendancetype objattendancetype = attendancetype.SingleOrDefault(u => u.userid == dt.Rows[0]["userid"] && u.instanceid == this.InstanceID);
                        //    if (objattendancetype != null)
                        //    {
                        //        if (objattendancetype.userattendType == 2)
                        //        {
                        //            this.Redirect("Flexibleworker.aspx?id=" + dt.Rows[0]["userid"]);
                        //        }
                        //        else
                        //        {
                        //            this.Redirect("Add_worker.aspx?id=" + Request.QueryString["id"].ToString() + "&uid=" + dt.Rows[0]["userid"] + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + hdnUserEmail.Value.Encrypt() + "&p=" + hdnUserPassword.Value.Encrypt());
                        //        }
                        //    }
                        //    else
                        //    {
                        //        this.Redirect("Add_worker.aspx?id=" + Request.QueryString["id"].ToString() + "&uid=" + dt.Rows[0]["userid"] + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + hdnUserEmail.Value.Encrypt() + "&p=" + hdnUserPassword.Value.Encrypt());
                        //    }
                        //}
                    }
                    else
                    {
                        //IsAdmin = true;
                        appAssignedId = Convert.ToInt32(dt.Rows[0]["userid"]);
                        if (UserFirstTime(appAssignedId, _InstanceID) == true)
                        {

                            //    if (AllowSplashScreen(appAssignedId, _InstanceID) == false)
                            //    {
                            //ScriptManager.RegisterStartupScript(this, GetType(), "", "showFirstTime();", true);
                            UpdateFirstTime(appAssignedId, _InstanceID);
                            //        Response.Redirect("StartUp.aspx?UserId=" + appAssignedId + "&InstanceId=" + _InstanceID);
                            //    }
                        }
                    }
                }
                // Intance Dependent Code - UnCommented
                else
                {
                    AvaimaUserProfile objwebser = new AvaimaUserProfile();
                    //string[] user = objwebser.Getonlineuserinfo(this.Context.User.Identity.Name).Split('#');
                    string[] user = objwebser.Getonlineuserinfo(OwnerId).Split('#');
                    using (SqlCommand cmd1 = new SqlCommand("insertworker", con))
                    {
                        string title = user[0] + " " + user[1];
                        cmd1.Parameters.AddWithValue("@title", title);
                        cmd1.Parameters.AddWithValue("@parentid", 0);
                        cmd1.Parameters.AddWithValue("@InstanceId", _InstanceID);
                        cmd1.Parameters.AddWithValue("@datefield", Convert.ToDateTime(DateTime.Now));
                        cmd1.Parameters.AddWithValue("@category", 0);
                        cmd1.Parameters.AddWithValue("@email", user[2]);
                        cmd1.Parameters.AddWithValue("@active", true);

                        cmd1.CommandType = CommandType.StoredProcedure;
                        string id = cmd1.ExecuteScalar().ToString();
                        Insertuserrole(OwnerId, "superuser", user[2], id);
                        try
                        {
                            objwebser.insertUserFull(OwnerId, user[3], user[2], "", "", "", "", "", "", user[0], user[1], "", "", "", "", "", "", "", "", id, testInstanceID, "");
                        }
                        catch (Exception ex)
                        {

                        }
                        isNew = true;
                    }
                }
                ////end

            }
            //if (!IsPostBack)
            //{
            //    try
            //    {
            //        if (!String.IsNullOrEmpty(Request.QueryString["allowed"].ToString()))
            //        {
            //            this.Redirect("Info.aspx");
            //            //  ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('You are not allowed to view this page');", true);
            //        }
            //    }
            //    catch (Exception ex)
            //    { }

            //}
            //ScriptManager.RegisterStartupScript(this, GetType(), "", "showFirstTime();", true);
        }


        //Check Daylight Savings
        //string AutoDetectedtimezone = atd.getStandardTime(Request.UserHostAddress);
        //if (!string.IsNullOrEmpty(AutoDetectedtimezone))
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "dst", "checkUserStandartTime('" + AutoDetectedtimezone + "'); ", true);
        //}
        //else
        //    ScriptManager.RegisterStartupScript(this, GetType(), "dst", "console.log(NO OFFSET FOUND);", true);
        ////End


        pg_Control.OnPageIndex_Changed += pg_rptrcontract_OnPageIndex_Changed;
        if (!IsPostBack)
        {
            hdnUserID.Value = Request.QueryString["id"].ToString();
            hfparentid.Value = Request.QueryString["id"].ToString();
            hdnUserEmail.Value = Request.QueryString["e"].Decrypt();
            hdnUserPassword.Value = Request.QueryString["p"].Decrypt();

            string isVerified = atd.VerifyLogin(hdnUserEmail.Value, hdnUserPassword.Value);
            if (isVerified != "TRUE")
                this.Redirect("Info.aspx");

            if (CheckEmployees(hdnUserID.Value, Request.QueryString["instanceid"].ToString(), true) == 0 && (!atd.isAdmin(Request.QueryString["id"].ToInt32())))
            {
                this.Redirect("Add_worker.aspx?id=" + hdnUserID.Value + "&uid=" + hdnUserID.Value + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString());
            }

            LoadUserData();
          //  timerForRepeater_Tick(sender, e);
            FillAttendance();
            ScriptManager.RegisterStartupScript(this, GetType(), "cache1", "clearCache();", true);

        }
        //if (_breadcrumdict.Count == 0)
        //{
        //    root.Visible = false;
        //}
        //else
        //{
        //    root.Visible = true;
        //}

        //if (Request.Form["__EVENTTARGET"] == "head_Rptrattendence")        {

        //   // RptrattendenceItemcommand(sender);        
        //}

    }

    private void LoadUserData()
    {
        DataTable userSettings = JsonConvert.DeserializeObject<DataTable>(atd.GetSettingInfo(hdnUserEmail.Value, hdnUserPassword.Value));
        AppId = userSettings.Rows[0]["userid"].ToString();

        DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.GetStatus(hdnUserID.Value));
        string response = "";

        //if (dt.Rows.Count > 0)
        //{
        //    response = dt.Rows[0]["lastout"].ToString();
        //    if (dt.Rows[0]["lastout"].ToString() == "")
        //    {
        //        string time = Convert.ToDateTime(dt.Rows[0]["lastin"]).ToShortTimeString();
        //        lblLastinTime.Text = "Last Clock-in " + time + " on " + Convert.ToDateTime(dt.Rows[0]["lastin"]).DayOfWeek + ", " + Convert.ToDateTime(dt.Rows[0]["lastin"]).ToString("MMMM dd,yyyy");
        //        //lblTimer.Text = dt.Rows[0]["hours"].ToString();
        //        //lblTimer.Visible = true;

        //        btnSignin.Attributes.Add("class", "btnSignInDisable");
        //        btnSignin.Attributes.Add("Disabled", "Disabled");
        //        btnSignout.Attributes.Add("class", "btnSignOutEnable");
        //        btnSignout.Attributes.Remove("Disabled");
        //    }
        //    else
        //    {
        //        string time = Convert.ToDateTime(dt.Rows[0]["lastout"]).ToShortTimeString();
        //        lblLastinTime.Text = "Last Clock-out " + time + " on " +
        //            Convert.ToDateTime(dt.Rows[0]["lastout"]).DayOfWeek + ", " +
        //            Convert.ToDateTime(dt.Rows[0]["lastout"]).ToString("MMMM dd,yyyy");
        //        //lblTimer.Text = "00:00";
        //        //lblTimer.Visible = false;

        //        btnSignin.Attributes.Add("class", "btnSignInEnable");
        //        btnSignin.Attributes.Remove("Disabled");
        //        btnSignout.Attributes.Add("class", "btnSignOutDisable");
        //        btnSignout.Attributes.Add("Disabled", "Disabled");

        //    }
        //}
        //else
        //{
        //    lblLastinTime.Text = "Last Clock-in NEVER";
        //    //lblTimer.Text = "00:00";
        //    //lblTimer.Visible = false;
        //    btnSignin.Attributes.Add("class", "btnSignInEnable");
        //    btnSignin.Attributes.Remove("Disabled");
        //    btnSignout.Attributes.Add("class", "btnSignOutDisable");
        //    btnSignout.Attributes.Add("Disabled", "Disabled");
        //}

        requestlink.HRef = atd.WebURL("adduser", hdnUserID.Value, InstanceID, hdnUserEmail.Value, hdnUserPassword.Value) + "&req=yes";

        DataTable req = JsonConvert.DeserializeObject<DataTable>(atd.GetAdminRightsRequests(Convert.ToInt32(hdnUserID.Value)));
        if (req.Rows.Count > 0)
            tr_req.Visible = true;
        else
            tr_req.Visible = false;

    }
    private Boolean ValidateDayText(TextBox txtCI, TextBox txtCO)
    {
        if (String.IsNullOrEmpty(txtCI.Text) || String.IsNullOrEmpty(txtCO.Text))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    protected void btnSaveWH_Click(object sender, EventArgs e)
    {
        //AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
        //WorkingHour wh = new WorkingHour();
        //if (ValidateDayText(txtMonCI, txtMonCO))
        //{
        //    wh = new WorkingHour();
        //    wh.WorkHourID = Convert.ToInt32(hdnMonID.Value);
        //    wh.Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtMonCI.Text, this.InstanceID));
        //    wh.Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtMonCO.Text, this.InstanceID));
        //    wh.Active = chkMon.Checked;
        //    wh.AutoPresent = chkAutoPresent.Checked;
        //    wh.crtDate = DateTime.Now;
        //    wh.DayTitle = "Monday";
        //    wh.UserID = 0;
        //    wh.InstanceID = this.InstanceID;
        //    wh.Save();
        //}

        //if (ValidateDayText(txtTuesCI, txtTuesCO))
        //{
        //    wh = new WorkingHour()
        //    {
        //        WorkHourID = Convert.ToInt32(hdnTueID.Value),
        //        Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtTuesCI.Text, InstanceID)),
        //        Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtTuesCO.Text, InstanceID)),
        //        Active = chkTue.Checked,
        //        AutoPresent = chkAutoPresent.Checked,
        //        crtDate = DateTime.Now,
        //        DayTitle = "Tuesday",
        //        InstanceID = this.InstanceID,
        //        UserID = 0
        //    };
        //    wh.Save();
        //}

        //if (ValidateDayText(txtWedCI, txtWedCO))
        //{
        //    wh = new WorkingHour()
        //    {
        //        WorkHourID = Convert.ToInt32(hdnWedID.Value),
        //        Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtWedCI.Text, InstanceID)),
        //        Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtWedCO.Text, InstanceID)),
        //        Active = chkWed.Checked,
        //        AutoPresent = chkAutoPresent.Checked,
        //        crtDate = DateTime.Now,
        //        DayTitle = "Wednesday",
        //        InstanceID = this.InstanceID,
        //        UserID = 0

        //    };
        //    wh.Save();
        //}

        //if (ValidateDayText(txtThuCI, txtThuCO))
        //{
        //    wh = new WorkingHour()
        //    {
        //        WorkHourID = Convert.ToInt32(hdnThuID.Value),
        //        Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtThuCI.Text, InstanceID)),
        //        Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtThuCO.Text, InstanceID)),
        //        Active = chkThur.Checked,
        //        AutoPresent = chkAutoPresent.Checked,
        //        crtDate = DateTime.Now,
        //        DayTitle = "Thursday",
        //        InstanceID = this.InstanceID,
        //        UserID = 0
        //    };
        //    wh.Save();
        //}

        //if (ValidateDayText(txtFriCI, txtFriCO))
        //{
        //    wh = new WorkingHour()
        //    {
        //        WorkHourID = Convert.ToInt32(hdnFriID.Value),
        //        Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtFriCI.Text, InstanceID)),
        //        Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtFriCO.Text, InstanceID)),
        //        Active = chkFri.Checked,
        //        AutoPresent = chkAutoPresent.Checked,
        //        crtDate = DateTime.Now,
        //        DayTitle = "Friday",
        //        InstanceID = this.InstanceID,
        //        UserID = 0
        //    };
        //    wh.Save();
        //}

        //if (ValidateDayText(txtSatCI, txtSatCO))
        //{
        //    wh = new WorkingHour()
        //    {
        //        WorkHourID = Convert.ToInt32(hdnSatID.Value),
        //        Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtSatCI.Text, InstanceID)),
        //        Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtSatCO.Text, InstanceID)),
        //        Active = chkSat.Checked,
        //        AutoPresent = chkAutoPresent.Checked,
        //        crtDate = DateTime.Now,
        //        DayTitle = "Saturday",
        //        InstanceID = this.InstanceID,
        //        UserID = 0
        //    };
        //    wh.Save();

        //}

        //if (ValidateDayText(txtSunCI, txtSunCO))
        //{
        //    wh = new WorkingHour()
        //    {
        //        WorkHourID = Convert.ToInt32(hdnSunID.Value),
        //        Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtSunCI.Text, InstanceID)),
        //        Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtSunCO.Text, InstanceID)),
        //        Active = chkSun.Checked,
        //        AutoPresent = chkAutoPresent.Checked,
        //        crtDate = DateTime.Now,
        //        DayTitle = "Sunday",
        //        InstanceID = this.InstanceID,
        //        UserID = 0
        //    };
        //    wh.Save();
        //}
        //if (chkAutoPresent.Checked)
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "", "StartAutoPresent();", true);
        //}
        //SetWorkingHours();
    }
    private void FillAttendance()
    {
        //RptrAttendanceFill(0, IsAdmin);
        //hfparentid.value = "0";
        RptrAttendanceFill(Convert.ToInt32(hfparentid.Value));

        TreeNode parentNode = new TreeNode(String.Format("<input type='radio' name='rbtncat' value='Root' onclick='setvalue(0)'/>&nbsp; Root"));
        parentNode.SelectAction = TreeNodeSelectAction.Expand;
        FillTreeView(parentNode.ChildNodes, "0");
        pg_Control.CurrentPage = 0;
    }

    protected void RptrAttendanceFill(int pid, bool IsActive = true, bool isAdmin = false, bool ShowFiltered = false, int status = 1)
    {
        DataTable dtstudentall = new DataTable();
        string q = "";

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            if (IsActive)
                q = "GetUsers";      //Accessible user's list with today work status
            else
                q = "sp_DGetAllAttendenceRecord1";      //List of Inactive Users


            using (SqlCommand cmd = new SqlCommand(q, con))
            {
                con.Open();

                if (IsActive)
                {
                    cmd.Parameters.AddWithValue("@userid", hdnUserID.Value);
                    cmd.Parameters.AddWithValue("@instanceid", _InstanceID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@id", 0);
                    cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                    cmd.Parameters.AddWithValue("@IsActive", 0);
                    cmd.Parameters.AddWithValue("@userid", hdnUserID.Value);
                }

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 90;

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dtstudentall);

                DataTable Unmarked = (dtstudentall.Select("status=''", "lastactivity DESC").Length > 0) ? dtstudentall.Select("status=''", "lastactivity DESC").CopyToDataTable() : null;
                DataTable Error = (dtstudentall.Select("status='Error'", "lastactivity DESC").Length > 0) ? dtstudentall.Select("status='Error'", "lastactivity DESC").CopyToDataTable() : null;
                DataTable Clockedout = (dtstudentall.Select("status='Clocked out'", "lastactivity DESC").Length > 0) ? dtstudentall.Select("status='Clocked out'", "lastactivity DESC").CopyToDataTable() : null;
                DataTable Absent = (dtstudentall.Select("status='Absent'", "AbsLogID DESC").Length > 0) ? dtstudentall.Select("status='Absent'", "AbsLogID DESC").CopyToDataTable() : null;
                DataTable Present = (dtstudentall.Select("status='Present'", "lastactivity DESC").Length > 0) ? dtstudentall.Select("status='Present'", "lastactivity DESC").CopyToDataTable() : null;
                //One who is marked as Absent but also clocked-in
                DataTable Absent_Present = (dtstudentall.Select("status='Absent-Present'", "lastactivity DESC").Length > 0) ? dtstudentall.Select("status='Absent-Present'", "lastactivity DESC").CopyToDataTable() : null;


                dtstudentall.Rows.Clear();
                if (Unmarked != null) { dtstudentall.Merge(Unmarked); lnkTotalUnmarked.Text = Unmarked.Rows.Count.ToString(); }
                if (Error != null) { dtstudentall.Merge(Error); /*lnkTotalError.Text = Error.Rows.Count.ToString();*/ }
                if (Clockedout != null) { dtstudentall.Merge(Clockedout); /*lnkTotalClockedout.Text = Clockedout.Rows.Count.ToString();*/ }
                if (Absent != null) { dtstudentall.Merge(Absent); lnkTotalAbsent.Text = Absent.Rows.Count.ToString(); }
                if (Absent_Present != null) { dtstudentall.Merge(Absent_Present); lnkTotalAbsent.Text = (lnkTotalAbsent.Text.ToInt32() + Absent_Present.Rows.Count).ToString(); }
                if (Present != null) { dtstudentall.Merge(Present); lnkTotalPresent.Text = Present.Rows.Count.ToString(); }
                lnkTotal.Text = dtstudentall.Select().Count().ToString();

                PagedDataSource pds = new PagedDataSource();
                pds.AllowPaging = true;
                pds.DataSource = dtstudentall.DefaultView;
                pds.CurrentPageIndex = pg_Control.CurrentPage;
                pds.PageSize = pg_Control.PageSize;
                pg_Control.TotalRecords = dtstudentall.Select().Count();
                if (pg_Control.TotalRecords >= 30)
                {
                    pg_Control.Visible = true;
                }
                else
                {
                    pg_Control.Visible = false;
                }

                if (dtstudentall.Rows.Count > 0)
                {
                    Rptrattendence.DataSource = pds;
                    autopresences = AutoPresent.GetAll();
                    Rptrattendence.DataBind();

                    main.Visible = true;
                    if (isNew)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "showFirstTime();", true);
                    }
                    Rptrattendence.Visible = true;
                    pg_Control.LoadStatus();

                    //int manage_check = 0;
                    //string check = "True";
                    //foreach (DataRow dr in dtstudentall.Rows)
                    //{
                    //    if (check != dr["isManaged"].ToString())
                    //    {                           
                    //        check = dr["isManaged"].ToString();
                    //        manage_check++;
                    //    }
                    //}

                    //if (manage_check > 0)
                    //{
                    //    var ids = (from r in Rptrattendence.Items.Cast<RepeaterItem>()
                    //               let selected = (r.FindControl("chkselecteditem") as CheckBox).Checked
                    //               let id = (r.FindControl("hf_ID") as HiddenField).Value
                    //               where selected
                    //               select id).ToList();

                    //    HtmlTableCell cell1 = Rptrattendence.Items.FindControl("tdh_manage") as HtmlTableCell;
                    //    cell1.Visible = false;
                    //    HtmlTableCell cell2 = Rptrattendence.Items.FindControl("tdr_manage") as HtmlTableCell;
                    //    cell2.Visible = false;
                    //}
                }
                else
                {
                    Rptrattendence.DataSource = pds;
                    autopresences = AutoPresent.GetAll();
                    Rptrattendence.DataBind();

                    //if (ShowFiltered)
                    //{
                    //    trmainbc.Visible = trmainfunction.Visible = main.Visible = true;
                    //    if (isNew)
                    //    {
                    //        ScriptManager.RegisterStartupScript(this, GetType(), "", "showFirstTime();", true);
                    //    }
                    //    lblemptygrid.Visible = true;
                    //}
                    //else
                    //{
                    //    lblemptygrid.Visible = false;

                    //    if (isNew)
                    //    {
                    //        ScriptManager.RegisterStartupScript(this, GetType(), "", "showFirstTime();", true);
                    //    }
                    //}

                    Rptrattendence.Visible = false;
                    pg_Control.LoadStatus();
                }
            }
        }
    }


    private bool UserExists(string email)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("select count(*) from attendence_management a inner join tbluserrole b on a.Id=b.userid where a.email = @email and b.instanceid = @insId", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@insId", _InstanceID);
                return ((int)cmd.ExecuteScalar() > 0);
            }
        }
    }

    public void Insertuserrole(string userid, string role, string email, string ids = "")
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("adduserrole", con))
                {
                    con.Open();
                    if (ids == "")
                    {
                        cmd.Parameters.AddWithValue("@id", Request["id"]);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@id", ids);
                    }

                    cmd.Parameters.AddWithValue("@role", role);
                    cmd.Parameters.AddWithValue("@userid", userid);
                    cmd.Parameters.AddWithValue("@email", email);
                    cmd.Parameters.AddWithValue("@instanceid", _InstanceID);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
        }
        catch (Exception ex)
        {

            throw;
        }
    }

    protected void BtnsendcontrorinfoClick(object sender, EventArgs e)
    {
        if (!UserExists(txtEmail.Text))
        {
            LnkAddContractClick(sender, e);

            Hashtable objuser = new Hashtable();
            AddinstanceWS objaddinst = new AddinstanceWS();
            string userid = objaddinst.AddInstance(txtEmail.Text, "6e815b00-6e13-4839-a57d-800a92809f21", _InstanceID, AppId, txtFirstName.Text, txtLastName.Text);
            AvaimaUserProfile objwebser = new AvaimaUserProfile();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("select count(*) from tbluserrole where ownerid = @id and instanceid = @insId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", userid);
                    cmd.Parameters.AddWithValue("@insId", _InstanceID);
                    int i = (int)cmd.ExecuteScalar();
                    if (i > 0)
                    {
                        cmd.Parameters.Clear();
                        cmd.CommandText = "del_records";
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@Pid", workeruserprofile.wid);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();

                        // to be continued
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "alreadyExists();", true);
                    }
                    else
                    {
                        objuser.Add("email", txtEmail.Text);
                        objwebser.insertUserFull(userid, "", objuser["email"].ToString(), "", "", "",
                                             "", "", "", txtFirstName.Text, txtLastName.Text, "", "", "", "", "", "", "", "", workeruserprofile.wid, _InstanceID, "");
                        Insertuserrole(userid, "worker", objuser["email"].ToString(), id.Value);
                        AvaimaEmailAPI email = new AvaimaEmailAPI();
                        //email.Adduseremail(HttpContext.Current.User.Identity.Name, userid, objuser["email"].ToString(), "Attendance Management");
                        StringBuilder subject = new StringBuilder();
                        StringBuilder body = new StringBuilder();
                        subject.Append(hdnAdminName.Value + " wants you to use Time & Attendance");
                        body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                        body.Append("<b>" + hdnAdminName.Value + "</b> wants you to use \"Time & Attendance\" software application on Avaima. You can use this application to clock-in / clock-out on a daily basis using your own user-account. Follow the steps below to start using the application:");
                        //body.Append("</br> He added you in this application and wants you to clock-in / clock-out on a daily basis.");
                        //body.Append("</br> Follow the steps below to start using the application:");
                        body.Append("<ul>");
                        body.Append("<li>In another email you should have received request to sign up on Avaima.com. Follow the instructions and sign up on Avaima &ndash; sign up is <b>FREE</b>. "
                            + "If you did not receive this email, go to Avaima.com and sign up using " + objuser["email"].ToString() + " as your email address</li>");
                        body.Append("<li>Go to <a href='http://www.avaima.com/signin'>www.avaima.com/signin</a> and login to your account</li>");
                        body.Append("<li>Find and click \"Time & Attendance\" in the left menu</li>");
                        body.Append("<li>\"Clock-In\" when you start your day at work and \"Clock-Out\" when you are done</li>");
                        body.Append("</ul>");
                        body.Append("For further questions, comments or help contact support@avaima.com.");
                        body.Append(Helper.AvaimaEmailSignature);
                        body.Append("</div>");
                        email.send_email(objuser["email"].ToString(), "Attendance Management", "", body.ToString(), subject.ToString());
                        if (string.IsNullOrEmpty(hfparentid.Value) || string.IsNullOrWhiteSpace(hfparentid.Value))
                        {
                            hfparentid.Value = "0";
                        }
                        RptrAttendanceFill(Convert.ToInt32(hfparentid.Value));
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "openConfirmBox('User added successfully.');", true);
                    }
                }
            }
            txtEmail.Text = "";
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "", "alreadyExists()", true);
            txtEmail.Text = "";
            //ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('User already exist in this instance.');", true);
        }
    }

    protected void Breadcrum(int id)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("getidforbreadcrum", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                cmd.CommandType = CommandType.StoredProcedure;
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    _breadcrumdict.Add(Convert.ToString(dt.Rows[0]["ID"]), Convert.ToString(dt.Rows[0]["title"]));
                    if (Convert.ToInt32(dt.Rows[0]["P_ID"]) != 0)
                    {
                        Breadcrum(Convert.ToInt32(dt.Rows[0]["P_ID"]));
                    }
                }
            }
        }
    }

    //protected void Bredcrumbind()
    //{
    //    var revbreadcrum = _breadcrumdict.Reverse();
    //    rptBreadCrum.DataSource = revbreadcrum;
    //    rptBreadCrum.DataBind();
    //    if (revbreadcrum.Count() == 0)
    //    {
    //        rptBreadCrum.Visible = false;
    //    }
    //    else
    //    {
    //        rptBreadCrum.Visible = true;
    //        root.Visible = true;
    //    }
    //}

    public void FillTreeView(TreeNodeCollection treenode, string parentid)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlDataAdapter adp = new SqlDataAdapter("gettreeviewid", con))
            {
                con.Open();
                adp.SelectCommand.Parameters.AddWithValue("@id", parentid);
                adp.SelectCommand.Parameters.AddWithValue("@InstanceId", _InstanceID);
                adp.SelectCommand.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        TreeNode node = new TreeNode(String.Format("<input type='radio' name='rbtncat' value='{0}' onclick='setvalue({0})'/>&nbsp;{1}", dr["ID"].ToString(), dr["Title"].ToString()));
                        //TreeNode node = new TreeNode(dr["Title"].ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        FillTreeView(node.ChildNodes, dr["ID"].ToString());
                        treenode.Add(node);
                    }
                }
            }
        }
    }

    protected void LnkAddContractClick(object sender, EventArgs e)
    {
        try
        {
            string s = hfparentid.Value;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_insertworker", con))
                {
                    string title = txtFirstName.Text + " " + txtLastName.Text;
                    con.Open();
                    cmd.Parameters.AddWithValue("@title", title);
                    cmd.Parameters.AddWithValue("@parentid", s);
                    cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                    cmd.Parameters.AddWithValue("@datefield", Convert.ToDateTime(DateTime.Now));
                    cmd.Parameters.AddWithValue("@category", 0);
                    cmd.Parameters.AddWithValue("@email", txtEmail.Text);
                    cmd.Parameters.AddWithValue("@active", true);

                    cmd.CommandType = CommandType.StoredProcedure;
                    id.Value = cmd.ExecuteScalar().ToString();

                    atd.UserAccess(id.Value, s, _InstanceID);

                    workeruserprofile.wid = id.Value;
                    workeruserprofile.InstanceID = _InstanceID;
                    RptrAttendanceFill(Convert.ToInt32(s));
                    //ScriptManager.RegisterStartupScript(this, GetType(), "", "openAdd();", true);
                }
            }
        }
        catch (Exception ex)
        {
            //lblexception.Text = ex.Message;
        }


    }

    protected void RootClick(object sender, EventArgs e)
    {
        _breadcrumdict.Clear();
        //  Bredcrumbind();
        //RptrAttendanceFill(0, IsAdmin);
    }


    protected void RptrattendenceItemcommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "openpopup")
        {
            LinkButton lnk = e.Item.FindControl("lnktitle") as LinkButton;
            this.Redirect("Add_worker.aspx?id=" + Request.QueryString["id"].ToString() + "&uid=" + lnk.CommandArgument + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + hdnUserEmail.Value.Encrypt() + "&p=" + hdnUserPassword.Value.Encrypt());

        }

        //else if (e.CommandName == "settings")
        //{
        //    ImageButton btnSettings = e.Item.FindControl("btnSettings") as ImageButton;
        //    fid.Value = btnSettings.CommandArgument;
        //    FolderSettings(Convert.ToInt32(fid.Value));
        //    clsbtn.Style.Add("display", "none");
        //    ScriptManager.RegisterStartupScript(this, GetType(), "", "settings();", true);
        //}
        else if (e.CommandName == "absent")
        {
            try
            {
                atd.MarkAbsent(e.CommandArgument.ToInt32(), "Marked absent by admin");
                FillAttendance();
            }
            catch (Exception ex) { }
        }
        else if (e.CommandName == "clockin")
        {
            try
            {
                atd.ClockIn(e.CommandArgument.ToString(), Request.UserHostAddress, "web", 2, "Clock-in by admin");
                FillAttendance();
            }
            catch (Exception ex) { }
        }
        else if (e.CommandName == "clockout")
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                {
                    using (SqlCommand cmd1 = new SqlCommand("SELECT rowID FROM workertrans WHERE P_PID = @ppid AND lastout IS NULL", con))
                    {
                        con.Open();
                        cmd1.Parameters.AddWithValue("@ppid", e.CommandArgument);
                        int rowId = Convert.ToInt32(cmd1.ExecuteScalar());

                        atd.ClockOut(e.CommandArgument.ToString(), Request.UserHostAddress, rowId, "web", 2, "Clock-out by admin");

                        FillAttendance();
                    }
                }
            }
            catch (Exception ex) { }
        }
    }


    protected void adminItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "delAdmin" && !string.IsNullOrEmpty(hfparentid.Value))
        {
            LinkButton lnk = e.Item.FindControl("delAdmin") as LinkButton;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("delcategoryadmin", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CatId", hfparentid.Value);
                cmd.Parameters.AddWithValue("@UserId", lnk.CommandArgument);
                cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                cmd.ExecuteNonQuery();
                this.Redirect("Default.aspx");

                SendRoleUpdateEmail(false);
            }
        }
    }


    protected void Rprteattendenceitemdatabound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();

            object container = e.Item.DataItem;
            string userId = DataBinder.Eval(e.Item.DataItem, "ID").ToString();
            Boolean userStatus = SP.IsWorkerActive(Convert.ToInt32(userId));

            //Managed By
            //Label managedby = e.Item.FindControl("lblmanaged") as Label;
            //if (managedby.Text == "True")
            //{
            //    managedby.Text = "Own";
            //}
            //else
            //{
            //    managedby.Text = "Parent";
            //}


            Label timein = e.Item.FindControl("lbltimein") as Label;
            if (timein.Text != "")
            {
                string inDate = timein.Text;
                //if (inDate.Contains("Today"))
                //{
                //    timein.Text = "Today";
                //}
                //else
                //{
                //    //timein.Text = inDate;
                //    timein.Text = "";
                //}
                if (Convert.ToDateTime(inDate).Date == DateTime.Now.Date)
                {
                    timein.Text = "Today";
                }
                else
                {
                    timein.Text = "";
                }


            }
            //else
            //{
            //    timein.Text = "Last Clock-in Never";
            //}

            LinkButton title = e.Item.FindControl("lnktitle") as LinkButton;
            //this.Redirect("Add_worker.aspx?id=" + Request.QueryString["id"].ToString() + "&uid=" + lnk.CommandArgument + "&e=" + hdnUserEmail.Value.Encrypt() + "&p=" + hdnUserPassword.Value.Encrypt());
            title.PostBackUrl = "Add_worker.aspx?id=" + Request.QueryString["id"].ToString() + "&uid=" + userId + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + hdnUserEmail.Value.Encrypt() + "&p=" + hdnUserPassword.Value.Encrypt();

            //Row Colors
            HtmlTableRow table_row = e.Item.FindControl("trdata") as HtmlTableRow;


            //for hours        
            Label lblworkhours = e.Item.FindControl("lblworkhours") as Label;
            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(container, "hours"))))
            {
                lblworkhours.Text = DataBinder.Eval(container, "hours").ToString();
            }

            //Clockin Time
            Label clockintime = e.Item.FindControl("lbltimeforin") as Label;
            clockintime.Text = (clockintime.Text != "") ? Convert.ToDateTime(clockintime.Text).ToString("hh:mm tt") : clockintime.Text;


            //Work Status 
            Label status = e.Item.FindControl("lblworkstatus") as Label;
            //LinkButton action = e.Item.FindControl("lnkAbsent") as LinkButton;
            LinkButton clockin = e.Item.FindControl("lnkClockin") as LinkButton;
            HiddenField user_status = e.Item.FindControl("hf_status") as HiddenField;

            //Error Case
            HiddenField lastactivity = e.Item.FindControl("hf_lastactivity") as HiddenField;
            Label error = e.Item.FindControl("error") as Label;


            #region Clock-in/out buttons & Parent Controls
            if (status.Text == "Present")
            {
                user_status.Value = "present";
                //action.Text = "Clocked-in";
                //action.CommandName = "clockout";
                //action.OnClientClick = "return confirm('Are you sure you want user to Clock-out?');";
                //action.ToolTip = "Please click to Clock-out user";
                //clockin.Visible = false;
                clockin.Visible = true;
                //clockin.Text = "Clocked-in";
                clockin.Text = "<i class='fa fa-fw fa-sign-out'></i>";
                clockin.CommandName = "clockout";
                // clockin.OnClientClick = "return confirm('Are you sure you want user to Clock-out?');";
                clockin.ToolTip = "Clock-out";
                //row.Cells["action"].Style.ForeColor = Color.Red;
                //row.Cells["action"].ToolTipText = "Clock-out";
                //row.Cells["action"].Value = "Clock-out";
            }
            else if (status.Text == "")
            {
                user_status.Value = "none";
                //action.Text = "Mark as Absent";
                //action.CommandName = "absent";
                //action.OnClientClick = "return confirm('Are you sure you want to mark user as Absent?');";
                clockin.Visible = true;
                clockin.CommandName = "clockin";
                //clockin.Text = "Clocked-out";
                clockin.Text = "<i class='fa fa-fw fa-sign-in'></i>";
                //clockin.OnClientClick = "return  $.confirm('Are you sure you want user to Clock-in?');";
                // clockin.OnClientClick = "return confirm('Are you sure you want user to Clock-in?');";
                clockin.ToolTip = "Clock-in";
                //row.Cells["action"].Style.ForeColor = Color.Red;
                //row.Cells["action"].ToolTipText = "Mark as Absent";
                //row.Cells["action"].Value = "Mark as Absent";
                //table_row.Attributes.Add("class", "yellowRow");

            }
            else if (status.Text == "Error")
            {
                status.Text = Convert.ToDateTime(lastactivity.Value).ToString("hh:mm tt, dd MMM yy ");
                error.Text = "<i class='fa fa-fw fa-exclamation-triangle'></i>Time";

                user_status.Value = "error";
                //clockin.Text = "Clocked-in";
                clockin.Text = "<i class='fa fa-fw fa-sign-out'></i>";
                clockin.CommandName = "clockout";
                //  clockin.OnClientClick = "return confirm('Are you sure you want user to Clock-out?');";
                clockin.ToolTip = "Clock-out";
                //action.Text = "User not clocked-out";
                //action.CommandName = "clockout";
                clockin.Visible = true;
            }
            else if (status.Text == "Clocked out")
            {
                user_status.Value = "clockedout";
                clockin.Visible = true;
                //clockin.Text = "Clocked-out";
                clockin.Text = "<i class='fa fa-fw fa-sign-in'></i>";
                //clockin.OnClientClick = "return $.confirm('Are you sure you want user to Clock-in?');";
                //clockin.OnClientClick = "return confirm('Are you sure you want user to Clock-in?');";
                clockin.ToolTip = "Clock-in";
                table_row.Attributes.Add("class", "yellowRow");

            }
            else
            {
                if (status.Text == "Absent-Present")
                {
                    user_status.Value = "absent-present";
                    clockin.Visible = true;
                    lblworkhours.Visible = true;
                    clockintime.Visible = true;
                }
                else
                {
                    user_status.Value = "absent";
                    clockin.Visible = false;
                    lblworkhours.Visible = false;
                    clockintime.Visible = false;
                }

                status.Text = "<b>Absent</b> ";

                //clockin.Visible = false;
                //lblworkhours.Visible = false;
                //clockintime.Visible = false;
                table_row.Attributes.Add("class", "absentRow");
            }


            //IP Verification Status
            HiddenField isVerified = (HiddenField)e.Item.FindControl("hdnIsVerified");
            if (isVerified.Value != "Verified" && isVerified.Value != "")
                error.Text = "<i class='fa fa-fw fa-exclamation-triangle'></i>IP";


            //Checkbox Enable only if you are parent of User
            //HiddenField parentid = e.Item.FindControl("hf_parentid") as HiddenField;
            //if (parentid.Value == "0")
            //{
            //    CheckBox chk = e.Item.FindControl("chkselecteditem") as CheckBox;
            //    chk.Enabled = false;
            //}
            #endregion

            //Highlight current user row
            if (userId == hfparentid.Value)
            {
                LinkButton Title = e.Item.FindControl("lnktitle") as LinkButton;
                Title.Font.Bold = true;

                HtmlTableRow row = e.Item.FindControl("trdata") as HtmlTableRow;
                row.Attributes.Add("class", "greenRow");
            }
            //else {
            //    HtmlTableRow row = e.Item.FindControl("trdata") as HtmlTableRow;
            //    row.Attributes.Add("class", "defaultRow");
            //}


            HtmlControl trdata = e.Item.FindControl("trdata") as HtmlControl;
            List<Absence> absences = Absence.GetAbsences(Convert.ToInt32(userId.ToString()));
            //List<ExDay> exdays = ExDay.GetExDays(Convert.ToInt32(userId.ToString()));

            //---- for status
            Label lblwork = e.Item.FindControl("lblworkstatus") as Label;
            String strMenuHtml = "";
            strMenuHtml = "<ul id='menu'>";
            bool ClockedIn = false;

            List<Absence> tempabs = absences.Where(u => u.CrtDate.Date == DateTime.Now.Date && u.Active == true).ToList();
            HtmlControl imgAction = e.Item.FindControl("imgAction") as HtmlControl;

            trdata.ID = "trdata" + userId;
            //imgAction.Style.Add("visibility", "visible");
            //imgAction.Visible = true;
            imgAction.Attributes.Add("data-ab-userid", DataBinder.Eval(container, "ID").ToString());
            imgAction.Attributes.Add("data-ab-date", DataBinder.Eval(container, "lastin").ToString());
            imgAction.Attributes.Add("data-ab-active", "true");
            imgAction.Attributes.Add("data-ab-tr", ("trdata" + userId));
            imgAction.Attributes.Add("data-inaddress", Request.UserHostAddress);
            imgAction.Attributes.Add("data-useractive", userStatus.ToString());
            imgAction.Attributes.Add("class", "imgAction");


            HiddenField parentid = e.Item.FindControl("hf_parentid") as HiddenField;
            if (parentid.Value == "0" || (userId == hdnUserID.Value))
            {
                imgAction.Attributes.Add("data-parentid", "0");
            }
            else
                imgAction.Attributes.Add("data-parentid", parentid.Value);

            if (user_status.Value == "none")
            {
                imgAction.Attributes.Add("data-ab-id", "0");
                imgAction.Attributes.Add("data-clock", ClockedIn.ToString());
                //string MarkAbsent = "<a class='lnkAbsent menuitem' id='lnkAbsent' href='#'  style='color:white'  data-userid='" + DataBinder.Eval(container, "ID").ToString() + "' data-date='" + DataBinder.Eval(container, "lastin").ToString() + "' data-active='false' data-tr='" + ("trdata" + userId) + "'></a>";
                //strMenuHtml += "<li>" + MarkAbsent + "</li>";
            }
            else
            {
                if (tempabs.Count > 0)
                {
                    if (user_status.Value == "absent-present")
                    {
                        ClockedIn = true;
                        imgAction.Attributes.Add("data-ab-id", "-1");
                    }
                    else
                    {
                        ClockedIn = false;
                        imgAction.Attributes.Add("data-ab-id", tempabs.Last().AbsLogID.ToString());                        
                    }

                    imgAction.Attributes.Add("data-clock", ClockedIn.ToString());
                    //string MarkAsAttendance = "<a href='#' class='lnkMarkAbsence' style='color:white' data-id='" + tempabs.Last().AbsLogID + "' data-userid='" + DataBinder.Eval(container, "ID").ToString() + "' data-date='" + DataBinder.Eval(container, "lastin").ToString() + "' data-active='false' data-tr='" + ("trdata" + userId) + "'>Mark as present</a>";
                    //strMenuHtml += "<li>" + MarkAsAttendance + "</li>";
                }
                else
                {
                    // if (user_status.Value == "present" || user_status.Value == "error" || user_status.Value == "clockedout")
                    if (user_status.Value == "present" || user_status.Value == "error")
                    {
                        ClockedIn = true;
                        imgAction.Attributes.Add("data-ab-id", "0");
                    }
                    //else if (user_status.Value == "clockedout")
                    //{
                    //    ClockedIn = true;
                    //    imgAction.Attributes.Add("data-ab-id", "-1");
                    //}
                    else
                    {
                        ClockedIn = false;
                        imgAction.Attributes.Add("data-ab-id", "0");
                    }

                    imgAction.Attributes.Add("data-clock", ClockedIn.ToString());
                }
            }
            strMenuHtml += "</ul>";


            //Managed User
            if (hdnUserID.Value == userId)
            {
                if (!atd.UserManaged(userId.ToInt32(), _InstanceID, "get"))
                {
                    clockin.Visible = false;
                    imgAction.Visible = false;
                }
            }

            if (UserFirstTime(userId.ToInt32()) == true)
            {
                UpdateTimeZone(userId.ToString());
            }

            //STATUS TOOLTIP
            //DateTime lastIn = SP.GetWorkerLastSignInDateTime(userId);
            //GetHistoryRecord(userId, lastIn.Date, lastIn.Date);
            //RecordModel record = records[0];

            //string strDetails = "<table id=\"tblDetail\">";
            //strDetails += "<tr><td colspan='2' style='text-align:center'><b>Last Clocked Out Details</b></td></tr>";

            //if (record.Records.Count > 0)
            //{
            //    string lblinDate = objATZ.GetDate(record.Records[0].LoginTime.ToString(), HttpContext.Current.User.Identity.Name);
            //    string lblinTime = objATZ.GetTime(record.Records[0].LoginTime.ToString(), HttpContext.Current.User.Identity.Name);
            //    string lbloutTime = objATZ.GetTime(record.Records[0].LogoutTime.ToString(), HttpContext.Current.User.Identity.Name);

            //    strDetails += "<tr><td colspan='2' style='text-align:center'>" + lblinDate + "</td><tr>";
            //    strDetails += "<tr><td class='caption'>Clock in: </td><td>" + lblinTime + "</td><tr>";
            //    strDetails += "<tr><td class='caption'>Clock Out: </td><td>" + lbloutTime + "</td><tr>";

            //    strDetails += "<tr><td class='caption'>Clock in location: </td><td>" + record.Records[0].InLocation;
            //    if (record.Records[0].LoginStatus == "Verified")
            //    {
            //        strDetails += "<image src=\"/images/Controls/tick.png\" width=\"12px\" style='padding-left:3px;' /></td></tr>";
            //    }
            //    else
            //    {
            //        strDetails += "<image src=\"/images/Controls/cross.png\" width=\"12px\" style='padding-left:3px;' /></td></tr>";
            //    }

            //    strDetails += "<tr><td class='caption'>Clock out location: </td><td>" + record.Records[0].OutLocation;

            //    if (record.Records[0].LogoutStatus == "Verified")
            //    {
            //        strDetails += "<image src=\"/images/Controls/tick.png\" width=\"12px\" style='padding-left:3px;' /></td></tr>";
            //    }
            //    else
            //    {
            //        strDetails += "<image src=\"/images/Controls/cross.png\" width=\"12px\" style='padding-left:3px;' /></td></tr>";
            //    }
            //    strDetails += "<tr><td class='caption'>Total Worked: </td><td>" + record.TotalWorkedHours + "</td><tr>";

            //}
            //else
            //{
            //    strDetails += "<tr><td colspan='2' style='text-align:center'></td><tr>";
            //    strDetails += "<tr><td class='caption'>Clock in: </td><td>Never</td><tr>";
            //    strDetails += "<tr><td class='caption'>Clock Out: </td><td>Never</td><tr>";
            //}

            ////lblworkhours.Visible = false;
            //#region When not accounted for
            //HtmlControl imgInfo = e.Item.FindControl("imgInfo") as HtmlControl;
            //imgInfo.Style.Add("display", "inline-block");
            //imgInfo.Visible = true;
            //imgInfo.Attributes.Add("class", "tooltip");

            //strDetails += "</table>";
            //imgInfo.Attributes.Add("Title", strDetails);
            //imgInfo.Attributes.Add("Tooltip", strDetails);
            //#endregion

            //STATUS TOOLTIP END

        }
    }

    private DataTable GetHistory(string userId, SqlConnection con)
    {
        using (SqlCommand cmd = new SqlCommand("sp_GetHistory", con))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            List<string> dates;
            dates = UtilityMethods.getpastDates(7);
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@UserId", userId);
            adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            return ds.Tables[0];

        }

    }

    private void GetHistoryRecord(string userID, DateTime start, DateTime end)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("getworkerrecordbydate", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@sDate", start.Date);
                cmd.Parameters.AddWithValue("@eDate", start.Date);
                cmd.Parameters.AddWithValue("@Id", userID);
                cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                records = new List<RecordModel>();
                records = UtilityMethods.getRecords(ds);
                //rptHistory.DataSource = records;
                //rptHistory.DataBind();            
            }
        }

    }

    protected void Rptbreadcrumitemdatabound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton lnk = e.Item.FindControl("lbtnBreadCrum") as LinkButton;
            lnk.Text = DataBinder.Eval(e.Item.DataItem, "value").ToString();
            lnk.CommandArgument = DataBinder.Eval(e.Item.DataItem, "key").ToString();
        }
    }

    protected void RptrbreadcrumitemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton lnk = e.Item.FindControl("lbtnBreadCrum") as LinkButton;
            hfparentid.Value = e.CommandArgument.ToString();
            Breadcrum(Convert.ToInt32(e.CommandArgument));
            //Bredcrumbind();
            RptrAttendanceFill(Convert.ToInt32(e.CommandArgument));
        }
    }


    public void pg_rptrcontract_OnPageIndex_Changed(object sender, EventArgs e)
    {
        RptrAttendanceFill(Convert.ToInt32(hfparentid.Value));
    }


    private void SendRoleUpdateEmail(Boolean admin)
    {
        AvaimaEmailAPI email = new AvaimaEmailAPI();
        StringBuilder subject = new StringBuilder();
        StringBuilder body = new StringBuilder();
        if (admin)
        {
            subject.Append(hdnAdminName.Value + " made you Admin in Time & Attendance");
            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
            body.Append("<b><p style='line-height:32px'>" + hdnAdminName.Value + "</b> assigned you as <b>Admin</b> in \"Time & Attendance\" software application on Avaima.");
            body.Append("</br> You can now see and manage the time and attendance of all the employees under your supervision. Your dashboard will give you an overview of the statuses of all the employees.");
            //body.Append("</br>For questions email <a href='mailto:support@avaima.com'>support@avaima.com</a></p>");
            //body.Append("</br></br><a href='http://avaima.com'>AVAIMA.COM</a>");
            //body.Append("</br><i>Organize your work!</i>");
            //body.Append("</br></br>For information contact: <a href='mailto:info@avaima.com'>info@avaima.com</a>");
            //body.Append("</br>For information contact: <a href='mailto:support@avaima.com '>support@avaima.com </i>");
            body.Append(Helper.AvaimaEmailSignature);
            body.Append("</div>");
        }
        else
        {
            subject.Append(hdnAdminName.Value + " removed your admin status");
            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
            body.Append("<b><p style='line-height:32px'>" + hdnAdminName.Value + "</b> removed your status as an \"Admin\" in \"Time & Attendance\" software application on Avaima.com.");
            body.Append("</br> You can only clock-in / clock-out and view your own statistics only.");
            //body.Append("</br>For questions email <a href='mailto:support@avaima.com'>support@avaima.com</a></p>");
            //body.Append("</br></br><a href='http://avaima.com'>AVAIMA.COM</a>");
            //body.Append("</br><i>Organize your work!</i>");
            //body.Append("</br></br>For information contact: <a href='mailto:info@avaima.com'>info@avaima.com</a>");
            //body.Append("</br>For information contact: <a href='mailto:support@avaima.com '>support@avaima.com </i>");
            body.Append(Helper.AvaimaEmailSignature);
            body.Append("</div>");
        }
        string str = body.ToString();
        //hdnAdminEmail.Value.ToString()
        email.send_email(SP.GetEmailByUserID(Convert.ToInt32(appAssignedId.ToString())), "Attendance Management", "", body.ToString(), subject.ToString());
    }


    protected void lnkMyAttendance_Click(object sender, EventArgs e)
    {
        attendancetype objattendancetype = attendancetype.SingleOrDefault(u => u.userid == hdnUserID.Value.ToString() && u.instanceid == this.InstanceID);
        this.Redirect("Add_worker.aspx?id=" + Request.QueryString["id"].ToString() + "&uid=" + Convert.ToInt32(hdnUserID.Value.ToString()) + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + hdnUserEmail.Value.Encrypt() + "&p=" + hdnUserPassword.Value.Encrypt());

    }

    public string testInstanceID { get { return "0"; } }

    public List<AutoPresent> autopresences { get; set; }
    protected void Button1_Click(object sender, EventArgs e)
    {
        AvaimaEmailAPI email = new AvaimaEmailAPI();
        StringBuilder subject = new StringBuilder();
        StringBuilder body = new StringBuilder();
        subject.Append(hdnAdminName.Value + " wants you to use Time & Attendance");
        body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
        body.Append("<b>" + hdnAdminName.Value + "</b> uses <b>\"Time & Attendance\"</b> software application on Avaima.");
        body.Append("</br> He added you in this application and wants you to clock-in / clock-out on a daily basis.");
        body.Append("</br> Follow the steps below to start using the application:");
        body.Append("<ul>");
        body.Append("<li>In another email you should have received request to sign up on Avaima.com. Follow the instructions and sign up on Avaima. Sign up is <b>FREE</b></br>"
            + "If you did not receive this email, go to Avaima.com and sign up using xyz@hotmail.com as your email address</li>");
        body.Append("<li>Go to <a href='http://www.avaima.com/signin'>www.avaima.com/signin</a> and login to your account</li>");
        body.Append("<li>Find and click \"Time & Attendance\" in the left menu</li>");
        body.Append("<li>\"Clock-In\" when you start your day at work and \"Clock-Out\" when you are done</li>");
        //body.Append("<li></li>");
        //body.Append("<li></li>");        
        body.Append("</ul>");
        //body.Append("</br>For questions email support@avaima.com");
        //body.Append("</br><a href='http://avaima.com'>AVAIMA.COM</a>");
        //body.Append("</br><i>Organize your work!</i>");
        //body.Append("</br></br>For information contact: <a href='mailto:info@avaima.com'>info@avaima.com</a>");
        //body.Append("</br>For information contact: <a href='mailto:support@avaima.com '>support@avaima.com </i>");
        body.Append(Helper.AvaimaEmailSignature);
        body.Append("</div>");
        string strbody = body.ToString();

        email.send_email("sbmuhammdfaizan@hotmail.com", hdnAdminName.Value.ToString(), hdnAdminEmail.Value.ToString(), body.ToString(), subject.ToString());
    }

    public List<RecordModel> records { get; set; }
    //protected void lnkLateReport_Click(object sender, EventArgs e)
    //{
    //    string page = "Reports.aspx?id=" + appAssignedId.ToString() + "&instanceid=" + _InstanceID;
    //    this.Redirect(page);
    //}

    //protected void Timer1_Tick(object sender, EventArgs e)
    //{
    //    lblTimer.Text = JsonConvert.DeserializeObject<DataTable>(atd.GetStatus(hdnUserID.Value)).Rows[0]["hours"].ToString();
    //}

    protected void btnSignin_Click(object sender, EventArgs e)
    {
        atd.ClockIn(hdnUserID.Value, Request.UserHostAddress, "web");
        LoadUserData();
        RptrAttendanceFill(Convert.ToInt32(hfparentid.Value), true);
    }

    protected void btnSignout_Click(object sender, EventArgs e)
    {
        string ppid = hdnUserID.Value;

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd1 = new SqlCommand("SELECT rowID FROM workertrans WHERE P_PID = @ppid AND lastout IS NULL", con))
            {
                con.Open();
                cmd1.Parameters.AddWithValue("@ppid", ppid);
                int rowId = Convert.ToInt32(cmd1.ExecuteScalar());

                atd.ClockOut(ppid, Request.UserHostAddress, rowId, "web");

            }
        }

        LoadUserData();
        RptrAttendanceFill(Convert.ToInt32(hfparentid.Value), true);
    }

    protected void lnkRemoveUser_Click(object sender, EventArgs e)
    {
        var ids = (from r in Rptrattendence.Items.Cast<RepeaterItem>()
                   let selected = (r.FindControl("chkselecteditem") as CheckBox).Checked
                   let id = (r.FindControl("hf_ID") as HiddenField).Value
                   where selected
                   select id).ToList();


        foreach (var id in ids)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Update attendence_management SET active= 0 where ID = @ID", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@ID", id);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        RptrAttendanceFill(Convert.ToInt32(hfparentid.Value));

        string Msg = "UpdateMenu(false); openConfirmBox('Employee removed successfully.');";

        if (ids.Count > 1)
            Msg = "UpdateMenu(false); openConfirmBox('Employees removed successfully.');";

        ScriptManager.RegisterStartupScript(this, GetType(), "", Msg, true);

    }

    [WebMethod]
    public static void RemoveUsers(int userid, int parentID)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("Update attendence_management SET active= 0 , datefield='" + DateTime.Now + "' where ID = @ID", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@ID", userid);
                cmd.ExecuteNonQuery();
            }
        }

        //var thisPage = new _Default();
        //thisPage.RptrAttendanceFill(parentID);
    }

    [WebMethod]
    public static void MarkAbsent(int userid, string reason, bool isadjustment)
    {
        Attendance atd = new Attendance();
        atd.MarkAbsent(userid, reason, null, isadjustment);
    }

    [WebMethod]
    public static int CheckEmployees(string userid, string instanceid, bool active)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("Select count(*) FROM schema_6e815b00_6e13_4839_a57d_800a92809f21.attendence_management  where parentid = @userid and active = @active and instanceid = @instanceid", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", userid);
                cmd.Parameters.AddWithValue("@instanceid", instanceid);
                cmd.Parameters.AddWithValue("@active", active);
                return ((int)cmd.ExecuteScalar());
            }
        }

    }

    protected void btnUpdateStatus_Click(object sender, EventArgs e)
    {
        string comment = "";
        int location = 1;

        if (hdn_stuserid.Value != hdnUserID.Value)
        {
            comment = "by admin";
            location = 2;
        }

        try
        {
            if (hdn_stclockin.Value == "False")
            {
                MyAttSys.avaimaTest0001DB db = new avaimaTest0001DB();
                DataTable dt1 = db.getcurentuserstatus(Convert.ToInt32(hdn_stuserid.Value)).ExecuteDataSet().Tables[0];
                if (dt1.Rows.Count > 0)
                {
                    //ScriptManager.RegisterStartupScript(this, GetType(), "", "alert();", true);
                    return;
                }

                atd.ClockIn(hdn_stuserid.Value, Request.UserHostAddress, "web", location, "Clock-in " + comment);
                FillAttendance();

                ScriptManager.RegisterStartupScript(this, GetType(), "", " openConfirmBox('Successful Clock-IN!');", true);
            }
            else if (hdn_stclockin.Value == "True")
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                {
                    using (SqlCommand cmd1 = new SqlCommand("SELECT rowID FROM workertrans WHERE P_PID = @ppid AND lastout IS NULL", con))
                    {
                        con.Open();
                        cmd1.Parameters.AddWithValue("@ppid", hdn_stuserid.Value);
                        int rowId = Convert.ToInt32(cmd1.ExecuteScalar());

                        atd.ClockOut(hdn_stuserid.Value, Request.UserHostAddress, rowId, "web", location, "Clock-out " + comment);

                        FillAttendance();
                    }
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "", " openConfirmBox('Successful Clock-OUT!');", true);

            }

            hdn_stclockin.Value = "";
        }
        catch (Exception ex) { }

    }
    protected void timerForRepeater_Tick(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "cache", "refreshDiv();", true);
       // FillAttendance();

    }

    [WebMethod]
    public static void SettingsUpdate(string userId, int rowId, string timezone, int dst_difference, bool sendEmail, string link)
    {
        try
        {
            Attendance atd = new Attendance();
            string IP = HttpContext.Current.Request.UserHostAddress;
            int dlsaving = 0;

            if (dst_difference > 0)
                dlsaving = 1;
            else if (dst_difference < 0)
                dlsaving = 2;
            else
                dlsaving = 0;

            dst_difference = Math.Abs(dst_difference);

            //Clock-out user (if clocked-in), than update timezone settings and than Clock-in again using new time settings
            if (rowId != 0)
                atd.ClockOut(userId, IP, rowId, "web", 2, "Auto Clock-out to update time sttings", null, false);

            atd.SetTimezone(atd.Timezone_datetime(userId, DateTime.Now.ToString(), ""), timezone, dlsaving, dst_difference);

            if (rowId != 0)
                atd.ClockIn(userId, IP, "web", 2, "Auto Clock-in to update time sttings", false);

            if (sendEmail)
            {
                //Email updates to support
                AvaimaEmailAPI email = new AvaimaEmailAPI();
                StringBuilder subject = new StringBuilder();
                StringBuilder body = new StringBuilder();

                subject.Append("Timezone Notification");
                body.Append("<br/>This email is to notify that timezone auto update feature was used!");
                string url = "http://www.avaima.com/6e815b00-6e13-4839-a57d-800a92809f21/W/" + link;

                body.Append("<br/>Please click <a href='" + url + "'>HERE</a> to visit user profile.");

                email.send_email("support@avaib.com", "Time & Attendance", "", body.ToString(), subject.ToString());
                email.send_email("sundus_csit@yahoo.com", "Time & Attendance", "", body.ToString(), subject.ToString());
            }
        }
        catch (Exception ex)
        {
        }
    }
}