﻿using AvaimaThirdpartyTool;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class W_FormerUsers : AvaimaWebPage
{
    Attendance atd = new Attendance();

    protected void Page_Load(object sender, EventArgs e)
    {
        hdnInstanceId.Value = Request.QueryString["instanceid"].ToString();
        hdnUserID.Value = Request.QueryString["id"].ToString();
        hdnUserEmail.Value = Request.QueryString["e"].Decrypt();
        hdnUserPassword.Value = Request.QueryString["p"].Decrypt();

        string isVerified = atd.VerifyLogin(hdnUserEmail.Value, hdnUserPassword.Value);
        if (isVerified != "TRUE")
            this.Redirect("Info.aspx");

        if (!IsPostBack)
        {
            LoadUsers(false);
        }

        if (Request.Form["__EVENTTARGET"] == "lnkRestoreUser")
        {
            lnkRestoreUser_Click(sender, e);
        }
    }

    public void LoadUsers(bool IsActive = false)
    {
        bool check = false;
        hdnIsActive.Value = IsActive.ToString();

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_DGetAllAttendenceRecord1", con))
            {
                cmd.Parameters.AddWithValue("@id", 0);
                cmd.Parameters.AddWithValue("@InstanceId", hdnInstanceId.Value);
                cmd.Parameters.AddWithValue("@IsActive", IsActive);
                cmd.Parameters.AddWithValue("@userid", hdnUserID.Value);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    rpthd.DataSource = dt;
                    rpthd.DataBind();

                    hdmsg.Visible = false;
                    rpthd.Visible = true;
                    check = true;
                }
                else
                {
                    hdmsg.Visible = true;
                    rpthd.Visible = false;
                    check = false;
                }
            }
        }
    }

    protected void rpthd_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label date = e.Item.FindControl("lblDateAdded") as Label;
            date.Text = Convert.ToDateTime(date.Text).ToString("dd MMMM yyyy");
        }
    }

    protected void rpthd_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "openpopup")
        {
            HiddenField id = e.Item.FindControl("hdnID") as HiddenField;
            this.Redirect("Add_worker.aspx?id=" + Request.QueryString["id"].ToString() + "&uid=" + id.Value + "&e=" + hdnUserEmail.Value.Encrypt() + "&p=" + hdnUserPassword.Value.Encrypt());
        }
    }

    protected void lnkRestoreUser_Click(object sender, EventArgs e)
    {
        var ids = (from r in rpthd.Items.Cast<RepeaterItem>()
                   let selected = (r.FindControl("chkselecteditem") as CheckBox).Checked
                   let id = (r.FindControl("hf_ID") as HiddenField).Value
                   where selected
                   select id).ToList();

        foreach (var id in ids)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Update attendence_management SET active= 1 , datefield = '" + DateTime.Now + "' where ID = @ID", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@ID", id);
                    cmd.ExecuteNonQuery();
                }
            }
        }
        LoadUsers(false);
        ScriptManager.RegisterStartupScript(this, GetType(), "", "UpdateMenu(false);confirmBox();", true);
    }
}