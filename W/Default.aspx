﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" EnableEventValidation="false" MasterPageFile="~/W/MasterPage.master" %>

<%@ Register Src="~/WebApplication1/PagerControl.ascx" TagPrefix="uc1" TagName="PagerControl" %>
<%@ Register Src="~/WebApplication1/userprofile.ascx" TagName="workeruserprofile" TagPrefix="uc2" %>

<asp:Content runat="server" ContentPlaceHolderID="head" ID="Content1">

    <script>

        localStorage.setItem("cacheDate", (new Date()).getDate());

        function removeLastChar(value, char) {
            var lastChar = value.slice(-1);
            if (lastChar == char) {
                value = value.slice(0, -1);
            }
            return value;
        }

        function GetParameterValues(param) {
            var url = removeLastChar(window.location.href, "#");
            url = url.slice(url.indexOf('?') + 1).split('&');

            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].slice(url[i].indexOf('=') + 1);
                if (url[i].slice(0, url[i].indexOf('=')) == param) {
                    return urlparam;
                }
            }
        }

        jQuery(document).ready(function ($) {
            var employeelink = GetParameterValues('employeelink');
            if (employeelink == "1") {
                openAdd();
            }
        });


        var mousePos;
        function handleMouseMove(event) {

            //not used anymore
            event = event || window.event; // IE-ism
            mousePos = {
                x: event.clientX,
                y: event.clientY
            };
            //end

            if (localStorage.getItem("cacheDate") < (new Date()).getDate()) {
                localStorage.setItem("cacheDate", (new Date()).getDate());
                location.reload('true');
                openConfirmBox("Please wait while page is loading");
                //refreshDiv();
                //$(".refreshDiv").show();

            }
        }

        function refreshDiv() {
            localStorage.setItem("refreshDiv", "yes");
            jq191('#divRefresh').dialog({
                modal: true,
                closeOnEscape: false,
                //To hide close icon on default
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                },
                buttons: {
                    "Refresh": function () {
                        localStorage.removeItem("refreshDiv");
                        jq191(this).dialog("close");
                        location.reload(true);
                    },
                    "Cancel": function () {
                        localStorage.removeItem("refreshDiv");
                        jq191(this).dialog("close");
                    }
                }
            });
        }

        jQuery(document).ready(function ($) {
            //1800000
            //setInterval(function () {
            //    //alert('auto reloading');
            //    location.reload(true);
            //}, 1800000);



            //Absence Adjustment
            if (GetParameterValues("instanceid") == 'ae5eb924-def7-461e-8097-c503ec893bfc')
                $("#tr_absence").show();
            else
                $("#tr_absence").hide();

            try {
                window.onmousemove = handleMouseMove;
            } catch (e) {
                alert(e.message);
            }

            $('body').click(function (event) {
                if (event.target.nodeName != 'IMG') {
                    $('#menu').hide(200);
                }
            });

            jQuery(document).on('click', '.imgAction', function ($)
            //jQuery('.imgAction').click(function ($) 
            {
                try {
                    //showCursor($(this));
                    var dataparentid = jQuery(this).attr('data-parentid');
                    var dataabid = jQuery(this).attr('data-ab-id');
                    var datauserid = jQuery(this).attr('data-ab-userid');
                    var datadate = jQuery(this).attr('data-ab-date');
                    var dataactive = jQuery(this).attr('data-ab-active');
                    var dataclockin = jQuery(this).attr('data-clock');
                    var datainaddress = jQuery(this).attr('data-inaddress');
                    var datauseractive = jQuery(this).attr('data-useractive');

                    var dataapid = jQuery(this).attr('data-ap-autopresentid');
                    var dataapci = jQuery(this).attr('data-ap-ci');
                    var dataapco = jQuery(this).attr('data-ap-co');
                    var dataapactive = jQuery(this).attr('data-ap-active');

                    //Commented out bcx on page scroll position was not correct 
                    //var pos = mousePos;                   
                    //if (!pos) {
                    //    jQuery('#menu').css('left', pos.x);
                    //    jQuery('#menu').css('top', pos.y);
                    //    jQuery('#menu').css('position', 'absolute');
                    //}
                    //else {
                    //    jQuery('#menu').css('left', pos.x);
                    //    jQuery('#menu').css('top', document.body.scrollHeight);
                    //    jQuery('#menu').css('position', 'absolute');
                    //}

                    //New
                    var pos = jQuery(this).offset();
                    var elPos = { X: pos.left, Y: pos.top };

                    jQuery('#menu').css('left', elPos.X);
                    jQuery('#menu').css('top', elPos.Y);
                    jQuery('#menu').css('position', 'absolute');


                    var dataexid = jQuery(this).attr('data-exday-id');
                    var dataextypeid = jQuery(this).attr('data-exday-typeid');
                    var dataexactive = jQuery(this).attr('data-exday-active');
                    var dataexdate = jQuery(this).attr('data-exday-date');

                    jQuery('#menu').show(200);
                    GenerateMenu(dataparentid, dataabid, datauserid, datadate, dataactive, dataclockin, datainaddress, dataapid, dataapci, dataapco, dataapactive, datauseractive, dataexid, dataextypeid, dataexactive, dataexdate);
                    jQuery('#menu').menu();
                } catch (e) {
                    //alert(e.message);
                    openConfirmBox(e.message);
                }
            });

            jQuery(document).on('click', '.lnkActive', function ($) {
                var userid = $('.lnkActive').attr('data-userid');
                var datauseractive = $('.lnkActive').attr('data-useractive');
                if (confirm('Are you sure?')) {
                    SetUserStatus(userid, datauseractive, true);
                }
            });

            function GenerateMenu(dataparentid, abid, userid, abdate, abactive, dataclockin, datainaddress, dataapid, dataapci, dataapco, dataapactive, datauseractive, dataexid, dataextypeid, dataexactive, dataexdate) {

                var instanceId = GetParameterValues("instanceid");
                $('.lnkRemove').attr('href', "#");
                $('.lnkRemove').attr('data-userid', userid);
                $('.lnkRemove').text('Remove Employee');

                if (dataparentid == "0") {
                    $(".lnkRemove").addClass("disable_a_href");
                    $(".lnkRemove").parent().addClass("notallowed");
                }
                else {
                    $(".lnkRemove").removeClass("disable_a_href");
                    $(".lnkRemove").parent().removeClass("notallowed");
                }

                $('.lnkAbsent').attr('clockin', 'false');
                $('.lnkAbsent').text("Mark as Absent");
                $('.lnkAbsent').attr('data-userid', userid);
                $('.lnkAbsent').attr('data-inaddress', datainaddress);

                //if (dataclockin == 'True') {
                //    $('.lnkClock').attr('clockin', 'false');
                //    $('.lnkClock').text("Clock in");
                //}
                //else {
                //    $('.lnkClock').attr('clockin', 'true');
                //    $('.lnkClock').text("Clock out");
                //}
                //$('.lnkClock').attr('data-userid', userid);
                //$('.lnkClock').attr('data-inaddress', datainaddress);

                //ENABLE Mark as ABSENT FOR ALL EXCEPT FOR 'Absent-Presnt' CASE
                //One who is marked as Absent but also clocked-in
                //if (dataclockin == 'True' || abid != "0")
                if ((dataclockin == 'True' && abid == "-1") || (abid != "0")) {
                    $('.lnkAbsent').parent().hide();
                }
                else {
                    $('.lnkAbsent').parent().show();
                }

            }

            $(function () {
                setInterval(function () { var d1 = $('#hdnCITime').val().split(' '); var d = $('#hdnCITime').val().split(' ')[0].split(':'); if (d[1] == "59") { d[1] = "00"; if (d[0] == "12") d[0] = "01"; else d[0] = parseInt(d[0]) + 1; } else d[1] = parseInt(d[1]) + 1; if (parseInt(d[1]) < 10) d[1] = "0" + d[1]; $('#hdnCITime').val(d[0] + ":" + d[1] + " " + d1[1]) }, 60000);
            });



            //function showDialog(title, message, obj) {
            //    $('#divClockedIn').attr("title", title);
            //    $('#divClockedIn').html(message);
            //    $('#divClockedIn').dialog({
            //        modal: true,
            //        buttons: {
            //            "Yes": function () {
            //                showAbsCommentBox(obj);
            //            },
            //            "No": function () {
            //                $(this).dialog("close");
            //            },
            //            "Cancel": function () {
            //                $(this).dialog("close");
            //            }
            //        }
            //    });
            //}

            //function showAbsCommentBox(obj) {
            //    $('#absComment').dialog({
            //        width: 375,
            //        modal: true,
            //        buttons: {
            //            "Save": function () {
            //                addAbsenceToday(obj, $('#txtAbsComment').val(), true);
            //                $(this).dialog('close');
            //                $('#divClockedIn').dialog("close");
            //            },
            //            "Cancel": function () {
            //                $(this).dialog('close');
            //            }
            //        }
            //    });
            //}
            jQuery('.lnkWorkingHours').click(function ($) {
                var dlg = $('#divWorkingHours').dialog({
                    modal: true,
                    width: 400,
                    hide: {
                        effect: "blind",
                    }

                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;
            });
            jQuery('.btnSaveWH').click(function ($) {
                $('#divWorkingHours').dialog('close');
            });
            jQuery('.btnCanceWH').click(function ($) {
                $('#divWorkingHours').dialog('close');
                return false;
            });
            jQuery('#btnApplyToAll').click(function ($) {
                var CI = $('.txtMonCI').val();
                var CO = $('.txtMonCO').val();
                $('.datetimeCI').val(CI);
                $('.datetimeCO').val(CO);
                return false;
            });

            jQuery('#lnkClock').click(function ($) {
                if ($(this).attr('clockin') == 'false') {
                    Clockin($(this));
                }
                else if ($(this).attr('clockin') == 'true') {
                    Clockout($(this));
                }
            });
        });



        function Workinghour() {
            var dlg = $('#divWorkingHours').dialog({
                modal: true,
                width: 400,
                hide: {
                    effect: "blind",
                }

            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            return false;
        }

        //function openAdd() {
        //    $('#txttitle').val('');

        //    var dlg = $("#divAddWorker").dialog({
        //        show: {
        //        },
        //        top: 80,
        //        left: 330,
        //        modal: true,
        //        hide: {
        //            effect: "blind",
        //        }

        //    });
        //    $('.txtEmail').focus();
        //    dlg.parent().appendTo($("form:first"));
        //    dlg.parent().css("z-index", "1000");
        //    dlg.parent().css("width", "455px");
        //    dlg.parent().css("top", "50px");
        //}

        function alreadyExists() {
            var dlg = $("#divError").dialog({
                show: {
                },
                modal: true,
                hide: {
                    effect: "blind",
                },
                buttons: {
                    "Ok": function () {
                        $(this).dialog('close');
                    }
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            dlg.parent().css("width", "455px");
        }

        function ren() {
            $('#lblrerror').text("");
            $('#txtrtitle').val('');
            var dlg = $("#divrenamedepartment").dialog({
                show: {


                    //effect: "blind",
                    //duration: 1000
                },
                modal: true,
                hide: {


                    effect: "blind",
                    //duration: 1000
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            dlg.parent().css("width", "247px");
            return false;
        }

        jQuery(document).on('click', '#btnsendcontrorinfo', function ($) {
            if ($('#txtEmail').val() == '') {
                $('.txtEmail').attr('placeholder', 'Please provide email address');
                return false;
            }
            $('#divAddWorker').hide(200);
        });



        function settings() {
            var dlg = $("#categorySettings").dialog({
                show: {
                    //effect: "blind",
                    //duration: 1000
                },
                modal: true,
                hide: {


                    effect: "blind",
                    //duration: 1000
                }

            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            return false;
        }

        function removeConfirmation(control_uniqueid, name_deleted) {
            $("#b_name_to_be_deleted").html(name_deleted);
            var r;
            $("input[id^='Rptrattendence_chkselect']").each(function () {
                if ($(this).is(":checked")) {
                    r = $(this).is(":checked");
                }
            });
            if (r) {
                var dlg = $("#removemail").dialog({
                    resizable: false,
                    height: 200,
                    width: 400,
                    modal: true,
                    autoOpen: true,
                    buttons: {
                        "Yes": function () {
                            $(this).dialog("close");
                            __doPostBack('lbtn_del', 'OnClick');
                            //__doPostBack(control_uniqueid, 'OnClick');
                        },
                        "No": function () {
                            $(this).dialog("close");
                            return false;
                        }
                    }
                });
            } else {
                alert("No item(s) selected to delete.");
            }
            return false;
        }

        function setvalue(id) {
            var x = document.getElementById("hftreenodevalue").value = id;
            return false;
        }


        //------ script for check all
        jQuery(function ($) {
            setInterval(function () {
                $('span[data-date]').each(function (index) {
                    //alert($(this).text());
                    var $target = '#' + $(this).attr('id');
                    var signintime = $($target).attr('data-date');
                    getDateTimeDifference(signintime, $target, new Date(new Date().toLocaleDateString() + ' ' + $('#hdnCITime').val()));
                });
            }, 1000);


            $('.cb_select_all input').change(function () {
                if (this.checked)
                    $($(this).parents("table")[0]).find(".cb_select input").each(function () {
                        this.checked = true;
                    });
                else {
                    $($(this).parents("table")[0]).find(".cb_select input").each(function () {
                        this.checked = false;
                    });
                }
            });

            //$('.lnkAdd_worker1').click(function () {
            //    openAdd();
            //});

            $("#btnupdate").click(function () {
                if ($('#txtrtitle').val() == "") {
                    $('#lblrerror').text('Required');
                    return false;
                }
            });

            $("#btnusercontrol").click(function () {
                var dlg = $("#divworkeradd").dialog({
                    height: 450,
                    width: 800,
                    show: {
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                var iframe = window.parent.document.getElementById("MainContent_ifapp");
                if (iframe != null)
                    iframe.height = "600px";
                $("#divAddWorker").dialog("close");
                return false;
            });

            $('#btncancel').click(function () {

            });


            //--- date picker
            //$("#txtstartdate").datepicker();
            //$("#txtenddate").datepicker();


            //--- empty search click funtion
            $('#btnserachdate').click(function () {
                if ($('#txtsearch').val() == "") {
                    return false;
                }
            });

            //--------- script for add department

            $("#lnkAdd_department").click(function () {
                $('#txttitle').val('');
                var dlg = $("#divadddepartment").dialog({
                    show: {



                        //effect: "blind",
                        //duration: 1000
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                        //duration: 1000
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;
                //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
            });

            $("#lnkAdd_department1").click(function () {
                $('#txttitle').val('');
                var dlg = $("#divadddepartment").dialog({
                    show: {



                        //effect: "blind",
                        //duration: 1000
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                        //duration: 1000
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;
                //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
            });

            //---- for show hide searchbox
            //for toogle div search
            //            $('#lnksearch').click(function (e) {
            //                e.preventDefault();
            //                $('#searchdate').toggle();
            //                $('#divfuntions').toggle();
            //                $('#categorySettings').hide();
            //                resizeiframepage();
            //                $('#txtsearch').focus();
            //                return false;
            //            });
            $('#btn_cancel').click(function (e) {
                e.preventDefault();
                $('#searchdate').toggle();
                $('#divfuntions').toggle();
                document.getElementById("txtsearch").value = "";
                resizeiframepage();
                return false;
            });

            $("#lnkcategorySettings").click(function (e) {
                e.preventDefault();
                $('#categorySettings').toggle();
                $('#searchdate').hide();
                try {
                    resizeiframepage();
                } catch (e) {

                }
                return false;
            });

            $("#clsbtn").click(function (e) {
                e.preventDefault();
                $('#categorySettings').hide();
                try {
                    resizeiframepage();
                } catch (e) {

                }
                return false;
            });

            //----for move item
            $("#lnkmove").click(function () {
                var r = false;
                $('#lblerror').text("");
                $("input[id^='Rptrattendence_chkselect']").each(function () {
                    if ($(this).is(":checked")) {
                        r = $(this).is(":checked");
                    }
                });
                if (r) {
                    var dlg = $("#divtreeview").dialog({
                        show: {
                            height: 100
                            //effect: "blind",
                            //duration: 1000
                        },
                        modal: true,
                        hide: {
                            effect: "blind",
                            //duration: 1000
                        }
                    });
                    dlg.parent().appendTo($("form:first"));
                    dlg.parent().css("z-index", "1000");
                } else {
                    alert("No item(s) selected to move.");
                }
                return false;
                //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
            });


            jQuery(document).on('click', '.lnkRemove', function ($) {
                var userid = jQuery(this).attr('data-userid');
                var obj = jQuery("[id$=lblcustommsg]").text("Removing an employee will move it under 'Former Employees' Tab and he/she will not be able to clock-in/out anymore. The history will be maintained.\nAre you sure you want to remove this employee?")
                obj.html(obj.html().replace(/\n/g, '<br/><br/>'));
                var dlg = jq191("#divCustomBox").dialog({
                    resizable: false,
                    width: 400,
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            jq191(this).dialog("close");
                            RemoveUser(userid);
                        },
                        "No": function () {
                            jq191(this).dialog("close");
                            return false;
                        }
                    }
                });
                dlg.parent().appendTo(jQuery("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });



            //jQuery(document).on('click', '.lnkAbsent', function ($) {
            //    var userid = jQuery(this).attr('data-userid');
            //    jQuery("[id$=lblcustommsg]").text("Are you sure you want to mark Absent?");
            //    var dlg = jq191("#divCustomBox").dialog({
            //        resizable: false,
            //        modal: true,
            //        buttons: {
            //            "Yes": function () {
            //                jq191(this).dialog("close");
            //                markAbsent(userid);
            //            },
            //            "No": function () {
            //                jq191(this).dialog("close");
            //                return false;
            //            }
            //        }
            //    });
            //    dlg.parent().appendTo(jQuery("form:first"));
            //    dlg.parent().css("z-index", "1000");

            //    return false;
            //});


            jQuery(document).on('click', '.lnkAbsent', function ($) {
                var userid = jQuery(this).attr('data-userid');
                jQuery("[id$=lblcustommsg]").text("Are you sure you want to mark Absent?");
                var dlg = jq191("#divCustomBox").dialog({
                    resizable: false,
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            jq191(this).dialog("close");
                            showAbsCommentBox(userid);
                        },
                        "No": function () {
                            jq191(this).dialog("close");
                            return false;
                        }
                    }
                });
                dlg.parent().appendTo(jQuery("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });



            jQuery(document).on('click', '.updateStatus', function ($) {
                // event.preventDefault();
                var $obj = jQuery(this);
                var dataRow = $obj.closest('tr').find('.imgAction');
                var userid = dataRow.attr("data-ab-userid");
                var clockin = dataRow.attr("data-clock");
                var username = $obj.closest('tr').find('.lnktitle').html();

                jQuery("[id$=hdn_stuserid]").val(userid);
                jQuery("[id$=hdn_stclockin]").val(clockin);

                if (clockin == "True")
                    jQuery("#st_text").text("Clock-OUT, " + username);
                else
                    jQuery("#st_text").text("Clock-IN, " + username);

                var dlg = jq191('#divConfirmBox').dialog({
                    modal: true
                });
                dlg.parent().appendTo(jQuery("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });

        });


        function showAbsCommentBox(userid) {
            jq191('#absComment').dialog({
                width: 375,
                modal: true,
                buttons: {
                    "Save": function () {
                        if ($('#txtAbsComment').val() == "") {
                            $("#abs_reason").css("display", "block");
                        }
                        else {
                            $("#abs_reason").css("display", "none");
                            markAbsent(userid, $('#txtAbsComment').val(), $('[id$=chkIsAdjustment]').is(":checked"));
                            $('#txtAbsComment').val("");
                            jq191(this).dialog('close');
                        }
                    },
                    "Cancel": function () {
                        $('#txtAbsComment').val("");
                        jq191(this).dialog('close');
                    }
                }
            });
        }

        function markAbsent(userid, reason, isadjustment) {
            ActivateAlertDiv('', 'AlertDiv');
            jQuery.ajax({
                url: "Default.aspx/MarkAbsent",
                type: "POST",
                data: "{'userid':'" + userid + "','reason':'" + reason + "','isadjustment':'" + isadjustment + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: function () {

                    //jQuery("[id$=lblcustommsg]").text("Employee marked Absent successfully!");
                    //var dlg = jq191("#divCustomBox").dialog({
                    //    resizable: false,
                    //    modal: true,
                    //    buttons: {
                    //        "Close": function () {
                    //            jq191(this).dialog("close");
                    //            location.reload(window.location.href);
                    //        }
                    //    }
                    //});
                    //dlg.parent().appendTo(jQuery("form:first"));
                    //dlg.parent().css("z-index", "1000");

                    //Master page function
                    openConfirmBox("Employee marked Absent successfully!");
                    location.reload(true);
                    //ActivateAlertDiv('none', 'AlertDiv');
                },
                error: function (response) {
                    ActivateAlertDiv('none', 'AlertDiv');
                    openConfirmBox('Sorry an error has occurred. Please try again.');
                    // alert('Sorry an error has occurred. Please try again.');
                }
            });
            return true;
        }

        function RemoveUser(userid) {
            var parentID = jQuery('[id$=hfparentid]').val();
            jQuery.ajax({
                url: "Default.aspx/RemoveUsers",
                type: "POST",
                data: "{'userid':'" + userid + "','parentID':'" + parentID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: function () {
                    UpdateMenu(false);
                    //jQuery("[id$=lblcustommsg]").text("Employee removed successfully!");
                    //var dlg = jq191("#divCustomBox").dialog({
                    //    resizable: false,
                    //    modal: true,
                    //    buttons: {
                    //        "Close": function () {
                    //            jq191(this).dialog("close");
                    //            location.reload(window.location.href);
                    //        }
                    //    }
                    //});
                    //dlg.parent().appendTo(jQuery("form:first"));
                    //dlg.parent().css("z-index", "1000");

                    //master page function
                    openConfirmBox("Employee removed successfully!");
                },
                error: function (response) {
                    //alert('Sorry an error has occurred. Please try again.');
                    openConfirmBox('Sorry an error has occurred. Please try again.');
                }
            });
            return true;

        }

        //function RemoveUser() {

        //    var check = confirm('Are you sure you want to remove this employee?');

        //    if (check) {
        //        var userid = $(this).attr('data-ab-userid');
        //        $.ajax({
        //            url: "/W/Default.aspx/RemoveUsers",
        //            type: "POST",
        //            data: "{'userid':'" + userid + "'}",
        //            dataType: "json",
        //            contentType: "application/json; charset=utf-8",
        //            async: true,
        //            success: function () {
        //                // alert('Employee added successfully!');
        //                location.reload(window.location.href);
        //            },
        //            error: function (response) {
        //                alert('Sorry an error has occurred. Please try again.');
        //            }
        //        });
        //        return true;
        //    }
        //    else
        //        return false;

        //}


        function confirmBox() {
            $('#divConfirmBox').dialog({
                modal: true,
                buttons: {
                    "Yes": function () {
                        $(this).dialog("close");
                        return true;
                    },
                    "No": function () {
                        $(this).dialog("close");
                    }
                }
            });

        }

        function ActivateAlertDiv(visstring, elem) {
            var adiv = $get(elem);

            var objLeft = (document.body.clientWidth) / 2 - 50;
            var objTop = (document.body.clientHeight) / 2 - 20;

            objLeft = objLeft + document.body.scrollLeft;
            objLeft = objLeft + "px";
            objTop = objTop + document.body.scrollTop;
            objTop = objTop + "px";

            adiv.style.display = visstring;
            adiv.style.position = "absolute";
            adiv.style.left = objLeft;
            adiv.style.top = objTop;
        }


        function resizeiframepage() {
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
            if (innerDoc.body.offsetHeight) {
                iframe.height = innerDoc.body.offsetHeight + 32 + "px";
            } else if (iframe.Document && iframe.Document.body.scrollHeight) {
                iframe.style.height = iframe.Document.body.scrollHeight + "px";
            }
        }

        function clearCache() {
            if (localStorage.getItem("cacheDate") != (new Date()).getDate())
                location.reload(true);
        }

        HTMLInputElement.prototype.startsWith = function () { return false; };


    </script>
    <!--<link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css"
            media="screen" type="text/css" rel="stylesheet" />-->


    <asp:HiddenField ID="hdnUserID" runat="server" />
    <asp:HiddenField ID="hdnUserEmail" runat="server" />
    <asp:HiddenField ID="hdnUserPassword" runat="server" />
    <asp:HiddenField ID="hdnAdminEmail" runat="server" />
    <asp:HiddenField ID="hdnAdminName" runat="server" />
    <asp:HiddenField ID="hfparentid" runat="server" />
    <asp:ScriptManager ID="scmang" runat="server" AsyncPostBackTimeout="360000">
    </asp:ScriptManager>

    <!--5 mints timer-->
    <asp:Timer ID="timerForRepeater" runat="server" OnTick="timerForRepeater_Tick" Interval="300000" Enabled="true"></asp:Timer>

    <asp:Label ID="lblDate" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lbl" runat="server" Visible="false"></asp:Label>
    <table class="table-responsive">
        <tr>
            <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12 btns-txt helppopup hide" style="padding-bottom: 10px;">
                <div class="alert alert-info">
                    <span style="font-size: 16px; font-weight: bold;"><i class="fa fa-fw fa-info-circle" style="color: #2273a5;"></i>
                        <asp:Label runat="server" ID="lblhead" Text="Help us Improve" CssClass="alert-heading"></asp:Label>
                    </span>
                    <br />
                    <asp:Label runat="server" ID="Label4">We are continually striving to improve Time & Attendance by adding new features & by resolving any issues that you might face. But we need your feedback & we guarantee fast response.
                        <br /> 
                        <a href="#" class="freesupport">Report a problem</a>&nbsp;|&nbsp; <a href="#" class="freesupport">Request new feature</a></asp:Label>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12 btns-txt refreshDiv hide">
                <div class="alert alert-danger">
                    <span style="font-size: 16px; font-weight: bold;"><i class="fa fa-fw fa-info-circle" style="color: #2273a5;"></i>
                        <asp:Label runat="server" ID="lblmsge" Text="Please refresh page to get latest grid!" CssClass="alert-heading"></asp:Label>
                    </span>
                </div>
            </div>
        </tr>
        <tr>
            <!--User's Request Notification tr-->
            <div id="tr_req" runat="server">
                <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12 btns-txt">
                    <div class="alert alert-danger">
                        <i class="fa fa-fw fa-exclamation-circle" style="color: red;"></i>
                        <asp:Label runat="server" ID="lblReqNotify">You have some pending requests, please take an action <a href="#" runat="server" id="requestlink">here</a></asp:Label>
                    </div>
                </div>
            </div>
        </tr>
        <%--  <tr>
            <div class="col-lg-12 btns-txt" style="display: none;">
                <asp:Button runat="server" ID="btnSignin" CssClass="btn btn-primary bg-in" Text="Clock-in" OnClick="btnSignin_Click" />
                <asp:Button runat="server" ID="btnSignout" CssClass="btn btn-danger bg-out" Text="Clock-out" OnClick="btnSignout_Click" />
                <asp:Label runat="server" CssClass="txt-block-centr" ID="lblLastinTime"></asp:Label>
            </div>
        </tr>--%>
        <%--  <tr>
            <td style="display: none;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Timer ID="Timer1" runat="server" Interval="2000" OnTick="Timer1_Tick">
                        </asp:Timer>
                        <asp:Label ID="lblTimer" runat="server" Text="" Font-Bold="true" Font-Size="Large"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>--%>
        <tr>
            <td>
                <div class="space" id="main" runat="server">
                    <%--<asp:LinkButton runat="server" ID="lnkMyAttendance" CssClass="table-top-txt" OnClick="lnkMyAttendance_Click" Text="My Attendance" Visible="false"></asp:LinkButton>--%>
                    <%--   <ol class="breadcrumb bread-list">
                        <li class="breadcrumb-item">
                            <a href="#" id="lnkAdd_worker1" class="lnkAdd_worker1">
                                <i class="fa fa-fw fa-user-plus"></i>
                                <span class="nav-link-text">Add Employee</span>
                            </a>
                        </li>
                       <li class="breadcrumb-item">
                            <asp:LinkButton runat="server" ID="lnkRemoveUser" Text="Remove Employee(s)" OnClick="lnkRemoveUser_Click" OnClientClick="return RemoveUser();"></asp:LinkButton>

                        </li>
                    </ol>--%>

                    <%--                    <span title="Refresh grid to view latest updates" onclick="javascript:document.location.reload(true);"><i class="fa fa-fw fa-refresh" style="color: forestgreen; float: right; cursor: pointer; font-size: 16px;"></i></span>--%>

                    <div>
                        <asp:UpdatePanel ID="updatePanel" runat="server">
                            <ContentTemplate>
                                <div class="legend-block">
                                    <asp:Label runat="server" ID="Label5">Absent:</asp:Label>
                                    <div class="color-block pink"></div>
                                    &nbsp;
                                    <asp:Label runat="server" ID="Label6">Present:</asp:Label>
                                    <div class="color-block none"></div>
                                    &nbsp;
                                    <asp:Label runat="server" ID="Label7">Clocked-out:</asp:Label>
                                    <div class="color-block yellow"></div>
                                    &nbsp;
                                    <asp:Label runat="server" ID="Label8">Myself:</asp:Label>
                                    <div class="color-block green"></div>
                                </div>
                                <br />
                                <div style="font-weight: bold;">
                                    <asp:Label runat="server" ID="lblTotalAbsent">Absent:
                                        <asp:LinkButton runat="server" ID="lnkTotalAbsent" Enabled="false">0</asp:LinkButton></asp:Label>
                                    &nbsp;
                                    <asp:Label runat="server" ID="Label1">Present:
                                         <asp:LinkButton runat="server" ID="lnkTotalPresent" Enabled="false">0</asp:LinkButton></asp:Label>
                                    &nbsp;
                                    <asp:Label runat="server" ID="Label2">Unmarked:
                                         <asp:LinkButton runat="server" ID="lnkTotalUnmarked" Enabled="false">0</asp:LinkButton></asp:Label>
                                    &nbsp;
                                    <asp:Label runat="server" ID="Label3">Total:
                                         <asp:LinkButton runat="server" ID="lnkTotal" Enabled="false">0</asp:LinkButton></asp:Label>
                                </div>
                                <br />
                                <asp:Repeater ID="Rptrattendence" runat="server"
                                    OnItemDataBound="Rprteattendenceitemdatabound">
                                    <HeaderTemplate>
                                        <table id="tblconctract table-responsive" class="tbl-grd-last-clmn">
                                            <tr class="smallGridHead">
                                                <%--<td>
                                            <asp:CheckBox ID="chkselectall" runat="server" CssClass="cb_select_all" />
                                        </td>--%>
                                                <td>Title
                                                </td>
                                                <%-- <td runat="server" id="tdh_manage">Managed By
                                        </td>--%>
                                                <td>Date
                                                </td>
                                                <td>Clockin
                                                </td>
                                                <td>Status
                                                </td>
                                                <td>Worked
                                                </td>
                                                <%--  <td></td>--%>
                                                <td></td>
                                                <td></td>
                                                <td class="blank"></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class="smallGrid" id="trdata" runat="server">
                                            <%-- <td>
                                        <asp:CheckBox ID="chkselecteditem" runat="server" CssClass="cb_select" />
                                       
                                    </td>--%>
                                            <td>
                                                <asp:HiddenField runat="server" ID="hf_ID" Value='<%# Eval("ID") %>' />
                                                <asp:HiddenField runat="server" ID="hf_parentid" Value='<%# Eval("parentid") %>' />
                                                <asp:HiddenField runat="server" ID="hf_status" />
                                                <asp:HiddenField runat="server" ID="hf_lastactivity" Value='<%# Eval("lastactivity") %>' />
                                                <asp:LinkButton ID="lnktitle" CommandArgument='<%# Eval("ID") %>' CommandName="openpopup"
                                                    runat="server" Style="" Text='<%# Eval("Title") %>' CssClass="lnktitle"></asp:LinkButton>
                                            </td>
                                            <%-- <td  runat="server" id="tdr_manage">
                                        <asp:Label runat="server" ID="lblmanaged" Text='<%# Eval("isManaged") %>'></asp:Label>
                                    </td>--%>
                                            <td>
                                                <asp:Label runat="server" ID="lbltimein" Text='<%# Eval("lastactivity") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lbltimeforin" Text='<%# Eval("lastin") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblworkstatus" Text='<%# Eval("status") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblworkhours"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lbltimeforout"></asp:Label>
                                                <asp:LinkButton ID="lnkClockin" CommandArgument='<%# Eval("ID") %>' CommandName="clockin"
                                                    runat="server" Text="" ForeColor="Green" CssClass="updateStatus"></asp:LinkButton>
                                                <%-- &nbsp;&nbsp;
                                            <asp:LinkButton ID="lnkAbsent" CommandArgument='<%# Eval("ID") %>' CommandName="status"
                                                runat="server" Text="" ForeColor="Red"></asp:LinkButton>--%>
                                            </td>
                                            <%--<td>   
                                        <image src="/images/controls/icon_info.png" style="padding-right: 5px; float: left;"
                                            runat="server" id="imgInfo" title="" tooltip="" alt="Info" class="imgInfo"></image>
                                    </td>--%>
                                            <td>
                                                <image src="../images/controls/gear.png" style="float: right" runat="server"
                                                    id="imgAction" title="" tooltip="" alt="Menu" class="imgAction"></image>
                                            </td>
                                            <td class="blank">
                                                <asp:HiddenField runat="server" ID="hdnIsVerified" Value='<%# Eval("IP") %>' />
                                                <asp:Label runat="server" ID="error" Font-Bold="true" ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="timerForRepeater" EventName="Tick" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:Button ID="Button1" runat="server" Text="Send Email Test" Visible="false" OnClick="Button1_Click" />
                        <asp:Label ID="lblemptygrid" CssClass="table-full" ForeColor="red" Text="No Record Found" Visible="False"
                            runat="server"></asp:Label>
                        <br />
                        <uc1:PagerControl runat="server" ID="pg_Control" PageSize="30" />
                    </div>

                </div>
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td></td>
        </tr>
    </table>
    <div id="divworkeradd" style="display: none; width: 600px">
        <uc2:workeruserprofile ID="workeruserprofile" runat="server" />
    </div>
    <div id="divAddWorker1" title="Add User" style="width: 440px; display: none;">
        <asp:Panel ID="pnlAddDept" DefaultButton="btnsendcontrorinfo" runat="server">
            <table class="profile" style="margin-top: 5px;">
                <tr>
                    <%-- <td colspan="2">When you fill out the form below, Avaima will send them an email and ask the user
                        to sign up just like how you did when you had signed up on Avaima. They can then
                        go to avaima.com, sign-in and clock-in/out and their data will automatically show
                        in the grid below as a new user. Each user will have to go to avaima.com and sign
                        in to clock-in/out using their respective accounts through which they sign up.<br />
                    </td>--%>
                    <td colspan="2"><b>Every employee manages his/her own clock-in/out.</b><br />
                        <br />
                </tr>
                <tr>
                    <td class="captionauto">First Name:
                    </td>
                    <td>
                        <asp:TextBox ID="txtFirstName" runat="server" EnableViewState="false" CssClass="standardField txtFirstName1 required"
                            placeholder="First Name..."></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="captionauto">Last Name:
                    </td>
                    <td>
                        <asp:TextBox ID="txtLastName" runat="server" CssClass="standardField txtLastName1 required"
                            EnableViewState="false" placeholder="Last Name..."></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="captionauto">Email:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtEmail" placeholder="Email Address" CssClass="standardField txtEmail1"></asp:TextBox><asp:RequiredFieldValidator
                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="Required"
                            ForeColor="Red" ValidationGroup="gg" Display="Dynamic">Required</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtEmail"
                            Display="Dynamic" ErrorMessage="Invalid Email" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ValidationGroup="gg"></asp:RegularExpressionValidator>
                        <asp:HiddenField runat="server" ID="id" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: right">
                        <asp:Button runat="server" ID="btnsendcontrorinfo" Text="Send Email" CssClass="ui-state-default btnsendcontrorinfo"
                            ValidationGroup="gg" OnClick="BtnsendcontrorinfoClick" ClientIDMode="Static" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Avaima collects and updates information from the User
                    </td>
                </tr>
                <%--<tr>
                        <td colspan="2">
                            <asp:LinkButton ID="btnusercontrol" Text="I want to provide employee information myself" runat="server" Style="color: blue"></asp:LinkButton>
                        </td>
                    </tr>--%>
            </table>
        </asp:Panel>
    </div>
    <div id="divError" class="divError" title="Error" style="display: none">
        <p style="padding: 10px">
            <b>ERROR:</b> The user already exists
        </p>
        <p style="padding: 10px">
            Path: Root /
        </p>
    </div>

    <ul id="menu" style="display: none;">
        <li><a class="lnkAbsent menuitem" id="lnkAbsent" href="#"></a></li>
        <li><a class="lnkRemove menuitem" id="lnkRemove" href="#"></a></li>
        <%--<li><a class="lnkEdit menuitem" id="lnkEdit" href="#"></a></li>
       <li><a href="#" class="lnkClock menuitem" id="lnkClock"></a></li>
        <li><a href="#" class="lnkAttendance menuitem" id="lnkAttendance"></a></li>--%>
        <%--<li><a href="#" class="lnkActive menuitem hide" id="lnkActive"></a></li>--%>
        <%--<li><a href="#" class="lnkPW menuitem hide" id="lnkPW" style="width: 140px"></a>--%>
        <%--</li>--%>
    </ul>

    <div id="main2" title="Welcome!" class="main2" runat="server" style="margin-left: 4px; display: none">
        <div class="breadCrumb" style="margin-left: 4px;">
            Welcome to Time & Attendance Management
        </div>
        <table class="" id="tblproducts" cellpadding="0" cellspacing="2" style="margin-left: 10px;">
            <tbody>
                <tr id="MainContent_tr_dmenu">
                    <td colspan="9">
                        <div>
                            <p>
                                Start by Adding Employee or Department. You can create as many employees and organize
                                them in department/sub department as you want.
                            </p>
                            <ul style="margin-left: 18px; padding: 11px">
                                <li>
                                    <a href="#" id="lnkAdd_worker11" class="lnkAdd_worker11" style="color: red">Add Employee</a>
                                    OR
                                </li>
                                <li>
                                    <asp:LinkButton ID="lnkClockin" Text="Start by recording your clock-in/out time"
                                        runat="server"></asp:LinkButton>
                                </li>
                            </ul>
                            <p>
                                For learning more about Time & Attendance please go to <a href="About.aspx">About</a>
                                or <a href="appinstruction.aspx">Help</a>.
                            </p>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div id="divRefresh" title="Attention!" style="display: none">
        <p>
            <span style="font-size: 13px; font-weight: bold;"><i class="fa fa-fw fa-info-circle" style="color: #2273a5;"></i>
                <asp:Label runat="server" ID="Label9" Text="Please refresh page to get latest grid!" CssClass="alert-heading"></asp:Label>
            </span>
        </p>
    </div>
    <div id="divClockedIn" title="Clocked In Successfully!" style="display: none">
        <p>
            You have successfully clocked in.
        </p>
        <br />
        <p>
            Please verify your location by clicking the link in your verification email sent
            to
            <asp:Label runat="server" ID="lblInEmail"></asp:Label>
        </p>
    </div>
    <div id="absComment" class="hide" title="Comment">
        <table class="table-responsive">
            <tr>
                <td>
                    <textarea id="txtAbsComment" class="txtAbsComment" style="width: 333px; height: 57px;"
                        placeholder="Please add your comments here..."></textarea>
                </td>
            </tr>
            <tr id="tr_absence">
                <td>
                    <asp:CheckBox runat="server" ID="chkIsAdjustment" ClientIDMode="Static" /> Mark as Adjustment                      
                </td>
            </tr>
            <tr>
                <td>
                    <label style="color: red; display: none;" id="abs_reason">Please provide a reason</label>
                </td>
            </tr>
        </table>
    </div>

    <div id="divWorkingHours" title="Working hours" style="display: none">
        <table class="profile" style="margin-top: 10px; padding: 0 13px">
            <tr>
                <td colspan="3" style="text-align: center">
                    <p>
                        Provide your working hours here
                    </p>
                </td>
            </tr>
            <tr>
                <th></th>
                <th>Clock in
                </th>
                <th>Clock out
                </th>
                <th>Active
                </th>
            </tr>
            <tr>
                <td class="captionauto">Monday
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hdnMon" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnMonID" Value="0" />
                    <asp:TextBox ID="txtMonCI" runat="server" CssClass="txtMonCI datetime datetimeCI"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtMonCO" runat="server" CssClass="txtMonCO datetime datetimeCO"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkMon" runat="server" />
                </td>
                <td>
                    <img id="btnApplyToAll" class="btnApplyToAll" src="" alt="Apply to all" style="float: right; margin-left: 5px; color: blue; cursor: pointer" />
                </td>
            </tr>
            <tr class="trAPC1">
                <td class="captionauto">Tuesday
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hdnTue" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnTueID" Value="0" />
                    <asp:TextBox ID="txtTuesCI" runat="server" CssClass="txtTuesCI datetime datetimeCI"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTuesCO" runat="server" CssClass="txtTuesCO datetime datetimeCO"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkTue" runat="server" />
                </td>
            </tr>
            <tr class="trAPC1">
                <td class="captionauto">Wednesday
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hdnWed" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnWedID" Value="0" />
                    <asp:TextBox ID="txtWedCI" runat="server" CssClass="txtWedCI datetime datetimeCI"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtWedCO" runat="server" CssClass="txtWedCO datetime datetimeCO"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkWed" runat="server" />
                </td>
            </tr>
            <tr class="trAPC1">
                <td class="captionauto">Thursday
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hdnThu" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnThuID" Value="0" />
                    <asp:TextBox ID="txtThuCI" runat="server" CssClass="txtThuCI datetime datetimeCI"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtThuCO" runat="server" CssClass="txtThuCO datetime datetimeCO"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkThur" runat="server" />
                </td>
            </tr>
            <tr class="trAPC1">
                <td class="captionauto">Friday
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hdnFri" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnFriID" Value="0" />
                    <asp:TextBox ID="txtFriCI" runat="server" CssClass="txtFriCI datetime datetimeCI"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtFriCO" runat="server" CssClass="txtFriCO datetime datetimeCO"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkFri" runat="server" />
                </td>
            </tr>
            <tr class="trAPC1">
                <td class="captionauto">Saturday
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hdnSat" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnSatID" Value="0" />
                    <asp:TextBox ID="txtSatCI" runat="server" CssClass="txtSatCI datetime datetimeCI"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtSatCO" runat="server" CssClass="txtSatCO datetime datetimeCO"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkSat" runat="server" />
                </td>
            </tr>
            <tr class="trAPC1">
                <td class="captionauto">Sunday
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hdnSun" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnSunID" Value="0" />
                    <asp:TextBox ID="txtSunCI" runat="server" CssClass="txtSunCI datetime datetimeCI"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtSunCO" runat="server" CssClass="txtSunCO datetime datetimeCO"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkSun" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="captionauto">Autopresent:
                </td>
                <td>
                    <asp:CheckBox ID="chkAutoPresent" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: right">
                    <asp:Button ID="btnSaveWH" CssClass="btnSaveWH ui-state-default raisesevent" runat="server"
                        Text="Save" OnClick="btnSaveWH_Click" />
                    <asp:Button ID="btnCanceWH" CssClass="btnCanceWH ui-state-default" runat="server"
                        Text="Cancel" />
                </td>
            </tr>
        </table>
    </div>

    <div id="divConfirmBox" title="Confirm" style="display: none">
        <asp:HiddenField runat="server" ID="hdn_stuserid" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hdn_stclockin" ClientIDMode="Static" />
        <p>
            <asp:Label runat="server" ID="lblconfirmmsg">Are you sure you want to <span id="st_text"></span>?</asp:Label>
        </p>
        <div class="divDialogButton">
            <asp:Button ID="btnUpdateStatus" runat="server" Text="Ok" CssClass="ui-state-default btnUpdateStatus raisesevent" OnClick="btnUpdateStatus_Click" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="ui-state-default" OnClientClick="event.preventDefault(); jq191('#divConfirmBox').dialog('close');" />
        </div>
    </div>

    <div id="divCustomBox" title="Confirm" style="display: none">
        <p>
            <asp:Label runat="server" ID="lblcustommsg" Text=""></asp:Label>
        </p>
    </div>

    <div id="AlertDiv" style="display: none; height: 100%; width: 100%; min-width: 100%; min-height: 100%; z-index: 999999 !important;" class="overlay">
        <div class="overlay-content">
            <img src="../images/loader.gif" alt='' height="70px" width="70px" />
        </div>
    </div>

    <asp:HiddenField ID="hdnCITime" runat="server" ClientIDMode="Static" />
    <asp:LinkButton runat="server" Text="Define working hours" ID="lnkWorkingHours" CssClass="lnkWorkingHours"
        ClientIDMode="Static" Style="display: none;"></asp:LinkButton>

</asp:Content>
