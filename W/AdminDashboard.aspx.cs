﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class W_AdminDashboard : System.Web.UI.Page
{
    DataAccessLayer dal = new DataAccessLayer(false);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            BindData();
    }

    public void BindData()
    {
        Hashtable ht = new Hashtable();
        ht["@action"] = "get";
        DataTable dt = dal.GetDataTable("sp_EmailNotifications", ht);

        if (dt.Rows.Count > 0)
        {
            hdmsg.Visible = false;

            rpt_notifications.DataSource = dt;
            rpt_notifications.DataBind();
            rpt_notifications.Visible = true;
        }
        else
        {
            rpt_notifications.Visible = false;
            hdmsg.Visible = true;
        }
    }

    public void ClearFields()
    {
        name.Text = "";
        desc.Text = "";
    }

    protected void Add_Click(object sender, EventArgs e)
    {
        Hashtable ht = new Hashtable();
        ht["@action"] = "insert";
        ht["@Name"] = name.Text;
        ht["@Description"] = desc.Text;
        bool res = dal.ExecuteNonQuery("sp_EmailNotifications", ht);
        ClearFields();
        BindData();
    }


    protected void chk_active_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox active = (CheckBox)sender;
        RepeaterItem item = (RepeaterItem)active.NamingContainer;
        Label name = (Label)item.FindControl("lblName");
        Label desc = (Label)item.FindControl("lblDesc");

        Hashtable ht = new Hashtable();

        ht["@action"] = "update";
        ht["@ID"] = active.Attributes["data-id"].ToInt32();
        ht["@Name"] = name.Text;
        ht["@Active"] = active.Checked;
        ht["@Description"] = desc.Text;

        bool res = dal.ExecuteNonQuery("sp_EmailNotifications", ht);

        BindData();
    }

    protected void rpt_notifications_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "delete")
        {
            int Id = e.CommandArgument.ToInt32();
            Hashtable ht = new Hashtable();

            ht["@action"] = "delete";
            ht["@ID"] = Id;

            bool res = dal.ExecuteNonQuery("sp_EmailNotifications", ht);

            BindData();
        }
    }
}