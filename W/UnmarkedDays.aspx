﻿<%@ Page Title="" Language="C#" MasterPageFile="~/W/MasterPage.master" AutoEventWireup="true" CodeFile="UnmarkedDays.aspx.cs" Inherits="W_UnmarkedDays" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>

        jQuery(function ($) {
            $('.cb_select_all input').change(function () {
                if (this.checked)
                    $($(this).parents("table")[0]).find(".cb_select input").each(function () {
                        this.checked = true;
                    });
                else {
                    $($(this).parents("table")[0]).find(".cb_select input").each(function () {
                        this.checked = false;
                    });
                }
            });
        });

        function ConfirmBox() {
            var i = 0;
            $('.cb_select').each(function () {
                if ($(this).find('[name$=chkselecteditem]').is(':checked'))
                { i++; }
            });
            if (i > 0) {
                $("[id$=lblconfirmmsg]").text("Are you sure to mark these days as Absent?");
                modal: true,
            jq191('#divConfirmBox').dialog({
                buttons: {
                    "Yes": function () {
                        jq191(this).dialog("close");
                        markAbsent();
                    },
                    "No": function () {
                        jq191(this).dialog("close");
                    }
                }
            });
            }
            else {
                if ($("[id$=hdnCheckdata]").val() == "true") {
                    $("[id$=lblconfirmmsg]").text("Please select dates below.");
                }
                else {
                    $("[id$=lblconfirmmsg]").text("No unmarked days available!");
                }

                jq191('#divConfirmBox').dialog({
                    modal: true,
                    buttons: {
                        "Ok": function () {
                            jq191(this).dialog("close");
                        }
                    }
                });
            }
            return false;
        }

        function markAbsent() {
            var dateArray = [];
            var userid = $("[id$=hdnUserID]").val();

            $('.cb_select').each(function () {
                if ($(this).find('[name$=chkselecteditem]').is(':checked')) {
                    $(this).closest('tr').find('day');
                    dateArray.push($(this).closest('tr').find('.day').html());
                }
            });

            var Dates = JSON.stringify(dateArray);
            console.log(Dates);
            $.ajax({
                url: "UnmarkedDays.aspx/MarkAbsent",
                type: "POST",
                data: "{'userid':'" + userid + "','dateArray':'" + Dates + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                async: true,
                success: function () {
                    location.reload(true);
                },
                error: function (response) {
                    openConfirmBox('Sorry an error has occurred. Please try again.');
                }
            });
        }

        HTMLInputElement.prototype.startsWith = function () { return false; };
    </script>

    <asp:ScriptManager runat="server" ID="scriptmanager1"></asp:ScriptManager>

    <h2 class="username">
        <asp:Label ID="lblUserName" runat="server" Text="Unmarked Days"></asp:Label>
    </h2>
    <asp:Label runat="server" ID="lblabout" Text="Days when the user did not clock-in, was not marked absent, is not a weekend, or a holiday."></asp:Label>
    <br />
    <br />
    <asp:LinkButton ID="lnkMarkAbsent" runat="server" Text="Mark Absent" OnClientClick="return ConfirmBox();" ClientIDMode="Static"></asp:LinkButton>

    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
            <asp:Repeater ID="rpt_unmarkeddays" runat="server" OnItemDataBound="rpt_unmarkeddays_ItemDataBound">
                <HeaderTemplate>
                    <table id="tblunmarkeddays" class="table-grid-sample1 table-responsive">
                        <tr class="smallGridHead">
                            <td>
                                <asp:CheckBox ID="chkselectall" runat="server" CssClass="cb_select_all" />
                            </td>
                            <td class="td_day">Date
                            </td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkselecteditem" runat="server" CssClass="cb_select" />
                        </td>
                        <td class="td_day">
                            <asp:Label runat="server" ID="lblday" Text='<%# Eval("Day") %>' CssClass="day"></asp:Label>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>                  
                </FooterTemplate>
            </asp:Repeater>
            <asp:Label ID="lblcount" runat="server" Font-Bold="true"></asp:Label>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkMarkAbsent" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>

    <div id="divConfirmBox" title="Confirm" style="display: none">
        <p>
            <asp:Label runat="server" ID="lblconfirmmsg"></asp:Label>
        </p>
    </div>

    <asp:HiddenField runat="server" ID="hdnCheckdata" />
    <asp:HiddenField runat="server" ID="hdnUserID" />
    <asp:HiddenField runat="server" ID="hdnUserEmail" />
    <asp:HiddenField runat="server" ID="hdnUserPassword" />
    <asp:HiddenField runat="server" ID="hfparentid" />

</asp:Content>

