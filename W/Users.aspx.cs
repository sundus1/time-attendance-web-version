﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AvaimaThirdpartyTool;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI.HtmlControls;

//[WebService(Namespace = "http://tempuri.org/")]
//[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
//// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
//[System.Web.Script.Services.ScriptService]
public partial class W_Users : AvaimaWebPage
{
    AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
    Attendance atd = new Attendance();
    string AppId = "";

    protected void Page_Load(object sender, EventArgs e)
    {
       // SendTestEmail();
        hdnInstanceId.Value = Request.QueryString["instanceid"].ToString();
        hdnUserID.Value = Request.QueryString["id"].ToString();
        hdnUserEmail.Value = Request.QueryString["e"].Decrypt();
        hdnUserPassword.Value = Request.QueryString["p"].Decrypt();

        string isVerified = atd.VerifyLogin(hdnUserEmail.Value, hdnUserPassword.Value);
        if (isVerified != "TRUE")
            this.Redirect("Info.aspx");

        DataTable userSettings = JsonConvert.DeserializeObject<DataTable>(atd.GetSettingInfo(hdnUserEmail.Value, hdnUserPassword.Value));
        AppId = userSettings.Rows[0]["userid"].ToString();
        hdnTitle.Value = userSettings.Rows[0]["firstname"].ToString() + " " + userSettings.Rows[0]["lastname"].ToString();

        if (!IsPostBack)
        {
            LoadUsers(true);
        }

        if (Request.QueryString["req"] != null)
        {
            lnkAccessRequest_Click(sender, e);
        }

        if (Request.Form["__EVENTTARGET"] == "btnSigninLink")
        {
            Send_SigninLink(Request.Form["__EVENTARGUMENT"].ToString());
        }
    }


    public void SendTestEmail()
    {
        //string smtphost = "smtp.postmarkapp.com";

        //System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
        //msg.From = new System.Net.Mail.MailAddress("no-reply@avaima.com");
        //msg.To.Add("no-reply@avaima.com");

        //msg.Subject = "Testing Email";
        //msg.IsBodyHtml = true;
        //msg.Body = "Hi !!!!!!";

        //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient(smtphost);
        //mSmtpClient.Credentials = new System.Net.NetworkCredential("f92fb9b5-3ab3-4b35-ade3-f417c937c2d9", "f92fb9b5-3ab3-4b35-ade3-f417c937c2d9");
        //mSmtpClient.EnableSsl = false;
        //mSmtpClient.Port = 587;
        //mSmtpClient.Send(msg);

        //string smtphost = "smtp.yandex.com";//"mail.avaima.com";

        //System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
        //msg.From = new System.Net.Mail.MailAddress("no-reply@avaima.com", "Avaima");
        //msg.To.Add("sundus_csit@yahoo.com");

        //msg.Subject = "Testing Email";
        //msg.IsBodyHtml = true;
        //msg.Body = "Hi !!!!!!";

        //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient(smtphost);
        //mSmtpClient.Credentials = new System.Net.NetworkCredential("avaima.com@yandex.com", "system2019@");
        //mSmtpClient.EnableSsl = false;
        //mSmtpClient.Port = 465;
        //mSmtpClient.Send(msg);


        string smtphost = "smtp.yandex.com";//"mail.avaima.com";

        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
        msg.From = new System.Net.Mail.MailAddress("PCM-71579@avaima.com");
        msg.To.Add("sundus_csit@yahoo.com");

        msg.Subject = "Testing Email";
        msg.IsBodyHtml = true;
        msg.Body = "Hi !!!!!!";

        System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient(smtphost);
        mSmtpClient.Credentials = new System.Net.NetworkCredential("PCM-71579@avaima.com", "Clm3!kjnP90");
        //("avaima.com@yandex.com", "system2019@");
        //("PCM-71579@avaima.com", "Clm3!kjnP90");
        mSmtpClient.EnableSsl = true;
        mSmtpClient.Port = 587;
        mSmtpClient.Send(msg);


        //string smtphost = "smtp.office365.com";//"mail.avaima.com";

        //System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
        //msg.From = new System.Net.Mail.MailAddress("no-reply@avaima.com", "Avaima");
        //msg.To.Add("sundus_csit@yahoo.com");

        //msg.Subject = "Testing Email";
        //msg.IsBodyHtml = true;
        //msg.Body = "Hi !!!!!!";

        //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient(smtphost);
        //mSmtpClient.Credentials = new System.Net.NetworkCredential("no-reply@avaima.com", "j4n8KFM023@");
        //mSmtpClient.EnableSsl = true;
        //mSmtpClient.Port = 587;
        //mSmtpClient.Send(msg);
    }

    public void LoadUsers(bool IsActive)
    {
        bool check = false;
        hdnIsActive.Value = IsActive.ToString();

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_DGetAllAttendenceRecord1", con))
            {
                cmd.Parameters.AddWithValue("@id", 0);
                cmd.Parameters.AddWithValue("@InstanceId", hdnInstanceId.Value);
                cmd.Parameters.AddWithValue("@IsActive", IsActive);
                cmd.Parameters.AddWithValue("@userid", hdnUserID.Value);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    rpthd.DataSource = dt;
                    rpthd.DataBind();

                    hdmsg.Visible = false;
                    rpthd.Visible = true;
                    check = true;
                }
                else
                {
                    hdmsg.Visible = true;
                    rpthd.Visible = false;
                    check = false;
                }

            }
        }

        RequestDiv.Visible = false;
        UsersDiv.Visible = true;
    }

    protected void AddUser_Click(object sender, EventArgs e)
    {
        AddUsers(hdnInstanceId.Value, hdnUserID.Value, AppId, txtFirstName.Text, txtLastName.Text, txtEmail.Text, chk_manage.Checked);
        //atd.AddEmployee(txtFirstName.Text, txtLastName.Text, txtEmail.Text, hdnUserID.Value, AppId, hdnInstanceId.Value);
        ScriptManager.RegisterStartupScript(this, GetType(), "", "$('#div_adduser').click(); openConfirmBox('Employee added successfully!');", true);
        LoadUsers(true);
    }

    [WebMethod]
    public static string AddUsers(string InstanceId, string UserID, string AppId, string FirstName, string LastName, string Email, bool Managed)
    {
        Attendance atd = new Attendance();
        string response = atd.AddEmployee(FirstName, LastName, Email, UserID, AppId, InstanceId, Managed);
        if (response == "Employee added successfully !")
            return FirstName + " " + LastName;
        else
            return response;

    }
    public void LoadRequests()
    {
        DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.GetAdminRightsRequests(Convert.ToInt32(hdnUserID.Value)));

        if (dt.Rows.Count > 0)
        {
            rptRequests.DataSource = dt;
            rptRequests.DataBind();
            rptRequests.Visible = true;
            lblNoReq.Visible = false;
        }
        else
        {
            rptRequests.Visible = false;
            lblNoReq.Visible = true;
        }

        RequestDiv.Visible = true;
        UsersDiv.Visible = false;

    }

    protected void rpthd_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label date = e.Item.FindControl("lblDateAdded") as Label;
            date.Text = Convert.ToDateTime(date.Text).ToString("dd MMMM yyyy");

            Label LastClockin = e.Item.FindControl("lblLastClockin") as Label;
            if (LastClockin.Text != "")
            {
                string inDate = Convert.ToDateTime(LastClockin.Text).ToString("ddd, MMM d yyy");
                if (Convert.ToDateTime(LastClockin.Text).Date == DateTime.Now.Date)
                    LastClockin.Text = "Today";
                else
                    LastClockin.Text = inDate;

            }
            else
                LastClockin.Text = " Never ";

            LinkButton lblTitle = e.Item.FindControl("lblTitle") as LinkButton;
            CheckBox chkReqAccess = e.Item.FindControl("chkReqAccess") as CheckBox;
            CheckBox chkGrtAccess = e.Item.FindControl("chkGrtAccess") as CheckBox;
            CheckBox chk_managed = e.Item.FindControl("chk_managed") as CheckBox;
            HiddenField hdnReqAccess = e.Item.FindControl("hdnReqAccess") as HiddenField;
            HiddenField hdnGrtAccess = e.Item.FindControl("hdnGrtAccess") as HiddenField;
            HiddenField hdnPermissionTaken = e.Item.FindControl("hdnPermissionTaken") as HiddenField;
            HiddenField hdnPermissionGiven = e.Item.FindControl("hdnPermissionGiven") as HiddenField;
            HiddenField hdnManaged = e.Item.FindControl("hf_isManaged") as HiddenField;
            Button btnLink = e.Item.FindControl("btnSigninLink") as Button;

            HiddenField parentid = e.Item.FindControl("hf_parentid") as HiddenField;

            HtmlTableCell tdrqaccess = e.Item.FindControl("tdrqaccess") as HtmlTableCell;
            HtmlTableCell tdgtaccess = e.Item.FindControl("tdgtaccess") as HtmlTableCell;
            HtmlTableCell tdblank = e.Item.FindControl("tdblank") as HtmlTableCell;
            HtmlTableCell tdmanaged = e.Item.FindControl("tdmanaged") as HtmlTableCell;

            Label Text = e.Item.FindControl("lblText") as Label;

            if (hdnIsActive.Value != "false")
            { }

            if (!string.IsNullOrEmpty(hdnReqAccess.Value) && hdnReqAccess.Value != "False")
            {
                chkReqAccess.Checked = true;
                chkReqAccess.ToolTip = "Access Granted!";
                lblTitle.ToolTip = "View Attendance";
                btnLink.Enabled = true;
                btnLink.Attributes.Add("class", "ui-state-default btnSigninLink");
            }
            else
            {
                chkReqAccess.Checked = false;
                chkReqAccess.ToolTip = "Request for Access";
                lblTitle.Enabled = false;
                lblTitle.ToolTip = "You are not authorized to view attendance of this user.";
                btnLink.Enabled = false;
                btnLink.Attributes.Add("class", "ui-state-disabled");

                if (!string.IsNullOrEmpty(hdnPermissionTaken.Value) && hdnPermissionTaken.Value != "False")
                {
                    chkReqAccess.Enabled = false;
                    chkReqAccess.ToolTip = "Approval pending";
                }
            }

            if (!string.IsNullOrEmpty(hdnGrtAccess.Value) && hdnGrtAccess.Value != "False")
            {
                chkGrtAccess.Checked = true;
                chkGrtAccess.ToolTip = "Access Granted!";
            }
            else
            {
                chkGrtAccess.Checked = false;
                chkGrtAccess.ToolTip = "Grant Access";

                if (!string.IsNullOrEmpty(hdnPermissionGiven.Value) && hdnPermissionGiven.Value != "False")
                {
                    chkGrtAccess.Enabled = false;
                    chkGrtAccess.ToolTip = "Request pending";
                }
            }

            //Managed User
            if (parentid.Value != hdnUserID.Value)
            {
                if (atd.UserManaged(hdnUserID.Value.ToInt32(), hdnInstanceId.Value, "get"))
                {
                    tdmanaged.Visible = false;
                    Text.Text = "";
                    tdblank.Attributes.Add("colspan", "1");
                    tdblank.Visible = true;
                }
                else
                {
                    tdgtaccess.Visible = false;
                    tdrqaccess.Visible = false;
                    tdmanaged.Visible = false;
                    Text.Text = "";
                    tdblank.Attributes.Add("colspan", "3");
                    tdblank.Visible = true;
                }
            }
            else
            {
                if (hdnManaged.Value == "True")
                {
                    tdblank.Visible = false;
                    tdgtaccess.Visible = true;
                    tdrqaccess.Visible = true;
                    chk_managed.Checked = true;
                }
                else
                {
                    Text.Text = "You manage this employee.";
                    tdblank.Attributes.Add("colspan", "2");
                    tdblank.Visible = true;
                    tdgtaccess.Visible = false;
                    tdrqaccess.Visible = false;
                    chk_managed.Checked = false;
                }
            }
        }
    }

    protected void rpthd_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "openpopup")
        {
            HiddenField id = e.Item.FindControl("hdnID") as HiddenField;
            this.Redirect("Add_worker.aspx?id=" + Request.QueryString["id"].ToString() + "&uid=" + id.Value + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + hdnUserEmail.Value.Encrypt() + "&p=" + hdnUserPassword.Value.Encrypt());
        }
    }

    protected void lnkInactiveUser_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        if (btn.Text == "Inactive Employees")
        {
            LoadUsers(false);
        }
        else
        {
            LoadUsers(true);
        }
    }

    protected void lnkRestoreUser_Click(object sender, EventArgs e)
    {
        var ids = (from r in rpthd.Items.Cast<RepeaterItem>()
                   let selected = (r.FindControl("chkselecteditem") as CheckBox).Checked
                   let id = (r.FindControl("hf_ID") as HiddenField).Value
                   where selected
                   select id).ToList();

        foreach (var id in ids)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Update attendence_management SET active= 1 where ID = @ID", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@ID", id);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        LoadUsers(false);
        ScriptManager.RegisterStartupScript(this, GetType(), "", " openConfirmBox('Employee restored successfully.')", true);
    }

    protected void lnkRemoveUser_Click(object sender, EventArgs e)
    {
        var ids = (from r in rpthd.Items.Cast<RepeaterItem>()
                   let selected = (r.FindControl("chkselecteditem") as CheckBox).Checked
                   let id = (r.FindControl("hf_ID") as HiddenField).Value
                   where selected
                   select id).ToList();

        foreach (var id in ids)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Update attendence_management SET active= 0 where ID = @ID", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@ID", id);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        LoadUsers(true);
        ScriptManager.RegisterStartupScript(this, GetType(), "", " openConfirmBox('Employee removed successfully.');", true);

    }

    protected void lnkAccessRequest_Click(object sender, EventArgs e)
    {
        LoadRequests();
    }

    protected void rptRequests_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        HiddenField rowid = e.Item.FindControl("rowID") as HiddenField;
        HiddenField userID = e.Item.FindControl("userID") as HiddenField;

        //atd.ApproveDenyAdminRights_Email(Convert.ToInt32(hdnUserID.Value), Convert.ToInt32(userID.Value), e.CommandName, System.DateTime.Now.ToString(), hdnInstanceId.Value, Convert.ToInt32(rowid.Value));
        //lnkAccessRequest_Click((object)rptRequests, e);
        //LoadUsers(true);
    }

    protected void rptRequests_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label title = e.Item.FindControl("title") as Label;
            title.Text = "<b>" + title.Text + "</b>, wants access to track your attendance.";
        }
    }

    public void Send_SigninLink(string userid)
    {
        atd.SendEmail(userid, hdnUserID.Value);
    }
}