﻿using MyAttSys;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Collections;
using System.Web.Services;
using AuthModel;
using System.Text;

public partial class settings : AvaimaThirdpartyTool.AvaimaWebPage
{
    public DayOfWeek[] days = {
    DayOfWeek.Sunday,
    DayOfWeek.Monday,
    DayOfWeek.Tuesday,
    DayOfWeek.Wednesday,
    DayOfWeek.Thursday,
    DayOfWeek.Friday,
    DayOfWeek.Saturday };

    private string userid = "";
    private string emailId = "";
    private string password = "";
    private string InstanceID = "";
    private string AppId = "";
    private int ParentID;

    Attendance atd = new Attendance();

    protected void Page_Load(object sender, EventArgs e)
    {
        userid = Request.QueryString["id"].ToString();
        emailId = Request.QueryString["e"].ToString().Decrypt();
        password = Request.QueryString["p"].ToString().Decrypt();
        InstanceID = Request.QueryString["instanceid"].ToString();
        ParentID = atd.getParent(userid).ToInt32();

        if (!IsPostBack)
        {
            string isVerified = atd.VerifyLogin(emailId, password);
            if (isVerified != "TRUE")
                this.Redirect("Info.aspx");

            FillData();
        }

        //if (Request.Form["__EVENTTARGET"] == "holidays")
        //{
        //    LoadHolidays();
        //    //ScriptManager.RegisterStartupScript(this, GetType(), "holidayload_script", "tabClick(sessionStorage.getItem('tabID'));", true);
        //}

        if (atd.getParent(userid) == userid)
            divaddipaddress.Visible = true;
        //if (InstanceID == "6752062f-f30f-4abe-b048-6cc2daee6d3d" || InstanceID == "f425f912-8d79-4415-ba72-89a5309acb10")
        //{
        //    div_chophours.Visible = true;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "showhide('Exclock');", true);
        //}

    }

    public void FillData()
    {
        foreach (KeyValuePair<string, string> entry in atd.TimeZone)
        {
            ddlTimzone.Items.Add(new ListItem(entry.Key, entry.Value));
        }

        DataTable userSettings = JsonConvert.DeserializeObject<DataTable>(atd.GetSettingInfo(emailId, password));

        ddlTimzone.SelectedValue = hdntimezone_check.Value = userSettings.Rows[0]["timezone"].ToString();
        AppId = userSettings.Rows[0]["userid"].ToString();
        ddldlsaving.SelectedValue = userSettings.Rows[0]["dlsaving"].ToString();
        ddldlshours.SelectedValue = userSettings.Rows[0]["dlsavinghour"].ToString();
        Enable_DST();
        txtTitle.Text = SP.GetWorkerName(userid);

        LoadWeekends();
        LoadHolidays();
        info.Attributes.Add("title", "Change Time zone & daylight savings as per your region.");

        //Absences Dropdown
        for (int i = 0; i <= 100; i++)
        {
            ddlAbsences.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }

        //Workinghours Dropdown
        for (int i = 1; i <= 24; i++)
        {
            ddlWorkinghours.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }

        //Extra Break Dropdown
        for (int i = 0; i <= 120; i = i + 5)
        {
            ddlAdditionalBreak.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }

        //Load default Report Setting 
        Hashtable ht = new Hashtable();
        ht["@userid"] = userid;
        ht["@action"] = "get";
        ReportSettings(ht);

        //Load Working hours definition
        LoadWorkingHours();
        BindExceptionalDays();
        Filliptable();
        FillTagTable();
        ScriptManager.RegisterStartupScript(this, GetType(), "", " ApplyClass();", true);

        BindNotifications();

        ScriptManager.RegisterStartupScript(this, this.GetType(), "SumoSelectList", "SumoSelectList();", true);

        DataTable employees = BindEmployees();
        if (employees.Rows.Count > 0)
        {
            lstEmployee.DataValueField = "ID";
            lstEmployee.DataTextField = "Title";
            lstEmployee.DataSource = employees;
            lstEmployee.DataBind();

            for (int i = 0; i < lstEmployee.Items.Count; i++)
            {
                ListItem item = lstEmployee.Items[i];
                item.Attributes.Add("email", employees.Select("ID=" + lstEmployee.Items[i].Value)[0]["email"].ToString());
                //item.Attributes["email"] = employees.Select("ID=" + lstEmployee.Items[i].Value)[0]["email"].ToString();
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "SumoSelect_Enable", "$('[id$=lstAssignEmployee]')[0].sumo.disable();", true);

            //Admin-Employee grid
            BindAdminEmployees();

            div_adminemployees.Visible = true;
        }
        else
        {
            div_adminemployees.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "SumoSelect_Enable", "$('[id$=lstEmployee]')[0].sumo.disable();$('[id$=lstAssignEmployee]')[0].sumo.disable();", true);
        }
    }

    public DataTable BindEmployees(string IDs = "0", bool Managed = true)
    {
        //Only Managed employees can be made "Admin", But an Unmanaged can be assigned to Admin
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            string query = "select * from  attendence_management where parentid = @parentid and active = 1  and ID not in (" + IDs + ")";

            if (Managed)
                query += " and isManaged = 1";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();
                //cmd.Parameters.AddWithValue("@parentid", ParentID);
                cmd.Parameters.AddWithValue("@parentid", userid);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }
        }

        return dt;
    }

    public void SaveWorkingHours()
    {
        DataTable dt;
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            con.Open();
            using (SqlCommand cmd = new SqlCommand("Select * from  schema_6e815b00_6e13_4839_a57d_800a92809f21.WorkingHours where userid = @userid", con))
            {
                dt = new DataTable();
                cmd.Parameters.AddWithValue("@userid", userid);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    using (SqlCommand command = new SqlCommand("DELETE from schema_6e815b00_6e13_4839_a57d_800a92809f21.WorkingHours where userid = @userid", con))
                    {
                        command.Parameters.AddWithValue("@userid", userid);
                        command.ExecuteNonQuery();
                    }
                }

                var working_hours =
                          (from r in rpt_workinghours.Items.Cast<RepeaterItem>()
                           let DayTitle = (r.FindControl("lblDay") as Label).Text
                           let Clockin = (r.FindControl("txtClockin") as TextBox).Text
                           let Clockout = (r.FindControl("txtClockout") as TextBox).Text
                           let Breakin = (r.FindControl("txtBreakin") as TextBox).Text
                           let Breakout = (r.FindControl("txtBreakout") as TextBox).Text
                           select new
                           {
                               DayTitle,
                               Clockin,
                               Clockout,
                               Breakin,
                               Breakout
                           }).ToList();

                foreach (var list in working_hours)
                {
                    using (SqlCommand command = new SqlCommand("sp_WorkingHours", con))
                    {
                        command.Parameters.AddWithValue("@UserID", userid);
                        command.Parameters.AddWithValue("@action", "insert");
                        command.Parameters.AddWithValue("@DayTitle", list.DayTitle);
                        command.Parameters.AddWithValue("@Clockin", list.Clockin);
                        command.Parameters.AddWithValue("@Clockout", list.Clockout);
                        command.Parameters.AddWithValue("@BreakStart", list.Breakin);
                        command.Parameters.AddWithValue("@BreakEnd", list.Breakout);
                        command.Parameters.AddWithValue("@InstanceID", Request.QueryString["instanceid"].ToString());
                        command.Parameters.AddWithValue("@crtDate", DateTime.Now.ToString());
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                    }
                }
            }
        }
    }

    public void LoadWorkingHours()
    {
        //Load Working hours definition
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            con.Open();

            using (SqlCommand cmd = new SqlCommand("Select * from schema_6e815b00_6e13_4839_a57d_800a92809f21.WorkingHours where UserID = @userid", con))
            {
                cmd.Parameters.AddWithValue("@userid", userid);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    rpt_workinghours.DataSource = dt;
                    rpt_workinghours.DataBind();
                }
                else
                {
                    var weekendDays = new List<string>();
                    foreach (ListItem item in chkWeekend.Items)
                    {
                        if (item.Selected)
                        {
                            weekendDays.Add(item.Text);
                        }
                    }

                    List<WorkingHours> wh = new List<WorkingHours>();

                    foreach (DayOfWeek val in Enum.GetValues(typeof(DayOfWeek)))
                    {
                        if (!weekendDays.Contains(val.ToString()))
                            wh.Add(new WorkingHours() { DayTitle = val.ToString(), Clockin = "09:00 AM", Clockout = "06:00 PM", BreakStart = "01:00 PM", BreakEnd = "02:00 PM" });

                    }

                    rpt_workinghours.DataSource = wh;
                    rpt_workinghours.DataBind();
                }
            }
        }
    }

    public void LoadHolidays()
    {
        DataTable dt = new DataTable();
        dt = new DataTable();
        dt = JsonConvert.DeserializeObject<DataTable>(atd.GetUserHolidaysWeekends(userid.ToInt32(), "1", 0));

        if (dt.Rows.Count > 0)
        {
            rpthd.DataSource = dt;
            rpthd.DataBind();
            rpthd.Visible = true;
            hdmsg.Visible = false;
        }
        else
        {
            hdmsg.Visible = true;
            rpthd.Visible = false;
        }
    }

    public void LoadWeekends()
    {
        DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.GetUserHolidaysWeekends(userid.ToInt32(), "0", 0));

        foreach (DataRow row in dt.Rows)
        {
            string curItem = row["title"].ToString();

            if (chkWeekend.Items.FindByText(curItem) != null)
                chkWeekend.Items.FindByText(curItem).Selected = true;
        }
    }

    protected void rpthd_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label hddate = e.Item.FindControl("hddate") as Label;
            Label isYearly = e.Item.FindControl("ey") as Label;
            if (isYearly.Text == "Yes")
            {
                hddate.Text = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "date")).ToString("dd MMMM ");
            }
            else
                hddate.Text = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "date")).ToString("ddd, MMM dd yyyy");

        }
    }

    protected void hAdd_Click(object sender, EventArgs e)
    {
        bool result = atd.Holidays(userid, "insert", htitle.Text, hdate.Text, cye.Checked, 0);
        FillData();
        ScriptManager.RegisterStartupScript(this, GetType(), "holiday_script", "$('#hideholiday').click(); $('#hdate').datepicker();openConfirmBox('Holiday added successfully!');", true);
        htitle.Text = "";
        hdate.Text = "";
    }

    protected void rpthd_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            HiddenField d = e.Item.FindControl("hdnID") as HiddenField;
            atd.Holidays("0", "del", "", "", false, Convert.ToInt32(d.Value));
            FillData();
        }
    }

    public int checkClockStatus()
    {
        int rowId = 0;
        DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.GetStatus(userid));
        if (dt.Rows.Count > 0)
        {
            //User clocked-in 
            if (string.IsNullOrEmpty(dt.Rows[0]["lastout"].ToString()))
            {
                rowId = dt.Rows[0]["rowID"].ToInt32();
                //ddlTimzone.Enabled = false;
                //ddldlsaving.Enabled = false;
                //ddldlshours.Enabled = false;
                //info.Attributes.Add("title", "You cannot change Time zone and daylight savings while being Clocked-in");
            }
        }

        return rowId;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string fullname = txtTitle.Text;
        string timezone = ddlTimzone.SelectedItem.Value;
        int dlsaving = ddldlsaving.SelectedValue.ToInt32();
        int dlsavinghour = ddldlshours.SelectedValue.ToInt32();
        Hashtable ht = new Hashtable();
        String weekendDays = "";
        AppId = JsonConvert.DeserializeObject<DataTable>(atd.GetSettingInfo(emailId, password)).Rows[0]["userid"].ToString();

        foreach (ListItem item in chkWeekend.Items)
        {
            if (item.Selected)
            {
                weekendDays += item + ",";
            }
        }
        weekendDays = weekendDays.TrimEnd(',');

        //Save Report Settings     
        ht["@action"] = hdntype.Value;
        ht["@userid"] = userid;
        ht["@working_hours"] = ddlWorkinghours.SelectedValue + ',' + "0";
        ht["@ApplyWorkingHours"] = chk_enableworkhours.Checked;
        ht["@AbsencesAllowed"] = ddlAbsences.SelectedValue;
        if (hdntype.Value == "insert")
            ht["@CreatedDate"] = DateTime.Now;
        else
            ht["@ModifiedDate"] = DateTime.Now;
        ht["@AllowTimeEdit"] = rbtn_allowedit.SelectedValue;

        if (hdn_additionalemails.Value != "")
            ht["@AdditionalEmails"] = txt_additionalemails.Text.Trim();

        if (chkclockbefore.Checked)
        {
            ht["@Chophours"] = true;
            ht["@ChophoursFrom"] = txtclockbefore.Text;
        }

        if (chk_ipverification.Checked)
            ht["@IPVerification"] = true;

        if (chk_autoclock.Items[0].Selected)
            ht["@AutoClockin"] = true;

        if (chk_autoclock.Items[1].Selected)
            ht["@AutoClockout"] = true;

        ReportSettings(ht);

        //If timezone is changed; check if user is clocked-in ; if yes, clocked him out with old settings and than clockin him again with new settings
        if (hdntimezone_check.Value != timezone)
        {
            int rowId = checkClockStatus();
            if (rowId != 0)
                atd.ClockOut(userid, Request.UserHostAddress, rowId, "web", 2, "Auto Clock-out to update time sttings", null, false);

            // Save Timzone
            atd.SetTimezone(AppId, timezone, dlsaving, dlsavinghour);

            if (rowId != 0)
                atd.ClockIn(userid, Request.UserHostAddress, "web", 2, "Auto Clock-in to update time sttings", false);
        }
        else
        {
            // Save Timzone
            atd.SetTimezone(AppId, timezone, dlsaving, dlsavinghour);

        }
        //End

        if (chk_enableworkhours.Checked)
        {
            SaveWorkingHours();
        }

        //Save Admin-Employee access relationship
        DataAccessLayer dal = new DataAccessLayer(false);


        string[] list_Ids = hdnAllEmp.Value.Split(',');
        if (lstEmployee.GetSelectedIndices().Count() > 0)
        {
            //Delete All previous relation
            foreach (ListItem admin in lstEmployee.Items)
            {
                //For only selected admin user
                if (admin.Selected == true)
                {
                    foreach (string empId in list_Ids)
                    {
                        atd.UserAccess_RevokeDel(empId, admin.Value, InstanceID, "delete");    //1: revoke, 2: delete
                    }

                    //foreach (ListItem emp in lstAssignEmployee.Items)
                    //{
                    //    atd.UserAccess_RevokeDel(emp.Value, admin.Value, InstanceID, "delete");    //1: revoke, 2: delete
                    //}
                }
            }

            list_Ids = hdnSelectedEmp.Value.Split(',');
            //Save new relation
            foreach (ListItem admin in lstEmployee.Items)
            {
                if (admin.Selected == true)
                {
                    foreach (string empId in list_Ids)
                    {
                        atd.UserAccess(empId, admin.Value, InstanceID);
                    }

                    //foreach (ListItem emp in lstAssignEmployee.Items)
                    //{
                    //    if (emp.Selected == true)
                    //    {
                    //        atd.UserAccess(emp.Value, admin.Value, InstanceID);
                    //    }
                    //}

                    //Set role as Admin
                    ht = new Hashtable();
                    ht["@userid"] = admin.Value;
                    dal.ExecuteIUDQuery("Update tbluserrole SET role = 'admin' where userid = @userid", ht);

                    //Send Admin an email
                    AvaimaEmailAPI email = new AvaimaEmailAPI();
                    StringBuilder subject = new StringBuilder();
                    StringBuilder body = new StringBuilder();
                    subject.Append("\"Admin Rights\" assigned !");
                    body.Append("<b>" + fullname + "</b> has made you Admin<br/><br/>");

                    body.Append("Please login to your account using <a href='http://www.avaima.com/signin'>www.avaima.com</a> OR use \"Time & Attendance\" application to view access.");
                    body.Append("</ul>");
                    body.Append("<br/>For further questions, comments or help contact support@avaima.com.");
                    body.Append(Helper.AvaimaEmailSignature);
                    body.Append("</div>");
                    email.send_email(SP.GetEmailByUserID(admin.Value.ToInt32()), "Time & Attendance", "", body.ToString(), subject.ToString());
                    //End
                }
            }
        }

        ScriptManager.RegisterStartupScript(this, GetType(), "refreshAfterSave", "ActivateAlertDiv('', 'AlertDiv');location.reload(true);openConfirmBox('Admin rights sucessfully assigned!');", true);
        //End

        FillData();

        bool result = atd.UpdateUserSetting(weekendDays, Convert.ToInt32(userid), fullname);
    }

    protected void ddldlsaving_SelectedIndexChanged(object sender, EventArgs e)
    {
        Enable_DST();
    }

    public void Enable_DST()
    {
        if (ddldlsaving.SelectedItem.Value == "0")
        {
            ddldlshours.Enabled = false;
            ddldlshours.Visible = false;
        }
        else
        {
            ddldlshours.Enabled = true;
            ddldlshours.Visible = true;
        }
    }

    protected void ReportSettings(Hashtable ht)
    {
        DataAccessLayer dal = new DataAccessLayer(false);
        bool res = false;
        DataTable dt = new DataTable();

        if (ht["@action"].ToString() == "insert")
            res = dal.ExecuteNonQuery("sp_ReportSettings", ht);

        if (ht["@action"].ToString() == "get")
        {
            dt = dal.GetDataTable("sp_ReportSettings", ht);
            if (dt.Rows.Count > 0)
            {
                ddlAbsences.SelectedValue = dt.Rows[0]["AbsencesAllowed"].ToString();
                ddlWorkinghours.SelectedValue = (dt.Rows[0]["working_hours"].ToString() != "") ? dt.Rows[0]["working_hours"].ToString().Split(',')[0] : "1";
                rbtn_allowedit.SelectedValue = (dt.Rows[0]["AllowTimeEdit"].ToString() == "True") ? "1" : "0";
                Enable_Workinghours(Convert.ToBoolean(dt.Rows[0]["ApplyWorkingHours"]));
                hdn_additionalemails.Value = dt.Rows[0]["AdditionalEmails"].ToString();
                rpt_AdditionalEmails(dt.Rows[0]["AdditionalEmails"].ToString());
                if (Convert.ToBoolean(dt.Rows[0]["Chophours"].ToString()))
                {
                    chkclockbefore.Checked = true;
                    txtclockbefore.Text = Convert.ToDateTime(dt.Rows[0]["ChophoursFrom"]).ToShortTimeString();
                }

                if (Convert.ToBoolean(dt.Rows[0]["IPVerification"].ToString()))
                    chk_ipverification.Checked = true;

                if (Convert.ToBoolean(dt.Rows[0]["AutoClockin"]))
                    chk_autoclock.Items[0].Selected = true;

                if (Convert.ToBoolean(dt.Rows[0]["AutoClockout"]))
                    chk_autoclock.Items[1].Selected = true;

                hdntype.Value = "update";
            }
            else
            {
                hdntype.Value = "insert";
            }
        }

        if (ht["@action"].ToString() == "update" || ht["@action"].ToString() == "update_email")
            res = dal.ExecuteNonQuery("sp_ReportSettings", ht);

    }

    protected void rpt_AdditionalEmails(string emails)
    {
        hdn_additionalemails.Value = emails;

        if (emails != "")
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("email");
            DataRow dr = null;

            foreach (var email in emails.Split(','))
            {
                dr = dt.NewRow();
                dr["email"] = email;
                dt.Rows.Add(dr);
            }
            rpt_additionalemails.DataSource = dt;
            rpt_additionalemails.DataBind();
            rpt_additionalemails.Visible = true;

            if (dt.Rows.Count == 5)
            {
                btn_additionalemails.Visible = false;
                txt_additionalemails.ReadOnly = true;
                txt_additionalemails.ToolTip = "Only 5 emails are allowed!";
                txt_additionalemails.CssClass = "information";
            }
            else
            {
                txt_additionalemails.ReadOnly = false;
                btn_additionalemails.Visible = true;
            }
        }
        else
        {
            txt_additionalemails.ReadOnly = false;
            rpt_additionalemails.Visible = false;
            btn_additionalemails.Visible = true;
        }
    }

    protected void rpt_additionalemails_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            string updated_emails = "";
            string[] emails = hdn_additionalemails.Value.Split(',');
            foreach (var email in emails)
            {
                if (Array.Find(emails, s => s.Equals(email)) != e.CommandArgument.ToString())
                    updated_emails += email + ",";
            }

            updated_emails = updated_emails.TrimEnd(',');
            Hashtable ht = new Hashtable();
            ht["@action"] = "update_email";
            ht["@userid"] = userid;
            ht["@AdditionalEmails"] = updated_emails;
            ReportSettings(ht);

            rpt_AdditionalEmails(updated_emails);
        }
    }
    protected void btn_additionalemails_Click(object sender, EventArgs e)
    {
        string emails = (hdn_additionalemails.Value != "") ? (hdn_additionalemails.Value + "," + txt_additionalemails.Text) : txt_additionalemails.Text;
        rpt_AdditionalEmails(emails);
        txt_additionalemails.Text = "";

        Hashtable ht = new Hashtable();
        ht["@action"] = "update_email";
        ht["@userid"] = userid;
        ht["@AdditionalEmails"] = emails;
        ReportSettings(ht);
    }

    public void Enable_Workinghours(bool check)
    {
        if (check)
        {
            ddlWorkinghours.Enabled = false;
            chk_enableworkhours.Checked = true;
            div_workinghours.Visible = true;
            div_autoclock.Visible = true;
        }
        else
        {
            ddlWorkinghours.Enabled = true;
            chk_enableworkhours.Checked = false;
            div_workinghours.Visible = false;
            div_autoclock.Visible = false;
        }

        ScriptManager.RegisterStartupScript(this, GetType(), "", " ApplyClass();", true);
    }

    protected void rpt_workinghours_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField check = e.Item.FindControl("hdn_check") as HiddenField;
        }
    }

    protected void btnAddExceptionalDays_Click(object sender, EventArgs e)
    {
        if (hdnGroupID.Value == "0")
        {
            SaveExceptionalDays();
        }
        else
        {
            ExceptionalDay.Delete(hdnGroupID.Value);
            SaveExceptionalDays();
        }
        BindExceptionalDays();
        ClearFields();
    }
    private void SaveExceptionalDays()
    {
        String guid = Guid.NewGuid().ToString();
        List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(userid.ToInt32(), InstanceID);
        for (DateTime date = Convert.ToDateTime(txtExFrom.Text); date <= Convert.ToDateTime(txtExTo.Text); date = date.AddDays(1))
        {
            if (exDays.Where(u => u.Date.Date == date).ToList().Count > 0)
            { continue; }
            ExceptionalDay exDay = new ExceptionalDay()
            {
                Active = true,
                GroupID = guid,
                Clockin = Convert.ToDateTime(txtExClockIn.Text),
                CrtDate = DateTime.Now,
                Clockout = Convert.ToDateTime(txtExClockOut.Text),
                InstanceID = InstanceID,
                Date = date,
                DayTitle = date.DayOfWeek.ToString(),
                GroupName = txtExTitle.Text,
                ModDate = DateTime.Now,
                UserID = userid.ToInt32(),
                AvoidBreaks = false
            };
            exDay.Save();
        }
    }
    private void BindExceptionalDays()
    {
        DataTable dt = ExceptionalDay.GetExceptionalDaysGroup(userid.ToInt32(), InstanceID).Tables[0];
        rptExceptionalDays.DataSource = dt;
        rptExceptionalDays.DataBind();
        if (dt.Rows.Count > 0)
        {
            rptExceptionalDays.Visible = true;
        }
        else
        {
            rptExceptionalDays.Visible = false;
        }
    }

    protected void rptExceptionalDays_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            ExceptionalDay.Delete(e.CommandArgument.ToString());
            BindExceptionalDays();
            ClearFields();
        }
        else if (e.CommandName == "Edit")
        {
            Label lblGroupName = (Label)e.Item.FindControl("lblGroupName");
            Label lblStartDate = (Label)e.Item.FindControl("lblStartDate");
            Label lblEndDate = (Label)e.Item.FindControl("lblEndDate");
            Label lblClockIn = (Label)e.Item.FindControl("lblClockIn");
            Label lblClockOut = (Label)e.Item.FindControl("lblClockOut");
            Label lblCrtDate = (Label)e.Item.FindControl("lblCrtDate");
            Label lblModDate = (Label)e.Item.FindControl("lblModDate");
            Label chkAvoidBreaks = (Label)e.Item.FindControl("chkAvoidBreaks");
            hdnGroupID.Value = e.CommandArgument.ToString();
            txtExTitle.Text = lblGroupName.Text;
            txtExClockIn.Text = lblClockIn.Text;
            txtExClockOut.Text = lblClockOut.Text;
            txtExFrom.Text = lblStartDate.Text;
            txtExTo.Text = lblEndDate.Text;
            //if (chkAvoidBreaks.Text == "No")
            //{
            //    chkEliminateBreaks.Checked = false;
            //}
            //else
            //{
            //    chkEliminateBreaks.Checked = true;
            //}
            btnAddExceptionalDays.Text = "Update";

            ScriptManager.RegisterStartupScript(this, GetType(), "", " $('.txtdatetime').datepicker();", true);

        }
    }

    private void ClearFields()
    {
        txtExClockIn.Text = "";
        txtExClockOut.Text = "";
        txtExFrom.Text = "";
        txtExTitle.Text = "";
        txtExTo.Text = "";
        //chkEliminateBreaks.Checked = false;
        hdnGroupID.Value = "0";
        btnAddExceptionalDays.Text = "Add";
        ExpectionalDays.Visible = true;
    }

    public void Filliptable()
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("getipadress", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@instanceId", InstanceID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    rptripaddress.DataSource = dt;
                    rptripaddress.DataBind();
                    rptripaddress.Visible = true;
                    lblempty.Visible = false;
                    chk_ipverification.Visible = true;
                }
                else
                {
                    rptripaddress.Visible = false;
                    lblempty.Visible = true;
                    chk_ipverification.Visible = false;
                }
            }
        }
    }

    protected void RptrItemcommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("delipaddress", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", e.CommandArgument);
                    cmd.Parameters.AddWithValue("@instanceId", InstanceID);
                    cmd.ExecuteNonQuery();
                    Filliptable();
                }
            }
        }
    }

    protected void RptrItemdatabound(object sender, RepeaterItemEventArgs e)
    {
    }
    protected void BtnaddClick(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("insertipaddress", con))
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ipaddress", txtipaddress.Text.Trim());
                cmd.Parameters.AddWithValue("@instanceId", InstanceID);
                cmd.Parameters.AddWithValue("@title", ipTitle.Text);
                cmd.ExecuteNonQuery();
                Filliptable();
                txtipaddress.Text = "";
                ipTitle.Text = "";
            }
        }
        ScriptManager.RegisterStartupScript(this, GetType(), "Ip check", "ipVerification();", true);
    }

    protected void lnkExNew_Click(object sender, EventArgs e)
    {
        ClearFields();
    }

    protected void chk_enableworkhours_CheckedChanged(object sender, EventArgs e)
    {
        Enable_Workinghours(chk_enableworkhours.Checked);
    }

    protected void ddlTimzone_SelectedIndexChanged(object sender, EventArgs e)
    {
        int rowId = checkClockStatus();
        if (rowId != 0)
        {
            string msg = "Note: You are currently clocked-in and you will be automatically clocked-out to update the timezone settings. Once update is made, you will be automatically be clocked-in with new timezone settings.";
            ScriptManager.RegisterStartupScript(this, GetType(), "timezoneUpdate", " openConfirmBox('" + msg + "');", true);
        }
    }

    public void BindNotifications()
    {
        Hashtable ht = new Hashtable();
        DataAccessLayer dal = new DataAccessLayer(false);

        ht["@action"] = "get";
        ht["@UserID"] = userid;
        DataTable dt = dal.GetDataTable("sp_User_EmailNotifications", ht);

        if (dt.Rows.Count > 0)
        {
            notificatio_msg.Visible = false;

            rpt_notifications.DataSource = dt;
            rpt_notifications.DataBind();
            rpt_notifications.Visible = true;
        }
        else
        {
            //In order to update Existing User's because they have no data in EMAIL NOTIFICATIONS table 
            ht = new Hashtable();
            ht["@UserID"] = userid;
            dt = dal.GetDataTable("sp_ADD_User_EmailNotifications", ht);
            notificatio_msg.Visible = false;

            rpt_notifications.DataSource = dt;
            rpt_notifications.DataBind();
            rpt_notifications.Visible = true;
            //End                     
        }
    }
    protected void chk_notificationEnabled_CheckedChanged(object sender, EventArgs e)
    {
        DataAccessLayer dal = new DataAccessLayer(false);
        CheckBox isEnabled = (CheckBox)sender;
        RepeaterItem item = (RepeaterItem)isEnabled.NamingContainer;
        HiddenField EmailNotifications_ID = (HiddenField)item.FindControl("hdn_EmailNotifications_ID");

        Hashtable ht = new Hashtable();

        ht["@action"] = "update";
        ht["@ID"] = isEnabled.Attributes["data-id"].ToInt32();
        ht["@EmailNotifications_ID"] = EmailNotifications_ID.Value;
        ht["@isEnabled"] = isEnabled.Checked;
        ht["@UserID"] = userid;

        bool res = dal.ExecuteNonQuery("sp_User_EmailNotifications", ht);

        BindNotifications();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "$('ul.tabs li').trigger('click');", true);
    }

    [WebMethod]
    public static void IUD_Holidays(string action, int Id)
    {
        Attendance atd = new Attendance();
        settings obj = new settings();

        if (action == "del")
        {
            atd.Holidays("0", "del", "", "", false, Id);
        }
    }

    [WebMethod]
    public static void reloadHolidays()
    {
        settings obj = new settings();
        obj.LoadHolidays();
    }

    public class WorkingHours
    {
        public string DayTitle { get; set; }
        public string Clockin { get; set; }
        public string Clockout { get; set; }
        public string BreakStart { get; set; }
        public string BreakEnd { get; set; }
    }
    public class Weekends
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public Weekends(string id, string text)
        {
            this.Id = id;
            this.Text = text;
        }
    }

    protected void lstEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SumoSelectList", "SumoSelectList();", true);

        string selectedValues = string.Empty;
        foreach (ListItem li in lstEmployee.Items)
        {
            if (li.Selected == true)
            {
                selectedValues += li.Value + ",";
            }
        }
        selectedValues = selectedValues.TrimEnd(',');

        if (!string.IsNullOrEmpty(selectedValues))
        {
            DataTable employees = BindEmployees(selectedValues, false);
            if (employees.Rows.Count > 0)
            {
                lstAssignEmployee.DataValueField = "ID";
                lstAssignEmployee.DataTextField = "Title";
                lstAssignEmployee.DataSource = employees;
                lstAssignEmployee.DataBind();
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "SumoSelect_Enable", "  $('[id$=lstAssignEmployee]')[0].sumo.toggSelAll(false);$('[id$=lstAssignEmployee]')[0].sumo.disable();", true);
        }
    }

    [WebMethod]
    public static string EmployeeSelectionChange(string id, string parentid)
    {
        settings obj = new settings();

        DataTable employees = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("select * from  attendence_management where parentid = @parentid and active = 1 and ID not in (" + id + ")", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@parentid", parentid);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(employees);
            }
        }

        return JsonConvert.SerializeObject(employees);
    }

    public void BindAdminEmployees()
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_GetAdminUsers", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", userid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    rpt_adminEmployee.DataSource = dt;
                    rpt_adminEmployee.DataBind();
                    rpt_adminEmployee.Visible = true;
                    lblempty_adminEmployee.Visible = false;
                }
                else
                {
                    rpt_adminEmployee.Visible = false;
                    lblempty_adminEmployee.Visible = true;
                }
            }
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "SumoSelectList", "SumoSelectList();", true);
    }

    protected void rpt_adminEmployee_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            LinkButton lnk = e.Item.FindControl("lnkdel") as LinkButton;
            HiddenField hdnemployeesID = e.Item.FindControl("hdnemployeesID") as HiddenField;
            string AdminID = e.CommandArgument.ToString();

            foreach (var empID in hdnemployeesID.Value.Split(','))
            {
                atd.UserAccess_RevokeDel(empID, AdminID, InstanceID, "delete");    //1: revoke, 2: delete
            }
        }

        //BindAdminEmployees();
        ScriptManager.RegisterStartupScript(this, GetType(), "refreshAfterDelete", "ActivateAlertDiv('', 'AlertDiv');location.reload(true);openConfirmBox('Admin rights sucessfully revoked!');", true);


    }

    protected void rpt_adminEmployee_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnadminID = e.Item.FindControl("hdnadminID") as HiddenField;
            Label lblemployees = e.Item.FindControl("lblemployees") as Label;
            HiddenField hdnemployeesID = e.Item.FindControl("hdnemployeesID") as HiddenField;

            DataTable dt = getAssignedEmployees(hdnadminID.Value);
            foreach (DataRow dr in dt.Rows)
            {
                lblemployees.Text += dr["Title"].ToString() + ',';
                hdnemployeesID.Value += dr["ID"].ToString() + ',';
            }

            lblemployees.Text = lblemployees.Text.TrimEnd(',');
            hdnemployeesID.Value = hdnemployeesID.Value.TrimEnd(',');
        }
    }

    public DataTable getAssignedEmployees(string adminID)
    {
        DataTable dt = new DataTable();

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("Select  ID, Title from attendence_management where ID in (Select userid from UserAccessRelation ac where ac.accessTo = @userid AND ac.access_given = 1) and parentid != @userid", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", adminID);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }
        }
        return dt;
    }

    protected void btnAddTag_Click(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("AddTags", con))
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@tag", txtTag.Text);
                cmd.Parameters.AddWithValue("@instanceid", InstanceID);
                cmd.Parameters.AddWithValue("@createdby", userid);
                cmd.Parameters.AddWithValue("@createdDate", DateTime.Now);
                cmd.Parameters.AddWithValue("@isactive", true);
                cmd.ExecuteNonQuery();
                FillTagTable();
                txtTag.Text = "";
            }
        }
    }

    public void FillTagTable()
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("GetTags", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@instanceid", InstanceID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    reptTags.DataSource = dt;
                    reptTags.DataBind();
                    reptTags.Visible = true;
                    lblTags.Visible = false;
                }
                else
                {
                    reptTags.Visible = false;
                    lblTags.Visible = true;
                }
            }
        }
    }

    protected void reptTags_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DeleteTags", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", e.CommandArgument);
                    cmd.Parameters.AddWithValue("@instanceId", InstanceID);
                    cmd.ExecuteNonQuery();
                    FillTagTable();
                }
            }
        }
    }
}