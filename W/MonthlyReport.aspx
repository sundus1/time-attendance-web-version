﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MonthlyReport.aspx.cs" Inherits="MonthlyReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" media="screen" href="_assets/style.css" />
    <link rel="stylesheet" href="_assets/jquery-ui.css" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="_assets/css/tooltipster.css" />
    <script type="text/javascript" src="_assets/js/jquery.tooltipster.js"></script>
    <link rel="stylesheet" href="_assets/jquery.timepicker.css" />
    <script src="_assets/jquery.timepicker.min.js"></script>
    <style>
        .SatSunHolidays {
            background-color: #FFCE9D !important;
        }
        .NormalHoliday {
            background-color: #CAF4CF !important;
        }
        .rowData {
            float: left;
            position: relative;
        }
        .absent
        {
            background-color:red !important; 
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#time").timepicker({ 'timeFormat': 'h:i A', 'step' : 15 });
            $("#filters").change(function () {
                var v = $(this).find("option:selected").val();
                switch (v) {
                    case "0":
                        $("tr[id*='reportR_rContainer']").show();
                        $("#hrsC").hide();
                        $("#tmC").hide();
                        break;
                    case "1":
                        $("tr[id*='reportR_rContainer']").hide();
                        $(".absent").parent().show();
                        $("#hrsC").hide();
                        $("#tmC").hide();
                        break;
                    case "3":
                        $("tr[id*='reportR_rContainer']").show();
                        $("#hrsC").show();
                        $("#tmC").hide();
                        break;
                    case "2":
                        $("tr[id*='reportR_rContainer']").show();
                        $("#hrsC").hide();
                        $("#tmC").show();
                        break;
                }

                $("#hours").change(function () {
                    $("tr[id*='reportR_rContainer']").hide();
                    var i = $(this).find("option:selected").val();
                    $("td[id*='reportR_dateTitleTD']").each(function () {
                        var t = $(this).attr('wTime').split('-');
                        if (t[1] != null && t[1] != undefined) {
                            if (parseInt(t[0]) >= parseInt(i)) {
                                $(this).parent().show();
                            }
                        }
                    });
                });

                $("#btntC").click(function () {
                    $("tr[id*='reportR_rContainer']").hide();
                    var i = $("#time").val();
                    if (i != "") {
                        $("td[id*='reportR_dateTitleTD']").each(function () {
                            var t = $(this).attr('iTime');
                            if (t != "") {
                                var t1 = t.split(" ");
                                var t = i.split(" ");
                                if (t1[2] == t[1]) {
                                    var hm1 = t1[1].split(":");
                                    var hm = t[0].split(":");
                                    if (parseInt(hm1[0]) >= parseInt(hm[0]) && parseInt(hm1[1]) >= parseInt(hm[1])) {
                                        $(this).parent().show();
                                    }
                                }
                            }
                        });
                    }
                });
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <div><asp:LinkButton runat="server" ID="bk" Text="Back to attendance" OnClick="bk_Click"></asp:LinkButton></div>
            <div style="">
                <div style="text-align:left">
                    <div>Please select month and year</div>
                    <div>
                        <asp:DropDownList runat="server" ID="month">
                            <asp:ListItem Text="Select Month" Value="0" Enabled="true"></asp:ListItem>
                            <asp:ListItem Text="January" Value="1"></asp:ListItem>
                            <asp:ListItem Text="February" Value="2"></asp:ListItem>
                            <asp:ListItem Text="March" Value="3"></asp:ListItem>
                            <asp:ListItem Text="April" Value="4"></asp:ListItem>
                            <asp:ListItem Text="May" Value="5"></asp:ListItem>
                            <asp:ListItem Text="June" Value="6"></asp:ListItem>
                            <asp:ListItem Text="July" Value="7"></asp:ListItem>
                            <asp:ListItem Text="August" Value="8"></asp:ListItem>
                            <asp:ListItem Text="September" Value="9"></asp:ListItem>
                            <asp:ListItem Text="October" Value="10"></asp:ListItem>
                            <asp:ListItem Text="November" Value="11"></asp:ListItem>
                            <asp:ListItem Text="December" Value="12"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList runat="server" ID="year">
                            <asp:ListItem Text="Select Year" Value="0" Enabled="true"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="btnGR" runat="server" OnClick="btnGR_Click" Text="Generate Report" Width="190px" style="margin-top:2px"/>
                    </div>
                    <div id="filtersContainer" runat="server" visible="false">
                        <select id="filters" name="filters">
                            <option value="0">Show All</option>
                            <option value="1">Show Absences Only</option>
                            <option value="2">Show Lates Only</option>
                            <option value="3">Show By Working Hours</option>
                        </select>
                        <span id="hrsC" style="display:none">
                            <select id="hours" name="hours">
                                <option value="1">1+</option>
                                <option value="2">2+</option>
                                <option value="3">3+</option>
                                <option value="4">4+</option>
                                <option value="5">5+</option>
                                <option value="6">6+</option>
                                <option value="7">7+</option>
                                <option value="8">8+</option>
                                <option value="9">9+</option>
                                <option value="10">10+</option>
                            </select>
                        </span>
                        <span id="tmC" style="display:none">
                            Start Time: <input type="text" id="time" name="time" style="width:85px" /><input type="button" value="Show" id="btntC" style="margin-top:2px"/>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div style="text-align:center" id="rpContainer" runat="server" Visible="False">
            <div>
                <div style="font-size:16px; font-weight:bold;height:18px"><asp:Label runat="server" ID="name"></asp:Label>'s Report For <asp:Label ID="monthName" runat="server"></asp:Label></div>
                <asp:Repeater runat="server" ID="reportR" OnItemDataBound="reportR_OnDataBound">
                    <HeaderTemplate>
                        <table class="smallGrid" style="width:795px; height:100%; margin:0 auto">
                            <tr class="smallGridHead">
                                <td>Date</td>
                                <td>Login Time</td>
                                <td>Logout Time</td>
                                <td>Total Time </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr id="rContainer" runat="server">
                                <td style="background-color:#FFFFFF" runat="server" ID="dateTD">
                                    <asp:Label runat="server" ID="lblDate"></asp:Label>
                                </td>
                                <td colspan="3" runat="server" ID="dateTitleTD">
                                    <div>
                                        <asp:Label runat="server" ID="lblDayTitle" style="font-weight:bold;"></asp:Label>
                                    </div>
                                    <div style="">
                                        <div class="rowData" style="left:50px;width:125px">
                                            <asp:Label runat="server" ID="lblInTime"></asp:Label>
                                        </div>
                                        <div class="rowData" style="left:163px;width:125px">
                                            <asp:Label runat="server" ID="lblOutTime"></asp:Label>
                                        </div>
                                        <div class="rowData" style="left:282px">
                                            <asp:Label runat="server" ID="lblTotal"></asp:Label>
                                        </div>
                                    </div>
                                </td>
<%--                                <td runat="server" ID="inTimeTD">
                                    <asp:Label runat="server" ID="lblInTime"></asp:Label>
                                </td>
                                <td runat="server" ID="outTimeTD">
                                    <asp:Label runat="server" ID="lblOutTime"></asp:Label>
                                </td>
                                <td runat="server" ID="totalTD">
                                    <asp:Label runat="server" ID="lblTotal"></asp:Label>
                                </td>--%>
                            </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                            <tr>
                                <td style="background-color:#FFFFFF"></td>
                                <td colspan="3" style="border-top:1px solid black; background-color:#FFFFFF">
                                    <div style="float:left; font-size:14px">
                                        <span style="font-weight:bold">Total Working Days: </span>
                                        <asp:Label ID="lblTWD" runat="server"></asp:Label>
                                    </div>
                                    <div style="float:left;margin-left:100px; font-size:14px">
                                        <span style="font-weight:bold">Total Absences: </span>
                                        <asp:Label ID="lblTA" runat="server"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
