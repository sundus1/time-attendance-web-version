﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class W_AdminPanel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadClockins();
        }
    }

    public void LoadData()
    {
        DataTable dt = new DataTable();

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_AdminStats", con))
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 90;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }
        }

        lbl_adminCount.Text = dt.Rows[0]["Admin"].ToString();
        lbl_empCount.Text = dt.Rows[0]["Employees"].ToString();
        lbl_empaccessCount.Text = dt.Rows[0]["EmployeesAccessed"].ToString();
        lbl_clockinCount.Text = dt.Rows[0]["Clockins"].ToString();
        lbl_clockinTodayCount.Text = dt.Rows[0]["ClockinsToday"].ToString();
        //lbl_this.Text = dt.Rows[0]["this"].ToString();
        //lbl_last.Text = dt.Rows[0]["last"].ToString();

        ScriptManager.RegisterStartupScript(this, GetType(), "", " google.charts.load('current', { 'packages': ['corechart']}); google.charts.setOnLoadCallback(drawChart);", true);

    }

    public void LoadClockins()
    {
        DataTable dt = new DataTable();
        string year = DateTime.Now.Year.ToString();

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            con.Open();
            string data;

            #region Month-wise Chart
            //Month-wise in a year
            //using (SqlCommand cmd = new SqlCommand("sp_TotalClockinYear", con))
            using (SqlCommand cmd = new SqlCommand("sp_TotalSignUpsMonths", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 90;
                //cmd.Parameters.AddWithValue("@year", year);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }

            var array1 = (from DataRow dr in dt.Rows
                          select new
                          {
                              Day = dr["Day"],
                              Signups = dr["Signups"]
                          }
                          ).ToArray();
            data = JsonConvert.SerializeObject(array1);
            ScriptManager.RegisterStartupScript(this, GetType(), "script1", "ClockinsDataArray('" + year + "','" + data + "',1);google.charts.load('current', { 'packages': ['corechart']}); google.charts.setOnLoadCallback(drawClockinChart);", true);
            #endregion

            #region Weekly Chart
            //Past 7 days clockins
            dt = new DataTable();
            using (SqlCommand cmd = new SqlCommand("sp_TotalClockinWeek", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 90;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }

            var array2 = (from DataRow dr in dt.Rows
                          select new
                          {
                              Day = dr["Day"],
                              Clockins = dr["Clockins"]
                          }
                          ).ToArray();
            data = JsonConvert.SerializeObject(array2);
            ScriptManager.RegisterStartupScript(this, GetType(), "script2", "ClockinsDataArray('" + year + "','" + data + "',2);google.charts.load('current', { 'packages': ['corechart']}); google.charts.setOnLoadCallback(drawClockinChart_week);", true);
            #endregion

            #region Active Users Pie Chart
            //Past 20 days clockins
            dt = new DataTable();
            using (SqlCommand cmd = new SqlCommand("sp_TotalActiveUsers", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 90;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }

            var array3 = (from DataRow dr in dt.Rows
                          select new
                          {
                              User = dr["User_type"],
                              Total = dr["Total"]
                          }
                          ).ToArray();
            data = JsonConvert.SerializeObject(array3);

            ScriptManager.RegisterStartupScript(this, GetType(), "script3", "ClockinsDataArray('','" + data + "',3);google.charts.load('current', { 'packages': ['corechart']}); google.charts.setOnLoadCallback(drawActiveUsersChart);", true);
            #endregion

            #region Active User Month-wise Bar Chart
            dt = new DataTable();
            using (SqlCommand cmd = new SqlCommand("sp_stats_ActiveUsers", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 90;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }

            var array4 = (from DataRow dr in dt.Rows
                          select new
                          {
                              Month = dr["Month"] + " " + Convert.ToDateTime(dr["CrtDate"]).ToString("yyyy"),
                              adminCount = dr["adminCount"],
                              employeeCount = dr["employeeCount"]
                          }
                          ).ToArray();
            data = JsonConvert.SerializeObject(array4);

            ScriptManager.RegisterStartupScript(this, GetType(), "script4", "ClockinsDataArray('','" + data + "',4);google.charts.load('current', {'packages':['bar']});google.charts.setOnLoadCallback(drawChart_ActiveEmpAdmin);", true);
            #endregion

            #region Employee Count
            dt = new DataTable();
            using (SqlCommand cmd = new SqlCommand("sp_EmployeeCount", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 90;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }

            var array5 = (from DataRow dr in dt.Rows
                          select new
                          {
                              label = dr["label"],
                              total = dr["total"]
                          }
                          ).ToArray();
            data = JsonConvert.SerializeObject(array5);

            ScriptManager.RegisterStartupScript(this, GetType(), "script5", "ClockinsDataArray('','" + data + "',5);google.charts.load('current', { 'packages': ['corechart']}); google.charts.setOnLoadCallback(drawChart);", true);
            #endregion
        }
    }
}