﻿<%@ Page Title="" Language="C#" MasterPageFile="~/W/MasterPage.master" AutoEventWireup="true" CodeFile="AddEmployee.aspx.cs" ValidateRequest="false" Inherits="W_AddEmployee" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style>
        .input-group-addon {
            font-size: inherit !important;
            line-height: none !important;
            padding: 5px 5px !important;
            color: gray !important;
        }

        .container {
            border: 1px solid lightgray !important;
            padding: 8px 5px !important;
            background: #d3d3d330 !important;
        }

        .table > tbody > tr > td {
            border-bottom: 1px solid lightgray !important;
            padding: 5px 0px 5px 0px !important;
        }

        .table {
            width: 85% !important;
            margin-bottom: 0px !important;
        }

            .table td {
                border-top: none !important;
            }

        .btn {
            font-size: inherit !important;
            cursor: pointer;
        }

        .standardField {
            padding: 5px 0px 4px 5px !important;
            border: 1px solid #ced4da;
            border-radius: .25em;
            width: 200px !important;
        }

        input[type="checkbox"] {
            cursor: pointer;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            outline: 0;
            background: lightgray;
            height: 16px;
            width: 16px;
            border: 1px solid white;
        }

            input[type="checkbox"] + label {
                cursor: pointer;
                color: #5f5858;
            }

            input[type="checkbox"]:checked {
                background: #0f73ccd4;
            }

            input[type="checkbox"]:hover {
                filter: brightness(90%);
            }

            input[type="checkbox"]:disabled {
                background: #e6e6e6;
                opacity: 0.6;
                pointer-events: none;
            }

            input[type="checkbox"]:after {
                content: '';
                position: relative;
                left: 40%;
                top: 20%;
                width: 15%;
                height: 40%;
                border: solid #fff;
                border-width: 0 2px 2px 0;
                transform: rotate(45deg);
                display: none;
            }

            input[type="checkbox"]:checked:after {
                display: block;
            }

            input[type="checkbox"]:disabled:after {
                border-color: #7b7b7b;
            }
    </style>

    <script>

        jQuery(document).ready(function ($) {

            if (GetParameterValues("n") != undefined) {
                $(".lnkclockinpage").attr('href', "Add_worker.aspx?" + window.location.href.split('?')[1].split('&n')[0]);
                $(".welcome_msg").show();
            }
            else {
                $(".welcome_msg").hide();
            }

            $(document).on('blur', '.rpFields', function () {
                if ($(this).val() != "")
                    $(this).parent().parent().find('[id*=hdnRowCheck_]').val('1');
                else
                    $(this).parent().parent().find('[id*=hdnRowCheck_]').val('0');
            });

            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        });

        function EndRequestHandler(sender, args) {
            if (args.get_error() != undefined && args.get_error().httpStatusCode == '500') {
                var errorMessage = args.get_error().message;
                args.set_errorHandled(true);
            }
            ActivateAlertDiv('none', 'AlertDiv');
        }

        function cursorTooltip() {
            jq191('.information').tooltipster({
                interactive: true,
            });
        }

        function validateData() {
            
            var tr_elemArray = document.getElementsByClassName('tr_fields'); //Get validate required fields Parent tr
            var regex = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            var check = true;
            var count = 0;

            for (var i = 0; i < tr_elemArray.length; i++) {

                var tr_elem = tr_elemArray[i].getElementsByClassName("rpFields");

                if (validateRows(tr_elemArray[i])) {

                    for (var j = 0; j < tr_elem.length; j++) {

                        var elem = tr_elem[j];

                        var emailFormat = regex.test(elem.value);

                        if (elem.id.indexOf("txtEmail") > 0) {
                            if (!emailFormat) {
                                check = false;
                                $("#" + elem.id).css("border", "1px solid #f54646");
                                $("#" + elem.id).attr("title", "Invalid Email Address");
                                $("#" + elem.id).addClass("information");
                                cursorTooltip();
                            }
                            else {
                                $("#" + elem.id).css("border", "1px solid darkgray");
                                $("#" + elem.id).removeAttr("title");
                                $("#" + elem.id).removeClass("information");
                            }
                        }
                        else if (elem.value == "") {
                            check = false;
                            $("#" + elem.id).css("border", "1px solid #f54646");
                            $("#" + elem.id).attr("title", "Required");
                            $("#" + elem.id).addClass("information");
                            cursorTooltip();
                        }
                        else {
                            $("#" + elem.id).css("border", "1px solid darkgray");
                            $("#" + elem.id).removeAttr("title");
                            $("#" + elem.id).removeClass("information");
                        }
                    }
                }
                else {
                    count++;

                    for (var j = 0; j < tr_elem.length; j++) {
                        var elem = tr_elem[j];
                        $("#" + elem.id).css("border", "1px solid darkgray");
                        $("#" + elem.id).removeAttr("title");
                        $("#" + elem.id).removeClass("information");
                    }
                }
            }

            if (tr_elemArray.length == count) {
                check = false;
            }

            //if (check && count == 0)
            if (check)
                return true;


            return false;
        }

        function validateRows(row_elem) {
            if (row_elem.length == 1)
                return true;
            else if (row_elem.lastElementChild.lastElementChild.value == "1") {
                return true;
            }
            return false;
        }

        function validateData_backup() {

            var elemArray = document.getElementsByClassName('rpFields');
            var regex = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            var check = true;

            for (var i = 0; i < elemArray.length; i++) {
                var elem = elemArray[i];

                var emailFormat = regex.test(elem.value);

                if (elem.id.indexOf("txtEmail") > 0) {
                    if (!emailFormat) {
                        check = false;
                        $("#" + elem.id).css("border", "1px solid #f54646");
                        $("#" + elem.id).attr("title", "Invalid Email Address");
                        $("#" + elem.id).addClass("information");
                        cursorTooltip();
                    }
                    else {
                        $("#" + elem.id).css("border", "1px solid darkgray");
                        $("#" + elem.id).removeAttr("title");
                        $("#" + elem.id).removeClass("information");
                    }
                }
                else if (elem.value == "") {
                    check = false;
                    $("#" + elem.id).css("border", "1px solid #f54646");
                    $("#" + elem.id).attr("title", "Required");
                    $("#" + elem.id).addClass("information");
                    cursorTooltip();
                }
                else {
                    $("#" + elem.id).css("border", "1px solid darkgray");
                    $("#" + elem.id).removeAttr("title");
                    $("#" + elem.id).removeClass("information");
                }
            }

            if (check)
                return true;


            return false;
        }

        function singleRowCheck() {
            var count = 0;
            $(".table tr").each(function () {
                if ($(this).children('td').last().find("a").attr("class").indexOf("lnkRemove") > 0) {
                    count++;
                }
            });

            if (count == 1) {
                $(".lnkRemove").addClass("hide");
            }
        }

        function lastRowCSS() {
            $(".table>tbody>tr:last-child td").each(function () {
                $(this).attr("style", " border-bottom: none !important");
            })
        }

        HTMLInputElement.prototype.startsWith = function () { return false; };
    </script>

    <asp:ScriptManager runat="server" ID="scriptmanager1"></asp:ScriptManager>

    <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12 btns-txt welcome_msg hide" style="padding-bottom: 10px;">
        <div class="alert alert-info">
            <span style="font-size: 16px; font-weight: bold;"><i class="fa fa-fw fa-info-circle" style="color: #2273a5;"></i>
                <asp:Label runat="server" ID="lblhead" Text="Welcome to Avaima's Time & Attendance" CssClass="alert-heading"></asp:Label>
            </span>
            <br />
            <br />
            <div style="margin-left: 25px;">
                <asp:Label runat="server" ID="lbltext" Text="You can start by adding employees using the form below. "></asp:Label>
                <br />
                Once an employee has been added: 
            <ul>
                <li>They can use their own logins to clock-in/out and you can view the updates as an administrator (see instructions at the end of this page) OR</li>
                <li>You can clock-in/out each of your employee yourself</li>
            </ul>
                <asp:LinkButton runat="server" ID="lnkclockinpage" Text="Click here" CssClass="lnkclockinpage"></asp:LinkButton>
                to view clock-in/out page
            </div>
        </div>
    </div>

    <h5 style="margin-top: -20px !important;">Add Employee(s)</h5>
    <hr />
    <span class="about-block" style="color: black!important;">Add one or more employees by entering their information below. All 3 fields are mandatory.</span>

    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <br />
            <asp:Repeater runat="server" ID="rpt_AddEmployeeForm" OnItemCommand="rpt_AddEmployeeForm_ItemCommand" OnItemCreated="rpt_AddEmployeeForm_ItemCreated">
                <HeaderTemplate>
                    <div class="container">
                        <table class="table" cellspacing="0" cellpadding="0">
                            <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="tr_fields">
                        <td>
                            <asp:TextBox ID="txtFirstName" runat="server" EnableViewState="false" Text='<%#DataBinder.Eval(Container.DataItem, "FirstName") %>' CssClass="standardField txtFirstName rpFields"
                                placeholder="First Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'First Name'"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtLastName" runat="server" CssClass="standardField txtLastName rpFields" Text='<%#DataBinder.Eval(Container.DataItem, "LastName") %>'
                                EnableViewState="false" placeholder="Last Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtEmail" placeholder="Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'" CssClass="standardField txtEmail rpFields" Text='<%#DataBinder.Eval(Container.DataItem, "Email") %>' EnableViewState="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:CheckBox runat="server" ID="chk_manage" Text="Employee will clock-in/out him/herself <i style='font-size: 20px; color: #7f7979;'>*</i>" Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "isManaged")) %>' EnableViewState="false" />
                        </td>
                        <td>
                            <asp:LinkButton runat="server" ID="lnkRemove" Text="<i class='fa fa-trash' style='color:red;font-size:15px;'></i>" CommandName="removeRow" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "ID") %>' CssClass="information lnkRemove" ToolTip="Remove"></asp:LinkButton>
                            <asp:HiddenField ID="hdnrowID" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "ID") %>' />
                            <asp:HiddenField runat="server" ID="hdnRowCheck" Value='<%#DataBinder.Eval(Container.DataItem, "RowCheck") %>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                </table>
                   </div>
                </FooterTemplate>
            </asp:Repeater>
            <br />
            <div style="margin-left: 10px;">
                <asp:LinkButton runat="server" ID="btnAdd" OnClick="btnAdd_Click" Style="cursor: pointer; font-size: 14px;"><i class="fa fa-fw fa-plus-circle"></i>&nbsp;Add More</asp:LinkButton>
                <asp:Label runat="server" ID="lblMsg" Visible="false" Text="You can only add 10 employees at a time!" Font-Size="Small" ForeColor="Red"></asp:Label>
                <br />
                <br />
                <asp:Button Text="Add Employee(s)" ID="btnSave" OnClick="btnSave_Click" runat="server" CssClass="btn btn-primary" OnClientClick="return validateData();" />
                <asp:Button Text="Cancel" ID="btnCancel" runat="server" CssClass="btn btn-default" OnClientClick="location.reload(true);" />
            </div>
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>

    <span class="about-block"><i style='font-size: 20px; color: #7f7979;'>*</i> <span style="font-weight: bold;">What happens if I allow the employee to clock-in/out him/herself?</span>
        <br />
        Once you enter the employee, they will be sent an email with the information to access their personal time & attendance dashboard using their personal computer or mobile devices. 
        <br />
        Employees will be responsible for opening time & attendance on their own computer to clock-in/out and you will get notifications via email about their activities which you can then monitor. As an admin, you will still have the ability to edit their timings, mark them absent, view their stats, and/or clock them in/out yourself.</span>

</asp:Content>
