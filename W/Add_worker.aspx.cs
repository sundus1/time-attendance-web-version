﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using MyAttSys;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Configuration;
using System.Collections;
using System.Web.Services;

namespace attendence
{
    public partial class Add_worker : AvaimaThirdpartyTool.AvaimaWebPage
    {
        public Dictionary<string, string> TimeZone = new Dictionary<string, string>()
        {
        { "(UTC+00.00) Western Europe Time, London, Lisbon, Casablanca, Monrovia", "(UTC+00.00) Western Europe Time, London, Lisbon, Casablanca, Monrovia"},
        { "(UTC-12.00) Eniwetok, Kwajalein", "(UTC-12.00) Eniwetok, Kwajalein"},
        { "(UTC-11.00) Midway Island, Samoa", "(UTC-11.00) Midway Island, Samoa"},
          { "(UTC-10.00) Hawaii","(UTC-10.00) Hawaii" },
          {"(UTC-09.00) Alaska","(UTC-09.00) Alaska"  },
          { "(UTC-08.00) Pacific Time (US & Canada)","(UTC-08.00) Pacific Time (US & Canada)" },
          {  "(UTC-07.00) Mountain Time (US & Canada)", "(UTC-07.00) Mountain Time (US & Canada)" },
          {  "(UTC-06.00) Central Time (US & Canada), Mexico City", "(UTC-06.00) Central Time (US & Canada), Mexico City" },
          { "(UTC-05.00) Eastern Time (US & Canada), Bogota, Lima, Quito","(UTC-05.00) Eastern Time (US & Canada), Bogota, Lima, Quito" },
          {"(UTC-04.00) Atlantic Time (Canada), Caracas, La Paz","(UTC-04.00) Atlantic Time (Canada), Caracas, La Paz"  },
          {   "(UTC-03.30) Newfoundland", "(UTC-03.50) Newfoundland"},
        {"(UTC-03.00) Brazil, Buenos Aires, Georgetown","(UTC-03.00) Brazil, Buenos Aires, Georgetown" },
        {"(UTC-02.00) Mid-Atlantic","(UTC-02.00) Mid-Atlantic" },
        {"(UTC-01.00) Azores, Cape Verde Islands","(UTC-01.00) Azores, Cape Verde Islands" },
        { "(UTC+01.00) CET(Central Europe Time), Brussels, Copenhagen, Madrid, Paris","(UTC+01.00) CET(Central Europe Time), Brussels, Copenhagen, Madrid, Paris"},
        { "(UTC+02.00) EET(Eastern Europe Time), Kaliningrad, South Africa", "(UTC+02.00) EET(Eastern Europe Time), Kaliningrad, South Africa" },
        {"(UTC+03.00) Baghdad, Kuwait, Riyadh, Moscow, St. Petersburg, Volgograd, Nairobi","(UTC+03.00) Baghdad, Kuwait, Riyadh, Moscow, St. Petersburg, Volgograd, Nairobi" },
        {  "(UTC+03.30) Tehran", "(UTC+03.50) Tehran"},
        {   "(UTC+04.00) Abu Dhabi, Muscat, Baku, Tbilisi",  "(UTC+04.00) Abu Dhabi, Muscat, Baku, Tbilisi" },
        {  "(UTC+04.30) Kabul", "(UTC+04.50) Kabul"},
        {   "(UTC+05.00) Ekaterinburg, Islamabad, Karachi, Tashkent",  "(UTC+05.00) Ekaterinburg, Islamabad, Karachi, Tashkent"},
        { "(UTC+05.30) Bombay, Calcutta, Madras, New Delhi","(UTC+05.50) Bombay, Calcutta, Madras, New Delhi"},
        {  "(UTC+06.00) Almaty, Dhaka, Colombo",   "(UTC+06.00) Almaty, Dhaka, Colombo"},
        { "(UTC+07.00) Bangkok, Hanoi, Jakarta", "(UTC+07.00) Bangkok, Hanoi, Jakarta" },
        {"(UTC+08.00) Beijing, Perth, Singapore, Hong Kong, Chongqing, Urumqi, Taipei","(UTC+08.00) Beijing, Perth, Singapore, Hong Kong, Chongqing, Urumqi, Taipei" },
        { "(UTC+09.00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk",  "(UTC+09.00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk" },
        {  "(UTC+09.30) Adelaide, Darwin",  "(UTC+09.50) Adelaide, Darwin"},
         { "(UTC+10.00) EAST(East Australian Standard), Guam, Papua New Guinea, Vladivostok", "(UTC+10.00) EAST(East Australian Standard), Guam, Papua New Guinea, Vladivostok"},
         { "(UTC+11.00) Magadan, Solomon Islands, New Caledonia","(UTC+11.00) Magadan, Solomon Islands, New Caledonia"},
         {  "(UTC+12.00) Auckland, Wellington, Fiji, Kamchatka, Marshall Island",  "(UTC+12.00) Auckland, Wellington, Fiji, Kamchatka, Marshall Island"},
         { "(UTC+13.00) Nuku'alofa","(UTC+13.00) Nuku'alofa"}
    };


        string[] industry_list = { "Accounting", "Advertising, Branding & Marketing", "Aerospace", "Aviation", "Automotive", "Biotechnology", "BusinessServices(Hotels, Lodging Places)", "Computers (Hardware, Software, Internet)", "Construction", "Home Improvement", "Consulting,Outsourcing, Offshoring", "Engineering", "Architecture", "Finance", "Banking", "Insurance", "Internet / Web 2.0", "Marketing", "MarketResearch", "Public Relations", "Media", "Printing", "Publishing", "Non-Profit", "Pharmaceutical", "Chemical", "Research", "Science", "Retail", "Utilities", "Wholesale", "Utilities (Electric, Gas, Sanitary Services)", "Business", "ProfessionalServices", "Agriculture", "Forestry", "Fishing", "Hunting", "Utilities", "Computer& Electronics Manufacturing", "Other Manufacturing", "Wholesale", "Transportation & Warehousing", "Telecommunications", "Broadcasting", "Information Services & DataProcessing", "Other Information Industry", "RealEstate,Rental & Leasing", "College, University, & Adult Education", "Primary/Secondary (K-12) Education", "Other Education Industry", "HealthCare & SocialAssistance", "Arts,Entertainment,& Recreation", "Hotel & Food Services", "Government & Public Administration", "Military", "Legal Services", "Scientific or Technical Services", "Motion Picture & Video", "E-Commerce", "Insurance & RiskManagement", "Job Seekers  & Careers", "Nanotechnology", "Homemaker", "Religious", "Timber", "Tobacco", "Arms", "Electrical power", "Petroleum", "Fruit production", "Hospitality", "Pulp & paper", "Steel", "Shipbuilding", "Film", "Music", "Mining", "Water", "Direct Selling", "Aircraft", "Airline", "Apparel & accessories", "Brokerage", "Call Centers", "Cargo Handling", "Cosmestics", "Consumer Products", "Electronics", "Executive & Leisure", "Food,Beverage & Tobacco", "MotionPicture & Video", "Newspaper Publishers", "Private Equity", "RealEstate", "Retail & Wholesale", "Soap & Detergent", "Sports", "Accident & Health Insurance", "Agricultural Chemicals", "BuildingMaterialsWholesale", "Dairy Products", "Business Equipment", "Cotton", "Garment", "Jewellery", "Plastic", "Pearl", "Oil", "Shipping", "Textile", "Silk", "Alternative & Renewable Energy", "Rubber", "Coatings & Plastics", "Wireless", "Tea", "Other Industry", "Don't work" };

        DataAccessLayer dal = new DataAccessLayer(false);
        DataAccessLayer dalAvaima = new DataAccessLayer(true);

        Attendance atd = new Attendance();
        private string AppId = "";
        private string Email = "";
        private string Password = "";
        private int rowID = 0;
        private string UserId = "";
        private bool IsSimpleClock = true;
        private string OwnerId;
        private bool isWorker = true;
        public string f = "12:00am", t = "12:00pm";
        public DateTime userTime = new DateTime();
        List<RecordModel> records = new List<RecordModel>();
        DateTime LastSignInTime;
        private bool isAdmin = false;
        public String _InstanceID { get; set; }

        private void UpdateFirstTime(int userId)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Update attendence_management SET Accessed = @Accessed  where ID = @UserId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@UserId", userId);
                    cmd.Parameters.AddWithValue("@Accessed", true);
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public void UpdateTimeZone(string userId, string timezone)
        {
            string parentUserID = atd.getParent(userId);
            DataTable dt = atd.GetTimezone(parentUserID);

            //if (parentUserID == Request.QueryString["id"].ToString())
            //{
            //    atd.SetTimezone(atd.Timezone_datetime(UserId, DateTime.Now.ToString(), ""), TimeZone.First(kvp => kvp.Key == lbltimezone.Text).Value, dt.Rows[0]["dlsaving"].ToInt32(), dt.Rows[0]["dlsavinghour"].ToInt32());
            //}
            //else
            //    atd.SetTimezone(atd.Timezone_datetime(UserId, DateTime.Now.ToString(), ""), dt.Rows[0]["timezone"].ToString(), dt.Rows[0]["dlsaving"].ToInt32(), dt.Rows[0]["dlsavinghour"].ToInt32());

            if (parentUserID == Request.QueryString["id"].ToString())
            {
                atd.SetTimezone(atd.Timezone_datetime(UserId, DateTime.Now.ToString(), ""), TimeZone.First(kvp => kvp.Key == timezone).Value, dt.Rows[0]["dlsaving"].ToInt32(), dt.Rows[0]["dlsavinghour"].ToInt32());
            }
            else
                atd.SetTimezone(atd.Timezone_datetime(UserId, DateTime.Now.ToString(), ""), dt.Rows[0]["timezone"].ToString(), dt.Rows[0]["dlsaving"].ToInt32(), dt.Rows[0]["dlsavinghour"].ToInt32());

        }

        private bool UserFirstTime(int userId)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select * from attendence_management where ID = @UserId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@UserId", userId);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                }
            }
            if (dt == null)
            { return true; }
            else
            {
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["Accessed"] == null || dt.Rows[0]["Accessed"].ToString() == "0")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //hdnCITime.Value = DateTime.Now.TimeOfDay.ToString();

            hdnMessageFlag.Value = "";
            if (_InstanceID == "0") { _InstanceID = App.InstanceID; }
            else { _InstanceID = this.InstanceID; }

            App.IsLocal = false;

            if (!string.IsNullOrEmpty(Request["uid"]))
            {
                UserId = Request["uid"];
            }

            Email = Request.QueryString["e"].ToString().Decrypt();
            Password = Request.QueryString["p"].ToString().Decrypt();
            hdnParentid.Value = Request.QueryString["id"].ToString();
            _InstanceID = Request.QueryString["instanceid"].ToString();

            foreach (KeyValuePair<string, string> entry in TimeZone)
            {
                ddltimezone.Items.Add(new ListItem(entry.Key, entry.Value));
            }

            DataTable userSettings = JsonConvert.DeserializeObject<DataTable>(atd.GetSettingInfo(Email, Password));
            AppId = userSettings.Rows[0]["userid"].ToString();
            ddltimezone.SelectedValue = lbltimezone.Text = userSettings.Rows[0]["timezone"].ToString();
            hdndlsaving.Value = userSettings.Rows[0]["dlsaving"].ToString();
            hdndlsavinghour.Value = userSettings.Rows[0]["dlsavinghour"].ToString();

            hdnCITime.Value = App.GetTime(DateTime.UtcNow.TimeOfDay.ToString(), AppId);

            //Get User Role
            //using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            //{
            //    using (SqlCommand cmd = new SqlCommand("sp_getuserrole", con))
            //    {
            //        con.Open();
            //        cmd.Parameters.AddWithValue("@userid", hdnParentid.Value);
            //        cmd.CommandType = CommandType.StoredProcedure;
            //        SqlDataAdapter adp = new SqlDataAdapter(cmd);
            //        DataTable dt = new DataTable();
            //        adp.Fill(dt);
            //        if (dt.Rows.Count > 0)
            //        {
            //            if (Convert.ToString(dt.Rows[0]["role"]) == "superuser" || Convert.ToString(dt.Rows[0]["role"]) == "admin")
            //            {
            //                userprofile_link.Visible = true;
            //            }
            //        }
            //    }
            //}

            //hdnCITime.Value = App.GetTime(DateTime.UtcNow.TimeOfDay.ToString(), AppId);

            UndoMsg.Visible = false;

            ////Check Daylight Savings
            //string AutoDetectedtimezone = atd.getStandardTime(Request.UserHostAddress);
            //if (!string.IsNullOrEmpty(AutoDetectedtimezone))
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "dst", "checkUserStandartTime('" + AutoDetectedtimezone + "'); ", true);
            //}
            //else
            //    ScriptManager.RegisterStartupScript(this, GetType(), "dst", "console.log(NO OFFSET FOUND);", true);
            ////End

            if (!IsPostBack)
            {
                string isVerified = atd.VerifyLogin(Email, Password);
                if (isVerified != "TRUE")
                    this.Redirect("Info.aspx");

                if (UserFirstTime(UserId.ToInt32()) == true)
                {
                    //Auto-detect timezone by IP
                    MapTimeZone();

                    //UpdateTimeZone(UserId);

                    //Show relavant notifications to user on First login
                    if (hdnParentid.Value == UserId)
                    {
                        string AutoDetectedtimezone = atd.getStandardTime(Request.UserHostAddress);
                        ScriptManager.RegisterStartupScript(this, GetType(), "dst", "updateTimeZoneSettings_FirstTime('" + AutoDetectedtimezone + "'); ", true);

                        ScriptManager.RegisterStartupScript(this, GetType(), "showFirstTime", "showFirstTime();", true);

                        if (CheckisAdmin() != 0)
                            ScriptManager.RegisterStartupScript(this, GetType(), "showFirstTime_Bio", "showFirstTime_Bio(); ", true);

                        UpdateFirstTime(UserId.ToInt32());
                        atd.WelcomeEmail(UserId);
                    }
                }

                //DataTable userSettings = JsonConvert.DeserializeObject<DataTable>(atd.GetSettingInfo(Email, Password));
                //AppId = userSettings.Rows[0]["userid"].ToString();

                //SetUserStatus();

                //try
                //{
                //    if (!String.IsNullOrEmpty(Request["allowed"].ToString()))
                //    {
                //        ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('You are not allowed to view this page');", true);
                //    }
                //}
                //catch (Exception ex) { }

                //MyAttSys.avaimaTest0001DB db = new avaimaTest0001DB();

                //if (Convert.ToBoolean(hdnIsWeekend.Value))
                //{
                //    lblErrTitle.Text = "Weekend Notification";
                //    // lblErrDetail.Text = "Today is a holiday. You cannot clock-in but you can add OVER-Time";
                //    lblErrDetail.Text = "Today is a holiday. You cannot clock-in";
                //    ScriptManager.RegisterStartupScript(this, GetType(), "", "OpenDialogWB('#divErr');", true);
                //}

                GetIPVerification();
                FillTagTable();
                // LoadEmployees();
            }

            SetUserStatus();
            // GetYearStats(hdnUserID.Value.ToInt32(), DateTime.Now.Year, InstanceID);
            //tr4.Visible = 
            tr1.Visible = true;
            tr2.Visible = true;

            //Hours worked in a month
            DateTime startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime endDate;

            if (startDate.Month == DateTime.Now.Month)
                endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            else
                endDate = startDate.AddMonths(1).AddDays(-1);

            //Get User Parent ID
            int parentid = atd.getParent(UserId).ToInt32();

            DataTable dtAbsencesM = atd.GetYearlyAbsences(UserId.ToInt32(), startDate, endDate);
            lblTotalAbsencesM.Text = dtAbsencesM.Rows.Count.ToString() + " day(s)";

            //string totalWorkedM = GetWorkedHoursInMonth(Convert.ToInt16(UserId), startDate, endDate);

            DataTable dtAttendanceM = atd.GetYearlyAttendance(UserId.ToInt32(), startDate, endDate);
            TimeSpan totalWorkedHours = atd.GetTotalWorkedTime(dtAttendanceM, parentid, AppId, Request.QueryString["instanceid"].ToString(), DateTime.Now.Year);

            //string totalWorkedM = Math.Round(totalWorkedHours.TotalHours) + " Hours " + ((totalWorkedHours.Minutes != 0) ? totalWorkedHours.Minutes + " Mins" : "");
            string totalWorkedM = atd.GetTotalHours(totalWorkedHours) + " Hrs " + ((totalWorkedHours.Minutes != 0) ? totalWorkedHours.Minutes + " Mins" : "");

            if (totalWorkedM == "")
            {
                lblTotalWorkedHourM.Text = "0";
            }
            else
            {
                if (atd.GetWorkingHours(parentid).Rows.Count > 0)
                {
                    List<TimeSpan> TotalWorkedHoursAndBreak = atd.WorkedHoursAndBreak(UserId.ToInt32(), startDate, endDate, AppId, Request.QueryString["instanceid"].ToString());
                    lblTotalWorkedHourM.Text = atd.ConvertTimeSpanInHours(TotalWorkedHoursAndBreak[0].Add(TotalWorkedHoursAndBreak[1])) + " (&nbsp;<b>Worked:</b> " + atd.ConvertTimeSpanInHours(TotalWorkedHoursAndBreak[0]) + " - <b>Break:</b> " + atd.ConvertTimeSpanInHours(TotalWorkedHoursAndBreak[1]) + ")";
                }
                else
                    lblTotalWorkedHourM.Text = atd.ConvertTimeSpanInHours(totalWorkedHours);
            }


            //Get current month Unmarked Days
            DateTime user_joindate = SP.GetWorkerFirstSignInThisYear(UserId, DateTime.Now.Year);
            if (user_joindate > startDate && (startDate.Month == user_joindate.Month))
            {
                startDate = user_joindate;
            }

            DateTime unmarked_endDate = (endDate.Date == DateTime.Now.Date) ? endDate.AddDays(-1) : endDate;
            dtAttendanceM = atd.GetYearlyAttendance(UserId.ToInt32(), startDate, unmarked_endDate);
            dtAbsencesM = atd.GetYearlyAbsences(UserId.ToInt32(), startDate, unmarked_endDate);
            DataTable dtHistory = new DataTable();
            dtHistory.Columns.Add("DateTime", typeof(DateTime));
            dtHistory.Columns.Add("Day");
            dtHistory.Columns.Add("Clock-in");
            dtHistory.Columns.Add("Clock-out");
            dtHistory.Columns.Add("Time Spent");
            dtHistory.Merge(atd.FormatDataTable(dtAttendanceM, "attendance", AppId));
            dtHistory.Merge(atd.FormatDataTable(dtAbsencesM, "absence", AppId));
            string unmarked_dates = atd.GetUnmarkedDays(dtHistory, startDate, unmarked_endDate, parentid, DateTime.Now.Year);
            if (unmarked_dates != "")
            {
                lblTotalUnmarkedDaysM.Text = unmarked_dates.Split(',').Count().ToString() + " day(s)";
                lnkViewUnmark.HRef = atd.WebURL("unmarked", UserId, Request.QueryString["instanceid"].ToString(), Request.QueryString["e"].Decrypt(), Request.QueryString["p"].Decrypt(), UserId.ToString(), startDate.ToString(), unmarked_endDate.ToString());
                tr3.Visible = true;
            }
            //End

            startDate = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
            endDate = SP.GetWorkerLastSignInThisYear(UserId, DateTime.Now.Year);
            lblTotalAbsencesR.Text = atd.GetYearlyAbsences(UserId.ToInt32(), startDate, endDate).Rows.Count.ToString() + " day(s)";

            lnkViewAbs.HRef = "Absence.aspx?id=" + Request.QueryString["id"].ToString() + "&uid=" + Request.QueryString["uid"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
            lnkViewStats.HRef = "Statistics.aspx?id=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString() + "&uid=" + Request.QueryString["uid"].ToString();
            lnk_settingpage.HRef = lnk_settingpage1.HRef = "settings.aspx?id=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString() + "&uid=" + Request.QueryString["uid"].ToString();

            //Hours worked in a year
            //string totalWorked = GetWorkedHoursInYear(Convert.ToInt16(UserId));
            //if (totalWorked == "")
            //{
            //    lblTotalWorkedHourY.Text = "0";
            //}
            //else
            //{
            //    lblTotalWorkedHourY.Text = totalWorked;
            //}

            //Check Error Msg
            if (Session["errorMsg"] != null)
                ScriptManager.RegisterStartupScript(this, GetType(), "", "openConfirmBox('Invalid Range -Time overlapping an existing entry on this day.');", true);

            Session["errorMsg"] = null;
        }

        public int CheckisAdmin()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select count(*) FROM schema_6e815b00_6e13_4839_a57d_800a92809f21.attendence_management  where ID = @userid and instanceid = @instanceid and parentid = 0", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@userid", UserId);
                    cmd.Parameters.AddWithValue("@instanceid", Request.QueryString["instanceid"].ToString());
                    return ((int)cmd.ExecuteScalar());
                }
            }
        }

        private void MapTimeZone()
        {
            try
            {
                ////Test URL (India): http://gd.geobytes.com/GetCityDetails?ip=103.43.40.69
                //HttpWebRequest request = WebRequest.Create("http://gd.geobytes.com/GetCityDetails?fqcn=" + Request.UserHostAddress) as HttpWebRequest;

                //using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                //{
                //    if (response.StatusCode != HttpStatusCode.OK)
                //        throw new Exception(String.Format(
                //        "Server error (HTTP {0}: {1}).",
                //        response.StatusCode,
                //        response.StatusDescription));

                //    string stringResponse = "";
                //    string[] data;
                //    using (System.IO.Stream resp = response.GetResponseStream())
                //    {
                //        System.IO.StreamReader sr = new System.IO.StreamReader(resp);
                //        stringResponse = sr.ReadToEnd();
                //        data = stringResponse.Replace('\"', ' ').Replace("{", "").Replace("}", "").Replace("\n", "").Split(',');
                //        string offset = data[19].Substring(data[19].IndexOf(':') + 1).Trim();
                //        if (!string.IsNullOrEmpty(offset))
                //            lbltimezone.Text = TimeZone_offset.First(kvp => kvp.Key == offset).Value;
                //        else
                //            lbltimezone.Text = "(UTC+00.00) Western Europe Time, London, Lisbon, Casablanca, Monrovia";
                //    }
                //}

                string timezone = atd.getTimeZone(Request.UserHostAddress);

                //Update detected timezone
                UpdateTimeZone(UserId, timezone);

                ddltimezone.SelectedValue = lbltimezone.Text = timezone;

            }
            catch (Exception ex)
            {
                AvaimaEmailAPI email = new AvaimaEmailAPI();
                StringBuilder body = new StringBuilder();
                body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                body.Append("<p><b>API Used: </b>http://gd.geobytes.com/GetCityDetails <br>");
                body.Append("<b>Error Message:</b> " + Request.UserHostAddress + "<br>");
                body.Append("<b>Error Message:</b> " + ex.Message.ToString() + "<br>");
                body.Append("<b>Detail:</b> " + ex.StackTrace.ToString() + "</p>");

                body.Append(Helper.AvaimaEmailSignature);
                body.Append("</div>");
                try
                {
                    //email.send_email("support@avaima.com", "AVAIMA", "", body.ToString(), "Auto IP Detection Exception");
                    email.send_email("sundus_csit@yahoo.com", "AVAIMA", "", body.ToString(), "Auto IP Detection Exception");
                }
                catch (Exception exception)
                {
                }
            }
        }

        private string GetWorkedHoursInYear(int userId)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_GetWorkedHourInYear", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@userid", userId);

                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);

                    adp.Fill(dt);
                }
            }
            TimeSpan WorkedHours = new TimeSpan();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["lastout"].ToString() == null || dt.Rows[i]["lastout"].ToString() == "")
                {
                    WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(DateTime.Now).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
                }
                else
                {
                    WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastout"].ToString())).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
                }
            }

            //return ConvertTimeSpan(WorkedHours) + " (" + Math.Ceiling(Math.Round(WorkedHours.TotalHours, 3)) + " Hours)";

            if (WorkedHours.TotalHours != 0)
                return atd.GetTotalHours(WorkedHours) + " Hours " + ((WorkedHours.Minutes != 0) ? WorkedHours.Minutes + " Mins" : "");
            //return Math.Round(WorkedHours.TotalHours) + " Hours " + ((WorkedHours.Minutes != 0) ? WorkedHours.Minutes + " Mins" : "");
            else
                return (WorkedHours.Minutes != 0) ? WorkedHours.Minutes + " Mins" : "0 Hours";

            //return Math.Ceiling(Math.Round(WorkedHours.TotalHours, 3)) + " Hours";

            //return ConvertTimeSpan(WorkedHours);
        }

        private string GetWorkedHoursInMonth(int userId, DateTime start, DateTime end)
        {
            DataTable dt = atd.GetYearlyAttendance(userId, start, end);

            TimeSpan WorkedHours = new TimeSpan();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["lastout"].ToString() == null || dt.Rows[i]["lastout"].ToString() == "")
                {
                    WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(DateTime.Now).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
                }
                else
                {
                    WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastout"].ToString())).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
                }
            }

            if (WorkedHours.TotalHours != 0)
                return atd.GetTotalHours(WorkedHours) + " Hours " + ((WorkedHours.Minutes != 0) ? WorkedHours.Minutes + " Mins" : "");
            //return Math.Round(WorkedHours.TotalHours) + " Hours " + ((WorkedHours.Minutes != 0) ? WorkedHours.Minutes + " Mins" : "");
            else
                return (WorkedHours.Minutes != 0) ? WorkedHours.Minutes + " Mins" : "0 Hours";

            //return Math.Ceiling(Math.Round(WorkedHours.TotalHours, 3)) + " Hours";

            // return ConvertTimeSpan(WorkedHours);
        }

        //private void GenerateStatistics(List<RecordModel> records, DateTime firstDateOfWeek)
        //{
        //    List<DateTime> wholeWeek = new List<DateTime>();
        //    List<Absence> absences = Absence.GetAbsences(Convert.ToInt32(UserId)).Where(u => u.Active == true).ToList();
        //    TimeSpan tsworkingHoursShouldBe = new TimeSpan(0);
        //    TimeSpan tsworkingHours = new TimeSpan(0);
        //    TimeSpan tsbreakhours = new TimeSpan(0);
        //    TimeSpan tsbreakhoursShouldBe = new TimeSpan(0);
        //    TimeSpan tsWeekendWorkedHours = new TimeSpan(0);
        //    int iDaysWorked = 0;
        //    for (DateTime i = firstDateOfWeek; i <= DateTime.Now.Date; i = i.AddDays(1)) {
        //        foreach (var record in records.Where(u => u.Date.Date == i.Date).ToList()) {
        //            if (record.DayTitle != null && record.Records.Count > 0) {
        //                tsWeekendWorkedHours += record.WorkingHours;
        //                tsbreakhours += record.TotalBreakTS;
        //                tsbreakhoursShouldBe += record.TotalBreakShouldBeTS;
        //                ++iDaysWorked;
        //            }
        //            else if (record.Records.Count > 0 && record.DayTitle == null) {
        //                tsworkingHours += record.TotalWorkedHoursTS;
        //                tsbreakhours += record.TotalBreakTS;
        //                tsbreakhoursShouldBe += record.TotalBreakShouldBeTS;
        //                tsworkingHoursShouldBe += record.WorkingHours;
        //                ++iDaysWorked;
        //            }
        //        }
        //    }
        //    //lblEstWorkingHours.Text = UtilityMethods.getFormatedTimeByMinutes(tsworkingHoursShouldBe.TotalMinutes.ToInt32());
        //    //lblWorkHours.Text = UtilityMethods.getFormatedTimeByMinutes(tsworkingHours.TotalMinutes.ToInt32());
        //    //lblBreaks.Text = UtilityMethods.getFormatedTimeByMinutes(tsbreakhours.TotalMinutes.ToInt32()) + " (should be " + UtilityMethods.getFormatedTimeByMinutes(tsbreakhoursShouldBe.TotalMinutes.ToInt32()) + ")";
        //    //lblExtraDays.Text = UtilityMethods.getFormatedTimeByMinutes(tsWeekendWorkedHours.TotalMinutes.ToInt32());
        //    //lblTotDays.Text = iDaysWorked.ToString();
        //}

        private static void CalculateTime(ref int bTotShouldHrs, ref int bTotShouldMins, string[] workedhours)
        {
            if (workedhours.Count() > 3)
            {
                bTotShouldHrs += Convert.ToInt32(workedhours[0]);
                bTotShouldMins += Convert.ToInt32(workedhours[2]);
                if (bTotShouldMins >= 60)
                {
                    bTotShouldHrs += Convert.ToInt32((bTotShouldMins / 60));
                    bTotShouldMins -= 60;
                }
            }
            else
            {
                if (Convert.ToInt32(workedhours[0]) > 0)
                {
                    bTotShouldMins += Convert.ToInt32(workedhours[0]);
                    if (bTotShouldMins >= 60)
                    {
                        bTotShouldHrs += Convert.ToInt32((bTotShouldMins / 60));
                        bTotShouldMins -= 60;
                    }
                }
            }
        }
        public List<DateTime> GetDatesBetween(DateTime startDate, DateTime endDate)
        {
            List<DateTime> allDates = new List<DateTime>();
            for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                allDates.Add(date);
            return allDates;

        }
        public DataTable GetAbsences(Int32 UserID)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_GetAbsences", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    cmd.Parameters.AddWithValue("@userID", UserID);
                    adp.Fill(dt);

                }
            }
            return dt;
        }
        private string ConvertTimeSpan(TimeSpan WorkedHours)
        {
            string strWorkingHours = String.Empty;
            if (WorkedHours.Days > 0)
            {
                strWorkingHours = WorkedHours.Days + " Days";
            }
            if (WorkedHours.Hours > 0)
            {
                strWorkingHours += " " + WorkedHours.Hours + " Hrs";
            }
            if (WorkedHours.Minutes > 0)
            {
                strWorkingHours += " " + WorkedHours.Minutes + " Mins";
            }
            //if (WorkedHours.Seconds > 0)
            //{
            //    strWorkingHours += " " + WorkedHours.Seconds + " Seconds";
            //}
            return strWorkingHours;
        }

        public DataTable GetFirstTimeSignIn(Int32 UserID)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_GetFirstTimeSignIn", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    cmd.Parameters.AddWithValue("@userID", UserID);
                    adp.Fill(dt);

                }
            }
            return dt;
        }
        private void CreateTable(DataTable dt)
        {
            DataColumn newColumn = new DataColumn("ManualRecord", typeof(System.String));
            newColumn.DefaultValue = "0";
            dt.Columns.Add(newColumn);
            DataTable dtHolidays = GetHolidays("holiday");
            DataTable dtWeekends = GetHolidays("weekend");
            DataTable dtAbsence = GetAbsences(Convert.ToInt32(Request.QueryString["uid"].ToString()));
            AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
            tblHistory.Rows.Clear();
            //tblHistory.CssClass = "smallGrid";
            tblHistory.CssClass = "tbl-grd-last-clmn";
            TableRow trow = new TableRow();
            trow.CssClass = "smallGridHead";
            TimeSpan TotalWoked_GridHours = new TimeSpan();

            #region IP Address
            DataTable dtIPs = SP.GetIPAddresses(Request.QueryString["instanceid"].ToString());
            #endregion

            #region AdditionalWork Variables
            TimeSpan WeekendWork = new TimeSpan();
            TimeSpan HolidayWork = new TimeSpan();

            #endregion

            #region Parent Working hours definition
            DataTable WorkingHours = new DataTable();
            WorkingHours = atd.GetWorkingHours(atd.getParent(Request.QueryString["uid"].ToString()).ToInt32());
            #endregion

            #region Table Headers
            trow.Cells.Add(new TableCell() { Text = "" });
            trow.Cells.Add(new TableCell() { Text = "Clock In" });
            //trow.Cells.Add(new TableCell() { Text = "Clock In Location" });
            trow.Cells.Add(new TableCell() { Text = "Clock Out" });
            //trow.Cells.Add(new TableCell() { Text = "Clock out Location" });

            if (WorkingHours.Rows.Count > 0)
            {
                TableCell rowCell = new TableCell();

                Label lbltxt = new Label();
                lbltxt.Text = "Time Spent ";
                rowCell.Controls.Add(lbltxt);

                Label lbltxticon = new Label();
                lbltxticon.Text = "<i class='fa fa-question-circle'></i>";
                lbltxticon.ToolTip = "Work done + Break Time (if break is defined by the admin)";
                lbltxticon.CssClass = "lbltxticon information";
                rowCell.Controls.Add(lbltxticon);

                //Image InfoIcon = new Image();
                //InfoIcon.ToolTip = "Worked Hours + Break Time";
                //InfoIcon.CssClass = "tooltip statusDetails";
                //InfoIcon.ImageUrl = "../images/controls/icon_info.png";
                //rowCell.Controls.Add(InfoIcon);

                trow.Cells.Add(rowCell);
            }
            else
            {
                trow.Cells.Add(new TableCell() { Text = "Time Spent" });
            }
            trow.Cells.Add(new TableCell() { Text = "" });
            trow.Cells.Add(new TableCell() { Text = "" });
            tblHistory.Rows.Add(trow);
            #endregion        

            DateTime dtnow = DateTime.Now;
            DateTime dtstart;
            DataTable dtFirstClockin = GetFirstTimeSignIn(Convert.ToInt32(Request.QueryString["uid"].ToString()));
            if (dtFirstClockin.Rows.Count > 0)
            {
                if (Convert.ToDateTime(dtFirstClockin.Rows[0]["lastin"].ToString()).Date <= dtnow.AddDays(-7).Date)
                {
                    dtstart = dtnow.AddDays(-7);
                }
                else
                {
                    int dts = (dtnow - Convert.ToDateTime(dtFirstClockin.Rows[0]["lastin"].ToString())).Days;
                    dtstart = dtnow.AddDays(-dts);
                }
            }
            else
            {
                dtstart = dtnow.AddDays(0);
            }
            //  DateTime dtstart = dtnow.AddDays(-7);
            List<DateTime> lst = GetDatesBetween(dtstart, dtnow);

            for (int j = 0; j < lst.Count; j++)
            {
                DataRow[] drs = dt.Select("FormatedDate = '" + lst[j].ToString("MM/dd/yyyy") + "'");
                if (drs.Length == 0)
                {
                    DataRow dr = dt.NewRow();
                    dr["lastin"] = lst[j];
                    dr["ManualRecord"] = 1;
                    dr["FormatedDate"] = lst[j].ToString("MM/dd/yyyy");
                    dt.Rows.Add(dr);
                }
            }

            bool allowAddHours = false;
            bool allowAddAbsence = false;

            int idS = 0;
            dt.DefaultView.Sort = "lastin desc";
            dt = dt.DefaultView.ToTable();
            int count = 0, totalrows = 0;
            DataRow[] drMultipleClockin = null;
            int a = 0;
            DateTime CurrentDateTime = Convert.ToDateTime(GetDate(objATZ.GetDate(DateTime.UtcNow.ToString(), AppId)));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int r = 0;

                if (count == totalrows)
                {
                    totalrows = 0;
                    count = 0;
                }
                if (count == 0)
                {
                    drMultipleClockin = dt.Select("FormatedDate='" + Convert.ToDateTime(dt.Rows[i]["lastin"]).ToString("MM/dd/yyyy") + "'");
                    totalrows = drMultipleClockin.Length;
                }
                else
                {
                    drMultipleClockin = null;

                }

                string tempLastIn = dt.Rows[i]["lastin"].ToString();
                //Add additional clock-in/out on selected day for ADMIN only
                Image imgAddHours = new Image();
                imgAddHours.AlternateText = " - Add Hours";
                imgAddHours.ImageUrl = "../images/Controls/add.png";
                imgAddHours.ID = "imgAddHours" + ++idS + "" + dt.Rows[i]["rowID"];
                imgAddHours.CssClass = "imgAddHours tooltip managedUser";
                imgAddHours.ToolTip = "Add Clock-in / Clock-out Time.";
                imgAddHours.Attributes.Add("Title", "Add Time");
                imgAddHours.Attributes.Add("data-frmTime", "09:00 am");
                imgAddHours.Attributes.Add("data-toTime", "06:00 pm");
                imgAddHours.Attributes.Add("data-frmdate", dt.Rows[i]["FormatedDate"].ToString());
                imgAddHours.Attributes.Add("data-todate", dt.Rows[i]["FormatedDate"].ToString());
                imgAddHours.Style.Add("float", "right");
                imgAddHours.Style.Add("width", "12px");
                imgAddHours.Style.Add("margin", "3px -5px 3px 0px");
                imgAddHours.Style.Add("cursor", "pointer");
                //End


                //Add Absence on selected day for ADMIN only
                Image imgAddAbsence = new Image();
                imgAddAbsence.AlternateText = " - Add Absence";
                imgAddAbsence.ImageUrl = "../images/Controls/minus.png";
                imgAddAbsence.ID = "imgAddAbsence" + ++idS + "" + dt.Rows[i]["rowID"];
                imgAddAbsence.CssClass = "imgAddAbsence tooltip managedUser";
                imgAddAbsence.ToolTip = "Add Absence";
                imgAddAbsence.Attributes.Add("Title", "Add Absence");
                imgAddAbsence.Attributes.Add("data-userid", UserId);
                imgAddAbsence.Attributes.Add("data-date", dt.Rows[i]["FormatedDate"].ToString());
                imgAddAbsence.Style.Add("float", "right");
                imgAddAbsence.Style.Add("width", "12px");
                imgAddAbsence.Style.Add("margin", "3px 3px 3px 5px");
                imgAddAbsence.Style.Add("cursor", "pointer");
                //End

                TableRow tRowRecord = new TableRow();
                string todaysDate = "";
                if (Convert.ToDateTime(dt.Rows[i]["lastin"].ToString()).Date == CurrentDateTime.Date)
                {
                    todaysDate = "Today";
                }
                else
                {
                    //todaysDate = Convert.ToDateTime(dt.Rows[i]["lastin"].ToString()).ToString("ddd, MMM d yyy");
                    todaysDate = Convert.ToDateTime(dt.Rows[i]["lastin"].ToString()).ToString("ddd, MMM d");

                }

                //if (todaysDate == "Today")
                //{
                //tRowRecord.Cells.Add(new TableCell() { Text = todaysDate });
                // }
                // else //Except Today
                // {
                if (drMultipleClockin != null)
                {
                    r = 0;
                    int b = a;

                    for (a = a; a < (b + totalrows); a++)
                    {
                        r = (dt.Rows[a]["signin"] != null && dt.Rows[a]["signin"].ToString() != "") || (dt.Rows[a]["signout"] != null && dt.Rows[a]["signout"].ToString() != "") ? r + 1 : r;
                    }

                    TableCell tcell = new TableCell();
                    Label lblDate = new Label();
                    lblDate.Text = todaysDate;
                    tcell.Controls.Add(lblDate);

                    if (drMultipleClockin.Length > 1)
                    {
                        tcell.RowSpan = drMultipleClockin.Length + r;
                        //tRowRecord.Cells.Add(new TableCell() { Text = todaysDate, RowSpan = drMultipleClockin.Length + r });
                    }
                    else
                    {
                        if (r == 1)
                            r = 2;
                        tcell.RowSpan = r;
                        //tRowRecord.Cells.Add(new TableCell() { Text = todaysDate, RowSpan = r });
                    }

                    //Check if Superadmin or Admin
                    bool isAdmin = false;
                    if (atd.getParent(UserId) == Request.QueryString["id"].ToString() || atd.isAdmin(Request.QueryString["id"].ToInt32()))
                        isAdmin = true;

                    //Add clockin/out entry option - For Admin only
                    if (todaysDate == "Today" && dt.Rows[i]["ManualRecord"].ToString() != "1" && dt.Rows[i]["lastout"].ToString() == "")
                    { allowAddHours = false; }
                    else
                    {
                        if (isAdmin)
                        {
                            allowAddHours = true;
                        }
                    }

                    if (allowAddHours)
                    {
                        tcell.Controls.Add(imgAddHours);
                    }
                    tRowRecord.Cells.Add(tcell);
                    //End

                    //Add Absence option on a clocked-in day - For Admin only
                    if (dt.Rows[i]["ManualRecord"].ToString() != "1" && dt.Rows[i]["lastin"].ToString() != "")
                    {
                        if (isAdmin)
                            allowAddAbsence = true;
                    }
                    else
                        allowAddAbsence = false;

                    if (allowAddAbsence)
                    {
                        DataRow[] isAbsences = dtAbsence.Select("absentDate='" + Convert.ToDateTime(dt.Rows[i]["lastin"]).Date + "'");
                        if (isAbsences.Length == 0)
                            tcell.Controls.Add(imgAddAbsence);
                    }
                    tRowRecord.Cells.Add(tcell);
                    //End
                }
                count++;
                // }

                if (dt.Rows[i]["ManualRecord"].ToString() == "1") //Means No Record Entry, added when mapping
                {

                    //DataRow[] drholdays = dtHolidays.Select("date='" + Convert.ToDateTime(dt.Rows[i]["lastin"]).ToString("M/d/yyyy") + " 12:00:00 AM" + "' and Type='1'");


                    DataRow[] drholdays = dtHolidays.Select("(date='" + Convert.ToDateTime(dt.Rows[i]["lastin"]).ToString("M/d/yyyy") + " 12:00:00 AM" + "') or ( Convert(date, 'System.String') LIKE  '%" + Convert.ToDateTime(dt.Rows[i]["lastin"]).ToString("/M/dd") + "%' AND  everyyear = 1) and Type='1'");

                    if (drholdays.Length > 0) //Holiday
                    {
                        TableCell tcellhw = new TableCell();

                        tRowRecord.Attributes.Add("class", "yellowRow");
                        //tRowRecord.Cells.Add(new TableCell() { Text = drholdays[0]["Title"].ToString(), ColumnSpan = 4 });
                        //tRowRecord.Cells[1].Font.Bold = true;
                        //tRowRecord.Cells.Add(new TableCell() { ColumnSpan = 3 });
                        tRowRecord.Cells.Add(new TableCell() { ColumnSpan = 4 });
                        //Add hours
                        //if (allowAddHours)
                        //{
                        //    tRowRecord.Cells.Add(new TableCell() { ColumnSpan = 3 });
                        //    tcellhw.Controls.Add(imgAddHours);
                        //    tRowRecord.Cells.Add(tcellhw);
                        //}
                        //else
                        //{
                        //    tRowRecord.Cells.Add(new TableCell() { ColumnSpan = 4 });
                        //}
                        tRowRecord.Cells.Add(new TableCell() { Text = "Holiday", CssClass = "information" });
                        tRowRecord.Cells[tRowRecord.Cells.Count - 1].Font.Bold = true;
                        tRowRecord.Cells[tRowRecord.Cells.Count - 1].ToolTip = drholdays[0]["Title"].ToString();
                    }
                    else
                    {
                        DataRow[] drWeekned = dtWeekends.Select("Title='" + Convert.ToDateTime(dt.Rows[i]["lastin"].ToString()).DayOfWeek + "' and Type='0'");
                        if (drWeekned.Length > 0) //Weekend
                        {
                            TableCell tcellhw = new TableCell();

                            tRowRecord.Attributes.Add("class", "greenRow");
                            tRowRecord.Cells.Add(new TableCell() { ColumnSpan = 4 });
                            //tRowRecord.Cells[1].Font.Bold = true;                          
                            //Add hours
                            //if (allowAddHours)
                            //{
                            //    tRowRecord.Cells.Add(new TableCell() { ColumnSpan = 3 });
                            //    tcellhw.Controls.Add(imgAddHours);
                            //    tRowRecord.Cells.Add(tcellhw);
                            //}
                            //else
                            //{
                            //    tRowRecord.Cells.Add(new TableCell() { ColumnSpan = 4 });
                            //}
                            tRowRecord.Cells.Add(new TableCell() { Text = "Weekend" });
                            tRowRecord.Cells[tRowRecord.Cells.Count - 1].Font.Bold = true;
                        }
                        else
                        {
                            DataRow[] drAbsences = dtAbsence.Select("absentDate='" + Convert.ToDateTime(dt.Rows[i]["lastin"]).Date + "'");
                            if (drAbsences.Length > 0) //Absent
                            {
                                TableCell tcellhw = new TableCell();
                                //Label lblText = new Label();
                                //lblText.ID = "lblText" + ++idS;
                                //lblText.CssClass = "lblText";
                                //if (drAbsences[0]["Comment"].ToString() == "")
                                //    lblText.Text = "Absent - (No reason given) ";
                                //else
                                //    lblText.Text = "Absent - (" + drAbsences[0]["Comment"].ToString() + ") ";
                                //tcellhw.Controls.Add(lblText);

                                Label lblText = new Label();
                                lblText.ID = "lblText" + ++idS;
                                lblText.CssClass = "lblText";
                                lblText.Text = "";
                                tcellhw.Controls.Add(lblText);
                                LinkButton lnkMarkAbsence = new LinkButton();
                                lnkMarkAbsence.ID = "lnkMarkPresent" + idS;
                                lnkMarkAbsence.CssClass = "lnkMarkAbsence managedUser";
                                lnkMarkAbsence.CommandName = "lnkMarkAbsence";
                                lnkMarkAbsence.Text = "Remove absence";
                                lnkMarkAbsence.CommandArgument = Convert.ToDateTime(dt.Rows[i]["lastin"]).Date.ToString();
                                lnkMarkAbsence.Attributes.Add("data-id", drAbsences[0]["AbsLogID"].ToString());
                                lnkMarkAbsence.Attributes.Add("data-userid", drAbsences[0]["UserID"].ToString());
                                lnkMarkAbsence.Attributes.Add("data-date", drAbsences[0]["CrtDate"].ToString());
                                lnkMarkAbsence.Attributes.Add("data-active", "False");
                                lnkMarkAbsence.PostBackUrl = "";
                                //tcellhw.Controls.Add(lblText);
                                tcellhw.Controls.Add(lnkMarkAbsence);
                                tRowRecord.Attributes.Add("class", "absentRow");
                                tcellhw.ColumnSpan = 4;
                                tcellhw.Font.Bold = true;
                                //Add hours
                                //if (allowAddHours)
                                //{
                                //    tcellhw.Controls.Add(imgAddHours);
                                //}
                                tRowRecord.Cells.Add(tcellhw);
                                tRowRecord.Cells.Add(new TableCell() { Text = "Absent", CssClass = "information" });
                                tRowRecord.Cells[tRowRecord.Cells.Count - 1].Font.Bold = true;
                                tRowRecord.Cells[tRowRecord.Cells.Count - 1].ToolTip = drAbsences[0]["Comment"].ToString();
                            }
                            else
                            {
                                TableCell tcellhw = new TableCell();
                                Label lblText = new Label();
                                lblText.ID = "lblText" + ++idS;
                                lblText.CssClass = "lblText";
                                lblText.Text = "No Entry - ";
                                LinkButton lnkMarkAbsence = new LinkButton();
                                lnkMarkAbsence.ID = "lnkMarkAbsence" + idS;
                                lnkMarkAbsence.CssClass = "lnkMarkAbsence managedUser";
                                lnkMarkAbsence.CommandName = "lnkMarkAbsence";
                                lnkMarkAbsence.Text = "Mark as absent";
                                lnkMarkAbsence.CommandArgument = Convert.ToDateTime(dt.Rows[i]["lastin"]).Date.ToString();
                                lnkMarkAbsence.Attributes.Add("data-id", "0");
                                lnkMarkAbsence.Attributes.Add("data-userid", UserId);
                                lnkMarkAbsence.Attributes.Add("data-date", Convert.ToDateTime(dt.Rows[i]["lastin"]).Date.ToString());
                                lnkMarkAbsence.Attributes.Add("data-active", "true");
                                lnkMarkAbsence.PostBackUrl = "";
                                tcellhw.Controls.Add(lblText);
                                tcellhw.Font.Bold = true;
                                tcellhw.Controls.Add(lnkMarkAbsence);
                                tRowRecord.Attributes.Add("class", "pinkRow");
                                tcellhw.ColumnSpan = 4;
                                //Add hours
                                //if (allowAddHours)
                                //{
                                //    tcellhw.Controls.Add(imgAddHours);
                                //}
                                tRowRecord.Cells.Add(tcellhw);
                            }
                        }
                    }
                }
                else
                {
                    bool auto_in = false;
                    bool auto_out = false;
                    string span_text = "<span style='font-size: smaller;'> [auto] </span>";

                    Label ia2 = new Label();
                    ia2.Text = Convert.ToDateTime(dt.Rows[i]["lastin"]).ToString("hh:mm tt");

                    if (!string.IsNullOrEmpty(dt.Rows[i]["autoclockin_id"].ToString()) && dt.Rows[i]["isauto_clockin"].ToString() == "True")
                    {
                        auto_in = true;
                        ia2.Text = ia2.Text + span_text;
                        ia2.CssClass = "information";
                        ia2.ToolTip = "The user did not manually clock-IN. The system registered this entry automatically since the date had changed while the user was still clocked-IN.";
                    }

                    //if (dt.Rows[i]["signin"].ToString() != "")
                    //{
                    //    //ia2.CssClass = "information";
                    //    //ia2.ToolTip = "Reason: " + dt.Rows[i]["signin"].ToString();

                    //    //ia2.Text = objATZ.GetTime(dt.Rows[i]["lastin"].ToString(), AppId).ToString() + "*";
                    //    //ia2.Text = objATZ.GetTime(dt.Rows[i]["lastin"].ToString(), AppId).ToString();
                    //    ia2.Text = Convert.ToDateTime(dt.Rows[i]["lastin"]).ToString("hh:mm tt");
                    //}
                    //else
                    //{
                    //    //ia2.Text = objATZ.GetTime(dt.Rows[i]["lastin"].ToString(), AppId).ToString();
                    //    ia2.Text = Convert.ToDateTime(dt.Rows[i]["lastin"]).ToString("hh:mm tt");
                    //}

                    TableCell tblCellLnkDelHours1 = new TableCell();
                    tblCellLnkDelHours1.Controls.Add(ia2);
                    tRowRecord.Cells.Add(tblCellLnkDelHours1);

                    //tRowRecord.Cells.Add(new TableCell() { Text = objATZ.GetTime(dt.Rows[i]["lastin"].ToString(), AppId) });

                    //Sign-in IP 
                    //Label ia1 = new Label();
                    //ia1.CssClass = "tooltip";
                    //ia1.ToolTip = "Click here to view the location of this IP.";

                    //ia1.Text = "<a href='http://ip-api.com/#" + dt.Rows[i]["signinaddress"].ToString() + "' target='_blank' >" + dt.Rows[i]["signinaddress"].ToString() + "</a>";
                    //TableCell signinip = new TableCell();
                    //signinip.Controls.Add(ia1);

                    //tRowRecord.Cells.Add(signinip);

                    Image imgAction = new Image();

                    //if (dt.Rows[i]["lastout"].ToString() == null || dt.Rows[i]["lastout"].ToString() == "")
                    //{
                    //    tRowRecord.Cells.Add(new TableCell() { Text = "" });
                    //}
                    //else
                    //{
                    string intime = dt.Rows[i]["lastin"].ToString();
                    string outtime = dt.Rows[i]["lastout"].ToString();
                    string inDate = intime;
                    string outDate = outtime;

                    //Image imgEditHours = new Image();
                    Label ia = new Label();

                    //if (dt.Rows[i]["signout"].ToString() != "")
                    //{
                    //    ia.CssClass = "information";
                    //    ia.ToolTip = "Reason: " + dt.Rows[i]["signout"].ToString();

                    //    //ia.Text = objATZ.GetTime(dt.Rows[i]["lastout"].ToString(), AppId).ToString() + "*";
                    //    //ia.Text = objATZ.GetTime(dt.Rows[i]["lastout"].ToString(), AppId).ToString();                       
                    //}
                    //else
                    //{
                    //ia.Text = objATZ.GetTime(dt.Rows[i]["lastout"].ToString(), AppId).ToString();
                    //}

                    if (dt.Rows[i]["lastout"].ToString() != "")
                    {
                        ia.Text = Convert.ToDateTime(dt.Rows[i]["lastout"]).ToString("hh:mm tt");
                        if (string.IsNullOrEmpty(dt.Rows[i]["autoclockin_id"].ToString()) && dt.Rows[i]["isauto_clockin"].ToString() == "True")
                        {
                            auto_out = true;
                            ia.Text = ia.Text + span_text;
                            ia.CssClass = "information";
                            ia.ToolTip = "The user did not manually clock-OUT. The system registered this entry automatically since the date had changed while the user was still clocked-IN.";
                        }
                    }
                    else
                        ia.Text = "";


                    #region Edit Hours Record[0] -- pencil.png
                    intime = intime.Split(' ')[0] + " " + (ia2.Text).Split('<')[0];

                    if (outtime.Split(' ')[0] != "")
                        outtime = outtime.Split(' ')[0] + " " + (ia.Text).Split('<')[0];
                    else
                        outtime = "";

                    imgAction.AlternateText = " - Edit Hours";
                    imgAction.ImageUrl = "../images/Controls/gear.png";
                    imgAction.ID = "imgEditHours" + ++idS + "" + dt.Rows[i]["rowID"];
                    imgAction.CssClass = "imgAction tooltip managedUser";
                    imgAction.ToolTip = "Edit Clock-in / Clock-out Time.";
                    imgAction.Attributes.Add("Title", "Edit Time");
                    imgAction.Attributes.Add("data-autoclockin", (auto_in == true) ? "1" : "0");
                    //imgEditHours.Attributes.Add("data-frmTime", intime);
                    //imgEditHours.Attributes.Add("data-toTime", outtime);
                    imgAction.Attributes.Add("data-frmTime", Convert.ToDateTime(intime.Split(' ')[1] + ' ' + intime.Split(' ')[2]).ToString("hh:mm tt"));
                    imgAction.Attributes.Add("data-toTime", (outtime != "") ? Convert.ToDateTime(outtime.Split(' ')[1] + ' ' + outtime.Split(' ')[2]).ToString("hh:mm tt") : outtime);
                    imgAction.Attributes.Add("data-frmdate", intime.Split(' ')[0]);
                    imgAction.Attributes.Add("data-todate", (outtime != "") ? outtime.Split(' ')[0] : outtime);
                    imgAction.Attributes.Add("data-recordID", dt.Rows[i]["rowID"].ToString());
                    //imgEditHours.Style.Add("width", "12px");
                    //imgEditHours.Style.Add("margin-left", "15px");
                    //imgEditHours.Style.Add("cursor", "pointer");
                    //imgEditHours.Style.Add("float", "right");
                    //imgAction.Style.Add("width", "12px");
                    imgAction.Style.Add("margin", "3px 15px 3px 0px");
                    imgAction.Style.Add("cursor", "pointer");

                    #endregion

                    #region Delete Hours Record -- cross.png
                    //Image imgDelHours = new Image();
                    //imgDelHours.AlternateText = " - Delete Hours";
                    //imgDelHours.ImageUrl = "../images/Controls/cross.png";
                    //imgDelHours.ID = "imgDelHours" + ++idS + "" + dt.Rows[i]["rowID"];
                    //imgDelHours.CssClass = "imgDelHours tooltip managedUser";
                    //imgDelHours.ToolTip = "Delete Clock-in / Clock-out Time.";
                    //imgDelHours.Attributes.Add("Title", "Delete Time");
                    //imgDelHours.Attributes.Add("data-frmTime", Convert.ToDateTime(intime.Split(' ')[1] + ' ' + intime.Split(' ')[2]).ToString("hh:mm tt"));
                    //imgDelHours.Attributes.Add("data-toTime", (outtime != "") ? Convert.ToDateTime(outtime.Split(' ')[1] + ' ' + outtime.Split(' ')[2]).ToString("hh:mm tt") : outtime);
                    //imgDelHours.Attributes.Add("data-frmdate", intime.Split(' ')[0]);
                    //imgDelHours.Attributes.Add("data-todate", (outtime != "") ? outtime.Split(' ')[0] : outtime);
                    //imgDelHours.Attributes.Add("data-recordID", dt.Rows[i]["rowID"].ToString());
                    ////imgDelHours.Style.Add("margin-left", "15px");
                    ////imgDelHours.Style.Add("float", "right");
                    //imgDelHours.Style.Add("width", "15px");
                    //imgDelHours.Style.Add("margin", "0px 15px 0px 0px");
                    //imgDelHours.Style.Add("cursor", "pointer");
                    #endregion

                    TableCell tblCellLnkDelHours = new TableCell();
                    tblCellLnkDelHours.Controls.Add(ia);
                    //tblCellLnkDelHours.Controls.Add(imgEditHours);

                    tRowRecord.Cells.Add(tblCellLnkDelHours);
                    //tRowRecord.Cells.Add(new TableCell() { Text = objATZ.GetTime(dt.Rows[i]["lastout"].ToString(), AppId) });
                    //}

                    //Sign-out IP
                    //ia1 = new Label();

                    //ia1.CssClass = "tooltip";
                    //ia1.ToolTip = "Click here to view the location of this IP.";
                    //ia1.Text = "<a href='http://ip-api.com/#" + dt.Rows[i]["signoutaddress"].ToString() + "' target='_blank' >" + dt.Rows[i]["signoutaddress"].ToString() + "</a>";

                    //signinip = new TableCell();
                    //signinip.Controls.Add(ia1);

                    //tRowRecord.Cells.Add(signinip);

                    TimeSpan WorkedHours = new TimeSpan();

                    //if (todaysDate == "Today")//If clockout is not performed show individual records
                    //{
                    //    if (dt.Rows[i]["lastout"].ToString() == null || dt.Rows[i]["lastout"].ToString() == "")
                    //    {
                    //        WorkedHours = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
                    //        tRowRecord.Cells.Add(new TableCell() { Text = ConvertTimeSpan(WorkedHours) });
                    //    }
                    //    else
                    //    {
                    //        WorkedHours = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastout"].ToString())).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
                    //        tRowRecord.Cells.Add(new TableCell() { Text = ConvertTimeSpan(WorkedHours) });
                    //    }
                    //}
                    //else
                    //{

                    if (drMultipleClockin != null)
                    {
                        if (drMultipleClockin.Length > 1) //
                        {
                            for (int p = 0; p < drMultipleClockin.Length; p++)
                            {
                                if (drMultipleClockin[p]["lastout"].ToString() == null || drMultipleClockin[p]["lastout"].ToString() == "")
                                {
                                    WorkedHours = WorkedHours = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(drMultipleClockin[p]["lastin"].ToString())));
                                }
                                else
                                {
                                    WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(drMultipleClockin[p]["lastout"].ToString())).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(drMultipleClockin[p]["lastin"].ToString())));
                                }
                            }

                            TotalWoked_GridHours += WorkedHours;
                            tRowRecord.Cells.Add(new TableCell() { Text = ConvertTimeSpan(WorkedHours), RowSpan = drMultipleClockin.Length + r });
                        }
                        else
                        {
                            if (r > 1)
                                r = 2;
                            if (dt.Rows[i]["lastout"].ToString() == null || dt.Rows[i]["lastout"].ToString() == "")
                            {
                                //WorkedHours = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
                                DateTime date_now = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(objATZ.GetTime(DateTime.Now.ToString(), AppId)));
                                WorkedHours = (date_now).Subtract(Convert.ToDateTime(dt.Rows[i]["lastin"]));
                                TotalWoked_GridHours += WorkedHours;
                                tRowRecord.Cells.Add(new TableCell() { Text = ConvertTimeSpan(WorkedHours), RowSpan = r });
                            }
                            else
                            {
                                WorkedHours = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastout"].ToString())).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
                                TotalWoked_GridHours += WorkedHours;
                                tRowRecord.Cells.Add(new TableCell() { Text = ConvertTimeSpan(WorkedHours), RowSpan = r });
                            }
                        }
                    }
                    // }

                    //IP Tooltip
                    Image imgInfo = new Image();
                    Image imgTag = new Image();
                    string iDate = "";
                    string iIntime = "";
                    string iOuttime = "";
                    if (Convert.ToDateTime(dt.Rows[i]["lastin"].ToString()).Date == DateTime.Now.Date)
                    {
                        iDate = "Today";
                    }
                    else
                    {
                        iDate = Convert.ToDateTime(dt.Rows[i]["lastin"].ToString()).ToString("ddd, MMM d yyy");
                    }

                    iIntime = Convert.ToDateTime(dt.Rows[i]["lastin"]).ToString("hh:mm tt");

                    string dlsavinghour = "(Daylight Savings: 0 minutes)";
                    if (dt.Rows[i]["dlsaving"].ToString() != "0")
                    {
                        dlsavinghour = dt.Rows[i]["dlsavinghour"].ToString() + " minutes";

                        if (dt.Rows[i]["dlsaving"].ToString() == "1")
                            dlsavinghour = "(Daylight Savings: + " + dlsavinghour + ")";
                        else
                            dlsavinghour = "(Daylight Savings: - " + dlsavinghour + ")";
                    }

                    string strDetails = "<table id=\"tblDetail\">";
                    //strDetails += "<tr><td colspan='2' style='text-align:center'>" + App.GetDate(dt.Rows[i]["lastin"].ToString(), AppId) + "</td><tr>";
                    //strDetails += "<tr><td class='caption'>Clock in: </td><td>" + objATZ.GetTime(dt.Rows[i]["lastin"].ToString(), AppId).ToString() + "</td><tr>";
                    strDetails += "<tr><td colspan='2' style='text-align:center'>" + iDate + "</td><tr>";
                    strDetails += "<tr><td colspan='2' style='text-align:center'>" + dt.Rows[i]["timezone"].ToString() + "</td><tr>";
                    strDetails += "<tr><td colspan='2' style='text-align:center'>" + dlsavinghour + "</td><tr>";

                    if (dt.Rows[i]["time_edit"] != DBNull.Value)
                        if (Convert.ToBoolean(dt.Rows[i]["time_edit"]))
                            strDetails += "<tr><td colspan='2' style='text-align:center;color:#0aa3ff;'>*Time edited*</td><tr>";

                    strDetails += "<tr><td class='caption align'>Clock in: </td><td>" + iIntime + ((auto_in == true) ? span_text : "") + "</td><tr>";

                    if (!string.IsNullOrEmpty(dt.Rows[i]["signin"].ToString()))
                        strDetails += "<tr><td class='caption align'>Clock in reason: </td><td>" + dt.Rows[i]["signin"].ToString() + "</td><tr>";

                    strDetails += "<tr><td class='caption align'>Clock in location: </td><td>" + dt.Rows[i]["signinaddress"].ToString();
                    strDetails += (atd.checkDataTable(dtIPs, "ipaddress = '" + dt.Rows[i]["signinaddress"].ToString() + "'") > 0) ? "(" + dtIPs.Select("ipaddress = '" + dt.Rows[i]["signinaddress"].ToString() + "'")[0]["title"] + ")" : "";
                    if (dt.Rows[i]["instatus"].ToString().ToLower() == "verified")
                        strDetails += "<image src=\"../images/Controls/tick.png\" width=\"12px\" style='padding-left:3px;' /></td></tr>";
                    else
                        strDetails += "<image src=\"../images/Controls/cross.png\" width=\"12px\" style='padding-left:3px;' /></td></tr>";

                    if (!string.IsNullOrEmpty(dt.Rows[i]["lastout"].ToString()))
                    {
                        iOuttime = Convert.ToDateTime(dt.Rows[i]["lastout"]).ToString("hh:mm tt");
                        //strDetails += "<tr><td class='caption'>Clock Out: </td><td>" + objATZ.GetTime(dt.Rows[i]["lastout"].ToString(), AppId).ToString() + "</td><tr>";
                        strDetails += "<tr><td class='caption align'>Clock Out: </td><td>" + iOuttime + ((auto_out == true) ? span_text : "") + "</td><tr>";

                        if (!string.IsNullOrEmpty(dt.Rows[i]["signout"].ToString()))
                            strDetails += "<tr><td class='caption align'>Clock out reason: </td><td>" + dt.Rows[i]["signout"].ToString() + "</td><tr>";

                        strDetails += "<tr><td class='caption align'>Clock out location: </td><td>" + dt.Rows[i]["signoutaddress"].ToString();
                        strDetails += (atd.checkDataTable(dtIPs, "ipaddress = '" + dt.Rows[i]["signoutaddress"].ToString() + "'") > 0) ? "(" + dtIPs.Select("ipaddress = '" + dt.Rows[i]["signoutaddress"].ToString() + "'")[0]["title"] + ")" : "";
                        if (dt.Rows[i]["outstatus"].ToString().ToLower() == "verified")
                            strDetails += "<image src=\"../images/Controls/tick.png\" width=\"12px\" style='padding-left:3px;' /></td></tr>";
                        else
                            strDetails += "<image src=\"../images/Controls/cross.png\" width=\"12px\" style='padding-left:3px;' /></td></tr>";

                    }

                    TimeSpan Worked = new TimeSpan();
                    TableCell details = new TableCell();
                    string strBreak = "";

                    DataRow[] drholdays = dtHolidays.Select("(date='" + Convert.ToDateTime(dt.Rows[i]["lastin"]).ToString("M/d/yyyy") + " 12:00:00 AM" + "') or ( Convert(date, 'System.String') LIKE  '%" + Convert.ToDateTime(dt.Rows[i]["lastin"]).ToString("M/dd") + "%' AND  everyyear = 1) and Type='1'");
                    DataRow[] drWeekned = dtWeekends.Select("Title='" + Convert.ToDateTime(dt.Rows[i]["lastin"].ToString()).DayOfWeek + "' and Type='0'");

                    if (WorkingHours.Rows.Count > 0 && (drholdays.Length == 0 && drWeekned.Length == 0))
                    {
                        DateTime clockin = Convert.ToDateTime(dt.Rows[i]["lastin"].ToString());
                        DateTime clockout = (dt.Rows[i]["lastout"].ToString() != "") ? Convert.ToDateTime(dt.Rows[i]["lastout"].ToString()) : DateTime.Now;
                        DataRow tempRow = WorkingHours.Select("DayTitle = '" + clockin.DayOfWeek + "'").FirstOrDefault();
                        DateTime bStart = Convert.ToDateTime(tempRow["BreakStart"]);
                        DateTime bEnd = Convert.ToDateTime(tempRow["BreakEnd"]);

                        Worked = TimeZoneInfo.ConvertTimeToUtc(clockout).Subtract(TimeZoneInfo.ConvertTimeToUtc(clockin));
                        TimeSpan breaktime = new TimeSpan();

                        //9AM - 6PM
                        if (bStart.TimeOfDay >= clockin.TimeOfDay && bEnd.TimeOfDay <= clockout.TimeOfDay)
                        {
                            breaktime = bEnd.TimeOfDay.Subtract(bStart.TimeOfDay);
                        }
                        //9AM - 1.30PM
                        else if (bStart.TimeOfDay >= clockin.TimeOfDay && (bStart.TimeOfDay <= clockout.TimeOfDay && bEnd.TimeOfDay >= clockout.TimeOfDay))
                        {
                            breaktime = clockout.TimeOfDay.Subtract(bStart.TimeOfDay);
                        }
                        //1PM - 6PM
                        else if ((bStart.TimeOfDay <= clockin.TimeOfDay && bEnd.TimeOfDay >= clockin.TimeOfDay) && bEnd.TimeOfDay <= clockout.TimeOfDay)
                        {
                            breaktime = bEnd.TimeOfDay.Subtract(clockin.TimeOfDay);
                        }
                        //1PM - 2PM
                        else if (bStart.TimeOfDay <= clockin.TimeOfDay && bEnd.TimeOfDay >= clockout.TimeOfDay)
                        {
                            breaktime = clockout.TimeOfDay.Subtract(clockin.TimeOfDay);
                        }

                        Worked = Worked.Subtract(breaktime);

                        if (breaktime.CompareTo(TimeSpan.Zero) != 0)
                            strBreak = "<tr><td class='caption align'>Break: </td><td>" + ConvertTimeSpan(breaktime) + "</td><tr>";
                        else
                            strBreak = "<tr><td class='caption align'>Break: </td><td>Break not included</td><tr>";


                        if (dt.Rows[i]["lastout"].ToString() == null || dt.Rows[i]["lastout"].ToString() == "")
                        {
                            //DateTime date_now = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(objATZ.GetTime(DateTime.Now.ToString(), AppId)));
                            //Worked = (date_now).Subtract(Convert.ToDateTime(dt.Rows[i]["lastin"]));
                            imgAction.Style.Add("margin", "3px 15px 3px 0px");
                        }
                        else
                        {
                            //Worked = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastout"])).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"])));
                            //details.Controls.Add(imgDelHours);
                        }
                    }
                    else
                    {
                        if (dt.Rows[i]["lastout"].ToString() == null || dt.Rows[i]["lastout"].ToString() == "")
                        {
                            //Worked = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"])));
                            DateTime date_now = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(objATZ.GetTime(DateTime.Now.ToString(), AppId)));
                            Worked = (date_now).Subtract(Convert.ToDateTime(dt.Rows[i]["lastin"]));
                            imgAction.Style.Add("margin", "3px 15px 3px 0px");
                        }
                        else
                        {
                            Worked = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastout"])).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"])));
                            //details.Controls.Add(imgDelHours);
                        }
                    }

                    string wHrs = (ConvertTimeSpan(Worked) != "") ? ConvertTimeSpan(Worked) : "0 Hrs";
                    strDetails += "<tr><td class='caption align'>Total Worked: </td><td>" + wHrs + "</td><tr>";
                    if (strBreak != "")
                        strDetails += strBreak;

                    //strDetails += "<tr><td class='caption'>Time Spent: </td><td>" + ConvertTimeSpan(Worked) + "</td><tr>"; UsmanMalikAwan
                    //strDetails += "<tr><td class='caption'>Total Worked: </td><td>" + ConvertTimeSpan(WorkedHours) + "</td><tr>";

                    string Tags = "";
                    DataTable dttags = GetAssignTags(Convert.ToInt32(dt.Rows[i]["rowID"].ToString()));
                    if(dttags.Rows.Count>0)
                    {
                        string tagsdata = "<table style=\"min-width: 124px;\">";

                        foreach (DataRow row in dttags.Rows)
                        {
                            tagsdata = tagsdata + "<tr><td>"+row["Tags"].ToString() + "</td></tr>";
                        }
                        tagsdata = tagsdata + "</table>";
                       
                        Tags = "<b>Tag Assigns:</b> <br /><br />";
                        Tags += tagsdata;
                       
                        imgTag.ToolTip = Tags;
                        imgTag.CssClass = "tooltip statusDetails";
                        imgTag.ImageUrl = "../images/controls/TaG1.png";
                    }


                    strDetails += "</table>";

                    imgInfo.ToolTip = strDetails;
                    imgInfo.CssClass = "tooltip statusDetails";
                    imgInfo.ImageUrl = "../images/controls/icon_info.png";
                    imgInfo.Style.Add("margin", "3px 15px 3px 0px");



                    if (Convert.ToBoolean(hdnIsEditAllowed.Value))
                    {
                        details.Controls.Add(imgAction);
                    }
                    details.Controls.Add(imgInfo);
                    details.Controls.Add(imgTag);
                    tRowRecord.Cells.Add(details);
                    //End

                    //Holiday/Weekend/Absent Work
                    DataRow[] isWeekned = dtWeekends.Select("Title='" + Convert.ToDateTime(dt.Rows[i]["lastin"].ToString()).DayOfWeek + "' and Type='0'");
                    DataRow[] isAbsences = dtAbsence.Select("absentDate='" + Convert.ToDateTime(dt.Rows[i]["lastin"]).Date + "'");
                    DataRow[] isholdays = dtHolidays.Select("date='" + Convert.ToDateTime(dt.Rows[i]["lastin"]).ToString("M/d/yyyy") + " 12:00:00 AM" + "' and Type='1'");

                    if (isWeekned.Length > 0)
                    {
                        tRowRecord.Cells.Add((new TableCell() { Text = "Weekend" }));
                        tRowRecord.Cells[tRowRecord.Cells.Count - 1].Font.Bold = true;
                    }
                    else if (isAbsences.Length > 0)
                    {
                        tRowRecord.Cells.Add((new TableCell() { Text = "Absent", CssClass = "information" }));
                        tRowRecord.Cells[tRowRecord.Cells.Count - 1].Font.Bold = true;
                        tRowRecord.Cells[tRowRecord.Cells.Count - 1].ToolTip = isAbsences[0]["Comment"].ToString();
                    }
                    else if (isholdays.Length > 0)
                    {
                        tRowRecord.Cells.Add((new TableCell() { Text = "Holiday", CssClass = "information" }));
                        tRowRecord.Cells[tRowRecord.Cells.Count - 1].Font.Bold = true;
                        tRowRecord.Cells[tRowRecord.Cells.Count - 1].ToolTip = isholdays[0]["Title"].ToString();
                    }
                    else
                        tRowRecord.Cells.Add((new TableCell() { Text = "" }));

                    //End
                }

                tblHistory.Rows.Add(tRowRecord);

                if ((dt.Rows[i]["signin"] != null && dt.Rows[i]["signin"].ToString() != "") || (dt.Rows[i]["signout"] != null && dt.Rows[i]["signout"].ToString() != ""))
                {
                    string remarks = "<style>.CiCoTime {padding: 0px 0px;width: 96%;margin: 0px 0 0px 0;}</style>";


                    //if ((dt.Rows[i]["signin"] != null && dt.Rows[i]["signin"].ToString() != ""))
                    //{
                    //    remarks += "<div class='CiCoTime'><strong>Clock-In Remark:</strong> " + dt.Rows[i]["signin"] + "</div>";
                    //}

                    //if ((dt.Rows[i]["signout"] != null && dt.Rows[i]["signout"].ToString() != ""))
                    //{
                    //    remarks += "<div class='CiCoTime'><strong>Clock-Out Remark:</strong> " + dt.Rows[i]["signout"] + "</div>";
                    //}


                    TableRow tRowRecord1 = new TableRow();
                    //tRowRecord1.Cells.Add(new TableCell() { Text = remarks, ColumnSpan = 4 });
                    tblHistory.Rows.Add(tRowRecord1);
                }
            }

            TableRow tLastRow = new TableRow();
            string LastrowCSS = "<style>.lastrow {font-weight: bold; text-align: right;background: #e5e5e5 !important;border-bottom: 1px solid #999 !important;}</style>";
            tLastRow.Cells.Add(new TableCell() { Text = LastrowCSS + "Total:", ColumnSpan = 3, CssClass = "lastrow" });
            tLastRow.Cells.Add(new TableCell() { Text = LastrowCSS + "<span style='float:left !important;'>" + atd.ConvertTimeSpanInHours(TotalWoked_GridHours) + "</span>", ColumnSpan = 2, CssClass = "lastrow" });
            //tLastRow.Cells.Add(new TableCell() { Text = LastrowCSS + "", CssClass = "lastrow" });
            tLastRow.Cells.Add(new TableCell() { Text = "" });
            tblHistory.Rows.Add(tLastRow);

            //Managed User
            if (Request.QueryString["uid"].ToString() == hdnParentid.Value)
            {
                if (!atd.UserManaged(hdnParentid.Value.ToInt32(), _InstanceID, "get"))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "ManagedUser", " $('.managedUser').addClass('hide');", true);
                }
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "", "console.log('DONE');jq191('.statusDetails, .information').tooltipster({interactive: true});", true);
        }

        class DateRange
        {
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
        }
        public void GetYearStats(int userid, int year, string instanceID)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                List<RecordModel> records = new List<RecordModel>();
                DateTime startDate = SP.GetWorkerFirstSignInThisYear(userid.ToString(), year);
                DateTime endDate = SP.GetWorkerLastSignInThisYear(userid.ToString(), year);
                #region Table Headers weekends
                List<RecordModel> recordList = new List<RecordModel>();
                List<DateRange> DateRanges = new List<DateRange>();
                #endregion
                List<Absence> absences = Absence.GetAbsences(Convert.ToInt32(userid)).Where(u => u.Active == true).ToList();
                List<WorkingHour> userWorkingHours = WorkingHour.GetWorkingHours(userid);
                if (userWorkingHours.Count <= 0)
                {
                    userWorkingHours = WorkingHour.GetWorkingHours(0);
                    userWorkingHours = userWorkingHours.Where(u => u.InstanceID == instanceID).ToList();
                }
                TimeSpan TotalOvertime = new TimeSpan(0);
                TimeSpan Totalundertime = new TimeSpan(0);
                TimeSpan tsTotalOvertime = new TimeSpan(0);
                TimeSpan tsTotalUndertime = new TimeSpan(0);
                TimeSpan tsworkingHoursShouldBe = new TimeSpan(0);
                TimeSpan tsworkingHours = new TimeSpan(0);
                TimeSpan tsbreakhours = new TimeSpan(0);
                TimeSpan tsbreakhoursShouldBe = new TimeSpan(0);
                TimeSpan tsOvertimeAuto = new TimeSpan(0);
                //TimeSpan tsWeekendWorkedHours = new TimeSpan(0);
                TimeSpan tsAbsenceHours = new TimeSpan(0);
                int totalAbs = 0;
                int totAbsDays = 0;
                int iDaysWorked = 0;

                if (startDate.Year <= year && endDate.Year >= year)
                {
                    for (int i = startDate.Month; i <= endDate.Month; i++)
                    {
                        DateTime qstartDate = new DateTime(year, i, 1);
                        DateTime qendDate = qstartDate.AddMonths(1).AddDays(-1);
                        using (SqlCommand cmd = new SqlCommand("getworkerrecordbydate", con))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@sDate", qstartDate.Date);
                            cmd.Parameters.AddWithValue("@eDate", qendDate.Date);
                            cmd.Parameters.AddWithValue("@Id", userid);
                            cmd.Parameters.AddWithValue("@InstanceId", instanceID);
                            DataSet ds = new DataSet();
                            SqlDataAdapter adp = new SqlDataAdapter(cmd);
                            adp.Fill(ds);
                            records = UtilityMethods.getRecords(ds);
                            records = records.OrderBy(u => u.Date).ToList();
                            DateRanges.Add(new DateRange() { EndDate = qendDate.Date, StartDate = qstartDate.Date });

                            foreach (var record in records)
                            {
                                if (record.DayTitle != null && record.Records.Count > 0)
                                {
                                    tsTotalOvertime += record.TotalOverTime;
                                    ++iDaysWorked;
                                }
                                else if (record.Records.Count > 0 && record.DayTitle == null)
                                {
                                    tsworkingHours += record.TotalWorkedHoursTS;
                                    tsworkingHoursShouldBe += record.WorkingHours;
                                    tsTotalOvertime += record.TotalOverTime;
                                    tsTotalUndertime += record.TotalUnderTimeWithoutLate;
                                }
                            }
                        }
                    }
                    foreach (Absence absence in absences.Where(u => u.CrtDate.Year == year).ToList()) { totAbsDays += SP.GetTodayUserWorkHours(this.InstanceID, hdnUserID.Value.ToInt32(), absence.CrtDate.Date); }
                    totalAbs += absences.Where(u => u.CrtDate.Date.Year == year).ToList().Count;
                }

                tsAbsenceHours = new TimeSpan(totAbsDays, 0, 0);
                //Response.Write(tsOvertimeAuto.ToString() + "<br />");
                if (tsOvertimeAuto >= new TimeSpan(0))
                {
                    TimeSpan tsOTAllTotal = tsTotalOvertime + tsOvertimeAuto;
                    //lblTotalOvertimeR.ForeColor = System.Drawing.Color.Green;
                    // Overtime including weekend and all
                    //usmanlblTotalOvertimeR.Text = Math.Ceiling(Convert.ToDecimal(iDaysWorked)) + " weekend work days [" + UtilityMethods.getFormatedTimeByMinutes(tsOTAllTotal.TotalMinutes.ToInt32()) + "]";
                    // lblTotalOvertimeR.Text = UtilityMethods.getFormatedTimeByMinutes(tsOTAllTotal.TotalMinutes.ToInt32());
                    TotalOvertime = tsOTAllTotal;
                }
                else
                {
                    TimeSpan tsOTAllTotal = tsTotalOvertime;
                    //lblTotalOvertimeR.ForeColor = System.Drawing.Color.Green;
                    // Overtime including weekend and all
                    //usmanlblTotalOvertimeR.Text = Math.Ceiling(Convert.ToDecimal(iDaysWorked)) + " weekend work days [" + UtilityMethods.getFormatedTimeByMinutes(tsOTAllTotal.TotalMinutes.ToInt32()) + "]";
                    //lblTotalOvertimeR.Text = UtilityMethods.getFormatedTimeByMinutes(tsOTAllTotal.TotalMinutes.ToInt32());
                    TotalOvertime = tsOTAllTotal;
                }

                if (tsOvertimeAuto < new TimeSpan(0))
                {
                    //Undertime exluding Late undertime
                    TimeSpan tsUTAllTotal = tsTotalUndertime + (-tsOvertimeAuto);
                    //lblTotalUndertimeR.ForeColor = System.Drawing.Color.Red;
                    // lblTotalUndertimeR.Text = UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32((tsUTAllTotal.TotalMinutes)));
                    //Response.Write((tsTotalUndertime).ToString() + "<br /> +");
                    //Response.Write(tsOvertimeAuto.ToString() + "<br /> +");
                    //lblOvertimeLabel.Text = "Missing time(Hours not completed): ";
                    Totalundertime = tsUTAllTotal;
                }
                else
                {
                    TimeSpan tsUTAllTotal = tsTotalUndertime;
                    //lblTotalUndertimeR.ForeColor = System.Drawing.Color.Red;
                    //lblTotalUndertimeR.Text = UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32((tsUTAllTotal.TotalMinutes)));
                    //Response.Write(tsUTAllTotal.ToString() + "<br />");
                    Totalundertime = tsUTAllTotal;
                }

                //usmanlblTotalAbsencesR.Text = totalAbs.ToString() + " day(s) (" + totAbsDays.ToString() + " hours)";
                lblTotalAbsencesR.Text = totalAbs.ToString() + " day(s)";
                lnkViewAbs.HRef = "Absence.aspx?id=" + Request.QueryString["id"].ToString() + "&uid=" + Request.QueryString["uid"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
                lnkViewStats.HRef = "Statistics.aspx?id=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString() + "&uid=" + Request.QueryString["uid"].ToString();
                //if (Request.QueryString["instanceid"].ToString() == "f425f912-8d79-4415-ba72-89a5309acb10")
                lnkViewStats.Visible = false;

                if (Request.QueryString["instanceid"].ToString() == "6752062f-f30f-4abe-b048-6cc2daee6d3d")
                    lnkViewStats.Visible = true;
                //lblEstWHrs.Text = UtilityMethods.getFormatedTimeByMinutes(tsworkingHoursShouldBe.TotalMinutes.ToInt32());
                //lblWHours.Text = UtilityMethods.getFormatedTimeByMinutes(tsworkingHours.TotalMinutes.ToInt32()) + " (Extra/Weekend Work Hours Excluded)";
                //lblTotBreakHrs.Text = UtilityMethods.getFormatedTimeByMinutes(tsbreakhours.TotalMinutes.ToInt32()) + " (should be " + UtilityMethods.getFormatedTimeByMinutes(tsbreakhoursShouldBe.TotalMinutes.ToInt32()) + ")";


                //lblTotDays.Text = iDaysWorked.ToString();

                TimeSpan Totalbalancetime = new TimeSpan(0);

                Totalbalancetime = TotalOvertime - Totalundertime;

                if (Totalbalancetime.TotalMinutes < 0)
                {
                    //lblbalancetime.ForeColor = System.Drawing.Color.Red;
                    //  lblbalancetime.ToolTip = "Absence not included";
                    // lblbalancetime.Text = UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32((Totalbalancetime.TotalMinutes * -1))) + " Undertime";
                }
                else if (Totalbalancetime.TotalMinutes >= 0)
                {
                    //  lblbalancetime.ForeColor = System.Drawing.Color.Green;
                    //  lblbalancetime.Text = UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32((Totalbalancetime.TotalMinutes))) + " Overtime";
                    //  lblbalancetime.ToolTip = "Absence not included";
                }


            }
        }

        public void lnkMarkAbsence_Click(object sender, CommandEventArgs e)
        {
            Button btnSender = (Button)sender;

            //SP.AddAbsence(0, Convert.ToInt32(UserId), Convert.ToDateTime(btnSender.CommandArgument), true);
            //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString);
            //FillandCreateHistory(UserId, con);
        }

        private void PopulateAdvanceAbsences()
        {
            List<Absence> advanceAbs = Absence.GetAbsences(Convert.ToInt32(UserId)).Where(u => u.Active == true && u.CrtDate.Date > DateTime.Now.Date).ToList();

            if (advanceAbs.Count > 0)
            {
                rptAbscenses.DataSource = advanceAbs;
                rptAbscenses.DataBind();
                lblLeaves.Text = "Leaves:";
            }
        }

        // Set user Sign In or Out Time
        private void SetUserStatus()
        {
            //aMonthlyReport.Attributes.Add("href", "MonthlyReportN.aspx?id=" + Request.QueryString["uid"].ToString() + "&instanceid=" + _InstanceID);
            //aYearlyReport.Attributes.Add("href", "YearlyReportN.aspx?id=" + Request.QueryString["uid"].ToString() + "&instanceid=" + _InstanceID);

            //if (SP.GetIPAddresses(_InstanceID).Rows.Count > 0)
            //{
            //    trVerifStatus.Visible = true;
            //}
            //else
            //{
            //    trVerifStatus.Visible = false;
            //}

            //PopulateAdvanceAbsences();
            Boolean isWorkerActive = SP.IsWorkerActive(Convert.ToInt32(UserId));
            hdnUserID.Value = UserId;

            //if (isAdmin)
            if (atd.getParent(UserId) == Request.QueryString["id"].ToString())
            {
                //lnkWorkingHours.Visible = true;
                //spWH.Visible = true;
                trStatus.Visible = false;
                lblactive.Visible = lblAutoPresent.Visible = true;
                lblActiveU.Visible = lblAutoPresentU.Visible = false;
                if (!isWorkerActive)
                {
                    lblactive.Text = "Inactive";
                    lblactive.Attributes.Add("data-active", "False");
                    lblactive.Attributes.Add("data-userid", UserId);
                    div_emp.Visible = false;
                    lblUserName.Text = SP.GetWorkerName(UserId);
                    lblUserName.Visible = true;
                }
                else
                {
                    lblactive.Text = "Active";
                    lblactive.Attributes.Add("data-active", "True");
                    lblactive.Attributes.Add("data-userid", UserId);
                }
                divMain.Visible = true;
                h1Inactive.Visible = false;
            }
            else
            {
                trStatus.Visible = false;
                //  lnkWorkingHours.Visible = false;
                //spWH.Visible = false;
                lblactive.Visible = lblAutoPresent.Visible = false;
                lblActiveU.Visible = lblAutoPresentU.Visible = true;
                if (!isWorkerActive)
                {
                    lblActiveU.Text = lblactive.Text = "Inactive";
                    lblactive.Attributes.Add("data-active", "False");
                    lblactive.Attributes.Add("data-userid", UserId);
                    divMain.Visible = false;
                    h1Inactive.Visible = true;
                    return;
                }
                else
                {
                    lblActiveU.Text = lblactive.Text = "Active";
                    lblactive.Attributes.Add("data-active", "True");
                    lblactive.Attributes.Add("data-userid", UserId);
                    divMain.Visible = true;
                    h1Inactive.Visible = false;
                }
            }


            div_emp.Visible = false;
            lblUserName.Text = SP.GetWorkerName(UserId);
            lblUserName.Visible = true;

            // Set Worker Info and Time status
            AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
            List<UserAttendanceStatus> userAttStatuses = SP.GetUserAttendanceStatus(UserId);
            if (userAttStatuses != null)
            {
                //userAttStatuses.OrderByDescending(u => u.LastDateTime);
                userTime = userAttStatuses[0].LastDateTime;
                lblSigninStatus.Text = userAttStatuses[0].Status;

                //if (userAttStatuses[0].InOutStatus.Equals("Verified"))
                //{
                //    lblVerStatus.Text = "Yes";
                //    //lblVerStatus.ForeColor = System.Drawing.Color.Green;
                //}
                //else if (userAttStatuses[0].InOutStatus.Equals("Notverified"))
                //{
                //    lblVerStatus.Text = "No";
                //    //lblVerStatus.ForeColor = System.Drawing.Color.Red;
                //}


                if (userAttStatuses[0].Status.Contains("Clocked in"))
                {
                    rowID = userAttStatuses[0].rowID;
                }

                if (hdnErowID.Value != "0" && !string.IsNullOrEmpty(hdnErowID.Value))
                    if (hdnErowID.Value.ToInt32() != rowID)
                    {
                        rowID = hdnErowID.Value.ToInt32();
                        hdnEcheck.Value = "1";
                    }
                    else
                        hdnEcheck.Value = "0";


                //lblSigninTime.Text = objATZ.GetTime(userAttStatuses[0].LastDateTime.ToShortTimeString(), AppId);
                lblSigninTime.Text = userAttStatuses[0].LastDateTime.ToShortTimeString();

                //lblSigninTime.Text = userAttStatuses[0].LastDateTime.ToShortTimeString();
                LastSignInTime = userAttStatuses[0].LastDateTime;

                //string datetime = App.GetDate(userAttStatuses[0].LastDateTime.ToString(), AppId);
                string datetime = userAttStatuses[0].LastDateTime.ToString();
                //String datetime = userAttStatuses[0].LastDateTime.ToString();
                DateTime tempDateTime1;
                if (datetime.Contains("Today"))
                {
                    tempDateTime1 = DateTime.Now;
                }
                else
                {
                    tempDateTime1 = Convert.ToDateTime(datetime);
                }
                //new DateTime(tempDateTime1.Year, tempDateTime1.Month, tempDateTime1.Day, Convert.ToInt32(lblSigninTime.Text.Split(' ')[0].Split(':')[0]), Convert.ToInt32(lblSigninTime.Text.Split(' ')[0].Split(':')[1]), 0);
                DateTime tempDateTime;
                tempDateTime = Convert.ToDateTime(tempDateTime1.ToShortDateString() + " " + Convert.ToDateTime(lblSigninTime.Text).ToShortTimeString());
                lblSigninDateTime.Text = tempDateTime.ToString();
                lblSignDate.Text = "on " + Convert.ToDateTime(tempDateTime1).ToString("dddd, MMMM dd, yyyy");
                //lblSignDate.Text = "on " + Convert.ToDateTime(objATZ.GetDate(userAttStatuses[0].LastDateTime.ToShortDateString(), AppId)).ToString("dddd, MMMM dd, yyyy");


                if (lblSigninStatus.Text.Contains("Last clock-out:"))
                {
                    //divWorkedTime.Visible = false;
                    // trAddOverTime.Visible = false;
                    lblSignDate.Text = lblSigninTime.Text + " " + lblSignDate.Text;
                    lblSigninTime.Visible = false;
                }
                else
                {
                    divWorkedTime.Visible = true;
                    //trWordDuration.Visible = true;
                    lblSigninTime.Visible = true;
                    MyAttSys.App_Setting setting = MyAttSys.App_Setting.SingleOrDefault(u => u.InstanceID == InstanceID);
                    if (setting != null)
                    {
                        //trAddOverTime.Visible = !Convert.ToBoolean(setting.AdminAddsOvertime); 
                    }
                    else
                    {
                        // trAddOverTime.Visible = true; 
                    }
                }
            }
            else
            {
                lblSigninStatus.Text = "Last clock-out: NEVER";
                lblSigninTime.Text = "";
                lblSignDate.Text = "";
                divWorkedTime.Visible = true;
            }

            // Get email address of user
            AvaimaUserProfile obj1 = new AvaimaUserProfile();
            DataTable dtcontractors = obj1.getuserbyInstanceId(UserId, _InstanceID);

            if (dtcontractors.Rows.Count > 0 && !String.IsNullOrEmpty(dtcontractors.Rows[0]["Email"].ToString()))
            {
                lblInEmail.Text = dtcontractors.Rows[0]["Email"].ToString();
                lblOutEmail.Text = dtcontractors.Rows[0]["Email"].ToString();
            }

        }

        //Get IP Verification
        private void GetIPVerification()
        {
            string parentid = atd.getParent(UserId);
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select IPVerification from schema_6e815b00_6e13_4839_a57d_800a92809f21.ReportSettings Where userid = @userid", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@userid", parentid);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IPVerification"]))
                            hdnIsVerified.Value = atd.VerifyInOutStatus(Request.UserHostAddress, parentid).ToString();
                    }
                }
            }
        }

        protected void btnAddExceptionalDays_Click(object sender, EventArgs e)
        {
            if (hdnGroupID.Value == "0")
            {
                SaveExceptionalDays();
            }
            else
            {
                ExceptionalDay.Delete(hdnGroupID.Value);
                SaveExceptionalDays();
            }
            BindExceptionalDays();
            ClearFields();
        }
        private void SaveExceptionalDays()
        {
            String guid = Guid.NewGuid().ToString();
            List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(Convert.ToInt32(hdnUserID.Value), _InstanceID);
            for (DateTime date = Convert.ToDateTime(txtExFrom.Text); date <= Convert.ToDateTime(txtExTo.Text); date = date.AddDays(1))
            {
                if (exDays.Where(u => u.Date.Date == date).ToList().Count > 0) { continue; }
                ExceptionalDay exDay = new ExceptionalDay()
                {
                    Active = true,
                    GroupID = guid,
                    Clockin = Convert.ToDateTime(txtExClockIn.Text),
                    CrtDate = DateTime.Now,
                    Clockout = Convert.ToDateTime(txtExClockOut.Text),
                    InstanceID = _InstanceID,
                    Date = date,
                    DayTitle = date.DayOfWeek.ToString(),
                    GroupName = txtExTitle.Text,
                    ModDate = DateTime.Now,
                    UserID = Convert.ToInt32(hdnUserID.Value),
                    AvoidBreaks = chkEliminateBreaks.Checked
                };
                exDay.Save();
            }
        }
        private void BindExceptionalDays()
        {
            DataTable dtUserExDays = ExceptionalDay.GetExceptionalDaysGroup(Convert.ToInt32(hdnUserID.Value), _InstanceID).Tables[0];
            DataTable dtStandardExDays = ExceptionalDay.GetExceptionalDaysGroup(0, _InstanceID).Tables[0];
            dtUserExDays.Merge(dtStandardExDays);
            if (dtUserExDays.Rows.Count >= 0)
            {
                rptExceptionalDays.DataSource = dtUserExDays;
                rptExceptionalDays.DataBind();
                //ExpectionalDays.Style.Remove("display");
                //lblExDaysMessage.Text = "User Defined";
            }
            else
            {
                //ExpectionalDays.Style.Add("display", "none");
            }

        }
        public String GetDate(String DateString)
        {
            AvaimaTimeZoneAPI atzAPI = new AvaimaTimeZoneAPI();
            //DateString = DateString;
            if (DateString == "Today")
            {
                DateString = DateTime.Now.ToShortDateString();
                DataTable userSettings = JsonConvert.DeserializeObject<DataTable>(atd.GetSettingInfo(Email, Password));
                string Date = atd.Timezone_datetime(UserId, Convert.ToDateTime(DateTime.UtcNow).ToString(), "date", userSettings.Rows[0]["timezone"] + "#" + userSettings.Rows[0]["dlsaving"] + "#" + userSettings.Rows[0]["dlsavinghour"]);
                string Time = atd.Timezone_datetime(UserId, Convert.ToDateTime(DateTime.UtcNow).ToString(), "time");
                DateString = Date + " " + Time;
                return DateString;
            }
            return DateString.ToString();
        }
        protected void rptExceptionalDays_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                ExceptionalDay.Delete(e.CommandArgument.ToString());
                BindExceptionalDays();
                ClearFields();
            }
            else if (e.CommandName == "Edit")
            {
                Label lblGroupName = (Label)e.Item.FindControl("lblGroupName");
                Label lblStartDate = (Label)e.Item.FindControl("lblStartDate");
                Label lblEndDate = (Label)e.Item.FindControl("lblEndDate");
                Label lblClockIn = (Label)e.Item.FindControl("lblClockIn");
                Label lblClockOut = (Label)e.Item.FindControl("lblClockOut");
                Label lblCrtDate = (Label)e.Item.FindControl("lblCrtDate");
                Label lblModDate = (Label)e.Item.FindControl("lblModDate");
                Label chkAvoidBreaks = (Label)e.Item.FindControl("chkAvoidBreaks");
                hdnGroupID.Value = e.CommandArgument.ToString();
                txtExTitle.Text = lblGroupName.Text;
                txtExClockIn.Text = lblClockIn.Text;
                txtExClockOut.Text = lblClockOut.Text;
                txtExFrom.Text = lblStartDate.Text;
                txtExTo.Text = lblEndDate.Text;
                if (chkAvoidBreaks.Text == "No")
                {
                    chkEliminateBreaks.Checked = false;
                }
                else
                {
                    chkEliminateBreaks.Checked = true;
                }
                btnAddExceptionalDays.Text = "Update";
            }
        }

        private void ClearFields()
        {
            txtExClockIn.Text = "";
            txtExClockOut.Text = "";
            txtExFrom.Text = "";
            txtExTitle.Text = "";
            txtExTo.Text = "";
            chkEliminateBreaks.Checked = false;
            hdnGroupID.Value = "0";
            btnAddExceptionalDays.Text = "Add";
            ExpectionalDays.Visible = true;
        }

        public Boolean GetExVisiblity(String UserID)
        {
            if (Convert.ToInt32(UserID) == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public String GetExInfo(String UserID)
        {
            if (Convert.ToInt32(UserID) == 0)
            {
                return "Standard";
            }
            else
            {
                return "";
            }
        }

        protected void btnSaveWH_Click(object sender, EventArgs e)
        {
            //AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
            //WorkingHour wh = new WorkingHour();
            //if (ValidateDayText(txtMonCI, txtMonCO))
            //{
            //    wh = new WorkingHour();
            //    wh.WorkHourID = Convert.ToInt32(hdnMonID.Value);
            //    wh.Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtMonCI.Text, _InstanceID).ToString());
            //    wh.Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtMonCO.Text, _InstanceID).ToString());
            //    wh.Active = true;
            //    wh.AutoPresent = chkAutoPresent.Checked;
            //    wh.crtDate = DateTime.Now;
            //    wh.DayTitle = "Monday";
            //    wh.UserID = Convert.ToInt32(hdnUserID.Value);
            //    wh.InstanceID = _InstanceID;
            //    wh.Save();
            //}
            //if (ValidateDayText(txtTuesCI, txtTuesCO))
            //{
            //    wh = new WorkingHour()
            //    {
            //        WorkHourID = Convert.ToInt32(hdnTueID.Value),
            //        Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtTuesCI.Text, _InstanceID).ToString()),
            //        Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtTuesCO.Text, _InstanceID).ToString()),
            //        Active = true,
            //        AutoPresent = chkAutoPresent.Checked,
            //        crtDate = DateTime.Now,
            //        DayTitle = "Tuesday",
            //        InstanceID = _InstanceID,
            //        UserID = Convert.ToInt32(hdnUserID.Value)
            //    };
            //    wh.Save();
            //}

            //if (ValidateDayText(txtWedCI, txtWedCO))
            //{
            //    wh = new WorkingHour()
            //    {
            //        WorkHourID = Convert.ToInt32(hdnWedID.Value),
            //        Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtWedCI.Text, _InstanceID).ToString()),
            //        Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtWedCO.Text, _InstanceID).ToString()),
            //        Active = true,
            //        AutoPresent = chkAutoPresent.Checked,
            //        crtDate = DateTime.Now,
            //        DayTitle = "Wednesday",
            //        InstanceID = _InstanceID,
            //        UserID = Convert.ToInt32(hdnUserID.Value)

            //    };
            //    wh.Save();
            //}

            //if (ValidateDayText(txtThuCI, txtThuCO))
            //{
            //    wh = new WorkingHour()
            //    {
            //        WorkHourID = Convert.ToInt32(hdnThuID.Value),
            //        Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtThuCI.Text, _InstanceID).ToString()),
            //        Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtThuCO.Text, _InstanceID).ToString()),
            //        Active = true,
            //        AutoPresent = chkAutoPresent.Checked,
            //        crtDate = DateTime.Now,
            //        DayTitle = "Thursday",
            //        InstanceID = _InstanceID,
            //        UserID = Convert.ToInt32(hdnUserID.Value)
            //    };
            //    wh.Save();
            //}

            //if (ValidateDayText(txtFriCI, txtFriCO))
            //{
            //    wh = new WorkingHour()
            //    {
            //        WorkHourID = Convert.ToInt32(hdnFriID.Value),
            //        Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtFriCI.Text, _InstanceID).ToString()),
            //        Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtFriCO.Text, _InstanceID).ToString()),
            //        Active = true,
            //        AutoPresent = chkAutoPresent.Checked,
            //        crtDate = DateTime.Now,
            //        DayTitle = "Friday",
            //        InstanceID = _InstanceID,
            //        UserID = Convert.ToInt32(hdnUserID.Value)
            //    };
            //    wh.Save();
            //}

            //if (ValidateDayText(txtSatCI, txtSatCO))
            //{
            //    wh = new WorkingHour()
            //    {
            //        WorkHourID = Convert.ToInt32(hdnSatID.Value),
            //        Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtSatCI.Text, _InstanceID).ToString()),
            //        Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtSatCO.Text, _InstanceID).ToString()),
            //        Active = true,
            //        AutoPresent = chkAutoPresent.Checked,
            //        crtDate = DateTime.Now,
            //        DayTitle = "Saturday",
            //        InstanceID = _InstanceID,
            //        UserID = Convert.ToInt32(hdnUserID.Value)
            //    };
            //    wh.Save();

            //}

            //if (ValidateDayText(txtSunCI, txtSunCO))
            //{
            //    wh = new WorkingHour()
            //    {
            //        WorkHourID = Convert.ToInt32(hdnSunID.Value),
            //        Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtSunCI.Text, _InstanceID).ToString()),
            //        Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtSunCO.Text, _InstanceID).ToString()),
            //        Active = true,
            //        AutoPresent = chkAutoPresent.Checked,
            //        crtDate = DateTime.Now,
            //        DayTitle = "Sunday",
            //        InstanceID = _InstanceID,
            //        UserID = Convert.ToInt32(hdnUserID.Value)
            //    };
            //    wh.Save();
            //}
            //if (chkAutoPresent.Checked)
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "", "StartAutoPresent();", true);
            //}
            //SetUserStatus();
        }

        private Boolean ValidateDayText(TextBox txtCI, TextBox txtCO)
        {
            if (String.IsNullOrEmpty(txtCI.Text) || String.IsNullOrEmpty(txtCO.Text))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            Fillgridworker(UserId);

            //Parent Working hours settings - To perform Auto-Clockin/out
            DataTable WorkingHours = new DataTable();
            int Workinghours_count = atd.GetWorkingHours(atd.getParent(Request.QueryString["uid"].ToString()).ToInt32()).Rows.Count;

            if (!IsSimpleClock)
            {
                RegularExpressionValidator3.Visible = true;
                RegularExpressionValidator4.Visible = true;
                RegularExpressionValidator1.Visible = false;
                RegularExpressionValidator2.Visible = false;

                RegularExpressionValidator6.Visible = true;
                RegularExpressionValidator8.Visible = true;
                RegularExpressionValidator5.Visible = false;
                RegularExpressionValidator7.Visible = false;

                RegularExpressionValidator13.Visible = false;
                RegularExpressionValidator14.Visible = true;
                RegularExpressionValidator10.Visible = false;
                RegularExpressionValidator11.Visible = true;
            }
            else
            {
                RegularExpressionValidator3.Visible = false;
                RegularExpressionValidator4.Visible = false;
                RegularExpressionValidator1.Visible = true;
                RegularExpressionValidator2.Visible = true;

                RegularExpressionValidator6.Visible = false;
                RegularExpressionValidator8.Visible = false;
                RegularExpressionValidator5.Visible = true;
                RegularExpressionValidator7.Visible = true;

                RegularExpressionValidator13.Visible = true;
                RegularExpressionValidator14.Visible = false;
                RegularExpressionValidator10.Visible = true;
                RegularExpressionValidator11.Visible = false;

            }
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();

                using (SqlCommand cmd = new SqlCommand("getcurentuserstatus", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@ppid", UserId);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        lnkcomment.Visible = true;
                        btnsignin.Enabled = false;
                        btnsignout.Enabled = true;
                        //trPauseTime.Visible = true;
                        //btnsigninout.Text = "Clock out";
                        if (string.IsNullOrEmpty(txtcomment.Text))
                            txtcomment.Text = dt.Rows[0]["signin"].ToString();
                        hfcommentstatus.Value = "out";

                        //Auto update timezone Exceptional-case Hidden field
                        hdnErowID.Value = dt.Rows[0]["rowID"].ToString();
                        hdnESigninDateTime.Value = dt.Rows[0]["lastin"].ToString();

                        //End

                        //If Working hours settings is enabled than perform Auto-Clockout 
                        if (Workinghours_count != 0)
                            // Estimate clockout time                        
                            EstimateAndSetClockoutTime(dt);


                        //try
                        //{
                        //    workertran workertran1 = workertran.Find(u => u.P_PID == Convert.ToInt32(hdnUserID.Value)).Where(u => u.lastin.Value.Date == DateTime.Now.Date).ToList().First();
                        //    db = new avaimaTest0001DB();
                        //    wh = db.GetWorkingHourOfUser(Convert.ToInt32(hdnUserID.Value), InstanceID, DateTime.Now.DayOfWeek.ToString()).ExecuteDataSet().Tables[0].ToWorkingHour();
                        //    if (workertran1 != null && wh != null)
                        //    {
                        //        if (workertran1.lastout.Value.TimeOfDay >= wh.Clockout.Value.TimeOfDay)
                        //        {
                        //            lnkAddOvertime.Visible = true;
                        //            hdnCOTime.Value = DateTime.Now.ToShortDateString() + " " + App.GetTime(workertran1.lastout.ToString(), AppId);
                        //            hdnCOServerTime.Value = workertran1.lastout.Value.ToString();
                        //            //Response.Write(workertran1.lastout.Value.ToString());
                        //            txtOverTimeFrom.Text = App.GetTime(workertran1.lastout.ToString(), AppId);
                        //            btnsignin.Visible = false;
                        //        }
                        //    }
                        //    else
                        //    { lnkAddOvertime.Visible = false; }
                        //}
                        //catch (Exception ex)
                        //{
                        //    lblErrTitle.Text = ex.Message;
                        //    lblErrDetail.Text = ex.ToString();
                        //    ScriptManager.RegisterStartupScript(this, GetType(), "", "OpenDialogWB('#divErr');", true);
                        //}

                    }
                    else
                    {
                        lnkcomment.Visible = false;
                        hfcommentstatus.Value = "in";
                        btnsignin.Enabled = true;
                        btnsignout.Enabled = false;

                        //Auto update timezone Exceptional-case Hidden field
                        hdnErowID.Value = "0";

                        //If Working hours settings is enabled than perform Auto-Clockin
                        if (Workinghours_count != 0)
                            // Estimate clockin time                        
                            EstimateAndSetClockinTime();


                        workertran workertran1;
                        try { workertran1 = workertran.Find(u => u.P_PID == Convert.ToInt32(hdnUserID.Value)).Where(u => u.lastin.Value.Date == DateTime.Now.Date).ToList().SingleOrDefault(); }
                        catch (Exception ex)
                        {
                            workertran1 = null;
                            //lblErrTitle.Text = ex.Message;
                            //lblErrDetail.Text = ex.ToString();
                            //ScriptManager.RegisterStartupScript(this, GetType(), "", "OpenDialogWB('#divErr');", true);
                        }

                        MyAttSys.avaimaTest0001DB db;
                        // MyAttSys.WorkingHour wh;
                        db = new avaimaTest0001DB();
                        //  wh = db.GetWorkingHourOfUser(Convert.ToInt32(hdnUserID.Value), InstanceID, DateTime.Now.DayOfWeek.ToString()).ExecuteDataSet().Tables[0].ToWorkingHour();
                        //if (workertran1 != null)
                        //{
                        //    //if (workertran1.lastout.Value.TimeOfDay >= wh.Clockout.Value.TimeOfDay)
                        //    //{

                        //    // lnkAddOvertime.Visible = true;

                        //    DateTime dtNow = (DateTime)DateTime.Now.toClientTime();
                        //    DateTime dtCOTime = (DateTime.Now.ToShortDateString() + " " + workertran1.lastout.ToString()).ToDateTime();
                        //    if (dtNow.ToString("tt") == "PM" && dtCOTime.ToString("tt") == "AM")
                        //    {
                        //        dtCOTime.AddDays(1);
                        //    }
                        //    hdnCOTime.Value = dtCOTime.ToString() + ".";
                        //    hdnCOServerTime.Value = workertran1.lastout.Value.ToString() + ".";
                        //    //Response.Write(workertran1.lastout.Value.ToString());
                        //    //txtOverTimeFrom.Text = App.GetTime(workertran1.lastout.ToString(), AppId);
                        //    btnsignin.Visible = true;
                        //    //}
                        //}
                        //else
                        //{
                        //    //if (DateTime.Now.TimeOfDay >= wh.Clockout.Value.TimeOfDay && DateTime.Now.TimeOfDay < Convert.ToDateTime(wh.Clockin.Value.ToShortTimeString()).AddMinutes(-3).TimeOfDay)
                        //    //{ lnkAddOvertime.Visible = true; }
                        //    //else
                        //    //{
                        //    if (hdnIsWeekend.Value == "false")
                        //    {
                        //        //lnkAddOvertime.Visible = false;
                        //    }


                        //    //}
                        //}

                        //trPauseTime.Visible = false;
                        //btnsigninout.Text = "Clock in";
                    }
                }
            }
        }

        private void EstimateAndSetClockoutTime(DataTable dt)
        {
            Hashtable ht;
            DataTable workinghours = new DataTable();

            DateTime? dtLogin = Convert.ToDateTime(dt.Rows[0]["lastin"].ToString());

            ht = new Hashtable();
            ht["@UserID"] = Convert.ToInt32(UserId);
            ht["@DayTitle"] = dtLogin.Value.DayOfWeek;
            ht["@parentID"] = Convert.ToInt32(atd.getParent(UserId));
            ht["@type"] = "out";
            string out_time = dal.GetDataTable("sp_GetClockInOutTime", ht).Rows[0]["AutoClockout_Time"].ToString();

            //Default 
            hdnCOTime.Value = "";
            hdnIsAutoClockout.Value = "false";

            if (dtLogin.Value.TimeOfDay < Convert.ToDateTime(dtLogin.Value.ToShortDateString() + " " + out_time).TimeOfDay)
            {
                if (out_time != "-1")
                {
                    lblClockouttime.Text = Convert.ToDateTime(dtLogin.Value.ToShortDateString() + " " + out_time).ToString("hh:mm tt");
                    hdnCOTime.Value = Convert.ToDateTime(dtLogin.Value.ToShortDateString() + " " + out_time).ToString();
                    hdnIsAutoClockout.Value = "true";
                }
            }
        }

        private void EstimateAndSetClockinTime()
        {
            Hashtable ht;
            DataTable workinghours = new DataTable();

            ht = new Hashtable();
            ht["@userid"] = atd.Timezone_datetime(UserId, "", "");
            DataTable dt = dalAvaima.GetDataTable("gettimezone", ht);

            //User's specific timezone from Avaima Profile
            DateTime currentDateTime = atd.Timezone_datetime(UserId, DateTime.UtcNow.ToString(), "date", dt.Rows[0]["timezone"] + "#" + dt.Rows[0]["dlsaving"] + "#" + dt.Rows[0]["dlsavinghour"]).ToDateTime();

            ht = new Hashtable();
            ht["@UserID"] = Convert.ToInt32(UserId);
            ht["@parentID"] = Convert.ToInt32(atd.getParent(UserId));
            ht["@DayTitle"] = currentDateTime.DayOfWeek;
            ht["@type"] = "in";
            string in_time = dal.GetDataTable("sp_GetClockInOutTime", ht).Rows[0]["AutoClockin_Time"].ToString();

            if (in_time != "-1")
            {
                hdnClockinTime.Value = Convert.ToDateTime(currentDateTime.ToShortDateString() + " " + in_time).ToString();
                hdnIsAutoClockin.Value = "true";
            }
            else
            {
                hdnClockinTime.Value = "";
                hdnIsAutoClockin.Value = "false";
            }
        }

        //private void EstimateAndSetClockoutTime_OLD(DataTable dt)
        //{
        //    MyAttSys.avaimaTest0001DB db;
        //    MyAttSys.WorkingHour wh;
        //    db = new avaimaTest0001DB();
        //    wh = db.GetWorkingHourOfUser(Convert.ToInt32(hdnUserID.Value), InstanceID, DateTime.Now.DayOfWeek.ToString()).ExecuteDataSet().Tables[0].ToWorkingHour();
        //    DateTime? dtLogin = Convert.ToDateTime(dt.Rows[0]["lastin"].ToString());
        //    List<UnderTime> underTimes = UnderTime.Find(u => u.UserID == Convert.ToInt32(hdnUserID.Value)).ToList().Where(u => u.FromTime.Value.Date == dtLogin.Value.Date).ToList();
        //    TimeSpan tsUndertime = new TimeSpan(0);
        //    foreach (UnderTime ut in underTimes)
        //    {
        //        if (ut.FromTime != null && ut.ToTime != null) { tsUndertime += (TimeSpan)(ut.ToTime - ut.FromTime); }
        //    }

        //    DateTime dtCOTime = Convert.ToDateTime(dtLogin.Value.ToShortDateString() + " " + App.GetTime(wh.Clockout.Value.ToString(), AppId)).AddMinutes(Convert.ToInt32(tsUndertime.TotalMinutes));
        //    DateTime dtNow = (dtLogin.Value.ToShortDateString() + " " + DateTime.Now.toClientTime().Value.ToShortTimeString()).ToDateTime();
        //    DateTime dtServerCOTime = Convert.ToDateTime((dtLogin.Value.ToShortDateString() + " " + wh.Clockout.Value.ToShortTimeString())).AddMinutes(Convert.ToInt32(tsUndertime.TotalMinutes));
        //    DateTime dtNowServer = (dtLogin.Value.ToShortDateString() + " " + DateTime.Now.ToShortDateString()).ToDateTime();
        //    if (dtNow.ToString("tt") == "PM" && dtCOTime.ToString("tt") == "AM")
        //    {
        //        dtCOTime = dtCOTime.AddDays(1);
        //    }
        //    if (dtNowServer.ToString("tt") == "PM" && dtServerCOTime.ToString("tt") == "AM")
        //    {
        //        dtServerCOTime = dtServerCOTime.AddDays(1);
        //    }

        //    hdnCOTime.Value = dtCOTime.ToString() + "...";
        //    hdnCOServerTime.Value = dtServerCOTime.ToString();
        //    ScriptManager.RegisterStartupScript(this, GetType(), "", "$('.tbCelli-i').html('" + Convert.ToDateTime(hdnCOTime.Value).ToShortTimeString() + "');", true);
        //}

        //private void EstimateAndSetClockoutTime(DataTable dt)
        //{
        //    MyAttSys.avaimaTest0001DB db;
        //    MyAttSys.WorkingHour wh;
        //    db = new avaimaTest0001DB();
        //  //  wh = db.GetWorkingHourOfUser(Convert.ToInt32(hdnUserID.Value), InstanceID, DateTime.Now.DayOfWeek.ToString()).ExecuteDataSet().Tables[0].ToWorkingHour();
        //    DateTime? dtLogin = Convert.ToDateTime(dt.Rows[0]["lastin"].ToString());
        //    List<UnderTime> underTimes = UnderTime.Find(u => u.UserID == Convert.ToInt32(hdnUserID.Value)).ToList().Where(u => u.FromTime.Value.Date == dtLogin.Value.Date && u.Type == "Late").ToList();
        //    TimeSpan tsUndertime = new TimeSpan(0);
        //    foreach (UnderTime ut in underTimes) { if (ut.FromTime != null && ut.ToTime != null) { tsUndertime += (TimeSpan)(ut.ToTime - ut.FromTime); } }

        //    //usman DateTime dtCOTime = Convert.ToDateTime(dtLogin.Value.ToShortDateString() + " " + App.GetTime(wh.Clockout.Value.ToString(), AppId)).AddMinutes(Convert.ToInt32(tsUndertime.TotalMinutes));
        //    //usman DateTime dtServerCOTime = Convert.ToDateTime((dtLogin.Value.ToShortDateString() + " " + wh.Clockout.Value.ToShortTimeString())).AddMinutes(Convert.ToInt32(tsUndertime.TotalMinutes));

        //   // DateTime dtCOTime = Convert.ToDateTime(dtLogin.Value.ToShortDateString() + " " + App.GetTime(wh.Clockout.Value.ToString(), AppId));
        //  //  DateTime dtServerCOTime = Convert.ToDateTime((dtLogin.Value.ToShortDateString() + " " + wh.Clockout.Value.ToShortTimeString()));
        //    DateTime dtNow = (DateTime)DateTime.Now.toClientDate();
        //    //if (dtCOTime.Date != dtNow.Date) { dtNow = (dtLogin.Value.ToShortDateString() + " " + DateTime.Now.toClientTime().Value.ToShortTimeString()).ToDateTime(); }
        //    DateTime dtNowServer = DateTime.Now;
        //    if (dtNowServer.Date != dtServerCOTime.Date) { (dtLogin.Value.ToShortDateString() + " " + DateTime.Now.ToShortTimeString()).ToDateTime(); }

        //   // if ((dtNow.ToString("tt") == "PM" && dtCOTime.ToString("tt") == "AM") && (dtNow.Date == dtCOTime.Date)) { dtCOTime = dtCOTime.AddDays(1); }
        //    if (dtNowServer.ToString("tt") == "PM" && dtServerCOTime.ToString("tt") == "AM" && (dtNowServer.Date == dtServerCOTime.Date)) { dtServerCOTime = dtServerCOTime.AddDays(1); }

        //    workertran workerTran = workertran.SingleOrDefault(u => u.rowID == hdnAnotherCO.Value.ToInt32());
        //    if (workerTran != null) { workerTran.lastout_actual = dtServerCOTime; }

        //    //wt.lastout_actual = dtServerCOTime;
        //    //wt.Update
        //    //hdnCOTime.Value = dtCOTime.ToString() + "...";
        //    hdnCOServerTime.Value = dtServerCOTime.ToString();
        //    ScriptManager.RegisterStartupScript(this, GetType(), "", "$('.tbCelli-i').html('" + Convert.ToDateTime(hdnCOTime.Value).ToShortTimeString() + "');", true);
        //}

        private string getParent(string userid)
        {
            DataTable dt = new DataTable();

            //Check if User has a Parent
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select * from schema_6e815b00_6e13_4839_a57d_800a92809f21.attendence_management where ID = @ID", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@ID", userid);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                }
            }
            return (dt.Rows[0]["parentid"].ToString() != "0") ? dt.Rows[0]["parentid"].ToString() : userid;

        }

        private DataTable GetHolidays(string type)
        {
            DataTable dt = new DataTable();
            string userid = atd.getParent(hdnParentid.Value);

            ////Check if User has a Parent
            //using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            //{                
            //    using (SqlCommand cmd = new SqlCommand("Select * from schema_6e815b00_6e13_4839_a57d_800a92809f21.attendence_management where ID = @ID", con))
            //    {
            //        con.Open();
            //        cmd.Parameters.AddWithValue("@ID", hdnParentid.Value);
            //        SqlDataAdapter adp = new SqlDataAdapter(cmd);
            //        adp.Fill(dt);
            //    }
            //}
            //string userid = (dt.Rows[0]["parentid"].ToString() != "0") ? dt.Rows[0]["parentid"].ToString() : hdnParentid.Value;

            //dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                if (type == "weekend")
                {
                    using (SqlCommand cmd = new SqlCommand("sp_Weekends", con))
                    {
                        con.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        cmd.Parameters.AddWithValue("@userid", userid);
                        cmd.Parameters.AddWithValue("@action", "get");
                        adp.Fill(dt);

                    }
                }
                else if (type == "holiday")
                {
                    using (SqlCommand cmd = new SqlCommand("sp_Holidays", con))
                    {
                        con.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        cmd.Parameters.AddWithValue("@userid", userid);
                        cmd.Parameters.AddWithValue("@action", "get");
                        adp.Fill(dt);
                    }
                }

            }
            return dt;
        }
        public void Fillgridworker(string pid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("getclocktype", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    cmd.Parameters.AddWithValue("@instanceId", _InstanceID);
                    bool i = (bool)cmd.ExecuteScalar();
                    if (i)
                    {
                        hftimeFormat.Value = "H:i";
                        IsSimpleClock = false;
                    }
                    else
                    {
                        hftimeFormat.Value = "h:i A";
                    }
                }
                using (SqlCommand cmd = new SqlCommand("getTodaysBreak", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    cmd.Parameters.Clear();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                    cmd.Parameters.AddWithValue("@Date", DateTime.Now.ToString("yyyy-MM-dd"));
                    cmd.Parameters.AddWithValue("@id", pid);
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0 && !string.IsNullOrEmpty(dt.Rows[0]["Bid"].ToString()))
                    {
                        // rptb.DataSource = dt;
                        //rptb.DataBind();
                    }
                    else
                    {
                        //noBreak.Visible = true;
                    }
                }

                FillandCreateHistory(pid, con);
                //GetHistory(pid, con);

                using (SqlCommand cmd = new SqlCommand("getLocations", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    cmd.Parameters.Clear();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                    cmd.Parameters.AddWithValue("@id", DBNull.Value);
                    dt = new DataTable();
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        location.DataSource = dt.Select("disabled = 0").CopyToDataTable();
                        location.DataTextField = "location";
                        location.DataValueField = "id";
                        location.DataBind();
                    }
                    else
                    {
                        LocationContainer.Visible = false;
                    }
                }
            }
        }
        private DataTable GetHistory(string userId, SqlConnection con)
        {
            using (SqlCommand cmd = new SqlCommand("sp_GetHistory", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                List<string> dates;
                dates = UtilityMethods.getpastDates(7);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@UserId", userId);
                adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                return ds.Tables[0];
                //records = UtilityMethods.getRecords(ds);
            }

        }

        private void FillandCreateHistory(string pid, SqlConnection con)
        {
            string parentid = getParent(pid);
            //if (pid != parentid)
            if (Request.QueryString["id"] != parentid)
            {
                //Edit Option Allowed
                DataTable settings = new DataTable();
                using (SqlCommand cmd = new SqlCommand("sp_ReportSettings", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@action", "get");
                    cmd.Parameters.AddWithValue("@userid", parentid);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(settings);
                }

                if (settings.Rows.Count > 0)
                {
                    hdnIsEditAllowed.Value = settings.Rows[0]["AllowTimeEdit"].ToString();
                }
            }
            //

            DataTable dt;
            using (SqlCommand cmd = new SqlCommand("sp_GetHistory", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                List<string> dates;
                dates = UtilityMethods.getpastDates(7);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@UserId", pid);
                adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                dt = ds.Tables[0];
            }

            CreateTable(dt);

            //DataTable dt=GetHistory(pid,con);
            //return dt;
            //using (SqlCommand cmd = new SqlCommand("getworkerrecordbydate", con))
            //{
            //cmd.CommandType = CommandType.StoredProcedure;
            //SqlDataAdapter adp = new SqlDataAdapter(cmd);
            //List<string> dates;
            //dates = UtilityMethods.getpastDates(7);
            //cmd.Parameters.Clear();
            //cmd.Parameters.AddWithValue("@sDate", dates.Last());
            //cmd.Parameters.AddWithValue("@eDate", dates.First());
            //cmd.Parameters.AddWithValue("@Id", pid);
            //cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
            //adp = new SqlDataAdapter(cmd);
            //DataSet ds = new DataSet();
            //adp.Fill(ds);

            //records = UtilityMethods.getRecords(ds);
            //rptHistory.DataSource = records;
            //rptHistory.DataBind();
            //try
            //{
            //    //Response.Write(records[records.Count - 1].Overtimes.Count.ToString());
            //    //Response.Write(records[records.Count - 1].Undertimes.Count.ToString());
            //}
            //catch (Exception)
            //{
            //}

            //if (records.Where(u => u.Records.Count() > 0).ToList().Count() > 0 || SP.GetWorkerFirstSignInn(UserId.ToString()) != Helper.DefaultDateTime)
            //{
            //    divStats.Visible = true;
            //    
            //}
            //else
            //{
            //    divStats.Visible = false;
            //    ScriptManager.RegisterStartupScript(this, GetType(), "", "InstructNewUser();", true);
            //}

            //}
        }

        protected void rpth_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void rpth_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
                RecordModel container = (RecordModel)e.Item.DataItem;
                Label lblDate = e.Item.FindControl("lblDate") as Label;
                if (lblDate.Text.Contains("Today"))
                {
                    return;
                }
                //Label loginTime = e.Item.FindControl("loginTime") as Label;
                //Label breakTime = e.Item.FindControl("breakTime") as Label;
                //Label logoutTime = e.Item.FindControl("logoutTime") as Label;
                //Label TotalHours = e.Item.FindControl("TotalHours") as Label;
                Label lblWorked = e.Item.FindControl("lblWorked") as Label;
                lblWorked.Text = container.TotalTime;
                Image imgMDetails = e.Item.FindControl("imgInfo") as Image;
                //Label dtitle = e.Item.FindControl("dtitle") as Label;
                HtmlControl tr = e.Item.FindControl("tr") as HtmlControl;
                HtmlControl rContainer = e.Item.FindControl("rContainer") as HtmlControl;
                //HtmlControl rFooter = e.Item.FindControl("rFooter") as HtmlControl;
                Repeater subRecordsClockIn = e.Item.FindControl("rptWorkerClockIn") as Repeater;
                Repeater subRecordsClockOut = e.Item.FindControl("rptWorkerClockOut") as Repeater;

                lblDate.Text = container.Date.ToString();
                subRecordsClockIn.DataSource = container.Records;
                subRecordsClockOut.DataSource = container.Records;
                subRecordsClockIn.DataBind();
                subRecordsClockOut.DataBind();
                String strDetails = "";
                //TotalHours.Text = container.TotalTime;
                if (container.Breaks.Count > 0 && container.Breaks.All(x => x.Id.HasValue == true))
                {
                    //breakTime.Text = "<div style=\"font-weight:bold;float:left\">Break(s):</div><div class=\"BPTime\">";
                    strDetails = "<table id=\"tblDetail\">";
                    foreach (BreakModel b in container.Breaks)
                    {
                        if (b.Id.HasValue)
                        {
                            //breakTime.Text += string.Format("<div>From: {0} To: {1}</div>", Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + b.BreakStartTime.ToString()).ToShortTimeString(), Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + b.BreakEndTime.ToString()).ToShortTimeString());
                            strDetails += string.Format("<tr><td><b>Break(s):</b></td><td>From: {0} To: {1}", Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + b.BreakStartTime.ToString()).ToShortTimeString(), Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + b.BreakEndTime.ToString()).ToShortTimeString()) + "</td></tr>";
                        }
                    }
                    //breakTime.Text += "</div>";
                }
                strDetails += "<tr><td><b>Total Time:</b><td>" + container.TotalTime + "</td></tr></table>";
                imgMDetails.ToolTip = strDetails;
                if (container.DayType.HasValue)
                {
                    if (container.Records.Count <= 0)
                    {
                        imgMDetails.Visible = false;
                        subRecordsClockIn.Visible = false;
                    }
                    if (string.IsNullOrEmpty(container.DayTitle))
                    {
                        //dtitle.Text = "Holiday";
                    }
                    else
                    {
                        //dtitle.Text = container.DayTitle;
                    }
                    //dtitle.Visible = true;
                    if (container.DayType.Value == 0)
                        //rContainer.Attributes.Add("class", "SatSunHolidays");
                        rContainer.Attributes.Add("class", "greenRow");
                    else
                        //rContainer.Attributes.Add("class", "NormalHoliday");
                        rContainer.Attributes.Add("class", "yellowRow");
                }
                else if (container.Records.Count <= 0)
                {
                    // Check on datetime
                    DateTime workerRequestDate = SP.GetWorkerFirstSignInn(UserId);

                    if (workerRequestDate.Date > container.Date)
                    {
                        imgMDetails.Visible = false;
                        //dtitle.Text = "Not Recruited";
                        subRecordsClockIn.Visible = false;
                        //dtitle.Visible = true;
                        rContainer.Attributes.Add("class", "NotRecruitDiv");
                        //rContainer.Style.Add("color", "#0094ff");
                    }
                    else if (workerRequestDate.Date == new DateTime(1900, 1, 1))
                    {
                        imgMDetails.Visible = false;
                        //dtitle.Text = "Request Sent";
                        subRecordsClockIn.Visible = false;
                        //dtitle.Visible = true;
                        rContainer.Attributes.Add("class", "RequestSendDiv");
                    }
                    else
                    {
                        imgMDetails.Visible = false;
                        //dtitle.Text = "Absent";
                        subRecordsClockIn.Visible = false;
                        //dtitle.Visible = true;
                        //rContainer.Attributes.Add("class", "AbsentDiv");
                        rContainer.Attributes.Add("class", "pinkRow");
                    }
                }
                else
                {
                    imgMDetails.Visible = true;
                    subRecordsClockIn.Visible = true;
                }
            }
        }

        protected void Rptrworkerdatabound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                //HtmlTableCell allowEdit = e.Item.FindControl("allowEdit") as HtmlTableCell;
                //if (isWorker)
                //{
                //    allowEdit.Visible = false;
                //}
            }
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                String strDetail = "";
                AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
                SubRecordModel container = (SubRecordModel)e.Item.DataItem;
                //LinkButton removeHours = e.Item.FindControl("removeHours") as LinkButton;
                LinkButton lnkedit = e.Item.FindControl("lnkedit") as LinkButton;
                //Label lblcomment = e.Item.FindControl("lblcomment") as Label;
                //Label lblinlocation = e.Item.FindControl("lblinlocation") as Label;
                Label lblTotal = e.Item.FindControl("lblTotal") as Label;
                //Label lbloutlocation = e.Item.FindControl("lbloutlocation") as Label;
                Label lblDetail = e.Item.FindControl("lblDetail") as Label;
                HtmlTableCell allowEditOpt = e.Item.FindControl("allowEditOpt") as HtmlTableCell;
                ImageButton si = e.Item.FindControl("undoSigIn") as ImageButton;
                ImageButton so = e.Item.FindControl("undoSignOut") as ImageButton;
                strDetail = "<div style=\"text-align:center\"><table id=\"tblDetail\"><tr><td>Sign-In Location: </td><td>" + container.InLocation + "</td></tr>";
                strDetail += "<tr><td>Sign-Out Location:</td> <td>" + container.OutLocation + "</td></tr>";

                lblTotal.Text = container.Time;

                //removeHours.CommandArgument = container.Id.ToString();
                if (!isWorker)
                {
                    lnkedit.CommandArgument = container.Id.ToString();
                }
                else
                {
                    allowEditOpt.Visible = false;
                }
                Label lbldate = e.Item.FindControl("lblintime") as Label;
                if (!String.IsNullOrEmpty(container.LoginTime))
                {
                    //if (Session["siID"] != null && (container.Id == Convert.ToInt32(Session["siID"])))
                    //{
                    //    if (DateTime.Now < (Convert.ToDateTime(container.LoginTime).Add(new TimeSpan(0, 5, 0))))
                    //    {
                    //        si.CommandName = "undoSigIn";
                    //        si.CommandArgument = container.Id.ToString();
                    //        si.Visible = true;
                    //    }
                    //}
                    string time = container.LoginTime;
                    //lbldate.Text = Convert.ToDateTime(App.GetDate(container.LoginTime,
                    //                              AppId) + " " + ((!IsSimpleClock) ? Convert.ToDateTime(time).ToString("HH:mm") : time)).ToString("HH:mm tt");
                    lbldate.Text = time;
                }
                else
                {
                    //removeHours.Visible = false;
                }

                Label lbltime = e.Item.FindControl("lblouttime") as Label;
                if (!String.IsNullOrEmpty(container.LogoutTime))
                {
                    //if (Session["soID"] != null && (container.Id == Convert.ToInt32(Session["soID"])))
                    //{
                    //    if (DateTime.Now < (Convert.ToDateTime(container.LogoutTime).Add(new TimeSpan(0, 5, 0))))
                    //    {
                    //        so.CommandName = "undoSignOut";
                    //        so.CommandArgument = container.Id.ToString();
                    //        so.Visible = true;
                    //    }
                    //}
                    string time = container.LogoutTime;
                    //lbltime.Text = Convert.ToDateTime(App.GetDate(container.LogoutTime, 
                    //                            AppId) + " " + ((!IsSimpleClock) ? Convert.ToDateTime(time).ToString("HH:mm") : time)).ToString("HH:mm tt");
                    lbltime.Text = time;
                }
                else
                {
                    //removeHours.Visible = false;
                }

                //Label lblinstatus = e.Item.FindControl("lblinstatus") as Label;
                if (!String.IsNullOrEmpty(container.LoginStatus))
                {
                    if (container.LoginStatus == "Verified")
                    {
                        strDetail += "<tr><td class=\"caption\">Sign-In Verification: </td><td><image src=\"../images/Controls/tick.png\" width=\"20px\" /></td></tr>";
                        //lblinstatus.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        strDetail += "<tr><td class=\"caption\">Sign-In Verification: </td><td><image src=\"../images/Controls/cross.png\" width=\"20px\" /></td></tr>";
                        //lblinstatus.ForeColor = System.Drawing.Color.Red;
                    }
                }

                //Label lbloutstatus = e.Item.FindControl("lbloutstaus") as Label;
                if (!String.IsNullOrEmpty(container.LogoutStatus))
                {
                    if (container.LogoutStatus == "Verified")
                    {
                        strDetail += "<tr><td class=\"caption\">Sign-Out Verification: </td><td><image src=\"../images/Controls/tick.png\" width=\"20px\" /></td></tr>";
                        //lbloutstatus.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        strDetail += "<tr><td class=\"caption\">Sign-Out Verification: </td><td><image src=\"../images/Controls/cross.png\" width=\"20px\" /></td></tr>";
                        //lbloutstatus.ForeColor = System.Drawing.Color.Red;
                    }
                }
                if (container.OutComment != null && container.OutComment.ToString().Length > 10)
                {
                    strDetail += "<tr><td>Comments:</td><td>" + container.OutComment.ToString() + "</td></tr>";
                    //lblcomment.ToolTip = container.OutComment;
                }
                strDetail += "</table>";
                lblDetail.Style.Add("cursor", "pointer");
                lblDetail.ToolTip = strDetail;
            }
        }

        protected void Rptrworkeritemcommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.CommandName == "delTime")
                {
                    AvaimaTimeZoneAPI obj = new AvaimaTimeZoneAPI();
                    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("getWorkerRecordByRowId", con))
                        {
                            con.Open();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@id", e.CommandArgument);
                            cmd.Parameters.AddWithValue("@ppid", UserId);
                            DataTable dt = new DataTable();
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            da.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                string time = dt.Rows[0].Field<DateTime>("lastin").ToString();
                                txtfrmTime.Text = ((!IsSimpleClock) ? Convert.ToDateTime(time).ToString("HH:mm") : time);
                                time = dt.Rows[0].Field<DateTime>("lastout").ToString();
                                txtToTime.Text = ((!IsSimpleClock) ? Convert.ToDateTime(time).ToString("HH:mm") : time);
                            }
                        }
                    }
                    rID.Value = e.CommandArgument.ToString();
                    ScriptManager.RegisterStartupScript(this, GetType(), "", "delTime();", true);
                }
                if (e.CommandName == "edit")
                {
                    AvaimaTimeZoneAPI obj = new AvaimaTimeZoneAPI();
                    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("getWorkerRecordByRowId", con))
                        {
                            con.Open();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@id", e.CommandArgument);
                            cmd.Parameters.AddWithValue("@ppid", UserId);
                            DataTable dt = new DataTable();
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            da.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                if (!String.IsNullOrEmpty(dt.Rows[0].Field<string>("instatus")))
                                {
                                    if (dt.Rows[0].Field<string>("instatus") == "Verified")
                                    {
                                        ddlsigninstatus.SelectedValue = "Verified";
                                    }
                                    else
                                    {
                                        ddlsigninstatus.SelectedValue = "Not Verified";
                                    }
                                }
                                if (!String.IsNullOrEmpty(dt.Rows[0].Field<string>("outstatus")))
                                {
                                    if (dt.Rows[0].Field<string>("outstatus") == "Verified")
                                    {
                                        ddlsignoutstatus.SelectedValue = "Verified";
                                    }
                                    else
                                    {
                                        ddlsignoutstatus.SelectedValue = "Not Verified";
                                    }
                                }

                                if (!string.IsNullOrEmpty(dt.Rows[0]["lastin"].ToString()))
                                {
                                    string time = dt.Rows[0].Field<DateTime>("lastin").ToString();
                                    txteditintime.Text = ((!IsSimpleClock)
                                                              ? Convert.ToDateTime(time).ToString("HH:mm")
                                                              : time);
                                    txteditsignin.Text = dt.Rows[0].Field<DateTime>("lastin").ToString("dd/MM/yyyy").Replace('-', '/');
                                }
                                if (!string.IsNullOrEmpty(dt.Rows[0]["lastout"].ToString()))
                                {
                                    string time = dt.Rows[0].Field<DateTime>("lastout").ToString();
                                    txteditoutitme.Text = ((!IsSimpleClock)
                                                               ? Convert.ToDateTime(time).ToString("HH:mm")
                                                               : time);
                                    txteditsignout.Text = dt.Rows[0].Field<DateTime>("lastout").ToString("dd/MM/yyyy").Replace('-', '/');
                                }
                                txteditcomment.Text = dt.Rows[0].Field<string>("signout");
                            }
                        }
                    }

                    hfeditid.Value = e.CommandArgument.ToString();
                    ScriptManager.RegisterStartupScript(this, GetType(), "", "editdiv();", true);
                }
                if (e.CommandName == "undoSigIn")
                {
                    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("getWorkerRecordByRowId", con))
                        {
                            con.Open();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@id", e.CommandArgument);
                            cmd.Parameters.AddWithValue("@ppid", UserId);
                            DataTable dt = new DataTable();
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            da.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dt.Rows[0]["lastin"].ToString()))
                                {
                                    if (DateTime.Now < (Convert.ToDateTime(dt.Rows[0]["lastin"].ToString()).Add(new TimeSpan(0, 5, 0))))
                                    {
                                        cmd.Parameters.Clear();
                                        cmd.CommandText = @"delete from workertrans where rowID = @id";
                                        cmd.Parameters.AddWithValue("@id", e.CommandArgument);
                                        cmd.CommandType = CommandType.Text;
                                        cmd.ExecuteNonQuery();
                                    }
                                }
                            }
                        }
                    }
                }
                if (e.CommandName == "undoSignOut")
                {
                    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("getWorkerRecordByRowId", con))
                        {
                            con.Open();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@id", e.CommandArgument);
                            cmd.Parameters.AddWithValue("@ppid", UserId);
                            DataTable dt = new DataTable();
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            da.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dt.Rows[0]["lastout"].ToString()))
                                {
                                    if (DateTime.Now < (Convert.ToDateTime(dt.Rows[0]["lastout"].ToString()).Add(new TimeSpan(0, 5, 0))))
                                    {
                                        cmd.Parameters.Clear();
                                        cmd.CommandText = @"update workertrans set lastout = null, outstatus = null, out_location = null  where rowID = @id";
                                        cmd.Parameters.AddWithValue("@id", e.CommandArgument);
                                        cmd.CommandType = CommandType.Text;
                                        cmd.ExecuteNonQuery();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void deleHrs_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(rID.Value))
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("getWorkerRecordByRowId", con))
                    {
                        AvaimaTimeZoneAPI obj = new AvaimaTimeZoneAPI();
                        con.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@id", rID.Value);
                        cmd.Parameters.AddWithValue("@ppid", UserId);
                        DataTable dt = new DataTable();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            DateTime frmDateTime = Convert.ToDateTime(dt.Rows[0].Field<DateTime>("lastin").ToString("g"));
                            DateTime toDateTime = Convert.ToDateTime(dt.Rows[0].Field<DateTime>("lastout").ToString("g"));

                            DateTime tfdt = Convert.ToDateTime(frmDateTime.ToShortDateString() + " " + obj.GetTimeReverse(txtfrmTime.Text, UserId));
                            DateTime ttdt = Convert.ToDateTime(toDateTime.ToShortDateString() + " " + obj.GetTimeReverse(txtToTime.Text, UserId));
                            if (tfdt < frmDateTime)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('From time must be greater than or equal to current saved login time')", true);
                                return;
                            }
                            else if (tfdt >= toDateTime)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('From time must be less than current saved logout time')", true);
                                return;
                            }
                            else if (ttdt > toDateTime)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('To time must be less than or equal to current saved logout time')", true);
                                return;
                            }
                            else if (ttdt <= frmDateTime)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('To time must be greater than current saved login time')", true);
                                return;
                            }
                            else if (tfdt >= ttdt)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('From time must be less than To time')", true);
                                return;
                            }
                            else
                            {
                                if (tfdt > frmDateTime && ttdt == toDateTime)
                                {
                                    cmd.CommandText = "updateWorkerTimeById";
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@Id", rID.Value);
                                    cmd.Parameters.AddWithValue("@ppId", UserId);
                                    cmd.Parameters.AddWithValue("@lastin", frmDateTime);
                                    cmd.Parameters.AddWithValue("@lastout", tfdt);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.ExecuteNonQuery();
                                }
                                else if (tfdt == frmDateTime && ttdt < toDateTime)
                                {
                                    cmd.CommandText = "updateWorkerTimeById";
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@Id", rID.Value);
                                    cmd.Parameters.AddWithValue("@ppId", UserId);
                                    cmd.Parameters.AddWithValue("@lastin", ttdt);
                                    cmd.Parameters.AddWithValue("@lastout", toDateTime);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.ExecuteNonQuery();
                                }
                                else if (tfdt > frmDateTime && ttdt < toDateTime)
                                {
                                    cmd.CommandText = "updateWorkerTimeById";
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@Id", rID.Value);
                                    cmd.Parameters.AddWithValue("@ppId", UserId);
                                    cmd.Parameters.AddWithValue("@lastin", frmDateTime);
                                    cmd.Parameters.AddWithValue("@lastout", tfdt);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.ExecuteNonQuery();

                                    cmd.CommandText = "insertWorkerTime";
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@ppId", UserId);
                                    cmd.Parameters.AddWithValue("@userid", UserId);
                                    cmd.Parameters.AddWithValue("@lastin", ttdt);
                                    cmd.Parameters.AddWithValue("@lastout", toDateTime);
                                    cmd.Parameters.AddWithValue("@address", Request.UserHostAddress);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.ExecuteNonQuery();
                                }
                                else if (tfdt == frmDateTime && ttdt == toDateTime)
                                {
                                    cmd.CommandText = "updateWorkerTimeById";
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@Id", rID.Value);
                                    cmd.Parameters.AddWithValue("@ppId", UserId);
                                    cmd.Parameters.AddWithValue("@lastin", frmDateTime);
                                    cmd.Parameters.AddWithValue("@lastout", frmDateTime);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                    SetUserStatus();
                    FillandCreateHistory(UserId, con);
                }
            }
        }

        protected void BtnupdateClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("updateworkertime", con))
                    {
                        AvaimaTimeZoneAPI objtime = new AvaimaTimeZoneAPI();
                        DateTime? timein = null;
                        if (!string.IsNullOrEmpty(txteditsignin.Text))
                        {
                            timein = Convert.ToDateTime(DateTime.ParseExact(txteditsignin.Text, "dd/MM/yyyy", null).ToShortDateString() + " " + objtime.GetTimeReverse(txteditintime.Text, AppId));
                        }

                        DateTime? timeout = null;
                        if (!string.IsNullOrEmpty(txteditsignout.Text))
                        {
                            timeout = Convert.ToDateTime(DateTime.ParseExact(txteditsignout.Text, "dd/MM/yyyy", null).ToShortDateString() + " " + objtime.GetTimeReverse(txteditoutitme.Text, AppId));
                        }
                        con.Open();
                        if (timein.HasValue)
                        {
                            cmd.Parameters.AddWithValue("@lastin", timein);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@lastin", DBNull.Value);
                        }

                        if (timeout.HasValue)
                        {
                            cmd.Parameters.AddWithValue("@lastout", timeout);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@lastout", DBNull.Value);
                        }
                        cmd.Parameters.AddWithValue("@id", hfeditid.Value);
                        cmd.Parameters.AddWithValue("@comment", txteditcomment.Text);
                        cmd.Parameters.AddWithValue("@instatus", ddlsigninstatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@outstatus", ddlsignoutstatus.SelectedValue);

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        protected void BtnsigninClick(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToBoolean(hdnIsVerified.Value))
                {
                    MyAttSys.avaimaTest0001DB db = new avaimaTest0001DB();
                    DataTable dt1 = db.getcurentuserstatus(Convert.ToInt32(hdnUserID.Value)).ExecuteDataSet().Tables[0];
                    if (dt1.Rows.Count > 0)
                    {
                        //ScriptManager.RegisterStartupScript(this, GetType(), "", "OpenDialog('#divAlreadyClockedIn');", true);
                        return;
                    }
                    string msgboxid = "";
                    if (!Convert.ToBoolean(hdnIsAutoClockin.Value))
                        atd.ClockIn(UserId, Request.UserHostAddress, "web", 1, TXTINReason.Text);
                    else
                        atd.ClockIn(UserId, Request.UserHostAddress, "web", 1, "Clock-in", true, hdnClockinTime.Value);

                    if (atd.ClockIn_Check(UserId, Request.QueryString["instanceid"].ToString()))
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "OpenDialog('#divEarlyClockIn');", true);

                    SetUserStatus();
                    ScriptManager.RegisterStartupScript(this, GetType(), "", "ActivateAlertDiv('none', 'AlertDiv');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ip", "openConfirmBox('Your IP Address is not verified by admin. Please consult your admin to allow access on your IP Address.');", true);

                }

                //Clock-in check
                //DataTable workinghours = atd.GetWorkingHours(atd.getParent(UserId).ToInt32());
                //DateTime utc_date = Convert.ToDateTime(DateTime.UtcNow);
                //string user_time = "00:00";
                ////atd.Timezone_datetime(UserId, utc_date.ToString(), "time");
                //string user_date = GetDate(atd.Timezone_datetime(UserId, utc_date.ToString(), "date"));
                //DataRow[] dr = workinghours.Select("DayTitle = '" + Convert.ToDateTime(user_date).DayOfWeek + "'");
                //List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(9845, "6752062f-f30f-4abe-b048-6cc2daee6d3d");
                ////List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(getParent(UserId).ToInt32(), instanceid);

                //DataTable dt = new DataTable();
                //using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                //{
                //    using (SqlCommand cmd = new SqlCommand("Select TOP 1 * from schema_6e815b00_6e13_4839_a57d_800a92809f21.ReportSettings where userid = @userid And Chophours = 1", con))
                //    {
                //        con.Open();
                //        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                //        cmd.Parameters.AddWithValue("@userid", UserId);
                //        adp.Fill(dt);
                //    }
                //}

                //DateTime ChophoursFrom = new DateTime();
                //if (dt.Rows.Count > 0)
                //    ChophoursFrom = Convert.ToDateTime(dt.Rows[0]["ChophoursFrom"]);

                //if (workinghours.Rows.Count > 0 && exDays.Count == 0)
                //{
                //    if (dr.Count() > 0 &&
                //        (Convert.ToDateTime(dr[0]["Clockin"]).TimeOfDay > user_time.ToDateTime().TimeOfDay && ChophoursFrom.TimeOfDay < user_time.ToDateTime().TimeOfDay))
                //    {
                //        ScriptManager.RegisterStartupScript(this, GetType(), "", "OpenDialog('#divEarlyClockIn');", true);
                //    }
                //}

                //if (exDays.Count > 0)
                //{
                //    List<ExceptionalDay> exDay = exDays.Where(u => u.Date.Date == Convert.ToDateTime(user_date).Date).ToList();

                //    if (exDay.Count > 0 &&
                //        (exDay[0].Clockin.TimeOfDay > user_time.ToDateTime().TimeOfDay && ChophoursFrom.TimeOfDay < user_time.ToDateTime().TimeOfDay)
                //        )
                //    {
                //        ScriptManager.RegisterStartupScript(this, GetType(), "", "OpenDialog('#divEarlyClockIn');", true);
                //    }
                //}
                //End


                //using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                //{
                //    using (SqlCommand cmd = new SqlCommand("insertuserintime", con))
                //    {
                //        con.Open();
                //        if (SP.GetIPAddresses(_InstanceID).Rows.Count == 0)
                //        {
                //            cmd.CommandText = "[insertuserintimeverified]";
                //        }
                //        cmd.CommandType = CommandType.StoredProcedure;
                //        cmd.Parameters.AddWithValue("@ppid", UserId);
                //        cmd.Parameters.AddWithValue("@userid", "uid");


                //        cmd.Parameters.AddWithValue("@lastin", Convert.ToDateTime(DateTime.Now));

                //        cmd.Parameters.AddWithValue("@inaddress", Request.UserHostAddress);
                //        cmd.Parameters.AddWithValue("@comment", "");
                //        if (location.Visible == true)
                //        {
                //            cmd.Parameters.AddWithValue("@location", location.SelectedValue);

                //        }
                //        else
                //        {
                //            cmd.Parameters.AddWithValue("@location", DBNull.Value);
                //        }
                //        int rowid = Convert.ToInt32(cmd.ExecuteScalar());
                //        workertran wt = workertran.SingleOrDefault(u => u.rowID == rowid);
                //        if (wt != null)
                //        {
                //            wt.lastin_actual = Convert.ToDateTime(DateTime.Now);
                //            wt.Update();
                //        }
                //        hrid.Value = rowid.ToString();




                //    }
                //}

            }
            catch (Exception ex)
            {
                lblErrTitle.Text = ex.Message;
                lblErrDetail.Text = ex.ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "", "OpenDialogWB('#divErr');", true);

            }
        }

        private DataTable getbreak(string pid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("getTodaysBreak", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    cmd.Parameters.Clear();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                    cmd.Parameters.AddWithValue("@Date", DateTime.Now.ToString("yyyy-MM-dd"));
                    cmd.Parameters.AddWithValue("@id", pid);
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    return dt;
                }
            }
        }

        protected void BtnSignoutClick(object sender, EventArgs e)
        {
            try
            {
                string msgboxid = "";

                if (!string.IsNullOrEmpty(txtAClockout.Text))
                {
                    //Check if on timezone update, current clockin time is less than last clockin time
                    //Get Dates accordingly incase of Forgot clockout
                    if (hdnEcheck.Value == "1")
                        atd.ClockOut(UserId, Request.UserHostAddress, rowID, "web", 1, txtAClockoutReason.Text, Convert.ToDateTime(hdnESigninDateTime.Value).ToShortDateString() + " " + txtAClockout.Text);
                    else
                        atd.ClockOut(UserId, Request.UserHostAddress, rowID, "web", 1, txtAClockoutReason.Text, Convert.ToDateTime(lblSigninDateTime.Text).ToShortDateString() + " " + txtAClockout.Text);
                }
                else
                {
                    DateTime currentTime = Convert.ToDateTime(atd.Timezone_datetime(UserId, DateTime.UtcNow.ToString(), "time"));

                    if (!Convert.ToBoolean(hdnIsAutoClockout.Value))
                        //No need of dates check on timezone update, incase of Normal clockout 
                        //rowID will always contain the ID of latest clockin regardless of less/more time
                        atd.ClockOut(UserId, Request.UserHostAddress, rowID, "web", 1, txtUOTReason.Text);
                    else
                        atd.ClockOut(UserId, Request.UserHostAddress, rowID, "web", 1, "Clock-out", null, true, hdnCOTime.Value);   //Automatic clockout

                }

                //Response.Write("....");

                //MyAttSys.avaimaTest0001DB db = new avaimaTest0001DB();
                //MyAttSys.WorkingHour workingHour = db.GetWorkingHourOfUser(Convert.ToInt32(hdnUserID.Value), InstanceID, DateTime.Now.DayOfWeek.ToString()).ToWorkingHour();
                //using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                //{
                //    using (SqlCommand cmd = new SqlCommand("insertuserouttime", con))
                //    {
                //        con.Open();
                //        if (SP.GetIPAddresses(_InstanceID).Rows.Count == 0)
                //        {
                //            cmd.CommandText = "[insertuserouttimeverified]";
                //        }
                //        cmd.CommandType = CommandType.StoredProcedure;
                //        cmd.Parameters.AddWithValue("@ppid", UserId);
                //        cmd.Parameters.AddWithValue("@rowid", "0");

                //        cmd.Parameters.AddWithValue("@lastout", Convert.ToDateTime(DateTime.Now));
                //        cmd.Parameters.AddWithValue("@outaddress", Request.UserHostAddress);
                //        cmd.Parameters.AddWithValue("@comment", txtUOTReason.Text);
                //        if (location.Visible == true)
                //        {
                //            cmd.Parameters.AddWithValue("@location", location.SelectedValue);
                //        }
                //        else
                //        {
                //            cmd.Parameters.AddWithValue("@location", DBNull.Value);
                //        }
                //        int rowid = Convert.ToInt32(cmd.ExecuteScalar());
                //        hrid.Value = rowid.ToString();
                //        txtcomment.Text = "";
                //        using (SqlCommand cmd1 = new SqlCommand("UPDATE attendence_management SET datefield=@date WHERE ID=@id", con))
                //        {
                //            if (hdnCOServerTime.Value != "")
                //            {
                //                if (Convert.ToDateTime(hdnCOServerTime.Value) <= DateTime.Now)
                //                {
                //                    cmd1.Parameters.AddWithValue("@date", Convert.ToDateTime(hdnCOServerTime.Value));
                //                }
                //                else
                //                {
                //                    cmd1.Parameters.AddWithValue("@date", Convert.ToDateTime(DateTime.Now));
                //                }
                //            }
                //            else
                //            {
                //                cmd1.Parameters.AddWithValue("@date", Convert.ToDateTime(DateTime.Now));
                //            }

                //            cmd1.Parameters.AddWithValue("@id", UserId);
                //            cmd1.ExecuteNonQuery();
                //        }


                SetUserStatus();
                hdnAutoCO.Value = "false";  //Day change Auto clockout entry
                hdnACO.Value = "false"; //Auto Clockout
                ScriptManager.RegisterStartupScript(this, GetType(), "", "ActivateAlertDiv('none', 'AlertDiv');", true);

                Response.Redirect(Request.Url.AbsoluteUri);

                //    }
                //}
            }
            catch (Exception ex)
            {
                lblErrTitle.Text = ex.Message;
                lblErrDetail.Text = ex.ToString();
                //ScriptManager.RegisterStartupScript(this, GetType(), "", "OpenDialogWB('#divErr');", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "", "console.log(" + ex.Message + ")", true);

            }
        }

        protected void BtncommentClick(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("insertcomments", con))
                {
                    cmd.Parameters.AddWithValue("@id", UserId);
                    cmd.Parameters.AddWithValue("@comment", txtcomment.Text);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        protected void rptb_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label title = e.Item.FindControl("btitle") as Label;
                Label from = e.Item.FindControl("bfrom") as Label;
                Label to = e.Item.FindControl("bto") as Label;
                HtmlControl r1 = e.Item.FindControl("r1") as HtmlControl;
                HtmlControl r2 = e.Item.FindControl("r2") as HtmlControl;
                HtmlControl r4 = e.Item.FindControl("r4") as HtmlControl;
                HiddenField bdate = e.Item.FindControl("bDate") as HiddenField;

                bdate.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Bdate"));
                title.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Btitle"));
                from.Text = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + DataBinder.Eval(e.Item.DataItem, "BfromTime").ToString()).ToShortTimeString();
                if (hfbfrom.Value == "0")
                    hfbfrom.Value = from.Text;

                to.Text = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + DataBinder.Eval(e.Item.DataItem, "BtoTime").ToString()).ToShortTimeString();
                hfbto.Value = to.Text;

                if (from.Text == to.Text)
                {
                    r1.Visible = false;
                    r2.Visible = false;
                    r4.Visible = true;
                }
            }
        }

        protected void rptb_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string[] ts = e.CommandArgument.ToString().Split("|".ToCharArray());
                if (e.CommandName == "editB")
                {
                    bID.Value = ts[0];
                    bModified.Value = ts[1];
                    bfrmTime.Text = (e.Item.FindControl("bfrom") as Label).Text;
                    btoTime.Text = (e.Item.FindControl("bto") as Label).Text;
                    beDate.Value = (e.Item.FindControl("bDate") as HiddenField).Value;
                    f = bfrmTime.Text;
                    t = btoTime.Text;
                    ScriptManager.RegisterStartupScript(this, GetType(), "", "editTime();", true);
                }

            }
        }

        protected void editBreak_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(bID.Value))
            {
                string tid = "";
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("getBreaks", con))
                    {
                        AvaimaTimeZoneAPI obj = new AvaimaTimeZoneAPI();
                        DataTable dt = new DataTable();
                        SqlDataAdapter da = null;
                        con.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (bModified.Value.ToLower() == "true")
                        {
                            cmd.CommandText = "getTodaysBreak";
                            cmd.Parameters.AddWithValue("@id", UserId);
                            cmd.Parameters.AddWithValue("@instanceId", _InstanceID);
                            cmd.Parameters.AddWithValue("@date", beDate.Value);
                            da = new SqlDataAdapter(cmd);
                            da.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                tid = dt.Rows[0]["BrealId"].ToString();
                            }
                            cmd.CommandText = "getBreaks";
                            cmd.Parameters.Clear();
                        }
                        else
                        {
                            tid = bID.Value;
                        }
                        cmd.Parameters.AddWithValue("@id", tid);
                        cmd.Parameters.AddWithValue("@instanceId", _InstanceID);
                        dt = new DataTable();
                        da = new SqlDataAdapter(cmd);
                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            TimeSpan frmDateTime = dt.Rows[0].Field<TimeSpan>("fromTime");
                            TimeSpan toDateTime = dt.Rows[0].Field<TimeSpan>("toTime");

                            TimeSpan tfdt = DateTime.Parse(bfrmTime.Text).TimeOfDay;
                            TimeSpan ttdt = DateTime.Parse(btoTime.Text).TimeOfDay;
                            if (tfdt == ttdt && (tfdt >= frmDateTime && tfdt <= toDateTime) && (ttdt <= toDateTime && ttdt >= frmDateTime))
                            {
                            }
                            else if (tfdt < frmDateTime)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('From time must be greater than or equal to current break start time')", true);
                                return;
                            }
                            else if (tfdt >= toDateTime)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('From time must be less than current break end time')", true);
                                return;
                            }
                            else if (ttdt > toDateTime)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('To time must be less than or equal to current break end time')", true);
                                return;
                            }
                            else if (ttdt <= frmDateTime)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('To time must be greater than current break start time')", true);
                                return;
                            }
                            else if (tfdt > ttdt)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('From time must be less than or equal to To time')", true);
                                return;
                            }
                            {
                                cmd.Parameters.Clear();
                                cmd.CommandText = "insertUserBreak";
                                if (bModified.Value.ToLower() == "false")
                                {
                                    cmd.Parameters.AddWithValue("@id", DBNull.Value);
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@id", bID.Value);
                                }
                                cmd.Parameters.AddWithValue("@date", beDate.Value);
                                cmd.Parameters.AddWithValue("@instanceId", _InstanceID);
                                cmd.Parameters.AddWithValue("@userId", UserId);
                                cmd.Parameters.AddWithValue("@fromTime", tfdt);
                                cmd.Parameters.AddWithValue("@toTime", ttdt);
                                cmd.Parameters.AddWithValue("@type", dt.Rows[0].Field<int>("type"));
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                    SetUserStatus();
                    FillandCreateHistory(UserId, con);
                }

            }
        }

        protected void lnkUndo_Click(object sender, EventArgs e)
        {
            if (ttype.Text == "in")
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("getWorkerRecordByRowId", con))
                    {
                        con.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@id", hrid.Value);
                        cmd.Parameters.AddWithValue("@ppid", UserId);
                        DataTable dt = new DataTable();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(dt.Rows[0]["lastin"].ToString()))
                            {
                                if (DateTime.Now < (Convert.ToDateTime(dt.Rows[0]["lastin"].ToString()).Add(new TimeSpan(0, 5, 0))))
                                {
                                    cmd.Parameters.Clear();
                                    cmd.CommandText = @"delete from workertrans where rowID = @id";
                                    cmd.Parameters.AddWithValue("@id", hrid.Value);
                                    cmd.CommandType = CommandType.Text;
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                }
            }

            if (ttype.Text == "out")
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("getWorkerRecordByRowId", con))
                    {
                        con.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@id", hrid.Value);
                        cmd.Parameters.AddWithValue("@ppid", UserId);
                        DataTable dt = new DataTable();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(dt.Rows[0]["lastout"].ToString()))
                            {
                                if (DateTime.Now < (Convert.ToDateTime(dt.Rows[0]["lastout"].ToString()).Add(new TimeSpan(0, 5, 0))))
                                {
                                    cmd.Parameters.Clear();
                                    cmd.CommandText = @"update workertrans set lastout = null, outstatus = null, out_location = null  where rowID = @id";
                                    cmd.Parameters.AddWithValue("@id", hrid.Value);
                                    cmd.CommandType = CommandType.Text;
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                }
            }
            UndoMsg.Visible = false;
        }

        protected void rpt_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["uid"]))
            {
                this.Redirect("MonthlyReportNew.aspx?id=" + Request.QueryString["uid"]);
            }
        }
        protected void yrpt_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["uid"]))
            {
                this.Redirect("YearlyReportNew.aspx?id=" + Request.QueryString["uid"]);
            }
        }

        protected void rptComments_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            // For eliminting empty comments row from repeater table
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlControl trow = e.Item.FindControl("trComment") as HtmlControl;
                String comment = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Comments"));
                if (String.IsNullOrEmpty(comment) || String.IsNullOrWhiteSpace(comment))
                {
                    trow.Style.Add("display", "none");
                }
            }
        }

        protected void rptHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
                RecordModel container = (RecordModel)e.Item.DataItem;
                Label lblDate = e.Item.FindControl("lblDate") as Label;
                if (lblDate.Text.Contains("Today"))
                {
                    return;
                }
                //Label loginTime = e.Item.FindControl("loginTime") as Label;
                //Label breakTime = e.Item.FindControl("breakTime") as Label;
                //Label logoutTime = e.Item.FindControl("logoutTime") as Label;
                //Label TotalHours = e.Item.FindControl("TotalHours") as Label;
                HtmlControl trDayOff = e.Item.FindControl("trDayOff") as HtmlControl;
                Label lblWorked = e.Item.FindControl("lblWorked") as Label;
                lblWorked.Text = container.TotalTime;
                Image imgMDetails = e.Item.FindControl("imgInfo") as Image;
                //Label dtitle = e.Item.FindControl("dtitle") as Label;
                HtmlControl tr = e.Item.FindControl("tr") as HtmlControl;
                HtmlControl rContainer = e.Item.FindControl("rContainer") as HtmlControl;
                //HtmlControl rFooter = e.Item.FindControl("rFooter") as HtmlControl;
                Repeater subRecordsClockIn = e.Item.FindControl("rptWorkerClockIn") as Repeater;
                Repeater subRecordsClockOut = e.Item.FindControl("rptWorkerClockOut") as Repeater;

                lblDate.Text = container.Date.ToString();
                //lblDate.Text = container.Date.ToString();
                subRecordsClockIn.DataSource = container.Records;
                subRecordsClockOut.DataSource = container.Records;
                subRecordsClockIn.DataBind();
                subRecordsClockOut.DataBind();
                String strDetails = "";
                //TotalHours.Text = container.TotalTime;
                if (container.Breaks.Count > 0 && container.Breaks.All(x => x.Id.HasValue == true))
                {
                    //breakTime.Text = "<div style=\"font-weight:bold;float:left\">Break(s):</div><div class=\"BPTime\">";
                    strDetails = "<table id=\"tblDetail\">";
                    foreach (BreakModel b in container.Breaks)
                    {
                        if (b.Id.HasValue)
                        {
                            //breakTime.Text += string.Format("<div>From: {0} To: {1}</div>", Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + b.BreakStartTime.ToString()).ToShortTimeString(), Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + b.BreakEndTime.ToString()).ToShortTimeString());
                            strDetails += string.Format("<tr><td><b>Break(s):</b></td><td>From: {0} To: {1}", Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + b.BreakStartTime.ToString()).ToShortTimeString(), Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + b.BreakEndTime.ToString()).ToShortTimeString()) + "</td></tr>";
                        }
                    }
                    //breakTime.Text += "</div>";
                }
                strDetails += "<tr><td><b>Total Time:</b><td>" + container.TotalTime + "</td></tr></table>";
                imgMDetails.ToolTip = strDetails;
                if (container.DayType.HasValue)
                {
                    if (container.Records.Count <= 0)
                    {
                        imgMDetails.Visible = false;
                        subRecordsClockIn.Visible = false;
                    }
                    if (string.IsNullOrEmpty(container.DayTitle))
                    {
                        //dtitle.Text = "Holiday";
                    }
                    else
                    {
                        //dtitle.Text = container.DayTitle;
                    }
                    //dtitle.Visible = true;
                    if (container.DayType.Value == 0)
                    {
                        trDayOff.Visible = true;
                        rContainer.Attributes.Add("class", "greenRow");
                    }
                    //rContainer.Attributes.Add("class", "SatSunHolidays");
                    else
                    {
                        //rContainer.Attributes.Add("class", "NormalHoliday");
                        rContainer.Attributes.Add("class", "yellowRow");
                        trDayOff.Visible = true;
                    }
                }
                else if (container.Records.Count <= 0)
                {
                    // Check on datetime
                    DateTime workerRequestDate = SP.GetWorkerFirstSignInn(UserId);

                    if (workerRequestDate.Date > container.Date)
                    {
                        imgMDetails.Visible = false;
                        //dtitle.Text = "Not Recruited";
                        subRecordsClockIn.Visible = false;
                        //dtitle.Visible = true;
                        rContainer.Attributes.Add("class", "NotRecruitDiv");
                        //rContainer.Style.Add("color", "#0094ff");
                    }
                    else if (workerRequestDate.Date == new DateTime(1900, 1, 1))
                    {
                        imgMDetails.Visible = false;
                        //dtitle.Text = "Request Sent";
                        subRecordsClockIn.Visible = false;
                        //dtitle.Visible = true;
                        rContainer.Attributes.Add("class", "RequestSendDiv");
                    }
                    else
                    {
                        imgMDetails.Visible = false;
                        //dtitle.Text = "Absent";
                        subRecordsClockIn.Visible = false;
                        //dtitle.Visible = true;                        
                        rContainer.Attributes.Add("class", "pinkRow");
                        trDayOff.Visible = true;
                    }
                }
                else
                {
                    imgMDetails.Visible = true;
                    subRecordsClockIn.Visible = true;
                }
            }
        }
        protected void rptWorkerClockIn_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
                SubRecordModel container = (SubRecordModel)e.Item.DataItem;
                Label lblClockIn = e.Item.FindControl("lblClockIn") as Label;
                if (!String.IsNullOrEmpty(container.LoginTime))
                {
                    string time = container.LoginTime;
                    lblClockIn.Text = time;
                }
            }
        }
        protected void rptWorkerClockOut_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
                SubRecordModel container = (SubRecordModel)e.Item.DataItem;
                Label lblClockIn = e.Item.FindControl("lblClockOut") as Label;
                if (!String.IsNullOrEmpty(container.LoginTime))
                {
                    string time = container.LoginTime;
                    lblClockIn.Text = time;
                }
            }
        }
        protected void btnsignout_Click(object sender, EventArgs e)
        {

        }
        protected void btnUpdateTime_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    int rows_affected = 0;

                    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                    {
                        if (!String.IsNullOrEmpty(txtEFrmTime.Text) && !String.IsNullOrEmpty(txtEToTime.Text))
                        {

                            if (Convert.ToDateTime(txtEFrmTime.Text).ToString() == Convert.ToDateTime(txtEToTime.Text).ToString())
                            {
                                using (SqlCommand cmd = new SqlCommand("DELETE workertrans WHERE rowID = @rowID", con))
                                {
                                    cmd.CommandType = CommandType.Text;
                                    cmd.Parameters.AddWithValue("@rowID", hdnerid.Value.ToString());
                                    con.Open();
                                    cmd.ExecuteNonQuery();
                                    SetUserStatus();
                                    FillandCreateHistory(UserId, con);
                                    return;
                                }
                            }
                        }

                        AvaimaTimeZoneAPI objtime = new AvaimaTimeZoneAPI();

                        using (SqlCommand cmd = new SqlCommand("updateworkertime", con))
                        {
                            DateTime? timein = null;
                            if (!string.IsNullOrEmpty(txtEFrmTime.Text))
                            {
                                timein = Convert.ToDateTime(hdnEDate.Value + " " + txtEFrmTime.Text);
                                //string time = objtime.GetTimeReverse(timein.Value.ToShortTimeString(), AppId);
                                //timein = Convert.ToDateTime(timein.Value.Date.ToShortDateString() + " " + time);
                            }
                            DateTime? timeout = null;
                            if (!string.IsNullOrEmpty(txtEToTime.Text))
                            {
                                timeout = Convert.ToDateTime(hdnEDate.Value + " " + txtEToTime.Text);
                                //string time = objtime.GetTimeReverse(timeout.Value.ToShortTimeString(), AppId);
                                //timeout = Convert.ToDateTime(timeout.Value.Date.ToShortDateString() + " " + time);
                            }
                            con.Open();
                            if (timein.HasValue)
                            {
                                cmd.Parameters.AddWithValue("@lastin", timein);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@lastin", DBNull.Value);
                            }

                            if (timeout.HasValue)
                            {
                                cmd.Parameters.AddWithValue("@lastout", timeout);
                                cmd.Parameters.AddWithValue("@outstatus", "Verified");
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@lastout", DBNull.Value);
                                cmd.Parameters.AddWithValue("@outstatus", DBNull.Value);
                            }
                            cmd.Parameters.AddWithValue("@userid", UserId);
                            cmd.Parameters.AddWithValue("@id", hdnerid.Value);
                            cmd.Parameters.AddWithValue("@comment", txteComments.Text);
                            cmd.Parameters.AddWithValue("@instatus", "Verified");

                            cmd.CommandType = CommandType.StoredProcedure;
                            rows_affected = cmd.ExecuteNonQuery();
                        }

                        Hashtable ht = new Hashtable();
                        ht["oldfrmTime"] = hdnOFrmTime.Value;
                        ht["oldtoTime"] = hdnOToTime.Value;
                        ht["comment"] = txteComments.Text;
                        ht["newfrmTime"] = hdnEDate.Value + " " + txtEFrmTime.Text;
                        ht["newtoTime"] = ((!string.IsNullOrWhiteSpace(hdnOToTime.Value)) && (!string.IsNullOrEmpty(hdnOToTime.Value))) ? hdnEDate.Value + " " + txtEToTime.Text : "";
                        //ht["day"] = txtEFrmTime.Text;
                        ht["day"] = hdnEDate.Value;
                        ht["rowid"] = hdnerid.Value;

                        if (rows_affected > 0)
                        {
                            //Notify Accessed User
                            atd.SendTimeEditNotification(UserId, ht);

                            SetUserStatus();
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "ActivateAlertDiv('none', 'AlertDiv');", true);
                            FillandCreateHistory(UserId, con);
                        }
                        else
                        {
                            Session["errorMsg"] = "Invalid Range -Time overlapping an existing entry on this day.";
                            //ScriptManager.RegisterStartupScript(this, GetType(), "", "openConfirmBox('Invalid Range -Time overlapping an existing entry on this day.');", true);
                        }

                        txteComments.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                //ScriptManager.RegisterStartupScript(this, GetType(), "", "openConfirmBox(" + ex.StackTrace + ");", true);

                AvaimaEmailAPI email = new AvaimaEmailAPI();
                StringBuilder body = new StringBuilder();
                body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                body.Append("<b>Error Message:</b> " + ex.Message.ToString() + "<br>");
                body.Append("<b>Detail:</b> " + ex.StackTrace.ToString() + "</p><br>");
                body.Append("<b>User:</b> " + UserId + "</p><br>");
                body.Append("<b>RowID:</b> " + hdnerid.Value + "</p>");

                body.Append(Helper.AvaimaEmailSignature);
                body.Append("</div>");
                //email.send_email("support@avaima.com", "AVAIMA", "", body.ToString(), "Auto IP Detection Exception");
                try
                {
                    email.send_email("sundus_csit@yahoo.com", "AVAIMA", "", body.ToString(), "Time-Edit Exception");
                }
                catch (Exception exception)
                { }
            }

            Response.Redirect(Request.Url.AbsoluteUri);

        }

        protected void btnUpdateTime_OLD_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                {
                    if (!String.IsNullOrEmpty(txtEFrmTime.Text) && !String.IsNullOrEmpty(txtEToTime.Text))
                    {

                        if (Convert.ToDateTime(txtEFrmTime.Text).ToString() == Convert.ToDateTime(txtEToTime.Text).ToString())
                        {
                            using (SqlCommand cmd = new SqlCommand("DELETE workertrans WHERE rowID = @rowID", con))
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.Parameters.AddWithValue("@rowID", hdnerid.Value.ToString());
                                con.Open();
                                cmd.ExecuteNonQuery();
                                SetUserStatus();
                                FillandCreateHistory(UserId, con);
                                return;
                            }
                        }
                    }

                    AvaimaTimeZoneAPI objtime = new AvaimaTimeZoneAPI();

                    using (SqlCommand cmd = new SqlCommand("updateworkertime", con))
                    {
                        DateTime? timein = null;
                        if (!string.IsNullOrEmpty(txtEFrmTime.Text))
                        {
                            timein = Convert.ToDateTime(txtEFrmTime.Text);
                            string time = objtime.GetTimeReverse(timein.Value.ToShortTimeString(), AppId);
                            timein = Convert.ToDateTime(timein.Value.Date.ToShortDateString() + " " + time);
                        }
                        DateTime? timeout = null;
                        if (!string.IsNullOrEmpty(txtEToTime.Text))
                        {
                            timeout = Convert.ToDateTime(txtEToTime.Text);
                            string time = objtime.GetTimeReverse(timeout.Value.ToShortTimeString(), AppId);
                            timeout = Convert.ToDateTime(timeout.Value.Date.ToShortDateString() + " " + time);
                        }
                        con.Open();
                        if (timein.HasValue)
                        {
                            cmd.Parameters.AddWithValue("@lastin", timein);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@lastin", DBNull.Value);
                        }

                        if (timeout.HasValue)
                        {
                            cmd.Parameters.AddWithValue("@lastout", timeout);
                            cmd.Parameters.AddWithValue("@outstatus", "Verified");
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@lastout", DBNull.Value);
                            cmd.Parameters.AddWithValue("@outstatus", DBNull.Value);
                        }
                        cmd.Parameters.AddWithValue("@id", hdnerid.Value);
                        cmd.Parameters.AddWithValue("@comment", txteComments.Text);
                        cmd.Parameters.AddWithValue("@instatus", "Verified");

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();

                    }

                    Hashtable ht = new Hashtable();
                    ht["oldfrmTime"] = hdnOFrmTime.Value;
                    ht["oldtoTime"] = hdnOToTime.Value;
                    ht["comment"] = txteComments.Text;
                    ht["newfrmTime"] = txtEFrmTime.Text;
                    ht["newtoTime"] = txtEToTime.Text;
                    ht["day"] = txtEFrmTime.Text;
                    ht["rowid"] = hdnerid.Value;

                    //Notify Accessed User
                    atd.SendTimeEditNotification(UserId, ht);

                    SetUserStatus();
                    ScriptManager.RegisterStartupScript(this, GetType(), "", "ActivateAlertDiv('none', 'AlertDiv');", true);

                    FillandCreateHistory(UserId, con);
                }
            }
        }

        protected void chkAutoPresent_CheckedChanged(object sender, EventArgs e)
        {

        }
        protected void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            if (SP.IsWorkerActive(Convert.ToInt32(UserId)))
            {
                // Inactivate

            }
            else
            {
                // Inactivate

            }
        }
        protected void ddlActive_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlActive.SelectedIndex == 0)
            //{
            //    //Active
            //    SP.SetUserStatus(Convert.ToInt32(UserId), true);
            //}
            //else if (ddlActive.SelectedIndex == 1)
            //{
            //    //Inactive
            //    SP.SetUserStatus(Convert.ToInt32(UserId), false);
            //}
        }
        protected void btnAddLeave_Click(object sender, EventArgs e)
        {
            AvaimaEmailAPI email = new AvaimaEmailAPI();
            DateTime startDate = Convert.ToDateTime(txtAbsDateFrom.Text).Date;
            DateTime endDate = Convert.ToDateTime(txtAbsDateTo.Text).Date;
            for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
            {
                AddLeave("0", hdnUserID.Value, date.ToString(), "True", txtAbsComments.Text, hdnUserID + "-" + startDate.ToString("yyyyMMdd") + "-" + endDate.ToString("yyyyMMdd"));
            }
            StringBuilder subject = new StringBuilder();
            StringBuilder body = new StringBuilder();
            string workerName = SP.GetWorkerName(hdnUserID.Value);
            if (Convert.ToDateTime(txtAbsDateFrom.Text).Date == Convert.ToDateTime(txtAbsDateTo.Text).Date)
            {
                subject.Append(workerName + " requested a leave for " + startDate.ToString("ddd, MMM dd yyyy") + ".");
            }
            else
            {
                subject.Append(workerName + " requested a leave from " + startDate.ToString("ddd, MMM dd yyyy") + " to " + endDate.ToString("ddd, MMM dd yyyy") + ".");
            }
            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
            body.Append("<b>" + workerName + "</b> requested a leave on " + DateTime.Now.ToString("dddd MMMM dd yyyy") + ".");
            body.Append(" Below is the description of his/her leave");
            body.Append("<ul style='list-style-type:circle'>");
            if (Convert.ToDateTime(txtAbsDateFrom.Text).Date == Convert.ToDateTime(txtAbsDateTo.Text).Date)
            {
                body.Append("<li>Leave Day: " + startDate.ToString("ddd, MMM dd yyyy") + "</li>");
            }
            else
            {
                body.Append("<li>Leave Days: " + startDate.ToString("ddd, MMM dd yyyy") + " - " + endDate.ToString("ddd, MMM dd yyyy") + "</li>");
            }
            body.Append("<li>Reason: " + txtAbsComments.Text + "</li>");
            body.Append("</ul>");
            body.Append("Your approval is required.");
            body.Append(Helper.AvaimaEmailSignature);
            body.Append("</div>");
            string strbody = body.ToString();

            List<int> adminIds = SP.GetAdminIDByUserID(Convert.ToInt32(UserId), _InstanceID);
            foreach (int adminID in adminIds)
            {
                email.send_email(SP.GetEmailByUserID(adminID), "Time & Attendance", "", body.ToString(), subject.ToString());
                email.send_email(WebConfigurationManager.AppSettings["DevEamil"], "Time & Attendance", "", body.ToString(), subject.ToString());
            }
            //email.send_email(hdnAdminEmail.Value, workerName, SP.GetEmailByUserID(Convert.ToInt32(hdnUserID.Value)), body.ToString(), subject.ToString());
            this.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
        }

        public void AddLeave(string AbsLogID, string UserID, string Date, string Active, string Comment, string groupID)
        {

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
            {
                if (Convert.ToInt32(AbsLogID) > 0)
                {
                    // Update
                    using (SqlCommand command = new SqlCommand("UPDATE [Absences] SET" +
                   "[UserID] = @UserID,[CrtDate] = @CrtDate,[ModDate] = @ModDate,[Active] = @Active,[Comment] = @Comment WHERE AbsLogID = @AbsLogID", connection))
                    {
                        command.Parameters.AddWithValue("@AbsLogID", AbsLogID);
                        command.Parameters.AddWithValue("@UserID", UserID);
                        command.Parameters.AddWithValue("@CrtDate", Convert.ToDateTime(Date));
                        command.Parameters.AddWithValue("@ModDate", Convert.ToDateTime(Date));
                        command.Parameters.AddWithValue("@Active", Active);
                        command.Parameters.AddWithValue("@Comment", Comment);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
                else
                {
                    // Save
                    using (SqlCommand command = new SqlCommand("INSERT INTO [Absences] " +
                   "([UserID],[CrtDate],[ModDate],[Active],[Comment],[groupID]) VALUES(@UserID,@CrtDate,@ModDate,@Active,@Comment,@groupID)", connection))
                    {
                        //string date = Convert.ToDateTime(Date).ToString("dd/MM/yyyy");
                        command.Parameters.AddWithValue("@UserID", UserID);
                        command.Parameters.AddWithValue("@CrtDate", Date);
                        command.Parameters.AddWithValue("@ModDate", Date);
                        command.Parameters.AddWithValue("@Active", Active);
                        command.Parameters.AddWithValue("@Comment", Comment);
                        command.Parameters.AddWithValue("@groupID", groupID);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    SetUserStatus();
                }
            }
        }



        protected void lnkEDALReport_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["uid"]))
            {
                this.Redirect("EDALReport.aspx?id=" + Request.QueryString["uid"]);
            }

        }

        protected void btnClearExDays_Click(object sender, EventArgs e)
        {

        }

        protected void btnEmailToDev_Click(object sender, EventArgs e)
        {
            AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
            attendence_management user = attendence_management.SingleOrDefault(u => u.ID == Convert.ToInt32(hdnUserID.Value));
            if (user != null)
            {
                string messagebody = "<b>Instance: </b>" + user.InstanceId;
                messagebody += "<br /><br /><b>User Name: </b>" + user.Title;
                messagebody += "<br /><br /><b>User Email: </b>" + user.email;
                messagebody += "<br /><br /><b>Error: </b>" + lblErrTitle.Text;
                messagebody += "<br /><br /><b>Error Detail: </b>" + lblErrDetail.Text;
                messagebody += Helper.AvaimaEmailSignature;
                emailAPI.send_email(WebConfigurationManager.AppSettings["DevEamil"], "Time & Attendance", "", messagebody, "Error - " + DateTime.Now.ToString());
            }
        }

        protected void lnkGetYearStats_Click(object sender, EventArgs e)
        {
            GetYearStats(hdnUserID.Value.ToInt32(), DateTime.Now.Year, InstanceID);
            tr1.Visible = true;
        }

        private string getdayval(int day)
        {
            string val = "1";
            switch (day)
            {
                case 1:
                    val = "2";
                    break;
                case 2:
                    val = "3";
                    break;
                case 3:
                    val = "4";
                    break;
                case 4:
                    val = "5";
                    break;
                case 5:
                    val = "6";
                    break;
                case 6:
                    val = "7";
                    break;
                default:
                    val = "1";
                    break;
            }
            return val;
        }
        protected void btnflexsave_Click(object sender, EventArgs e)
        {
            try
            {
                attendancetype objattendancetype = attendancetype.SingleOrDefault(u => u.userid == hdnUserID.Value && u.instanceid == this.InstanceID);
                if (objattendancetype != null)
                {
                    objattendancetype.userid = hdnUserID.Value;
                    objattendancetype.instanceid = this.InstanceID;
                    objattendancetype.userattendType = 2;
                    objattendancetype.Update();
                }
                else
                {
                    objattendancetype = new attendancetype()
                    {
                        userid = hdnUserID.Value,
                        instanceid = this.InstanceID,
                        userattendType = 2,
                    };
                    objattendancetype.Add();
                }

                flexiblesetting objflexsetting = flexiblesetting.SingleOrDefault(u => u.instanceid == this.InstanceID && u.userid == hdnUserID.Value);
                if (objflexsetting != null)
                {
                    objflexsetting.workinghours = txthours.Text.ToInt32();
                    objflexsetting.BreakInclude = Convert.ToBoolean(ddlflexbreak.SelectedValue);
                    objflexsetting.Update();
                }
                else
                {
                    objflexsetting = new flexiblesetting()
                    {
                        instanceid = this.InstanceID,
                        workinghours = txthours.Text.ToInt32(),
                        userid = hdnUserID.Value,
                        BreakInclude = Convert.ToBoolean(ddlflexbreak.SelectedValue),
                    };
                    objflexsetting.Add();
                }
                this.Redirect("Flexibleworker.aspx?id=" + hdnUserID.Value);
            }
            catch (Exception ex)
            {
                lblErrTitle.Text = ex.Message;
                lblErrDetail.Text = ex.ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "", "OpenDialogWB('#divErr');", true);
            }
        }

        protected void timerForRepeater_Tick(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString);
            FillandCreateHistory(UserId, con);

            ScriptManager.RegisterStartupScript(this, GetType(), "", "jq191('.statusDetails').tooltipster({interactive: true });", true);
            //    

        }

        [WebMethod]
        public static string LoadMoreStats(int userid, string appid, string instanceid)
        {
            Add_worker obj = new Add_worker();
            Attendance atd = new Attendance();
            DataTable dt = new DataTable();
            int parentid = atd.getParent(userid.ToString()).ToInt32();

            DateTime startDate = SP.GetWorkerFirstSignInThisYear(userid.ToString(), DateTime.Now.Year);
            DateTime endDate = SP.GetWorkerLastSignInThisYear(userid.ToString(), DateTime.Now.Year);

            DataTable dtAttendance = atd.GetYearlyAttendance(userid, startDate, endDate);
            DataTable dtAbsences = atd.GetYearlyAbsences(userid, startDate, endDate);

            TimeSpan totalWorkedHours = atd.GetTotalWorkedTime(dtAttendance, obj.getParent(userid.ToString()).ToInt32(), appid, instanceid, DateTime.Now.Year);
            string totalWorkedY = atd.GetTotalHours(totalWorkedHours) + " Hrs " + ((totalWorkedHours.Minutes != 0) ? totalWorkedHours.Minutes + " Mins" : "");
            //string totalWorkedY = Math.Round(totalWorkedHours.TotalHours) + " Hours " + ((totalWorkedHours.Minutes != 0) ? totalWorkedHours.Minutes + " Mins" : "");

            //string yearlyHours = obj.GetWorkedHoursInYear(userid);
            string yearlyHours = totalWorkedY;
            string unmarkeddays = "0 Day(s)";

            if (yearlyHours != "0 Hrs")
            {
                //using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                //{
                //    using (SqlCommand cmd = new SqlCommand("select TOP 1 * from workertrans where p_pid=@userid and year(lastin)=YEAR(getdate())", con))
                //    {
                //        con.Open();
                //        cmd.Parameters.AddWithValue("@userid", userid);
                //        SqlDataAdapter adp = new SqlDataAdapter(cmd);

                //        adp.Fill(dt);
                //    }
                //}
                //DateTime startDate = Convert.ToDateTime(dt.Rows[0]["lastin"]);
                //DateTime endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);


                DataTable dtHistory = new DataTable();
                dtHistory.Columns.Add("DateTime", typeof(DateTime));
                dtHistory.Columns.Add("Day");
                dtHistory.Columns.Add("Clock-in");
                dtHistory.Columns.Add("Clock-out");
                dtHistory.Columns.Add("Time Spent");
                dtHistory.Merge(atd.FormatDataTable(dtAttendance, "attendance", appid));
                dtHistory.Merge(atd.FormatDataTable(dtAbsences, "absence", appid));

                string unmarked_dates = atd.GetUnmarkedDays(dtHistory, startDate, endDate, parentid, DateTime.Now.Year);
                if (unmarked_dates != "")
                    unmarkeddays = unmarked_dates.Split(',').Count().ToString() + " Day(s)";
            }
            return yearlyHours + "," + unmarkeddays;
        }

        private void LoadEmployees()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_GetUsers_New", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@instanceid", InstanceID);
                    cmd.Parameters.AddWithValue("@userid", hdnParentid.Value);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);

                    dt.DefaultView.Sort = "ID ASC";
                    dt = dt.DefaultView.ToTable();

                    ddlemployees.DataValueField = "ID";
                    ddlemployees.DataTextField = "Title";
                    ddlemployees.DataSource = dt;
                    ddlemployees.DataBind();

                    for (int i = 0; i < ddlemployees.Items.Count; i++)
                    {
                        ListItem item = ddlemployees.Items[i];
                        if (ddlemployees.Items[i].Value == hdnParentid.Value)
                            item.Text = SP.GetWorkerName(hdnParentid.Value);
                    }

                    if (dt.Rows.Count == 1)
                    {
                        div_emp.Visible = false;
                        lblUserName.Text = SP.GetWorkerName(UserId);
                        lblUserName.Visible = true;
                    }
                    else
                    {
                        ddlemployees.SelectedValue = UserId;
                        div_emp.Visible = true;
                        lblUserName.Visible = false;
                    }
                }
            }

            //  ScriptManager.RegisterStartupScript(this, GetType(), "load", " $('[id$=ddlemployees]').trigger('change');", true);

        }
        protected void ddlemployees_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("Add_worker.aspx?id=" + Request.QueryString["id"].ToString() + "&uid=" + ddlemployees.SelectedValue + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString());
        }

        [WebMethod]
        public static string CurrentTime(string userid, string timezone, string dlsaving, string dlsavinghour)
        {
            Attendance atd = new Attendance();
            timezone = timezone + "#" + dlsaving + "#" + dlsavinghour;
            return atd.Timezone_datetime(userid, DateTime.UtcNow.ToString(), "time", timezone);
        }

        [WebMethod]
        public static string CheckClockInOut_Timespan(string userid, string timezone, string dlsaving, string dlsavinghour, string clockin_datetime)
        {
            Attendance atd = new Attendance();
            DateTime datetime;
            timezone = timezone + "#" + dlsaving + "#" + dlsavinghour;
            datetime = Convert.ToDateTime(atd.Timezone_datetime(userid, DateTime.UtcNow.ToString(), "date", timezone) + ' ' + atd.Timezone_datetime(userid, DateTime.UtcNow.ToString(), "time", timezone));

            if ((datetime - Convert.ToDateTime(clockin_datetime)).TotalHours > 10)
            {
                return "yes";
            }
            else
                return "";
        }

        [WebMethod]
        public static void UpdateIndustry(string userid, string industry)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["aviamaConn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Update cms_user SET industry = @industry where userid = (Select ownerid from schema_6e815b00_6e13_4839_a57d_800a92809f21.tbluserrole where userid = @userid)", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@userid", userid);
                    cmd.Parameters.AddWithValue("@industry", industry);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        [WebMethod]
        public static void UpdateUserInfo(string userid, string employeeCount)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Update schema_6e815b00_6e13_4839_a57d_800a92809f21.attendence_management SET employeeCount = @employeeCount where ID =  @userid", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@userid", userid);
                    cmd.Parameters.AddWithValue("@employeeCount", employeeCount);
                    cmd.ExecuteNonQuery();
                }
            }
        }
        protected void btnAddTime_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    int rows_added = 0;

                    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                    {
                        AvaimaTimeZoneAPI objtime = new AvaimaTimeZoneAPI();

                        using (SqlCommand cmd = new SqlCommand("addworkertime", con))
                        {
                            DateTime? timein = null;
                            if (!string.IsNullOrEmpty(txtAFrmTime.Text))
                            {
                                timein = Convert.ToDateTime(hdnAFrmTime.Value + " " + txtAFrmTime.Text);
                            }
                            DateTime? timeout = null;
                            if (!string.IsNullOrEmpty(txtAToTime.Text))
                            {
                                timeout = Convert.ToDateTime(hdnAToTime.Value + " " + txtAToTime.Text);
                            }

                            con.Open();
                            cmd.Parameters.AddWithValue("@lastin", timein);
                            cmd.Parameters.AddWithValue("@instatus", "Verified");
                            cmd.Parameters.AddWithValue("@lastout", timeout);
                            cmd.Parameters.AddWithValue("@outstatus", "Verified");
                            cmd.Parameters.AddWithValue("@userid", UserId);
                            cmd.Parameters.AddWithValue("@comment", txtaComments.Text);
                            cmd.Parameters.AddWithValue("@signinaddress", Request.UserHostAddress);
                            cmd.Parameters.AddWithValue("@signoutaddress", Request.UserHostAddress);

                            cmd.CommandType = CommandType.StoredProcedure;
                            rows_added = cmd.ExecuteNonQuery();

                            txtaComments.Text = "";
                        }

                        if (rows_added > 0)
                        {
                            SetUserStatus();
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "ActivateAlertDiv('none', 'AlertDiv');", true);
                            FillandCreateHistory(UserId, con);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "openConfirmBox('There is a conflict in your entry. Please check & enter appropriate time that does not collide with other entries.');", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "openConfirmBox('Sorry for the inconvinience. We are unable to process your request.');", true);
            }
        }

        protected void btnDelTime_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    int rows_updated = 0;

                    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                    {
                        AvaimaTimeZoneAPI objtime = new AvaimaTimeZoneAPI();

                        using (SqlCommand cmd = new SqlCommand("delworkertime", con))
                        {
                            con.Open();
                            cmd.Parameters.AddWithValue("@rowID", hdndrid.Value);
                            cmd.Parameters.AddWithValue("@comment", txtdComments.Text);
                            cmd.CommandType = CommandType.StoredProcedure;
                            rows_updated = cmd.ExecuteNonQuery();
                        }
                        Hashtable ht = new Hashtable();
                        ht["comment"] = txtdComments.Text;
                        ht["frmTime"] = hdnDFrmTime.Value;
                        ht["toTime"] = hdnDToTime.Value;
                        ht["day"] = hdnDFrmTime.Value;

                        atd.SendDeleteNotification(UserId, ht);
                        SetUserStatus();
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "ActivateAlertDiv('none', 'AlertDiv');", true);
                        FillandCreateHistory(UserId, con);

                        txtdComments.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "openConfirmBox('Sorry for the inconvinience. We are unable to process your request.');", true);
            }
        }

        public void FillTagTable()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("GetTags", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@instanceid", InstanceID);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        reptTags.DataSource = dt;
                        reptTags.DataBind();
                        reptTags.Visible = true;
                        lblTags.Visible = false;
                        hdnTagCount.Value = dt.Rows.Count.ToString();
                    }
                    else
                    {
                        reptTags.Visible = false;
                        lblTags.Visible = true;
                        hdnTagCount.Value = "0";
                    }
                }
            }
        }


        protected void btnaddtags_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    foreach (RepeaterItem item in reptTags.Items)
                    {
                        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                        {
                            if (((CheckBox)item.FindControl("chktag")).Checked)
                            {
                                int TagID = Convert.ToInt32(((HiddenField)item.FindControl("hftagID")).Value);
                                AddTagAssige(TagID);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "openConfirmBox('" + ex.Message + "');", true);
            }
        }

        private void AddTagAssige(int TagID)
        {
            int rows_added = 0;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("AddTagAssign", con))
                {

                    con.Open();
                    cmd.Parameters.AddWithValue("@WorkTransID", hdnerid.Value);
                    cmd.Parameters.AddWithValue("@TagID", TagID);
                    cmd.Parameters.AddWithValue("@UserID", UserId);
                    cmd.CommandType = CommandType.StoredProcedure;
                    rows_added = cmd.ExecuteNonQuery();
                }
            }
        }

        public DataTable GetAssignTags(int WorkTranID)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("GetTagAssign", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@WorkTransID", WorkTranID);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    return dt;
                }
            }
        }
    }
}