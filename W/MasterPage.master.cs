﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    string AppId = "";
    Attendance atd;
    protected void Page_Load(object sender, EventArgs e)
    {
        SetParameters();

        //Check Daylight Savings
        string AutoDetectedtimezone = atd.getStandardTime(Request.UserHostAddress);
        
        //Disable auto timezone check for office morning and night shift instance
        if (!string.IsNullOrEmpty(AutoDetectedtimezone) && (hdnInstanceId.Value != "ae5eb924-def7-461e-8097-c503ec893bfc" && hdnInstanceId.Value != "f425f912-8d79-4415-ba72-89a5309acb10"))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "dst", "checkUserStandartTime('" + AutoDetectedtimezone + "'); ", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "dst", "console.log(NO OFFSET FOUND);", true);
        //End

        //+ "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString()
        //navbar.HRef = "Default.aspx?id=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
        if (CheckEmployees(true) == 0)
        {
            dashboard.Attributes.Add("data-link", "about.aspx");
            dashboard.HRef = "About.aspx?id=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
            //dashboard.HRef = "Add_worker.aspx?id=" + Request.QueryString["id"].ToString() + "&uid=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
        }
        else
        {
            dashboard.Attributes.Add("data-link", "default.aspx");
            dashboard.HRef = "Default.aspx?id=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
        }

        myattendance.HRef = "Add_worker.aspx?id=" + Request.QueryString["id"].ToString() + "&uid=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
        settings.HRef = settingsPagelnk.HRef = "settings.aspx?id=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
        users.HRef = "Users.aspx?id=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();

        if (HttpContext.Current.Request.Url.Host != "localhost")
            users.Visible = false;

        statistics.HRef = "Statistics.aspx?id=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
        formerusers.HRef = "FormerUsers.aspx?id=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
        //notifications.HRef = "Users.aspx?id=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString() + "&req=yes";

        lnkAdd_worker.HRef = "AddEmployee.aspx?id=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
     
        //Avaima Admin Email Notification Panel
        avaimaAdmin_dashboard.HRef = "AdminDashboard.aspx?id=" + Request.QueryString["id"].ToString() + "&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();

        int count = CheckEmployees(false);
        if (count > 0)
        {
            formerusers_count.InnerText = count.ToString();
            li_formerusers.Visible = true;
        }
        else
            li_formerusers.Visible = false;

        //For office instance - and explicit users
        isSuperAdmin();

        //If is made ADMIN
        if (atd.isAdmin(Request.QueryString["id"].ToInt32()))
        {
            admin_dashboard.HRef = "Default.aspx?id=" + Request.QueryString["id"].ToString() + "&admin=1&instanceid=" + Request.QueryString["instanceid"].ToString() + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
            li_admin_dashboard.Visible = true;

            //for office instance - as only few menu items are visible to an employee
            if (hdnInstanceId.Value == "f425f912-8d79-4415-ba72-89a5309acb10" || hdnInstanceId.Value == "ae5eb924-def7-461e-8097-c503ec893bfc")
            {
                li_addemployee.Visible = true;
            }
        }

        //Test Case
        //if(hdnInstanceId.Value == "f425f912-8d79-4415-ba72-89a5309acb10" && (hdnUserID.Value == "11025" || hdnUserID.Value == "10999"))
        //    li_accessrights.Visible = true;
    }

    public void SetParameters()
    {
        Session.Remove("superadmin");
        atd = new Attendance();
        hdnInstanceId.Value = Request.QueryString["instanceid"].ToString();
        hdnUserID.Value = hdnAdminID.Value = Request.QueryString["id"].ToString();
        string email = Request.QueryString["e"].Decrypt();
        string password = Request.QueryString["p"].Decrypt();

        string isVerified = atd.VerifyLogin(email, password);
        if (isVerified != "TRUE")
            Response.Redirect("Info.aspx");

        DataTable userSettings = JsonConvert.DeserializeObject<DataTable>(atd.GetSettingInfo(email, password));
        hdnAppId.Value = userSettings.Rows[0]["userid"].ToString();
        hdntimezone.Value = userSettings.Rows[0]["timezone"].ToString();
        hdndlsaving.Value = userSettings.Rows[0]["dlsaving"].ToString();
        hdndlsavinghour.Value = userSettings.Rows[0]["dlsavinghour"].ToString();
        hdnEmpCount.Value = CheckEmployees(true).ToString();

        //Check if user is clocked-in
        hdnLastInID.Value = "0";

        DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.GetStatus(hdnAdminID.Value));
        if (dt.Rows.Count > 0)
        {
            if (string.IsNullOrEmpty(dt.Rows[0]["lastout"].ToString()))
            {
                hdnIsClockedIn.Value = "true";
                hdnLastInID.Value = dt.Rows[0]["rowID"].ToString();
            }
            else
                hdnIsClockedIn.Value = "false";
        }
        else
            hdnIsClockedIn.Value = "false";
    }

    public int CheckEmployees(bool active)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("Select count(*) FROM schema_6e815b00_6e13_4839_a57d_800a92809f21.attendence_management  where parentid = @userid and active = @active and instanceid = @instanceid", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", hdnUserID.Value);
                cmd.Parameters.AddWithValue("@instanceid", hdnInstanceId.Value);
                cmd.Parameters.AddWithValue("@active", active);
                return ((int)cmd.ExecuteScalar());
            }
        }
    }

    public void isSuperAdmin()
    {
        int result;

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("Select count(*) FROM schema_6e815b00_6e13_4839_a57d_800a92809f21.tbluserrole  where userid = @userid and role = 'superuser'", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", hdnUserID.Value);
                result = ((int)cmd.ExecuteScalar());
            }
        }

        if (result == 0)
        {
            if (hdnInstanceId.Value == "ae5eb924-def7-461e-8097-c503ec893bfc" || hdnInstanceId.Value == "f425f912-8d79-4415-ba72-89a5309acb10" || hdnInstanceId.Value == "7d7a412d-6da6-4364-b662-aab3dd1b9cfb")
            {
                dashboard.Visible = false;
                li_settings.Visible = false;
                li_addemployee.Visible = false;
                li_formerusers.Visible = false;
                li_accessrights.Visible = false;
                //   li_statistics.Visible = false;
            }

            //8f1f61d4-5365-405a-bad6-a5933e18da6f - custom code on reuqest of "rita.marfo@toldoit.com" on 3rd April 2019
            if (hdnInstanceId.Value == "8f1f61d4-5365-405a-bad6-a5933e18da6f" && hdnUserID.Value != "13555")
            {
                li_addemployee.Visible = false;
                li_formerusers.Visible = false;
                li_accessrights.Visible = false;
            }

            Session["superadmin"] = "no";
        }
        else
        {
            Session["superadmin"] = "yes";
        }

        if (hdnUserID.Value == "9844")
        {
            if (HttpContext.Current.Request.Url.Host != "localhost")
            //li_admin.Visible = true;
            {
                li_admin.Visible = true;
                li_avaimaAdmin_dashboard.Visible = false;
            }
            else
            {
                li_avaimaAdmin_dashboard.Visible = true;
                li_admin.Visible = false;
            }
        }

    }

    protected void btnsubmitsupport_Click(object sender, EventArgs e)
    {
        try
        {
            AvaimaEmailAPI email = new AvaimaEmailAPI();
            StringBuilder subject = new StringBuilder();
            StringBuilder body = new StringBuilder();

            subject.Append("Need Free consultancy");
            body.Append("Following User Need Free consultancy, User info are as follow:<br /><br />Name: " + SP.GetWorkerName(hdnUserID.Value));
            body.Append("<br /><br /> Email: " + Request.QueryString["e"].Decrypt());
            body.Append("<br /><br />IP Address: <a href='http://ip-api.com/" + Request.UserHostAddress + "'>" + Request.UserHostAddress + "</a>");
            body.Append("<br /><br /> Message: " + txtmsg.Text);
            string url = "http://www.avaima.com/6e815b00-6e13-4839-a57d-800a92809f21/W/Add_worker.aspx?id=" + hdnUserID.Value + "&uid=" + hdnUserID.Value + "&instanceid=" + hdnInstanceId.Value + "&e=" + Request.QueryString["e"].ToString() + "&p=" + Request.QueryString["p"].ToString();
            body.Append("<br /><br />Profile: <a href='" + url + "'> Click Here! </a>");
            email.send_email("support@avaib.com", "Time & Attendance", "", body.ToString(), subject.ToString());
            //email.send_email("sundus_csit@yahoo.com", "Time & Attendance", "", body.ToString(), subject.ToString()); 

            ScriptManager.RegisterStartupScript(this, GetType(), "", "$('#txtmsg').val('');openConfirmBox('Email Sent Successfully!!!');", true);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "", "$('#txtmsg').val('');openConfirmBox('We are unable to process your request. Sorry for the inconvenience.');", true);

        }
    }
}
