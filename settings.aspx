﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="settings.aspx.cs" Inherits="settings" MasterPageFile="~/MasterPage.master" %>

<asp:Content runat="server" ContentPlaceHolderID="head" >

    <style type="text/css">
        .auto-style1 {
            width: 159px;
        }

        .auto-style2 {
            width: 137px;
        }

        .Clear {
            clear: both;
        }

    </style>

    
   

    <script>
        jQuery(document).ready(function ($) {
            $("input[id$='hdate']").datepicker();

        });
    </script>

    <asp:ScriptManager runat="server" ID="scriptmanager1"></asp:ScriptManager>
    <asp:HiddenField runat="server" ID="Option" ClientIDMode="Static"/>
    <div class="space">
        <div class="Clear"></div>
        <div class="Tab AttendenceSettingDiv3">
            <div>

                <div>
                    <table style="width: 314px">
                        <tr class="smallGrid">
                            <td class="auto-style3">Title </td>
                            <td class="auto-style1">
                                <asp:TextBox ID="txtTitle" runat="server" MaxLength="150" ValidationGroup="setting" ClientIDMode="Static"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="smallGrid" id="">
                            <td class="auto-style3">Timezone </td>
                            <td class="auto-style1">
                                <asp:DropDownList runat="server" ID="ddlTimzone" ValidationGroup="setting"  CssClass="ddl" ClientIDMode="Static" style="width: 400px;">
                                    <asp:ListItem Text="Sunday" Value="1"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="smallGrid" id="">
                            <td class="auto-style3">Weekend </td>
                            <td class="auto-style1">
                               <asp:ListBox runat="server" ID="ddlWeekend" SelectionMode="Multiple" ClientIDMode="Static"></asp:ListBox>
                            </td>
                        </tr>
                        <tr class="smallGrid">
                            <td class="auto-style3"></td>
                            <td class="auto-style1">
                                <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="setting" Width="50px" CssClass="ui-state-default" OnClick="btnSave_Click"  ClientIDMode="Static"/>
                            </td>
                        </tr>
                    </table>

                </div>

                <%--                <h2>Holidays Information</h2>--%>
                <h2></h2>

                <table style="width: 314px">
                    <tr class="smallGrid">
                        <td class="auto-style3">Title </td>
                        <td class="auto-style1">
                            <asp:TextBox ID="htitle" runat="server" MaxLength="150" ValidationGroup="hd" ClientIDMode="Static"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="smallGrid" id="hdateContainer">
                        <td class="auto-style3">Date </td>
                        <td class="auto-style1">
                            <asp:TextBox ID="hdate" runat="server" ValidationGroup="hd" ClientIDMode="Static"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="smallGrid" style="display: none" id="hyc">
                        <td class="auto-style3"></td>
                        <td class="auto-style1">
                            <asp:CheckBox runat="server" ID="cye" Text="Every Year" ClientIDMode="Static" />
                        </td>
                    </tr>
                    <tr class="smallGrid">
                        <td class="auto-style3"></td>
                        <td class="auto-style1">
                            <asp:Button ID="hAdd" runat="server" Text="Add" ValidationGroup="hd" Width="50px" CssClass="ui-state-default" OnClick="hAdd_Click" ClientIDMode="Static" />
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <asp:UpdatePanel runat="server" ID="holidays_panel">
                    <ContentTemplate>
                        <asp:Repeater runat="server" ID="rpthd" OnItemDataBound="rpthd_ItemDataBound" OnItemCommand="rpthd_ItemCommand">
                            <HeaderTemplate>
                                <table class="smallGrid">
                                    <tr class="smallGridHead">
                                        <td>Title
                                        </td>
                                        <td>Date/Day
                                        </td>
                                        <td>Every Year
                                        </td>
                                        <td></td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="smallGrid">
                                    <td>
                                        <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("id") %>' />
                                        <asp:Label runat="server" ID="hdtitle" Text='<%# Eval("title") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="hdDate" Text='<%# Eval("date") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="ey" Text='<%# Eval("Yearly") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:LinkButton runat="server" ID="del" CommandName="del" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete?')"></asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:Label runat="server" ForeColor="red" Text="No Record Found." Visible="False" ID="hdmsg"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="hAdd" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>

        <div class="Clear"></div>
    </div>

    <div title="Not allowed" id="divNotAllowed" class="hide">
        <p>
            You are not allowed to view Time & Attendence Settings.
        </p>
    </div>
</asp:Content>
