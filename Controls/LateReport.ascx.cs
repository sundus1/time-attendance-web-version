﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_LateReport : System.Web.UI.UserControl
{
    private bool IsSimpleClock = true;
    List<RecordModel> records = new List<RecordModel>();
    private int UserId;
    public String InstanceID { get; set; }
    DateTime signInDate;
    public DateTime WorkerLastSignIn { get; set; }
    public DateTime WorkerFirstSignIn { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        AddinstanceWS objinst = new AddinstanceWS();

        String testInstanceID = this.InstanceID;
        // Intance Dependent Code - UnCommented
        OwnerId = objinst.GetUserID(HttpContext.Current.User.Identity.Name);
        //OwnerId = "83c43a21-4397-4404-afd1-0175302f198c";
        //OwnerId = "e8a92994-352a-4319-838d-882e8dc1bdeb";
        //OwnerId = "300";

        if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out UserId))
        {
            WorkerLastSignIn = SP.GetWorkerLastSignIn(UserId.ToString()).Date.Date;
            WorkerFirstSignIn = SP.GetWorkerFirstSignInn(UserId.ToString()).Date.Date;
            //hdnerid.Value = UserId.ToString();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("select * from attendence_management a where a.Id = @id and a.InstanceId = @insId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", UserId);
                    cmd.Parameters.AddWithValue("@insId", this.InstanceID);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];
                        lblUserName.Text = dr["Title"].ToString();
                    }
                    //else
                    //{
                    //    Response.Redirect("Default.aspx?instanceid=" + this.InstanceID);
                    //}
                }

            }
        }
        //else
        //{
        //    Response.Redirect("Default.aspx?instanceid=" + this.InstanceID);
        //}
        //workeruserprofile.InstanceID = testInstanceID;
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("getuserrole", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", OwnerId.Trim());
                cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {

                    //hdnAdminName.Value = SP.GetWorkerName(dt.Rows[0][1].ToString());
                    //hdnAdminEmail.Value = SP.GetEmailByUserID(Convert.ToInt32(dt.Rows[0][1].ToString()));
                    AdminFirstSignIn = SP.GetWorkerFirstSignInn(dt.Rows[0][1].ToString()).Date;

                    if (Convert.ToString(dt.Rows[0]["role"]) == "worker" || Convert.ToString(dt.Rows[0]["role"]) == "admin")
                    {
                        if (Convert.ToString(dt.Rows[0]["role"]) == "admin")
                        {
                            IsAdmin = true;
                            appAssignedId = Convert.ToInt32(dt.Rows[0]["userid"]);
                            hfparentid.Value = SP.GetAdminCatID(Convert.ToInt32(dt.Rows[0]["userid"].ToString())).ToString();
                        }
                        else
                        {
                            Response.Redirect("Add_worker.aspx?id=" + dt.Rows[0]["userid"]);
                        }
                    }
                    else
                    {
                        //IsAdmin = true;
                        appAssignedId = Convert.ToInt32(dt.Rows[0]["userid"]);
                    }
                }
            }
        }
        if (!IsPostBack)
        {
            GenerateDate();
            //FillAttendance();
        }
    }
    private void FillAttendance()
    {
        RptrAttendanceFill(0, IsAdmin);
        hfparentid.Value = "0";
    }
    private void GenerateDate()
    {

        for (int i = DateTime.Now.Year; i >= AdminFirstSignIn.Year; i--)
        {
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        for (int i = 1; i <= 12; i++)
        {
            ddlMonth.Items.Add(new ListItem(new DateTime(DateTime.Now.Year, i, i).ToString("MMMM"), i.ToString()));
        }

        ddlMonth.SelectedIndex = 1;
        ddlYear.SelectedIndex = 1;
    }
    public string OwnerId { get; set; }
    protected void btnGR_Click(object sender, EventArgs e)
    {

        if (Convert.ToInt32(ddlMonth.SelectedValue) > 0)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                CreateLateTable(con, rdoCameAfter.Checked);
                //FillandCreateHistory(con);
            }
            //hdnData.Value = "true";
        }
        else
        {
            //hdnData.Value = "false";
        }
    }

    private void CreateLateTable(SqlConnection con, Boolean whocameafter)
    {
        FillAttendance();
        AvaimaTimeZoneAPI atzObj = new AvaimaTimeZoneAPI();
        Int32 count = 0;
        if (Convert.ToInt32(ddlYear.SelectedValue) == 0 || Convert.ToInt32(ddlMonth.SelectedValue) == 0)
        {
            return;
        }
        foreach (User user in Users)
        {
            count = 0;
            using (SqlCommand cmd = new SqlCommand("[GetLateCount]", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                cmd.Parameters.Clear();
                string lastin = "";
                if (whocameafter)
                {
                    lastin = atzObj.GetTimeReverse(txtLateTimein.Text, this.Context.User.Identity.Name);
                }
                else
                {
                    lastin = atzObj.GetTimeReverse(txtLateTimeout.Text, this.Context.User.Identity.Name);
                }

                //string lastout = atzObj.GetTimeReverse(txtLateTimeout.Text, this.Context.User.Identity.Name);
                cmd.Parameters.AddWithValue("@lastin", lastin);
                cmd.Parameters.AddWithValue("@year", ddlYear.SelectedValue.ToString());
                cmd.Parameters.AddWithValue("@month", ddlMonth.SelectedValue.ToString());
                cmd.Parameters.AddWithValue("@userId", user.userID.ToString());
                cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                if (whocameafter)
                {
                    cmd.Parameters.AddWithValue("@come", true.ToString());
                }
                else
                {
                    cmd.Parameters.AddWithValue("@come", false.ToString());
                }

                adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    latePresences.Single(u => u.UserID == user.userID).Count = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
                }
            }
        }
        rptLate.DataSource = latePresences.OrderByDescending(u => u.Count);
        rptLate.DataBind();
    }

    protected void bk_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out UserId))
        {
            Response.Redirect("Reports.aspx?id=" + UserId + "&instanceid=" + this.InstanceID);
        }
    }

    public bool isAdmin { get; set; }

    public bool isWorker { get; set; }

    public DateTime AdminFirstSignIn { get; set; }


    protected void RptrAttendanceFill(int pid, bool isAdmin = false, bool ShowFiltered = false, int status = 1)
    {
        DataTable dtstudentall = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            string q = "getallattendencerecordbyid";
            if (ShowFiltered)
            {
                if (status == 1)
                    q = "getallactiveusers";
                else
                {
                    q = "getallinactiveusers";
                }
            }
            else
            {
                if (isAdmin)
                {
                    q = "getfilteredattendancerecordbyid";
                }
            }

            using (SqlCommand cmd = new SqlCommand(q, con))
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", pid);
                cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                if (isAdmin)
                {
                    if (q == "getfilteredattendancerecordbyid")
                    {
                        cmd.Parameters.AddWithValue("@userid", UserId.ToString());
                    }
                }
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dtstudentall);
                cmd.CommandText = "getclocktype";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@instanceId", this.InstanceID);
                bool i = (bool)cmd.ExecuteScalar();
                if (i)
                {
                    IsSimpleClock = false;
                }

                PagedDataSource pds = new PagedDataSource();
                pds.AllowPaging = true;
                pds.DataSource = dtstudentall.DefaultView;
                Users = new List<User>();
                latePresences = new List<LatePresence>();
                if (dtstudentall.Rows.Count > 0)
                {                    // Fetch user name
                    foreach (DataRow row in dtstudentall.Rows)
                    {
                        if (Convert.ToBoolean(row["category"].ToString()) == false)
                        {
                            Users.Add(new User() { userID = Convert.ToInt32(row[0].ToString()), userName = row[1].ToString() });
                            latePresences.Add(new LatePresence() { UserID = Convert.ToInt32(row[0].ToString()), UserName = row[1].ToString(), Count = 0 });
                        }

                    }
                }
            }
        }
    }

    //public List<Int32> UserIDs { get; set; }
    public bool IsAdmin { get; set; }

    public int appAssignedId { get; set; }
    public List<User> Users { get; set; }
    public class User
    {
        public int userID { get; set; }
        public string userName { get; set; }
    }

    public List<LatePresence> latePresences { get; set; }
}