﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LateReport.ascx.cs" Inherits="Controls_LateReport" %>
<style type="text/css">
    .auto-style1 {
        height: 23px;
    }
</style>
<script>
    $(document).ready(function () {
        $('.datetime').timepicker({
            timeFormat: 'hh:mm tt',
            controlType: 'select'
        });
    });
</script>
<div id="secprofile" style="margin-top: 5px">
    <h1 class="username">
        <asp:Label ID="lblUserName" runat="server" Text="Late Report" Style="margin-bottom: 5px;"></asp:Label>
        &nbsp;<asp:LinkButton runat="server" ID="bk" Text="Back to reports" Style="font-size: 12px;" OnClick="bk_Click"></asp:LinkButton>
    </h1>
    
    <div style="clear: both">
        <div style="text-align: left">
            <div id="filtersContainer" runat="server" visible="false">
                <span id="tmC" style="display: none">Start Time:
                    <input type="text" id="time" name="time" style="width: 85px" /><input type="button" value="Show" id="btntC" style="margin-top: 2px" />
                </span>
            </div>
        </div>
    </div>
    <table class="profile" style="margin-top: 12px">
        <tr>
            <td colspan="2">
                
            </td>
        </tr>
        <tr>
            <td colspan="2">Please select month and year
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList runat="server" ID="ddlYear" Height="21px" Width="148px">
                    <asp:ListItem Text="Select Year" Value="0" Enabled="true"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlMonth" Height="21px" Width="148px">
                    <asp:ListItem Text="Select Month" Value="0" Enabled="true"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">                
                Who came after:
                <asp:TextBox ID="txtLateTimein" runat="server" Text="09:15 am" CssClass="datetime" Width="111px">
                </asp:TextBox>
                <asp:RadioButton ID="rdoCameAfter" runat="server" Text="" GroupName="laterdo" Checked="true" />
            </td>            
        </tr>
        <tr>
            <td style="text-align: right;">                
                Who left after:
                <asp:TextBox ID="txtLateTimeout" runat="server" Text="06:00 pm" CssClass="datetime" Width="111px">
                </asp:TextBox>
                <asp:RadioButton ID="rdoLeftAfter" runat="server" Text=""  GroupName="laterdo"/>                
            </td>
            <td>
                <asp:Button ID="btnGR" runat="server" Text="Preview" Width="148px" Style="margin-top: -2px" OnClick="btnGR_Click" class="ui-state-default" />
            </td>
        </tr>
        <tr class="trdata">
            <td></td>
            <td>
                <asp:Repeater ID="rptLate" runat="server">
                    <HeaderTemplate>
                        <table class="smallGrid">
                            <thead>
                                <tr class="smallGridHead">
                                    <td>Name
                                    </td>
                                    <td>Late
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class='<%# (Convert.ToInt32(DataBinder.Eval(Container.DataItem, "Count")) > 0)? "pinkRow": "" %>'>
                            <td>
                                <%--PostBackUrl='<%# "~/MonthlyReportN.aspx?id=" + DataBinder.Eval(Container.DataItem, "UserID").ToString() + "&instanceid=" + this.InstanceID %>'--%>
                                <asp:Label ID="lblUserName" CssClass="lblUserName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UserName") %>'></asp:Label>
                            </td>
                            <td style="text-align: right;">
                                <asp:Label ID="lblLateCount" CssClass="lblLateCount" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Count") %>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfparentid" runat="server" />
</div>
