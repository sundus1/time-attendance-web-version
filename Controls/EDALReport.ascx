﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EDALReport.ascx.cs" Inherits="Controls_EDALReport" %>

<%--Context Menu--%>


<%--Context Menu End--%>
<style>
    .hide {
        display: none !important;
    }
</style>
<script>
    $(document).ready(function () {
        if ($('#hdnData').val() != "true") {
            $('.trdata').hide();
        }
        else {
            $('.trdata').show();
        }
    });
</script>
<script>
    $(document).ready(function () {

        $('.lnkMarkNPW').click(function () {
            var dataid = $(this).attr('data-id');
            var datatype = $(this).attr('data-ex-type');
            var datauserid = $(this).attr('data-userid');
            var datadate = $(this).attr('data-date');
            var dataactive = $(this).attr('data-active');
            var $obj = $(this);

            $('#Dialog').attr('title', $(this).attr('tooltip'));
            $('#Dialog').html('Are you sure you want to ' + $(this).attr('tooltip') + "?");
            $('#Dialog').dialog({
                modal: true,
                buttons: {
                    "Yes": function () {
                        AddUpdateExDays(dataid, datatype, datauserid, datadate, dataactive, 'Partial Working', true);
                        $(this).dialog('close');
                        if (dataactive == "False") {
                            $obj.closest('tr').removeClass('eggshellRow');
                        }
                        else {
                            $obj.closest('tr').addClass('eggshellRow');
                        }

                    },
                    "No": function () {
                        $(this).dialog('close');
                    },
                    "Cancel": function () {
                        $(this).dialog('close');
                    }
                }
            });
            return false;
        });
        $('.tooltip').tooltipster({
            interactive: true
        });
        $('.datetime').timepicker({
            timeFormat: 'hh:mm tt',
            controlType: 'select'
        });
        $('#btnUpdateTime').click(function () {
            if ($('.txtEFrmTime').val() == 'Select' || $('.txtEToTime').val() == 'Select') {
                alert('Please select valid date time range');
                return false;
            }
            $('#divEditTime').dialog().hide(200);
        });
        $('#btnDeleteTime').click(function () {
            var frmTime = $('.txtfrmTime').val();
            var toTime = $('.txtToTime').val();
            if (frmTime == 'Select' || toTime == 'Select') {
                alert('Please select a time range');
                return false;
            }
            getTimeDifference(frmTime, toTime, '#lblHoursToDel');
            var dlg = $('#divConfirmDeleteTime').dialog({
                model: true
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            //if (iframe != null)
            //    iframe.height = "600px";
            return false;
        });
        $('#btnNo, #btnCancel').click(function () {
            //$('#divConfirmDeleteTime').hide(200);
            $('#divConfirmDeleteTime').dialog("close");
            return false;
        });
        $('.lnkMarkAbsence').click(function () {
            var label;
            $('.lnkMarkAbsence').each(function () {
                label = $(this).text();
            });
            showDialog("Are you sure?", "<p>Are you sure you want to " + label + "?</p>", $(this));
            return false;
        });
        function showDialog(title, message, obj) {
            $('#divClockedIn').attr("title", title);
            $('#divClockedIn').html(message);
            $('#divClockedIn').dialog({
                model: true,
                buttons: {
                    "Yes": function () {
                        showAbsCommentBox(obj);
                    },
                    "No": function () {
                        $(this).dialog("close");
                    },
                    "Cancel": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
        function showAbsCommentBox(obj) {
            $('#absComment').dialog({
                width: 375,
                modal: true,
                buttons: {
                    "Save": function () {
                        addAbsence(obj, $('#txtAbsComment').val(), false);
                        $(this).dialog('close');
                        $('#divClockedIn').dialog("close");
                    },
                    "Cancel": function () {
                        $(this).dialog('close');
                    }
                }
            });
        }
        $('.imgDeleteHours').click(function () {
            //alert('delete hours');
            var $obj = $(this);
            var frmTime = $obj.attr('data-frmTime');
            var toTime = $obj.attr('data-toTime');
            var recordID = $obj.attr('data-recordID');
            var breakTime = "Break Time: " + frmTime + " - " + toTime;
            var dataday = $obj.attr('data-day');
            $('#lbldataday').text(dataday);

            $('.txtfrmTime').val('Select');
            $('.txtToTime').val('Select');
            $('#rID').val(recordID);

            var dlg = $("#revHr").dialog({
                height: 270,
                width: 380,
                modal: true,
                hide: {
                    effect: "blind",
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            return false;
        });
        $('.imgEditHours').click(function () {
            var $obj = $(this);

            var frmTime = $obj.attr('data-frmTime');
            var toTime = $obj.attr('data-toTime');
            var frmDate = $obj.attr('data-frmdate');
            var toDate = $obj.attr('data-todate');
            var recordID = $obj.attr('data-recordID');
            var breakTime = "Clock in time: " + frmTime + " - " + toTime;

            $('.txtEFrmTime').val(frmDate);
            $('.txtEToTime').val(toDate);
            $('#hdnerid').val(recordID);

            var dlg = $("#divEditTime").dialog({
                width: 393,
                modal: true,
                hide: {
                    effect: "blind",
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            //if (iframe != null)
            //    iframe.height = "600px";
            return false;
        });
    });

    $(document).ready(function () {
        $("#lnkcomment").click(function () {
            $('#lblerror').text("");

            var dlg = $("#addcomment").dialog({
                height: 160,
                width: 450,
                modal: true,
                hide: {
                    effect: "blind",
                }
            });
            var textbox = document.getElementById('txtcomment');
            textbox.focus();
            textbox.value = textbox.value;
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            //if (iframe != null)
            //    iframe.height = "600px";
            return false;
        });
        $("#link").click(function () {
            openpopup();
            return false;
        });
        $(".DatePick").datepicker({
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true
        });
        $('.bedit').click(function () {
            var frmTime = $(this).attr('data-bfrom');
            var toTime = $(this).attr('data-bto');
            setTimePicker(frmTime, toTime, $(this));
        });
        $('.imgDeleteHours').click(function () {
            var frmTime = $(this).attr('data-frmTime');
            var toTime = $(this).attr('data-toTime');
            setTimePicker(frmTime, toTime);
        });
        function setTimePicker(from, to) {
            var f = $("#hftimeFormat").val();
            var fromTime = parseInt(getTimeFromAMPM(from).split(':')[0]);
            var toTime = parseInt(getTimeFromAMPM(to).split(':')[0]);
            $(".TimePick").timepicker({
                hourMin: fromTime,
                hourMax: toTime,
                //controlType: 'select',
                //'minTime': from,
                //'maxTime': to,
                timeFormat: 'hh:mm tt',
                controlType: 'select'
                //'showDuration': true
            });
        }
        $(".TimePick").timepicker({
            //'timeFormat': f, 'step': 1, 'forceRoundTime': true,            
            //hourMin: fromTime,
            //hourMax: toTime,
            //controlType: 'select',
            //'minTime': from,
            //'maxTime': to,
            timeFormat: 'hh:mm tt',
            controlType: 'select'
            //'showDuration': true
        });
        function setTimePicker(from, to, $obj) {
            //var f = $("#hftimeFormat").val();
            var fromTime = parseInt(getTimeFromAMPM(from).split(':')[0]);
            var toTime = parseInt(getTimeFromAMPM(to).split(':')[0]);
            $obj.timepicker({
                //'timeFormat': f, 'step': 1, 'forceRoundTime': true,

                hourMin: fromTime,
                hourMax: toTime,
                //controlType: 'select',
                //'minTime': from,
                //'maxTime': to,
                timeFormat: 'hh:mm tt',
                controlType: 'select'
                //'showDuration': true
            });
        }
        $(".DateTimePick").datetimepicker({
            timeFormat: "hh:mm tt",
            controlType: 'select'
        });
    });

    $(document).on('click', 'img.bedit', function () {
        try {
            var $edit = $(this);
            var $parenttr = $edit.parent();
            var dataflag = $edit.attr("data-flag");
            var databid = $edit.attr("data-bid");
            var bfrom = $edit.attr("data-bfrom");
            var bto = $edit.attr("data-bto");
            var bdate = $edit.attr("data-bdate");
            $('.bfrmTime').val(bfrom);
            $('.btoTime').val(bto);
            $('#beDate').val(bdate);
            $('#bID').val(databid);
            $('#bModified').val(dataflag);
            $('.lblBreakTime').text('Break Time: ' + bfrom + " - " + bto);
            var dlg = $("#edtHr").dialog({
                height: 226,
                width: 291,
                top: 60,
                show: {
                },
                modal: true,
                hide: {
                    effect: "blind",
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");

        } catch (e) {
            alert(e.message);
        }
        return false;
    });

    $(document).ready(function () {
        if ($('#hdnData').val() != "true") {
            $('.trdata').hide();
        }
        else {
            $('.trdata').show();
        }
    });

    function delTime() {
        var dlg = $("#revHr").dialog({
            height: 140,
            width: 200,
            modal: true,
            hide: {
                effect: "blind",
            }
        });
        dlg.parent().appendTo($("form:first"));
        dlg.parent().css("z-index", "1000");
        var iframe = window.parent.document.getElementById("MainContent_ifapp");
        //if (iframe != null)
        //    iframe.height = "600px";
        return false;
    }
</script>
<section id="secprofile" style="margin-top: 5px">
    <h1 class="username">
        <asp:Label ID="lblUserName" runat="server" Text="Derp" Style="margin-bottom: 5px;"></asp:Label>
        &nbsp;<asp:LinkButton runat="server" ID="bk" Text="Back to reports" Style="font-size: 12px;" OnClick="bk_Click"></asp:LinkButton>
    </h1>
    <div style="clear: both">
    </div>
    <div style="clear: both">
        <div style="text-align: left">
            <div id="filtersContainer" runat="server" visible="false">
                <span id="tmC" style="display: none">Start Time:
                    <input type="text" id="time" name="time" style="width: 85px" /><input type="button" value="Show" id="btntC" style="margin-top: 2px" />
                </span>
            </div>
        </div>
    </div>

    <table class="profile" style="margin-top: 12px">
        <tr id="trSelUser" runat="server">
            <td colspan="2">Select Employee: 
                <asp:DropDownList runat="server" ID="ddlUsers">
                    <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">Please select month and year
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList runat="server" ID="ddlYear" Height="21px" Width="148px">
                    <asp:ListItem Text="Select Year" Value="0" Enabled="true"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="btnGR" runat="server" Text="Generate Report" Width="190px" Style="margin-top: -2px" OnClick="btnGR_Click" class="ui-state-default" />
            </td>
        </tr>
        <tr class="trdata">
            <td colspan="2" style="font-weight: bold; padding: 7px 0px">From
                <asp:Label ID="lblStartDate" runat="server"></asp:Label>&nbsp
                to &nbsp<asp:Label ID="lblEndDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="trdata" id="trtblextrawork" runat="server">
            <td class="caption">History:
            </td>
            <td>
                <asp:Table ID="tblWeekendWork" runat="server" EnableViewState="false">
                </asp:Table>
            </td>
        </tr>
        <tr class="trdata" id="trabsence" runat="server">
            <td class="caption">Total absences:
            </td>
            <td>
                <asp:Label ID="lblTotalAbsences" runat="server" Text="0"></asp:Label>
            </td>
        </tr>
        <tr class="trdata" id="tr1" runat="server">
            <td class="caption">Overtime: 
            </td>
            <td>
                <asp:Label ID="lblWeekendWorkDays" runat="server" Text="0"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="caption" style="text-align: right">
                <asp:Label ID="lblOvertimeLabel" CssClass="lblOvertimeLabel" runat="server" Text="Undertime: "></asp:Label>
            </td>
            <%--"--%>
            <td style="">
                <asp:Label ID="lblExTime" CssClass="divworker defaulte" runat="server" data-text="As the time is recorded" data-label="" Style="float: left"></asp:Label>
                <div style="background-color: black; width: 20px">
                </div>
                <%--<img alt="Info" src="images/controls/question.png" class="tooltip" title="Hours Info" runat="server" id="imgBreakHourInfo" style="padding-left: 4px" />--%>
            </td>
        </tr>
        <tr>
            <td style="text-align: right; padding: 3px 9px">
                <asp:Label ID="lblResultLabel" runat="server" Font-Bold="true"></asp:Label>
            </td>
            <td style="border-top: 1px solid black; padding-top: 3px">
                <b>
                    <asp:Label ID="lblResultDays" CssClass="divworker defaulte" runat="server"></asp:Label>
                    <%--<asp:Label ID="Label3" CssClass="divworker defaulte" runat="server"></asp:Label>--%>

                    <%--<asp:Label ID="lblResultEarlyMinus" CssClass="divworker subtracte" runat="server" Style="display: none"></asp:Label>

                    <asp:Label ID="lblResultAfterMinus" CssClass="divworker subtracta" runat="server" Style="display: none"></asp:Label>

                    <asp:Label ID="lblResultEarlyNAfter" CssClass="divworker subtractea" runat="server" Style="display: none"></asp:Label>--%>
                </b>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr style="display: none">
            <td></td>
            <td style="display: none">
                <table style="display: none">
                    <tr class="trdata" id="trextrawork" runat="server">
                    </tr>
                    <tr class="trdata" id="trExtraTimeWorked" runat="server">
                    </tr>
                    <tr class="trdata" id="trTotExtraTime" runat="server">
                        <td class="caption" style="text-align: left">
                            <asp:Label ID="lblTotalTimeT" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblTotalTime" runat="server" Style="float: right"></asp:Label>
                            <%--<img alt="Info" src="images/controls/question.png" class="tooltip" title="Hours Info" runat="server" id="imgBreakHourInfo" style="padding-left: 4px" />--%>
                        </td>
                        <td>
                            <asp:Label ID="lblTotalTimeDays" runat="server" Style="float: right"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <%--<tr class="trdata" id="trtblabsence" runat="server" visible="false">
            <td class="caption">Absences:
            </td>
            <td>
                <asp:Table ID="tblAbsences" runat="server" EnableViewState="false" >
                </asp:Table>
            </td>
        </tr>--%>
        <tr class="trdata hide">
            <td class="caption" style="text-align: left">Total days worked: 
            </td>
            <td>
                <asp:Label ID="lblTotDays" runat="server" Text="0"></asp:Label>
            </td>
        </tr>
        <%--<tr class="trdata">
            <td class="caption">Hours this year: 
            </td>
            <td>
                <asp:Label ID="lblTotalHours" runat="server" Text=""></asp:Label>
            </td>
        </tr>--%>
        <tr class="trdata hide">
            <td class="caption">Working hrs should be: 
            </td>
            <td>
                <asp:Label ID="lblEstWHrs" runat="server" Text="0"></asp:Label>
            </td>
        </tr>
        <tr class="trdata hide">
            <td class="caption">Worked this year:
            </td>
            <td>
                <asp:Label ID="lblWHours" runat="server" Style="float: left"></asp:Label>
                <%--<img alt="Info" src="images/controls/question.png" class="tooltip" title="Hours Info" runat="server" id="imgWorkedHourInfo" style="padding-left: 4px" />--%>
            </td>
        </tr>
        <tr class="trdata hide">
            <td class="caption">Breaks this year:
            </td>
            <td>
                <asp:Label ID="lblTotBreakHrs" runat="server" Style="float: left"></asp:Label>
                <%--<img alt="Info" src="images/controls/question.png" class="tooltip" title="Hours Info" runat="server" id="imgBreakHourInfo" style="padding-left: 4px" />--%>
            </td>
        </tr>

        <%--<tr class="trdata" style="display:none">
            <td class="caption">Extra hours worked:
            </td>
            <td>
                <asp:Label ID="lblExtraHours" runat="server" Style="float: left" text="0 days"></asp:Label>                
            </td>
        </tr>--%>
    </table>
    <div>
        <asp:Label ID="lblTest" runat="server" Visible="false"></asp:Label>
    </div>
    <asp:HiddenField ID="hdnData" ClientIDMode="Static" Value="false" runat="server" />
    <asp:HiddenField ID="hfparentid" runat="server" />
    <%--<asp:Table ID="tblHistory" runat="server">
    </asp:Table>--%>
</section>
<div id="revHr" title="Delete Time" style="display: none">
    <asp:HiddenField ClientIDMode="Static" runat="server" ID="rID" />
    <p>
        Use this option to delete time from your recorded history in order to fix any excess time you recorded by mistake.
    </p>
    <p style="padding: 12px 32px; font-weight: bold">
        <label id="lbldataday"></label>
    </p>
    <div class="divDelHours">
        <asp:Label runat="server" ID="Label1" CssClass="Label1" Text="From:" Width="57px"></asp:Label>
        <asp:TextBox ID="txtfrmTime" runat="server" CssClass="TimePick txtfrmTime" ValidationGroup="dt" Width="65px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtfrmTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="dt">Required</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtfrmTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="(0?[1-9]|(10|11|12))(:[0-5][0-9])( )*(AM|PM|am|pm)" ValidationGroup="dt">Invalid Time</asp:RegularExpressionValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtfrmTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="([01]?[0-9]|2[0-3]):[0-5][0-9]" Visible="False" ValidationGroup="dt">Invalid Time</asp:RegularExpressionValidator>
    </div>
    <div class="divDelHours">
        <asp:Label runat="server" ID="Label2" Text="To:" Width="57px"></asp:Label>
        <asp:TextBox ID="txtToTime" runat="server" CssClass="TimePick txtToTime" ValidationGroup="dt" Width="65px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtToTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="dt">Required</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtToTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="(0?[1-9]|(10|11|12))(:[0-5][0-9])( )*(AM|PM|am|pm)" ValidationGroup="dt">Invalid Time</asp:RegularExpressionValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtToTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="([01]?[0-9]|2[0-3]):[0-5][0-9]" Visible="False" ValidationGroup="dt">Invalid Time</asp:RegularExpressionValidator>
    </div>
    <div class="divDialogButton">
        <button value="Delete Time" id="Button1" class="btnDeleteTime ui-state-default">Delete Time</button>
    </div>
</div>
<div id="divEditTime" title="Edit Time" style="display: none">
    <asp:HiddenField ClientIDMode="Static" runat="server" ID="hdnerid" />
    <table class="tblDlg" id="tblDlg">
        <tr>
            <th style="text-align: left" colspan="2">Use this option to edit time from your recorded history.
            </th>
        </tr>
        <tr>
            <td>&nbsp;
                            <asp:Label ID="lblRowID" CssClass="lblRowID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <th>From:
            </th>
            <td>
                <asp:TextBox ID="txtEFrmTime" runat="server" CssClass="DateTimePick txtEFrmTime" ValidationGroup="dt" Width="160px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEFrmTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="dt">Required</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <th>To:
            </th>
            <td>
                <asp:TextBox ID="txtEToTime" runat="server" CssClass="DateTimePick txtEToTime" ValidationGroup="dt" Width="160px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtEToTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="dt">Required</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <th>Clock in:
            </th>
            <td>
                <asp:DropDownList ID="ddlCIV" runat="server">
                    <asp:ListItem Text="Verified" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Not verified" Value="0"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <th>Clock out:
            </th>
            <td>
                <asp:DropDownList ID="ddlCOV" runat="server">
                    <asp:ListItem Text="Verified" Value="Verified" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Not Verified" Value="Not Verified"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <th>Comments:
            </th>
            <td>
                <asp:TextBox ID="txteComments" runat="server" TextMode="MultiLine">
                </asp:TextBox>
            </td>
        </tr>
    </table>
    <div class="divDialogButton">
        <asp:Button ID="btnUpdateTime" runat="server" Text="Update Time" CssClass="ui-state-default btnUpdateTime raisesevent" OnClick="btnUpdateTime_Click" />
    </div>
</div>
<div id="divConfirmDeleteTime" class="hide" title="Are you sure?">
    <p style="margin-bottom: 5px">
        <b>Warning:</b> Once the hours are deleted you will not be able to restore them.
    </p>
    <p style="margin-bottom: 5px">
        You are deleting <b>
            <label id="lblHoursToDel" class="lblHoursToDel">#</label></b> from your work log. 
    </p>
    <p style="margin-bottom: 5px">
        Are you sure you want to do delete this time?
    </p>
    <div class="divDialogButton">
        <asp:Button runat="server" ID="deleHrs" Text="Yes" OnClick="deleHrs_Click" CssClass="raisesevent ui-state-default" ValidationGroup="dt" />
        <button id="btnNo" class="btnclosedlg ui-state-default" value="No">No</button>
        <button id="btnCancel" class="btnclosedlg ui-state-default" value="Cancel">Cancel</button>
    </div>
</div>
<div id="addcomment" title="Comment" style="display: none">
    <asp:HiddenField ClientIDMode="Static" runat="server" ID="hfcommentstatus" />
    <asp:TextBox runat="server" ID="txtcomment" TextMode="MultiLine" Height="50px"></asp:TextBox>
    <br />
    <br />

    <asp:Button runat="server" ID="btnin" CssClass="raisesevent ui-state-default" Text="Post Comment" placeholder="Please provide your comments..." OnClick="BtncommentClick" Style="float: right" />
</div>
<div id="addcomentout" title="Signout Comment" style="display: none">
    <asp:TextBox runat="server" ID="txtcommentout" TextMode="MultiLine" Height="50px"></asp:TextBox>
    <br />
    <br />
    <asp:Button ID="btnout" runat="server" Text="Post Comment" Style="float: right" />
</div>
<div id="absComment" class="hide" title="Comment">
    <table>
        <tr>
            <td>
                <textarea id="txtAbsComment" class="txtAbsComment" style="width: 333px; height: 57px;" placeholder="Please add your comments here..."></textarea>
            </td>
        </tr>
    </table>
</div>
<div id="Dialog" title="">
</div>
<div class="divDialogButton hide">
    <button value="Delete Time" id="btnDeleteTime" class="btnDeleteTime ui-state-default">Delete Time</button>
</div>
<div id="divClockedIn" title="Clocked In Successfully!" style="display: none">
    <p>
        You have successfully clocked in. 
    </p>
    <br />
    <p>
        Please verify your location by clicking the link in your verification email sent to 
                <asp:Label runat="server" ID="lblInEmail"></asp:Label>
    </p>
</div>
<div id="divErrUOT" title="Invalid Attempt" class="hide">
    <br />
    <p>
        <b>Error:</b>
        <asp:Label ID="lblUOTMessage" runat="server"></asp:Label>
    </p>
    <%-- <p>
                <b>Stack Overflow:</b> <asp:Label ID="lblErrStack" runat="server"></asp:Label>
            </p>--%>
</div>
<script>
    function OpenDialog(id) {
        $(id).dialog({
            width: 400,
            modal: true,
            hide: {
                effect: "blind",
            },
            buttons: {
                'Ok': function () {
                    $(this).dialog('close');
                }
            }
        });
    }

    $(function () {

        $.contextMenu({
            selector: '#divWorker',
            trigger: 'left',
            callback: function (key, options) {
                $('.divworker').hide(200);
                $('.' + key).show(200);
                $('.lblResultMsg').text($('.' + key).attr('data-text'));
                $('.lblOvertimeLabel').text($('.' + key).attr('data-label'));
                //var m = "clicked: " + key;
                //window.console && console.log(m) || alert(m);
            },
            items: {
                "defaulte": { name: "As the time is recorded" },
                "subtracte": { name: "Subtract time before clockins" },
                "subtracta": { name: "Subtract time after clockouts" },
                "subtractea": { name: "Subtract time before clockins and after clockouts" }
            }
        });

        $('#divWorker').on('click', function (e) {
            console.log('clicked', this);
        })
    });
</script>
<style>
    ul.context-menu-list {
        width: 296px !important;
        max-width: 304px !important;
    }

    li.context-menu-item {
        font-size: 12px !important;
        padding: 4px 2px 2px 11px !important;
        font-style: italic !important;
    }
</style>
