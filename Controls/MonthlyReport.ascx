﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonthlyReport.ascx.cs" Inherits="Controls_MonthlyReport" %>
<script>
    $(document).ready(function () {
        // Refresh Worked hour
        setInterval(function () {
            $('span[data-incdate]').each(function (index) {
                //alert($(this).text());
                var $target = '#' + $(this).attr('id');
                var signintime = $($target).attr('data-incdate');
                getWorkedHours(signintime, $target);
            });
        }, 1000);

        $('.lnkMarkNPW').click(function () {
            var dataid = $(this).attr('data-id');
            var datatype = $(this).attr('data-ex-type');
            var datauserid = $(this).attr('data-userid');
            var datadate = $(this).attr('data-date');
            var dataactive = $(this).attr('data-active');
            var $obj = $(this);

            $('#Dialog').attr('title', $(this).attr('tooltip'));
            $('#Dialog').html('Are you sure you want to ' + $(this).attr('tooltip') + "?");
            $('#Dialog').dialog({
                modal: true,
                buttons: {
                    "Yes": function () {
                        AddUpdateExDays(dataid, datatype, datauserid, datadate, dataactive, 'Partial Working', true);
                        $(this).dialog('close');
                        if (dataactive == "False") {
                            $obj.closest('tr').removeClass('eggshellRow');
                        }
                        else {
                            $obj.closest('tr').addClass('eggshellRow');
                        }

                    },
                    "No": function () {
                        $(this).dialog('close');
                    },
                    "Cancel": function () {
                        $(this).dialog('close');
                    }
                }
            });
            return false;
        });
        $('.tooltip').tooltipster({
            interactive: true
        });
        $('.datetime').timepicker({
            timeFormat: 'hh:mm tt',
            controlType: 'select'
        });
        $('.lblAutoPresent').click(function () {
            var AutoPresentID = $(this).attr('data-ap-id');
            var UserID = $(this).attr('data-ap-userid');
            var ClockInTime = $(this).attr('data-ap-clockin');
            var ClockOutTime = $(this).attr('data-ap-clockout');
            $('.txtOPStartTime').val(ClockInTime);
            $('.txtOPEndTime').val(ClockOutTime);

            var Active = $(this).attr('data-ap-active');
            $('#divAutoPresent').dialog({
                modal: true,
                buttons: {
                    "Save": function () {
                        ClockInTime = $('.txtOPStartTime').val();
                        ClockOutTime = $('.txtOPEndTime').val();
                        try {
                            if ($('.trAPC').css('display') == 'none') {
                                Active = 'False';
                            }
                            else {
                                Active = 'True';
                            }
                            AddUpdateAutoPresent(AutoPresentID, UserID, ClockInTime, ClockOutTime, Active);
                            $('.lblAutoPresent').attr('data-ap-clockin', $('.txtOPStartTime').val());
                            $('.lblAutoPresent').attr('data-ap-clockout', $('.txtOPEndTime').val());
                            $('.lblAutoPresent').attr('data-ap-active', Active);
                            if (Active == "True") {
                                $('.lblAutoPresent').text('On');
                            }
                            else {
                                $('.lblAutoPresent').text('Off');
                            }

                            $(this).dialog("close");
                        } catch (e) {

                        }
                    },
                    "Cancel": function () {
                        //alert('close');
                        $(this).dialog("close");
                    }
                },
                show: {

                }
            });
        })
        $('.lblactive').click(function () {
            var userid = $(this).attr('data-userid');
            var status = $(this).attr('data-active');
            $('#divActive').attr('title', "Set status");
            if (status == "True") {
                $('#divActive').html("<p>Do you want to deactivate employee?</p>");
                $('#divActive').dialog({
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            SetUserStatus(userid, status, false);
                            $('.lblactive').text("Inactive");
                            $('.lblactive').attr("data-active", "False");
                            $(this).dialog('close');
                        },
                        "No": function () {
                            $(this).dialog('close');
                        },
                        "Cancel": function () {
                            $(this).dialog('close');
                        }
                    }
                });
            }
            else {
                $('#divActive').html("<p>Do you want to activate employee?</p>");
                $('#divActive').dialog({
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            SetUserStatus(userid, status, false);
                            $('.lblactive').text("Active");
                            $('.lblactive').attr("data-active", "True");
                            $(this).dialog('close');
                        },
                        "No": function () {
                            $(this).dialog('close');
                        },
                        "Cancel": function () {
                            $(this).dialog('close');
                        }
                    }
                });
            }

        });
        $('.chkAutoPresent').click(function () {
            if ($(this).is(":checked")) {
                var returnVal = confirm("Are you sure?");
                $(this).attr("checked", returnVal);
                alert(returnVal);
            }
        });
        $('#btnUpdateTime').click(function () {
            if ($('.txtEFrmTime').val() == 'Select' || $('.txtEToTime').val() == 'Select') {
                alert('Please select valid date time range');
                return false;
            }
            $('#divEditTime').dialog().hide(200);
        });
        $('#btnDeleteTime').click(function () {
            var frmTime = $('.txtfrmTime').val();
            var toTime = $('.txtToTime').val();
            if (frmTime == 'Select' || toTime == 'Select') {
                alert('Please select a time range');
                return false;
            }
            getTimeDifference(frmTime, toTime, '#lblHoursToDel');
            var dlg = $('#divConfirmDeleteTime').dialog({
                model: true
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            //if (iframe != null)
            //    iframe.height = "600px";
            return false;
        });
        $('#btnNo, #btnCancel').click(function () {
            //$('#divConfirmDeleteTime').hide(200);
            $('#divConfirmDeleteTime').dialog("close");
            return false;
        });
        $('.lnkMarkAbsence').click(function () {
            var label;
            $('.lnkMarkAbsence').each(function () {
                label = $(this).text();
            });
            showDialog("Are you sure?", "<p>Are you sure you want to " + label + "?</p>", $(this));
            return false;
        });
        function showDialog(title, message, obj) {
            $('#divClockedIn').attr("title", title);
            $('#divClockedIn').html(message);
            $('#divClockedIn').dialog({
                model: true,
                buttons: {
                    "Yes": function () {
                        showAbsCommentBox(obj);
                    },
                    "No": function () {
                        $(this).dialog("close");
                    },
                    "Cancel": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
        function showAbsCommentBox(obj) {
            $('#absComment').dialog({
                width: 375,
                modal: true,
                buttons: {
                    "Save": function () {
                        addAbsence(obj, $('#txtAbsComment').val(), false);
                        $(this).dialog('close');
                        $('#divClockedIn').dialog("close");
                    },
                    "Cancel": function () {
                        $(this).dialog('close');
                    }
                }
            });
        }
        $('.imgDeleteHours').click(function () {
            //alert('delete hours');
            var $obj = $(this);
            var frmTime = $obj.attr('data-frmTime');
            var toTime = $obj.attr('data-toTime');
            var recordID = $obj.attr('data-recordID');
            var breakTime = "Break Time: " + frmTime + " - " + toTime;
            var dataday = $obj.attr('data-day');
            $('#lbldataday').text(dataday);

            $('.txtfrmTime').val('Select');
            $('.txtToTime').val('Select');
            $('#rID').val(recordID);

            var dlg = $("#revHr").dialog({
                height: 270,
                width: 380,
                modal: true,
                hide: {
                    effect: "blind",
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            return false;
        });
        $('.imgEditHours').click(function () {
            var $obj = $(this);

            var frmTime = $obj.attr('data-frmTime');
            var toTime = $obj.attr('data-toTime');
            var frmDate = $obj.attr('data-frmdate');
            var toDate = $obj.attr('data-todate');
            var recordID = $obj.attr('data-recordID');
            var breakTime = "Clock in time: " + frmTime + " - " + toTime;

            $('.txtEFrmTime').val(frmDate);
            $('.txtEToTime').val(toDate);
            $('#hdnerid').val(recordID);

            var dlg = $("#divEditTime").dialog({
                width: 393,
                modal: true,
                hide: {
                    effect: "blind",
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            //if (iframe != null)
            //    iframe.height = "600px";
            return false;
        });
    });

    $(document).ready(function () {
        $("#lnkcomment").click(function () {
            $('#lblerror').text("");

            var dlg = $("#addcomment").dialog({
                height: 160,
                width: 450,
                modal: true,
                hide: {
                    effect: "blind",
                }
            });
            var textbox = document.getElementById('txtcomment');
            textbox.focus();
            textbox.value = textbox.value;
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            //if (iframe != null)
            //    iframe.height = "600px";
            return false;
        });
        $("#link").click(function () {
            openpopup();
            return false;
        });
        $(".DatePick").datepicker({
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true
        });
        $('.bedit').click(function () {
            var frmTime = $(this).attr('data-bfrom');
            var toTime = $(this).attr('data-bto');
            setTimePicker(frmTime, toTime, $(this));
        });
        $('.imgDeleteHours').click(function () {
            var frmTime = $(this).attr('data-frmTime');
            var toTime = $(this).attr('data-toTime');
            setTimePicker(frmTime, toTime);
        });
        function setTimePicker(from, to) {
            var f = $("#hftimeFormat").val();
            var fromTime = parseInt(getTimeFromAMPM(from).split(':')[0]);
            var toTime = parseInt(getTimeFromAMPM(to).split(':')[0]);
            $(".TimePick").timepicker({
                hourMin: fromTime,
                hourMax: toTime,
                //controlType: 'select',
                //'minTime': from,
                //'maxTime': to,
                timeFormat: 'hh:mm tt',
                controlType: 'select'
                //'showDuration': true
            });
        }
        $(".TimePick").timepicker({
            //'timeFormat': f, 'step': 1, 'forceRoundTime': true,            
            //hourMin: fromTime,
            //hourMax: toTime,
            //controlType: 'select',
            //'minTime': from,
            //'maxTime': to,
            timeFormat: 'hh:mm tt',
            controlType: 'select'
            //'showDuration': true
        });
        function setTimePicker(from, to, $obj) {
            //var f = $("#hftimeFormat").val();
            var fromTime = parseInt(getTimeFromAMPM(from).split(':')[0]);
            var toTime = parseInt(getTimeFromAMPM(to).split(':')[0]);
            $obj.timepicker({
                //'timeFormat': f, 'step': 1, 'forceRoundTime': true,

                hourMin: fromTime,
                hourMax: toTime,
                //controlType: 'select',
                //'minTime': from,
                //'maxTime': to,
                timeFormat: 'hh:mm tt',
                controlType: 'select'
                //'showDuration': true
            });
        }
        $(".DateTimePick").datetimepicker({
            timeFormat: "hh:mm tt",
            controlType: 'select'
        });
    });

    $(document).on('click', 'img.bedit', function () {
        try {
            var $edit = $(this);
            var $parenttr = $edit.parent();
            var dataflag = $edit.attr("data-flag");
            var databid = $edit.attr("data-bid");
            var bfrom = $edit.attr("data-bfrom");
            var bto = $edit.attr("data-bto");
            var bdate = $edit.attr("data-bdate");
            $('.bfrmTime').val(bfrom);
            $('.btoTime').val(bto);
            $('#beDate').val(bdate);
            $('#bID').val(databid);
            $('#bModified').val(dataflag);
            $('.lblBreakTime').text('Break Time: ' + bfrom + " - " + bto);
            var dlg = $("#edtHr").dialog({
                height: 226,
                width: 291,
                top: 60,
                show: {
                },
                modal: true,
                hide: {
                    effect: "blind",
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");

        } catch (e) {
            alert(e.message);
        }
        return false;
    });

    $(document).ready(function () {
        if ($('#hdnData').val() != "true") {
            $('.trdata').hide();
        }
        else {
            $('.trdata').show();
        }
    });

    function editTime() {
        var bID = "";
        var bModified = "";
        var bfrmTime = $('.bfrom').val();
        var btoTime = $('.bto').val();
        var bDate = $('#bDate').val();


        var dlg = $("#edtHr").dialog({
            height: 226,
            width: 291,
            show: {
            },
            modal: true,
            hide: {
                effect: "blind",
            }
        });
        dlg.parent().appendTo($("form:first"));
        dlg.parent().css("z-index", "1000");
        var iframe = window.parent.document.getElementById("MainContent_ifapp");
        //if (iframe != null)
        //    iframe.height = "600px";
        return false;
    }

    function openpopup() {
        $('#lblerror').text("");
        var dlg = $("#divworkeradd").dialog({
            height: 450,
            width: 800,
            show: {
            },
            modal: true,
            hide: {
                effect: "blind",
            }
        });
        dlg.parent().appendTo($("form:first"));
        dlg.parent().css("z-index", "1000");
        var iframe = window.parent.document.getElementById("MainContent_ifapp");
        //if (iframe != null)
        //    iframe.height = "600px";
        return false;
    }

    function editdiv() {
        var dlg = $("#editworker").dialog({
            height: 255,
            width: 450,
            modal: true,
            hide: {
                effect: "blind",
            }
        });
        dlg.parent().appendTo($("form:first"));
        dlg.parent().css("z-index", "1000");
        var iframe = window.parent.document.getElementById("MainContent_ifapp");
        //if (iframe != null)
        //    iframe.height = "600px";
        return false;

    }

    function delTime() {
        var dlg = $("#revHr").dialog({
            height: 140,
            width: 200,
            modal: true,
            hide: {
                effect: "blind",
            }
        });
        dlg.parent().appendTo($("form:first"));
        dlg.parent().css("z-index", "1000");
        var iframe = window.parent.document.getElementById("MainContent_ifapp");
        //if (iframe != null)
        //    iframe.height = "600px";
        return false;
    }
</script>
<div id="secprofile" style="margin-top: 5px">
    <asp:HiddenField ID="hfparentid" runat="server" />
    <h1 class="username">
        <asp:Label ID="lblUserName" runat="server" Text="Derp" Style="margin-bottom: 5px;"></asp:Label>
        &nbsp;<asp:LinkButton runat="server" ID="LinkButton1" Text="Back to reports" Style="font-size: 12px;" OnClick="bk_Click"></asp:LinkButton>

    </h1>
    <div style="clear: both">
        <div style="text-align: left">
            <div id="filtersContainer" runat="server" visible="false">
                <span id="tmC" style="display: none">Start Time:
                    <input type="text" id="time" name="time" style="width: 85px" /><input type="button" value="Show" id="btntC" style="margin-top: 2px" />
                </span>
            </div>
        </div>
    </div>
    <table class="profile" style="margin-top: 12px;">
        <tr id="trSelUser" runat="server">
            <td colspan="2">Select Employee: 
                <asp:DropDownList runat="server" ID="ddlUsers">
                    <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">Please select month and year
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:DropDownList runat="server" ID="ddlYear" Height="21px">
                    <asp:ListItem Text="Select Year" Value="0" Enabled="true"></asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList runat="server" ID="ddlMonth1" Height="21px">
                </asp:DropDownList>
                <%--<asp:DropDownList runat="server" ID="ddlMonth" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged" Height="21px" Width="148px">
                    <asp:ListItem Text="Select Month" Value="0" Enabled="true"></asp:ListItem>
                </asp:DropDownList>--%>
                <asp:DropDownList runat="server" ID="ddlDayNotAcc" Height="23px" Width="184px">
                    <asp:ListItem Text="All" Value="0" Enabled="true"></asp:ListItem>
                    <asp:ListItem Text="Days with no entries" Value="1" Enabled="true"></asp:ListItem>
                    <asp:ListItem Text="Absences" Value="2" Enabled="true"></asp:ListItem>
                    <asp:ListItem Text="Holidays when this employee worked" Value="3" Enabled="true"></asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="btnGR" runat="server" Text="Generate Report" Width="190px" Style="margin-top: -2px" OnClick="btnGR_Click" class="ui-state-default" />
            </td>

        </tr>
        <tr class="trdata">
            <td></td>
            <td>
                <asp:Table ID="tblHistory" runat="server">
                </asp:Table>
            </td>
        </tr>
        <tr class="trdata">
            <td class="caption">Absences this month: 
            </td>
            <td>
                <asp:Label ID="lblTotalAbsences" runat="server" Text="0"></asp:Label>

                <asp:Label ID="lblTotalNE" runat="server" Text="" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr class="trdata">
            <td class="caption">Total days worked: 
            </td>
            <td>
                <asp:Label ID="lblTotDays" runat="server" Text="0"></asp:Label>

            </td>
        </tr>
        <tr class="trdata">
        </tr>
        <%--<tr class="trdata" style="display: none">
            <td class="caption">Total hours should be: 
            </td>
            <td>
                <asp:Label ID="lblEstTotalHours" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr class="trdata" style="display: none">
            <td class="caption">Hours this month: 
            </td>
            <td>
                <asp:Label ID="lblTotalHours" runat="server" Text=""></asp:Label>
            </td>
        </tr>--%>
        <tr class="trdata">
            <td class="caption">Working hrs should be: 
            </td>
            <td>
                <asp:Label ID="lblEstWorkingHours" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <%--<tr class="trdata" style="display: none">
            <td class="caption">Break hrs should be: 
            </td>
            <td>
                <asp:Label ID="lblEstBreakHrs" runat="server" Text=""></asp:Label>
            </td>
        </tr>--%>
        <tr class="trdata">
            <td class="caption">Worked this month:
            </td>
            <td>
                <asp:Label ID="lblWHours" runat="server" Style="float: left"></asp:Label>
                <%--<img alt="Info" src="images/controls/question.png" class="tooltip" title="Hours Info" runat="server" id="imgWorkedHourInfo" style="padding-left: 4px" />--%>
            </td>
        </tr>
        <tr class="trdata">
            <td class="caption">Breaks this month:
            </td>
            <td>
                <asp:Label ID="lblTotBreakHrs" runat="server" Style="float: left"></asp:Label>
                <%--<img alt="Info" src="images/controls/question.png" class="tooltip" title="Hours Info" runat="server" id="imgBreakHourInfo" style="padding-left: 4px" />--%>
            </td>
        </tr>
        <tr class="trdata">
            <td class="caption">Extra days worked:
            </td>
            <td>
                <asp:Label ID="lblExtraDays" runat="server" Style="float: left"></asp:Label>
                <%--<img alt="Info" src="images/controls/question.png" class="tooltip" title="Hours Info" runat="server" id="imgBreakHourInfo" style="padding-left: 4px" />--%>
            </td>
        </tr>
        <tr id="trMsg" runat="server" visible="false">
            <td colspan="2">
                <asp:Label ID="lblMsg" runat="server" ForeColor="#800000" Text="No records found..."></asp:Label>
            </td>
        </tr>
        <%--<tr class="trdata" style="display:none">
            <td class="caption">Extra hours worked:
            </td>
            <td>
                <asp:Label ID="lblExtraHrs" runat="server" Style="float: left" text="0 hrs"></asp:Label>
                <%--<img alt="Info" src="images/controls/question.png" class="tooltip" title="Hours Info" runat="server" id="imgBreakHourInfo" style="padding-left: 4px" />
            </td>
        </tr>--%>
        <%--<tr class="trdata">
            <td class="caption">Max hours:
            </td>
            <td>
                <asp:Label ID="lblMaxHours" runat="server" Style="float: left"></asp:Label>
                <%--<img alt="Info" src="images/controls/question.png" class="tooltip" title="Hours Info" runat="server" id="imgBreakHourInfo" style="padding-left: 4px" />
            </td>
        </tr>--%>
    </table>
    <div>
        <asp:Label ID="lblTest" runat="server" Visible="false"></asp:Label>
    </div>
    <asp:HiddenField ID="hdnData" ClientIDMode="Static" Value="false" runat="server" />
    <div id="divClockedIn" title="Clocked In Successfully!" style="display: none">
        <p>
            You have successfully clocked in. 
        </p>
        <br />
        <p>
            Please verify your location by clicking the link in your verification email sent to 
                <asp:Label runat="server" ID="lblInEmail"></asp:Label>
        </p>
    </div>
</div>



<div id="revHr" title="Delete Time" style="display: none">
    <asp:HiddenField ClientIDMode="Static" runat="server" ID="rID" />
    <p>
        Use this option to delete time from your recorded history in order to fix any excess time you recorded by mistake.
    </p>
    <p style="padding: 12px 32px; font-weight: bold">
        <label id="lbldataday"></label>
    </p>
    <div class="divDelHours">
        <asp:Label runat="server" ID="Label1" CssClass="Label1" Text="From:" Width="57px"></asp:Label>
        <asp:TextBox ID="txtfrmTime" runat="server" CssClass="TimePick txtfrmTime" ValidationGroup="dt" Width="65px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtfrmTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="dt">Required</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtfrmTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="(0?[1-9]|(10|11|12))(:[0-5][0-9])( )*(AM|PM|am|pm)" ValidationGroup="dt">Invalid Time</asp:RegularExpressionValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtfrmTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="([01]?[0-9]|2[0-3]):[0-5][0-9]" Visible="False" ValidationGroup="dt">Invalid Time</asp:RegularExpressionValidator>
    </div>
    <div class="divDelHours">
        <asp:Label runat="server" ID="Label2" Text="To:" Width="57px"></asp:Label>
        <asp:TextBox ID="txtToTime" runat="server" CssClass="TimePick txtToTime" ValidationGroup="dt" Width="65px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtToTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="dt">Required</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtToTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="(0?[1-9]|(10|11|12))(:[0-5][0-9])( )*(AM|PM|am|pm)" ValidationGroup="dt">Invalid Time</asp:RegularExpressionValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtToTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="([01]?[0-9]|2[0-3]):[0-5][0-9]" Visible="False" ValidationGroup="dt">Invalid Time</asp:RegularExpressionValidator>
    </div>
    <div class="divDialogButton">
        <button value="Delete Time" id="btnDeleteTime" class="btnDeleteTime ui-state-default">Delete Time</button>
    </div>
</div>
<div id="divEditTime" title="Edit Time" style="display: none">
    <asp:HiddenField ClientIDMode="Static" runat="server" ID="hdnerid" />
    <table class="tblDlg" id="tblDlg">
        <tr>
            <th style="text-align: left" colspan="2">Use this option to edit time from your recorded history.
            </th>
        </tr>
        <tr>
            <td>&nbsp;
                            <asp:Label ID="lblRowID" CssClass="lblRowID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <th>From:
            </th>
            <td>
                <asp:TextBox ID="txtEFrmTime" runat="server" CssClass="DateTimePick txtEFrmTime" ValidationGroup="dt" Width="160px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEFrmTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="dt">Required</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <th>To:
            </th>
            <td>
                <asp:TextBox ID="txtEToTime" runat="server" CssClass="DateTimePick txtEToTime" ValidationGroup="dt" Width="160px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtEToTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="dt">Required</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <th>Clock in:
            </th>
            <td>
                <asp:DropDownList ID="ddlCIV" runat="server">
                    <asp:ListItem Text="Verified" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Not verified" Value="0"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <th>Clock out:
            </th>
            <td>
                <asp:DropDownList ID="ddlCOV" runat="server">
                    <asp:ListItem Text="Verified" Value="Verified" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Not Verified" Value="Not Verified"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <th>Comments:
            </th>
            <td>
                <asp:TextBox ID="txteComments" runat="server" TextMode="MultiLine">
                </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: right">
                <asp:LinkButton ID="lnkEditUndertime" CssClass="_lnkEditUndertime hide" runat="server" OnClick="lnkEditUndertime_Click" Text="Edit Undertime"></asp:LinkButton>
                &nbsp;&ndash;&nbsp;
                            <asp:LinkButton ID="lnkEditOvertime" CssClass="_lnkEditOvertime hide" runat="server" OnClick="lnkEditOvertime_Click" Text="Edit Overtime"></asp:LinkButton>
            </td>
        </tr>
    </table>
    <div class="divDialogButton">
        <asp:Button ID="btnUpdateTime" runat="server" Text="Update Time" CssClass="ui-state-default btnUpdateTime raisesevent" OnClick="btnUpdateTime_Click" />
    </div>
</div>
<div id="divConfirmDeleteTime" class="hide" title="Are you sure?">
    <p style="margin-bottom: 5px">
        <b>Warning:</b> Once the hours are deleted you will not be able to restore them.
    </p>
    <p style="margin-bottom: 5px">
        You are deleting <b>
            <label id="lblHoursToDel" class="lblHoursToDel">#</label></b> from your work log. 
    </p>
    <p style="margin-bottom: 5px">
        Are you sure you want to do delete this time?
    </p>
    <div class="divDialogButton">
        <asp:Button runat="server" ID="deleHrs" Text="Yes" OnClick="deleHrs_Click" CssClass="raisesevent ui-state-default" ValidationGroup="dt" />
        <button id="btnNo" class="btnclosedlg ui-state-default" value="No">No</button>
        <button id="btnCancel" class="btnclosedlg ui-state-default" value="Cancel">Cancel</button>
    </div>
</div>
<div id="addcomment" title="Comment" style="display: none">
    <asp:HiddenField ClientIDMode="Static" runat="server" ID="hfcommentstatus" />
    <asp:TextBox runat="server" ID="txtcomment" TextMode="MultiLine" Height="50px"></asp:TextBox>
    <br />
    <br />

    <asp:Button runat="server" ID="btnin" CssClass="raisesevent ui-state-default" Text="Post Comment" placeholder="Please provide your comments..." OnClick="BtncommentClick" Style="float: right" />
</div>
<div id="addcomentout" title="Signout Comment" style="display: none">
    <asp:TextBox runat="server" ID="txtcommentout" TextMode="MultiLine" Height="50px"></asp:TextBox>
    <br />
    <br />
    <asp:Button ID="btnout" runat="server" Text="Post Comment" Style="float: right" />
</div>
<div id="absComment" class="hide" title="Comment">
    <table>
        <tr>
            <td>
                <textarea id="txtAbsComment" class="txtAbsComment" style="width: 333px; height: 57px;" placeholder="Please add your comments here..."></textarea>
            </td>
        </tr>
    </table>
</div>
<div id="Dialog" title="">
</div>
<div id="divOvertimeEdit" class="divOvertimeEdit hide" title="Edit Over-time">
    <asp:Repeater ID="rptOvertimeEdit" runat="server" OnItemCommand="rptOvertimes_ItemCommand">
        <HeaderTemplate>
            <table>
                <table class="smallGrid">
                    <tr class="smallGridHead">
                        <td>Title
                        </td>
                        <td>From
                        </td>
                        <td>To
                        </td>
                        <td>Comment
                        </td>
                        <td></td>
                    </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <%# Eval("Type") %>
                </td>
                <td>
                    <asp:Label ID="lblOverTimeFrom" CssClass="lblOverTimeFrom" runat="server" Text='<%# App.GetTime(Eval("FromTime").ToString(),HttpContext.Current.User.Identity.Name) %>'></asp:Label>
                    <asp:TextBox ID="txtOverTimeFrom" CssClass="txtOverTimeFrom TimePick hide" runat="server" Text='<%# App.GetTime(Eval("FromTime").ToString(),HttpContext.Current.User.Identity.Name) %>'></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="lblOverTimeTo" CssClass="lblOverTimeTo" runat="server" Text='<%# App.GetTime(Eval("ToTime").ToString(),HttpContext.Current.User.Identity.Name) %>'>></asp:Label>
                    <asp:TextBox ID="txtOverTimeTo" CssClass="txtOverTimeTo TimePick hide" runat="server" Text='<%# App.GetTime(Eval("ToTime").ToString(),HttpContext.Current.User.Identity.Name) %>'></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="lblOverTimeComment" CssClass="lblOverTimeComment" runat="server" Text='<%# Eval("Comment") %>'></asp:Label>
                    <asp:TextBox ID="txtOverTimeComment" CssClass="txtOverTimeComment hide" runat="server" TextMode="MultiLine" Text='<%# Eval("Comment") %>'>></asp:TextBox>
                </td>
                <td>
                    <asp:LinkButton ID="lnkEditOvertime" CssClass="lnkEditOvertime lnkeditO" runat="server">Edit&nbsp;</asp:LinkButton>

                    <asp:LinkButton ID="lnkCancelOvertime" CssClass="lnkCancelOvertime lnkcancelO hide" runat="server">Cancel&nbsp;</asp:LinkButton>

                    <asp:LinkButton ID="lnkUpdateOvertime" CssClass="lnkUpdateOvertime hide lnkupdateO raisesevent" CommandName="Update" CommandArgument='<%# Eval("OTID") %>' runat="server">Update&nbsp;</asp:LinkButton>

                    <asp:LinkButton ID="lnkDeleteOvertime" CssClass="lnkDeleteOvertime lnkdeleteO raisesevent" CommandArgument='<%# Eval("OTID") %>' CommandName="Delete" runat="server">Delete</asp:LinkButton>
                    <asp:HiddenField ID="hdnOvertimeDate" runat="server" Value='<%# Eval("FromTime") %>' />
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</div>
<div id="divUOT" class="divUOT hide" title="Edit Under-time">
    <asp:Repeater ID="rptUOT" runat="server" OnItemCommand="rptUOT_ItemCommand">
        <HeaderTemplate>
            <table class="smallGrid">
                <tr class="smallGridHead">
                    <td>Title
                    </td>
                    <td>From
                    </td>
                    <td>To
                    </td>
                    <td>Comment
                    </td>
                    <td></td>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <asp:Label ID="lblUnderTimeTitle" CssClass="lblUnderTimeTitle" runat="server" Text='<%# Eval("Type") %>'></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblUnderTimeFrom" CssClass="lblUnderTimeFrom" runat="server" Text='<%# App.GetTime(Eval("FromTime").ToString(),HttpContext.Current.User.Identity.Name) %>'></asp:Label>
                    <asp:TextBox ID="txtUnderTimeFrom" CssClass="txtUnderTimeFrom TimePick hide" runat="server" Text='<%# App.GetTime(Eval("FromTime").ToString(),HttpContext.Current.User.Identity.Name) %>'></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="lblUnderTimeTo" CssClass="lblUnderTimeTo" runat="server" Text='<%# App.GetTime((Eval("ToTime") == null)? "":Eval("ToTime").ToString(),HttpContext.Current.User.Identity.Name) %>'>></asp:Label>
                    <asp:TextBox ID="txtUnderTimeTo" CssClass="txtUnderTimeTo TimePick hide" runat="server" Text='<%# App.GetTime((Eval("ToTime") == null)? "":Eval("ToTime").ToString(),HttpContext.Current.User.Identity.Name) %>'></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="lblUnderTimeComment" CssClass="lblUnderTimeComment" runat="server" Text='<%# Eval("Comment") %>'></asp:Label>
                    <asp:TextBox ID="txtUnderTimeComment" CssClass="txtUnderTimeComment hide" runat="server" TextMode="MultiLine" Text='<%# Eval("Comment") %>'>></asp:TextBox>
                </td>
                <td>
                    <asp:LinkButton ID="lnkEditUndertime" CssClass="lnkEditUndertime lnkeditO" runat="server">Edit&nbsp;</asp:LinkButton>

                    <asp:LinkButton ID="lnkCancelUndertime" CssClass="lnkCancelUndertime lnkcancelO hide" runat="server">Cancel&nbsp;</asp:LinkButton>

                    <asp:LinkButton ID="lnkUpdateUndertime" CssClass="lnkUpdateUndertime hide lnkupdateO raisesevent" CommandName="Update" CommandArgument='<%# Eval("UTID") %>' runat="server">Update&nbsp;</asp:LinkButton>

                    <asp:LinkButton ID="lnkDeleteUndertime" CssClass="lnkDeleteUndertime lnkdeleteO raisesevent" CommandArgument='<%# Eval("UTID") %>' CommandName="Delete" runat="server">Delete&nbsp;</asp:LinkButton>
                    <asp:HiddenField ID="hdnUnderTimeDate" runat="server" Value='<%# Eval("FromTime") %>' />
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <asp:HiddenField ID="hdnEditUOT" runat="server" ClientIDMode="Static" />

    <div id="divAddOvertime" class="divAddOvertime" style="display: none">
        <table class="profile" style="margin-top: 10px">
            <tr>
                <td class="captionauto">Date: 
                </td>
                <td>
                    <asp:TextBox ID="txtUODate" CssClass="txtUODate DatePick" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="AddOvertime" ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtUODate" ForeColor="#cc0000" ErrorMessage="Required"></asp:RequiredFieldValidator>
                </td>

            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <label id="lblUOTitle" class="lblUOTitle"></label>
                </td>
            </tr>
            <tr>
                <td class="captionauto">From: 
                </td>
                <td>
                    <asp:TextBox type="text" ID="txtOverTimeFrom" runat="server" CssClass="txtOverTimeFrom TimePick"></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="AddOvertime" ID="rfvTxtOverTimeFrom" runat="server" ControlToValidate="txtOverTimeFrom" ForeColor="#cc0000" ErrorMessage="Required"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="captionauto">To: 
                </td>
                <td>
                    <asp:TextBox type="text" ID="txtOverTimeTo" runat="server" CssClass="txtOverTimeTo TimePick"></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="AddOvertime" ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtOverTimeTo" ForeColor="#cc0000" ErrorMessage="Required"></asp:RequiredFieldValidator>
                    <br />
                    <asp:Label ID="lblUOTDuration" CssClass="lblUOTDuration" runat="server">0 mins</asp:Label>
                </td>
            </tr>
            <tr>
                <td class="captionauto">Comment: 
                </td>
                <td>
                    <asp:TextBox ID="txtOverTimeComment" ValidationGroup="AddOvertime" CssClass="txtOverTimeComment" runat="server" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: right">
                    <asp:Button ID="btnAddOvertime" CssClass="btnAddOvertime raisesevent ui-state-default" runat="server" Text="Add" OnClick="btnAddOvertime_Click" ValidationGroup="AddOvertime" />
                    <asp:Button ID="btnCancelOvertime" CssClass="btnCancelOvertime ui-state-default" runat="server" Text="Cancel" />
                    <asp:HiddenField ID="hdnUOTime" runat="server" ClientIDMode="Static" />
                </td>
            </tr>
        </table>
    </div>
    <asp:LinkButton ID="lnkAddOvertime1" CssClass="lnkAddOvertime hide" runat="server" >Add Overtime</asp:LinkButton>
    <asp:LinkButton ID="lnkAddUndertime" CssClass="lnkAddUndertime hide" runat="server">Add Undertime</asp:LinkButton>
</div>
<script>
    function OpenEditUOT() {
        var dlg = $('#divUOT').dialog({
            width: 360,
            modal: true,
            hide: { effect: "blind" },
            buttons: {
                'Cancel': function () {
                    $(this).dialog('close');
                }
            }
        });
        dlg.parent().appendTo($("form:first"));
        dlg.parent().css("z-index", "1000");
        var iframe = window.parent.document.getElementById("MainContent_ifapp");
    }

    function OpenEditOvertime() {

        $('#divOvertimeEdit').attr('title', 'Edit Over-time');
        var dlg = $('#divOvertimeEdit').dialog({
            width: 360,
            modal: true,
            hide: { effect: "blind" },
            buttons: {
                'Cancel': function () {
                    $(this).dialog('close');
                }
            }
        });
        dlg.parent().appendTo($("form:first"));
        dlg.parent().css("z-index", "1000");
        var iframe = window.parent.document.getElementById("MainContent_ifapp");
    }

    $(function () {
        $('.lnkEditUndertime_').click(function () {
            $('#hdnerid').val($(this).attr('data-recid'));
            if (!$(this).hasClass('lnkAddUT_')) {
                $('._lnkEditUndertime')[0].click();
            }
            return false;
        });

        $('.lnkEditOvertime_').click(function () {
            $('#hdnerid').val($(this).attr('data-recid'));
            if (!$(this).hasClass('lnkAddOT_')) {
                $('._lnkEditOvertime')[0].click();
            }
            return false;
        });

        $('.lnkeditO').click(function () {
            $(this).parent().parent().find('span').hide();
            $(this).parent().parent().find('input[type="text"], textarea').show();
            $(this).parent().find('.lnkupdateO').show();
            $(this).parent().find('.lnkcancelO').show();
            $(this).hide();
            $(this).parent().parent().find('input[type="text"]').first().focus();
            return false;
        });

        $('.lnkcancelO').click(function () {
            $(this).parent().parent().find('span').show();
            $(this).parent().parent().find('input[type="text"], textarea').hide();
            $(this).parent().find('.lnkupdateO').hide();
            $(this).hide();
            $(this).parent().find('.lnkeditO').show();
            return false;
        });

        $('.lnkAddOvertime, .lnkAddUndertime').click(function () {
            var dialogTitle = "";
            var UOTitle = "";
            if ($(this).hasClass('lnkAddOvertime')) {
                $('#hdnUOTime').val('true');
                //dialogTitle = "Overtime" + " " + (new Date()).toLocaleDateString();
                dialogTitle = "Overtime" + " ";
                UOTitle = "Add overtime";
            }
            else {
                $('#hdnUOTime').val('false');
                //dialogTitle = "Undertime" + " " + (new Date()).toLocaleDateString();
                dialogTitle = "Undertime" + " ";
                UOTitle = "Add undetime";
            }
            var dlg = $('#divAddOvertime').dialog({
                title: dialogTitle, modal: true, hide: { effect: "blind" }
            });
            $('.btnCancelOvertime').click(function () {
                $('#divAddOvertime').dialog('close');
                return false;
            });
            $('.btnAddOvertime').click(function () {
                $('#divAddOvertime').hide('blind');
            });

            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");

            return false;
        });

        $('.lnkAddOT_').click(function () {
            $('.txtUODate').val($(this).attr('data-date'));
            $('.lnkAddOvertime')[0].click();
            return false;
        });
        $('.lnkAddUT_').click(function () {
            $('.txtUODate').val($(this).attr('data-date'));
            $('.lnkAddUndertime')[0].click();
            return false;
        });

        $('.divAddOvertime .txtOverTimeTo, .divAddOvertime  .txtOverTimeFrom').change(function () {
            var fromDate = new Date((new Date()).toDateString() + ' ' + $('.divAddOvertime .txtOverTimeFrom').val());
            var toDate = new Date((new Date()).toDateString() + ' ' + $('.divAddOvertime .txtOverTimeTo').val());
            var duration = GetDHMs((toDate - fromDate) / 60000);
            $('.lblUOTDuration').text(duration);
        })
    });

</script>
