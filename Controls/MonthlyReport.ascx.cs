﻿using MyAttSys;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_MonthlyReport : System.Web.UI.UserControl
{
    private bool IsSimpleClock = true;
    List<RecordModel> records = new List<RecordModel>();
    private int UserId;
    public String InstanceID { get; set; }
    DateTime signInDate;
    public DateTime WorkerLastSignIn { get; set; }
    public DateTime WorkerFirstSignIn { get; set; }
    public DateTime AdminFirstSignIn { get; set; }
    List<AttUser> users = AttUser.GetAll();
    //String breakTimeSho = TimeDifference.GetTimeSpan(new TimeSpan(0, 140, 0));

    protected void Page_Load(object sender, EventArgs e)
    {
        signInDate = SP.GetWorkerFirstSignInn(UserId.ToString());
        if (!IsPostBack) {
            GenerateDate();
            //ddlYear_SelectedIndexChanged(sender, e);
            hdnData.Value = "false";
        }
        if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out UserId)) {
            WorkerLastSignIn = SP.GetWorkerLastSignIn(UserId.ToString()).Date.Date;
            WorkerFirstSignIn = SP.GetWorkerFirstSignInn(UserId.ToString()).Date.Date;
            //hdnerid.Value = UserId.ToString();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString)) {
                using (SqlCommand cmd = new SqlCommand("select * from attendence_management a where a.Id = @id and a.InstanceId = @insId", con)) {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", UserId);
                    cmd.Parameters.AddWithValue("@insId", this.InstanceID);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0) {
                        DataRow dr = dt.Rows[0];
                        lblUserName.Text = dr["Title"].ToString();
                    }
                    else {
                        Response.Redirect("Default.aspx?instanceid=" + this.InstanceID);
                    }
                }

            }
        }
        else {
            Response.Redirect("Default.aspx?instanceid=" + this.InstanceID);
        }

        AddinstanceWS objinst = new AddinstanceWS();
        OwnerId = objinst.GetUserID(HttpContext.Current.User.Identity.Name);
        //OwnerId = "e8a92994-352a-4319-838d-882e8dc1bdeb";

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString)) {
            using (SqlCommand cmd = new SqlCommand("getuserrole", con)) {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", OwnerId.Trim());
                cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0) {

                    AdminFirstSignIn = SP.GetWorkerFirstSignInn(dt.Rows[0][1].ToString()).Date;

                    if (Convert.ToString(dt.Rows[0]["role"]) == "worker" || Convert.ToString(dt.Rows[0]["role"]) == "admin") {
                        if (Convert.ToString(dt.Rows[0]["role"]) == "admin") {
                            isAdmin = true;
                            isWorker = false;
                            trSelUser.Visible = true;
                            appAssignedId = Convert.ToInt32(dt.Rows[0]["userid"]);
                            hfparentid.Value = SP.GetAdminCatID(Convert.ToInt32(dt.Rows[0]["userid"].ToString())).ToString();
                        }
                        else {
                            isWorker = true;
                            //Response.Redirect("Add_worker.aspx?id=" + dt.Rows[0]["userid"]);
                        }
                    }
                    else {
                        isWorker = false;
                        appAssignedId = Convert.ToInt32(dt.Rows[0]["userid"]);
                    }
                }
            }
            if (!IsPostBack) {
                if (ddlMonth1.Items.Count > 0) { ddlMonth1.SelectedIndex = 0; }
                if (ddlYear.Items.Count > 0) {
                    ddlYear.SelectedIndex = 0;
                    //ddlYear_SelectedIndexChanged(sender, e); 
                }
                if (!isWorker) {
                    FillUsers();
                    ddlUsers.SelectedIndex = Users.FindIndex(u => u.userID == UserId);
                    lblUserName.Text = Users.Where(u => u.userID == UserId).SingleOrDefault().userName;
                    trSelUser.Visible = true;
                }
                else {
                    AttUser user = AttUser.GetUserByID(UserId);
                    if (user == null) {
                        ddlUsers.Items.Add(new ListItem("User", UserId.ToString()));
                    }
                    else {
                        ddlUsers.Items.Add(new ListItem(user.UserName, UserId.ToString()));
                    }
                    ddlUsers.SelectedIndex = 1;
                    trSelUser.Style.Add("display", "none");
                    FillandCreateHistory(ddlUsers.SelectedValue.ToString(), con);
                }
            }
        }
    }

    private void FillUsers()
    {
        PopulateUsersAccord(0, isAdmin);
        hfparentid.Value = "0";
    }

    protected void PopulateUsersAccord(int pid, bool isAdmin = false, bool ShowFiltered = false, int status = 1)
    {
        DataTable dtstudentall = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString)) {
            string q = "getallattendencerecordbyid";
            if (ShowFiltered) {
                if (status == 1)
                    q = "getallactiveusers";
                else {
                    q = "getallinactiveusers";
                }
            }
            else {
                if (isAdmin) {
                    q = "getfilteredattendancerecordbyid";
                }
            }

            using (SqlCommand cmd = new SqlCommand(q, con)) {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", pid);
                cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                if (isAdmin) {
                    if (q == "getfilteredattendancerecordbyid") {
                        cmd.Parameters.AddWithValue("@userid", appAssignedId);
                    }
                }
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dtstudentall);
                cmd.CommandText = "getclocktype";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@instanceId", this.InstanceID);
                bool i = (bool)cmd.ExecuteScalar();
                if (i) {
                    IsSimpleClock = false;
                }
                Users = new List<User>();
                if (dtstudentall.Rows.Count > 0) {
                    foreach (DataRow row in dtstudentall.Rows) {
                        if (Convert.ToBoolean(row["category"].ToString()) == false) {
                            Users.Add(new User() { userID = Convert.ToInt32(row[0].ToString()), userName = row[1].ToString() });
                        }
                        else {
                            FetchUsers(Convert.ToInt32(row[0].ToString()));
                        }
                    }
                }
                ddlUsers.DataSource = Users;
                ddlUsers.DataTextField = "userName";
                ddlUsers.DataValueField = "userID";
                ddlUsers.DataBind();
            }
        }
    }

    private void FetchUsers(Int32 parentID)
    {
        List<AttUser> filteredUsers = users.FindAll(u => u.ParentID == parentID);
        foreach (AttUser user in filteredUsers) {
            if (user.Category == false) {
                Users.Add(new User() { userID = user.UserID, userName = user.UserName });
            }
            else {
                FetchUsers(user.ParentID);
            }
        }
    }

    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ddlMonth1.Items.Clear();
        //DateTime startDate = new DateTime();
        //DateTime endDate = new DateTime();
        //if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out UserId))
        //{
        //    startDate = SP.GetWorkerFirstSignInThisYear(UserId.ToString(), Convert.ToInt32(ddlYear.SelectedValue));
        //    if (SP.IsWorkerActive(Convert.ToInt32(UserId.ToString())))
        //    {
        //        endDate = SP.GetWorkerLastSignInThisYear(UserId.ToString(), Convert.ToInt32(ddlYear.SelectedValue));
        //    }
        //    else
        //    {
        //        endDate = SP.GetWorkerLastSignInThisYear(UserId.ToString(), Convert.ToInt32(ddlYear.SelectedValue));
        //    }
        //}
        //String strDate = "";
        //ddlMonth1.Items.Add(new ListItem("Select Month", "0"));
        //for (int i = 0; i <= (endDate.Month - startDate.Month); i++)
        //{
        //    strDate = endDate.AddMonths(-i).ToString("MMMM");
        //    ddlMonth1.Items.Add(new ListItem(strDate, endDate.AddMonths(-i).Month.ToString()));
        //}
        //hdnData.Value = "false";
    }

    private void GenerateDate()
    {
        ddlYear.Items.Clear();
        DateTime startDate = new DateTime();
        DateTime endDate = new DateTime();
        if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out UserId)) {
            startDate = SP.GetWorkerFirstSignInn(UserId.ToString());
            endDate = SP.GetWorkerLastSignIn(UserId.ToString());
        }
        DateTime tempDt = new DateTime(DateTime.Now.Year, 1, 1);
        String strDate = "";
        //ddlYear.Items.Add(new ListItem("Select Year", "0"));
        //ddlYear.Items.Add(new ListItem(endDate.ToString("yyyy"), endDate.ToString("yyyy")));
        //int diff = endDate.Year - startDate.Year;
        for (int i = endDate.Year; i >= startDate.Year; i--) {
            //strDate = startDate.AddYears(i).ToString("yyyy");
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }

        ddlMonth1.Items.Clear();
        for (int i = 1; i <= 12; i++) {
            ddlMonth1.Items.Add(new ListItem((new DateTime(1990, i, 1)).ToString("MMMM"), (new DateTime(1990, i, 1)).Month.ToString()));
        }

        //ddlMonth.Items.Clear();
        //DateTime startDate = new DateTime();
        //DateTime endDate = new DateTime();
        //if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out UserId))
        //{
        //    startDate = SP.GetWorkerFirstSignInThisYear(UserId.ToString(), DateTime.Now.Year);
        //    if (SP.IsWorkerActive(Convert.ToInt32(UserId.ToString())))
        //    {
        //        endDate = DateTime.Now.Date;
        //    }
        //    else
        //    {
        //        endDate = SP.GetWorkerLastSignIn(UserId.ToString());
        //    }
        //}
        //String strDate = "";
        //ddlMonth.Items.Add(new ListItem("Select Month", "0"));
        //for (int i = 0; i <= (endDate.Month - startDate.Month); i++)
        //{
        //    strDate = endDate.AddMonths(-i).ToString("MMMM - yyyy");
        //    ddlMonth.Items.Add(new ListItem(strDate, endDate.AddMonths(-i).Month.ToString()));
        //}
    }

    private void FillandCreateHistory(string userid, SqlConnection con)
    {
        using (SqlCommand cmd = new SqlCommand("getworkerrecordbydate", con)) {
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            List<string> dates;

            DateTime startDate = new DateTime(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(ddlMonth1.SelectedValue), 1);
            DateTime endDate = startDate.AddMonths(1).AddDays(-1);
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@sDate", startDate.Date);
            cmd.Parameters.AddWithValue("@eDate", endDate.Date);
            cmd.Parameters.AddWithValue("@Id", ddlUsers.SelectedValue.ToInt32());
            cmd.Parameters.AddWithValue("@InstanceId", InstanceID);
            adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);

            records = UtilityMethods.getRecords(ds);
            //rptHistory.DataSource = records;
            //rptHistory.DataBind();
            tblHistory.Rows.Clear();
            if (ddlDayNotAcc.SelectedValue == "0") {
                CreateAdminTable(records);
            }
            else if (ddlDayNotAcc.SelectedValue == "1") {
                CreateNAFTable(records, false);
            }
            else if (ddlDayNotAcc.SelectedValue == "3") {
                CreateWeekendTable(records);
            }
            else if (ddlDayNotAcc.SelectedValue == "2") {
                //Abscences
                CreateNAFTable(records, true);
            }

        }
    }

    //private void GenerateStatistics(List<RecordModel> records)
    //{
    //    int totDays = 0;
    //    int totExtDays = 0;
    //    int totExtWeekendsHrs = 0;
    //    int totExtWeekendsMins = 0;
    //    int totHrs = 0;
    //    int totMins = 0;
    //    int wtotHrs = 0;
    //    int maxHours = 0;
    //    int maxMin = 0;
    //    int wtotMins = 0;
    //    int bTotShouldHrs = 0;
    //    int bTotShouldMins = 0;
    //    int btotHrs = 0;
    //    int btotMins = 0;
    //    TimeSpan totBreakTime = new TimeSpan();

    //    List<Absence> absences = Absence.GetAbsences(Convert.ToInt32(ddlUsers.SelectedValue)).Where(u => u.Active == true).ToList();
    //    foreach (RecordModel record in records.OrderBy(u => u.Date.Date).ToList())
    //    {
    //        // Extra days and weekend hours
    //        if (record.Records.Count > 0)
    //        {

    //            if (record.DayTitle != null)
    //            {
    //                totExtDays++;
    //                string[] weekendWorkHours;
    //                // Weekend Worked hours
    //                weekendWorkHours = record.TotalWorkedHours.Split(' ');
    //                CalculateTime(ref totExtWeekendsHrs, ref totExtWeekendsMins, weekendWorkHours);
    //            }
    //            else
    //            {
    //                totDays++;
    //            }
    //        }
    //        // Total Hours without breaks
    //        string[] workedhours = record.TotalTime.Split(' ');
    //        if (workedhours.Count() > 3)
    //        {
    //            totHrs += Convert.ToInt32(workedhours[0]);
    //            totMins += Convert.ToInt32(workedhours[2]);
    //            if (totMins >= 60)
    //            {
    //                totHrs += Convert.ToInt32((totMins / 60));
    //                totMins -= 60;
    //            }

    //            if (Convert.ToInt32(workedhours[0]) > maxHours)
    //            {
    //                maxHours = Convert.ToInt32(workedhours[0]);
    //                maxMin = Convert.ToInt32(workedhours[2]);
    //                //lblMaxHours.Text = record.TotalWorkedHours + " on " + record.Date.ToString("ddd, MMM dd yyyy"); ;
    //            }
    //            else if (Convert.ToInt32(workedhours[0]) == maxHours)
    //            {
    //                if (Convert.ToInt32(workedhours[2]) > maxMin)
    //                {
    //                    maxHours = Convert.ToInt32(workedhours[0]);
    //                    maxMin = Convert.ToInt32(workedhours[2]);
    //                    //lblMaxHours.Text = record.TotalWorkedHours + " on " + record.Date.ToString("ddd, MMM dd yyyy");
    //                }
    //            }
    //        }
    //        else
    //        {
    //            if (Convert.ToInt32(workedhours[0]) < 0)
    //            {
    //                totMins += Convert.ToInt32(workedhours[0]);
    //                if (totMins >= 60)
    //                {
    //                    totHrs += Convert.ToInt32((totMins / 60));
    //                    totMins -= 60;
    //                }
    //            }
    //        }

    //        // Total Should Break hours
    //        //if (record.DayTitle == null)
    //        //{
    //        //    if (absences.Where(u => u.CrtDate.Date == record.Date.Date).Count() <= 0)
    //        //    {
    //        //        workedhours = record.TotalBreak.Split(' ');
    //        //        CalculateTime(ref bTotShouldHrs, ref bTotShouldMins, workedhours);
    //        //    }
    //        //}


    //        //if (record.DayTitle == null)
    //        //{
    //        // Total Break hours
    //        if (record.Records.Count > 0)
    //        {
    //            workedhours = record.TotalBreak.Split(' ');
    //            TimeSpan ts = SP.GetTodaysBreak(this.InstanceID, record.Date.Date);
    //            totBreakTime = totBreakTime.Add(ts);
    //            //int bHours = ts.TotalMilliseconds;
    //            //bTotShouldHrs += bHours;
    //            CalculateTime(ref btotHrs, ref btotMins, workedhours);
    //        }


    //        // Worked hours without breaks
    //        workedhours = record.TotalWorkedHours.Split(' ');
    //        CalculateTime(ref wtotHrs, ref wtotMins, workedhours);
    //        //}
    //    }

    //    lblExtraDays.Text = totExtDays.ToString() + " days (" + totExtWeekendsHrs + " hrs " + totExtWeekendsMins + " mins)";
    //    lblTotDays.Text = totDays.ToString();
    //    HoursCalculation(totHrs, totMins, wtotHrs, wtotMins, btotHrs, btotMins, totBreakTime, bTotShouldMins);
    //}

    private void GenerateStatistics(List<RecordModel> records)
    {
        List<DateTime> wholeWeek = new List<DateTime>();
        List<Absence> absences = Absence.GetAbsences(Convert.ToInt32(UserId)).Where(u => u.Active == true).ToList();
        TimeSpan tsworkingHoursShouldBe = new TimeSpan(0);
        TimeSpan tsworkingHours = new TimeSpan(0);
        TimeSpan tsbreakhours = new TimeSpan(0);
        TimeSpan tsbreakhoursShouldBe = new TimeSpan(0);
        TimeSpan tsWeekendWorkedHours = new TimeSpan(0);
        int iDaysWorked = 0;
        foreach (var record in records) {
            if (record.DayTitle != null && record.Records.Count > 0) {
                tsWeekendWorkedHours += record.TotalWorkedHoursTS;
                tsbreakhours += record.TotalBreakTS;
                tsbreakhoursShouldBe += record.TotalBreakShouldBeTS;
                ++iDaysWorked;
            }
            else if (record.Records.Count > 0 && record.DayTitle == null) {
                tsworkingHours += record.TotalWorkedHoursTS;
                tsbreakhours += record.TotalBreakTS;
                tsbreakhoursShouldBe += record.TotalBreakShouldBeTS;
                tsworkingHoursShouldBe += record.WorkingHours;
                ++iDaysWorked;
            }
        }

        lblEstWorkingHours.Text = UtilityMethods.getFormatedTimeByMinutes(tsworkingHoursShouldBe.TotalMinutes.ToInt32());
        lblWHours.Text = UtilityMethods.getFormatedTimeByMinutes(tsworkingHours.TotalMinutes.ToInt32()) + " (Weekend Work Hours Excluded)";
        lblTotBreakHrs.Text = UtilityMethods.getFormatedTimeByMinutes(tsbreakhours.TotalMinutes.ToInt32()) + " (should be " + UtilityMethods.getFormatedTimeByMinutes(tsbreakhoursShouldBe.TotalMinutes.ToInt32()) + ")";
        lblExtraDays.Text = UtilityMethods.getFormatedTimeByMinutes(tsWeekendWorkedHours.TotalMinutes.ToInt32());
        lblTotDays.Text = iDaysWorked.ToString();
    }



    private void CreateAdminTable(List<RecordModel> records)
    {
        AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
        tblHistory.Rows.Clear();
        //tblHistory = new Table();
        tblHistory.CssClass = "smallGrid";
        TableRow trow = new TableRow();
        trow.CssClass = "smallGridHead";
        #region Table Headers
        trow.Cells.Add(new TableCell() { Text = "" });
        trow.Cells.Add(new TableCell() { Text = "Clock In" });
        trow.Cells.Add(new TableCell() { Text = "Clock Out" });
        trow.Cells.Add(new TableCell() { Text = "UT" });
        trow.Cells.Add(new TableCell() { Text = "OT" });
        trow.Cells.Add(new TableCell() { Text = "Breaks" });
        trow.Cells.Add(new TableCell() { Text = "Worked" });
        trow.Cells.Add(new TableCell() { Text = "" });
        tblHistory.Rows.Add(trow);
        #endregion
        int idS = 0;
        List<Absence> _absences = Absence.GetAbsences(Convert.ToInt32(ddlUsers.SelectedValue)).Where(u => u.Active == true).ToList();
        List<ExDay> _exDays = ExDay.GetExDays(Convert.ToInt32(ddlUsers.SelectedValue)).Where(u => u.Active == true).ToList();
        lblTotalAbsences.Text = _absences.Where(u => u.CrtDate.Month == Convert.ToInt32(ddlMonth1.SelectedValue) && u.CrtDate.Year == Convert.ToInt32(ddlYear.SelectedValue)).ToList().Count().ToString();
        int iTotalNE = 0;
        Boolean lastSignIn = false;
        GenerateStatistics(records);
        #region Generate Records
        foreach (RecordModel record in records) {
            if (SP.IsWorkerActive(Convert.ToInt32(UserId))) {
                if (DateTime.Now.Date < record.Date.Date && _absences.Where(u => u.CrtDate.Date == record.Date && u.Active == true).ToList().Count <= 0) {
                    continue;
                }
            }
            else {
                if (WorkerLastSignIn < record.Date.Date && _absences.Where(u => u.CrtDate.Date == record.Date && u.Active == true).ToList().Count <= 0) {
                    continue;
                }
            }

            if (WorkerFirstSignIn > record.Date.Date) {
                continue;
            }

            TableRow tRowRecord = new TableRow();
            string todaysDate = objATZ.GetDate(record.Date.ToString(), HttpContext.Current.User.Identity.Name);

            tRowRecord.Cells.Add(new TableCell() { Text = todaysDate, RowSpan = record.Records.Count });

            #region Handle weekends, holidays etc and set trow class accordingly for first record
            TableCell tcellhw = new TableCell();
            if (record.DayType.HasValue) {
                if (record.Records.Count <= 0) {
                    //imgMDetails.Visible = false;
                    //subRecordsClockIn.Visible = false;
                }
                if (string.IsNullOrEmpty(record.DayTitle)) {
                    tcellhw.Text = "Holiday";
                    if (record.TotalOverTime.TotalMinutes != 0) {
                        tcellhw.Text += " - Overtime: " + UtilityMethods.getFormatedTimeByMinutes(record.TotalOverTime.TotalMinutes.ToInt32());
                    }
                }
                else {
                    tcellhw.Text = record.DayTitle;
                    if (record.TotalOverTime.TotalMinutes != 0) {
                        tcellhw.Text += " - Overtime: " + UtilityMethods.getFormatedTimeByMinutes(record.TotalOverTime.TotalMinutes.ToInt32());
                    }
                }
                tcellhw.Font.Bold = true;
                tcellhw.ColumnSpan = 7;
                //tRowRecord.Cells[0].RowSpan = record.Records.Count + 1;
                tRowRecord.Cells.Add(tcellhw);
                tblHistory.Rows.Add(tRowRecord);

                //dtitle.Visible = true;
                if (record.DayType.Value == 0) {
                    tRowRecord.Attributes.Add("class", "greenRow");
                }
                //rContainer.Attributes.Add("class", "SatSunHolidays");
                else {
                    //rContainer.Attributes.Add("class", "NormalHoliday");
                    tRowRecord.Attributes.Add("class", "yellowRow");
                    //trDayOff.Visible = true;
                }
                tRowRecord = new TableRow();
            }
            else if (record.Records.Count <= 0) {
                //Absence absence = new Absence();                                        
                if (_absences.Where(u => u.CrtDate.Date == record.Date && u.Active == true).ToList().Count <= 0) {
                    Label lblText = new Label();
                    lblText.ID = "lblText" + ++idS;
                    lblText.CssClass = "lblText";
                    lblText.Text = "No Entry - ";
                    ++iTotalNE;
                    lblTotalNE.Text = iTotalNE.ToString();
                    LinkButton lnkMarkAbsence = new LinkButton();
                    lnkMarkAbsence.ID = "lnkMarkAbsence" + idS;
                    lnkMarkAbsence.CssClass = "lnkMarkAbsence";
                    lnkMarkAbsence.CommandName = "lnkMarkAbsence";
                    lnkMarkAbsence.Text = "Mark as absent";
                    lnkMarkAbsence.CommandArgument = record.Date.ToString();
                    lnkMarkAbsence.Attributes.Add("data-id", "0");
                    lnkMarkAbsence.Attributes.Add("data-UserId", ddlUsers.SelectedValue.ToString());
                    lnkMarkAbsence.Attributes.Add("data-date", record.Date.ToString());
                    lnkMarkAbsence.Attributes.Add("data-active", "true");
                    lnkMarkAbsence.PostBackUrl = "";
                    tcellhw.Controls.Add(lblText);
                    tcellhw.Controls.Add(lnkMarkAbsence);
                }
                else {
                    Label lblText = new Label();
                    lblText.ID = "lblText" + ++idS;
                    lblText.CssClass = "lblText";
                    lblText.Text = "Absent - ";
                    tcellhw.Controls.Add(lblText);
                    LinkButton lnkMarkAbsence = new LinkButton();
                    lnkMarkAbsence.ID = "lnkMarkPresent" + idS;
                    lnkMarkAbsence.CssClass = "lnkMarkAbsence";
                    lnkMarkAbsence.CommandName = "lnkMarkAbsence";
                    lnkMarkAbsence.Text = "Remove absence";
                    lnkMarkAbsence.CommandArgument = record.Date.ToString();
                    Absence abs = _absences.SingleOrDefault(u => u.CrtDate.Date == record.Date && u.Active == true);
                    lnkMarkAbsence.Attributes.Add("data-id", abs.AbsLogID.ToString());
                    lnkMarkAbsence.Attributes.Add("data-UserId", abs.UserID.ToString());
                    lnkMarkAbsence.Attributes.Add("data-date", abs.CrtDate.ToString());
                    lnkMarkAbsence.Attributes.Add("data-active", "False");
                    lnkMarkAbsence.PostBackUrl = "";
                    Image imgCommentDetail = new Image();
                    //tRowRecord.Cells.Add(new TableCell() { Text = "<img src=\"images/Controls/icon_info.png\" alt=\"\" title=\"" + details + "\" Class=\"tooltip\" ToolTip=\"" + details + "\" />", RowSpan = record.Records.Count });
                    imgCommentDetail.ImageUrl = "../images/Controls/icon_info.png";
                    imgCommentDetail.CssClass = "tooltip";
                    imgCommentDetail.Attributes.Add("title", abs.Comment);
                    imgCommentDetail.ToolTip = abs.Comment;
                    imgCommentDetail.Style.Add("float", "right");
                    tcellhw.Controls.Add(lblText);
                    if (!isWorker) {
                        tcellhw.Controls.Add(lnkMarkAbsence);
                    }
                    tcellhw.Controls.Add(imgCommentDetail);

                }

                DateTime workerRequestDate = SP.GetWorkerFirstSignInn(ddlUsers.SelectedValue.ToString());
                tRowRecord.Attributes.Add("class", "pinkRow");
                tcellhw.Font.Bold = true;
                tcellhw.ColumnSpan = 7;
                //tRowRecord.Cells[0].RowSpan = record.Records.Count + 1;
                tRowRecord.Cells.Add(tcellhw);
                tblHistory.Rows.Add(tRowRecord);
                tRowRecord = new TableRow();
            }
            else {
                if (_absences.Where(u => u.CrtDate.Date == record.Date && u.Active == true && u.UserID == Convert.ToInt32(ddlUsers.SelectedValue)).ToList().Count > 0) {
                    Absence abs = _absences.SingleOrDefault(u => u.CrtDate.Date == record.Date && u.Active == true);
                    Label lblText = new Label();
                    lblText.ID = "lblText" + ++idS;
                    lblText.CssClass = "lblText";
                    lblText.Text = "Absent - ";
                    LinkButton lnkMarkAbsence = new LinkButton();
                    lnkMarkAbsence.ID = "lnkMarkPresent" + idS;
                    lnkMarkAbsence.CssClass = "lnkMarkAbsence";
                    lnkMarkAbsence.CommandName = "lnkMarkAbsence";
                    lnkMarkAbsence.Text = "Remove absence";
                    lnkMarkAbsence.CommandArgument = record.Date.ToString();
                    lnkMarkAbsence.Attributes.Add("data-id", abs.AbsLogID.ToString());
                    lnkMarkAbsence.Attributes.Add("data-UserId", abs.UserID.ToString().ToString());
                    lnkMarkAbsence.Attributes.Add("data-date", abs.CrtDate.ToString());
                    lnkMarkAbsence.Attributes.Add("data-active", "False");
                    lnkMarkAbsence.PostBackUrl = "";
                    tcellhw.Controls.Add(lblText);
                    if (!isWorker) {
                        tcellhw.Controls.Add(lnkMarkAbsence);
                    }



                    Image imgCommentDetail = new Image();
                    //tRowRecord.Cells.Add(new TableCell() { Text = "<img src=\"images/Controls/icon_info.png\" alt=\"\" title=\"" + details + "\" Class=\"tooltip\" ToolTip=\"" + details + "\" />", RowSpan = record.Records.Count });
                    imgCommentDetail.ImageUrl = "~/images/Controls/icon_info.png";
                    imgCommentDetail.CssClass = "tooltip";
                    imgCommentDetail.Attributes.Add("title", abs.Comment);
                    imgCommentDetail.ToolTip = abs.Comment;
                    imgCommentDetail.Style.Add("float", "right");

                    tRowRecord.Attributes.Add("class", "pinkRow");
                    tcellhw.Font.Bold = true;
                    tcellhw.ColumnSpan = 7;

                    //tRowRecord.Cells[0].RowSpan = record.Records.Count + 1;
                    tRowRecord.Cells.Add(tcellhw);
                    tcellhw.Controls.Add(imgCommentDetail);

                    tblHistory.Rows.Add(tRowRecord);
                    tRowRecord = new TableRow();
                }

            }
            #endregion

            bool overtimeonly = false;
            if (record.Records.Count > 0) {
                if (record.DayType.HasValue) {
                    if (record.DayType.Value == 0) {
                        tRowRecord.Attributes.Add("class", "greenRow");
                        overtimeonly = true;
                    }
                    else {
                        tRowRecord.Attributes.Add("class", "yellowRow");
                        overtimeonly = true;
                    }
                }

                if (!overtimeonly) {
                    if ((String.IsNullOrEmpty(record.Records[0].LogoutTime) || record.Records[0].LogoutTime == "") && record.Records.Count < 1) {
                        tRowRecord.CssClass = "hide";
                    }

                    // Add Clock-In time
                    if (!String.IsNullOrEmpty(record.Records[0].LoginTime) || record.Records[0].LoginTime != "") {
                        if (record.Date.Date == Convert.ToDateTime(record.Records[0].LoginTime).Date) {
                            tRowRecord.Cells.Add(new TableCell() { Text = objATZ.GetTime(record.Records[0].LoginTime, HttpContext.Current.User.Identity.Name) });
                        }
                        else {
                            tRowRecord.Cells.Add(new TableCell() { Text = objATZ.GetTime(record.Records[0].LoginTime, HttpContext.Current.User.Identity.Name) + " - " + Convert.ToDateTime(record.Records[0].LoginTime).Date.ToString("MMM dd yyyy") });
                        }
                    }

                    // Add Clock-Out time
                    Label lblTotalTime = new Label();
                    if (!String.IsNullOrEmpty(record.Records[0].LogoutTime) || record.Records[0].LogoutTime != "") {
                        #region Delete Hours for Record[0]
                        Image imgDeleteHours = new Image();
                        imgDeleteHours.AlternateText = " - Delete Hours";
                        imgDeleteHours.ImageUrl = "~/images/Controls/Minusicon.png";

                        imgDeleteHours.ID = "imgDeleteHours" + ++idS + "" + record.Records[0].Id;


                        imgDeleteHours.CssClass = "imgDeleteHours tooltip";
                        imgDeleteHours.ToolTip = "Use this option to delete time from your recorded history in order to fix any excess time you recorded by mistake.";
                        imgDeleteHours.Attributes.Add("Title", "Delete Hours");

                        string intime = objATZ.GetTime(record.Records[0].LoginTime, ddlUsers.SelectedValue.ToString());
                        intime = ((!IsSimpleClock) ? Convert.ToDateTime(intime).ToString("HH:mm") : intime);
                        imgDeleteHours.Attributes.Add("data-frmTime", intime);

                        String outtime = objATZ.GetTime(record.Records[0].LogoutTime, ddlUsers.SelectedValue.ToString());
                        outtime = ((!IsSimpleClock) ? Convert.ToDateTime(outtime).ToString("HH:mm") : outtime);

                        string inDate = objATZ.GetDate(record.Records[0].LoginTime, HttpContext.Current.User.Identity.Name);
                        if (inDate.Contains("Today")) {
                            inDate = DateTime.Now.ToString();
                        }
                        string outDate = objATZ.GetDate(record.Records[0].LogoutTime, HttpContext.Current.User.Identity.Name);
                        if (outDate.Contains("Today")) {
                            outDate = DateTime.Now.ToString();
                        }
                        imgDeleteHours.Attributes.Add("data-toTime", outtime);

                        imgDeleteHours.Attributes.Add("data-day", record.Date.ToString("ddd, MMM dd yyyy") + " from " + intime + " to " + outtime);
                        imgDeleteHours.Attributes.Add("data-recordID", record.Records[0].Id.ToString());
                        imgDeleteHours.Style.Add("float", "right");
                        #endregion

                        #region Edit Hours Record[0] -- pencil.png

                        Image imgEditHours = new Image();
                        imgEditHours.AlternateText = " - Edit Hours";
                        imgEditHours.ImageUrl = "~/images/Controls/pencil.png";
                        imgEditHours.ID = "imgEditHours" + ++idS + "" + record.Records[0].Id;
                        imgEditHours.CssClass = "imgEditHours tooltip";
                        imgEditHours.ToolTip = "Use this option to edit time. Admin only";
                        imgEditHours.Attributes.Add("Title", "Edit Time");
                        imgEditHours.Attributes.Add("data-frmTime", intime);
                        imgEditHours.Attributes.Add("data-toTime", outtime);
                        imgEditHours.Attributes.Add("data-frmdate", Convert.ToDateTime(inDate).ToShortDateString() + " " + intime);
                        imgEditHours.Attributes.Add("data-todate", Convert.ToDateTime(outDate).ToShortDateString() + " " + outtime);
                        imgEditHours.Attributes.Add("data-recordID", record.Records[0].Id.ToString());
                        imgEditHours.Style.Add("width", "12px");
                        imgEditHours.Style.Add("margin-left", "15px");
                        imgEditHours.Style.Add("cursor", "pointer");
                        imgEditHours.Style.Add("float", "right");
                        #endregion

                        TableCell tblCellLnkDelHours = new TableCell();
                        Label lblLogout = new Label();
                        if (record.Date.Date == Convert.ToDateTime(record.Records[0].LogoutTime).Date) {
                            lblLogout.Text = objATZ.GetTime(record.Records[0].LogoutTime, HttpContext.Current.User.Identity.Name);
                            tblCellLnkDelHours.Controls.Add(lblLogout);
                            //tblCellLnkDelHours.Controls.Add(imgDeleteHours);
                            if (!isWorker) {
                                tblCellLnkDelHours.Controls.Add(imgEditHours);
                            }
                            tRowRecord.Cells.Add(tblCellLnkDelHours);
                        }
                        else {
                            lblLogout.Text = objATZ.GetTime(record.Records[0].LogoutTime, HttpContext.Current.User.Identity.Name) + " - " + Convert.ToDateTime(record.Records[0].LogoutTime).Date.ToString("MMM dd yyyy");
                            tblCellLnkDelHours.Controls.Add(lblLogout);
                            //tblCellLnkDelHours.Controls.Add(imgDeleteHours);
                            if (!isWorker) {
                                tblCellLnkDelHours.Controls.Add(imgEditHours);
                            }
                            tRowRecord.Cells.Add(tblCellLnkDelHours);
                        }
                    }
                    else {
                        //lnkDeleteHours.CssClass = "imgDeleteHours";
                        //lnkDeleteHours.ToolTip = "Use this option to delete time from your recorded history in order to fix any excess time you recorded by mistake.";

                        //string time = objATZ.GetTime(record.Records[0].LoginTime, UserId.toString());
                        //time = ((!IsSimpleClock) ? Convert.ToDateTime(time).ToString("HH:mm") : time);
                        //lnkDeleteHours.Attributes.Add("data-frmTime", time);

                        //time = objATZ.GetTime(DateTime.Now.ToString(), UserId.toString());
                        //time = ((!IsSimpleClock) ? Convert.ToDateTime(time).ToString("HH:mm") : time);
                        //lnkDeleteHours.Attributes.Add("data-toTime", time);
                        //lnkDeleteHours.Attributes.Add("data-recordID", record.Records[0].Id.ToString());

                        //imgDeleteHours.Attributes.Add("data-day", record.Date.ToString("ddd, MM dd yyyy") + " from " + intime + " to " + outtime);

                        string intime = objATZ.GetTime(record.Records[0].LoginTime, UserId.ToString());
                        intime = ((!IsSimpleClock) ? Convert.ToDateTime(intime).ToString("HH:mm") : intime);
                        //lnkDeleteHours.Attributes.Add("data-frmTime", intime);

                        String outtime = objATZ.GetTime(record.Records[0].LogoutTime, UserId.ToString());
                        outtime = ((!IsSimpleClock) ? Convert.ToDateTime(outtime).ToString("HH:mm") : outtime);

                        string inDate = objATZ.GetDate(record.Records[0].LoginTime, HttpContext.Current.User.Identity.Name);
                        if (inDate.Contains("Today")) {
                            inDate = DateTime.Now.ToString();
                        }
                        string outDate = objATZ.GetDate(record.Records[0].LogoutTime, HttpContext.Current.User.Identity.Name);
                        if (outDate.Contains("Today")) {
                            outDate = DateTime.Now.ToString();
                        }
                        //lnkDeleteHours.Attributes.Add("data-toTime", outtime);

                        if (String.IsNullOrWhiteSpace(outtime) || outtime.Contains("&nbsp;")) {
                            //lnkDeleteHours.Attributes.Add("data-day", record.Date.ToString("ddd, MMM dd yyyy") + " from " + intime + " to till now");
                        }
                        else {
                            //lnkDeleteHours.Attributes.Add("data-day", record.Date.ToString("ddd, MMM dd yyyy") + " from " + intime + " to " + outtime);
                        }


                        //lnkDeleteHours.Attributes.Add("data-title", "");
                        TableCell tblCellLnkDelHours = new TableCell();
                        Label lblLogout = new Label();
                        tRowRecord.Cells.Add(new TableCell() { Text = "..." });

                        lastSignIn = true;
                        lblTotalTime.Attributes.Add("data-incdate", UtilityMethods.getClientDateTime(Convert.ToDateTime(record.Records[0].LoginTime)).ToString());
                        lblTotalTime.ID = "lblTotalTime" + idS + "" + record.Records[0];
                    }

                    // Adding Undertime and Undertime in grid
                    TableCell tbCell = new TableCell();
                    Label lblUndertime = new Label();
                    lblUndertime.ID = "lblUndertime" + idS + "" + record.Records[0];
                    lblUndertime.Text = UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(record.TotalUnderTimeWithoutLate.TotalMinutes)).ToString();
                    ImageButton lnkEditUndertime = new ImageButton();
                    lnkEditUndertime.ID = "lnkEditUndertime" + idS + "" + record.Records[0];
                    lnkEditUndertime.ImageUrl = "../images/Controls/pencil.png";
                    lnkEditUndertime.ToolTip = "Use this option to edit Under-time. Admin only";
                    lnkEditUndertime.Attributes.Add("Title", "Edit Time");
                    lnkEditUndertime.Style.Add("width", "12px");
                    lnkEditUndertime.Style.Add("margin-left", "5px");
                    lnkEditUndertime.Style.Add("cursor", "pointer");
                    lnkEditUndertime.Style.Add("float", "right");
                    lnkEditUndertime.CssClass = "raisesevent lnkEditUndertime_";
                    //lnkEditUndertime.Click += lnkEditUndertime_Click;
                    lnkEditUndertime.Attributes["data-recid"] = record.Records[0].Id.ToString();
                    lnkEditUndertime.CommandArgument = record.Records[0].Id.ToString();
                    tbCell.Controls.Add(lblUndertime);
                    if (Convert.ToInt32(record.TotalUnderTime.TotalMinutes) == 0) { lnkEditUndertime.CssClass += " lnkAddUT_"; lnkEditUndertime.Attributes["data-date"] = record.Date.ToShortDateString(); }
                    tbCell.Controls.Add(lnkEditUndertime);

                    tRowRecord.Cells.Add(tbCell);
                    tbCell = new TableCell();
                    lblUndertime = new Label();
                    lblUndertime.ID = "lblOvertime" + idS + "" + record.Records[0];
                    lblUndertime.Text = UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(record.TotalOverTime.TotalMinutes)).ToString();
                    lnkEditUndertime = new ImageButton();
                    lnkEditUndertime.ID = "lnkEditOvertime" + idS + "" + record.Records[0];
                    lnkEditUndertime.ImageUrl = "../images/Controls/pencil.png";
                    lnkEditUndertime.ToolTip = "Use this option to edit Over-time. Admin only";
                    lnkEditUndertime.Attributes.Add("Title", "Edit Time");
                    lnkEditUndertime.Style.Add("width", "12px");
                    lnkEditUndertime.Style.Add("margin-left", "5px");
                    lnkEditUndertime.Style.Add("cursor", "pointer");
                    lnkEditUndertime.Style.Add("float", "right");
                    lnkEditUndertime.CssClass = "raisesevent lnkEditOvertime_";
                    lnkEditUndertime.Attributes["data-recid"] = record.Records[0].Id.ToString();
                    lnkEditUndertime.CommandArgument = record.Records[0].Id.ToString();
                    tbCell.Controls.Add(lblUndertime);
                    if (Convert.ToInt32(record.TotalOverTime.TotalMinutes) == 0) { lnkEditUndertime.CssClass += " lnkAddOT_"; lnkEditUndertime.Attributes["data-date"] = record.Date.ToShortDateString(); }
                    tbCell.Controls.Add(lnkEditUndertime);

                    //tbCell.Text = UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(record.TotalOverTime.TotalMinutes)).ToString();
                    tRowRecord.Cells.Add(tbCell);

                    // Add Total break time                    
                    TableCell cellTotalBreak = new TableCell();
                    cellTotalBreak.Text = record.TotalBreak;
                    cellTotalBreak.RowSpan = record.Records.Count;
                    tRowRecord.Cells.Add(cellTotalBreak);


                    // Add Totaltime with breaks
                    lblTotalTime.Text = record.TotalWorkedHours;
                    TableCell cellTotalTime = new TableCell();
                    cellTotalTime.Controls.Add(lblTotalTime);
                    cellTotalTime.RowSpan = record.Records.Count;
                    tRowRecord.Cells.Add(cellTotalTime);

                    #region --- Create detail information ---
                    String details = "<table id='tblDetail'><tr>";
                    details += "<td>Clock-In Location:</td>";
                    int breakcolspanIn = 0;
                    int breakcolspanOut = 0;
                    // Check if location in is same or not
                    bool sameLocationIn = true;
                    bool sameLocationOut = true;
                    foreach (SubRecordModel subRecord in record.Records) {
                        if (subRecord.LoginStatus != record.Records[0].LoginStatus) {
                            sameLocationIn = false;
                        }
                        if (subRecord.LogoutStatus != record.Records[0].LogoutStatus) {
                            sameLocationOut = false;
                        }
                    }

                    bool execIn = true;
                    foreach (SubRecordModel subRecord in record.Records) {
                        if (sameLocationIn) {
                            if (execIn) {
                                breakcolspanIn++;
                                details += "<td>" + subRecord.InLocation;
                                if (!String.IsNullOrEmpty(subRecord.InLocation)) {
                                    details += ((subRecord.LoginStatus == "Notverified") ? "<img src='images/Controls/cross.png' style='margin-left:4px' alt='Notverified' width='13px'/>" : "<img src='images/Controls/tick.png' width='13px' alt='Verified' class='tooltip' style='margin-left:4px' />") + "</td>";
                                }
                                execIn = false;
                            }
                        }
                        else {
                            breakcolspanIn++;
                            details += "<td>" + subRecord.InLocation;
                            if (!String.IsNullOrEmpty(subRecord.InLocation)) {
                                details += ((subRecord.LoginStatus == "Notverified") ? "<img src='images/Controls/cross.png' style='margin-left:4px' alt='Notverified' width='13px'/>" : "<img src='images/Controls/tick.png' width='13px' alt='Verified' class='tooltip' style='margin-left:4px' />") + "</td>";
                            }
                        }
                    }
                    details += "</tr><tr><td>Clock-Out Location:</td>";
                    bool execOut = true;
                    foreach (SubRecordModel subRecord in record.Records) {
                        if (sameLocationOut) {
                            if (execOut) {
                                breakcolspanOut++;
                                details += "<td>" + subRecord.OutLocation;
                                if (!String.IsNullOrEmpty(subRecord.OutLocation)) {
                                    details += ((subRecord.LogoutStatus == "Notverified") ? "<img src='images/Controls/cross.png' style='margin-left:4px' alt='Notverified' width='13px'/>" : "<img src='images/Controls/tick.png' width='13px' alt='Verified' class='tooltip' style='margin-left:4px' />") + "</td>";
                                }
                                else {
                                    details += "... </td>";
                                }
                                execOut = false;
                            }
                        }
                        else {
                            breakcolspanOut++;
                            details += "<td>" + subRecord.OutLocation;
                            if (!String.IsNullOrEmpty(subRecord.OutLocation)) {
                                details += ((subRecord.LogoutStatus == "Notverified") ? "<img src='images/Controls/cross.png' style='margin-left:4px' alt='Notverified' width='13px'/>" : "<img src='images/Controls/tick.png' width='13px' alt='Verified' class='tooltip' style='margin-left:4px' />") + "</td>";
                            }
                        }
                    }
                    details += "</tr>";

                    foreach (BreakModel breakModel in record.Breaks) {
                        details += string.Format("<tr><td>Break:</td><td colspan='" + ((breakcolspanIn > breakcolspanOut) ? breakcolspanIn + 1 : breakcolspanOut + 1) + "'>From: {0} To: {1}", Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + breakModel.BreakStartTime.ToString()).ToShortTimeString(), Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + breakModel.BreakEndTime.ToString()).ToShortTimeString()) + "</td>";
                    }
                    details += "</tr><tr><td>Total Time:</td><td colspan='" + ((breakcolspanIn > breakcolspanOut) ? breakcolspanIn + 1 : breakcolspanOut + 1) + "'>" + record.TotalTime + "</td></table>";
                    // Info Image
                    tRowRecord.Cells.Add(new TableCell() { Text = "<img src=\"images/Controls/icon_info.png\" alt=\"\" title=\"" + details + "\" Class=\"tooltip\" ToolTip=\"" + details + "\" />", RowSpan = record.Records.Count });

                    #region --- Mark as Partial ---
                    TableCell tbcell = new TableCell();
                    if (_exDays.Where(u => u.CrtDate.Date.Date == record.Date.Date && u.Active == true).ToList().Count > 0) {
                        ExDay exDay = new ExDay();
                        try {
                            exDay = _exDays.Last();
                        }
                        catch (Exception) {
                        }

                        //Label lblText = new Label();
                        //lblText.ID = "lblText" + ++idS;
                        //lblText.CssClass = "lblText";
                        //lblText.Text = "Partial Day - ";
                        Image lnkMarkNPW = new Image();
                        lnkMarkNPW.ID = "lnkMarkNPW" + idS;
                        lnkMarkNPW.CssClass = "lnkMarkNPW tooltip";
                        lnkMarkNPW.ImageUrl = "~/images/controls/piechart.png";
                        lnkMarkNPW.Attributes.Add("tooltip", "Mark as not partial working");
                        lnkMarkNPW.Attributes.Add("Title", "Mark as not partial working");
                        //lnkMarkNPW.Attributes.Add("class", "tooltip");
                        lnkMarkNPW.Style.Add("width", "16px");
                        lnkMarkNPW.Style.Add("cursor", "pointer");
                        lnkMarkNPW.Attributes.Add("data-id", exDay.ExDayID.ToString());
                        lnkMarkNPW.Attributes.Add("data-ex-type", Convert.ToInt32(exDay.ExDayTypeID).ToString());
                        lnkMarkNPW.Attributes.Add("data-UserId", exDay.UserID.ToString().ToString());
                        lnkMarkNPW.Attributes.Add("data-date", exDay.CrtDate.ToString());
                        lnkMarkNPW.Attributes.Add("data-active", "False");
                        //tbcell.Controls.Add(lblText);
                        //tbcell.Controls.Add(lnkMarkNPW);

                        //Image imgCommentDetail = new Image();
                        ////tRowRecord.Cells.Add(new TableCell() { Text = "<img src=\"images/Controls/icon_info.png\" alt=\"\" title=\"" + details + "\" Class=\"tooltip\" ToolTip=\"" + details + "\" />", RowSpan = record.Records.Count });
                        //imgCommentDetail.ImageUrl = "images/Controls/icon_info.png";
                        //imgCommentDetail.CssClass = "tooltip";
                        //imgCommentDetail.Attributes.Add("title", exDay.Comment);
                        //imgCommentDetail.ToolTip = exDay.Comment;
                        //imgCommentDetail.Style.Add("float", "right");

                        tRowRecord.Attributes.Add("class", "eggshellRow");
                        tbcell.Font.Bold = true;
                        tRowRecord.Cells[0].RowSpan = record.Records.Count;
                        tbcell.RowSpan = record.Records.Count;
                        //tRowRecord.Cells.Add(tbcell);
                        //tcellhw.Controls.Add(imgCommentDetail);                        
                    }
                    else {
                        Label lblText = new Label();
                        lblText.ID = "lblText" + ++idS;
                        lblText.CssClass = "lblText";
                        lblText.Text = "";
                        Image lnkMarkNPW = new Image();
                        lnkMarkNPW.ID = "lnkMarkNPW" + idS;
                        lnkMarkNPW.CssClass = "lnkMarkNPW tooltip";
                        lnkMarkNPW.ImageUrl = "~/images/controls/piechart.png";
                        lnkMarkNPW.Attributes.Add("tooltip", "Mark as partial working");
                        lnkMarkNPW.Attributes.Add("Title", "Mark as partial working");
                        //lnkMarkNPW.Attributes.Add("class", "tooltip");
                        lnkMarkNPW.Style.Add("width", "16px");
                        lnkMarkNPW.Style.Add("cursor", "pointer");
                        lnkMarkNPW.Attributes.Add("data-id", "0");
                        lnkMarkNPW.Attributes.Add("data-ex-type", "1");
                        lnkMarkNPW.Attributes.Add("data-UserId", ddlUsers.SelectedValue.ToString());
                        lnkMarkNPW.Attributes.Add("data-date", record.Date.ToString());
                        lnkMarkNPW.Attributes.Add("data-active", "True");
                        tbcell.Controls.Add(lblText);
                        //tbcell.Controls.Add(lnkMarkNPW);
                        tbcell.RowSpan = record.Records.Count;
                        //tRowRecord.Cells.Add(tbcell);
                    }
                    #endregion

                    tblHistory.Rows.Add(tRowRecord);
                    #endregion



                    #region --- Create SubRecords ---
                    for (int i = 1; i < record.Records.Count; i++) {
                        TableRow subRecord = new TableRow();
                        if (record.DayType.HasValue) {
                            if (record.DayType.Value == 0) {
                                subRecord.Attributes.Add("class", "greenRow");
                            }
                            else {
                                subRecord.Attributes.Add("class", "yellowRow");
                            }
                        }
                        TableCell cellClockInTime = new TableCell();
                        TableCell cellClockOutTime = new TableCell();
                        if (!String.IsNullOrEmpty(record.Records[i].LoginTime) || record.Records[i].LoginTime != "") {
                            if (record.Date.Date == Convert.ToDateTime(record.Records[i].LoginTime).Date) {
                                cellClockInTime.Text = objATZ.GetTime(record.Records[i].LoginTime, HttpContext.Current.User.Identity.Name);
                            }
                            else {
                                cellClockInTime.Text = objATZ.GetTime(record.Records[i].LoginTime, HttpContext.Current.User.Identity.Name) + "-" + Convert.ToDateTime(record.Records[i].LoginTime).Date.ToShortDateString();
                            }
                        }
                        if (!String.IsNullOrEmpty(record.Records[i].LogoutTime) || record.Records[i].LogoutTime != "") {
                            #region Delete Hours for Record[i]
                            Image imgDeleteHours2 = new Image();
                            imgDeleteHours2.AlternateText = " - Delete Hours";
                            imgDeleteHours2.ImageUrl = "../images/Controls/Minusicon.png";
                            imgDeleteHours2.ID = "imgDeleteHours2" + i + "" + record.Records[i].Id;
                            imgDeleteHours2.CssClass = "imgDeleteHours tooltip";
                            imgDeleteHours2.ToolTip = "Use this option to delete time from your recorded history in order to fix any excess time you recorded by mistake.";
                            imgDeleteHours2.Attributes.Add("Title", "Delete Hours");

                            string intime = objATZ.GetTime(record.Records[i].LoginTime, ddlUsers.SelectedValue.ToString());
                            intime = ((!IsSimpleClock) ? Convert.ToDateTime(intime).ToString("HH:mm") : intime);
                            imgDeleteHours2.Attributes.Add("data-frmTime", intime);

                            String outtime = objATZ.GetTime(record.Records[i].LogoutTime, ddlUsers.SelectedValue.ToString());
                            outtime = ((!IsSimpleClock) ? Convert.ToDateTime(outtime).ToString("HH:mm") : outtime);

                            string inDate = objATZ.GetDate(record.Records[i].LoginTime, HttpContext.Current.User.Identity.Name);
                            if (inDate.Contains("Today")) {
                                inDate = DateTime.Now.ToString();
                            }
                            string outDate = objATZ.GetDate(record.Records[i].LogoutTime, HttpContext.Current.User.Identity.Name);
                            if (outDate.Contains("Today")) {
                                outDate = DateTime.Now.ToString();
                            }
                            imgDeleteHours2.Attributes.Add("data-toTime", outtime);

                            imgDeleteHours2.Attributes.Add("data-day", record.Date.ToString("ddd, MMM dd yyyy") + " from " + intime + " to " + outtime);
                            imgDeleteHours2.Attributes.Add("data-recordID", record.Records[i].Id.ToString());
                            imgDeleteHours2.Style.Add("float", "right");
                            Label lblLogout = new Label();
                            #endregion

                            #region Edit Hours Record[i] -- pencil.png
                            Image imgEditHours2 = new Image();
                            imgEditHours2.AlternateText = " - Edit Hours";
                            imgEditHours2.ImageUrl = "~/images/Controls/pencil.png";
                            imgEditHours2.ID = "imgEditHours2" + i + "" + record.Records[i].Id;
                            imgEditHours2.CssClass = "imgEditHours tooltip";
                            imgEditHours2.ToolTip = "Use this option to edit time. Admin only";
                            imgEditHours2.Attributes.Add("Title", "Edit Time");
                            imgEditHours2.Attributes.Add("data-frmTime", intime);
                            imgEditHours2.Attributes.Add("data-toTime", outtime);
                            imgEditHours2.Attributes.Add("data-frmDate", inDate);
                            imgEditHours2.Attributes.Add("data-toDate", outDate);
                            imgEditHours2.Attributes.Add("data-recordID", record.Records[i].Id.ToString());
                            imgEditHours2.Style.Add("width", "12px");
                            imgEditHours2.Style.Add("cursor", "pointer");
                            imgEditHours2.Style.Add("float", "right");
                            imgEditHours2.Style.Add("margin-left", "15px");
                            #endregion

                            if (record.Date.Date == Convert.ToDateTime(record.Records[i].LogoutTime).Date) {
                                lblLogout.Text = objATZ.GetTime(record.Records[i].LogoutTime, HttpContext.Current.User.Identity.Name);
                                cellClockOutTime.Controls.Add(lblLogout);
                                //cellClockOutTime.Controls.Add(imgDeleteHours2);
                                if (!isWorker) {
                                    cellClockOutTime.Controls.Add(imgEditHours2);
                                }
                                tRowRecord.Cells.Add(cellClockOutTime);
                            }
                            else {
                                lblLogout.Text = objATZ.GetTime(record.Records[i].LogoutTime, HttpContext.Current.User.Identity.Name) + " - " + Convert.ToDateTime(record.Records[i].LogoutTime).Date.ToString("MMM dd yyyy");
                                cellClockOutTime.Controls.Add(lblLogout);
                                //cellClockOutTime.Controls.Add(imgDeleteHours2);
                                if (!isWorker) {
                                    cellClockOutTime.Controls.Add(imgEditHours2);
                                }
                                tRowRecord.Cells.Add(cellClockOutTime);
                            }
                        }
                        else {
                            lastSignIn = true;
                        }

                        subRecord.Cells.Add(cellClockInTime);
                        subRecord.Cells.Add(cellClockOutTime);
                        tblHistory.Rows.Add(subRecord);
                    }
                    #endregion
                }
                else {
                    tblHistory.Rows.Add(tRowRecord);
                }
            }
            else {

                if (record.DayType.HasValue) {
                    if (record.Records.Count <= 0) {
                        //imgMDetails.Visible = false;
                        //subRecordsClockIn.Visible = false;
                    }
                    if (string.IsNullOrEmpty(record.DayTitle)) {
                        tcellhw.Text = "Holiday";
                    }
                    else {
                        tcellhw.Text = record.DayTitle;
                    }
                }
                else {
                    //imgMDetails.Visible = true;
                    //subRecordsClockIn.Visible = true;
                }
            }

        }
        #endregion
    }

    private static void CalculateTime(ref int bTotShouldHrs, ref int bTotShouldMins, string[] workedhours)
    {
        if (workedhours.Count() > 3) {
            bTotShouldHrs += Convert.ToInt32(workedhours[0]);
            bTotShouldMins += Convert.ToInt32(workedhours[2]);
            if (bTotShouldMins >= 60) {
                bTotShouldHrs += Convert.ToInt32((bTotShouldMins / 60));
                bTotShouldMins -= 60;
            }
        }
        else {
            if (Convert.ToInt32(workedhours[0]) > 0) {
                bTotShouldMins += Convert.ToInt32(workedhours[0]);
                if (bTotShouldMins >= 60) {
                    bTotShouldHrs += Convert.ToInt32((bTotShouldMins / 60));
                    bTotShouldMins -= 60;
                }
            }
        }
    }

    //private void HoursCalculation(int totHrs, int totMins, int wtotHrs, int wtotMins, int btotHrs, int btotMins, TimeSpan bTotShouldHrs, int bTotShouldMins)
    //{
    //    #region Estimated total hours calculations
    //    List<Absence> absences = Absence.GetAbsences(Convert.ToInt32(ddlUsers.SelectedValue)).Where(u => u.Active == true).ToList();
    //    Int32 daysInMonth = new DateTime(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(ddlMonth1.SelectedValue), 1).AddMonths(1).AddDays(-1).Day;
    //    DateTime firstSignIn = SP.GetWorkerFirstSignInn(ddlUsers.SelectedValue.ToString());
    //    Int32 totHours = 0;
    //    Int32 totWorkHours = 0;
    //    Int32 totBreakHoursShouldBe = 0;
    //    Boolean weekend = false;
    //    //lblTest.Text = "0";
    //    int i = 1;
    //    if (firstSignIn.Date.Month == Convert.ToInt32(ddlMonth1.SelectedValue))
    //    {
    //        i = firstSignIn.Day;
    //    }
    //    for (; i <= daysInMonth; i++)
    //    {
    //        DateTime currDateTime = new DateTime(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(ddlMonth1.SelectedValue), i).Date;
    //        if (absences.Where(u => u.CrtDate == currDateTime.Date).ToList().Count < 1)
    //        {
    //            Int32 userWorkHours = SP.GetTodayUserWorkHours(this.InstanceID, Convert.ToInt32(ddlUsers.SelectedValue), currDateTime);
    //            Int32 userTotHours = SP.GetTodayUserTotHours(this.InstanceID, Convert.ToInt32(ddlUsers.SelectedValue), currDateTime);
    //            weekend = false;
    //            if (WorkerLastSignIn.Date.Date >= currDateTime.Date)
    //            {
    //                if (userTotHours < 0)
    //                {
    //                    //lblTest.Text += "<0";
    //                    totWorkHours += (-(userWorkHours) + 12 - (SP.GetUserBreakTimeDiff(Convert.ToInt32(ddlUsers.SelectedValue), this.InstanceID, currDateTime) * 2));
    //                    totHours += (-(userTotHours) + 12);
    //                }
    //                else
    //                {
    //                    //lblTest.Text += "<br> " + currDateTime.ToString("MMM dd yyyy") + "  " + totWorkHours.ToString();
    //                    totWorkHours += userWorkHours;
    //                    totHours += userTotHours;
    //                }
    //                //totBreakHoursShouldBe = SP.GetUserBreakTimeDiff(Convert.ToInt32(ddlUsers.SelectedValue), this.InstanceID, currDateTime);
    //            }
    //        }

    //    }
    //    #endregion
    //    //lblEstTotalHours.Text = totHours + " hrs";
    //    lblEstWorkingHours.Text = totWorkHours.ToString() + " hrs (" + lblTotDays.Text + " days)";
    //    //lblTotalHours.Text = totHrs + " hrs " + totMins + " mins";
    //    lblWHours.Text = wtotHrs + " hrs " + wtotMins + " mins  (" + lblTotDays.Text + " days)";
    //    //lblTotBreakHrs.Text = btotHrs + " hrs " + btotMins + " mins should be " + bTotShouldHrs + " hrs " + bTotShouldMins + " mins";
    //    String breakTimeShouldBe = TimeDifference.GetTimeSpan(bTotShouldHrs);
    //    lblTotBreakHrs.Text = btotHrs + " hrs " + btotMins + " mins should be " + breakTimeShouldBe;
    //    //lblTotBreakHrs.Text = btotHrs + " hrs " + btotMins + " mins should be " + bTotShouldHrs + " hrs";

    //    //string[] wHourMin = lblWHours.Text.Split(' ');
    //    //try
    //    //{
    //    //    int exHr = 0;
    //    //    int exMin = 0;
    //    //    if (wHourMin.Count() > 3)
    //    //    {
    //    //        exHr = Convert.ToInt32(wHourMin[0]);
    //    //        exMin = Convert.ToInt32(wHourMin[2]);
    //    //    }
    //    //    else
    //    //    {
    //    //        exMin = Convert.ToInt32(wHourMin[0]);
    //    //    }
    //    //    exHr = exHr - Convert.ToInt32(lblEstWorkingHours.Text.Split(' ')[0]);
    //    //    if (exHr > 0)
    //    //    {
    //    //        lblExtraHrs.Text = exHr + " hrs " + exMin + " min";
    //    //    }
    //    //    else
    //    //    {
    //    //        lblExtraHrs.Text = "0 hrs";
    //    //    }

    //    //}
    //    //catch (Exception)
    //    //{ }

    //}

    private void CreateWeekendTable(List<RecordModel> records)
    {
        AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
        tblHistory.Rows.Clear();
        tblHistory.CssClass = "smallGrid";
        TableRow trow = new TableRow();
        trow.CssClass = "smallGridHead";
        #region Table Headers
        trow.Cells.Add(new TableCell() { Text = "" });
        trow.Cells.Add(new TableCell() { Text = "Clock In" });
        trow.Cells.Add(new TableCell() { Text = "Clock Out" });
        trow.Cells.Add(new TableCell() { Text = "UT" });
        trow.Cells.Add(new TableCell() { Text = "OT" });
        trow.Cells.Add(new TableCell() { Text = "Breaks" });
        trow.Cells.Add(new TableCell() { Text = "Worked" });
        trow.Cells.Add(new TableCell() { Text = "" });
        tblHistory.Rows.Add(trow);
        #endregion
        int idS = 0;
        List<Absence> _absences = Absence.GetAbsences(Convert.ToInt32(ddlUsers.SelectedValue)).Where(u => u.Active == true).ToList();
        List<ExDay> _exDays = ExDay.GetExDays(Convert.ToInt32(ddlUsers.SelectedValue)).Where(u => u.Active == true).ToList();
        lblTotalAbsences.Text = _absences.Where(u => u.CrtDate.Month == Convert.ToInt32(ddlMonth1.SelectedValue) && u.CrtDate.Year == Convert.ToInt32(ddlYear.SelectedValue)).ToList().Count().ToString();
        int iTotalNE = 0;
        GenerateStatistics(records);
        #region Generate Records
        Boolean lastSignIn = false;
        foreach (RecordModel record in records) {
            //&& !record.DayType.HasValue
            if (WorkerLastSignIn < record.Date.Date && _absences.Where(u => u.CrtDate.Date == record.Date && u.Active == true).ToList().Count <= 0) {
                continue;
            }

            if (WorkerFirstSignIn > record.Date.Date) {
                continue;
            }
            if (record.Records.Count == 0) {
                continue;
            }

            TableRow tRowRecord = new TableRow();
            string todaysDate = objATZ.GetDate(record.Date.ToString(), HttpContext.Current.User.Identity.Name);

            tRowRecord.Cells.Add(new TableCell() { Text = todaysDate, RowSpan = record.Records.Count });

            #region Handle weekends, holidays etc and set trow class accordingly for first record
            TableCell tcellhw = new TableCell();
            if (record.DayType.HasValue) {
                if (record.Records.Count <= 0) {
                    //imgMDetails.Visible = false;
                    //subRecordsClockIn.Visible = false;
                }
                if (string.IsNullOrEmpty(record.DayTitle)) {
                    tcellhw.Text = "Holiday";
                }
                else {
                    tcellhw.Text = record.DayTitle;
                }
                tcellhw.Font.Bold = true;
                tcellhw.ColumnSpan = 7;
                //tRowRecord.Cells[0].RowSpan = record.Records.Count + 1;
                tRowRecord.Cells.Add(tcellhw);
                tblHistory.Rows.Add(tRowRecord);

                //dtitle.Visible = true;
                if (record.DayType.Value == 0) {
                    tRowRecord.Attributes.Add("class", "greenRow");
                }
                //rContainer.Attributes.Add("class", "SatSunHolidays");
                else {
                    //rContainer.Attributes.Add("class", "NormalHoliday");
                    tRowRecord.Attributes.Add("class", "yellowRow");
                    //trDayOff.Visible = true;
                }
                tRowRecord = new TableRow();
            }


            #endregion

            if (record.Records.Count > 0 && record.DayType.HasValue) {
                if (record.DayType.HasValue) {
                    if (record.DayType.Value == 0) {
                        tRowRecord.Attributes.Add("class", "greenRow");
                    }
                    else {
                        tRowRecord.Attributes.Add("class", "yellowRow");
                    }
                }

                if ((String.IsNullOrEmpty(record.Records[0].LogoutTime) || record.Records[0].LogoutTime == "") && record.Records.Count < 1) {
                    tRowRecord.CssClass = "hide";
                }

                // Add Clock-In time
                if (!String.IsNullOrEmpty(record.Records[0].LoginTime) || record.Records[0].LoginTime != "") {
                    if (record.Date.Date == Convert.ToDateTime(record.Records[0].LoginTime).Date) {
                        tRowRecord.Cells.Add(new TableCell() { Text = objATZ.GetTime(record.Records[0].LoginTime, HttpContext.Current.User.Identity.Name) });
                    }
                    else {
                        tRowRecord.Cells.Add(new TableCell() { Text = objATZ.GetTime(record.Records[0].LoginTime, HttpContext.Current.User.Identity.Name) + " - " + Convert.ToDateTime(record.Records[0].LoginTime).Date.ToString("MMM dd yyyy") });
                    }
                }

                // Add Clock-Out time
                Label lblTotalTime = new Label();
                if (!String.IsNullOrEmpty(record.Records[0].LogoutTime) || record.Records[0].LogoutTime != "") {
                    #region Delete Hours for Record[0]
                    Image imgDeleteHours = new Image();
                    imgDeleteHours.AlternateText = " - Delete Hours";
                    imgDeleteHours.ImageUrl = "~/images/Controls/Minusicon.png";

                    imgDeleteHours.ID = "imgDeleteHours" + ++idS + "" + record.Records[0].Id;


                    imgDeleteHours.CssClass = "imgDeleteHours tooltip";
                    imgDeleteHours.ToolTip = "Use this option to delete time from your recorded history in order to fix any excess time you recorded by mistake.";
                    imgDeleteHours.Attributes.Add("Title", "Delete Hours");

                    string intime = objATZ.GetTime(record.Records[0].LoginTime, ddlUsers.SelectedValue.ToString());
                    intime = ((!IsSimpleClock) ? Convert.ToDateTime(intime).ToString("HH:mm") : intime);
                    imgDeleteHours.Attributes.Add("data-frmTime", intime);

                    String outtime = objATZ.GetTime(record.Records[0].LogoutTime, ddlUsers.SelectedValue.ToString());
                    outtime = ((!IsSimpleClock) ? Convert.ToDateTime(outtime).ToString("HH:mm") : outtime);

                    string inDate = objATZ.GetDate(record.Records[0].LoginTime, HttpContext.Current.User.Identity.Name);
                    if (inDate.Contains("Today")) {
                        inDate = DateTime.Now.ToString();
                    }
                    string outDate = objATZ.GetDate(record.Records[0].LogoutTime, HttpContext.Current.User.Identity.Name);
                    if (outDate.Contains("Today")) {
                        outDate = DateTime.Now.ToString();
                    }
                    imgDeleteHours.Attributes.Add("data-toTime", outtime);

                    imgDeleteHours.Attributes.Add("data-day", record.Date.ToString("ddd, MMM dd yyyy") + " from " + intime + " to " + outtime);
                    imgDeleteHours.Attributes.Add("data-recordID", record.Records[0].Id.ToString());
                    imgDeleteHours.Style.Add("float", "right");
                    #endregion

                    #region Edit Hours Record[0] -- pencil.png

                    Image imgEditHours = new Image();
                    imgEditHours.AlternateText = " - Edit Hours";
                    imgEditHours.ImageUrl = "~/images/Controls/pencil.png";
                    imgEditHours.ID = "imgEditHours" + ++idS + "" + record.Records[0].Id;
                    imgEditHours.CssClass = "imgEditHours tooltip";
                    imgEditHours.ToolTip = "Use this option to edit time. Admin only";
                    imgEditHours.Attributes.Add("Title", "Edit Time");
                    imgEditHours.Attributes.Add("data-frmTime", intime);
                    imgEditHours.Attributes.Add("data-toTime", outtime);
                    imgEditHours.Attributes.Add("data-frmdate", Convert.ToDateTime(inDate).ToShortDateString() + " " + intime);
                    imgEditHours.Attributes.Add("data-todate", Convert.ToDateTime(outDate).ToShortDateString() + " " + outtime);
                    imgEditHours.Attributes.Add("data-recordID", record.Records[0].Id.ToString());
                    imgEditHours.Style.Add("width", "12px");
                    imgEditHours.Style.Add("margin-left", "15px");
                    imgEditHours.Style.Add("cursor", "pointer");
                    imgEditHours.Style.Add("float", "right");
                    #endregion

                    TableCell tblCellLnkDelHours = new TableCell();
                    Label lblLogout = new Label();
                    if (record.Date.Date == Convert.ToDateTime(record.Records[0].LogoutTime).Date) {
                        lblLogout.Text = objATZ.GetTime(record.Records[0].LogoutTime, HttpContext.Current.User.Identity.Name);
                        tblCellLnkDelHours.Controls.Add(lblLogout);
                        //tblCellLnkDelHours.Controls.Add(imgDeleteHours);
                        if (!isWorker) {
                            tblCellLnkDelHours.Controls.Add(imgEditHours);
                        }
                        tRowRecord.Cells.Add(tblCellLnkDelHours);
                    }
                    else {
                        lblLogout.Text = objATZ.GetTime(record.Records[0].LogoutTime, HttpContext.Current.User.Identity.Name) + " - " + Convert.ToDateTime(record.Records[0].LogoutTime).Date.ToString("MMM dd yyyy");
                        tblCellLnkDelHours.Controls.Add(lblLogout);
                        //tblCellLnkDelHours.Controls.Add(imgDeleteHours);
                        if (!isWorker) {
                            tblCellLnkDelHours.Controls.Add(imgEditHours);
                        }
                        tRowRecord.Cells.Add(tblCellLnkDelHours);
                    }
                }
                else {
                    //lnkDeleteHours.CssClass = "imgDeleteHours";
                    //lnkDeleteHours.ToolTip = "Use this option to delete time from your recorded history in order to fix any excess time you recorded by mistake.";

                    //string time = objATZ.GetTime(record.Records[0].LoginTime, UserId.toString());
                    //time = ((!IsSimpleClock) ? Convert.ToDateTime(time).ToString("HH:mm") : time);
                    //lnkDeleteHours.Attributes.Add("data-frmTime", time);

                    //time = objATZ.GetTime(DateTime.Now.ToString(), UserId.toString());
                    //time = ((!IsSimpleClock) ? Convert.ToDateTime(time).ToString("HH:mm") : time);
                    //lnkDeleteHours.Attributes.Add("data-toTime", time);
                    //lnkDeleteHours.Attributes.Add("data-recordID", record.Records[0].Id.ToString());

                    //imgDeleteHours.Attributes.Add("data-day", record.Date.ToString("ddd, MM dd yyyy") + " from " + intime + " to " + outtime);

                    string intime = objATZ.GetTime(record.Records[0].LoginTime, ddlUsers.SelectedValue.ToString());
                    intime = ((!IsSimpleClock) ? Convert.ToDateTime(intime).ToString("HH:mm") : intime);
                    //lnkDeleteHours.Attributes.Add("data-frmTime", intime);

                    String outtime = objATZ.GetTime(record.Records[0].LogoutTime, ddlUsers.SelectedValue.ToString());
                    outtime = ((!IsSimpleClock) ? Convert.ToDateTime(outtime).ToString("HH:mm") : outtime);

                    string inDate = objATZ.GetDate(record.Records[0].LoginTime, HttpContext.Current.User.Identity.Name);
                    if (inDate.Contains("Today")) {
                        inDate = DateTime.Now.ToString();
                    }
                    string outDate = objATZ.GetDate(record.Records[0].LogoutTime, HttpContext.Current.User.Identity.Name);
                    if (outDate.Contains("Today")) {
                        outDate = DateTime.Now.ToString();
                    }
                    //lnkDeleteHours.Attributes.Add("data-toTime", outtime);

                    if (String.IsNullOrWhiteSpace(outtime) || outtime.Contains("&nbsp;")) {
                        //lnkDeleteHours.Attributes.Add("data-day", record.Date.ToString("ddd, MMM dd yyyy") + " from " + intime + " to till now");
                    }
                    else {
                        //lnkDeleteHours.Attributes.Add("data-day", record.Date.ToString("ddd, MMM dd yyyy") + " from " + intime + " to " + outtime);
                    }


                    //lnkDeleteHours.Attributes.Add("data-title", "");
                    TableCell tblCellLnkDelHours = new TableCell();
                    Label lblLogout = new Label();
                    tRowRecord.Cells.Add(new TableCell() { Text = "..." });

                    lastSignIn = true;
                    lblTotalTime.Attributes.Add("data-incdate", UtilityMethods.getClientDateTime(Convert.ToDateTime(record.Records[0].LoginTime)).ToString());
                    lblTotalTime.ID = "lblTotalTime" + idS + "" + record.Records[0];
                }

                // Adding Undertime and Undertime in grid
                TableCell tbCell = new TableCell();
                Label lblUndertime = new Label();
                lblUndertime.ID = "lblUndertime" + idS + "" + record.Records[0];
                lblUndertime.Text = UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(record.TotalUnderTimeWithoutLate.TotalMinutes)).ToString();
                ImageButton lnkEditUndertime = new ImageButton();
                lnkEditUndertime.ID = "lnkEditUndertime" + idS + "" + record.Records[0];
                lnkEditUndertime.ImageUrl = "../images/Controls/pencil.png";
                lnkEditUndertime.ToolTip = "Use this option to edit Under-time. Admin only";
                lnkEditUndertime.Attributes.Add("Title", "Edit Time");
                lnkEditUndertime.Style.Add("width", "12px");
                lnkEditUndertime.Style.Add("margin-left", "5px");
                lnkEditUndertime.Style.Add("cursor", "pointer");
                lnkEditUndertime.Style.Add("float", "right");
                lnkEditUndertime.CssClass = "raisesevent lnkEditUndertime_";
                //lnkEditUndertime.Click += lnkEditUndertime_Click;
                lnkEditUndertime.Attributes["data-recid"] = record.Records[0].Id.ToString();
                lnkEditUndertime.CommandArgument = record.Records[0].Id.ToString();
                tbCell.Controls.Add(lblUndertime);
                if (Convert.ToInt32(record.TotalUnderTime.TotalMinutes) != 0 && !isWorker) { tbCell.Controls.Add(lnkEditUndertime); }

                tRowRecord.Cells.Add(tbCell);
                tbCell = new TableCell();
                lblUndertime = new Label();
                lblUndertime.ID = "lblOvertime" + idS + "" + record.Records[0];
                lblUndertime.Text = UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(record.TotalOverTime.TotalMinutes)).ToString();
                lnkEditUndertime = new ImageButton();
                lnkEditUndertime.ID = "lnkEditOvertime" + idS + "" + record.Records[0];
                lnkEditUndertime.ImageUrl = "../images/Controls/pencil.png";
                lnkEditUndertime.ToolTip = "Use this option to edit Over-time. Admin only";
                lnkEditUndertime.Attributes.Add("Title", "Edit Time");
                lnkEditUndertime.Style.Add("width", "12px");
                lnkEditUndertime.Style.Add("margin-left", "5px");
                lnkEditUndertime.Style.Add("cursor", "pointer");
                lnkEditUndertime.Style.Add("float", "right");
                lnkEditUndertime.CssClass = "raisesevent lnkEditOvertime_";
                lnkEditUndertime.Attributes["data-recid"] = record.Records[0].Id.ToString();
                lnkEditUndertime.CommandArgument = record.Records[0].Id.ToString();
                tbCell.Controls.Add(lblUndertime);
                if (Convert.ToInt32(record.TotalOverTime.TotalMinutes) != 0 && !isWorker) { tbCell.Controls.Add(lnkEditUndertime); }

                //tbCell.Text = UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(record.TotalOverTime.TotalMinutes)).ToString();
                tRowRecord.Cells.Add(tbCell);

                // Add Totaltime with breaks
                lblTotalTime.Text = record.TotalWorkedHours;
                TableCell cellTotalTime = new TableCell();
                cellTotalTime.Controls.Add(lblTotalTime);
                cellTotalTime.RowSpan = record.Records.Count;
                tRowRecord.Cells.Add(cellTotalTime);

                // Add Total break time                    
                TableCell cellTotalBreak = new TableCell();
                cellTotalBreak.Text = record.TotalBreak;
                cellTotalBreak.RowSpan = record.Records.Count;
                tRowRecord.Cells.Add(cellTotalBreak);

                #region --- Create detail information ---
                String details = "<table id='tblDetail'><tr>";
                details += "<td>Clock-In Location:</td>";
                int breakcolspanIn = 0;
                int breakcolspanOut = 0;
                // Check if location in is same or not
                bool sameLocationIn = true;
                bool sameLocationOut = true;
                foreach (SubRecordModel subRecord in record.Records) {
                    if (subRecord.LoginStatus != record.Records[0].LoginStatus) {
                        sameLocationIn = false;
                    }
                    if (subRecord.LogoutStatus != record.Records[0].LogoutStatus) {
                        sameLocationOut = false;
                    }
                }

                bool execIn = true;
                foreach (SubRecordModel subRecord in record.Records) {
                    if (sameLocationIn) {
                        if (execIn) {
                            breakcolspanIn++;
                            details += "<td>" + subRecord.InLocation;
                            if (!String.IsNullOrEmpty(subRecord.InLocation)) {
                                details += ((subRecord.LoginStatus == "Notverified") ? "<img src='images/Controls/cross.png' style='margin-left:4px' alt='Notverified' width='13px'/>" : "<img src='images/Controls/tick.png' width='13px' alt='Verified' class='tooltip' style='margin-left:4px' />") + "</td>";
                            }
                            execIn = false;
                        }
                    }
                    else {
                        breakcolspanIn++;
                        details += "<td>" + subRecord.InLocation;
                        if (!String.IsNullOrEmpty(subRecord.InLocation)) {
                            details += ((subRecord.LoginStatus == "Notverified") ? "<img src='images/Controls/cross.png' style='margin-left:4px' alt='Notverified' width='13px'/>" : "<img src='images/Controls/tick.png' width='13px' alt='Verified' class='tooltip' style='margin-left:4px' />") + "</td>";
                        }
                    }
                }
                details += "</tr><tr><td>Clock-Out Location:</td>";
                bool execOut = true;
                foreach (SubRecordModel subRecord in record.Records) {
                    if (sameLocationOut) {
                        if (execOut) {
                            breakcolspanOut++;
                            details += "<td>" + subRecord.OutLocation;
                            if (!String.IsNullOrEmpty(subRecord.OutLocation)) {
                                details += ((subRecord.LogoutStatus == "Notverified") ? "<img src='images/Controls/cross.png' style='margin-left:4px' alt='Notverified' width='13px'/>" : "<img src='images/Controls/tick.png' width='13px' alt='Verified' class='tooltip' style='margin-left:4px' />") + "</td>";
                            }
                            else {
                                details += "... </td>";
                            }
                            execOut = false;
                        }
                    }
                    else {
                        breakcolspanOut++;
                        details += "<td>" + subRecord.OutLocation;
                        if (!String.IsNullOrEmpty(subRecord.OutLocation)) {
                            details += ((subRecord.LogoutStatus == "Notverified") ? "<img src='images/Controls/cross.png' style='margin-left:4px' alt='Notverified' width='13px'/>" : "<img src='images/Controls/tick.png' width='13px' alt='Verified' class='tooltip' style='margin-left:4px' />") + "</td>";
                        }
                    }
                }
                details += "</tr>";

                foreach (BreakModel breakModel in record.Breaks) {
                    details += string.Format("<tr><td>Break:</td><td colspan='" + ((breakcolspanIn > breakcolspanOut) ? breakcolspanIn + 1 : breakcolspanOut + 1) + "'>From: {0} To: {1}", Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + breakModel.BreakStartTime.ToString()).ToShortTimeString(), Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + breakModel.BreakEndTime.ToString()).ToShortTimeString()) + "</td>";
                }
                details += "</tr><tr><td>Total Time:</td><td colspan='" + ((breakcolspanIn > breakcolspanOut) ? breakcolspanIn + 1 : breakcolspanOut + 1) + "'>" + record.TotalTime + "</td></table>";
                // Info Image
                tRowRecord.Cells.Add(new TableCell() { Text = "<img src=\"images/Controls/icon_info.png\" alt=\"\" title=\"" + details + "\" Class=\"tooltip\" ToolTip=\"" + details + "\" />", RowSpan = record.Records.Count });

                #region --- Mark as Partial ---
                TableCell tbcell = new TableCell();
                if (_exDays.Where(u => u.CrtDate.Date.Date == record.Date.Date && u.Active == true).ToList().Count > 0) {
                    ExDay exDay = new ExDay();
                    try {
                        exDay = _exDays.Last();
                    }
                    catch (Exception) {
                    }

                    //Label lblText = new Label();
                    //lblText.ID = "lblText" + ++idS;
                    //lblText.CssClass = "lblText";
                    //lblText.Text = "Partial Day - ";
                    Image lnkMarkNPW = new Image();
                    lnkMarkNPW.ID = "lnkMarkNPW" + idS;
                    lnkMarkNPW.CssClass = "lnkMarkNPW tooltip";
                    lnkMarkNPW.ImageUrl = "~/images/controls/piechart.png";
                    lnkMarkNPW.Attributes.Add("tooltip", "Mark as not partial working");
                    lnkMarkNPW.Attributes.Add("Title", "Mark as not partial working");
                    //lnkMarkNPW.Attributes.Add("class", "tooltip");
                    lnkMarkNPW.Style.Add("width", "16px");
                    lnkMarkNPW.Style.Add("cursor", "pointer");
                    lnkMarkNPW.Attributes.Add("data-id", exDay.ExDayID.ToString());
                    lnkMarkNPW.Attributes.Add("data-ex-type", Convert.ToInt32(exDay.ExDayTypeID).ToString());
                    lnkMarkNPW.Attributes.Add("data-UserId", exDay.UserID.ToString().ToString());
                    lnkMarkNPW.Attributes.Add("data-date", exDay.CrtDate.ToString());
                    lnkMarkNPW.Attributes.Add("data-active", "False");
                    //tbcell.Controls.Add(lblText);
                    //tbcell.Controls.Add(lnkMarkNPW);

                    //Image imgCommentDetail = new Image();
                    ////tRowRecord.Cells.Add(new TableCell() { Text = "<img src=\"images/Controls/icon_info.png\" alt=\"\" title=\"" + details + "\" Class=\"tooltip\" ToolTip=\"" + details + "\" />", RowSpan = record.Records.Count });
                    //imgCommentDetail.ImageUrl = "images/Controls/icon_info.png";
                    //imgCommentDetail.CssClass = "tooltip";
                    //imgCommentDetail.Attributes.Add("title", exDay.Comment);
                    //imgCommentDetail.ToolTip = exDay.Comment;
                    //imgCommentDetail.Style.Add("float", "right");

                    tRowRecord.Attributes.Add("class", "eggshellRow");
                    tbcell.Font.Bold = true;
                    tRowRecord.Cells[0].RowSpan = record.Records.Count;
                    tbcell.RowSpan = record.Records.Count;
                    //tRowRecord.Cells.Add(tbcell);
                    //tcellhw.Controls.Add(imgCommentDetail);                        
                }
                else {
                    Label lblText = new Label();
                    lblText.ID = "lblText" + ++idS;
                    lblText.CssClass = "lblText";
                    lblText.Text = "";
                    Image lnkMarkNPW = new Image();
                    lnkMarkNPW.ID = "lnkMarkNPW" + idS;
                    lnkMarkNPW.CssClass = "lnkMarkNPW tooltip";
                    lnkMarkNPW.ImageUrl = "~/images/controls/piechart.png";
                    lnkMarkNPW.Attributes.Add("tooltip", "Mark as partial working");
                    lnkMarkNPW.Attributes.Add("Title", "Mark as partial working");
                    //lnkMarkNPW.Attributes.Add("class", "tooltip");
                    lnkMarkNPW.Style.Add("width", "16px");
                    lnkMarkNPW.Style.Add("cursor", "pointer");
                    lnkMarkNPW.Attributes.Add("data-id", "0");
                    lnkMarkNPW.Attributes.Add("data-ex-type", "1");
                    lnkMarkNPW.Attributes.Add("data-UserId", ddlUsers.SelectedValue.ToString());
                    lnkMarkNPW.Attributes.Add("data-date", record.Date.ToString());
                    lnkMarkNPW.Attributes.Add("data-active", "True");
                    tbcell.Controls.Add(lblText);
                    tbcell.Controls.Add(lnkMarkNPW);
                    tbcell.RowSpan = record.Records.Count;
                    //tRowRecord.Cells.Add(tbcell);
                }
                #endregion

                tblHistory.Rows.Add(tRowRecord);
                #endregion



                #region --- Create SubRecords ---
                for (int i = 1; i < record.Records.Count; i++) {
                    TableRow subRecord = new TableRow();
                    if (record.DayType.HasValue) {
                        if (record.DayType.Value == 0) {
                            subRecord.Attributes.Add("class", "greenRow");
                        }
                        else {
                            subRecord.Attributes.Add("class", "yellowRow");
                        }
                    }
                    TableCell cellClockInTime = new TableCell();
                    TableCell cellClockOutTime = new TableCell();
                    if (!String.IsNullOrEmpty(record.Records[i].LoginTime) || record.Records[i].LoginTime != "") {
                        if (record.Date.Date == Convert.ToDateTime(record.Records[i].LoginTime).Date) {
                            cellClockInTime.Text = objATZ.GetTime(record.Records[i].LoginTime, HttpContext.Current.User.Identity.Name);
                        }
                        else {
                            cellClockInTime.Text = objATZ.GetTime(record.Records[i].LoginTime, HttpContext.Current.User.Identity.Name) + "-" + Convert.ToDateTime(record.Records[i].LoginTime).Date.ToShortDateString();
                        }
                    }
                    if (!String.IsNullOrEmpty(record.Records[i].LogoutTime) || record.Records[i].LogoutTime != "") {
                        #region Delete Hours for Record[i]
                        Image imgDeleteHours2 = new Image();
                        imgDeleteHours2.AlternateText = " - Delete Hours";
                        imgDeleteHours2.ImageUrl = "../images/Controls/Minusicon.png";
                        imgDeleteHours2.ID = "imgDeleteHours2" + i + "" + record.Records[i].Id;
                        imgDeleteHours2.CssClass = "imgDeleteHours tooltip";
                        imgDeleteHours2.ToolTip = "Use this option to delete time from your recorded history in order to fix any excess time you recorded by mistake.";
                        imgDeleteHours2.Attributes.Add("Title", "Delete Hours");

                        string intime = objATZ.GetTime(record.Records[i].LoginTime, ddlUsers.SelectedValue.ToString());
                        intime = ((!IsSimpleClock) ? Convert.ToDateTime(intime).ToString("HH:mm") : intime);
                        imgDeleteHours2.Attributes.Add("data-frmTime", intime);

                        String outtime = objATZ.GetTime(record.Records[i].LogoutTime, ddlUsers.SelectedValue.ToString());
                        outtime = ((!IsSimpleClock) ? Convert.ToDateTime(outtime).ToString("HH:mm") : outtime);

                        string inDate = objATZ.GetDate(record.Records[i].LoginTime, HttpContext.Current.User.Identity.Name);
                        if (inDate.Contains("Today")) {
                            inDate = DateTime.Now.ToString();
                        }
                        string outDate = objATZ.GetDate(record.Records[i].LogoutTime, HttpContext.Current.User.Identity.Name);
                        if (outDate.Contains("Today")) {
                            outDate = DateTime.Now.ToString();
                        }
                        imgDeleteHours2.Attributes.Add("data-toTime", outtime);

                        imgDeleteHours2.Attributes.Add("data-day", record.Date.ToString("ddd, MMM dd yyyy") + " from " + intime + " to " + outtime);
                        imgDeleteHours2.Attributes.Add("data-recordID", record.Records[i].Id.ToString());
                        imgDeleteHours2.Style.Add("float", "right");
                        Label lblLogout = new Label();
                        #endregion

                        #region Edit Hours Record[i] -- pencil.png
                        Image imgEditHours2 = new Image();
                        imgEditHours2.AlternateText = " - Edit Hours";
                        imgEditHours2.ImageUrl = "~/images/Controls/pencil.png";
                        imgEditHours2.ID = "imgEditHours2" + i + "" + record.Records[i].Id;
                        imgEditHours2.CssClass = "imgEditHours tooltip";
                        imgEditHours2.ToolTip = "Use this option to edit time. Admin only";
                        imgEditHours2.Attributes.Add("Title", "Edit Time");
                        imgEditHours2.Attributes.Add("data-frmTime", intime);
                        imgEditHours2.Attributes.Add("data-toTime", outtime);
                        imgEditHours2.Attributes.Add("data-frmDate", inDate);
                        imgEditHours2.Attributes.Add("data-toDate", outDate);
                        imgEditHours2.Attributes.Add("data-recordID", record.Records[i].Id.ToString());
                        imgEditHours2.Style.Add("width", "12px");
                        imgEditHours2.Style.Add("cursor", "pointer");
                        imgEditHours2.Style.Add("float", "right");
                        imgEditHours2.Style.Add("margin-left", "15px");
                        #endregion

                        if (record.Date.Date == Convert.ToDateTime(record.Records[i].LogoutTime).Date) {
                            lblLogout.Text = objATZ.GetTime(record.Records[i].LogoutTime, HttpContext.Current.User.Identity.Name);
                            cellClockOutTime.Controls.Add(lblLogout);
                            //cellClockOutTime.Controls.Add(imgDeleteHours2);
                            if (!isWorker) {
                                cellClockOutTime.Controls.Add(imgEditHours2);
                            }
                            tRowRecord.Cells.Add(cellClockOutTime);
                        }
                        else {
                            lblLogout.Text = objATZ.GetTime(record.Records[i].LogoutTime, HttpContext.Current.User.Identity.Name) + " - " + Convert.ToDateTime(record.Records[i].LogoutTime).Date.ToString("MMM dd yyyy");
                            cellClockOutTime.Controls.Add(lblLogout);
                            //cellClockOutTime.Controls.Add(imgDeleteHours2);
                            if (!isWorker) {
                                cellClockOutTime.Controls.Add(imgEditHours2);
                            }
                            tRowRecord.Cells.Add(cellClockOutTime);
                        }
                    }
                    else {
                        lastSignIn = true;
                    }

                    subRecord.Cells.Add(cellClockInTime);
                    subRecord.Cells.Add(cellClockOutTime);
                    tblHistory.Rows.Add(subRecord);
                }
                #endregion
            }
            else {

                if (record.DayType.HasValue) {
                    if (record.Records.Count <= 0) {
                        //imgMDetails.Visible = false;
                        //subRecordsClockIn.Visible = false;
                    }
                    if (string.IsNullOrEmpty(record.DayTitle)) {
                        tcellhw.Text = "Holiday";
                    }
                    else {
                        tcellhw.Text = record.DayTitle;
                    }
                }
                else {
                    //imgMDetails.Visible = true;
                    //subRecordsClockIn.Visible = true;
                }
            }
        }
        #endregion
    }

    private void CreateNAFTable(List<RecordModel> records, Boolean abscences)
    {
        AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
        tblHistory.Rows.Clear();
        tblHistory.CssClass = "smallGrid";
        TableRow trow = new TableRow();
        trow.CssClass = "smallGridHead";
        #region Table Headers
        trow.Cells.Add(new TableCell() { Text = "" });
        trow.Cells.Add(new TableCell() { Text = "Clock In" });
        trow.Cells.Add(new TableCell() { Text = "Clock Out" });
        trow.Cells.Add(new TableCell() { Text = "Worked" });
        trow.Cells.Add(new TableCell() { Text = "Breaks" });
        trow.Cells.Add(new TableCell() { Text = "" });
        tblHistory.Rows.Add(trow);
        #endregion
        int idS = 0;
        List<Absence> _absences = Absence.GetAbsences(Convert.ToInt32(ddlUsers.SelectedValue)).Where(u => u.Active == true).ToList();
        List<ExDay> _exDays = ExDay.GetExDays(Convert.ToInt32(ddlUsers.SelectedValue)).Where(u => u.Active == true).ToList();
        lblTotalAbsences.Text = _absences.Where(u => u.CrtDate.Month == Convert.ToInt32(ddlMonth1.SelectedValue) && u.CrtDate.Year == Convert.ToInt32(ddlYear.SelectedValue)).ToList().Count().ToString();
        int iTotalNE = 0;
        Boolean lastSignIn = false;
        GenerateStatistics(records);
        #region Generate Records
        foreach (RecordModel record in records) {
            //&& !record.DayType.HasValue
            if (WorkerLastSignIn < record.Date.Date && _absences.Where(u => u.CrtDate.Date == record.Date && u.Active == true).ToList().Count <= 0) {
                continue;
            }

            if (WorkerFirstSignIn > record.Date.Date) {
                continue;
            }

            TableRow tRowRecord = new TableRow();
            string todaysDate = objATZ.GetDate(record.Date.ToString(), HttpContext.Current.User.Identity.Name);

            tRowRecord.Cells.Add(new TableCell() { Text = todaysDate, RowSpan = record.Records.Count });

            #region Handle weekends, holidays etc and set trow class accordingly for first record
            TableCell tcellhw = new TableCell();
            if (record.Records.Count <= 0 && !record.DayType.HasValue) {
                tRowRecord.Attributes.Add("class", "pinkRow");
                //Absence absence = new Absence();                                        
                if (_absences.Where(u => u.CrtDate.Date == record.Date && u.Active == true).ToList().Count <= 0) {
                    if (abscences) {
                        continue;
                    }
                    Label lblText = new Label();
                    lblText.ID = "lblText" + ++idS;
                    lblText.CssClass = "lblText";
                    lblText.Text = "No Entry - ";
                    ++iTotalNE;
                    lblTotalNE.Text = iTotalNE.ToString();
                    LinkButton lnkMarkAbsence = new LinkButton();
                    lnkMarkAbsence.ID = "lnkMarkAbsence" + idS;
                    lnkMarkAbsence.CssClass = "lnkMarkAbsence";
                    lnkMarkAbsence.CommandName = "lnkMarkAbsence";
                    lnkMarkAbsence.Text = "Mark as absent";
                    lnkMarkAbsence.CommandArgument = record.Date.ToString();
                    lnkMarkAbsence.Attributes.Add("data-id", "0");
                    lnkMarkAbsence.Attributes.Add("data-UserId", ddlUsers.SelectedValue.ToString());
                    lnkMarkAbsence.Attributes.Add("data-date", record.Date.ToString());
                    lnkMarkAbsence.Attributes.Add("data-active", "true");
                    lnkMarkAbsence.PostBackUrl = "";
                    tcellhw.Controls.Add(lblText);
                    tcellhw.Controls.Add(lnkMarkAbsence);
                }
                else {
                    if (!abscences) {
                        continue;
                    }
                    Label lblText = new Label();
                    lblText.ID = "lblText" + ++idS;
                    lblText.CssClass = "lblText";
                    lblText.Text = "Absent - ";
                    tcellhw.Controls.Add(lblText);
                    LinkButton lnkMarkAbsence = new LinkButton();
                    lnkMarkAbsence.ID = "lnkMarkPresent" + idS;
                    lnkMarkAbsence.CssClass = "lnkMarkAbsence";
                    lnkMarkAbsence.CommandName = "lnkMarkAbsence";
                    lnkMarkAbsence.Text = "Remove absence";
                    lnkMarkAbsence.CommandArgument = record.Date.ToString();
                    Absence abs = _absences.SingleOrDefault(u => u.CrtDate.Date == record.Date && u.Active == true);
                    lnkMarkAbsence.Attributes.Add("data-id", abs.AbsLogID.ToString());
                    lnkMarkAbsence.Attributes.Add("data-UserId", abs.UserID.ToString());
                    lnkMarkAbsence.Attributes.Add("data-date", abs.CrtDate.ToString());
                    lnkMarkAbsence.Attributes.Add("data-active", "False");
                    lnkMarkAbsence.PostBackUrl = "";
                    Image imgCommentDetail = new Image();
                    //tRowRecord.Cells.Add(new TableCell() { Text = "<img src=\"images/Controls/icon_info.png\" alt=\"\" title=\"" + details + "\" Class=\"tooltip\" ToolTip=\"" + details + "\" />", RowSpan = record.Records.Count });
                    imgCommentDetail.ImageUrl = "../images/Controls/icon_info.png";
                    imgCommentDetail.CssClass = "tooltip";
                    imgCommentDetail.Attributes.Add("title", abs.Comment);
                    imgCommentDetail.ToolTip = abs.Comment;
                    imgCommentDetail.Style.Add("float", "right");
                    tcellhw.Controls.Add(lblText);
                    if (!isWorker) {
                        tcellhw.Controls.Add(lnkMarkAbsence);
                    }
                    tcellhw.Controls.Add(imgCommentDetail);

                }

                DateTime workerRequestDate = SP.GetWorkerFirstSignInn(ddlUsers.SelectedValue.ToString());
                tcellhw.Font.Bold = true;
                tcellhw.ColumnSpan = 7;
                //tRowRecord.Cells[0].RowSpan = record.Records.Count + 1;
                tRowRecord.Cells.Add(tcellhw);
                tblHistory.Rows.Add(tRowRecord);
                tRowRecord = new TableRow();
            }
            #endregion
        }
        #endregion
    }

    protected void btnGR_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(ddlMonth1.SelectedValue) > 0) {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString)) {
                tblHistory.EnableViewState = false;
                FillandCreateHistory(ddlUsers.SelectedValue.ToString(), con);
                //Response.Write(tblHistory.Rows.Count.ToString());
                if (tblHistory.Rows.Count > 1) {
                    hdnData.Value = "true";
                    trMsg.Visible = false;
                }
                else {
                    hdnData.Value = "false";
                    trMsg.Visible = true;
                }
                lblUserName.Text = ((ListItem)ddlUsers.SelectedItem).Text;
            }
            //hdnData.Value = "true";
        }
        else {
            hdnData.Value = "false";
        }
    }

    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(ddlMonth1.SelectedValue) > 0) {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString)) {
                FillandCreateHistory(ddlUsers.SelectedValue.ToString(), con);
            }
        }
    }

    protected void bk_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out UserId)) {
            Response.Redirect("Reports.aspx?id=" + UserId + "&instanceid=" + this.InstanceID);
        }
    }
    protected void BtncommentClick(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString)) {
            con.Open();
            using (SqlCommand cmd = new SqlCommand("insertcomments", con)) {
                cmd.Parameters.AddWithValue("@id", ddlUsers.SelectedValue);
                cmd.Parameters.AddWithValue("@comment", txtcomment.Text);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }
    }

    protected void deleHrs_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(rID.Value)) {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString)) {
                using (SqlCommand cmd = new SqlCommand("getWorkerRecordByRowId", con)) {
                    AvaimaTimeZoneAPI obj = new AvaimaTimeZoneAPI();
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", rID.Value);
                    cmd.Parameters.AddWithValue("@ppid", ddlUsers.SelectedValue);
                    DataTable dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    if (dt.Rows.Count > 0) {
                        DateTime frmDateTime = Convert.ToDateTime(dt.Rows[0].Field<DateTime>("lastin").ToString("g"));
                        DateTime toDateTime = Convert.ToDateTime(dt.Rows[0].Field<DateTime>("lastout").ToString("g"));

                        DateTime tfdt = Convert.ToDateTime(frmDateTime.ToShortDateString() + " " + obj.GetTimeReverse(txtfrmTime.Text, ddlUsers.SelectedValue.ToString()));
                        DateTime ttdt = Convert.ToDateTime(toDateTime.ToShortDateString() + " " + obj.GetTimeReverse(txtToTime.Text, ddlUsers.SelectedValue.ToString()));
                        if (tfdt < frmDateTime) {
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('From time must be greater than or equal to current saved login time')", true);
                            return;
                        }
                        else if (tfdt >= toDateTime) {
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('From time must be less than current saved logout time')", true);
                            return;
                        }
                        else if (ttdt > toDateTime) {
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('To time must be less than or equal to current saved logout time')", true);
                            return;
                        }
                        else if (ttdt <= frmDateTime) {
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('To time must be greater than current saved login time')", true);
                            return;
                        }
                        else if (tfdt >= ttdt) {
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('From time must be less than To time')", true);
                            return;
                        }
                        else {
                            if (tfdt > frmDateTime && ttdt == toDateTime) {
                                cmd.CommandText = "updateWorkerTimeById";
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@Id", rID.Value);
                                cmd.Parameters.AddWithValue("@ppId", ddlUsers.SelectedValue);
                                cmd.Parameters.AddWithValue("@lastin", frmDateTime);
                                cmd.Parameters.AddWithValue("@lastout", tfdt);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.ExecuteNonQuery();
                            }
                            else if (tfdt == frmDateTime && ttdt < toDateTime) {
                                cmd.CommandText = "updateWorkerTimeById";
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@Id", rID.Value);
                                cmd.Parameters.AddWithValue("@ppId", ddlUsers.SelectedValue);
                                cmd.Parameters.AddWithValue("@lastin", ttdt);
                                cmd.Parameters.AddWithValue("@lastout", toDateTime);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.ExecuteNonQuery();
                            }
                            else if (tfdt > frmDateTime && ttdt < toDateTime) {
                                cmd.CommandText = "updateWorkerTimeById";
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@Id", rID.Value);
                                cmd.Parameters.AddWithValue("@ppId", ddlUsers.SelectedValue);
                                cmd.Parameters.AddWithValue("@lastin", frmDateTime);
                                cmd.Parameters.AddWithValue("@lastout", tfdt);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.ExecuteNonQuery();

                                cmd.CommandText = "insertWorkerTime";
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@ppId", ddlUsers.SelectedValue);
                                cmd.Parameters.AddWithValue("@userid", ddlUsers.SelectedValue);
                                cmd.Parameters.AddWithValue("@lastin", ttdt);
                                cmd.Parameters.AddWithValue("@lastout", toDateTime);
                                cmd.Parameters.AddWithValue("@address", Request.UserHostAddress);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.ExecuteNonQuery();
                            }
                            else if (tfdt == frmDateTime && ttdt == toDateTime) {
                                cmd.CommandText = "updateWorkerTimeById";
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@Id", rID.Value);
                                cmd.Parameters.AddWithValue("@ppId", ddlUsers.SelectedValue);
                                cmd.Parameters.AddWithValue("@lastin", frmDateTime);
                                cmd.Parameters.AddWithValue("@lastout", frmDateTime);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
        }
    }

    protected void btnUpdateTime_Click(object sender, EventArgs e)
    {
        //hdnerid.Value = UserId.ToString();
        if (Page.IsValid) {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString)) {
                if (!String.IsNullOrEmpty(txtEFrmTime.Text) && !String.IsNullOrEmpty(txtEToTime.Text)) {

                    if (Convert.ToDateTime(txtEFrmTime.Text).ToString() == Convert.ToDateTime(txtEToTime.Text).ToString()) {
                        using (SqlCommand cmd = new SqlCommand("DELETE workertrans WHERE rowID = @rowID", con)) {
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@rowID", hdnerid.Value.ToString());
                            con.Open();
                            cmd.ExecuteNonQuery();
                            //SetUserStatus();
                            //FillandCreateHistory(UserId, con);
                            FillandCreateHistory(ddlUsers.SelectedValue.ToString(), con);
                            return;
                        }
                    }
                }


                using (SqlCommand cmd = new SqlCommand("updateworkertime", con)) {
                    AvaimaTimeZoneAPI objtime = new AvaimaTimeZoneAPI();
                    DateTime? timein = null;
                    if (!string.IsNullOrEmpty(txtEFrmTime.Text)) {
                        //timein = Convert.ToDateTime(DateTime.ParseExact(txteditsignin.Text, "dd/MM/yyyy", null).ToShortDateString() + " " + objtime.GetTimeReverse(txteditintime.Text, HttpContext.Current.User.Identity.Name));
                        timein = Convert.ToDateTime(txtEFrmTime.Text);
                        string time = objtime.GetTimeReverse(timein.Value.ToShortTimeString(), HttpContext.Current.User.Identity.Name);
                        timein = Convert.ToDateTime(timein.Value.Date.ToShortDateString() + " " + time);
                    }
                    DateTime? timeout = null;
                    if (!string.IsNullOrEmpty(txtEToTime.Text)) {
                        timeout = Convert.ToDateTime(txtEToTime.Text);
                        string time = objtime.GetTimeReverse(timeout.Value.ToShortTimeString(), HttpContext.Current.User.Identity.Name);
                        timeout = Convert.ToDateTime(timeout.Value.Date.ToShortDateString() + " " + time);
                    }
                    con.Open();
                    if (timein.HasValue) {
                        cmd.Parameters.AddWithValue("@lastin", timein);
                    }
                    else {
                        cmd.Parameters.AddWithValue("@lastin", DBNull.Value);
                    }

                    if (timeout.HasValue) {
                        cmd.Parameters.AddWithValue("@lastout", timeout);
                    }
                    else {
                        cmd.Parameters.AddWithValue("@lastout", DBNull.Value);
                    }
                    cmd.Parameters.AddWithValue("@id", hdnerid.Value);
                    cmd.Parameters.AddWithValue("@comment", txteComments.Text);
                    cmd.Parameters.AddWithValue("@instatus", ddlCIV.SelectedValue);
                    cmd.Parameters.AddWithValue("@outstatus", ddlCOV.SelectedValue);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
                //SetUserStatus();
                //FillandCreateHistory(UserId, con);
                FillandCreateHistory(ddlUsers.SelectedValue.ToString(), con);
            }
        }
    }

    protected void lnkEditUndertime_Click(object sender, EventArgs e)
    {
        //Response.Write("undertime");
        workertran wt = workertran.SingleOrDefault(u => u.rowID == Convert.ToInt32(hdnerid.Value));
        //Response.Write(wt.ToString());
        if (wt != null) {
            hdnEditUOT.Value = "Undertime";
            rptUOT.DataSource = UnderTime.Find(u => u.UserID == Convert.ToInt32(ddlUsers.SelectedValue)).ToList().Where(u => u.FromTime.Value.Date == wt.lastin.Value.Date);
            rptUOT.DataBind();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString)) { con.Open(); FillandCreateHistory(ddlUsers.SelectedValue.ToString(), con); }
            ScriptManager.RegisterStartupScript(this, GetType(), "", "OpenEditUOT();", true);
        }
    }
    protected void lnkEditOvertime_Click(object sender, EventArgs e)
    {
        workertran wt = workertran.SingleOrDefault(u => u.rowID == Convert.ToInt32(Convert.ToInt32(hdnerid.Value)));
        if (wt != null) {
            hdnEditUOT.Value = "Overtime";
            rptOvertimeEdit.DataSource = OverTime.Find(u => u.UserID == Convert.ToInt32(ddlUsers.SelectedValue)).ToList().Where(u => u.FromTime.Value.Date == wt.lastin.Value.Date);
            rptOvertimeEdit.DataBind();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString)) { con.Open(); FillandCreateHistory(ddlUsers.SelectedValue.ToString(), con); }
            ScriptManager.RegisterStartupScript(this, GetType(), "", "OpenEditOvertime();", true);
        }
    }
    protected void rptUOT_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (hdnEditUOT.Value == "Overtime") { rptOvertimes_ItemCommand(source, e); }
        else if (hdnEditUOT.Value == "Undertime") { rptUndertime_ItemCommand(source, e); }
    }

    protected void rptOvertimes_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Update") {
            AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
            TextBox txtOverTimeFrom = e.Item.FindControl("txtOverTimeFrom") as TextBox;
            TextBox txtOverTimeTo = e.Item.FindControl("txtOverTimeTo") as TextBox;
            TextBox txtOverTimeComment = e.Item.FindControl("txtOverTimeComment") as TextBox;
            HiddenField hdnOverTimeDate = e.Item.FindControl("hdnOvertimeDate") as HiddenField;
            OverTime overtime = OverTime.SingleOrDefault(u => u.OTID == Convert.ToInt32(e.CommandArgument));
            if (overtime != null)
            {
                DateTime fromtime = Convert.ToDateTime(Convert.ToDateTime(hdnOverTimeDate.Value).ToShortDateString() + " " + atz.GetTimeReverse(txtOverTimeFrom.Text, HttpContext.Current.User.Identity.Name));
                DateTime totime = Convert.ToDateTime(Convert.ToDateTime(hdnOverTimeDate.Value).ToShortDateString() + " " + atz.GetTimeReverse(txtOverTimeTo.Text, HttpContext.Current.User.Identity.Name));
                sendupdateemail(txtOverTimeComment.Text, fromtime, totime, Convert.ToDateTime(overtime.FromTime), Convert.ToDateTime(overtime.ToTime), false);
                overtime.FromTime = fromtime;
                overtime.ToTime = totime;
                overtime.Comment = txtOverTimeComment.Text;
                overtime.Update();
            }
        }
        else if (e.CommandName == "Delete") {
            OverTime overtime = OverTime.SingleOrDefault(u => u.OTID == Convert.ToInt32(e.CommandArgument));
            if (overtime != null) { overtime.Delete(); }
        }
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString)) { con.Open(); FillandCreateHistory(ddlUsers.SelectedValue.ToString(), con); }
    }

    public void sendupdateemail(string comment, DateTime from, DateTime to, DateTime oldfrom, DateTime oldto, bool ch)
    {
        List<int> adminIDs = SP.GetAdminIDByUserID(Convert.ToInt32(ddlUsers.SelectedValue.ToInt32()), InstanceID);
        AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
        if (ch)
        {
            foreach (int adminID in adminIDs)
            {
                attendence_management adminUser = attendence_management.SingleOrDefault(u => u.ID == adminID);
                attendence_management user = attendence_management.SingleOrDefault(u => u.ID == Convert.ToInt32(ddlUsers.SelectedValue.ToInt32()));
                if (adminUser != null && user != null)
                {
                    string messagebody = user.Title + " Update undertime from " + App.GetTime(from.ToString(), HttpContext.Current.User.Identity.Name) + " to " + App.GetTime(to.ToString(), HttpContext.Current.User.Identity.Name);
                    messagebody += "<br />";
                    messagebody += ((comment != null) ? "<br /> <b>Reason:</b> " + comment : "");
                    messagebody += "<br /> <b>From:</b> " + from.toClientTime().Value.ToShortTimeString() + " to " + to.toClientTime().Value.ToShortTimeString() + " - " + UtilityMethods.getFormatedTimeByMinutes(((to - from).TotalMinutes.ToInt32()));
                    messagebody += "<br /> <b>Day:</b> " + from.DayOfWeek.ToString();
                    messagebody += "<br /> <b>Date:</b> " + from.ToString("MMMM, dd, yyyy");
                    messagebody += "<br /><br />";
                    messagebody += "Old Record for this Entry";
                    messagebody += "<br /><br />";
                    messagebody += "<br /> <b>From:</b> " + oldfrom.toClientTime().Value.ToShortTimeString() + " to " + oldto.toClientTime().Value.ToShortTimeString() + " - " + UtilityMethods.getFormatedTimeByMinutes(((oldto - oldfrom).TotalMinutes.ToInt32()));
                    messagebody += "<br /> <b>Day:</b> " + oldfrom.DayOfWeek.ToString();
                    messagebody += "<br /> <b>Date:</b> " + oldfrom.ToString("MMMM, dd, yyyy");
                    messagebody += Helper.AvaimaEmailSignature;
                    emailAPI.send_email(adminUser.email, "Time & Attendance", "", messagebody, "Under-Time by " + user.Title);
                    emailAPI.send_email(user.email, "Time & Attendance", "", messagebody, "Under-Time by " + user.Title);
                    emailAPI.send_email(WebConfigurationManager.AppSettings["DevEamil"], "Time & Attendance", "", messagebody, "Under-Time by " + user.Title);
                }
            }
        }
        else
        {
            foreach (int adminID in adminIDs)
            {
                attendence_management adminUser = attendence_management.SingleOrDefault(u => u.ID == adminID);
                attendence_management user = attendence_management.SingleOrDefault(u => u.ID == Convert.ToInt32(ddlUsers.SelectedValue.ToInt32()));
                if (adminUser != null && user != null)
                {
                    string messagebody = user.Title + " Update Overtime from " + App.GetTime(from.ToString(), HttpContext.Current.User.Identity.Name) + " to " + App.GetTime(to.ToString(), HttpContext.Current.User.Identity.Name);
                    messagebody += "<br />";
                    messagebody += ((comment != null) ? "<br /> <b>Reason:</b> " + comment : "");
                    messagebody += "<br /> <b>From:</b> " + from.toClientTime().Value.ToShortTimeString() + " to " + to.toClientTime().Value.ToShortTimeString() + " - " + UtilityMethods.getFormatedTimeByMinutes(((to - from).TotalMinutes.ToInt32()));
                    messagebody += "<br /> <b>Day:</b> " + from.DayOfWeek.ToString();
                    messagebody += "<br /> <b>Date:</b> " + from.ToString("MMMM, dd, yyyy");
                    messagebody += "<br /><br />";
                    messagebody += "Old Record for this Entry";
                    messagebody += "<br /><br />";
                    messagebody += "<br /> <b>From:</b> " + oldfrom.toClientTime().Value.ToShortTimeString() + " to " + oldto.toClientTime().Value.ToShortTimeString() + " - " + UtilityMethods.getFormatedTimeByMinutes(((oldto - oldfrom).TotalMinutes.ToInt32()));
                    messagebody += "<br /> <b>Day:</b> " + oldfrom.DayOfWeek.ToString();
                    messagebody += "<br /> <b>Date:</b> " + oldfrom.ToString("MMMM, dd, yyyy");
                    messagebody += Helper.AvaimaEmailSignature;
                    emailAPI.send_email(adminUser.email, "Time & Attendance", "", messagebody, "Over-Time by " + user.Title);
                    emailAPI.send_email(user.email, "Time & Attendance", "", messagebody, "Over-Time by " + user.Title);
                    emailAPI.send_email(WebConfigurationManager.AppSettings["DevEamil"], "Time & Attendance", "", messagebody, "Over-Time by " + user.Title);
                }
            }
        }
    }

    protected void rptUndertime_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
        if (e.CommandName == "Update") {
            TextBox txtUnderTimeFrom = e.Item.FindControl("txtUnderTimeFrom") as TextBox;
            TextBox txtUnderTimeTo = e.Item.FindControl("txtUnderTimeTo") as TextBox;
            TextBox txtUnderTimeComment = e.Item.FindControl("txtUnderTimeComment") as TextBox;
            HiddenField hdnUnderTimeDate = e.Item.FindControl("hdnUnderTimeDate") as HiddenField;
            UnderTime undertime = UnderTime.SingleOrDefault(u => u.UTID == Convert.ToInt32(e.CommandArgument));
            if (undertime != null)
            {
                DateTime fromtime = Convert.ToDateTime(Convert.ToDateTime(hdnUnderTimeDate.Value).ToShortDateString() + " " + atz.GetTimeReverse(txtUnderTimeFrom.Text, HttpContext.Current.User.Identity.Name));
                DateTime totime = Convert.ToDateTime(Convert.ToDateTime(hdnUnderTimeDate.Value).ToShortDateString() + " " + (atz.GetTimeReverse(txtUnderTimeTo.Text, HttpContext.Current.User.Identity.Name)));
                sendupdateemail(txtOverTimeComment.Text, fromtime, totime, Convert.ToDateTime(undertime.FromTime), Convert.ToDateTime(undertime.ToTime), true);
                undertime.FromTime = fromtime;
                undertime.ToTime = totime;
                undertime.Comment = txtUnderTimeComment.Text;
                undertime.Update();
            }
        }
        else if (e.CommandName == "Delete") {
            UnderTime undertime = UnderTime.SingleOrDefault(u => u.UTID == Convert.ToInt32(e.CommandArgument));
            if (undertime != null) { undertime.Delete(); }
        }
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString)) { con.Open(); FillandCreateHistory(ddlUsers.SelectedValue.ToString(), con); }
    }

    protected void btnAddOvertime_Click(object sender, EventArgs e)
    {
        try {
            AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
            if (Convert.ToBoolean(hdnUOTime.Value)) {
                //workertran workertran1 = workertran.Find(u => u.P_PID == Convert.ToInt32(ddlUsers.SelectedValue.ToInt32())).Where(u => u.lastin.Value.Date == DateTime.Now.Date).ToList().First();
                //avaimaTest0001DB db = new avaimaTest0001DB();
                //MyAttSys.WorkingHour wh = db.GetWorkingHourOfUser(Convert.ToInt32(ddlUsers.SelectedValue.ToInt32()), InstanceID, DateTime.Now.DayOfWeek.ToString()).ExecuteDataSet().Tables[0].ToWorkingHour();
                //if (workertran1 == null)
                //{
                //    using (SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                //    {
                //        using (SqlCommand cmd1 = new SqlCommand("insertuserintime", con1))
                //        {
                //            con1.Open();
                //            if (SP.GetIPAddresses(_InstanceID).Rows.Count == 0)
                //            { cmd1.CommandText = "[insertuserintimeverified]"; }
                //            cmd1.CommandType = CommandType.StoredProcedure;
                //            cmd1.Parameters.AddWithValue("@ppid", UserId);
                //            cmd1.Parameters.AddWithValue("@userid", "uid");
                //            cmd1.Parameters.AddWithValue("@lastin", Convert.ToDateTime(wh.Clockin.Value.ToShortTimeString()));
                //            cmd1.Parameters.AddWithValue("@inaddress", Request.UserHostAddress);
                //            cmd1.Parameters.AddWithValue("@comment", txtcomment.Text);
                //            if (location.Visible == true)
                //            { cmd1.Parameters.AddWithValue("@location", location.SelectedValue); }
                //            else
                //            { cmd1.Parameters.AddWithValue("@location", DBNull.Value); }

                //            int rowid = Convert.ToInt32(cmd1.ExecuteScalar());
                //            workertran wt = workertran.SingleOrDefault(u => u.rowID == rowid);
                //            if (wt != null)
                //            {
                //                wt.lastin_actual = Convert.ToDateTime(DateTime.Now);
                //                wt.lastout = wt.lastin;
                //                wt.outverificationaddress = wt.inverificationaddress;
                //                wt.out_location = wt.in_location;
                //                wt.Update();
                //            }
                //            hrid.Value = rowid.ToString();
                //            SetUserStatus();
                //            ScriptManager.RegisterStartupScript(this, GetType(), "", "OpenDialog('#divLateClockIn');", true);
                //        }
                //    }
                //}
                OverTime overTime = new OverTime() {
                    Active = true,
                    Comment = txtOverTimeComment.Text,
                    FromTime = Convert.ToDateTime(txtUODate.Text.ToDateTime().ToShortDateString() + " " + txtOverTimeFrom.Text.toServerTime().Value.ToShortTimeString()),
                    ToTime = Convert.ToDateTime(txtUODate.Text.ToDateTime().ToShortDateString() + " " + txtOverTimeTo.Text.toServerTime().Value.ToShortTimeString()),
                    InstanceID = this.InstanceID,
                    UserID = Convert.ToInt32(ddlUsers.SelectedValue.ToInt32()),
                    Type = "Overtime"
                };

                if (txtUODate.Text.ToDateTime().Date == DateTime.Now.Date) { overTime.FromTime = Convert.ToDateTime(atz.GetTimeReverse(txtOverTimeFrom.Text, HttpContext.Current.User.Identity.Name)); }
                overTime.Add();
                List<int> adminIDs = SP.GetAdminIDByUserID(Convert.ToInt32(ddlUsers.SelectedValue.ToInt32()), InstanceID);
                AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
                foreach (int adminID in adminIDs) {
                    attendence_management adminUser = attendence_management.SingleOrDefault(u => u.ID == adminID);
                    attendence_management user = attendence_management.SingleOrDefault(u => u.ID == Convert.ToInt32(ddlUsers.SelectedValue.ToInt32()));
                    if (adminUser != null && user != null) {
                        string messagebody = user.Title + " added overtime from " + App.GetTime(overTime.FromTime.Value.ToString(), HttpContext.Current.User.Identity.Name) + " to " + App.GetTime(overTime.ToTime.Value.ToString(), HttpContext.Current.User.Identity.Name);
                        messagebody += "<br />";
                        messagebody += ((overTime.Comment != null) ? "<br /> <b>Reason:</b> " + overTime.Comment : "");
                        messagebody += "<br /> <b>From:</b> " + overTime.FromTime.toClientTime().Value.ToShortTimeString() + " to " + overTime.ToTime.toClientTime().Value.ToShortTimeString() + " - " + UtilityMethods.getFormatedTimeByMinutes(((overTime.ToTime - overTime.FromTime).ToTimeSpan().TotalMinutes.ToInt32()));
                        messagebody += "<br /> <b>Day:</b> " + overTime.FromTime.Value.DayOfWeek.ToString();
                        messagebody += "<br /> <b>Date:</b> " + overTime.FromTime.Value.ToString("MMMM, dd, yyyy");
                        messagebody += Helper.AvaimaEmailSignature;
                        emailAPI.send_email(adminUser.email, "Time & Attendance", "", messagebody, "OVER-Time by " + user.Title);
                        emailAPI.send_email(user.email, "Time & Attendance", "", messagebody, "OVER-Time by " + user.Title);
                        emailAPI.send_email(WebConfigurationManager.AppSettings["DevEamil"], "Time & Attendance", "", messagebody, "OVER-Time by " + user.Title);
                    }
                }
            }
            else if (!Convert.ToBoolean(hdnUOTime.Value)) {
                UnderTime undertime = new UnderTime() {
                    Active = true,
                    Comment = txtOverTimeComment.Text,
                    FromTime = Convert.ToDateTime(txtUODate.Text.ToDateTime().ToShortDateString() + " " + txtOverTimeFrom.Text.toServerTime().Value.ToShortTimeString()),
                    ToTime = Convert.ToDateTime(txtUODate.Text.ToDateTime().ToShortDateString() + " " + txtOverTimeTo.Text.toServerTime().Value.ToShortTimeString()),
                    InstanceID = this.InstanceID,
                    UserID = Convert.ToInt32(ddlUsers.SelectedValue.ToInt32()),
                    Type = "Undertime"
                };
                undertime.Add();
                List<int> adminIDs = SP.GetAdminIDByUserID(Convert.ToInt32(ddlUsers.SelectedValue.ToInt32()), InstanceID);
                AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
                foreach (int adminID in adminIDs) {
                    attendence_management adminUser = attendence_management.SingleOrDefault(u => u.ID == adminID);
                    attendence_management user = attendence_management.SingleOrDefault(u => u.ID == Convert.ToInt32(ddlUsers.SelectedValue.ToInt32()));
                    if (adminUser != null && user != null) {
                        string messagebody = user.Title + " added undertime from " + App.GetTime(undertime.FromTime.Value.ToString(), HttpContext.Current.User.Identity.Name) + " to " + App.GetTime(undertime.ToTime.Value.ToString(), HttpContext.Current.User.Identity.Name);
                        messagebody += "<br />";
                        messagebody += ((undertime.Comment != null) ? "<br /> <b>Reason:</b> " + undertime.Comment : "");
                        messagebody += "<br /> <b>From:</b> " + undertime.FromTime.toClientTime().Value.ToShortTimeString() + " to " + undertime.ToTime.toClientTime().Value.ToShortTimeString() + " - " + UtilityMethods.getFormatedTimeByMinutes(((undertime.ToTime - undertime.FromTime).ToTimeSpan().TotalMinutes.ToInt32()));
                        messagebody += "<br /> <b>Day:</b> " + undertime.FromTime.Value.DayOfWeek.ToString();
                        messagebody += "<br /> <b>Date:</b> " + undertime.FromTime.Value.ToString("MMMM, dd, yyyy");
                        messagebody += Helper.AvaimaEmailSignature;
                        emailAPI.send_email(adminUser.email, "Time & Attendance", "", messagebody, "Under-Time by " + user.Title);
                        emailAPI.send_email(user.email, "Time & Attendance", "", messagebody, "Under-Time by " + user.Title);
                        emailAPI.send_email(WebConfigurationManager.AppSettings["DevEamil"], "Time & Attendance", "", messagebody, "Under-Time by " + user.Title);
                    }
                }
            }
            btnGR_Click(sender, e);
            txtOverTimeFrom.Text = "";
            txtOverTimeTo.Text = "";
            txtOverTimeComment.Text = "";
        }
        catch (Exception ex) {
            //lblErrTitle.Text = ex.Message;
            //lblErrDetail.Text = ex.ToString();
            //ScriptManager.RegisterStartupScript(this, GetType(), "", "OpenDialogWB('#divErr');", true);
            //divError.Visible = true;
            //lblErrorMessage.Text = "Add overtime: " + ex.Message;
        }
    }

    public bool isWorker { get; set; }

    public bool isAdmin { get; set; }

    public string OwnerId { get; set; }

    public int appAssignedId { get; set; }

    public List<User> Users { get; set; }

    public bool isSuperUser { get; set; }

    public bool isDepAdmin { get; set; }

}