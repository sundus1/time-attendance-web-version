﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="workerverification.aspx.cs" Inherits="workerverification" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" media="screen" href="_assets/style.css" />
    <link rel="stylesheet" href="_assets/jquery-ui.css" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="_assets/css/tooltipster.css" />
    <script type="text/javascript" src="_assets/js/jquery.tooltipster.js"></script>
    <link rel="stylesheet" href="_assets/jquery.timepicker.css" />
    <script src="_assets/jquery.timepicker.min.js"></script>
    <script src="_assets/DateTimeHelper.js"></script>
    <link href="_assets/jquery-ui-timepicker-addon.css" rel="stylesheet" />
    <script src="_assets/jquery-ui-timepicker-addon.js"></script>
    <script src="_assets/otf.js"></script>
    <script>
        function AddLeave() {
            var userid = $('#hdnAbsUserID').val();
            $('.txtAbsComments').val('');
            var dlg = $('.divAbs').dialog({
                modal: true,
                hide: {
                    effect: "blind"
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            return false;
        }

        $(document).ready(function () {
           
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:HiddenField ID="hdnAbsUserID" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="hdnAbsFlag" ClientIDMode="Static" runat="server" />

            <div id="divAbs" class="divAbs" style="display: none" title="Add leaves">
                <asp:HiddenField runat="server" ID="hdfAbsLogID" />
                <asp:HiddenField runat="server" ID="hdfActive" />
                <asp:HiddenField runat="server" ID="hdfUserID" />
                <table class="profile" style="margin-top: 10px">                    
                    <tr>
                        <td>
                            Provide your reason to be absent
                        </td>
                    </tr>
                    <tr>                        
                        <td>
                            <asp:TextBox ID="txtAbsComments" CssClass="txtAbsComments" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <%--<td colspan="2" style="text-align: center"><b>Your request will be emailed to your administrators for approval</b>
                        </td>--%>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right">
                            <asp:Button ID="btnAddLeave" CssClass="btnAddLeave raisesevent ui-state-default" runat="server" Text="Add" OnClick="btnAddLeave_Click"/>
                            <asp:Button ID="btnLeaveCancel" CssClass="btnLeaveCancel ui-state-default" runat="server" Text="Cancel" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
