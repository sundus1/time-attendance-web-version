﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
//using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// 
/// </summary>
public static class SP
{
    public static String GetTime(String datetime, String name)
    {
        return Convert.ToDateTime(datetime).ToString();
    }
    public static String GetDate(String datetime, String name)
    {
        return Convert.ToDateTime(datetime).ToShortTimeString();
    }
    public static String GetTimeReverse(String datetime, String name)
    {
        return Convert.ToDateTime(datetime).ToString();
    }
    public static void SetUserStatus(Int32 userid, Boolean active)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE attendence_management SET active = @active WHERE id = @userid", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@userid", userid.ToString());
                    cmd.Parameters.AddWithValue("@active", active);
                    cmd.ExecuteNonQuery();
                }
            }

        }
        catch (Exception ex)
        {
            //this.Redirect("Add_worker.aspx?id=" + UserId);
        }
    }
    public static Boolean IsWorkerActive(Int32 userID)
    {
        string queryString = "SELECT active FROM attendence_management WHERE id = @userid";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.Text;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userid", userID);
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                try
                {
                    ad.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return Convert.ToBoolean(ds.Tables[0].Rows[0][0]);
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {

                    return false;
                }

            }
        }
    }
    public static String GetWorkerRole(Int32 userID)
    {
        string queryString = "SELECT [role] FROM tbluserrole WHERE userid = @userid";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.Text;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userid", userID);
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0][0].ToString();
                }
                else
                {
                    return "not found";
                }
            }
        }
    }
    public static Int32 GetAdminCatID(Int32 userID)
    {
        string queryString = "SELECT P_PID FROM tblcattoadmin WHERE userid = @userid";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.Text;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userid", userID);
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return Convert.ToInt32(ds.Tables[0].Rows[0]["P_PID"].ToString());
                }
                else
                {
                    return 0;
                }
            }
        }
    }
    public static DateTime GetWorkerFirstSignInThisYear(String id, int year)
    {
        string queryString = "SELECT TOP(1) lastin FROM workertrans WHERE P_PID = @userid AND DATEPART(yyyy, lastin) = " + year.ToString() + " ORDER BY lastin";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.Text;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userid", id);
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DateTime attendDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["lastin"].ToString());
                    return attendDate;
                }
            }
            return DateTime.Now;
        }
    }
    public static DateTime GetWorkerFirstSignInn(String id)
    {
        string queryString = "SELECT TOP(1) lastin FROM workertrans WHERE P_PID = @userid ORDER BY lastin";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.Text;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userid", id);
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DateTime attendDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["lastin"].ToString());
                    return attendDate;
                }
                else
                {
                    return Helper.DefaultDateTime;
                }
            }
            return DateTime.Now;
        }
    }
    public static DateTime GetWorkerLastSignIn(String id)
    {
        string queryString = "SELECT TOP(1) lastin FROM workertrans WHERE P_PID = @userid ORDER BY rowid desc";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.Text;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userid", id);
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DateTime attendDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["lastin"].ToString());
                    return attendDate;
                }
            }
        }
        //return DateTime.Now;
        return new DateTime(1900, 1, 1);
    }
    public static DateTime GetWorkerLastSignOut(String id)
    {
        string queryString = "SELECT TOP(1) lastout FROM workertrans WHERE P_PID = @userid ORDER BY rowid desc";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.Text;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userid", id);
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        DateTime attendDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["lastout"].ToString());
                        return attendDate;
                    }
                    catch (Exception)
                    {
                        return new DateTime(1900, 1, 1);
                    }

                }
            }
        }
        //return DateTime.Now;
        return new DateTime(1900, 1, 1);
    }
    public static DateTime GetWorkerLastSignInThisYear(String id, int year)
    {
        string queryString = "SELECT TOP(1) lastin FROM workertrans WHERE P_PID = @userid AND DATEPART(yyyy, lastin) = " + year.ToString() + " ORDER BY lastin desc";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.Text;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userid", id);
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DateTime attendDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["lastin"].ToString());
                    return attendDate;
                }
            }
        }
        return DateTime.Now;
    }
    public static DateTime GetWorkerLastSignInDateTime(String id)
    {
        string queryString = "SELECT TOP(1) lastin FROM workertrans WHERE P_PID = @userid AND lastout is not null ORDER BY lastout desc";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.Text;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userid", id);
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        DateTime attendDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["lastin"].ToString());
                        return attendDate;
                    }
                    catch (Exception)
                    {
                        return new DateTime(1900, 1, 1);
                    }

                }
            }
        }
        //return DateTime.Now;
        return new DateTime(1900, 1, 1);
    }
    public static DateTime GetWorkerLastAMEdit(Int32 id)
    {
        string queryString = "SELECT datefield FROM attendence_management WHERE id = @userid";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.Text;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userid", id.ToString());
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                try
                {
                    ad.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return Convert.ToDateTime(ds.Tables[0].Rows[0][0]);
                    }
                    else
                    {
                        return Helper.DefaultDateTime;
                    }
                }
                catch (Exception)
                {

                    return Helper.DefaultDateTime;
                }

            }
        }

    }
    public static String GetWorkerName(String userID)
    {
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT title FROM attendence_management WHERE ID = @userID", connection))
            {
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userID", userID);
                command.Connection.Open();
                command.CommandType = CommandType.Text;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    String workerName = ds.Tables[0].Rows[0]["title"].ToString();
                    return workerName;
                }
            }
        }
        return "Not Found";
    }
    public static List<SubRecordModel> GetAllWorkerRecordById(String userID)
    {
        List<SubRecordModel> _recordModels = new List<SubRecordModel>();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("getallworkerrecordbyid", connection))
            {

                command.Parameters.Clear();
                command.Parameters.AddWithValue("@ppID", userID);
                command.Connection.Open();
                command.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRowView row in ds.Tables[0].Rows)
                    {
                        _recordModels.Add(new SubRecordModel()
                        {
                            Id = Convert.ToInt32(row["P_PID"]),
                            InComment = row["signin"].ToString(),
                            InLocation = row["in_location"].ToString(),
                            LoginStatus = row["instatus"].ToString(),
                            LoginTime = row["lastin"].ToString(),
                            LogoutStatus = row["outstatus"].ToString(),
                            LogoutTime = row["lastout"].ToString(),
                            OutComment = row["signout"].ToString(),
                            OutLocation = row["out_location"].ToString(),
                            RowTime = Convert.ToInt32(row["rowID"])
                        });

                    }
                    return _recordModels;
                }
                return null;
            }
        }
    }
    public static List<UserAttendanceStatus> GetUserAttendanceStatus(String id)
    {
        List<UserAttendanceStatus> _users = new List<UserAttendanceStatus>();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("getallworkerrecordbyid", connection))
            {

                command.Parameters.Clear();
                command.Parameters.AddWithValue("@ppID", id);
                command.Connection.Open();
                command.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        if (!String.IsNullOrEmpty(item["lastout"].ToString()))
                        {
                            //Signout Time
                            _users.Add(new UserAttendanceStatus(Convert.ToInt32(item["P_PID"].ToString()), "Last clock-out:", Convert.ToDateTime(item["lastout"]), item["outstatus"].ToString(), item["signin"].ToString()));
                        }
                        else
                        {
                            //SignIn Time
                            _users.Add(new UserAttendanceStatus(Convert.ToInt32(item["P_PID"].ToString()), "Clocked in at", Convert.ToDateTime(item["lastin"].ToString()), item["instatus"].ToString(), item["signin"].ToString(), item["rowID"].ToInt32()));
                        }
                    }
                    return _users;
                }
            }
        }
        return null;
    }
    internal static void ClockOut(AutoPresent obj, String UserHostAddress)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("insertuserouttime", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ppid", obj.UserID);
                    cmd.Parameters.AddWithValue("@rowid", "0");
                    cmd.Parameters.AddWithValue("@lastout", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, obj.ClockOutTime.Hour, obj.ClockOutTime.Minute, obj.ClockOutTime.Second));
                    cmd.Parameters.AddWithValue("@outaddress", ":1");
                    cmd.Parameters.AddWithValue("@comment", "Auto Clock-Out");
                    cmd.Parameters.AddWithValue("@location", "2");
                    int rowid = Convert.ToInt32(cmd.ExecuteScalar());
                    using (SqlCommand cmd1 = new SqlCommand("UPDATE attendence_management SET datefield=@date WHERE ID=@id", con))
                    {
                        cmd1.Parameters.AddWithValue("@date", Convert.ToDateTime(DateTime.Now));
                        cmd1.Parameters.AddWithValue("@id", obj.UserID);
                        cmd1.ExecuteNonQuery();
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    internal static void ClockOut(WorkingHour obj, String UserHostAddress)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("insertuserouttime", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ppid", obj.UserID);
                    cmd.Parameters.AddWithValue("@rowid", "0");
                    cmd.Parameters.AddWithValue("@lastout", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, obj.Clockout.Hour, obj.Clockout.Minute, obj.Clockout.Second));
                    cmd.Parameters.AddWithValue("@outaddress", ":1");
                    cmd.Parameters.AddWithValue("@comment", "Auto Clock-Out");
                    cmd.Parameters.AddWithValue("@location", "2");
                    int rowid = Convert.ToInt32(cmd.ExecuteScalar());
                    using (SqlCommand cmd1 = new SqlCommand("UPDATE attendence_management SET datefield=@date WHERE ID=@id", con))
                    {
                        cmd1.Parameters.AddWithValue("@date", Convert.ToDateTime(DateTime.Now));
                        cmd1.Parameters.AddWithValue("@id", obj.UserID);
                        cmd1.ExecuteNonQuery();
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    internal static void ClockOutVerified(WorkingHour obj, String UserHostAddress)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("[insertuserouttimeverified]", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ppid", obj.UserID);
                    cmd.Parameters.AddWithValue("@rowid", "0");
                    cmd.Parameters.AddWithValue("@lastout", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, obj.Clockout.Hour, obj.Clockout.Minute, obj.Clockout.Second));
                    cmd.Parameters.AddWithValue("@outaddress", ":1");
                    cmd.Parameters.AddWithValue("@comment", "Auto Clock-Out");
                    cmd.Parameters.AddWithValue("@location", "1");
                    int rowid = Convert.ToInt32(cmd.ExecuteScalar());
                    using (SqlCommand cmd1 = new SqlCommand("UPDATE attendence_management SET datefield=@date WHERE ID=@id", con))
                    {
                        cmd1.Parameters.AddWithValue("@date", Convert.ToDateTime(DateTime.Now));
                        cmd1.Parameters.AddWithValue("@id", obj.UserID);
                        cmd1.ExecuteNonQuery();
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    internal static void ClockIn(AutoPresent obj, String UserHostAddress)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("insertuserintime", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ppid", obj.UserID);
                    cmd.Parameters.AddWithValue("@userid", "uid");
                    cmd.Parameters.AddWithValue("@lastin", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, obj.ClockInTime.Hour, obj.ClockInTime.Minute, obj.ClockInTime.Second));
                    cmd.Parameters.AddWithValue("@inaddress", UserHostAddress);
                    cmd.Parameters.AddWithValue("@comment", "Auto Clock-In");
                    cmd.Parameters.AddWithValue("@location", "2");
                    int rowid = Convert.ToInt32(cmd.ExecuteScalar());
                    //Session["siID"] = rowid;
                    //Session["soID"] = null;
                    //string messagebody = "<a href='http://www.avaima.com/714fcd17-288b-4725-a0c2-7641c997eda4/workerverification.aspx?action=in&rowid=" + rowid + "&pid=" + UserId + "&iid=" + this.InstanceID + "'>Click here </a> to verify signin time for" + DateTime.Now.ToString("MM/dd/yyyy") + ". This email is valid for 15 min after the time of delieverd";


                    using (SqlCommand cmd1 = new SqlCommand("UPDATE attendence_management SET datefield=@date WHERE ID=@id", con))
                    {
                        cmd1.Parameters.AddWithValue("@date", Convert.ToDateTime(DateTime.Now));
                        cmd1.Parameters.AddWithValue("@id", obj.UserID);
                        cmd1.ExecuteNonQuery();
                    }

                    //AvaimaEmailAPI email = new AvaimaEmailAPI();
                    //email.send_email(dtcontractors.Rows[0]["email"].ToString(), "Worker", "", messagebody, "Verification signin email");

                    //btnsignin.Attributes.Add("Disabled", "Disabled");
                }
            }

        }
        catch (Exception ex)
        {
            //this.Redirect("Add_worker.aspx?id=" + UserId);
        }
    }
    internal static void ClockIn(WorkingHour obj, String UserHostAddress)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("insertuserintime", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ppid", obj.UserID);
                    cmd.Parameters.AddWithValue("@userid", "uid");
                    cmd.Parameters.AddWithValue("@lastin", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, obj.Clockin.Hour, obj.Clockin.Minute, obj.Clockin.Second));
                    cmd.Parameters.AddWithValue("@inaddress", UserHostAddress);
                    cmd.Parameters.AddWithValue("@comment", "Auto Clock-In");
                    cmd.Parameters.AddWithValue("@location", "2");
                    int rowid = Convert.ToInt32(cmd.ExecuteScalar());

                    using (SqlCommand cmd1 = new SqlCommand("UPDATE attendence_management SET datefield=@date WHERE ID=@id", con))
                    {
                        cmd1.Parameters.AddWithValue("@date", Convert.ToDateTime(DateTime.Now));
                        cmd1.Parameters.AddWithValue("@id", obj.UserID);
                        cmd1.ExecuteNonQuery();
                    }
                }
            }

        }
        catch (Exception ex)
        {
            //this.Redirect("Add_worker.aspx?id=" + UserId);
        }
    }
    internal static void ClockInVerified(WorkingHour obj, String UserHostAddress)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("[insertuserintimeverified]", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ppid", obj.UserID);
                    cmd.Parameters.AddWithValue("@userid", "uid");
                    cmd.Parameters.AddWithValue("@lastin", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, obj.Clockin.Hour, obj.Clockin.Minute, obj.Clockin.Second));
                    cmd.Parameters.AddWithValue("@inaddress", UserHostAddress);
                    cmd.Parameters.AddWithValue("@comment", "Auto Clock-In");
                    cmd.Parameters.AddWithValue("@location", "1");
                    int rowid = Convert.ToInt32(cmd.ExecuteScalar());

                    using (SqlCommand cmd1 = new SqlCommand("UPDATE attendence_management SET datefield=@date WHERE ID=@id", con))
                    {
                        cmd1.Parameters.AddWithValue("@date", Convert.ToDateTime(DateTime.Now));
                        cmd1.Parameters.AddWithValue("@id", obj.UserID);
                        cmd1.ExecuteNonQuery();
                    }
                }
            }

        }
        catch (Exception ex)
        {
            //this.Redirect("Add_worker.aspx?id=" + UserId);
        }
    }
    public static String GetEmailByUserID(Int32 userID)
    {
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT email FROM attendence_management WHERE ID = @userID", connection))
            {

                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userID", userID);
                command.Connection.Open();
                command.CommandType = CommandType.Text;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    String email = ds.Tables[0].Rows[0][0].ToString();
                    return email;
                }
            }
        }
        return "Not Found";
    }
    /// <summary>
    /// Returns UserWorkHours
    /// </summary>
    /// <param name="instanceID"></param>
    /// <param name="userID"></param>
    /// <param name="datetime"></param>
    /// <returns></returns>
    public static Int32 GetTodayUserWorkHours(string instanceID, Int32 userID, DateTime datetime)
    {
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("[GetTodayUserWorkHours]", connection))
            {
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@Id", userID);
                command.Parameters.AddWithValue("@InstanceId", instanceID);
                command.Parameters.AddWithValue("@Date", datetime.Date);
                command.Parameters.AddWithValue("@day", datetime.DayOfWeek.ToString());
                command.Connection.Open();
                command.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        Int32 hrs = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
                        return hrs;
                    }
                    catch (Exception)
                    {
                        return 0;
                    }

                }
            }
        }
        return 0;
    }
    public static Int32 GetTodayUserTotHours(string instanceID, Int32 userID, DateTime datetime)
    {
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("[GetTodayUserTotHours]", connection))
            {
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@Id", userID);
                command.Parameters.AddWithValue("@InstanceId", instanceID);
                command.Parameters.AddWithValue("@Date", datetime.Date);
                command.Parameters.AddWithValue("@day", datetime.DayOfWeek.ToString());
                command.Connection.Open();
                command.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        Int32 hrs = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
                        return hrs;
                    }
                    catch (Exception)
                    {
                        return 0;
                    }

                }
            }
        }
        return 0;
    }
    public static Int32 GetUserBreakTimeDiff(Int32 userID, string instanceID, DateTime datetime)
    {
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("getTodaysBreakHours", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@Id", userID);
                command.Parameters.AddWithValue("@InstanceId", instanceID);
                command.Parameters.AddWithValue("@Date", datetime.Date);
                command.Connection.Open();
                command.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {

                    Int32 hrs = 0;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        try
                        {
                            hrs += Convert.ToInt32(row[0].ToString());

                        }
                        catch (Exception)
                        {

                            hrs += 0;
                        }
                    }
                    return hrs;
                }
            }
            return 0;

        }


    }

    public static TimeSpan GetTodaysBreak(string instanceID, DateTime datetime)
    {
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("Gettodaysbreakacc", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@InstanceId", instanceID);
                command.Parameters.AddWithValue("@Date", datetime.Date);
                command.Connection.Open();
                command.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Int32 minutes = 0;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        try
                        {
                            minutes += Convert.ToInt32(row["btimeinminutes"].ToString());

                        }
                        catch (Exception)
                        {

                            minutes += 0;
                        }
                    }

                    TimeSpan ts = new TimeSpan(0, minutes, 0);
                    return ts;
                }
            }
            return new TimeSpan();
        }

    }

    public static DataTable GetHolidays(string instanceID)
    {
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("[getHolidays]", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@instanceId", instanceID);
                command.Parameters.AddWithValue("@id", DBNull.Value);
                command.Connection.Open();
                command.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0];
                }
            }
        }
        return new DataTable();
    }
    public static void AddLeave(string AbsLogID, string UserID, string Date, string Active, string Comment)
    {

        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            if (Convert.ToInt32(AbsLogID) > 0)
            {
                // Update
                using (SqlCommand command = new SqlCommand("UPDATE [Absences] SET" +
               "[UserID] = @UserID,[CrtDate] = @CrtDate,[ModDate] = @ModDate,[Active] = @Active,[Comment] = @Comment WHERE AbsLogID = @AbsLogID", connection))
                {
                    command.Parameters.AddWithValue("@AbsLogID", AbsLogID);
                    command.Parameters.AddWithValue("@UserID", UserID);
                    command.Parameters.AddWithValue("@CrtDate", Convert.ToDateTime(Date));
                    command.Parameters.AddWithValue("@ModDate", Convert.ToDateTime(Date));
                    command.Parameters.AddWithValue("@Active", Active);
                    command.Parameters.AddWithValue("@Comment", Comment);
                    connection.Open();
                    command.ExecuteNonQuery();
                }
            }
            else
            {
                // Save
                using (SqlCommand command = new SqlCommand("INSERT INTO [Absences] " +
               "([UserID],[CrtDate],[ModDate],[Active],[Comment]) VALUES(@UserID,@CrtDate,@ModDate,@Active,@Comment)", connection))
                {
                    //string date = Convert.ToDateTime(Date).ToString("dd/MM/yyyy");
                    command.Parameters.AddWithValue("@UserID", UserID);
                    command.Parameters.AddWithValue("@CrtDate", Date);
                    command.Parameters.AddWithValue("@ModDate", Date);
                    command.Parameters.AddWithValue("@Active", Active);
                    command.Parameters.AddWithValue("@Comment", Comment);
                    connection.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

    }
    public static DataTable GetIPAddresses(String instanceID)
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("getipadress", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@instanceId", instanceID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }
        }
        return dt;
    }
    public static List<Int32> GetAdminIDsByPID(Int32 parentID, string instanceID)
    {
        List<int> ids = new List<int>();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT userid FROM tblcattoadmin WHERE p_pid = @p_pid AND instanceID = @instanceID", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@p_pid", parentID);
                command.Parameters.AddWithValue("@instanceID", instanceID);
                command.Connection.Open();
                command.CommandType = CommandType.Text;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ids.Add(Convert.ToInt32(ds.Tables[0].Rows[0][0]));
                }
                else
                {
                    using (SqlCommand selsuperadmincommand = new SqlCommand("SELECT userid FROM tbluserrole WHERE role = 'superuser' and instanceID= @instanceid", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@instanceID", instanceID);
                        command.Connection.Open();
                        command.CommandType = CommandType.Text;
                        ds = new DataSet();
                        ad = new SqlDataAdapter(command);
                        ad.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ids.Add(Convert.ToInt32(ds.Tables[0].Rows[0][0]));
                        }
                    }
                }
            }
        }
        return ids;
    }
    public static List<Int32> GetAdminIDByUserID(Int32 userID, string instanceID)
    {
        List<int> ids = new List<int>();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT userid FROM tblcattoadmin WHERE p_pid IN " +
                                                                               "(" +
                                                                                    "SELECT " +
                                                                                        "a.P_ID " +
                                                                                    "FROM " +
                                                                                        "attendence_management a" +
                                                                                    " WHERE" +
                                                                                        " a.ID = @userID" +
                                                                                ")", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userID", userID);
                command.Parameters.AddWithValue("@instanceID", instanceID);
                command.Connection.Open();
                command.CommandType = CommandType.Text;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ids.Add(Convert.ToInt32(ds.Tables[0].Rows[0][0]));
                }
                else
                {
                    using (SqlCommand selsuperadmincommand = new SqlCommand("SELECT userid FROM tbluserrole WHERE role = 'superuser' and instanceID= @instanceid", connection))
                    {
                        selsuperadmincommand.CommandType = CommandType.StoredProcedure;
                        selsuperadmincommand.Parameters.Clear();
                        selsuperadmincommand.Parameters.AddWithValue("@instanceid", instanceID);
                        selsuperadmincommand.CommandType = CommandType.Text;
                        ds = new DataSet();
                        ad = new SqlDataAdapter(selsuperadmincommand);
                        ad.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ids.Add(Convert.ToInt32(ds.Tables[0].Rows[0][0]));
                        }
                    }
                }
            }
        }
        return ids;
    }
    /// <summary>
    /// Get OwnerID from tbluserrole of specific user by UserID.
    /// Returns null if not found
    /// </summary>
    /// <param name="userID">userID of user based on attendence_management ID</param>
    /// <returns>String OwnerID</returns>
    public static String GetOwnerID(Int32 userID)
    {
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT ownerid FROM tbluserrole WHERE userid = @userID", connection))
            {

                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userID", userID);
                command.Connection.Open();
                command.CommandType = CommandType.Text;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    String ownerID = ds.Tables[0].Rows[0][0].ToString();
                    return ownerID;
                }
            }
        }
        return null;
    }
    public static List<String> GetAllInstanceIDs()
    {
        List<String> instanceIDs = new List<string>();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT DISTINCT instanceid FROM attendence_management WHERE instanceid <> '0' OR instanceid <> null", connection))
            {
                command.Connection.Open();
                command.CommandType = CommandType.Text;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if (!String.IsNullOrEmpty(row[0].ToString()))
                    {
                        instanceIDs.Add(row[0].ToString());
                    }
                }
            }
        }
        return instanceIDs;
    }
}

public static class Helper
{
    public static String AvaimaEmailSignature
    {
        get
        {
            StringBuilder body = new StringBuilder();
            body.Append("<br><br><a href='http://avaima.com'>AVAIMA.COM</a>");
            body.Append("<br><i>Organize your work!</i>");
            body.Append("<br>For questions email <a href='mailto:support@avaima.com'>support@avaima.com</a>");
            body.Append("<br>For information contact: <a href='mailto:info@avaima.com'>info@avaima.com</a>");
            //body.Append("<br>For support contact: <a href='mailto:support@avaima.com '>support@avaima.com </i>");
            return body.ToString();
        }
    }
    public static String DefaultDateFormat
    {
        get
        {
            return "ddd, MMM dd yyyy";
        }
    }
    public static DateTime DefaultDateTime
    {
        get
        {
            return new DateTime(1900, 1, 1);
        }
    }
}

public enum ExDayTypes
{
    PARTIAL_WORKING = 1, ABSENT = 2
}

public class UserAttendanceStatus
{
    public UserAttendanceStatus(Int32 p_pid, String status, DateTime lastDateTime, String inOutStatus, String comments, Int32 rowID = 0)
    {
        this.P_PID = p_pid;
        this.Status = status;
        this.LastDateTime = lastDateTime;
        this.Comments = comments;
        this.InOutStatus = inOutStatus;
        this.rowID = rowID;
    }
    public Int32 P_PID { get; set; }
    public String Status { get; set; }
    public DateTime LastDateTime { get; set; }
    public String InOutStatus { get; set; }
    public String Comments { get; set; }
    public Int32 rowID { get; set; }

}

public class AutoPresent
{
    public int AutoPresentID { get; set; }
    public int UserID { get; set; }
    public DateTime ClockInTime { get; set; }
    public DateTime ClockOutTime { get; set; }
    public Boolean Active { get; set; }

    public static List<AutoPresent> GetAll()
    {
        List<AutoPresent> aps = new List<AutoPresent>();
        string queryString = "SELECT * FROM AutoPresents";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.Text;
                command.Parameters.Clear();
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        aps.Add(new AutoPresent()
                        {
                            Active = Convert.ToBoolean(row["Active"].ToString()),
                            AutoPresentID = Convert.ToInt32(row["AutoPresentID"].ToString()),
                            ClockInTime = Convert.ToDateTime(row["ClockInTime"].ToString()),
                            ClockOutTime = Convert.ToDateTime(row["ClockOutTime"].ToString()),
                            UserID = Convert.ToInt32(row["UserID"].ToString())
                        });
                    }

                }
            }
            return aps;
        }
    }

    public static List<AutoPresent> GetByUserID(Int32 userID)
    {
        List<AutoPresent> aps = new List<AutoPresent>();
        string queryString = "SELECT * FROM AutoPresents WHERE userID = @userid";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.Text;
                command.Parameters.Clear();
                command.Parameters.Add("@userid", userID);
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        aps.Add(new AutoPresent()
                        {
                            Active = Convert.ToBoolean(row["Active"].ToString()),
                            AutoPresentID = Convert.ToInt32(row["AutoPresentID"].ToString()),
                            ClockInTime = Convert.ToDateTime(row["ClockInTime"].ToString()),
                            ClockOutTime = Convert.ToDateTime(row["ClockOutTime"].ToString()),
                            UserID = Convert.ToInt32(row["UserID"].ToString())
                        });
                    }

                }
            }
            return aps;
        }
    }

    public Int32 Add()
    {
        string queryString = "[InsertAutoPresent]";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Clear();
                command.Parameters.Add("@UserID", this.UserID);
                command.Parameters.Add("@ClockInTime", this.ClockInTime);
                command.Parameters.Add("@ClockOutTime", this.ClockOutTime);
                command.Parameters.Add("@Active", this.Active);
                command.Connection.Open();
                return command.ExecuteNonQuery();
            }
        }
    }
    public Int32 Update()
    {
        string queryString = "[UpdateAutoPresent]";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Clear();
                command.Parameters.Add("@AutoPresentID", this.AutoPresentID);
                command.Parameters.Add("@UserID", this.UserID);
                command.Parameters.Add("@ClockInTime", this.ClockInTime);
                command.Parameters.Add("@ClockOutTime", this.ClockOutTime);
                command.Parameters.Add("@Active", this.Active);
                command.Connection.Open();
                return command.ExecuteNonQuery();
            }
        }
    }
    public Int32 Delete()
    {
        string queryString = "DELETE AutoPresents"
                            + "WHERE AutoPresentID = @AutoPresentID";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.Text;
                command.Parameters.Clear();
                command.Parameters.Add("@AutoPresentID", this.UserID);
                command.Connection.Open();
                return command.ExecuteNonQuery();
            }
        }
    }
}

public class WorkingHour
{
    private AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
    public Int32 WorkHourID { get; set; }
    public Int32 UserID { get; set; }
    public String DayTitle { get; set; }
    public DateTime Clockin { get; set; }
    public DateTime Clockout { get; set; }
    public DateTime _userClockin
    {
        get
        {
            return Convert.ToDateTime(atz.GetTime(Clockin.ToString(), InstanceID).ToString());
        }
    }
    public DateTime _userClockout
    {
        get
        {
            return Convert.ToDateTime(atz.GetTime(Clockout.ToString(), InstanceID).ToString());
        }
    }
    public Boolean AutoPresent { get; set; }
    public DateTime crtDate { get; set; }
    public Boolean Active { get; set; }
    public String InstanceID { get; set; }
    public static List<WorkingHour> GetWorkingHours(Int32 userID)
    {
        List<WorkingHour> _WorkingHour = new List<WorkingHour>();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT * FROM WorkingHours WHERE [UserID] = @userID", connection))
            {
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userID", userID.ToString());
                command.Connection.Open();
                command.CommandType = CommandType.Text;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        _WorkingHour.Add(new WorkingHour()
                        {
                            WorkHourID = Convert.ToInt32(item[0]),
                            UserID = Convert.ToInt32(item[1]),
                            DayTitle = item[2].ToString(),
                            Clockin = Convert.ToDateTime(item[3]),
                            Clockout = Convert.ToDateTime(item[4]),
                            AutoPresent = Convert.ToBoolean(item[5].ToString()),
                            crtDate = Convert.ToDateTime(item[6].ToString()),
                            Active = Convert.ToBoolean(item[7].ToString()),
                            InstanceID = item[8].ToString()
                        });
                    }
                    return _WorkingHour;
                }
                else
                {
                    return _WorkingHour;
                }
            }
        }
    }
    public static List<WorkingHour> GetAll()
    {
        List<WorkingHour> _WorkingHour = new List<WorkingHour>();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT * FROM WorkingHours", connection))
            {
                command.Connection.Open();
                command.CommandType = CommandType.Text;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        _WorkingHour.Add(new WorkingHour()
                        {
                            WorkHourID = Convert.ToInt32(item[0]),
                            UserID = Convert.ToInt32(item[1]),
                            DayTitle = item[2].ToString(),
                            Clockin = Convert.ToDateTime(item[3]),
                            Clockout = Convert.ToDateTime(item[4]),
                            AutoPresent = Convert.ToBoolean(item[5].ToString()),
                            crtDate = Convert.ToDateTime(item[6].ToString()),
                            Active = Convert.ToBoolean(item[7].ToString()),
                            InstanceID = item[8].ToString()
                        });
                    }
                    return _WorkingHour;
                }
                else
                {
                    return _WorkingHour;
                }
            }
        }
    }
    public Int32 Save()
    {

        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            if (Convert.ToInt32(this.WorkHourID) > 0)
            {
                // Update
                using (SqlCommand command = new SqlCommand("UPDATE [WorkingHours] SET" +
               "[UserID] = @UserID,[DayTitle] = @DayTitle,[Clockin] = @Clockin,[Clockout] = @Clockout,[AutoPresent] = @AutoPresent, [crtDate]= @crtDate, [Active]=@Active, [InstanceID]=@InstanceID WHERE WorkHourID = @WorkHourID", connection))
                {
                    command.Parameters.AddWithValue("@WorkHourID", this.WorkHourID);
                    command.Parameters.AddWithValue("@UserID", this.UserID);
                    command.Parameters.AddWithValue("@DayTitle", this.DayTitle);
                    command.Parameters.AddWithValue("@Clockin", this.Clockin);
                    command.Parameters.AddWithValue("@Clockout", this.Clockout);
                    command.Parameters.AddWithValue("@AutoPresent", this.AutoPresent);
                    command.Parameters.AddWithValue("@crtDate", this.crtDate);
                    command.Parameters.AddWithValue("@Active", this.Active);
                    command.Parameters.AddWithValue("@InstanceID", this.InstanceID);
                    connection.Open();
                    return command.ExecuteNonQuery();
                }
            }
            else
            {
                // Save
                using (SqlCommand command = new SqlCommand("INSERT INTO [WorkingHours] " +
               "([UserID],[DayTitle],[Clockin],[Clockout],[AutoPresent],[crtDate],[Active],[InstanceID]) VALUES(@UserID,@DayTitle,@Clockin,@Clockout,@AutoPresent,@crtDate,@Active,@InstanceID)", connection))
                {
                    //string date = Convert.ToDateTime(Date).ToString("dd/MM/yyyy");
                    //command.Parameters.AddWithValue("@WorkHourID", this.WorkHourID);
                    command.Parameters.AddWithValue("@UserID", this.UserID);
                    command.Parameters.AddWithValue("@DayTitle", this.DayTitle);
                    command.Parameters.AddWithValue("@Clockin", this.Clockin);
                    command.Parameters.AddWithValue("@Clockout", this.Clockout);
                    command.Parameters.AddWithValue("@AutoPresent", this.AutoPresent);
                    command.Parameters.AddWithValue("@crtDate", this.crtDate);
                    command.Parameters.AddWithValue("@Active", this.Active);
                    command.Parameters.AddWithValue("@InstanceID", this.InstanceID);
                    connection.Open();
                    return command.ExecuteNonQuery();
                }
            }
        }
    }
    public static void Delete(Int32 UserID)
    {

        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {

            using (SqlCommand command = new SqlCommand("DELETE [WorkingHours] WHERE [UserID] = @UserID", connection))
            {
                command.Parameters.AddWithValue("@UserID", UserID);
                connection.Open();
                command.ExecuteNonQuery();
            }

        }
    }
}

public class Absence
{
    public Int32 AbsLogID { get; set; }
    public Int32 UserID { get; set; }
    public DateTime CrtDate { get; set; }
    public DateTime ModDate { get; set; }
    public Boolean Active { get; set; }
    public String Comment { get; set; }
    public String groupID { get; set; }
    public Boolean isAdjustment { get; set; }

    public static List<Absence> GetAbsences(Int32 UserID)
    {
        List<Absence> _absences = new List<Absence>();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT * FROM Absences WHERE [UserID] = @userID", connection))
            {
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userID", UserID);
                command.Connection.Open();
                command.CommandType = CommandType.Text;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        _absences.Add(new Absence()
                        {
                            AbsLogID = Convert.ToInt32(item[0]),
                            UserID = Convert.ToInt32(item[1]),
                            CrtDate = Convert.ToDateTime(item[2]),
                            ModDate = Convert.ToDateTime(item[3]),
                            Active = Convert.ToBoolean(item[4]),
                            Comment = item[5].ToString(),
                            groupID = item[6].ToString(),
                            isAdjustment = Convert.ToBoolean(item[7])
                        });
                    }
                    return _absences;
                }
                else
                {
                    return _absences;
                }
            }
        }
    }

    public static List<Absence> GetAll()
    {
        List<Absence> _absences = new List<Absence>();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT * FROM Absences", connection))
            {
                command.Connection.Open();
                command.CommandType = CommandType.Text;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        _absences.Add(new Absence()
                        {
                            AbsLogID = Convert.ToInt32(item[0]),
                            UserID = Convert.ToInt32(item[1]),
                            CrtDate = Convert.ToDateTime(item[2]),
                            ModDate = Convert.ToDateTime(item[3]),
                            Active = Convert.ToBoolean(item[4]),
                            Comment = item[5].ToString(),
                            groupID = item[6].ToString(),
                            isAdjustment = Convert.ToBoolean(item[7])
                        });
                    }
                    return _absences;
                }
                else
                {
                    return _absences;
                }
            }
        }
    }
}

public class ExDay
{
    public Int32 ExDayID { get; set; }
    public ExDayTypes ExDayTypeID { get; set; }
    public Int32 UserID { get; set; }
    public DateTime CrtDate { get; set; }
    public DateTime ModDate { get; set; }
    public Boolean Active { get; set; }
    public String Comment { get; set; }

    public static List<ExDay> GetExDays(Int32 UserID)
    {
        List<ExDay> exDays = new List<ExDay>();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT * FROM ExDays WHERE [UserID] = @userID", connection))
            {
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userID", UserID);
                command.Connection.Open();
                command.CommandType = CommandType.Text;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        exDays.Add(new ExDay()
                        {
                            ExDayID = Convert.ToInt32(item[0]),
                            ExDayTypeID = (ExDayTypes)Convert.ToInt32(item[1]),
                            UserID = Convert.ToInt32(item[2]),
                            CrtDate = Convert.ToDateTime(item[3]),
                            ModDate = Convert.ToDateTime(item[4]),
                            Active = Convert.ToBoolean(item[5]),
                            Comment = item[6].ToString()
                        });
                    }
                }
                return exDays;
            }
        }
    }
}

public class LatePresence
{
    public Int32 UserID { get; set; }
    public String UserName { get; set; }
    public Int32 Count { get; set; }
}

public class User
{
    public int userID { get; set; }
    public string userName { get; set; }
}

public class AttUser
{
    public Int32 UserID { get; set; }
    public Int32 ParentID { get; set; }
    public String UserName { get; set; }
    public String InstanceID { get; set; }
    public Boolean Category { get; set; }
    public String Email { get; set; }
    public Boolean Active { get; set; }
    public String Role { get; set; }
    public String OwnerID { get; set; }
    public String ClockType { get; set; }
    public Int32 AssignedAdminID { get; set; }
    public Int32 UserParent { get; set; }

    public static AttUser GetUserByID(int userID)
    {
        AttUser AttUsers = new AttUser();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT * FROM attendence_management where ID = @userid", connection))
            {
                command.Connection.Open();
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@userid", userID.ToString());

                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        AttUsers = new AttUser()
                        {
                            UserID = Convert.ToInt32(item["ID"]),
                            ParentID = Convert.ToInt32(item["P_ID"]),
                            UserName = Convert.ToString(item["Title"]),
                            InstanceID = Convert.ToString(item["InstanceId"]),
                            Category = Convert.ToBoolean(item["category"]),
                            Email = Convert.ToString(item["email"]),
                            Active = Convert.ToBoolean(item["active"]),
                            UserParent = Convert.ToInt32(item["parentid"]),
                        };
                    }

                    using (SqlCommand roleCommand = new SqlCommand("SELECT * FROM tbluserrole WHERE userid = @userid", connection))
                    {
                        roleCommand.CommandType = CommandType.Text;
                        roleCommand.Parameters.AddWithValue("@userid", AttUsers.UserID);
                        ds = new DataSet();
                        ad = new SqlDataAdapter(roleCommand);
                        ad.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            AttUsers.Role = ds.Tables[0].Rows[0]["role"].ToString();
                            AttUsers.OwnerID = ds.Tables[0].Rows[0]["ownerid"].ToString();
                            AttUsers.ClockType = ds.Tables[0].Rows[0]["clocktype"].ToString();
                        }
                    }

                    using (SqlCommand cattoadmincommand = new SqlCommand("SELECT P_PID FROM tblcattoadmin WHERE userid = @userid", connection))
                    {
                        cattoadmincommand.CommandType = CommandType.Text;
                        cattoadmincommand.Parameters.AddWithValue("@userid", AttUsers.UserID);
                        ds = new DataSet();
                        ad = new SqlDataAdapter(cattoadmincommand);
                        ad.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            AttUsers.AssignedAdminID = Convert.ToInt32(ds.Tables[0].Rows[0]["P_PID"]);
                        }
                        else
                        {
                            AttUsers.AssignedAdminID = 0;
                        }
                    }

                    return AttUsers;
                }
                else
                {
                    return AttUsers;
                }
            }
        }
    }

    public static List<AttUser> GetAll()
    {
        List<AttUser> AttUsers = new List<AttUser>();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT * FROM attendence_management where active is not null", connection))
            {
                command.Connection.Open();
                command.CommandType = CommandType.Text;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        AttUsers.Add(new AttUser()
                        {
                            UserID = Convert.ToInt32(item["ID"]),
                            ParentID = Convert.ToInt32(item["P_ID"]),
                            UserName = Convert.ToString(item["Title"]),
                            InstanceID = Convert.ToString(item["InstanceId"]),
                            Category = Convert.ToBoolean(item["category"]),
                            Email = Convert.ToString(item["email"]),
                            Active = Convert.ToBoolean(item["active"]),
                            UserParent = Convert.ToInt32(item["parentid"]),
                        });
                    }
                    foreach (AttUser User in AttUsers)
                    {
                        using (SqlCommand roleCommand = new SqlCommand("SELECT * FROM tbluserrole WHERE userid = @userid", connection))
                        {
                            roleCommand.CommandType = CommandType.Text;
                            roleCommand.Parameters.AddWithValue("@userid", User.UserID);
                            ds = new DataSet();
                            ad = new SqlDataAdapter(roleCommand);
                            ad.Fill(ds);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                User.Role = ds.Tables[0].Rows[0]["role"].ToString();
                                User.OwnerID = ds.Tables[0].Rows[0]["ownerid"].ToString();
                                User.ClockType = ds.Tables[0].Rows[0]["clocktype"].ToString();
                            }
                        }

                        using (SqlCommand cattoadmincommand = new SqlCommand("SELECT P_PID FROM tblcattoadmin WHERE userid = @userid", connection))
                        {
                            cattoadmincommand.CommandType = CommandType.Text;
                            cattoadmincommand.Parameters.AddWithValue("@userid", User.UserID);
                            ds = new DataSet();
                            ad = new SqlDataAdapter(cattoadmincommand);
                            ad.Fill(ds);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                User.AssignedAdminID = Convert.ToInt32(ds.Tables[0].Rows[0]["P_PID"]);
                            }
                            else
                            {
                                User.AssignedAdminID = 0;
                            }
                        }
                    }
                    return AttUsers;
                }
                else
                {
                    return AttUsers;
                }
            }
        }
    }

    public static List<AttUser> GetAll(string instanceID)
    {
        List<AttUser> AttUsers = new List<AttUser>();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT * FROM attendence_management where active is not null and instanceid = @instanceid", connection))
            {
                command.Connection.Open();
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@instanceid", instanceID);
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        AttUsers.Add(new AttUser()
                        {
                            UserID = Convert.ToInt32(item["ID"]),
                            ParentID = Convert.ToInt32(item["P_ID"]),
                            UserName = Convert.ToString(item["Title"]),
                            InstanceID = Convert.ToString(item["InstanceId"]),
                            Category = Convert.ToBoolean(item["category"]),
                            Email = Convert.ToString(item["email"]),
                            Active = Convert.ToBoolean(item["active"]),
                            UserParent = Convert.ToInt32(item["parentid"]),
                        });
                    }
                    foreach (AttUser User in AttUsers)
                    {
                        using (SqlCommand roleCommand = new SqlCommand("SELECT * FROM tbluserrole WHERE userid = @userid", connection))
                        {
                            roleCommand.CommandType = CommandType.Text;
                            roleCommand.Parameters.AddWithValue("@userid", User.UserID);
                            ds = new DataSet();
                            ad = new SqlDataAdapter(roleCommand);
                            ad.Fill(ds);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                User.Role = ds.Tables[0].Rows[0]["role"].ToString();
                                User.OwnerID = ds.Tables[0].Rows[0]["ownerid"].ToString();
                                User.ClockType = ds.Tables[0].Rows[0]["clocktype"].ToString();
                            }
                        }

                        using (SqlCommand cattoadmincommand = new SqlCommand("SELECT P_PID FROM tblcattoadmin WHERE userid = @userid", connection))
                        {
                            cattoadmincommand.CommandType = CommandType.Text;
                            cattoadmincommand.Parameters.AddWithValue("@userid", User.UserID);
                            ds = new DataSet();
                            ad = new SqlDataAdapter(cattoadmincommand);
                            ad.Fill(ds);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                User.AssignedAdminID = Convert.ToInt32(ds.Tables[0].Rows[0]["P_PID"]);
                            }
                            else
                            {
                                User.AssignedAdminID = 0;
                            }
                        }
                    }
                    return AttUsers;
                }
                else
                {
                    return AttUsers;
                }
            }
        }
    }

    public static List<AttUser> GetAll(int parentid)
    {
        List<AttUser> AttUsers = new List<AttUser>();
        string parentsql = "UNION SELECT * FROM attendence_management where ID = @parentid";

        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT * FROM attendence_management where active is not null and parentid = @parentid and isManaged = 1 "+parentsql, connection))
            {
                command.Connection.Open();
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@parentid", parentid);
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        AttUsers.Add(new AttUser()
                        {
                            UserID = Convert.ToInt32(item["ID"]),
                            ParentID = Convert.ToInt32(item["P_ID"]),
                            UserName = Convert.ToString(item["Title"]),
                            InstanceID = Convert.ToString(item["InstanceId"]),
                            Category = Convert.ToBoolean(item["category"]),
                            Email = Convert.ToString(item["email"]),
                            Active = Convert.ToBoolean(item["active"]),
                            UserParent = Convert.ToInt32(item["parentid"]),
                        });
                    }
                    foreach (AttUser User in AttUsers)
                    {
                        using (SqlCommand roleCommand = new SqlCommand("SELECT * FROM tbluserrole WHERE userid = @userid", connection))
                        {
                            roleCommand.CommandType = CommandType.Text;
                            roleCommand.Parameters.AddWithValue("@userid", User.UserID);
                            ds = new DataSet();
                            ad = new SqlDataAdapter(roleCommand);
                            ad.Fill(ds);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                User.Role = ds.Tables[0].Rows[0]["role"].ToString();
                                User.OwnerID = ds.Tables[0].Rows[0]["ownerid"].ToString();
                                User.ClockType = ds.Tables[0].Rows[0]["clocktype"].ToString();
                            }
                        }

                        using (SqlCommand cattoadmincommand = new SqlCommand("SELECT P_PID FROM tblcattoadmin WHERE userid = @userid", connection))
                        {
                            cattoadmincommand.CommandType = CommandType.Text;
                            cattoadmincommand.Parameters.AddWithValue("@userid", User.UserID);
                            ds = new DataSet();
                            ad = new SqlDataAdapter(cattoadmincommand);
                            ad.Fill(ds);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                User.AssignedAdminID = Convert.ToInt32(ds.Tables[0].Rows[0]["P_PID"]);
                            }
                            else
                            {
                                User.AssignedAdminID = 0;
                            }
                        }
                    }
                    return AttUsers;
                }
                else
                {
                    return AttUsers;
                }
            }
        }
    }

    public Boolean HasUserClockedIn()
    {
        string queryString = "SELECT TOP(1) lastin FROM workertrans WHERE P_PID = @userid ORDER BY lastin";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.Text;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userid", this.UserID.ToString());
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DateTime attendDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["lastin"].ToString());
                    return true;
                }
            }
            return false;
        }
    }
}

public class Holiday
{
    public Int32 id { get; set; }
    public String instanceId { get; set; }
    public DateTime date { get; set; }
    public Int32 day { get; set; }
    public Int32 type { get; set; }
    public String title { get; set; }
    public Boolean AvoidBreaks { get; set; }
    public Boolean Everyyear { get; set; }
    public Int32 userid { get; set; }

    public static List<Holiday> GetAll()
    {
        List<Holiday> holidays = new List<Holiday>();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT * FROM schema_6e815b00_6e13_4839_a57d_800a92809f21.tblholidays WHERE type=1", connection))
            {
                command.Connection.Open();
                command.CommandType = CommandType.Text;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        holidays.Add(new Holiday()
                        {
                            id = Convert.ToInt32(item["id"]),
                            instanceId = Convert.ToString(item["instanceId"]),
                            date = Convert.ToDateTime(item["date"]),
                            day = Convert.ToInt32(item["day"]),
                            type = Convert.ToInt32(item["type"]),
                            title = Convert.ToString(item["title"]),
                            Everyyear = Convert.ToBoolean(item["everyyear"]),
                            userid = Convert.ToInt32(item["userid"]),
                            //AvoidBreaks = Convert.ToBoolean(item["AvoidBreaks"]),
                        });
                    }
                    return holidays;
                }
                else
                {
                    return holidays;
                }
            }
        }
    }

    public static Boolean IsTodayHoliday_old(DateTime currDate)
    {
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT * FROM tblholidays WHERE date=@datetime", connection))
            {
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@datetime", currDate.Date);
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                try
                {
                    ad.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }

            }
        }

    }
    public static Boolean IsTodayHoliday(DateTime currDate)
    {
        string queryString = "[sp_getHolidaysByDate]";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@datetime", currDate.Date);
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                try
                {
                    ad.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }

            }
        }

    }

    public static Boolean IsTodayHoliday(DateTime currDate, string instanceID)
    {
        string queryString = "[getHolidaysByDate]";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@instanceId", instanceID);
                command.Parameters.AddWithValue("@datetime", currDate.Date);
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                try
                {
                    ad.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }

            }
        }

    }

    public static Boolean GetWeekenoffday(string dayval, string instanceID)
    {
        string queryString = "[GetWeekenoffday]";
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@instanceid", instanceID);
                command.Parameters.AddWithValue("@day", dayval);
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                try
                {
                    ad.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }

            }
        }

    }

    public static List<Holiday> GetTodaysHolidays(DateTime currDate, string instanceID)
    {
        string queryString = "[getHolidaysByDate]";
        List<Holiday> holidays = new List<Holiday>();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@instanceId", instanceID);
                command.Parameters.AddWithValue("@datetime", currDate.Date);
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                try
                {
                    ad.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            holidays.Add(new Holiday()
                            {
                                id = Convert.ToInt32(item["id"]),
                                instanceId = Convert.ToString(item["instanceId"]),
                                date = Convert.ToDateTime(item["date"]),
                                day = Convert.ToInt32(item["day"]),
                                type = Convert.ToInt32(item["type"]),
                                title = Convert.ToString(item["title"]),
                                //AvoidBreaks = Convert.ToBoolean(item["AvoidBreaks"]),
                            });
                        }
                    }
                }
                catch (Exception)
                {
                }
            }
            return holidays;
        }

    }
}


public class ExceptionalDay
{
    private AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();

    public Int32 EDID { get; set; }
    public Int32 UserID { get; set; }
    public String DayTitle { get; set; }
    public DateTime Date { get; set; }
    private DateTime _Date
    {
        get
        {
            return Convert.ToDateTime(atz.GetDate(Date.ToString(), InstanceID).ToString());
        }
    }
    public DateTime Clockin { get; set; }
    public DateTime Clockout { get; set; }
    private DateTime _userClockin
    {
        get
        {
            return Convert.ToDateTime(atz.GetTime(Clockin.ToString(), InstanceID).ToString());
        }
    }
    private DateTime _userClockout
    {
        get
        {
            return Convert.ToDateTime(atz.GetTime(Clockout.ToString(), InstanceID).ToString());
        }
    }
    public String GroupID { get; set; }
    public String GroupName { get; set; }
    public DateTime CrtDate { get; set; }
    public DateTime ModDate { get; set; }
    public Boolean Active { get; set; }
    public String InstanceID { get; set; }
    public Boolean AvoidBreaks { get; set; }
    public static List<ExceptionalDay> GetExceptionalDays(Int32 userID, String InstanceID)
    {
        List<ExceptionalDay> _ExceptionalDays = new List<ExceptionalDay>();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT * FROM ExceptionalDays WHERE [UserID] = @userID AND [InstanceID] = @InstanceID", connection))
            {
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@userID", userID.ToString());
                command.Parameters.AddWithValue("@InstanceID", InstanceID.ToString());
                command.Connection.Open();
                command.CommandType = CommandType.Text;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        _ExceptionalDays.Add(new ExceptionalDay()
                        {
                            EDID = Convert.ToInt32(item["EDID"]),
                            UserID = Convert.ToInt32(item["UserID"]),
                            DayTitle = item["DayTitle"].ToString(),
                            Clockin = Convert.ToDateTime(item["Clockin"]),
                            Clockout = Convert.ToDateTime(item["Clockout"]),
                            GroupID = item["GroupID"].ToString(),
                            GroupName = item["GroupName"].ToString(),
                            CrtDate = Convert.ToDateTime(item["CrtDate"].ToString()),
                            ModDate = Convert.ToDateTime(item["ModDate"].ToString()),
                            Active = Convert.ToBoolean(item["Active"].ToString()),
                            InstanceID = item["InstanceID"].ToString(),
                            Date = Convert.ToDateTime(item["Date"].ToString())
                        });
                    }
                    return _ExceptionalDays;
                }
                else
                {
                    return _ExceptionalDays;
                }
            }
        }
    }
    public static DataSet GetExceptionalDaysGroup(Int32 userID, String InstanceID)
    {
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("GetExpectionalDays", connection))
            {
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@UserID", userID.ToString());
                command.Parameters.AddWithValue("@InstanceID", InstanceID.ToString());
                command.CommandType = CommandType.StoredProcedure;
                command.Connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                return ds;
            }
        }
    }
    public static List<ExceptionalDay> GetAll(String InstanceID)
    {
        List<ExceptionalDay> _ExceptionalDay = new List<ExceptionalDay>();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand command = new SqlCommand("SELECT * FROM ExceptionalDays WHERE [InstanceID] = @InstanceID", connection))
            {
                command.Connection.Open();
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@InstanceID", InstanceID.ToString());
                command.CommandType = CommandType.Text;
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter(command);
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        _ExceptionalDay.Add(new ExceptionalDay()
                        {
                            EDID = Convert.ToInt32(item["EDID"]),
                            UserID = Convert.ToInt32(item["UserID"]),
                            DayTitle = item["DayTitle"].ToString(),
                            Clockin = Convert.ToDateTime(item["Clockin"]),
                            Clockout = Convert.ToDateTime(item["Clockout"]),
                            GroupID = item["GroupID"].ToString(),
                            GroupName = item["GroupName"].ToString(),
                            CrtDate = Convert.ToDateTime(item["CrtDate"].ToString()),
                            ModDate = Convert.ToDateTime(item["ModDate"].ToString()),
                            Active = Convert.ToBoolean(item["Active"].ToString()),
                            InstanceID = item["InstanceID"].ToString(),
                        });
                    }
                    return _ExceptionalDay;
                }
                else
                {
                    return _ExceptionalDay;
                }
            }
        }
    }
    public static void Delete(String GroupID)
    {
        List<ExceptionalDay> _ExceptionalDay = new List<ExceptionalDay>();
        try
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
            {
                using (SqlCommand command = new SqlCommand("DELETE ExceptionalDays WHERE [GroupID] = @GroupID", connection))
                {
                    command.Connection.Open();
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@GroupID", GroupID);
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public Int32 Save()
    {

        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            if (Convert.ToInt32(this.EDID) > 0)
            {
                // Update
                using (SqlCommand command = new SqlCommand("UPDATE [ExceptionalDays] SET" +
               "[UserID] = @UserID,[DayTitle] = @DayTitle,[Clockin] = @Clockin,[Clockout] = @Clockout,[GroupID] = @GroupID,[GroupName] = @GroupName, [CrtDate]= @CrtDate, [ModDate]= @ModDate, [Active]=@Active, [InstanceID]=@InstanceID, [Date] = @Date, [AvoidBreaks] = @AvoidBreaks WHERE WorkHourID = @WorkHourID", connection))
                {
                    command.Parameters.AddWithValue("@EDID", this.EDID);
                    command.Parameters.AddWithValue("@UserID", this.UserID);
                    command.Parameters.AddWithValue("@DayTitle", this.DayTitle);
                    command.Parameters.AddWithValue("@Clockin", this.Clockin);
                    command.Parameters.AddWithValue("@Clockout", this.Clockout);
                    command.Parameters.AddWithValue("@GroupID", this.GroupID);
                    command.Parameters.AddWithValue("@GroupName", this.GroupName);
                    command.Parameters.AddWithValue("@CrtDate", this.CrtDate);
                    command.Parameters.AddWithValue("@ModDate", this.ModDate);
                    command.Parameters.AddWithValue("@Active", this.Active);
                    command.Parameters.AddWithValue("@InstanceID", this.InstanceID);
                    command.Parameters.AddWithValue("@Date", this.Date);
                    command.Parameters.AddWithValue("@AvoidBreaks", this.AvoidBreaks);
                    connection.Open();
                    return command.ExecuteNonQuery();
                }
            }
            else
            {
                // Save
                using (SqlCommand command = new SqlCommand("INSERT INTO [ExceptionalDays] " +
               "([UserID],[DayTitle],[Clockin],[Clockout],[GroupID],[GroupName],[CrtDate],[ModDate],[Active],[InstanceID],[Date],[AvoidBreaks]) VALUES(@UserID,@DayTitle,@Clockin,@Clockout,@GroupID,@GroupName,@CrtDate,@ModDate,@Active,@InstanceID,@Date,@AvoidBreaks)", connection))
                {
                    //string date = Convert.ToDateTime(Date).ToString("dd/MM/yyyy");
                    //command.Parameters.AddWithValue("@WorkHourID", this.WorkHourID);
                    command.Parameters.AddWithValue("@UserID", this.UserID);
                    command.Parameters.AddWithValue("@DayTitle", this.DayTitle);
                    command.Parameters.AddWithValue("@Clockin", this.Clockin);
                    command.Parameters.AddWithValue("@Clockout", this.Clockout);
                    command.Parameters.AddWithValue("@GroupID", this.GroupID);
                    command.Parameters.AddWithValue("@GroupName", this.GroupName);
                    command.Parameters.AddWithValue("@CrtDate", DateTime.Now);
                    command.Parameters.AddWithValue("@ModDate", DateTime.Now);
                    command.Parameters.AddWithValue("@Active", this.Active);
                    command.Parameters.AddWithValue("@InstanceID", this.InstanceID);
                    command.Parameters.AddWithValue("@Date", this.Date);
                    command.Parameters.AddWithValue("@AvoidBreaks", this.AvoidBreaks);
                    connection.Open();
                    return command.ExecuteNonQuery();
                }
            }
        }
    }
}
