﻿using MyAttSys;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;

/// <summary>
/// Summary description for UtilityMethods
/// </summary>
public static class UtilityMethods
{
    public static Dictionary<string, int> Namazi2014Avaib
    {
        get
        {
            Dictionary<string, int> na = new Dictionary<string, int>();
            na.Add("AnamIqbal", 178);
            na.Add("Rao Arsalan", 179);
            na.Add("Tasleem Tunio", 181);
            na.Add("Fatima Danish", 182);
            na.Add("Mobin Molai", 197);
            na.Add("Usman", 201);
            na.Add("Shafat", 209);
            na.Add("Haseeb", 1312);
            na.Add("Afaq", 1335);
            na.Add("Haris", 4407);
            na.Add("Naufal", 4408);
            na.Add("Shahzaib Khan", 4437);
            na.Add("Mehvish Iqbal", 4467);
            return na;
            //na.Add("",)
        }
    }
    public static DateTime getClientDateTime(DateTime dateTime)
    {
        AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
        String localDate = objATZ.GetDate(Convert.ToString(dateTime.ToString()), HttpContext.Current.User.Identity.Name);

        String localTime = objATZ.GetTime(Convert.ToString(dateTime.ToString()), HttpContext.Current.User.Identity.Name);
        DateTime tempDateTime1;
        if (localDate.Contains("Today")) {
            tempDateTime1 = DateTime.Now;
        }
        else {
            tempDateTime1 = Convert.ToDateTime(localDate);
        }
        //new DateTime(tempDateTime1.Year, tempDateTime1.Month, tempDateTime1.Day, Convert.ToInt32(lblSigninTime.Text.Split(' ')[0].Split(':')[0]), Convert.ToInt32(lblSigninTime.Text.Split(' ')[0].Split(':')[1]), 0);
        DateTime tempDateTime;
        return tempDateTime = Convert.ToDateTime(tempDateTime1.ToShortDateString() + " " + Convert.ToDateTime(localTime).ToShortTimeString());
    }
    public static List<string> getpastDates(int days)
    {
        List<string> dates = new List<string>();
        for (int i = 0; i >= -days; i--) {
            dates.Add(DateTime.Now.AddDays(i).ToString("yyyy-MM-dd"));
        }
        return dates;
    }

    public static string getFormatedTimeByMinutes(int minutes)
    {
        int h = (int)(minutes / 60);
        int m = minutes - (h * 60);
        return ((h > 0) ? h.ToString() + " hrs " + m.ToString() + " mins" : m.ToString() + " mins");
    }

    public static List<RecordModel> getRecords(DataSet ds)
    {
        // Clock in/out records
        DataTable record1 = ds.Tables[0];
        // Break records
        DataTable record2 = ds.Tables[1];
        // Undertime records
        DataTable record3 = ds.Tables[2];
        // Overtime records
        DataTable record4 = ds.Tables[3];
        //Working hours
        DataTable record5 = ds.Tables[4];
        // User and InstanceID
        DataTable record6 = ds.Tables[5];
        // Merge Overtime in in/out record
        //ds.Tables[0].Merge(ds.Tables[6]);


        //MyAttSys.avaimaTest0001DB db = new avaimaTest0001DB();        
        //if (record6.Rows.Count > 0)
        //{
        //    MyAttSys.WorkingHour workingHour = db.GetWorkingHourOfUser(Convert.ToInt32(hdnUserID.Value), InstanceID, DateTime.Now.DayOfWeek.ToString()).ToWorkingHour();
        //}

        List<RecordModel> records = new List<RecordModel>();
        records = record2.Select().GroupBy(x => x.Field<DateTime>("Bdate")).Select(y => new RecordModel() {
            Date = y.Key,
            Records = record1.Select().Where(z => z.Field<DateTime>("Date") == y.Key).Select(
            c => new SubRecordModel() {
                Id = c.Field<int>("rowId"),
                LoginTime = c.Field<DateTime?>("lastin").ToString(),
                LoginStatus = c.Field<string>("instatus"),
                LogoutTime = c.Field<DateTime?>("lastout").ToString(),
                LogoutStatus = c.Field<string>("outstatus"),
                InComment = c.Field<string>("signin"),
                OutComment = c.Field<string>("signout"),
                Time = c.Field<string>("time"),
                RowTime = c.Field<int>("rowTime"),
                InLocation = c.Field<string>("in_location"),
                OutLocation = c.Field<string>("out_location"),
                LoginTimeDT = c.Field<DateTime?>("lastin"),
                LogoutTimeDT = c.Field<DateTime?>("lastout"),
                InstanceID = c.Field<String>("InstanceID"),
            }).ToList(),
            DayTitle = y.First().Field<string>("DayTitle"),
            DayType = y.First().Field<int?>("DayType"),
            Breaks = record2.Select().Where(z => z.Field<DateTime>("Bdate") == y.Key).Select(
            t => new BreakModel() {
                Id = t.Field<int?>("Bid"),
                BreakTitle = t.Field<string>("Btitle"),
                BreakStartTime = t.Field<TimeSpan?>("BfromTime"),
                BreakEndTime = t.Field<TimeSpan?>("BtoTime"),
                IsModified = t.Field<object>("BisModified"),
                Time = t.Field<double?>("BtimeInMinutes")
            }).ToList(),
            Undertimes = record3.Select().Where(u => u.Field<DateTime>("FromTime").Date == y.Key.Date).Select(u => new UnderTime() {
                Active = u.Field<Boolean?>("Active"),
                Comment = u.Field<String>("Comment"),
                FromTime = u.Field<DateTime?>("FromTime"),
                InstanceID = u.Field<String>("InstanceID"),
                ToTime = u.Field<DateTime?>("ToTime"),
                Type = u.Field<String>("Type"),
                UserID = u.Field<int?>("UserID"),
                UTID = u.Field<int>("UTID"),
            }).ToList(),
            Overtimes = record4.Select().Where(u => u.Field<DateTime>("FromTime").Date == y.Key.Date).Select(u => new OverTime() {
                Active = u.Field<Boolean?>("Active"),
                Comment = u.Field<String>("Comment"),
                FromTime = u.Field<DateTime?>("FromTime"),
                InstanceID = u.Field<String>("InstanceID"),
                ToTime = u.Field<DateTime?>("ToTime"),
                Type = u.Field<String>("Type"),
                UserID = u.Field<int?>("UserID"),
                OTID = u.Field<int>("OTID"),
            }).ToList(),
            workingHour = record5.Select().Select(u => new MyAttSys.WorkingHour() {
                Active = u.Field<Boolean?>("Active"),
                InstanceID = u.Field<String>("InstanceID"),
                UserID = u.Field<int?>("UserID"),
                AutoPresent = u.Field<Boolean?>("AutoPresent"),
                Clockin = u.Field<DateTime?>("Clockin"),
                Clockout = u.Field<DateTime?>("Clockout"),
                crtDate = u.Field<DateTime?>("crtDate"),
                DayTitle = u.Field<string>("DayTitle"),
                WorkHourID = u.Field<int>("WorkHourID"),
            }).ToList().SingleOrDefault(u => u.DayTitle.ToString().Contains(y.Key.DayOfWeek.ToString())),
            InstanceID = record6.Rows[0]["InstanceID"].ToString(),
            UserID = record6.Rows[0]["UserID"].ToInt32(),
        }).ToList();
        return records;
    }

    public static String GetFormattedDifference(decimal timeInMilliSeconds)
    {
        string returnString = "";
        //decimal days = Math.Floor(timeInMilliSeconds / 86400);
        //decimal hours = Math.Floor(timeInMilliSeconds / 3600) % 24;
        decimal hours = Math.Floor(timeInMilliSeconds / 3600);
        decimal minutes = Math.Floor(timeInMilliSeconds / 60) % 60;
        decimal seconds = Math.Floor(timeInMilliSeconds) % 60;

        //if (days != 0)
        //{
        //    returnString += days + " days ";
        //}
        if (hours != 0) {
            returnString = hours + " hrs ";
        }
        if (minutes != 0) {
            returnString += minutes + " mins ";
        }
        else if (seconds != 0) {
            returnString += seconds + " secs";
        }
        return returnString;
    }

    public static String GetFormattedDifference(TimeSpan time)
    {
        string returnString = "";
        //decimal days = Math.Floor(timeInMilliSeconds / 86400);
        //decimal hours = Math.Floor(timeInMilliSeconds / 3600) % 24;
        decimal hours = Math.Floor(Convert.ToDecimal(time.TotalSeconds) / 3600);
        decimal minutes = Math.Floor(Convert.ToDecimal(time.TotalSeconds) / 60) % 60;
        decimal seconds = Math.Floor(Convert.ToDecimal(time.TotalSeconds)) % 60;

        //if (days != 0)
        //{
        //    returnString += days + " days ";
        //}
        if (hours != 0) {
            returnString = hours + " hrs ";
        }
        if (minutes != 0) {
            returnString += minutes + " mins ";
        }
        else if (seconds != 0) {
            returnString += seconds + " secs";
        }
        return returnString;
    }

    public static String GetFormattedHours(decimal timeInMilliSeconds, int byHours)
    {
        string returnString = "";
        //decimal days = Math.Floor(timeInMilliSeconds / 86400);        
        decimal days = Math.Floor(timeInMilliSeconds / 28800);
        decimal hours = Math.Floor(timeInMilliSeconds / 3600) % 8;
        decimal minutes = Math.Floor(timeInMilliSeconds / 60) % 60;
        //decimal seconds = Math.Floor(timeInMilliSeconds) % 60;


        if (days != 0) {
            if (days > 0) {
                returnString = days + " days ";
            }
            else {
                returnString = days + " day ";
            }

        }
        if (hours != 0) {
            returnString += hours + " hours ";
        }
        if (minutes != 0) {
            returnString += minutes + " mins ";
        }
        //else if (seconds != 0)
        //{
        //    returnString += seconds + " secs";
        //}
        return returnString;
    }

    public static String GetFormattedHours(TimeSpan time, int byHours)
    {
        string returnString = "";
        //decimal days = Math.Floor(timeInMilliSeconds / 86400);        
        decimal days = Math.Floor(Convert.ToDecimal(time.TotalSeconds) / 28800);
        decimal hours = Math.Floor(Convert.ToDecimal(time.TotalSeconds) / 3600) % 8;
        decimal minutes = Math.Floor(Convert.ToDecimal(time.TotalSeconds) / 60) % 60;
        //decimal seconds = Math.Floor(timeInMilliSeconds) % 60;


        if (days != 0) {
            if (days > 0) {
                returnString = days + " days ";
            }
            else {
                returnString = days + " day ";
            }

        }
        if (hours != 0) {
            returnString += hours + " hours ";
        }
        if (minutes != 0) {
            returnString += minutes + " mins ";
        }
        //else if (seconds != 0)
        //{
        //    returnString += seconds + " secs";
        //}
        return returnString;
    }


    /// <summary>
    /// Checks if overtime overlaps with another overtime
    /// Checks if overtime is betweeen working hours
    /// Checks if there are breaks between overtime and break overtime according to breaks.
    /// </summary>
    /// <param name="FromTime"></param>
    /// <param name="ToTime"></param>
    /// <param name="Comment"></param>
    /// <param name="UserID"></param>
    /// <param name="InstanceID"></param>
    /// <returns></returns>
    public static void AddOvertime(OverTime overtime, String InstanceID)
    {
        avaimaTest0001DB db = new avaimaTest0001DB();
        //MyAttSys.WorkingHour workingHour = db.GetWorkingHourOfUser(overtime.UserID.ToInt32(), InstanceID, overtime.FromTime.Value.DayOfWeek.ToString()).ToWorkingHour();
        //if (workingHour != null)
        //{
        //    if ((bool)workingHour.Active)
        //    {
        //        DateTime? dtClockIn = workingHour.Clockin;
        //        DateTime? dtClockOut = workingHour.Clockout;
        //        // If undertime is between working hours
        //        if ((overtime.FromTime.Value.TimeOfDay >= dtClockIn.Value.TimeOfDay && overtime.FromTime.Value.TimeOfDay <= dtClockOut.Value.TimeOfDay) && (overtime.ToTime.Value.TimeOfDay >= dtClockIn.Value.TimeOfDay && overtime.ToTime.Value.TimeOfDay <= dtClockOut.Value.TimeOfDay))
        //        { }
        //        else
        //        { throw new Exception("Overtime duration is not between your Working hours. Please add undertime between: " + dtClockIn.Value.toClientTime().Value.ToShortTimeString() + " - " + dtClockOut.Value.toClientTime().Value.ToShortTimeString()); }
        //    }
        //    else
        //    { throw new Exception("Working hours are disabled on " + overtime.FromTime.Value.Date.ToString("MMMM, dd, MM yyyy")); }

        //}
        //else
        //{ throw new Exception("Working hours are disabled on " + overtime.FromTime.Value.Date.ToString("MMMM, dd, MM yyyy")); }
        //List<OverTime> overtimes = OverTime.Find(u => u.UserID == UserID).ToList().Where(u => u.FromTime.Value.Date == FromTime.Date).ToList();

        // Check for Overtime conflict
        List<OverTime> overtimes = OverTime.Find(u => u.UserID == overtime.UserID.ToInt32()).ToList().Where(u => u.FromTime.Value.Date == overtime.FromTime.Value.Date).ToList();
        //List<UnderTime> undertimes = UnderTime.Find(u => u.UserID == overtime.UserID.ToInt32()).ToList().Where(u => u.FromTime.Value.Date == overtime.FromTime.Value.Date).ToList();
        foreach (var item in overtimes) {
            if ((overtime.FromTime >= item.FromTime && overtime.FromTime <= item.ToTime) || (overtime.ToTime >= item.FromTime && overtime.ToTime <= item.ToTime)) {
                throw new Exception("The entered time range is in conflict with another Overtime entry for this date as follows:<br />" +
                    "Over-Time already entered: " + item.FromTime.Value.toClientTime().Value.ToShortTimeString() + " - " + item.ToTime.Value.toClientTime().Value.ToShortTimeString());
                return;
            }
        }


        db = new avaimaTest0001DB();
        DataTable dt = db.getTodaysBreak(overtime.UserID.ToInt32(), InstanceID, overtime.FromTime.Value.Date).ExecuteDataSet().Tables[0];
        DateTime? lasttime = overtime.FromTime;
        DateTime? lastout = overtime.FromTime;
        //Response.Write("rows: " + dt.Rows.Count);
        int undertimeBreak = 0;
        foreach (DataRow row in dt.Rows) {
            AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
            DateTime startTime = Convert.ToDateTime(atz.GetTimeReverse(row["Bfromtime"].ToString(), HttpContext.Current.User.Identity.Name));
            DateTime endTime = Convert.ToDateTime(atz.GetTimeReverse(row["Btotime"].ToString(), HttpContext.Current.User.Identity.Name));
            if (lasttime.Value.TimeOfDay < startTime.TimeOfDay && overtime.ToTime.Value.TimeOfDay > startTime.TimeOfDay) {
                undertimeBreak++;
                OverTime o1 = new OverTime() {
                    Active = true,
                    Comment = overtime.Comment,
                    FromTime = Convert.ToDateTime(overtime.FromTime.Value.ToShortDateString() + " " + lasttime.Value.ToShortTimeString()),
                    ToTime = Convert.ToDateTime(overtime.FromTime.Value.ToShortDateString() + " " + startTime.ToShortTimeString()),
                    InstanceID = InstanceID,
                    Type = overtime.Type,
                    UserID = overtime.UserID,
                };
                o1.Add();
                SendOvertimeEmail(overtime.UserID.ToInt32(), InstanceID, o1);
                lastout = endTime;
            }
            lasttime = endTime;
        }

        if (lastout.Value.TimeOfDay < overtime.ToTime.Value.TimeOfDay) {
            undertimeBreak++;
            //Response.Write(lastout.Value.ToString());
            OverTime o2 = new OverTime() {
                Active = true,
                Comment = overtime.Comment,
                FromTime = Convert.ToDateTime(overtime.FromTime.Value.ToShortDateString() + " " + lastout.Value.ToShortTimeString()),
                ToTime = overtime.ToTime,
                InstanceID = overtime.InstanceID,
                Type = overtime.Type,
                UserID = overtime.UserID,
            };
            o2.Add();
            SendOvertimeEmail(overtime.UserID.ToInt32(), InstanceID, o2);
        }
    }

    /// <summary>
    /// Checks if undertime overlaps with another undertime
    /// Checks if undertime is betweeen working hours
    /// Checks if there are breaks between undertime and break undertime according to breaks.
    /// </summary>
    /// <param name="FromTime"></param>
    /// <param name="ToTime"></param>
    /// <param name="Comment"></param>
    /// <param name="UserID"></param>
    /// <param name="InstanceID"></param>
    /// <returns></returns>
    public static void AddUndertime(DateTime FromTime, DateTime ToTime, String Type, String Comment, int UserID, String InstanceID)
    {
        avaimaTest0001DB db = new avaimaTest0001DB();
        MyAttSys.WorkingHour workingHour = db.GetWorkingHourOfUser(UserID, InstanceID, FromTime.DayOfWeek.ToString()).ToWorkingHour();
        if (workingHour != null) {
            if ((bool)workingHour.Active) {
                DateTime? dtClockIn = workingHour.Clockin;
                DateTime? dtClockOut = workingHour.Clockout;
                // If undertime is between working hours
                if ((FromTime.TimeOfDay >= dtClockIn.Value.TimeOfDay && FromTime.TimeOfDay <= dtClockOut.Value.TimeOfDay) && (ToTime.TimeOfDay >= dtClockIn.Value.TimeOfDay && ToTime.TimeOfDay <= dtClockOut.Value.TimeOfDay)) { }
                else {
                    throw new Exception("Undertime duration is not between your Working hours. Please add undertime between: " + dtClockIn.Value.toClientTime().Value.ToShortTimeString() + " - " + dtClockOut.Value.toClientTime().Value.ToShortTimeString());
                }
            }
            else { throw new Exception("Working hours are disabled on " + FromTime.Date.ToString("MMMM, dd, MM yyyy")); }

        }
        else { throw new Exception("Working hours are disabled on " + FromTime.Date.ToString("MMMM, dd, MM yyyy")); }
        //List<OverTime> overtimes = OverTime.Find(u => u.UserID == UserID).ToList().Where(u => u.FromTime.Value.Date == FromTime.Date).ToList();

        // Check for Undertime conflict
        List<UnderTime> undertimes = UnderTime.Find(u => u.UserID == UserID).ToList().Where(u => u.FromTime.Value.Date == FromTime.Date).ToList();
        foreach (var item in undertimes) {
            if ((FromTime >= item.FromTime && FromTime <= item.ToTime) || (ToTime >= item.FromTime && ToTime <= item.ToTime)) {
                throw new Exception("The entered time range is in conflict with another Undertime entry for this date as follows:<br />" +
                    "Under-Time already entered: " + item.FromTime.Value.toClientTime().Value.ToShortTimeString() + " - " + item.ToTime.Value.toClientTime().Value.ToShortTimeString());
            }
        }


        db = new avaimaTest0001DB();
        DataTable dt = db.getTodaysBreak(UserID, InstanceID, FromTime.Date).ExecuteDataSet().Tables[0];
        DateTime? lasttime = FromTime;
        DateTime? lastout = FromTime;
        //Response.Write("rows: " + dt.Rows.Count);
        int undertimeBreak = 0;
        foreach (DataRow row in dt.Rows) {
            AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
            DateTime startTime = Convert.ToDateTime(atz.GetTimeReverse(row["Bfromtime"].ToString(), HttpContext.Current.User.Identity.Name));
            DateTime endTime = Convert.ToDateTime(atz.GetTimeReverse(row["Btotime"].ToString(), HttpContext.Current.User.Identity.Name));
            if (lasttime.Value.TimeOfDay < startTime.TimeOfDay && ToTime.TimeOfDay > startTime.TimeOfDay) {
                undertimeBreak++;
                UnderTime undetime = new UnderTime() {
                    Active = true,
                    Comment = Comment,
                    FromTime = Convert.ToDateTime(lasttime.Value.ToShortTimeString()),
                    ToTime = Convert.ToDateTime(startTime.ToShortTimeString()),
                    InstanceID = InstanceID,
                    Type = Type,
                    UserID = UserID,
                };
                undetime.Add();
                string UTID1 = undetime.UTID.ToString();
                SendUndetimeEmail(UserID, InstanceID, undetime, UTID1);
                lastout = endTime;
            }
            lasttime = endTime;
        }

        if (lastout.Value.TimeOfDay < ToTime.TimeOfDay) {
            undertimeBreak++;
            //Response.Write(lastout.Value.ToString());
            UnderTime undetime = new UnderTime() {
                Active = true,
                Comment = Comment,
                FromTime = FromTime,
                ToTime = ToTime,
                InstanceID = InstanceID,
                Type = Type,
                UserID = UserID,
            };
            undetime.Add();
            string UTID = undetime.UTID.ToString();
            SendUndetimeEmail(UserID, InstanceID, undetime, UTID);
        }
    }

    public static void UpdateUndertime(UnderTime undertimetoupdate, String InstanceID)
    {
        avaimaTest0001DB db = new avaimaTest0001DB();
        MyAttSys.WorkingHour workingHour = db.GetWorkingHourOfUser(undertimetoupdate.UserID.ToInt32(), InstanceID, undertimetoupdate.FromTime.Value.DayOfWeek.ToString()).ToWorkingHour();
        if (workingHour != null) {
            if ((bool)workingHour.Active) {
                DateTime? dtClockIn = workingHour.Clockin;
                DateTime? dtClockOut = workingHour.Clockout;
                if (undertimetoupdate.ToTime == null) {
                    // If undertime is between working hours
                    if ((undertimetoupdate.FromTime.Value.TimeOfDay >= dtClockIn.Value.TimeOfDay && undertimetoupdate.FromTime.Value.TimeOfDay <= dtClockOut.Value.TimeOfDay)) { }
                    else {
                        throw new Exception("Undertime duration is not between your Working hours. Please add undertime between: " + dtClockIn.Value.toClientTime().Value.ToShortTimeString() + " - " + dtClockOut.Value.toClientTime().Value.ToShortTimeString());
                    }
                }
                else {
                    // If undertime is between working hours
                    if ((undertimetoupdate.FromTime.Value.TimeOfDay >= dtClockIn.Value.TimeOfDay && undertimetoupdate.FromTime.Value.TimeOfDay <= dtClockOut.Value.TimeOfDay) && (undertimetoupdate.ToTime.Value.TimeOfDay >= dtClockIn.Value.TimeOfDay && undertimetoupdate.ToTime.Value.TimeOfDay <= dtClockOut.Value.TimeOfDay)) { }
                    else {
                        throw new Exception("Undertime duration is not between your Working hours. Please add undertime between: " + dtClockIn.Value.toClientTime().Value.ToShortTimeString() + " - " + dtClockOut.Value.toClientTime().Value.ToShortTimeString());
                    }
                }


            }
            else { throw new Exception("Working hours are disabled on " + undertimetoupdate.FromTime.Value.Date.ToString("MMMM, dd, MM yyyy")); }

        }
        else { throw new Exception("Working hours are disabled on " + undertimetoupdate.FromTime.Value.Date.ToString("MMMM, dd, MM yyyy")); }
        //List<OverTime> overtimes = OverTime.Find(u => u.UserID == UserID).ToList().Where(u => u.FromTime.Value.Date == FromTime.Date).ToList();

        // Check for Undertime conflict
        List<UnderTime> undertimes = UnderTime.Find(u => u.UserID == undertimetoupdate.UserID).ToList().Where(u => u.FromTime.Value.Date == undertimetoupdate.FromTime.Value.Date).ToList();
        foreach (var item in undertimes) {
            if (item.UTID == undertimetoupdate.UTID) { continue; }

            if ((undertimetoupdate.FromTime >= item.FromTime && undertimetoupdate.FromTime <= item.ToTime) || (undertimetoupdate.ToTime >= item.FromTime && undertimetoupdate.ToTime <= item.ToTime)) {
                throw new Exception("The entered time range is in conflict with another Undertime entry for this date as follows:<br />" +
                    "Under-Time already entered: " + item.FromTime.Value.toClientTime().Value.ToShortTimeString() + " - " + item.ToTime.Value.toClientTime().Value.ToShortTimeString());
            }
        }


        db = new avaimaTest0001DB();
        DataTable dt = db.getTodaysBreak(undertimetoupdate.UserID.ToInt32(), InstanceID, undertimetoupdate.FromTime.Value.Date).ExecuteDataSet().Tables[0];
        DateTime? lasttime = undertimetoupdate.FromTime;
        DateTime? lastout = undertimetoupdate.FromTime;
        int undertimeBreak = 0;
        //Response.Write("rows: " + dt.Rows.Count);
        foreach (DataRow row in dt.Rows) {
            AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
            DateTime startTime = Convert.ToDateTime(atz.GetTimeReverse(row["Bfromtime"].ToString(), HttpContext.Current.User.Identity.Name));
            DateTime endTime = Convert.ToDateTime(atz.GetTimeReverse(row["Btotime"].ToString(), HttpContext.Current.User.Identity.Name));
            if (lasttime.Value.TimeOfDay < startTime.TimeOfDay && undertimetoupdate.ToTime.Value.TimeOfDay > startTime.TimeOfDay) {
                undertimeBreak++;
                UnderTime undetime = new UnderTime() {
                    Active = true,
                    Comment = undertimetoupdate.Comment,
                    FromTime = Convert.ToDateTime(lasttime.Value.ToShortTimeString()),
                    ToTime = Convert.ToDateTime(startTime.ToShortTimeString()),
                    InstanceID = InstanceID,
                    Type = undertimetoupdate.Type,
                    UserID = undertimetoupdate.UserID,
                };
                undetime.Add();
                string UTID = undetime.UTID.ToString();
                SendUndetimeEmail(undertimetoupdate.UserID.ToInt32(), InstanceID, undetime, UTID);
                lastout = endTime;
            }
            lasttime = endTime;
        }

        if (lastout.Value.TimeOfDay < undertimetoupdate.ToTime.Value.TimeOfDay) {
            undertimeBreak++;
            //Response.Write(lastout.Value.ToString());
            UnderTime undetime = new UnderTime() {
                Active = true,
                Comment = undertimetoupdate.Comment,
                FromTime = Convert.ToDateTime(lastout.Value.ToShortTimeString()),
                ToTime = undertimetoupdate.ToTime,
                InstanceID = InstanceID,
                Type = undertimetoupdate.Type,
                UserID = undertimetoupdate.UserID,
            };

            undetime.Add();
            string UTID = undetime.UTID.ToString();
            SendUndetimeEmail(undertimetoupdate.UserID.ToInt32(), InstanceID, undetime, UTID);
        }
    }

    private static void SendUndetimeEmail(int userID, string instanceID, UnderTime underTime, string UTID)
    {
        string type = "2";
        List<int> adminIDs = SP.GetAdminIDByUserID(userID, instanceID);
        AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
        foreach (int adminID in adminIDs) {
            attendence_management adminUser = attendence_management.SingleOrDefault(u => u.ID == adminID);
            attendence_management user = attendence_management.SingleOrDefault(u => u.ID == userID);
            if (adminUser != null && user != null) {
                string messagebody = user.Title + " took undertime from " + App.GetTime(underTime.FromTime.Value.ToString(), HttpContext.Current.User.Identity.Name) + " to " + App.GetTime(underTime.ToTime.Value.ToString(), HttpContext.Current.User.Identity.Name);
                messagebody += "<br />";
                messagebody += ((underTime.Comment != null) ? "<br /> <b>Reason:</b> " + underTime.Comment : "");
                messagebody += "<br /> <b>From:</b> " + underTime.FromTime.toClientTime().Value.ToShortTimeString() + " to " + underTime.ToTime.toClientTime().Value.ToShortTimeString() + " - " + UtilityMethods.getFormatedTimeByMinutes(((underTime.ToTime - underTime.FromTime).ToTimeSpan().TotalMinutes.ToInt32())); ;
                messagebody += "<br /> <b>Day:</b> " + underTime.FromTime.Value.DayOfWeek.ToString();
                messagebody += "<br /> <b>Date:</b> " + underTime.FromTime.Value.ToString("MMMM dd, yyyy");

                //messagebody += "<br /><br /><a href=\"http://www.avaima.com/714fcd17-288b-4725-a0c2-7641c997eda4/DeleteOUTime.aspx?UOID=" + UTID.Encrypt() + "&userid=" + userID.ToString().Encrypt() + "&type=" + type.Encrypt() + "  \"> Click here </a> to DELETE this entry (Under Time)";
                
                messagebody += Helper.AvaimaEmailSignature;
                emailAPI.send_email(adminUser.email, "Time & Attendance", "", messagebody, "UNDER-Time by " + user.Title);
                emailAPI.send_email(user.email, "Time & Attendance", "", messagebody, "UNDER-Time by " + user.Title);
                emailAPI.send_email(WebConfigurationManager.AppSettings["DevEamil"], "Time & Attendance", "", messagebody, "UNDER-Time by " + user.Title);
            }
        }
    }
    private static void SendOvertimeEmail(int userID, string instanceID, OverTime ot)
    {
        List<int> adminIDs = SP.GetAdminIDByUserID(userID, instanceID);
        AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
        foreach (int adminID in adminIDs) {
            attendence_management adminUser = attendence_management.SingleOrDefault(u => u.ID == adminID);
            attendence_management user = attendence_management.SingleOrDefault(u => u.ID == userID);
            if (adminUser != null && user != null) {
                string messagebody = user.Title + " took overtime from " + App.GetTime(ot.FromTime.Value.ToString(), HttpContext.Current.User.Identity.Name) + " to " + App.GetTime(ot.ToTime.Value.ToString(), HttpContext.Current.User.Identity.Name);
                messagebody += "<br />";
                messagebody += ((ot.Comment != null) ? "<br /> <b>Reason:</b> " + ot.Comment : "");
                messagebody += "<br /> <b>From:</b> " + ot.FromTime.toClientTime().Value.ToShortTimeString() + " to " + ot.ToTime.toClientTime().Value.ToShortTimeString() + " - " + UtilityMethods.getFormatedTimeByMinutes(((ot.ToTime - ot.FromTime).ToTimeSpan().TotalMinutes.ToInt32())); ;
                messagebody += "<br /> <b>Day:</b> " + ot.FromTime.Value.DayOfWeek.ToString();
                messagebody += "<br /> <b>Date:</b> " + ot.FromTime.Value.ToString("MMMM, dd, yyyy");
                messagebody += Helper.AvaimaEmailSignature;
                emailAPI.send_email(adminUser.email, "Time & Attendance", "", messagebody, "UNDER-Time by " + user.Title);
                emailAPI.send_email(user.email, "Time & Attendance", "", messagebody, "OVER-Time by " + user.Title);
                emailAPI.send_email(WebConfigurationManager.AppSettings["DevEamil"], "Time & Attendance", "", messagebody, "OVER-Time by " + user.Title);
            }
        }
    }

    public static String Encrypt(this String str)
    {
        if (!string.IsNullOrEmpty(str))
        {
            byte[] strBytes = System.Text.Encoding.Unicode.GetBytes(str);
            string encryptString = Convert.ToBase64String(strBytes);
            return encryptString;
        }
        else
        {
            return str;
        }
    }

    public static String Decrypt(this String str)
    {
        if (!string.IsNullOrEmpty(str))
        {
            byte[] strBytes = Convert.FromBase64String(str);
            string originalString = System.Text.Encoding.Unicode.GetString(strBytes);
            return originalString;
        }
        else
        {
            return str;
        }
    }
}