﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Services;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService
{
    public WebService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public void SignIn(string userid, string inaddress)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("insertuserintime", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ppid", userid);
                    cmd.Parameters.AddWithValue("@userid", "uid");
                    cmd.Parameters.AddWithValue("@lastin", Convert.ToDateTime(DateTime.Now));
                    cmd.Parameters.AddWithValue("@inaddress", inaddress);
                    cmd.Parameters.AddWithValue("@comment", "Clock-In by admin");
                    cmd.Parameters.AddWithValue("@location", "2");
                    int rowid = Convert.ToInt32(cmd.ExecuteScalar());

                    using (SqlCommand cmd1 = new SqlCommand("UPDATE attendence_management SET datefield=@date WHERE ID=@id", con))
                    {
                        cmd1.Parameters.AddWithValue("@date", Convert.ToDateTime(DateTime.Now));
                        cmd1.Parameters.AddWithValue("@id", userid);
                        cmd1.ExecuteNonQuery();
                    }
                }
            }

        }
        catch (Exception ex)
        {
            //this.Redirect("Add_worker.aspx?id=" + UserId);
        }
    }

    [WebMethod]
    public void SignOut(string userid, string inaddress)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("insertuserouttime", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ppid", userid);
                    cmd.Parameters.AddWithValue("@rowid", "0");
                    cmd.Parameters.AddWithValue("@lastout", Convert.ToDateTime(DateTime.Now));
                    cmd.Parameters.AddWithValue("@outaddress", inaddress);
                    cmd.Parameters.AddWithValue("@comment", "Clock-Out by admin");
                    cmd.Parameters.AddWithValue("@location", "2");
                    int rowid = Convert.ToInt32(cmd.ExecuteScalar());
                    using (SqlCommand cmd1 = new SqlCommand("UPDATE attendence_management SET datefield=@date WHERE ID=@id", con))
                    {
                        cmd1.Parameters.AddWithValue("@date", Convert.ToDateTime(DateTime.Now));
                        cmd1.Parameters.AddWithValue("@id", userid);
                        cmd1.ExecuteNonQuery();
                    }
                    //FillAttendance();
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    [WebMethod]
    public Int32 AddAbsence(string AbsLogID, string UserID, string Date, string Active, string Comment, bool isAdjustment)
    {
        try
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
            {
                if (Convert.ToInt32(AbsLogID) > 0)
                {
                    // Update
                    using (SqlCommand command = new SqlCommand("UPDATE [Absences] SET" +
                   "[UserID] = @UserID,[CrtDate] = @CrtDate,[ModDate] = @ModDate,[Active] = @Active,[Comment] = @Comment WHERE AbsLogID = @AbsLogID", connection))
                    {
                        command.Parameters.AddWithValue("@AbsLogID", AbsLogID);
                        command.Parameters.AddWithValue("@UserID", UserID);
                        command.Parameters.AddWithValue("@CrtDate", Convert.ToDateTime(Date));
                        command.Parameters.AddWithValue("@ModDate", Convert.ToDateTime(Date));
                        command.Parameters.AddWithValue("@Active", Active);
                        command.Parameters.AddWithValue("@Comment", Comment);
                        connection.Open();

                        if (Active == "0" || Active == "False")
                            SendAbsentNotification(UserID, Convert.ToDateTime(Date), Comment, "delete");
                        else
                            SendAbsentNotification(UserID, Convert.ToDateTime(Date), Comment, "update");

                        return command.ExecuteNonQuery();
                    }
                }
                else
                {
                    // Save
                    using (SqlCommand command = new SqlCommand("INSERT INTO [Absences] " +
                   "([UserID],[CrtDate],[ModDate],[Active],[Comment],[Adjustment]) VALUES(@UserID,@CrtDate,@ModDate,@Active,@Comment, @isAdjustment)", connection))
                    {
                        //string date = Convert.ToDateTime(Date).ToString("dd/MM/yyyy");
                        command.Parameters.AddWithValue("@UserID", UserID);
                        command.Parameters.AddWithValue("@CrtDate", Date);
                        command.Parameters.AddWithValue("@ModDate", Date);
                        command.Parameters.AddWithValue("@Active", Active);
                        command.Parameters.AddWithValue("@Comment", Comment);
                        command.Parameters.AddWithValue("@isAdjustment", isAdjustment);
                        connection.Open();

                        SendAbsentNotification(UserID, Convert.ToDateTime(Date), Comment, "insert");

                        return command.ExecuteNonQuery();
                    }
                }

            }
        }
        catch (Exception)
        {
            return 15;
        }

    }

    [WebMethod]

    public bool SendAbsentNotification(string userid, DateTime CrtDate, string Comment, string type)
    {
        AvaimaEmailAPI email = new AvaimaEmailAPI();
        StringBuilder subject = new StringBuilder();
        StringBuilder body = new StringBuilder();
        string subjectTxt = "";
        string bodyTxt = "";
        DataTable dt = new DataTable();

        if (type == "insert")
            subjectTxt = "Absence Notification by ";
        else if (type == "update")
            subjectTxt = "Absence Updated by ";
        else if (type == "delete")
            subjectTxt = "Absence Deleted by ";

        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            using (SqlCommand cmd = new SqlCommand("sp_GetUserAccess", connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@userid", userid);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);

                DataRow[] sender = dt.Select("ID=" + userid);

                subjectTxt += sender[0]["Title"].ToString();
                bodyTxt = subjectTxt + ". <br/>";
                bodyTxt += "<b>Day: </b>" + CrtDate + "<br/>";
                bodyTxt += "<b>Reason: </b>" + Comment;

                body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                subject.Append(subjectTxt);
                body.Append(bodyTxt);
                body.Append("<br><ul>");
                body.Append("<li>Go to <a href='http://www.avaima.com/signin'>www.avaima.com/signin</a> and login to your account</li>");
                body.Append("<li>Find and click \"Time & Attendance\" in the left menu</li>");
                body.Append("<li>\"Clock-In\" when you start your day at work and \"Clock-Out\" when you are done</li>");
                body.Append("</ul>");
                body.Append("For further questions, comments or help contact support@avaima.com.");
                body.Append(Helper.AvaimaEmailSignature);
                body.Append("</div>");

                List<string> emails = new List<string>();

                //Filter users to check if Email Notifications are enabled
                Attendance atd = new Attendance();
                dt = atd.EmailNotifications_Enabled(dt, 5);     //5; refers to Absent notification ID in "EmailNotifications" table 

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataAccessLayer dal = new DataAccessLayer(false);
                    Hashtable ht = new Hashtable();
                    ht["@userid"] = dt.Rows[i]["ID"].ToString();
                    DataTable settings = dal.ExecuteSQLQueryData("Select * from ReportSettings where userid = @userid", ht);

                    if (dt.Rows[i]["ID"].ToString() != userid)
                    {
                        if (dt.Rows[i]["sendemail"].ToInt32()  != -1) //If user has disabled email notifications
                            emails.Add(dt.Rows[i]["email"].ToString());

                        if (settings.Rows.Count > 0)
                        {
                            string[] ed = atd.DataList(settings);
                            foreach (var val in ed)
                                emails.Add(val);
                        }
                    }
                }

                foreach (var emailaddress in emails)
                {
                    email.send_email(emailaddress, "Time & Attendance", "", body.ToString(), subject.ToString());
                }

                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    if (dt.Rows[i]["ID"].ToString() != userid)
                //        email.send_email(dt.Rows[i]["email"].ToString(), "Time & Attendance", "", body.ToString(), subject.ToString());
                //}
            }
        }

        return true;
    }


    [WebMethod]
    public Int32 AddAbsenceToday(string AbsLogID, string UserID, string Date, string Active, string Comment)
    {
        try
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
            {
                if (Convert.ToInt32(AbsLogID) > 0)
                {
                    // Update
                    using (SqlCommand command = new SqlCommand("UPDATE [Absences] SET" +
                   "[UserID] = @UserID,[CrtDate] = @CrtDate,[ModDate] = @ModDate,[Active] = @Active,[Comment] = @Comment WHERE AbsLogID = @AbsLogID", connection))
                    {
                        command.Parameters.AddWithValue("@AbsLogID", AbsLogID);
                        command.Parameters.AddWithValue("@UserID", UserID);
                        command.Parameters.AddWithValue("@CrtDate", DateTime.Now.Date);
                        command.Parameters.AddWithValue("@ModDate", DateTime.Now.Date);
                        command.Parameters.AddWithValue("@Active", Active);
                        command.Parameters.AddWithValue("@Comment", Comment);
                        connection.Open();
                        return command.ExecuteNonQuery();
                    }
                }
                else
                {
                    // Save
                    using (SqlCommand command = new SqlCommand("INSERT INTO [Absences] " +
                   "([UserID],[CrtDate],[ModDate],[Active],[Comment]) VALUES(@UserID,@CrtDate,@ModDate,@Active,@Comment)", connection))
                    {
                        //string date = Convert.ToDateTime(Date).ToString("dd/MM/yyyy");
                        command.Parameters.AddWithValue("@UserID", UserID);
                        command.Parameters.AddWithValue("@CrtDate", DateTime.Now.Date);
                        command.Parameters.AddWithValue("@ModDate", DateTime.Now.Date);
                        command.Parameters.AddWithValue("@Active", Active);
                        command.Parameters.AddWithValue("@Comment", Comment);
                        connection.Open();
                        return command.ExecuteNonQuery();
                    }
                }
            }
        }
        catch (Exception)
        {
            return 15;
        }

    }

    [WebMethod]
    public Int32 AddUpdateExDays(string ExDayID, string ExDayTypeID, string UserID, string Date, string Active, string Comment)
    {
        try
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
            {
                if (Convert.ToInt32(ExDayID) > 0)
                {
                    // Update
                    using (SqlCommand command = new SqlCommand("UPDATE [ExDays] SET" +
                   "[UserID] = @UserID,[CrtDate] = @CrtDate,[ModDate] = @ModDate,[Active] = @Active,[Comment] = @Comment WHERE ExDayID = @ExDayID", connection))
                    {
                        command.Parameters.AddWithValue("@ExDayID", ExDayID);
                        command.Parameters.AddWithValue("@UserID", UserID);
                        command.Parameters.AddWithValue("@CrtDate", Convert.ToDateTime(Date));
                        command.Parameters.AddWithValue("@ModDate", Convert.ToDateTime(Date));
                        command.Parameters.AddWithValue("@Active", Active);
                        command.Parameters.AddWithValue("@Comment", Comment);
                        connection.Open();
                        return command.ExecuteNonQuery();
                    }
                }
                else
                {
                    // Save
                    using (SqlCommand command = new SqlCommand("INSERT INTO [ExDays] " +
                   "([UserID],[ExDayTypeID],[CrtDate],[ModDate],[Active],[Comment]) VALUES(@UserID, @ExDayTypeID,@CrtDate,@ModDate,@Active,@Comment)", connection))
                    {
                        //string date = Convert.ToDateTime(Date).ToString("dd/MM/yyyy");
                        command.Parameters.AddWithValue("@UserID", UserID);
                        command.Parameters.AddWithValue("@ExDayTypeID", ExDayTypeID);
                        command.Parameters.AddWithValue("@CrtDate", Date.ToString());
                        command.Parameters.AddWithValue("@ModDate", Date.ToString());
                        command.Parameters.AddWithValue("@Active", Active);
                        command.Parameters.AddWithValue("@Comment", Comment);
                        connection.Open();
                        return command.ExecuteNonQuery();
                    }
                }
            }
        }
        catch (Exception)
        {
            return 15;
        }

    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [WebMethod]
    public string AddUpdateAutoPresent(string AutoPresentID, string UserID, string ClockInTime, string ClockOutTime, string Active)
    {
        AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ToString()))
        {
            if (Convert.ToInt32(AutoPresentID) > 0)
            {
                AutoPresent ap = new AutoPresent()
                {
                    AutoPresentID = Convert.ToInt32(AutoPresentID),
                    UserID = Convert.ToInt32(UserID),
                    ClockInTime = DateTime.Parse(DateTime.Now.ToShortDateString() + " " + atz.GetTimeReverse(ClockInTime, HttpContext.Current.User.Identity.Name)),
                    ClockOutTime = DateTime.Parse(DateTime.Now.ToShortDateString() + " " + atz.GetTimeReverse(ClockOutTime, HttpContext.Current.User.Identity.Name)),
                    Active = Convert.ToBoolean(Active)
                };
                ap.Update();
                return "Updated";
            }
            else
            {
                // Save
                AutoPresent ap = new AutoPresent()
                {
                    AutoPresentID = Convert.ToInt32(AutoPresentID),
                    UserID = Convert.ToInt32(UserID),
                    ClockInTime = DateTime.Parse(DateTime.Now.ToShortDateString() + " " + atz.GetTimeReverse(ClockInTime, HttpContext.Current.User.Identity.Name)),
                    ClockOutTime = DateTime.Parse(DateTime.Now.ToShortDateString() + " " + atz.GetTimeReverse(ClockOutTime, HttpContext.Current.User.Identity.Name)),
                    Active = Convert.ToBoolean(Active)
                };
                ap.Add();
                return "Added";
            }
        }
    }

    [WebMethod]
    public void SetUserStatus(string userid, string active)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE attendence_management SET active = @active, datefield = @datetime WHERE id = @userid", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@userid", userid);
                    cmd.Parameters.AddWithValue("@active", !Convert.ToBoolean(active));
                    cmd.Parameters.AddWithValue("@datetime", DateTime.Now);
                    cmd.ExecuteNonQuery();
                }
            }

        }
        catch (Exception ex)
        {
            //this.Redirect("Add_worker.aspx?id=" + UserId);
        }
    }

    [WebMethod]
    public void PerformAutoPresent()
    {
        List<AutoPresent> autoPresents = AutoPresent.GetAll().Where(u => u.Active == true).ToList();
        foreach (AutoPresent obj in autoPresents)
        {
            DateTime lastSignIn = SP.GetWorkerLastSignIn(obj.UserID.ToString());
            DateTime lastSignOut = SP.GetWorkerLastSignOut(obj.UserID.ToString());


            if (DateTime.Now.Date > lastSignIn.Date)
            {
                if (DateTime.Now.TimeOfDay >= obj.ClockInTime.TimeOfDay)
                {
                    SP.ClockIn(obj, ":1");
                }
            }
            else if (lastSignOut.Date == lastSignIn.Date)
            {
                if (lastSignOut != null)
                {
                    if (DateTime.Now.TimeOfDay >= obj.ClockOutTime.TimeOfDay)
                    {
                        SP.ClockOut(obj, ":1");
                    }
                }
            }
        }
    }

    [WebMethod]
    public void PerformAutoPresentByWorkingHours()
    {
        AvaimaTimeZoneAPI atzAPI = new AvaimaTimeZoneAPI();
        AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
        List<WorkingHour> _WorkingHour = WorkingHour.GetAll().Where(u => u.Active == true && u.AutoPresent == true && u.DayTitle == DateTime.Now.DayOfWeek.ToString()).ToList();

        foreach (WorkingHour obj in _WorkingHour)
        {
            int iworkerTodaysWorkHours = SP.GetTodayUserWorkHours(obj.InstanceID, obj.UserID, DateTime.Now.Date);
            if (iworkerTodaysWorkHours == 0) { continue; }
            List<Absence> abs = Absence.GetAbsences(obj.UserID);
            if (abs.Where(u => u.CrtDate.Date == DateTime.Now.Date && u.Active == true).ToList().Count > 0) { continue; }
            DateTime lastSignIn = SP.GetWorkerLastSignIn(obj.UserID.ToString());
            DateTime lastSignOut = SP.GetWorkerLastSignOut(obj.UserID.ToString());
            DateTime actualClockIn = obj._userClockin;
            DateTime actualClockOut = obj._userClockout;
            if (DateTime.Now.Date > lastSignIn.Date)
            {
                if (DateTime.Now.TimeOfDay >= obj.Clockin.TimeOfDay)
                {
                    SP.ClockInVerified(obj, "::1");
                    String subject = "Auto Clock-in notification";
                    StringBuilder body = new StringBuilder();
                    body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                    body.Append("This email is to inform you that you are automatically clocked-in at " + actualClockIn.ToShortTimeString() + " on " + actualClockIn.ToString("ddd, MMM dd yyyy") + ". ");
                    body.Append("You can check your statistics using Time & Attendance on <a href=\"www.avaima.com\">Avaima.com</a>.");
                    body.Append(Helper.AvaimaEmailSignature);
                    body.Append("</div>");
                    emailAPI.send_email(SP.GetEmailByUserID(obj.UserID), "Attendance Management", "", body.ToString(), subject);
                }
            }
            else if (lastSignOut.Date != lastSignIn.Date)
            {
                if (lastSignOut == new DateTime(1900, 1, 1))
                {
                    if (DateTime.Now.TimeOfDay >= obj.Clockout.TimeOfDay)
                    {
                        SP.ClockOutVerified(obj, "::1");
                        String subject = "Auto Clock-out notification";
                        StringBuilder body = new StringBuilder();
                        body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                        body.Append("This email is to inform you that you are automatically clocked-out at " + actualClockOut.ToShortTimeString() + " on " + actualClockOut.ToString("ddd, MMM dd yyyy") + ". ");
                        body.Append("You can check your statistics using Time & Attendance on <a href=\"www.avaima.com\">Avaima.com</a>.");
                        body.Append(Helper.AvaimaEmailSignature);
                        body.Append("</div>");
                        emailAPI.send_email(SP.GetEmailByUserID(obj.UserID), "Attendance Management", "", body.ToString(), subject);
                    }
                }
            }
        }
    }

    [WebMethod]
    public void PerformClockNotifications()
    {
        AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
        try
        {
            AvaimaTimeZoneAPI atzAPI = new AvaimaTimeZoneAPI();
            List<WorkingHour> _standarWorkingHours = WorkingHour.GetAll().Where(u => u.Active == true && u.UserID == 0 && u.DayTitle == DateTime.Now.DayOfWeek.ToString()).ToList();
            List<WorkingHour> _usersWorkingHours = WorkingHour.GetAll().Where(u => u.Active == true && u.DayTitle == DateTime.Now.DayOfWeek.ToString()).ToList();
            List<AttUser> AttUsers = AttUser.GetAll().Where(u => u.Active == true && u.Category == false && !String.IsNullOrEmpty(u.Email)).ToList();
            List<String> instanceIDs = AttUsers.Select(u => u.InstanceID).ToList().Distinct().ToList();

            foreach (AttUser user in AttUsers)
            {
                if (Holiday.IsTodayHoliday(DateTime.Now.Date, user.InstanceID))
                {
                    continue;
                }
                DateTime lastSignIn = SP.GetWorkerLastSignIn(user.UserID.ToString());
                DateTime lastSignOut = SP.GetWorkerLastSignOut(user.UserID.ToString());

                WorkingHour notifWH = _usersWorkingHours.SingleOrDefault(u => u.UserID == user.UserID);
                if (notifWH == null)
                {
                    notifWH = _standarWorkingHours.SingleOrDefault(u => u.InstanceID == user.InstanceID);
                }

                if (notifWH != null && !notifWH.AutoPresent)
                {
                    DateTime sclockin = notifWH._userClockin;
                    DateTime sclockout = notifWH._userClockout;

                    if (DateTime.Now.Date > lastSignIn.Date)
                    {
                        if (DateTime.Now.TimeOfDay >= notifWH.Clockin.AddHours(-1).TimeOfDay && DateTime.Now.TimeOfDay < notifWH.Clockin.TimeOfDay)
                        {
                            String subject = "Remember to Clock-In using Avaima.com!";
                            StringBuilder body = new StringBuilder();
                            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                            body.Append("Remember to clock-in using Time & Attendance on <a href=\"www.avaima.com\">Avaima.com</a>.");
                            body.Append(Helper.AvaimaEmailSignature);
                            body.Append("</div>");
                            emailAPI.send_email(user.Email, "Attendance Management", "", body.ToString(), subject);
                        }
                        else if (DateTime.Now.TimeOfDay >= notifWH.Clockin.TimeOfDay && DateTime.Now.TimeOfDay < notifWH.Clockin.AddHours(1).TimeOfDay)
                        {
                            String subject = "You haven't clocked-in yet!";
                            StringBuilder body = new StringBuilder();
                            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                            body.Append("Your working hours are: " + sclockin.ToShortTimeString() + " - " + sclockout.ToShortTimeString() + ". It is past " + sclockin.ToShortTimeString() + " and you haven't clocked-in yet.");
                            body.Append("<br>In case you are absent use the link below to ");
                            body.Append("<a href='http://www.avaima.com/714fcd17-288b-4725-a0c2-7641c997eda4/workerverification.aspx?action=absent&uid=" + user.UserID + "&iid=" + user.InstanceID + "'>mark yourself as absent</a>");
                            body.Append(Helper.AvaimaEmailSignature);
                            body.Append("</div>");
                            emailAPI.send_email(user.Email, "Attendance Management", "", body.ToString(), subject);
                        }
                    }
                    else if (lastSignOut.Date != lastSignIn.Date)
                    {
                        if (lastSignOut == new DateTime(1900, 1, 1))
                        {
                            if (DateTime.Now.TimeOfDay >= notifWH.Clockout.AddHours(-1).TimeOfDay && DateTime.Now.TimeOfDay < notifWH.Clockout.TimeOfDay)
                            {
                                String subject = "Remember to Clock-out using Avaima.com!";
                                StringBuilder body = new StringBuilder();
                                body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                                body.Append("Remember to clock-out using Time & Attendance on <a href=\"www.avaima.com\">Avaima.com</a>!");
                                body.Append(Helper.AvaimaEmailSignature);
                                body.Append("</div>");
                                emailAPI.send_email(user.Email, "Attendance Management", "", body.ToString(), subject);
                            }
                            else if (DateTime.Now.TimeOfDay >= notifWH.Clockout.TimeOfDay && DateTime.Now.TimeOfDay < notifWH.Clockout.AddHours(1).TimeOfDay)
                            {
                                String subject = "You haven't clocked-out yet!";
                                StringBuilder body = new StringBuilder();
                                body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                                body.Append("Your working hours are: " + sclockin.ToShortTimeString() + " - " + sclockout.ToShortTimeString() + ". It is past " + sclockout.ToShortTimeString() + " and you haven't clocked-out yet.");
                                body.Append("<br>This email serves as a reminder in case you have forgotten.");
                                body.Append(Helper.AvaimaEmailSignature);
                                body.Append("</div>");
                                emailAPI.send_email(user.Email, "Attendance Management", "", body.ToString(), subject);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            emailAPI.send_email(WebConfigurationManager.AppSettings["DevEamil"], "Attendance Management", "", "Stack Trace: " + ex.StackTrace + "<br /><br />Inner Exception" + ex.InnerException, "Web Service Error PerformClockNotifications: " + ex.Message);
        }
    }

    [WebMethod]
    public void PerformAbscenceNotification()
    {
        AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();

        try
        {
            AvaimaTimeZoneAPI atzAPI = new AvaimaTimeZoneAPI();
            List<AttUser> AttUsers = AttUser.GetAll().Where(u => u.Active == true && u.Category == false && !String.IsNullOrEmpty(u.Email)).ToList();
            List<Absence> Absences = Absence.GetAll().Where(u => u.Active == true && u.CrtDate.Date >= DateTime.Now.Date).ToList();
            List<String> groupedAbs = Absences.Select(u => u.groupID).ToList().Distinct().ToList();
            //if (DateTime.Now.TimeOfDay > atzAPI.GetTime(DateTime.Parse("1/1/1990 07:00 am"),).TimeOfDay && DateTime.Now.TimeOfDay < DateTime.Parse("1/1/1990 08:00 am").TimeOfDay)
            //{
            foreach (Absence abs in Absences)
            {
                string instanceID = "";
                List<AttUser> users = AttUsers.Where(u => u.UserID == abs.UserID).ToList();
                if (users.Count > 0)
                {
                    instanceID = users[0].InstanceID;
                }
                // EACH DAY
                if (DateTime.Now.TimeOfDay > Convert.ToDateTime(atzAPI.GetTime("1/1/1990 07:00 am", instanceID)).TimeOfDay && DateTime.Now.TimeOfDay < Convert.ToDateTime(atzAPI.GetTime("1/1/1990 08:00 am", instanceID)).TimeOfDay)
                {
                    if (abs.CrtDate.Date == DateTime.Now.Date)
                    {
                        StringBuilder subject = new StringBuilder();
                        StringBuilder body = new StringBuilder();
                        string workerName = SP.GetWorkerName(abs.UserID.ToString());
                        subject.Append(workerName + " requested a leave for " + abs.CrtDate.ToString("ddd, MMM dd yyyy") + ".");
                        body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                        body.Append("<b>" + workerName + "</b> requested a leave on " + DateTime.Now.ToString("dddd MMMM dd yyyy") + ".");
                        body.Append(" Below is the description of his/her leave");
                        body.Append("<ul style='list-style-type:circle'>");
                        body.Append("<li>Leave Day: " + abs.CrtDate.ToString("ddd, MMM dd yyyy") + "</li>");
                        body.Append("<li>Reason: " + abs.Comment + "</li>");
                        body.Append("</ul>");
                        body.Append("Your approval is required.");
                        body.Append(Helper.AvaimaEmailSignature);
                        body.Append("</div>");
                        string strbody = body.ToString();
                        List<int> adminIDs = SP.GetAdminIDByUserID(abs.UserID, AttUsers.SingleOrDefault(u => u.UserID == abs.UserID).InstanceID);
                        foreach (int adminID in adminIDs)
                        {
                            emailAPI.send_email(SP.GetEmailByUserID(adminID), "Attendance Management", "", body.ToString(), subject.ToString());
                        }
                    }
                }
            }

            foreach (String abs in groupedAbs)
            {
                string instanceID = "";
                try
                {
                    instanceID = AttUsers.Where(u => u.UserID == Absences.Where(x => x.groupID == abs).ToList()[0].UserID).ToList()[0].InstanceID;
                }
                catch (Exception) { }
                // EACH DAY
                if (DateTime.Now.TimeOfDay > Convert.ToDateTime(atzAPI.GetTime("1/1/1990 07:00 am", instanceID)).TimeOfDay && DateTime.Now.TimeOfDay < Convert.ToDateTime(atzAPI.GetTime("1/1/1990 08:00 am", instanceID)).TimeOfDay)
                {
                    DateTime leaveStart = Absences.Where(u => u.groupID == abs).Min(u => u.CrtDate.Date);
                    DateTime leaveEnd = Absences.Where(u => u.groupID == abs).Max(u => u.CrtDate.Date);
                    Absence absent = Absences[0];
                    StringBuilder subject = new StringBuilder();
                    StringBuilder body = new StringBuilder();
                    string workerName = SP.GetWorkerName(absent.UserID.ToString());
                    if (leaveStart.Date == leaveEnd.Date)
                    {
                        subject.Append(workerName + " requested a leave for " + leaveStart.ToString("ddd, MMM dd yyyy") + ".");
                    }
                    else
                    {
                        subject.Append(workerName + " requested a leave from " + leaveStart.ToString("ddd, MMM dd yyyy") + " to " + leaveEnd.ToString("ddd, MMM dd yyyy") + ".");
                    }
                    body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                    body.Append("<b>" + workerName + "</b> requested a leave on " + DateTime.Now.ToString("dddd MMMM dd yyyy") + ".");
                    body.Append(" Below is the description of his/her leave");
                    body.Append("<ul style='list-style-type:circle'>");
                    if (leaveStart.Date == leaveEnd.Date)
                    {
                        body.Append("<li>Leave Day: " + leaveStart.ToString("ddd, MMM dd yyyy") + "</li>");
                    }
                    else
                    {
                        body.Append("<li>Leave Days: " + leaveStart.ToString("ddd, MMM dd yyyy") + " - " + leaveEnd.ToString("ddd, MMM dd yyyy") + "</li>");
                    }
                    body.Append("<li>Reason: " + absent.Comment + "</li>");
                    body.Append("</ul>");
                    body.Append("Your approval is required.");
                    body.Append(Helper.AvaimaEmailSignature);
                    body.Append("</div>");
                    string strbody = body.ToString();

                    if (leaveStart.Date == DateTime.Now.AddDays(7).Date)
                    {
                        List<int> adminIDs = SP.GetAdminIDByUserID(absent.UserID, AttUsers.SingleOrDefault(u => u.UserID == absent.UserID).InstanceID);
                        foreach (int adminID in adminIDs)
                        {
                            emailAPI.send_email(SP.GetEmailByUserID(adminID), "Attendance Management", "", body.ToString(), subject.ToString());
                        }
                    }
                    else if (leaveStart.Date == DateTime.Now.AddDays(3).Date)
                    {
                        List<int> adminIDs = SP.GetAdminIDByUserID(absent.UserID, AttUsers.SingleOrDefault(u => u.UserID == absent.UserID).InstanceID);
                        foreach (int adminID in adminIDs)
                        {
                            emailAPI.send_email(SP.GetEmailByUserID(adminID), "Attendance Management", "", body.ToString(), subject.ToString());
                        }
                    }
                    else if (leaveStart.Date == DateTime.Now.AddDays(1).Date)
                    {
                        List<int> adminIDs = SP.GetAdminIDByUserID(absent.UserID, AttUsers.SingleOrDefault(u => u.UserID == absent.UserID).InstanceID);
                        foreach (int adminID in adminIDs)
                        {
                            emailAPI.send_email(SP.GetEmailByUserID(adminID), "Attendance Management", "", body.ToString(), subject.ToString());
                        }
                    }
                }
                //}
            }
        }
        catch (Exception ex)
        {
            emailAPI.send_email(System.Configuration.ConfigurationManager.AppSettings["DevEmail"], "Attendance Management", "", "Stack Trace: " + ex.StackTrace + "<br /><br />Inner Exception" + ex.InnerException, "Web Service Error PerformAbscenceNotification: " + ex.Message);
        }

    }

    [WebMethod]
    public void PerformHolidayNotification123()
    {
        AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
        try
        {
            AvaimaTimeZoneAPI atzAPI = new AvaimaTimeZoneAPI();
            List<AttUser> AttUsers = AttUser.GetAll().Where(u => u.Active == true && u.Category == false && !String.IsNullOrEmpty(u.Email)).ToList();
            List<Holiday> holidays = Holiday.GetAll();
            foreach (AttUser user in AttUsers)
            {
                //if (DateTime.Now.TimeOfDay > Convert.ToDateTime(atzAPI.GetTime("1/1/1990 07:00 am", user.InstanceID)).TimeOfDay && DateTime.Now.TimeOfDay < Convert.ToDateTime(atzAPI.GetTime("1/1/1990 08:00 am", user.InstanceID)).TimeOfDay)
                //{
                // SEVEN DAY BEFORE
                if (Holiday.IsTodayHoliday(DateTime.Now.AddDays(7).Date, user.InstanceID))
                {
                    List<Holiday> todayHolidays = Holiday.GetTodaysHolidays(DateTime.Now.AddDays(7).Date, user.InstanceID);
                    foreach (Holiday holiday in todayHolidays)
                    {
                        StringBuilder subject = new StringBuilder();
                        StringBuilder body = new StringBuilder();
                        subject.Append("Holiday notification for " + holiday.title + " next week.");
                        body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                        body.Append("This is to notify that " + DateTime.Now.AddDays(7).Date.ToString("dddd, MMM dd, yyyy") + " is a holiday on account of \"" + holiday.title + "\"");
                        body.Append(Helper.AvaimaEmailSignature);
                        body.Append("</div>");
                        string strbody = body.ToString();
                        emailAPI.send_email(user.Email, "Attendance Management", "", body.ToString(), subject.ToString());
                    }
                }
                // THREE DAY BEFORE
                else if (Holiday.IsTodayHoliday(DateTime.Now.AddDays(3).Date, user.InstanceID))
                {
                    List<Holiday> todayHolidays = Holiday.GetTodaysHolidays(DateTime.Now.AddDays(3).Date, user.InstanceID);
                    foreach (Holiday holiday in todayHolidays)
                    {
                        StringBuilder subject = new StringBuilder();
                        StringBuilder body = new StringBuilder();
                        subject.Append("Holiday notification for " + holiday.title + " on " + DateTime.Now.AddDays(3).Date.ToString("ddd, MMM dd yyyy") + ".");
                        body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                        body.Append("This is to notify that " + DateTime.Now.AddDays(3).Date.ToString("dddd, MMM dd, yyyy") + " is a holiday on account of \"" + holiday.title + "\"");
                        body.Append(Helper.AvaimaEmailSignature);
                        body.Append("</div>");
                        string strbody = body.ToString();
                        emailAPI.send_email(user.Email, "Attendance Management", "", body.ToString(), subject.ToString());
                    }
                }
                // TOMORROW
                else if (Holiday.IsTodayHoliday(DateTime.Now.AddDays(1).Date, user.InstanceID))
                {
                    List<Holiday> todayHolidays = Holiday.GetTodaysHolidays(DateTime.Now.AddDays(1).Date, user.InstanceID);
                    foreach (Holiday holiday in todayHolidays)
                    {
                        StringBuilder subject = new StringBuilder();
                        StringBuilder body = new StringBuilder();
                        subject.Append("Holiday notification for " + holiday.title + " tomorrow.");
                        body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                        body.Append("This is to notify that tomorrow is a holiday on account of \"" + holiday.title + "\"");
                        body.Append(Helper.AvaimaEmailSignature);
                        body.Append("</div>");
                        string strbody = body.ToString();
                        emailAPI.send_email(user.Email, "Attendance Management", "", body.ToString(), subject.ToString());
                    }
                }

                //}

            }
        }
        catch (Exception ex)
        {
            emailAPI.send_email(System.Configuration.ConfigurationManager.AppSettings["DevEmail"], "Attendance Management", "", "Stack Trace: " + ex.StackTrace + "<br /><br />Inner Exception" + ex.InnerException, "Web Service Error PerformHolidayNotification: " + ex.Message);
        }
    }

    [WebMethod]
    public void PerformNoResponseNotification()
    {
        AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
        try
        {
            //DateTime.Now.Date.DayOfWeek == DayOfWeek.Monday &&
            if (DateTime.Now.TimeOfDay >= Convert.ToDateTime("1/1/1990 07:00 am").TimeOfDay && DateTime.Now.TimeOfDay < Convert.ToDateTime("1/1/1990 08:00 am").TimeOfDay)
            {
                //AvaimaUserProfile userProfile = new AvaimaUserProfile();
                //AvaimaTimeZoneAPI atzAPI = new AvaimaTimeZoneAPI();                
                List<String> instanceIDs = SP.GetAllInstanceIDs();
                foreach (String instanceID in instanceIDs)
                {
                    //if (!instanceID.Contains("de368dcf-3084-4159-8d5c-4637c1238902"))
                    //{
                    //    continue;
                    //}
                    List<AttUser> AttUsers = AttUser.GetAll().Where(u => u.Active == true && u.Category == false && !String.IsNullOrEmpty(u.Email) && u.InstanceID == instanceID).ToList();
                    List<AttUser> notUsed = new List<AttUser>();
                    List<AttUser> SuperAdmins = AttUsers.Where(u => u.Role == "superuser").ToList();
                    List<AttUser> admins = AttUsers.Where(u => u.Role == "admin").ToList();
                    foreach (AttUser superAdmin in SuperAdmins)
                    {
                        foreach (AttUser attUser in AttUsers)
                        {
                            //DateTime firstSignIn = SP.GetWorkerFirstSignInn(attUser.UserID.ToString());
                            if (!attUser.HasUserClockedIn())
                            {
                                notUsed.Add(attUser);
                            }
                        }
                        if (notUsed.Count > 0)
                        {
                            StringBuilder subject = new StringBuilder();
                            StringBuilder body = new StringBuilder();
                            subject.Append("Summary - Employees that are not using Time & Attendance.");
                            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                            body.Append("Dear " + superAdmin.UserName + ", ");
                            body.Append("<br><br>This is to inform you that following employees you entered in the system have either not signed up or are not using the software application to clock-in/out:");
                            body.Append("<ul>");
                            foreach (AttUser notUsedUser in notUsed)
                            {
                                body.Append("<li>" + notUsedUser.UserName + "</li>");
                            }
                            body.Append("</ul>");
                            body.Append(Helper.AvaimaEmailSignature);
                            body.Append("</div>");
                            string strbody = body.ToString();
                            emailAPI.send_email(superAdmin.Email, "Attendance Management", "", body.ToString(), subject.ToString());
                        }

                        //String ownerID = SP.GetOwnerID(user.UserID);
                        //if (String.IsNullOrEmpty(ownerID))
                        //{
                        //    String userInfo = userProfile.Getonlineuserinfo(ownerID);
                        //    String[] userName = userInfo.Split('#');
                        //    if (String.IsNullOrEmpty(userName[0]) && String.IsNullOrEmpty(userName[1]))
                        //    {
                        //        notRegisteredUsers.Add(user);
                        //    }
                        //}
                    }

                    foreach (AttUser admin in admins)
                    {
                        List<AttUser> underAdminUsers = AttUsers.Where(u => u.ParentID == admin.AssignedAdminID && u.ParentID != 0).ToList();
                        List<AttUser> notUsedUsers = new List<AttUser>();
                        foreach (AttUser underAdminUser in underAdminUsers)
                        {
                            //DateTime firstSignIn = SP.GetWorkerFirstSignInn(underAdminUser.UserID.ToString());
                            if (!underAdminUser.HasUserClockedIn())
                            {
                                notUsedUsers.Add(underAdminUser);
                            }
                        }
                        if (notUsedUsers.Count > 0)
                        {
                            StringBuilder subject = new StringBuilder();
                            StringBuilder body = new StringBuilder();
                            subject.Append("Summary - Employees that are not using Time & Attendance.");
                            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                            body.Append("Dear " + admin.UserName + ", ");
                            body.Append("<br><br>This is to inform you that following employees you entered in the system have either not signed up or are not using the software application to clock-in/out:");
                            body.Append("<ul>");
                            foreach (AttUser notUsedUser in notUsedUsers)
                            {
                                body.Append("<li>" + notUsedUser.UserName + "</li>");
                            }
                            body.Append("</ul>");
                            body.Append(Helper.AvaimaEmailSignature);
                            body.Append("</div>");
                            string strbody = body.ToString();
                            emailAPI.send_email(admin.Email, "Attendance Management", "", body.ToString(), subject.ToString());
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            emailAPI.send_email(System.Configuration.ConfigurationManager.AppSettings["DevEmail"], "Attendance Management", "", "Stack Trace: " + ex.StackTrace + "<br /><br />Inner Exception" + ex.InnerException, "Web Service Error PerformNoResponseNotification: " + ex.Message);
        }
    }

    [WebMethod]
    public void CheckGetEmployeeRecord()
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("getworkerrecordbydate", con))
            {
                List<RecordModel> records = new List<RecordModel>();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                List<string> dates;
                dates = UtilityMethods.getpastDates(0);
                DateTime dt1 = new DateTime(2015, 1, 16);
                DateTime dt2 = new DateTime(2015, 1, 16);
                int userID = 210;
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@sDate", dt1.ToString());
                cmd.Parameters.AddWithValue("@eDate", dt2.ToString());
                cmd.Parameters.AddWithValue("@Id", userID);
                cmd.Parameters.AddWithValue("@InstanceId", "de368dcf-3084-4159-8d5c-4637c1238902");
                adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                //rptHistory.DataSource = records;
                //rptHistory.DataBind();
                records = UtilityMethods.getRecords(ds);
            }
        }
    }

    [WebMethod]
    public void ConvertWeekendToOvertime(int userid, int year, string instanceID)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            List<RecordModel> records = new List<RecordModel>();
            DateTime startDate = SP.GetWorkerFirstSignInThisYear(userid.ToString(), year);
            DateTime endDate = SP.GetWorkerLastSignInThisYear(userid.ToString(), year);
            #region Table Headers weekends
            List<RecordModel> recordList = new List<RecordModel>();
            List<DateRange> DateRanges = new List<DateRange>();
            #endregion
            List<Absence> absences = Absence.GetAbsences(Convert.ToInt32(userid)).Where(u => u.Active == true).ToList();
            List<WorkingHour> userWorkingHours = WorkingHour.GetWorkingHours(userid);
            if (userWorkingHours.Count <= 0)
            {
                userWorkingHours = WorkingHour.GetWorkingHours(0);
                userWorkingHours = userWorkingHours.Where(u => u.InstanceID == instanceID).ToList();
            }
            TimeSpan TotalOvertime = new TimeSpan(0);
            //HttpContext.Current.Session["UsersWorkingHours"] = null;
            if (startDate.Year <= year && endDate.Year >= year)
            {
                for (int i = startDate.Month; i <= endDate.Month; i++)
                {
                    DateTime qstartDate = new DateTime(year, i, 1);
                    DateTime qendDate = qstartDate.AddMonths(1).AddDays(-1);
                    using (SqlCommand cmd = new SqlCommand("getworkerrecordbydate", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@sDate", qstartDate.Date);
                        cmd.Parameters.AddWithValue("@eDate", qendDate.Date);
                        cmd.Parameters.AddWithValue("@Id", userid);
                        cmd.Parameters.AddWithValue("@InstanceId", instanceID);
                        DataSet ds = new DataSet();
                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        adp.Fill(ds);
                        records = UtilityMethods.getRecords(ds);
                        records = records.OrderBy(u => u.Date).ToList();
                        DateRanges.Add(new DateRange() { EndDate = qendDate.Date, StartDate = qstartDate.Date });

                        foreach (var record in records)
                        {
                            if (record.DayTitle != null && record.Records.Count > 0)
                            {
                                foreach (SubRecordModel subRecord in record.Records)
                                {
                                    MyAttSys.OverTime ot = new MyAttSys.OverTime()
                                    {
                                        Active = true,
                                        Comment = "WEEKEND WORK AUTO " + subRecord.LoginTimeDT.Value.DayOfWeek.ToString() + " " + subRecord.Id,
                                        FromTime = subRecord.LoginTimeDT,
                                        ToTime = subRecord.LogoutTimeDT,
                                        InstanceID = instanceID,
                                        Type = "Weekend_Work",
                                        UserID = userid
                                    };
                                    try { UtilityMethods.AddOvertime(ot, instanceID); }
                                    catch (Exception ex) { }
                                    MyAttSys.workertran wt = MyAttSys.workertran.SingleOrDefault(u => u.rowID == subRecord.Id);
                                    if (wt != null)
                                    {
                                        wt.Delete();
                                    }
                                }
                            }
                            else if (record.DayTitle == null && record.Records.Count > 0)
                            {
                                //MyAttSys.UnderTime ut = new MyAttSys.UnderTime()
                                //{
                                //    Active = true,
                                //    Comment = "NAMAZ UNDERTIME AUTO",
                                //    FromTime = (record.Date.ToShortDateString() + " " + "12:00").ToDateTime(),
                                //    ToTime = (record.Date.ToShortDateString() + " " + "12:10").ToDateTime(),
                                //    InstanceID = instanceID,
                                //    Type = "Undertime_Auto_Namaz",
                                //    UserID = userid
                                //};
                            }
                        }
                    }
                }
            }
        }
    }


    public void PerformHolidayNotification_old()
    {
        AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
        try
        {
            AvaimaTimeZoneAPI atzAPI = new AvaimaTimeZoneAPI();

            //List<AttUser> AttUsers = AttUser.GetAll().Where(u => u.Active == true && u.Category == false && !String.IsNullOrEmpty(u.Email)).ToList();
            List<Holiday> holidays = Holiday.GetAll();
            foreach (Holiday day in holidays)
            {
                if (DateTime.Now.TimeOfDay > Convert.ToDateTime(atzAPI.GetTime("1/1/1990 07:00 am", day.instanceId)).TimeOfDay && DateTime.Now.TimeOfDay < Convert.ToDateTime(atzAPI.GetTime("1/1/1990 08:00 am", day.instanceId)).TimeOfDay)
                {
                    if (day.date.Year == DateTime.Now.Year || day.Everyyear)
                    {
                        // SEVEN DAY BEFORE
                        if (Holiday.IsTodayHoliday(day.date.Date, day.instanceId))
                        {
                            //List<Holiday> todayHolidays = Holiday.GetTodaysHolidays(day.date.Date, day.instanceId);
                            //foreach (Holiday holiday in todayHolidays)
                            //{
                            bool chsend = false;
                            int addday = 0;
                            DateTime today = new DateTime(DateTime.Now.Year, day.date.Month, day.date.Day);
                            if ((today.Date - DateTime.Now.Date).TotalDays == 7)
                            {
                                chsend = true;
                                addday = 7;
                            }
                            else if ((today.Date - DateTime.Now.Date).TotalDays == 3)
                            {
                                chsend = true;
                                addday = 3;
                            }
                            else if ((today.Date - DateTime.Now.Date).TotalDays == 1)
                            {
                                chsend = true;
                                addday = 1;
                            }

                            if (chsend)
                            {
                                List<AttUser> AttUsers = AttUser.GetAll(day.instanceId).Where(u => u.Active == true && u.Category == false && !String.IsNullOrEmpty(u.Email) && u.InstanceID == day.instanceId).ToList();

                                foreach (AttUser appuser in AttUsers)
                                {

                                    StringBuilder subject = new StringBuilder();
                                    StringBuilder body = new StringBuilder();
                                    subject.Append("Holiday notification for " + day.title);
                                    body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                                    body.Append("This is to notify that " + DateTime.Now.AddDays(addday).Date.ToString("dddd, MMM dd, yyyy") + " is a holiday on account of \"" + day.title + "\"");
                                    body.Append(Helper.AvaimaEmailSignature);
                                    body.Append("</div>");
                                    string strbody = body.ToString();
                                    emailAPI.send_email(appuser.Email, "Time & Attendance", "", body.ToString(), subject.ToString());
                                }
                            }
                            //}
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            emailAPI.send_email(System.Configuration.ConfigurationManager.AppSettings["DevEmail"], "Time & Attendance", "", "Stack Trace: " + ex.StackTrace + "<br /><br />Inner Exception" + ex.InnerException, "Web Service Error PerformHolidayNotification: " + ex.Message);
        }
    }

    [WebMethod]
    public int PerformHolidayNotification()
    {
        int emails = 0;
        AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
        try
        {
            Attendance atd = new Attendance();
            DataAccessLayer dal = new DataAccessLayer(false);
            AvaimaTimeZoneAPI atzAPI = new AvaimaTimeZoneAPI();
            List<Holiday> holidays = Holiday.GetAll().ToList();

            foreach (Holiday day in holidays)
            {
                if (DateTime.Now.TimeOfDay > Convert.ToDateTime(atzAPI.GetTime("1/1/1990 07:00 am", day.userid.ToString())).TimeOfDay && DateTime.Now.TimeOfDay < Convert.ToDateTime(atzAPI.GetTime("1/1/1990 08:00 am", day.userid.ToString())).TimeOfDay)
                {
                    if (day.date.Year == DateTime.Now.Year || day.Everyyear)
                    {
                        if (Holiday.IsTodayHoliday(day.date.Date))
                        {
                            bool chsend = false;
                            int addday = 0;
                            DateTime today = new DateTime(DateTime.Now.Year, day.date.Month, day.date.Day);
                            if ((today.Date - DateTime.Now.Date).TotalDays == 7)
                            {
                                chsend = true;
                                addday = 7;
                            }
                            else if ((today.Date - DateTime.Now.Date).TotalDays == 3)
                            {
                                chsend = true;
                                addday = 3;
                            }
                            else if ((today.Date - DateTime.Now.Date).TotalDays == 1)
                            {
                                chsend = true;
                                addday = 1;
                            }

                            if (chsend)
                            {
                                if (day.userid != 0) //Safety check, bcx of migration 
                                {
                                    List<AttUser> AttUsers = AttUser.GetAll(day.userid).Where(u => u.Active == true && u.Category == false && !String.IsNullOrEmpty(u.Email)).ToList();

                                    Hashtable ht = new Hashtable();

                                    foreach (AttUser appuser in AttUsers)
                                    {
                                        //Filter users to check if Email Notifications are enabled
                                        ht["@UserID"] = appuser.UserID.ToInt32();
                                        ht["@EmailNotifications_ID"] = 6;    //6; refers to Holiday notification ID in "EmailNotifications" table 
                                        int i = dal.GetDataTable("sp_User_EmailNotifications_Enabled", ht).Rows[0]["response"].ToInt32();
                                        if (i != 0)
                                        {
                                            StringBuilder subject = new StringBuilder();
                                            StringBuilder body = new StringBuilder();
                                            subject.Append("Holiday notification for " + day.title);
                                            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                                            body.Append("This is to notify that " + DateTime.Now.AddDays(addday).Date.ToString("dddd, MMM dd, yyyy") + " is a holiday on account of \"" + day.title + "\"");
                                            body.Append(Helper.AvaimaEmailSignature);
                                            body.Append("</div>");
                                            string strbody = body.ToString();
                                            emailAPI.send_email(appuser.Email, "Time & Attendance", "", body.ToString(), subject.ToString());

                                            emails++;
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
            return emails;
        }
        catch (Exception ex)
        {
            emailAPI.send_email(System.Configuration.ConfigurationManager.AppSettings["DevEmail"], "Time & Attendance", "", "Stack Trace: " + ex.StackTrace + "<br /><br />Inner Exception" + ex.InnerException, "Web Service Error PerformHolidayNotification: " + ex.Message);
            return -1;
        }


    }

    class DateRange
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}