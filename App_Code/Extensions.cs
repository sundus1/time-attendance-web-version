﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for Extensions
/// </summary>
public static class Extensions
{
    //public Extensions()
    //{

    //}

    public static MyAttSys.WorkingHour ToWorkingHour(this DataTable dt)
    {
        return dt.Select().Select(u => new MyAttSys.WorkingHour()
        {
            UserID = u.Field<int>("UserID"),
            Active = u.Field<Boolean>("Active"),
            AutoPresent = u.Field<Boolean>("AutoPresent"),
            Clockin = u.Field<DateTime>("Clockin"),
            Clockout = u.Field<DateTime>("Clockout"),
            crtDate = u.Field<DateTime>("crtDate"),
            DayTitle = u.Field<String>("DayTitle"),
            InstanceID = u.Field<String>("InstanceID"),
            WorkHourID = u.Field<int>("WorkHourID")
        }).SingleOrDefault();
    }

    public static MyAttSys.WorkingHour ToWorkingHour(this SubSonic.Schema.StoredProcedure sp)
    {
        return sp.ExecuteDataSet().Tables[0].Select().Select(u => new MyAttSys.WorkingHour()
        {
            UserID = u.Field<int>("UserID"),
            Active = u.Field<Boolean>("Active"),
            AutoPresent = u.Field<Boolean>("AutoPresent"),
            Clockin = u.Field<DateTime>("Clockin"),
            Clockout = u.Field<DateTime>("Clockout"),
            crtDate = u.Field<DateTime>("crtDate"),
            DayTitle = u.Field<String>("DayTitle"),
            InstanceID = u.Field<String>("InstanceID"),
            WorkHourID = u.Field<int>("WorkHourID")
        }).SingleOrDefault();
    }

    public static List<MyAttSys.WorkingHour> ToWorkingHourList(this SubSonic.Schema.StoredProcedure sp)
    {
        return sp.ExecuteDataSet().Tables[0].Select().Select(u => new MyAttSys.WorkingHour()
        {
            UserID = u.Field<int>("UserID"),
            Active = u.Field<Boolean>("Active"),
            AutoPresent = u.Field<Boolean>("AutoPresent"),
            Clockin = u.Field<DateTime>("Clockin"),
            Clockout = u.Field<DateTime>("Clockout"),
            crtDate = u.Field<DateTime>("crtDate"),
            DayTitle = u.Field<String>("DayTitle"),
            InstanceID = u.Field<String>("InstanceID"),
            WorkHourID = u.Field<int>("WorkHourID")
        }).ToList();
    }

    public static TimeSpan ToTimeSpan(this TimeSpan? ts)
    {
        return ((TimeSpan)ts);
    }
    public static Int32 ToInt32(this Object str)
    {
        if (str == null)
        { return 0; }
        return Convert.ToInt32(str);
    }

    public static DateTime ToDateTime(this String DateTimeString)
    {
        if (String.IsNullOrEmpty(DateTimeString))
        { return DateTime.Now; }
        return Convert.ToDateTime(DateTimeString);
    }

    public static DateTime? toClientDate(this Object obj)
    {
        if (obj != null)
        {
            DateTime dt = Convert.ToDateTime(obj);
            AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
            if (atz.GetDate(dt.ToString(), HttpContext.Current.User.Identity.Name).Contains("Today"))
            { return DateTime.Now.Date; }
            else
            { return atz.GetDate(dt.ToString(), HttpContext.Current.User.Identity.Name).ToDateTime(); }
        }
        return null;
    }

    public static DateTime? toClientTime(this Object obj)
    {
        if (obj != null)
        {
            DateTime dt = Convert.ToDateTime(obj);
            AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
            return atz.GetTime(dt.ToString(), HttpContext.Current.User.Identity.Name).ToDateTime();
        }
        return null;
    }

    public static TimeSpan toClientTimeTs(this Object obj)
    {
        if (obj != null)
        {
            DateTime dt = Convert.ToDateTime(obj.ToString());
            AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
            return atz.GetTime(dt.ToString(), HttpContext.Current.User.Identity.Name).ToDateTime().TimeOfDay;
        }
        else
        { return new TimeSpan(0); }
    }

    public static TimeSpan toServerTimeTs(this Object obj)
    {
        if (obj != null)
        {
            DateTime dt = Convert.ToDateTime(obj.ToString());
            AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
            return atz.GetTimeReverse(dt.ToString(), HttpContext.Current.User.Identity.Name).ToDateTime().TimeOfDay;
        }
        else
        { return new TimeSpan(0); }
    }

    public static DateTime? toServerTime(this Object obj)
    {
        if (obj != null)
        {
            DateTime dt = Convert.ToDateTime(obj.ToString());
            AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
            return atz.GetTimeReverse(dt.ToString(), HttpContext.Current.User.Identity.Name).ToDateTime();
        }
        return null;
    }

}