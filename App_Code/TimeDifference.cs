﻿using System;
using System.Collections.Generic;
using System.Text;

public class TimeDifference
{
    public int Days { get; set; }
    public int Hours { get; set; }
    public int Minutes { get; set; }
    public int Seconds { get; set; }

    public static String GetTimeSpan(TimeSpan ts)
    {
        double milliSeconds = Convert.ToDouble(ts.TotalMilliseconds);
        //double Days = Math.Floor(milliSeconds / 86400000);
        double Hours = ts.TotalHours;
        double Minutes = Math.Floor(milliSeconds / 60000) % 60;
        double Seconds = Math.Floor(milliSeconds) % 60;
        String outputstring = "";
        //if (Days > 0)
        //{
        //    outputstring += Days + " days ";
        //}
        if (Hours > 0)
        {
            outputstring += Hours + " hrs ";
        }
        if (Minutes > 0)
        {
            outputstring += Minutes + " mins ";
        }
        else
        {
            outputstring += "0 mins ";
        }
        return outputstring;
    }
}
