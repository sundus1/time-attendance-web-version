﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Collections;
//using Newtonsoft.Json;
using System.Data;
using AuthModel;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Text;
using System.Globalization;
using System.Security.Cryptography;
using System.Configuration;
using System.Net;


/// <summary>
/// Summary description for Attendance
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class Attendance : System.Web.Services.WebService
{
    enum WeekDays
    {
        Sunday = 1,
        Monday = 2,
        Tuesday = 3,
        Wednesday = 4,
        Thursday = 5,
        Friday = 6,
        Saturday = 7
    }

    //Refer to table "EmailNotifications" before altering ID number/sequence
    enum Notification_Type
    {
        Clockin = 1,
        Clockout = 2,
        TimeEdit = 3,
        TimeDelete = 4,
        Absent = 5,
        Holiday = 6
    }

    //Developer and Avaima Support Emails 
    string DevEmail = "sundus_csit@yahoo.com";
    string supportEmail = "support@avaib.com";

    //To be used in pages 
    public Dictionary<string, string> TimeZone = new Dictionary<string, string>()
        {
        { "(UTC+00.00) Western Europe Time, London, Lisbon, Casablanca, Monrovia", "(UTC+00.00) Western Europe Time, London, Lisbon, Casablanca, Monrovia"},
        { "(UTC-12.00) Eniwetok, Kwajalein", "(UTC-12.00) Eniwetok, Kwajalein"},
        { "(UTC-11.00) Midway Island, Samoa", "(UTC-11.00) Midway Island, Samoa"},
          { "(UTC-10.00) Hawaii","(UTC-10.00) Hawaii" },
          {"(UTC-09.00) Alaska","(UTC-09.00) Alaska"  },
          { "(UTC-08.00) Pacific Time (US & Canada)","(UTC-08.00) Pacific Time (US & Canada)" },
          {  "(UTC-07.00) Mountain Time (US & Canada)", "(UTC-07.00) Mountain Time (US & Canada)" },
          {  "(UTC-06.00) Central Time (US & Canada), Mexico City", "(UTC-06.00) Central Time (US & Canada), Mexico City" },
          { "(UTC-05.00) Eastern Time (US & Canada), Bogota, Lima, Quito","(UTC-05.00) Eastern Time (US & Canada), Bogota, Lima, Quito" },
          {"(UTC-04.00) Atlantic Time (Canada), Caracas, La Paz","(UTC-04.00) Atlantic Time (Canada), Caracas, La Paz"  },
          {   "(UTC-03.30) Newfoundland", "(UTC-03.50) Newfoundland"},
        {"(UTC-03.00) Brazil, Buenos Aires, Georgetown","(UTC-03.00) Brazil, Buenos Aires, Georgetown" },
        {"(UTC-02.00) Mid-Atlantic","(UTC-02.00) Mid-Atlantic" },
        {"(UTC-01.00) Azores, Cape Verde Islands","(UTC-01.00) Azores, Cape Verde Islands" },
        { "(UTC+01.00) CET(Central Europe Time), Brussels, Copenhagen, Madrid, Paris","(UTC+01.00) CET(Central Europe Time), Brussels, Copenhagen, Madrid, Paris"},
        { "(UTC+02.00) EET(Eastern Europe Time), Kaliningrad, South Africa", "(UTC+02.00) EET(Eastern Europe Time), Kaliningrad, South Africa" },
        {"(UTC+03.00) Baghdad, Kuwait, Riyadh, Moscow, St. Petersburg, Volgograd, Nairobi","(UTC+03.00) Baghdad, Kuwait, Riyadh, Moscow, St. Petersburg, Volgograd, Nairobi" },
        {  "(UTC+03.30) Tehran", "(UTC+03.50) Tehran"},
        {   "(UTC+04.00) Abu Dhabi, Muscat, Baku, Tbilisi",  "(UTC+04.00) Abu Dhabi, Muscat, Baku, Tbilisi" },
        {  "(UTC+04.30) Kabul", "(UTC+04.50) Kabul"},
        {   "(UTC+05.00) Ekaterinburg, Islamabad, Karachi, Tashkent",  "(UTC+05.00) Ekaterinburg, Islamabad, Karachi, Tashkent"},
        { "(UTC+05.30) Bombay, Calcutta, Madras, New Delhi","(UTC+05.50) Bombay, Calcutta, Madras, New Delhi"},
        {  "(UTC+06.00) Almaty, Dhaka, Colombo",   "(UTC+06.00) Almaty, Dhaka, Colombo"},
        { "(UTC+07.00) Bangkok, Hanoi, Jakarta", "(UTC+07.00) Bangkok, Hanoi, Jakarta" },
        {"(UTC+08.00) Beijing, Perth, Singapore, Hong Kong, Chongqing, Urumqi, Taipei","(UTC+08.00) Beijing, Perth, Singapore, Hong Kong, Chongqing, Urumqi, Taipei" },
        { "(UTC+09.00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk",  "(UTC+09.00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk" },
        {  "(UTC+09.30) Adelaide, Darwin",  "(UTC+09.50) Adelaide, Darwin"},
         { "(UTC+10.00) EAST(East Australian Standard), Guam, Papua New Guinea, Vladivostok", "(UTC+10.00) EAST(East Australian Standard), Guam, Papua New Guinea, Vladivostok"},
         { "(UTC+11.00) Magadan, Solomon Islands, New Caledonia","(UTC+11.00) Magadan, Solomon Islands, New Caledonia"},
         {  "(UTC+12.00) Auckland, Wellington, Fiji, Kamchatka, Marshall Island",  "(UTC+12.00) Auckland, Wellington, Fiji, Kamchatka, Marshall Island"},
         { "(UTC+13.00) Nuku'alofa","(UTC+13.00) Nuku'alofa"}
    };

    public Dictionary<string, string> TimeZone_offset = new Dictionary<string, string>() {
  {
   "+00:00",
   "(UTC+00.00) Western Europe Time, London, Lisbon, Casablanca, Monrovia"
  }, {
   "-12:00",
   "(UTC-12.00) Eniwetok, Kwajalein"
  }, {
   "-11:00",
   "(UTC-11.00) Midway Island, Samoa"
  }, {
   "-10:00",
   "(UTC-10.00) Hawaii"
  }, {
   "-09:00",
   "(UTC-09.00) Alaska"
  }, {
   "-08:00",
   "(UTC-08.00) Pacific Time (US & Canada)"
  }, {
   "-07:00",
   "(UTC-07.00) Mountain Time (US & Canada)"
  }, {
   "-06:00",
   "(UTC-06.00) Central Time (US & Canada), Mexico City"
  }, {
   "-05:00",
   "(UTC-05.00) Eastern Time (US & Canada), Bogota, Lima, Quito"
  }, {
   "-04:00",
   "(UTC-04.00) Atlantic Time (Canada), Caracas, La Paz"
  }, {
   "-03:30",
   "(UTC-03.30) Newfoundland"
  }, {
   "-03:00",
   "(UTC-03.00) Brazil, Buenos Aires, Georgetown"
  }, {
   "-02:00",
   "(UTC-02.00) Mid-Atlantic"
  }, {
   "-01:00",
   "(UTC-01.00) Azores, Cape Verde Islands"
  }, {
   "+01:00",
   "(UTC+01.00) CET(Central Europe Time), Brussels, Copenhagen, Madrid, Paris"
  }, {
   "+02:00",
   "(UTC+02.00) EET(Eastern Europe Time), Kaliningrad, South Africa"
  }, {
   "+03:00",
   "(UTC+03.00) Baghdad, Kuwait, Riyadh, Moscow, St. Petersburg, Volgograd, Nairobi"
  }, {
   "+03:30",
   "(UTC+03.30) Tehran"
  }, {
   "+04:00",
   "(UTC+04.00) Abu Dhabi, Muscat, Baku, Tbilisi"
  }, {
   "+04:30",
   "(UTC+04.30) Kabul"
  }, {
   "+05:00",
   "(UTC+05.00) Ekaterinburg, Islamabad, Karachi, Tashkent"
  }, {
   "+05:30",
   "(UTC+05.30) Bombay, Calcutta, Madras, New Delhi"
  }, {
   "+06:00",
   "(UTC+06.00) Almaty, Dhaka, Colombo"
  }, {
   "+07:00",
   "(UTC+07.00) Bangkok, Hanoi, Jakarta"
  }, {
   "+08:00",
   "(UTC+08.00) Beijing, Perth, Singapore, Hong Kong, Chongqing, Urumqi, Taipei"
  }, {
   "+09:00",
   "(UTC+09.00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk"
  }, {
   "+09:30",
   "(UTC+09.30) Adelaide, Darwin"
  }, {
   "+10:00",
   "(UTC+10.00) EAST(East Australian Standard), Guam, Papua New Guinea, Vladivostok"
  }, {
   "+11:00",
   "(UTC+11.00) Magadan, Solomon Islands, New Caledonia"
  }, {
   "+12:00",
   "(UTC+12.00) Auckland, Wellington, Fiji, Kamchatka, Marshall Island"
  }, {
   "+13:00",
   "(UTC+13.00) Nuku'alofa"
  }
 };


    String appid = "6e815b00-6e13-4839-a57d-800a92809f21";
    AvaimaTimeZoneAPI atp = new AvaimaTimeZoneAPI();

    public string AddOrdinal(int num)
    {
        if (num <= 0) return num.ToString();
        switch (num % 100)
        {
            case 11:
            case 12:
            case 13:
                return num + "th";
        }
        switch (num % 10)
        {
            case 1:
                return num + "st";
            case 2:
                return num + "nd";
            case 3:
                return num + "rd";
            default:
                return num + "th";
        }
    }

    DataAccessLayer dal = new DataAccessLayer(false);
    DataAccessLayer dalAvaima = new DataAccessLayer(true);
    public Attendance()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    public String ConvertDataTableTojSonString(DataTable dataTable)
    {
        System.Web.Script.Serialization.JavaScriptSerializer serializer =
               new System.Web.Script.Serialization.JavaScriptSerializer();

        List<Dictionary<String, Object>> tableRows = new List<Dictionary<String, Object>>();

        Dictionary<String, Object> row;

        foreach (DataRow dr in dataTable.Rows)
        {
            row = new Dictionary<String, Object>();
            foreach (DataColumn col in dataTable.Columns)
            {
                row.Add(col.ColumnName, dr[col]);
            }
            tableRows.Add(row);
        }
        return serializer.Serialize(tableRows);
    }
    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    public string UpdateUsers()
    {
        string res = "";
        try
        {
            Hashtable ht = new Hashtable();
            DataTable UserProfile = dalAvaima.ExecuteSQLQueryData("select * from  cms_user where useremail IS NULL", ht);

            foreach (DataRow dr in UserProfile.Rows)
            {
                res = dr["userid"].ToString();

                if (dr["email"] != DBNull.Value && dr["email"] != "")
                {
                    string email = Decrypt(dr["email"].ToString(), true);
                    ht = new Hashtable();
                    ht["@email"] = email;
                    ht["@userid"] = dr["userid"].ToString();
                    //Uncomment only when updates need to make on LIVE 
                    //dalAvaima.ExecuteIUDQuery("Update cms_user SET useremail = @email  where userid = @userid", ht);

                    res += " " + email;
                }
            }
        }
        catch (Exception ex)
        {
        }

        return res;
    }


    [WebMethod]
    public void PerformClockNotifications()
    {
        AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
        try
        {
            AvaimaTimeZoneAPI atzAPI = new AvaimaTimeZoneAPI();
            List<WorkingHour> _standarWorkingHours = WorkingHour.GetAll().Where(u => u.Active == true && u.UserID == 0 && u.DayTitle == DateTime.Now.DayOfWeek.ToString()).ToList();
            List<WorkingHour> _usersWorkingHours = WorkingHour.GetAll().Where(u => u.Active == true && u.DayTitle == DateTime.Now.DayOfWeek.ToString()).ToList();
            List<AttUser> AttUsers = AttUser.GetAll().Where(u => u.Active == true && u.Category == false && !String.IsNullOrEmpty(u.Email)).ToList();
            List<String> instanceIDs = AttUsers.Select(u => u.InstanceID).ToList().Distinct().ToList();

            foreach (AttUser user in AttUsers)
            {
                if (Holiday.IsTodayHoliday(DateTime.Now.Date, user.InstanceID))
                {
                    continue;
                }
                DateTime lastSignIn = SP.GetWorkerLastSignIn(user.UserID.ToString());
                DateTime lastSignOut = SP.GetWorkerLastSignOut(user.UserID.ToString());

                WorkingHour notifWH = _usersWorkingHours.SingleOrDefault(u => u.UserID == user.UserID);
                if (notifWH == null)
                {
                    notifWH = _standarWorkingHours.SingleOrDefault(u => u.InstanceID == user.InstanceID);
                }

                if (notifWH != null && !notifWH.AutoPresent)
                {
                    DateTime sclockin = notifWH._userClockin;
                    DateTime sclockout = notifWH._userClockout;

                    if (DateTime.Now.Date > lastSignIn.Date)
                    {
                        if (DateTime.Now.TimeOfDay >= notifWH.Clockin.AddHours(-1).TimeOfDay && DateTime.Now.TimeOfDay < notifWH.Clockin.TimeOfDay)
                        {
                            String subject = "Remember to Clock-In using Avaima.com!";
                            StringBuilder body = new StringBuilder();
                            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                            body.Append("Remember to clock-in using Time & Attendance on <a href=\"www.avaima.com\">Avaima.com</a>.");
                            body.Append(Helper.AvaimaEmailSignature);
                            body.Append("</div>");
                            emailAPI.send_email(user.Email, "Attendance Management", "", body.ToString(), subject);
                        }
                        else if (DateTime.Now.TimeOfDay >= notifWH.Clockin.TimeOfDay && DateTime.Now.TimeOfDay < notifWH.Clockin.AddHours(1).TimeOfDay)
                        {
                            String subject = "You haven't clocked-in yet!";
                            StringBuilder body = new StringBuilder();
                            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                            body.Append("Your working hours are: " + sclockin.ToShortTimeString() + " - " + sclockout.ToShortTimeString() + ". It is past " + sclockin.ToShortTimeString() + " and you haven't clocked-in yet.");
                            body.Append("<br>In case you are absent use the link below to ");
                            body.Append("<a href='http://www.avaima.com/714fcd17-288b-4725-a0c2-7641c997eda4/workerverification.aspx?action=absent&uid=" + user.UserID + "&iid=" + user.InstanceID + "'>mark yourself as absent</a>");
                            body.Append(Helper.AvaimaEmailSignature);
                            body.Append("</div>");
                            emailAPI.send_email(user.Email, "Attendance Management", "", body.ToString(), subject);
                        }
                    }
                    else if (lastSignOut.Date != lastSignIn.Date)
                    {
                        if (lastSignOut == new DateTime(1900, 1, 1))
                        {
                            if (DateTime.Now.TimeOfDay >= notifWH.Clockout.AddHours(-1).TimeOfDay && DateTime.Now.TimeOfDay < notifWH.Clockout.TimeOfDay)
                            {
                                String subject = "Remember to Clock-out using Avaima.com!";
                                StringBuilder body = new StringBuilder();
                                body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                                body.Append("Remember to clock-out using Time & Attendance on <a href=\"www.avaima.com\">Avaima.com</a>!");
                                body.Append(Helper.AvaimaEmailSignature);
                                body.Append("</div>");
                                emailAPI.send_email(user.Email, "Attendance Management", "", body.ToString(), subject);
                            }
                            else if (DateTime.Now.TimeOfDay >= notifWH.Clockout.TimeOfDay && DateTime.Now.TimeOfDay < notifWH.Clockout.AddHours(1).TimeOfDay)
                            {
                                String subject = "You haven't clocked-out yet!";
                                StringBuilder body = new StringBuilder();
                                body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                                body.Append("Your working hours are: " + sclockin.ToShortTimeString() + " - " + sclockout.ToShortTimeString() + ". It is past " + sclockout.ToShortTimeString() + " and you haven't clocked-out yet.");
                                body.Append("<br>This email serves as a reminder in case you have forgotten.");
                                body.Append(Helper.AvaimaEmailSignature);
                                body.Append("</div>");
                                emailAPI.send_email(user.Email, "Attendance Management", "", body.ToString(), subject);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //emailAPI.send_email(WebConfigurationManager.AppSettings["DevEamil"], "Attendance Management", "", "Stack Trace: " + ex.StackTrace + "<br /><br />Inner Exception" + ex.InnerException, "Web Service Error PerformClockNotifications: " + ex.Message);
        }
    }


    [WebMethod]
    public string VerifyLogin(string emailId, string password)
    {
        string encryptedPassword = AvaimaUtilities.EncryptData(password);
        string encryptedEmail = AvaimaUtilities.EncryptData(emailId);

        Hashtable ht = new Hashtable();
        ht["@EncryptedEmailId"] = encryptedEmail;
        ht["@EncryptedPassword"] = encryptedPassword;

        if (dalAvaima.GetDataTable("sp_DVerifyLogin", ht).Rows.Count > 0)
        {
            return "TRUE";
        }
        else
        {
            return "FALSE";
        }
    }

    [WebMethod]
    public string GetSettingInfo(string emailId, string password)
    {
        string encryptedPassword = AvaimaUtilities.EncryptData(password);
        string encryptedEmail = AvaimaUtilities.EncryptData(emailId);

        Hashtable ht = new Hashtable();
        ht["@EncryptedEmailId"] = encryptedEmail;
        ht["@EncryptedPassword"] = encryptedPassword;
        return JsonConvert.SerializeObject(dalAvaima.GetDataTable("sp_DVerifyLogin", ht));
    }

    [WebMethod]
    public bool UpdateUserSetting(String weekendDays, int userid, string fullname)
    {
        bool res = false;
        int success = 0;
        string[] weekends = weekendDays.Split(',');
        Hashtable ht;
        string type = "0";  // 0 indicates Weekend

        ht = new Hashtable();
        ht["@userid"] = userid;
        ht["@title"] = fullname;
        dal.ExecuteIUDQuery("UPDATE attendence_management SET Title=@title where ID = @userid", ht);

        ht = new Hashtable();
        ht["@userid"] = userid;
        ht["@action"] = "delete";
        User_HolidaysWeekends(ht, "0");

        ht = new Hashtable();
        ht["@userid"] = userid;
        ht["@action"] = "insert";

        foreach (string weekday in weekends)
        {
            ht["@title"] = weekday;

            foreach (var enumValue in Enum.GetValues(typeof(WeekDays)))
            {
                if (enumValue.ToString() == weekday)
                {
                    ht["@day"] = (int)enumValue;
                    res = User_HolidaysWeekends(ht, "0");
                    if (!res)
                        success++;
                }
            }
        }

        if (success > 0)
            return false;
        else
            return true;

    }


    [WebMethod]
    public string GetUserHolidaysWeekends(int userid, string type, int year = 0)
    {
        DataTable dt = null;
        Hashtable ht = new Hashtable();
        ht["@userid"] = userid;
        ht["@action"] = "get";
        if (year > 0)
            ht["@year"] = year;

        if (type == "1")
        {
            dt = dal.GetDataTable("sp_Holidays", ht);
        }
        else if (type == "0")
        {
            dt = dal.GetDataTable("sp_Weekends", ht);
        }

        return JsonConvert.SerializeObject(dt);
    }

    [WebMethod]
    public bool Holidays(string userid, string action, string title, string date, bool everyyear, int rowid)
    {
        Hashtable ht = new Hashtable();
        ht["@userid"] = userid;
        ht["@action"] = action;
        ht["@title"] = title;
        ht["@date"] = date;
        ht["@day"] = 1;
        ht["@everyyear"] = everyyear;
        ht["@rowid"] = rowid;
        bool result = User_HolidaysWeekends(ht, "1");

        return result;
    }

    public bool User_HolidaysWeekends(Hashtable ht, string type)
    {
        bool res = false;
        if (type == "1")
        {
            res = dal.ExecuteNonQuery("sp_Holidays", ht);
        }
        else if (type == "0")
        {
            res = dal.ExecuteNonQuery("sp_Weekends", ht);
        }

        return res;
    }

    public DataTable GetTimezone(string userid)
    {
        DataTable dt = new DataTable();
        Hashtable ht;

        ht = new Hashtable();
        ht["@userid"] = Timezone_datetime(userid, "", "");
        dt = dalAvaima.GetDataTable("gettimezone", ht);
        return dt;
    }

    public string SetTimezone(string UserId, string Timezone, int dlsaving, int dlsavinghour)
    {
        Hashtable ht = new Hashtable();
        ht["@userid"] = UserId;
        ht["@timezone"] = Timezone;
        ht["@dlsaving"] = dlsaving;
        ht["@dlsavinghour"] = dlsavinghour;

        return JsonConvert.SerializeObject(dalAvaima.GetDataTable("sp_DUpdateUserSettings", ht));
    }
    [WebMethod]
    public string SetTimezone(string UserId, string Timezone, int dlsaving, int dlsavinghour, string Id)
    {
        Hashtable ht;

        ht = new Hashtable();
        ht["@UserId"] = Id;
        ht["@Accessed"] = 1;
        dal.ExecuteIUDQuery("Update attendence_management SET Accessed = @Accessed  where ID = @UserId", ht);

        ht = new Hashtable();
        ht["@userid"] = UserId;
        ht["@timezone"] = Timezone;
        ht["@dlsaving"] = dlsaving;
        ht["@dlsavinghour"] = dlsavinghour;

        return JsonConvert.SerializeObject(dalAvaima.GetDataTable("sp_DUpdateUserSettings", ht));
    }

    [WebMethod]
    public string GetLoginInfo(string emailId)
    {
        Hashtable ht = new Hashtable();
        ht["@EmailId"] = emailId;
        //string jsonString = "";
        //jsonString = JsonConvert.SerializeObject();
        return JsonConvert.SerializeObject(dal.GetDataTable("sp_DGetLoginInfo", ht));
    }
    [WebMethod]
    public string GetStatus(string userId)
    //Clock in/clockout status to visible btn
    {
        Hashtable ht = new Hashtable();
        ht["@ppid"] = userId;
        //JavaScriptSerializer serializer = new JavaScriptSerializer();
        return JsonConvert.SerializeObject(dal.GetDataTable("sp_DGetStatus", ht));
    }
    [WebMethod]
    public string ClockIn(string userid, string inaddress, string logIn_device, int in_location = 1, string comment = "", bool sendEmail = true, string automatic_clockin = "")
    {
        Hashtable ht;

        ht = new Hashtable();
        ht["@userid"] = Timezone_datetime(userid, "", "");
        DataTable dt = dalAvaima.GetDataTable("gettimezone", ht);

        string lastDate = Timezone_datetime(userid, Convert.ToDateTime(DateTime.UtcNow).ToString(), "date", dt.Rows[0]["timezone"] + "#" + dt.Rows[0]["dlsaving"] + "#" + dt.Rows[0]["dlsavinghour"]);
        //lastDate = (lastDate == "Today") ? DateTime.UtcNow.ToShortDateString() : lastDate;
        string lastTime = Timezone_datetime(userid, Convert.ToDateTime(DateTime.UtcNow).ToString(), "time");

        ht = new Hashtable();
        ht["@timezone"] = dt.Rows[0]["timezone"];
        ht["@dlsaving"] = dt.Rows[0]["dlsaving"];
        ht["@dlsavinghour"] = dt.Rows[0]["dlsavinghour"];
        ht["@ppid"] = userid;
        //ht["@lastin"] = Convert.ToDateTime(DateTime.Now);
        if (automatic_clockin != "")
            ht["@lastin"] = Convert.ToDateTime(automatic_clockin);
        else
            ht["@lastin"] = lastDate + " " + lastTime;
        ht["@inaddress"] = inaddress;
        ht["@in_location"] = in_location;
        ht["@comment"] = comment;
        ht["@logIn_device"] = logIn_device;
        ht["@instatus"] = VerifyInOutStatus(inaddress, userid) == true ? "Verified" : "Notverified";

        //string data = atp.GetTime(ht["@lastin"].ToString(), AppId).ToString();
        //if (userid == "9845")
        if (sendEmail)
            SendInOutStatus(userid, "clock-in", ht["@lastin"].ToString());
        //else
        //    SendInOutStatus(userid, "clock-in", lastTime);

        //JavaScriptSerializer serializer = new JavaScriptSerializer();
        return JsonConvert.SerializeObject(dal.GetDataTable("sp_DClockIn", ht));
    }
    [WebMethod]
    public string ClockOut(string userId, string outaddress, int rowId, string logOut_device, int out_location = 1, string comment = "", string autoclockout = null, bool sendEmail = true, string automatic_clockout = "")
    {
        Hashtable ht;
        ht = new Hashtable();
        ht["@userid"] = Timezone_datetime(userId, "", "");
        DataTable dt = dalAvaima.GetDataTable("gettimezone", ht);

        string lastDate = Timezone_datetime(userId, Convert.ToDateTime(DateTime.UtcNow).ToString(), "date", dt.Rows[0]["timezone"] + "#" + dt.Rows[0]["dlsaving"] + "#" + dt.Rows[0]["dlsavinghour"]);
        //lastDate = (lastDate == "Today") ? DateTime.UtcNow.ToShortDateString() : lastDate;
        string lastTime = Timezone_datetime(userId, Convert.ToDateTime(DateTime.UtcNow).ToString(), "time");
        bool isautoclockout = false;

        ht = new Hashtable();
        ht["@timezone"] = dt.Rows[0]["timezone"];
        ht["@dlsaving"] = dt.Rows[0]["dlsaving"];
        ht["@dlsavinghour"] = dt.Rows[0]["dlsavinghour"];
        ht["@ppid"] = userId;
        ht["@rowid"] = rowId;
        ht["@outaddress"] = outaddress;
        //ht["@lastout"] = Convert.ToDateTime(DateTime.Now);
        if (automatic_clockout != "")
        {
            ht["@lastout"] = Convert.ToDateTime(automatic_clockout);
        }
        else if (string.IsNullOrEmpty(autoclockout))
        {
            ht["@lastout"] = lastDate + " " + lastTime;
        }
        else
        {
            ht["@lastout"] = autoclockout;
            isautoclockout = true;
        }

        ht["@out_location"] = out_location;
        ht["@comment"] = comment;
        ht["@logOut_device"] = logOut_device;
        ht["@outstatus"] = VerifyInOutStatus(outaddress, userId) == true ? "Verified" : "Notverified";

        string response = (dal.ExecuteNonQuery("sp_DClockout", ht)).ToString();
        //JavaScriptSerializer serializer = new JavaScriptSerializer();

        //if (userId == "9845")

        if (sendEmail)
            SendInOutStatus(userId, "clock-out", ht["@lastout"].ToString(), isautoclockout);
        //else
        //    SendInOutStatus(userId, "clock-out", lastTime);


        return response;
    }

    public bool VerifyInOutStatus(string ipaddress, string userId)
    {
        Hashtable ht = new Hashtable();
        ht["@userId"] = userId;
        string InstanceID = dal.ExecuteSQLQueryData("select * from  schema_6e815b00_6e13_4839_a57d_800a92809f21.attendence_management where ID = @userId", ht).Rows[0]["InstanceID"].ToString();
        DataTable IPs = SP.GetIPAddresses(InstanceID);
        if (IPs.Rows.Count > 0)
        {
            if (checkDataTable(IPs, "ipaddress = '" + ipaddress + "'") > 0)
                return true;
            else
                return false;
        }
        else
            return true;

    }

    /// <summary>
    /// Check whether user clocked-in earlier than defined working hours.
    /// userid: Current User Id
    /// instanceid: Application Instance Id
    /// </summary>
    [WebMethod]
    public bool ClockIn_Check(string userid, string instanceid)
    {
        Hashtable ht;
        DataTable dt;
        string parentid = getParent(userid);
        DataTable workinghours = GetWorkingHours(parentid.ToInt32());
        DateTime utc_date = Convert.ToDateTime(DateTime.UtcNow);
        string user_time = Timezone_datetime(userid, utc_date.ToString(), "time");
        //"09:00";

        ht = new Hashtable();
        ht["@userid"] = Timezone_datetime(userid, "", "");
        dt = dalAvaima.GetDataTable("gettimezone", ht);

        string user_date = Timezone_datetime(userid, utc_date.ToString(), "date", dt.Rows[0]["timezone"] + "#" + dt.Rows[0]["dlsaving"] + "#" + dt.Rows[0]["dlsavinghour"]);
        //DataRow[] dr = workinghours.Select("DayTitle = '" + Convert.ToDateTime(user_date).DayOfWeek + "'");
        //List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(9845, "6752062f-f30f-4abe-b048-6cc2daee6d3d");
        List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(parentid.ToInt32(), instanceid);

        ht = new Hashtable();
        ht["@userid"] = parentid;
        DataTable settings = dal.ExecuteSQLQueryData("Select TOP 1 * from schema_6e815b00_6e13_4839_a57d_800a92809f21.ReportSettings where userid = @userid And Chophours = 1", ht);

        DateTime ChophoursFrom = new DateTime();

        if (settings.Rows.Count > 0)
            ChophoursFrom = Convert.ToDateTime(settings.Rows[0]["ChophoursFrom"]);

        if (workinghours.Rows.Count > 0)
        {
            DataRow[] dr = workinghours.Select("DayTitle = '" + Convert.ToDateTime(user_date).DayOfWeek + "'");

            if (exDays.Count > 0)
            {
                List<ExceptionalDay> exDay = exDays.Where(u => u.Date.Date == Convert.ToDateTime(user_date).Date).ToList();

                if (ChophoursFrom != System.DateTime.MinValue)
                {
                    if (exDay.Count > 0 &&
                   (exDay[0].Clockin.TimeOfDay > user_time.ToDateTime().TimeOfDay && Convert.ToDateTime(ChophoursFrom).TimeOfDay < user_time.ToDateTime().TimeOfDay)
                   )
                    {
                        return true;
                    }

                    if (dr.Count() > 0 &&
            (Convert.ToDateTime(dr[0]["Clockin"]).TimeOfDay > user_time.ToDateTime().TimeOfDay && Convert.ToDateTime(ChophoursFrom).TimeOfDay < user_time.ToDateTime().TimeOfDay))
                    {
                        return true;
                    }
                }
                else
                {
                    if (exDay.Count > 0 && (exDay[0].Clockin.TimeOfDay > user_time.ToDateTime().TimeOfDay))
                    {
                        return true;
                    }

                    if (dr.Count() > 0 && (Convert.ToDateTime(dr[0]["Clockin"]).TimeOfDay > user_time.ToDateTime().TimeOfDay))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public DataTable GetLastActivity(DateTime date, int userid, string type)
    {
        DataTable dt = new DataTable();
        Hashtable ht = new Hashtable();
        ht["@userid"] = userid;
        ht["@date"] = date.ToShortDateString();

        string query = "";

        if (type == "clock-in")
            query = " Top 1 ";

        if (type != "")
            dt = dal.ExecuteSQLQueryData("Select " + query + " * from workertrans where p_pid = @userid and convert(date,lastin) = convert(date, @date) and lastout IS NOT NULL Order by 1 desc", ht);
        else
            dt = dal.ExecuteSQLQueryData("Select " + query + " * from workertrans where p_pid = @userid and lastout IS NOT NULL Order by 1 desc", ht);

        return dt;
    }

    [WebMethod]
    public string MarkAbsent(int userid, string comment = "", DateTime? date = null, bool isAdjustment = false)
    {
        Hashtable ht = new Hashtable();
        ht["@userid"] = userid;
        ht["@comment"] = (comment == "") ? "Reason not added by the user" : comment;
        ht["@date"] = (date == null) ? System.DateTime.Now.Date : date;
        ht["@isadjustment"] = isAdjustment;

        SendAbsentNotification(userid.ToString(), Convert.ToDateTime(ht["@date"]), comment, "insert");

        return (dal.ExecuteNonQuery("sp_MarkAbsent", ht)).ToString();

    }

    [WebMethod]
    public string SignUp(string EmailId, string Password)
    {
        string encryptedPassword = AvaimaUtilities.EncryptData(Password);
        string encryptedEmail = AvaimaUtilities.EncryptData(EmailId);
        return "";
        //Hashtable ht = new Hashtable();
        //ht["@ppid"] = userId;
        //ht["@rowid"] = rowId;
        //ht["@outaddress"] = outaddress;
        //ht["@lastout"] = Convert.ToDateTime(DateTime.Now);
        ////JavaScriptSerializer serializer = new JavaScriptSerializer();
        //return (dal.ExecuteNonQuery("sp_DClockout", ht)).ToString();
    }


    public bool UserExists(string email, string InstanceID)
    {
        Hashtable ht = new Hashtable();
        ht["@email"] = email;
        ht["@insId"] = InstanceID;
        return (dal.ExecuteScalar("sp_DUserExists", ht));
    }

    [WebMethod]
    public string AddEmployee(string fname, string lname, string user_email, string pid, string AppId, string InstanceId, bool Managed)
    {
        try
        {
            bool email_check = true;
            string msg = "";
            try
            {
                AvaimaEmailAPI test_email = new AvaimaEmailAPI();
                test_email.send_email("sundusmusheer@gmail.com", "AVAIMA", "", "test", "test");
            }
            catch (Exception e)
            {
                email_check = false;
                msg = e.Message;
            }


            bool admin = false;
            if (email_check)
            {
                string AdminID = pid;
                string AdminAppId = AppId;

                //If an Admin adds NEW EMPLOYEE , assign NEW EMPLOYEE to Admin and make it as child of Admin's Parent 
                if (getParent(pid) != pid && isAdmin(pid.ToInt32()))
                {
                    admin = true;
                    pid = getParent(pid);   //Assign parent id 
                    AppId = Timezone_datetime(pid, DateTime.Now.ToString(), "");
                }
                //End

                if (!UserExists(user_email, InstanceId))
                {
                    Hashtable ht;

                    //Add Employee
                    ht = new Hashtable();
                    ht["@title"] = fname + " " + lname;
                    ht["@parentid"] = pid;
                    ht["@InstanceId"] = InstanceId;
                    ht["@datefield"] = Convert.ToDateTime(DateTime.Now);
                    ht["@category"] = 0;
                    ht["@email"] = user_email;
                    ht["@managed"] = Managed;
                    ht["@active"] = true;

                    string empId = (dal.ExecuteQuery("sp_insertworker", ht)).ToString();

                    UserAccess(empId, pid, InstanceId);
                    SetDefaultSetting(empId, pid);

                    //If Admin adds an employee - assign access
                    if (admin)
                    {
                        UserAccess(empId, AdminID, InstanceId);
                    }
                    //End

                    string userid = RegisterUser(user_email, InstanceId, AppId, empId, fname, lname);

                    ht = new Hashtable();
                    ht["@id"] = 0;
                    ht["@InstanceId"] = InstanceId;
                    ht["@userid"] = pid;
                    DataTable Admin = GetUserDetails(ht);

                    //If user doesnot exist
                    if (userid != "")
                    {
                        ht = new Hashtable();
                        ht.Add("email", user_email);
                        ht.Add("admin", Admin.Rows[0]["Title"]);
                        AvaimaUserProfile objwebser = new AvaimaUserProfile();
                        objwebser.insertUserFull(userid, "", ht["email"].ToString(), "", "", "",
                                             "", "", "", fname, lname, "", "", "", "", "", "", "", "", empId, InstanceId, "");
                        Insertuserrole(userid, "worker", ht["email"].ToString(), InstanceId, empId);

                        //Add T&A to user menu; AppId refers to parent user avaima "userid"
                        InsertMenu(userid, AppId, InstanceId);
                        //End

                        Hashtable url_ht = new Hashtable();
                        url_ht["userid"] = userid;
                        string password = Decrypt(dalAvaima.ExecuteSQLQueryData("Select * from cms_user where userid = @userid", url_ht).Rows[0]["password"].ToString(), true);
                        string url = WebURL("myattendance", empId, InstanceId, user_email, password, empId);

                        AvaimaEmailAPI email = new AvaimaEmailAPI();
                        StringBuilder subject = new StringBuilder();
                        StringBuilder body = new StringBuilder();

                        //subject.Append(ht["admin"] + " wants you to use Time & Attendance");
                        //body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                        //body.Append("<b>" + ht["admin"] + "</b> wants you to use \"Time & Attendance\" software application on Avaima.<br/>.<br/> You can use this application to clock-in / clock-out on a daily basis using your own user-account. Follow the steps below to start using the application:");
                        //body.Append("<ul>");
                        //body.Append("<li>Please click <a href='" + url + "'>HERE</a> and use \"Time & Attendance\"</li>");
                        //body.Append("<li>In another email you should have received request to sign up on Avaima.com if you are not signed up. Follow the instructions and sign up on Avaima &ndash; sign up is <b>FREE</b>. "
                        //    + "If you did not receive this email, go to Avaima.com and sign up using " + ht["email"].ToString() + " as your email address</li>");
                        //body.Append("<li>Go to <a href='http://www.avaima.com/signin'>www.avaima.com/signin</a> and login to your account</li>");
                        //body.Append("<li>Find and click \"Time & Attendance\" in the left menu</li>");
                        //body.Append("<li>\"Clock-In\" when you start your day at work and \"Clock-Out\" when you are done</li>");
                        //body.Append("</ul>");
                        //body.Append("<br/><br/>For further questions, comments or help contact support@avaima.com.");
                        //body.Append(Helper.AvaimaEmailSignature);
                        //body.Append("</div>");
                        //email.send_email(ht["email"].ToString(), "Time & Attendance", "", body.ToString(), subject.ToString());

                        subject.Append(ht["admin"] + " wants you to use Time & Attendance!");
                        //body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
                        body.Append("<div>");
                        body.Append("Hi " + fname + " " + lname + ",<br/><br/>");
                        body.Append(ht["admin"] + " added you in Time & Attendance software application by Avaima.com.<br/><br/>");
                        body.Append("Go to Avaima.com and access your account by using these credentials:<br/>");
                        body.Append("Username: " + user_email + "<br/>");
                        body.Append("Password: " + password + "<br/>");

                        body.Append("<br/>");

                        body.Append("OR access your account by clicking the link below:<br/><a href='" + url + "'> Click Here! </a><br/><br/>");
                        body.Append("Note: Email us at support@avaima.com in case you do not know the person who added you and want yourself removed.");

                        body.Append(Helper.AvaimaEmailSignature);
                        body.Append("</div>");
                        email.send_email(ht["email"].ToString(), "AVAIMA", "", body.ToString(), subject.ToString());
                        //Copy mail to support -NEW              
                        email.send_email(supportEmail, "AVAIMA", "", body.ToString(), subject.ToString());

                        //TEST
                        //email.send_email(DevEmail, "Time & Attendance", "", body.ToString(), subject.ToString());


                    }
                    return "Employee added successfully !";
                }
                else
                {
                    return "User email already exist!";
                }
            }
            else
                return msg;// "Add employee is currently down as our email provider's server is not working. Please try again later. We highly regret the inconvenience this has caused you and we are applying fixes so that it doesn't again.";

        }
        catch (Exception ex)
        {
            try
            {
                AvaimaEmailAPI failure_email = new AvaimaEmailAPI();
                //    failure_email.send_email(supportEmail, "AVAIMA", "", "Exception Message: " + ex.Message + " <br/>Detail: " + ex.StackTrace, "Add Employee Exception");
                failure_email.send_email(DevEmail, "AVAIMA", "", "Exception Message: " + ex.Message + " <br/>Detail: " + ex.StackTrace, "Add Employee Exception");
            }
            catch (Exception ee)
            {
            }

            //return ex.StackTrace;
            return "We are unable to add employee, please try again later. Sorry for the inconvenience!.";

        }
    }


    public bool UserAccess(string userid, string accessTo, string instanceid)
    {
        bool res = false;

        Hashtable ht = new Hashtable();
        ht["@userid"] = userid;
        ht["@permission"] = "allow";
        ht["@accessTo"] = accessTo;
        ht["@accessDate"] = DateTime.Now.ToString();
        ht["@instanceid"] = instanceid;
        ht["@type"] = "request";

        res = dal.ExecuteNonQuery("sp_StatisticsAccess", ht);
        return res;
    }

    public bool UserAccess_RevokeDel(string userid, string accessTo, string instanceid, string type)
    {
        Hashtable ht = new Hashtable();
        ht["@userid"] = userid;
        ht["@permission"] = "access_given";
        ht["@accessTo"] = accessTo;
        ht["@revokeDate"] = DateTime.Now.ToString();
        ht["@instanceid"] = instanceid;
        ht["@type"] = type;

        return dal.ExecuteNonQuery("sp_StatisticsAccess", ht);

    }

    public bool SetDefaultSetting(string empId, string pid)
    {
        Hashtable ht = new Hashtable();
        DataTable dt = JsonConvert.DeserializeObject<DataTable>(GetUserHolidaysWeekends(Convert.ToInt32(pid), "0"));

        foreach (DataRow row in dt.Rows)
        {
            ht["@userid"] = empId;
            ht["@action"] = "insert";
            ht["@title"] = row["title"].ToString();

            foreach (var enumValue in Enum.GetValues(typeof(WeekDays)))
            {
                if (enumValue.ToString() == row["title"].ToString())
                {
                    ht["@day"] = (int)enumValue;
                    User_HolidaysWeekends(ht, "0");
                }
            }
        }
        return true;
    }

    public DataTable _getuserprofilebyid(string userid)
    {
        DataTable dt = new DataTable();

        return dt;
    }

    [WebMethod]
    public string GetEmployees(string pid, string InstanceId, bool IsActive, int userid)
    {
        Hashtable ht = new Hashtable();
        ht["@id"] = pid;
        ht["@InstanceId"] = InstanceId;
        ht["@IsActive"] = IsActive;
        ht["@userid"] = userid;

        return JsonConvert.SerializeObject(dal.GetDataTable("sp_DGetAllAttendenceRecord1", ht));
    }

    public string RegisterUser(string user_email, string InstanceID, string AppId, string EmpId, string FirstName, string LastName)
    {
        Hashtable objuser;
        AddinstanceWS objaddinst = new AddinstanceWS();
        string userid = objaddinst.AddInstance(user_email, "6e815b00-6e13-4839-a57d-800a92809f21", InstanceID, AppId, FirstName, LastName);

        objuser = new Hashtable();
        objuser["@id"] = userid;
        objuser["@insId"] = InstanceID;

        int i = dal.ExecuteSQLQuery("select count(*) from tbluserrole where ownerid = @id and instanceid = @insId", objuser);
        if (i > 0)
        {
            objuser = new Hashtable();
            objuser["Pid"] = EmpId;
            dal.ExecuteNonQuery("del_records", objuser);

            return "";
        }

        return userid;

    }

    public void Insertuserrole(string userid, string role, string email, string InstanceId, string ids = "")
    {
        Hashtable ht = new Hashtable();
        ht["@id"] = ids;
        ht["@role"] = role;
        ht["@userid"] = userid;
        ht["@email"] = email;
        ht["@instanceid"] = InstanceId;

        dal.ExecuteNonQuery("adduserrole", ht);

    }

    public void InsertMenu(string userid, string parentid, string instanceid)
    {
        Hashtable ht = new Hashtable();
        ht["@userid"] = userid;
        ht["@appid"] = appid;
        DataTable dt = dalAvaima.ExecuteSQLQueryData("Select b.appid, b.apptitle, b.directoryname, b.defaultpage, b.isactive, b.apptype, b.usedirecturl, b.isexternal from core_mainmenu a JOIN app_applications b ON a.appid = b.appid Where a.userid = @userid AND a.isvisible = 1  and a.appid = @appid AND(LOWER(b.apptype) = 'sale' OR LOWER(b.apptype) = 'menu' OR LOWER(b.apptype) = 'freewidget')  Order By a.orderindex ASC", ht);

        if (dt.Rows.Count == 0)
        {
            ht = new Hashtable();
            ht["@userid"] = userid;
            dalAvaima.ExecuteIUDQuery("INSERT INTO core_mainmenu VALUES(@userid, '6e815b00-6e13-4839-a57d-800a92809f21', null, 1)", ht);

            ht = new Hashtable();
            ht["@userid"] = userid;
            ht["@parentid"] = parentid;
            ht["@instanceid"] = instanceid;
            ht["@guid"] = new Guid();

            dalAvaima.ExecuteIUDQuery("INSERT INTO app_userapps VALUES (@userid, '6e815b00-6e13-4839-a57d-800a92809f21', @instanceid, 'Time & Attendance', @userid, getdate(), @guid,@parentid, 0, @parentid, 0, 0, null, 4, null, null)", ht);

        }
    }

    [WebMethod]
    public bool RequestAdminRights_Email(int userid, int accessTo, string Title, string permission, string instanceid, bool access_taken, bool access_given)
    {
        Hashtable ht;
        AvaimaEmailAPI email = new AvaimaEmailAPI();
        StringBuilder subject = new StringBuilder();
        StringBuilder body = new StringBuilder();

        ht = new Hashtable();
        ht["@id"] = 0;
        ht["@InstanceId"] = instanceid;
        ht["@userid"] = userid;
        DataTable UserDetail = GetUserDetails(ht);

        ht = new Hashtable();
        ht["@permission"] = permission;
        ht["@instanceid"] = instanceid;
        ht["@type"] = "request";

        if (access_taken)
        {
            ht["@userid"] = userid;
            ht["@accessTo"] = accessTo;

            subject.Append("Request for \"Admin Rights\" in Time & Attendance");
            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
            body.Append("<b>" + Title + "</b> added you in his network of \"Time & Attendance\" software application on Avaima.");
            body.Append("<ul>");
            body.Append("Do you want to grant access to " + Title + " to view/modify your \"Time & Attendance\" statistics?<br>");
        }
        else if (access_given)
        {
            ht["@userid"] = accessTo;
            ht["@accessTo"] = userid;

            subject.Append("\"Admin Rights\" in Time & Attendance");
            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
            body.Append("<b>" + Title + "</b> added you in his network of \"Time & Attendance\" software application on Avaima.");
            body.Append("<ul>");
            body.Append(Title + " has given access to view/modify his \"Time & Attendance\" statistics<br>");
        }


        bool res = dal.ExecuteNonQuery("sp_StatisticsAccess", ht);

        if (res)
        {
            body.Append("Please login to your account using <a href='http://www.avaima.com/signin'>www.avaima.com</a> OR use \"Time & Attendance\" application to grant access.");
            body.Append("</ul>");
            body.Append("<br/>For further questions, comments or help contact support@avaima.com.");
            body.Append(Helper.AvaimaEmailSignature);
            body.Append("</div>");

            email.send_email(UserDetail.Rows[0]["email"].ToString(), "Time & Attendance", "", body.ToString(), subject.ToString());
            return true;
        }

        return false;


    }
    [WebMethod]
    public bool ApproveDenyAdminRights_Email(int userid, int accessTo, string permission, string accessDate, string instanceid, int rowid)
    {
        Hashtable ht;

        ht = new Hashtable();
        ht["@id"] = 0;
        ht["@InstanceId"] = instanceid;
        ht["@userid"] = accessTo;
        DataTable UserDetail = GetUserDetails(ht);


        ht = new Hashtable();
        ht["@id"] = 0;
        ht["@InstanceId"] = instanceid;
        ht["@userid"] = userid;
        DataTable SenderDetail = GetUserDetails(ht);

        ht = new Hashtable();
        ht["@userid"] = userid;
        ht["@permission"] = permission;
        ht["@accessTo"] = accessTo;

        if (permission == "allow")
            ht["@accessDate"] = DateTime.Now.ToString();

        ht["@instanceid"] = instanceid;
        ht["@rowid"] = rowid;
        ht["@type"] = "approve";

        bool res = dal.ExecuteNonQuery("sp_StatisticsAccess", ht);

        AvaimaEmailAPI email = new AvaimaEmailAPI();
        StringBuilder subject = new StringBuilder();
        StringBuilder body = new StringBuilder();

        if (res)
        {
            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
            if (permission == "allow")
            {
                subject.Append("Request approved for \"Admin Rights\" in Time & Attendance");
                body.Append("<b>" + SenderDetail.Rows[0]["Title"] + "</b> granted you access!<br/>");
                body.Append("<ul>");
                body.Append("You can view/modify  <b> " + SenderDetail.Rows[0]["Title"] + " </b> Clockin\\Clockout data in \"Time & Attendance\"");
            }
            else
            {
                subject.Append("Request denied for \"Admin Rights\" in Time & Attendance");
                body.Append("<b>" + SenderDetail.Rows[0]["Title"] + "</b> rejected your request for access!<br/>");
                body.Append("<ul>");
                body.Append("You can request again to view/modify  <b> " + SenderDetail.Rows[0]["Title"] + " </b> Clockin\\Clockout data in \"Time & Attendance\"");
            }

            body.Append("Please login to your account using <a href='http://www.avaima.com/signin'>www.avaima.com</a> OR use \"Time & Attendance\" application to view access.");
            body.Append("</ul>");
            body.Append("<br/>For further questions, comments or help contact support@avaima.com.");
            body.Append(Helper.AvaimaEmailSignature);
            body.Append("</div>");
            email.send_email(UserDetail.Rows[0]["email"].ToString(), "Time & Attendance", "", body.ToString(), subject.ToString());
            return true;
        }

        return false;
    }

    [WebMethod]
    public bool RevokeAdminRights_Email(int userid, int accessTo, string Title, string permission, string instanceid, bool access_taken, bool access_given)
    {
        Hashtable ht;

        AvaimaEmailAPI email = new AvaimaEmailAPI();
        StringBuilder subject = new StringBuilder();
        StringBuilder body = new StringBuilder();
        DataTable UserDetail;

        ht = new Hashtable();
        ht["@id"] = 0;
        ht["@InstanceId"] = instanceid;


        if (access_given)
        {
            ht["@userid"] = accessTo;

            subject.Append("\"Admin Rights\" access revoked in Time & Attendance");
            body.Append("<b>" + Title + "</b> has taken back access!<br/>");
            body.Append("You are no longer able to view/modify Clockin/Clockout statistics<br/>");
            body.Append("However you may request again<br/>");
            body.Append("<ul>");
        }

        if (access_taken)
        {
            ht["@userid"] = userid;

            subject.Append("\"Admin Rights\" access revoked in Time & Attendance");
            body.Append("<b>" + Title + "</b> has denied from access!<br/>");
            body.Append("He longer want to view/modify your Clockin/Clockout statistics<br/>");
            body.Append("However you may request again<br/>");
            body.Append("<ul>");
        }

        //Email Id
        UserDetail = GetUserDetails(ht);

        ht = new Hashtable();
        ht["@userid"] = userid;
        ht["@permission"] = permission;
        ht["@accessTo"] = accessTo;
        ht["@revokeDate"] = DateTime.Now.ToString();
        ht["@instanceid"] = instanceid;
        ht["@type"] = "revoke";

        bool res = dal.ExecuteNonQuery("sp_StatisticsAccess", ht);

        if (res)
        {
            body.Append("Please login to your account using <a href='http://www.avaima.com/signin'>www.avaima.com</a> OR use \"Time & Attendance\" application to view access.");
            body.Append("</ul>");
            body.Append("<br/>For further questions, comments or help contact support@avaima.com.");
            body.Append(Helper.AvaimaEmailSignature);
            body.Append("</div>");
            email.send_email(UserDetail.Rows[0]["email"].ToString(), "Time & Attendance", "", body.ToString(), subject.ToString());

            //Save Stats for later use
            //            Hashtable statistics = JsonConvert.DeserializeObject<Hashtable>(CalculateStatistics(8, DateTime.Now.Year, userid, UserDetail.Rows[0]["InstanceId"].ToString()));


            return true;
        }

        return false;
    }

    public DataTable GetUserDetails(Hashtable ht)
    {
        DataTable dt = dal.GetDataTable("getfilteredattendancerecordbyid", ht);
        return dt;
    }

    [WebMethod]
    public string GetAdminRightsRequests(int userid)
    {
        Hashtable ht = new Hashtable();
        ht["@userid"] = userid;

        return JsonConvert.SerializeObject(dal.GetDataTable("sp_GetAdminRightsRequest", ht));
    }

    [WebMethod]
    public string GetUsers(int userid, string InstanceId)
    {
        Hashtable ht = new Hashtable();
        ht["@userid"] = userid;
        ht["@instanceid"] = InstanceId;

        return JsonConvert.SerializeObject(dal.GetDataTable("sp_GetUsers", ht));

    }

    [WebMethod]
    public bool SendInOutStatus(string userid, string type, string data, bool isautoclockout = false)
    {
        int notification_type = 0;

        try
        {
            Hashtable ht = new Hashtable();
            ht["@userid"] = userid;
            DataTable dt = dal.GetDataTable("sp_GetUserAccess", ht);
            DataRow[] sender = dt.Select("ID=" + userid);

            AvaimaEmailAPI email = new AvaimaEmailAPI();
            StringBuilder subject = new StringBuilder();
            StringBuilder body = new StringBuilder();
            string subjectTxt = "";
            string bodyTxt = "";
            DataTable last_activity = new DataTable();
            DateTime date;
            TimeSpan WorkedHours = new TimeSpan();
            string number = "";

            if (type == "clock-in")
            {
                notification_type = Notification_Type.Clockin.ToInt32();

                last_activity = GetLastActivity(Convert.ToDateTime(data), userid.ToInt32(), type);

                number = AddOrdinal(Get_TimeinoutCount(1, userid, data));
                //number = AddOrdinal(Get_TimeinoutCount(1, userid, DateTime.Now.ToShortDateString()));

                subjectTxt = number + " Clock-IN by " + sender[0]["Title"].ToString();

                bodyTxt = "<b> Clocked-In: </b>" + Convert.ToDateTime(data).ToShortTimeString() + "<br/>";
                //if (userid == "9845")
                //{
                if (last_activity.Rows.Count > 0)
                {
                    bodyTxt += "<br/><b>Time since last Clock-Out: </b>" +
                     ConvertTimeSpan(Convert.ToDateTime(data).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(last_activity.Rows[0]["lastout"].ToString()))));
                    //ConvertTimeSpan(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + data)).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(last_activity.Rows[0]["lastout"].ToString()))));
                    //  ConvertTimeSpan(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(data)).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(last_activity.Rows[0]["lastout"]))));

                    WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(last_activity.Rows[0]["lastout"])).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(last_activity.Rows[0]["lastin"])));
                    string timespent = (ConvertTimeSpan(WorkedHours) != "") ? ConvertTimeSpan(WorkedHours) : "0";
                    bodyTxt += "<br/> <b>Time spent today (before this Clock-IN):</b> " + timespent + "<br/>";

                    bodyTxt += "<br/><b>Related Information:</b>";
                    bodyTxt += "<br/> <b>Last Clock-Out: </b>" + Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToString("hh:mm tt") + " on " + Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToString("dddd, MMMM dd, yyyy");
                    bodyTxt += "<br/> <b>Last Clock-In: </b>" + Convert.ToDateTime(last_activity.Rows[0]["lastin"]).ToString("hh:mm tt") + " on " + Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToString("dddd, MMMM dd, yyyy");

                }
                else
                {
                    DateTime yesterday = Convert.ToDateTime(data).AddDays(-1);
                    last_activity = GetLastActivity(Convert.ToDateTime(yesterday), userid.ToInt32(), ""); //type = "" bcx we want yesterday's activity worklog

                    if (last_activity.Rows.Count > 0)
                    {
                        //string lastout_date = Timezone_datetime(userid, last_activity.Rows[0]["lastout"].ToString(), "date");
                        //if (lastout_date == "Today")
                        //    lastout_date = DateTime.Now.ToShortDateString();

                        string lastout_date = Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToShortDateString();

                        bodyTxt += "<br/><b>Time since last Clock-Out: </b>" +
                                      //ConvertTimeSpan(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(data)).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(last_activity.Rows[0]["lastout"]))));
                                      //ConvertTimeSpan(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + data)).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToShortDateString() + " " + Timezone_datetime(userid, last_activity.Rows[0]["lastout"].ToString(), "time"))))) + "<br/>";
                                      //ConvertTimeSpan(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + data)).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(Convert.ToDateTime(lastout_date).ToShortDateString() + " " + Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToString("hh:mm tt"))))) + "<br/>";
                                      ConvertTimeSpan(Convert.ToDateTime(data).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(Convert.ToDateTime(lastout_date).ToShortDateString() + " " + Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToString("hh:mm tt"))))) + "<br/>";

                        //Timezone_datetime(userid, last_activity.Rows[0]["lastout"].ToString(), "date")


                        //foreach (DataRow dr in last_activity.Rows)
                        //{
                        //    DateTime datein = Convert.ToDateTime(dr["lastin"]);
                        //    DateTime dateout = Convert.ToDateTime(dr["lastout"]);
                        //    WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(dateout).Subtract(TimeZoneInfo.ConvertTimeToUtc(datein));
                        //}

                        //string timespent = (ConvertTimeSpan(WorkedHours) != "") ? ConvertTimeSpan(WorkedHours) : "0";
                        ////bodyTxt += "<br/> <b>Time spent before Clock-IN:</b> " + timespent + " ("+ Convert.ToDateTime(last_activity.Rows[0]["lastin"]).ToString("dddd, MMMM dd, yyyy") + ")<br/>";

                        bodyTxt += "<br/><b>Related Information:</b>";
                        bodyTxt += "<br/> <b>Last Clock-Out: </b>" + Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToString("hh:mm tt") + " on "
                            + ((Convert.ToDateTime(last_activity.Rows[0]["lastout"].ToString()).Date == DateTime.Now.Date) ? "Today" : Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToString("dddd, MMMM dd, yyyy"));
                        bodyTxt += "<br/> <b>Last Clock-In: </b>" + Convert.ToDateTime(last_activity.Rows[0]["lastin"]).ToString("hh:mm tt") + " on "
                        //+ Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToString("dddd, MMMM dd, yyyy");
                        + ((Convert.ToDateTime(last_activity.Rows[0]["lastin"].ToString()).Date == DateTime.Now.Date) ? "Today" : Convert.ToDateTime(last_activity.Rows[0]["lastin"]).ToString("dddd, MMMM dd, yyyy"));
                    }
                    else
                    {
                        bodyTxt += "<br/><b>Time since last Clock-Out: </b> 0 <br/>";
                        bodyTxt += "<br/><b>Related Information:</b>";
                        bodyTxt += "<br/> <b>Last Clock-Out: </b> Never";
                        bodyTxt += "<br/> <b>Last Clock-In: </b> Never";
                    }
                }
                //}
                //else
                //    bodyTxt += " <br/>";
            }
            else if (type == "clock-out")
            {
                notification_type = Notification_Type.Clockout.ToInt32();

                last_activity = GetLastActivity(Convert.ToDateTime(data), userid.ToInt32(), type);

                if (!isautoclockout)
                {
                    number = AddOrdinal(Get_TimeinoutCount(2, userid, data));
                    subjectTxt = number + " Clock-OUT by " + sender[0]["Title"].ToString();
                    bodyTxt += "<b>Clocked-Out: </b>" + Convert.ToDateTime(data).ToString("dddd, MMMM dd, yyyy") + " at " + Convert.ToDateTime(data).ToShortTimeString() + " - ";
                }
                else
                {
                    subjectTxt = "Clock-Out with Time Edit by " + sender[0]["Title"].ToString();
                    bodyTxt += "<b>I forgot to Clock-Out on <span style='color:red;' >" + Convert.ToDateTime(data).ToString("dddd, MMMM dd, yyyy") + "</span>, so I chose to edit time.</b><br/><br/>";
                    bodyTxt += "<b>Updated Clock-Out Time: <span style='color:red;' >" + Convert.ToDateTime(data).ToShortTimeString() + "</span></b><br/>";
                }

                //bodyTxt += "<b> Clocked-Out: </b>" + Convert.ToDateTime(data).ToString("dddd, MMMM dd, yyyy") + " at " + Convert.ToDateTime(data).ToShortTimeString();

                if (last_activity.Rows.Count > 0)
                {
                    int i = 0;

                    foreach (DataRow dr in last_activity.Rows)
                    {
                        DateTime datein = Convert.ToDateTime(dr["lastin"]);
                        DateTime dateout = Convert.ToDateTime(dr["lastout"]);
                        WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(dateout).Subtract(TimeZoneInfo.ConvertTimeToUtc(datein));

                        if (i == 0)
                        {
                            string timespent = (ConvertTimeSpan(WorkedHours) != "") ? ConvertTimeSpan(WorkedHours) : "0";
                            bodyTxt += "<b>Worked: </b>" + timespent + "<br/>";
                            bodyTxt += "<b>Last Clock-In:</b> " + Convert.ToDateTime(dr["lastin"]).ToString("hh:mm tt") + " on " + Convert.ToDateTime(datein).ToString("dddd, MMMM dd, yyyy");
                        }
                        i++;
                    }

                    bodyTxt += "<br/><b>Total Work on " + Convert.ToDateTime(data).ToString("dddd, MMMM dd, yyyy") + ":</b> " + ConvertTimeSpan(WorkedHours) + "<br/>";
                }
            }

            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
            subject.Append(subjectTxt);
            body.Append("<br/>" + bodyTxt);
            body.Append(Helper.AvaimaEmailSignature);
            body.Append("</div>");

            //dt = dt.Select("isManaged=1").CopyToDataTable();

            List<string> emails = new List<string>();

            //Filter users to check if Email Notifications are enabled
            dt = EmailNotifications_Enabled(dt, notification_type);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ht = new Hashtable();
                ht["@userid"] = dt.Rows[i]["ID"].ToString();
                DataTable settings = dal.ExecuteSQLQueryData("Select * from ReportSettings where userid = @userid", ht);

                //Send email notifications to "Admin Managed" users too  -  Change in logic by Sir Fahad on customer demand
                //if (Convert.ToBoolean(dt.Rows[i]["isManaged"]))
                //{
                //    if (dt.Rows[i]["sendemail"].ToInt32() != -1) //If user has disabled email notifications
                //        emails.Add(dt.Rows[i]["email"].ToString());

                //    if (settings.Rows.Count > 0)
                //    {
                //        string[] ed = DataList(settings);
                //        foreach (var val in ed)
                //            emails.Add(val);
                //    }
                //}
                //else
                //{
                //    if (userid != dt.Rows[i]["ID"].ToString())
                //    {
                if (dt.Rows[i]["sendemail"].ToInt32() != -1) //If user has disabled email notifications
                    emails.Add(dt.Rows[i]["email"].ToString());

                if (settings.Rows.Count > 0)
                {
                    string[] ed = DataList(settings);
                    foreach (var val in ed)
                        emails.Add(val);
                }
                //    }
                //}
            }

            foreach (var emailaddress in emails)
            {
                email.send_email(emailaddress, "Time & Attendance", "", body.ToString(), subject.ToString());
            }

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public bool SendInOutStatus_Old(string userid, string type, string data)
    {
        Hashtable ht = new Hashtable();
        ht["@userid"] = userid;
        DataTable dt = dal.GetDataTable("sp_GetUserAccess", ht);
        DataRow[] sender = dt.Select("ID=" + userid);

        AvaimaEmailAPI email = new AvaimaEmailAPI();
        StringBuilder subject = new StringBuilder();
        StringBuilder body = new StringBuilder();
        string subjectTxt = "";
        string bodyTxt = "";
        DataTable last_activity = new DataTable();
        DateTime date;
        TimeSpan WorkedHours = new TimeSpan();

        if (type == "clock-in")
        {
            last_activity = GetLastActivity(Convert.ToDateTime(data), userid.ToInt32(), type);

            subjectTxt = "Clock-IN by " + sender[0]["Title"].ToString();

            bodyTxt = "<b> Clocked-In: </b>" + Convert.ToDateTime(data).ToShortTimeString() + "<br/>";
            //if (userid == "9845")
            //{
            if (last_activity.Rows.Count > 0)
            {
                bodyTxt += "<br/><b>Time since last Clock-Out: </b>" +
                ConvertTimeSpan(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + data)).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(last_activity.Rows[0]["lastout"].ToString()))));
                //  ConvertTimeSpan(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(data)).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(last_activity.Rows[0]["lastout"]))));

                WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(last_activity.Rows[0]["lastout"])).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(last_activity.Rows[0]["lastin"])));
                string timespent = (ConvertTimeSpan(WorkedHours) != "") ? ConvertTimeSpan(WorkedHours) : "0";
                bodyTxt += "<br/> <b>Time spent today (before this Clock-IN):</b> " + timespent + "<br/>";

                bodyTxt += "<br/><b>Related Information:</b>";
                bodyTxt += "<br/> <b>Last Clock-Out: </b>" + Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToString("hh:mm tt") + " on " + Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToString("dddd, MMMM dd, yyyy");
                bodyTxt += "<br/> <b>Last Clock-In: </b>" + Convert.ToDateTime(last_activity.Rows[0]["lastin"]).ToString("hh:mm tt") + " on " + Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToString("dddd, MMMM dd, yyyy");

            }
            else
            {
                DateTime yesterday = Convert.ToDateTime(data).AddDays(-1);
                last_activity = GetLastActivity(Convert.ToDateTime(yesterday), userid.ToInt32(), ""); //type = "" bcx we want yesterday's activity worklog

                if (last_activity.Rows.Count > 0)
                {
                    //string lastout_date = Timezone_datetime(userid, last_activity.Rows[0]["lastout"].ToString(), "date");
                    //if (lastout_date == "Today")
                    //    lastout_date = DateTime.Now.ToShortDateString();

                    string lastout_date = Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToShortDateString();

                    bodyTxt += "<br/><b>Time since last Clock-Out: </b>" +
                 //ConvertTimeSpan(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(data)).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(last_activity.Rows[0]["lastout"]))));
                 //ConvertTimeSpan(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + data)).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToShortDateString() + " " + Timezone_datetime(userid, last_activity.Rows[0]["lastout"].ToString(), "time"))))) + "<br/>";
                 ConvertTimeSpan(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + data)).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(Convert.ToDateTime(lastout_date).ToShortDateString() + " " + Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToString("hh:mm tt"))))) + "<br/>";

                    //Timezone_datetime(userid, last_activity.Rows[0]["lastout"].ToString(), "date")


                    //foreach (DataRow dr in last_activity.Rows)
                    //{
                    //    DateTime datein = Convert.ToDateTime(dr["lastin"]);
                    //    DateTime dateout = Convert.ToDateTime(dr["lastout"]);
                    //    WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(dateout).Subtract(TimeZoneInfo.ConvertTimeToUtc(datein));
                    //}

                    //string timespent = (ConvertTimeSpan(WorkedHours) != "") ? ConvertTimeSpan(WorkedHours) : "0";
                    ////bodyTxt += "<br/> <b>Time spent before Clock-IN:</b> " + timespent + " ("+ Convert.ToDateTime(last_activity.Rows[0]["lastin"]).ToString("dddd, MMMM dd, yyyy") + ")<br/>";

                    bodyTxt += "<br/><b>Related Information:</b>";
                    bodyTxt += "<br/> <b>Last Clock-Out: </b>" + Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToString("hh:mm tt") + " on "
                        + ((Convert.ToDateTime(last_activity.Rows[0]["lastout"].ToString()).Date == DateTime.Now.Date) ? "Today" : Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToString("dddd, MMMM dd, yyyy"));
                    bodyTxt += "<br/> <b>Last Clock-In: </b>" + Convert.ToDateTime(last_activity.Rows[0]["lastin"]).ToString("hh:mm tt") + " on "
                    //+ Convert.ToDateTime(last_activity.Rows[0]["lastout"]).ToString("dddd, MMMM dd, yyyy");
                    + ((Convert.ToDateTime(last_activity.Rows[0]["lastin"].ToString()).Date == DateTime.Now.Date) ? "Today" : Convert.ToDateTime(last_activity.Rows[0]["lastin"]).ToString("dddd, MMMM dd, yyyy"));
                }
                else
                {
                    bodyTxt += "<br/><b>Time since last Clock-Out: </b> 0 <br/>";
                    bodyTxt += "<br/><b>Related Information:</b>";
                    bodyTxt += "<br/> <b>Last Clock-Out: </b> Never";
                    bodyTxt += "<br/> <b>Last Clock-In: </b> Never";
                }
            }
            //}
            //else
            //    bodyTxt += " <br/>";
        }
        else if (type == "clock-out")
        {
            //DataTable user_joindate = dal.ExecuteSQLQueryData("Select top 1 * from schema_6e815b00_6e13_4839_a57d_800a92809f21.workertrans where rowID=@id");
            last_activity = GetLastActivity(Convert.ToDateTime(data), userid.ToInt32(), type);

            subjectTxt = "Clock-OUT by " + sender[0]["Title"].ToString();

            //if (userid == "9845")
            //{
            bodyTxt = "<b> Clocked-Out: </b>" + Convert.ToDateTime(data).ToShortTimeString();
            if (last_activity.Rows.Count > 0)
            {
                int i = 0;

                foreach (DataRow dr in last_activity.Rows)
                {
                    DateTime datein = Convert.ToDateTime(dr["lastin"]);
                    DateTime dateout = Convert.ToDateTime(dr["lastout"]);
                    WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(dateout).Subtract(TimeZoneInfo.ConvertTimeToUtc(datein));

                    if (i == 0)
                    {
                        string timespent = (ConvertTimeSpan(WorkedHours) != "") ? ConvertTimeSpan(WorkedHours) : "0";
                        bodyTxt += " - <b>Worked: </b>" + timespent + "<br/>";
                        bodyTxt += "<b>Last Clock-In:</b> " + Convert.ToDateTime(dr["lastin"]).ToString("hh:mm tt") + " on " + Convert.ToDateTime(datein).ToString("dddd, MMMM dd, yyyy");
                    }
                    i++;
                }

                bodyTxt += "<br/><b>Total Work on " + Convert.ToDateTime(data).ToString("dddd, MMMM dd, yyyy") + ":</b> " + ConvertTimeSpan(WorkedHours) + "<br/>";
            }
            //}
            //else
            //    bodyTxt = "<b> Clocked-Out: </b>" + Convert.ToDateTime(data).ToShortTimeString() + " <br/>";

        }

        body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
        subject.Append(subjectTxt);
        body.Append("<br/>" + bodyTxt);
        body.Append(Helper.AvaimaEmailSignature);
        body.Append("</div>");

        //dt = dt.Select("isManaged=1").CopyToDataTable();

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            //if (Convert.ToBoolean(dt.Rows[i]["isManaged"]))
            //{
            //    email.send_email(dt.Rows[i]["email"].ToString(), "Time & Attendance", "", body.ToString(), subject.ToString());
            //}
            //else
            //{
            //    if (userid != dt.Rows[i]["ID"].ToString())
            //    {
            email.send_email(dt.Rows[i]["email"].ToString(), "Time & Attendance", "", body.ToString(), subject.ToString());
            //    }
            //}
        }

        return true;
    }

    private int Get_TimeinoutCount(int num, string id, string date)
    {
        int count = 0;
        Hashtable ht = new Hashtable();
        ht["@id"] = id;
        ht["@date"] = date;

        /*
         * 1: clockin
         * 2: clockout
         * 0: edit
         */

        if (num == 1)
        {
            count = dal.ExecuteSQLQueryData("Select Count(*) as count from schema_6e815b00_6e13_4839_a57d_800a92809f21.workertrans where p_pid = @id and convert(date,lastin) = convert(date,@date)", ht).Rows[0]["count"].ToInt32();
            count = count + 1;
        }
        else if (num == 2)
        {
            count = dal.ExecuteSQLQueryData("Select Count(*) as count from schema_6e815b00_6e13_4839_a57d_800a92809f21.workertrans where p_pid = @id and convert(date,lastout) = convert(date,@date)", ht).Rows[0]["count"].ToInt32();
        }
        else
        {
            count = dal.ExecuteSQLQueryData("Select time_edit as count from schema_6e815b00_6e13_4839_a57d_800a92809f21.workertrans where rowID = @id", ht).Rows[0]["count"].ToInt32();
            ht["@time_edit"] = count + 1;
            dal.ExecuteIUDQuery("Update schema_6e815b00_6e13_4839_a57d_800a92809f21.workertrans SET time_edit = @time_edit where rowID = @id", ht);
        }
        return count;
    }

    public string Timezone_datetime(string userid, string datetime, string type, string zone = "")
    {
        AvaimaTimeZoneAPI atp = new AvaimaTimeZoneAPI();
        Hashtable ht = new Hashtable();
        ht["@userid"] = userid;
        string AppId = dal.ExecuteSQLQueryData("select ownerid from tbluserrole where userid = @userid", ht).Rows[0]["ownerid"].ToString();

        if (type == "time")
        {
            if (zone != "")
                return GetDateByUser(zone, Convert.ToDateTime(datetime)).ToShortTimeString();
            else
                return atp.GetTime(datetime, AppId).ToString();
        }
        else if (type == "date")
        {
            string date = atp.GetDate(datetime, AppId).ToString();
            if (date == "Today" && zone != "")
                return GetDateByUser(zone, Convert.ToDateTime(datetime)).ToString("yyyy/MM/dd");
            else
                return date;
        }
        else
            return AppId;

    }

    public System.DateTime GetDateByUser(string zone, System.DateTime recorddate)
    {
        string[] values = zone.Split('#');

        DateTime utcTime = recorddate;
        TimeSpan utcOffset = GetHourDiff(values[0]);

        DateTime resultDateTime = utcTime.Add(utcOffset);

        if (values[1] == "1")
            resultDateTime = resultDateTime.AddMinutes(double.Parse(values[2]));
        else if (values[1] == "2")
            resultDateTime = resultDateTime.AddMinutes(((-1) * (double.Parse(values[2]))));

        return resultDateTime;
    }

    public static TimeSpan GetHourDiff(string zone)
    {
        TimeSpan tspan = new TimeSpan(0, 0, 0, 0);
        switch (zone)
        {
            case "(UTC-12.00) Eniwetok, Kwajalein":
                tspan = new TimeSpan(0, -12, 0, 0);
                break;
            case "(UTC-11.00) Midway Island, Samoa":
                tspan = new TimeSpan(0, -11, 0, 0);
                break;
            case "(UTC-10.00) Hawaii":
                tspan = new TimeSpan(0, -10, 0, 0);
                break;
            case "(UTC-09.00) Alaska":
                tspan = new TimeSpan(0, -9, 0, 0);
                break;
            case "(UTC-08.00) Pacific Time (US & Canada)":
                tspan = new TimeSpan(0, -8, 0, 0);
                break;
            case "(UTC-07.00) Mountain Time (US & Canada)":
                tspan = new TimeSpan(0, -7, 0, 0);
                break;
            case "(UTC-06.00) Central Time (US & Canada), Mexico City":
                tspan = new TimeSpan(0, -6, 0, 0);
                break;
            case "(UTC-05.00) Eastern Time (US & Canada), Bogota, Lima, Quito":
                tspan = new TimeSpan(0, -5, 0, 0);
                break;
            case "(UTC-04.00) Atlantic Time (Canada), Caracas, La Paz":
                tspan = new TimeSpan(0, -4, 0, 0);
                break;
            case "(UTC-03.50) Newfoundland":
                tspan = new TimeSpan(0, -3, -30, 0);
                break;
            case "(UTC-03.00) Brazil, Buenos Aires, Georgetown":
                tspan = new TimeSpan(0, -3, 0, 0);
                break;
            case "(UTC-02.00) Mid-Atlantic":
                tspan = new TimeSpan(0, -2, 0, 0);
                break;
            case "(UTC-01.00) Azores, Cape Verde Islands":
                tspan = new TimeSpan(0, -1, 0, 0);
                break;
            case "(UTC+00.00) Western Europe Time, London, Lisbon, Casablanca, Monrovia":
                tspan = new TimeSpan(0, 0, 0, 0);
                break;
            case "(UTC+01.00) CET(Central Europe Time), Brussels, Copenhagen, Madrid, Paris":
                tspan = new TimeSpan(0, 1, 0, 0);
                break;
            case "(UTC+02.00) EET(Eastern Europe Time), Kaliningrad, South Africa":
                tspan = new TimeSpan(0, 2, 0, 0);
                break;
            case "(UTC+03.00) Baghdad, Kuwait, Riyadh, Moscow, St. Petersburg, Volgograd, Nairobi":
                tspan = new TimeSpan(0, 3, 0, 0);
                break;
            case "(UTC+03.50) Tehran":
                tspan = new TimeSpan(0, 3, 30, 0);
                break;
            case "(UTC+04.00) Abu Dhabi, Muscat, Baku, Tbilisi":
                tspan = new TimeSpan(0, 4, 0, 0);
                break;
            case "(UTC+04.50) Kabul":
                tspan = new TimeSpan(0, 4, 30, 0);
                break;
            case "(UTC+05.00) Ekaterinburg, Islamabad, Karachi, Tashkent":
                tspan = new TimeSpan(0, 5, 0, 0);
                break;
            case "(UTC+05.50) Bombay, Calcutta, Madras, New Delhi":
                tspan = new TimeSpan(0, 5, 30, 0);
                break;
            case "(UTC+06.00) Almaty, Dhaka, Colombo":
                tspan = new TimeSpan(0, 6, 0, 0);
                break;
            case "(UTC+07.00) Bangkok, Hanoi, Jakarta":
                tspan = new TimeSpan(0, 7, 0, 0);
                break;
            case "(UTC+08.00) Beijing, Perth, Singapore, Hong Kong, Chongqing, Urumqi, Taipei":
                tspan = new TimeSpan(0, 8, 0, 0);
                break;
            case "(UTC+09.00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk":
                tspan = new TimeSpan(0, 9, 0, 0);
                break;
            case "(UTC+09.50) Adelaide, Darwin":
                tspan = new TimeSpan(0, 9, 30, 0);
                break;
            case "(UTC+10.00) EAST(East Australian Standard), Guam, Papua New Guinea, Vladivostok":
                tspan = new TimeSpan(0, 10, 0, 0);
                break;
            case "(UTC+11.00) Magadan, Solomon Islands, New Caledonia":
                tspan = new TimeSpan(0, 11, 0, 0);
                break;
            case "(UTC+12.00) Auckland, Wellington, Fiji, Kamchatka, Marshall Island":
                tspan = new TimeSpan(0, 12, 0, 0);
                break;
            case "(UTC+13.00) Nuku'alofa":
                tspan = new TimeSpan(0, 13, 0, 0);
                break;
        };

        return tspan;
    }

    public string ConvertTimeSpan(TimeSpan WorkedHours)
    {
        string strWorkingHours = String.Empty;
        if (WorkedHours.Days > 0)
        {
            strWorkingHours = WorkedHours.Days + " Days";
        }
        if (WorkedHours.Hours > 0)
        {
            strWorkingHours += " " + WorkedHours.Hours + " Hours";
        }
        if (WorkedHours.Minutes > 0)
        {
            strWorkingHours += " " + WorkedHours.Minutes + " Minutes";
        }

        return strWorkingHours;
    }

    public string ConvertTimeSpanInHours(TimeSpan WorkedHours)
    {
        string strWorkingHours = String.Empty;

        if (WorkedHours.Days > 0)
        {
            strWorkingHours = ((WorkedHours.Days * 24) + WorkedHours.Hours).ToString() + " Hrs";
        }
        else
        {
            strWorkingHours = WorkedHours.Hours.ToString() + " Hrs";
        }
        if (WorkedHours.Minutes > 0)
        {
            strWorkingHours += " " + WorkedHours.Minutes + " Mins";
        }

        return strWorkingHours;
    }

    public bool SendAbsentNotification(string userid, DateTime CrtDate, string Comment, string type)
    {
        try
        {
            Hashtable ht = new Hashtable();
            ht["@userid"] = userid;
            DataTable dt = dal.GetDataTable("sp_GetUserAccess", ht);
            DataRow[] sender = dt.Select("ID=" + userid);

            AvaimaEmailAPI email = new AvaimaEmailAPI();
            StringBuilder subject = new StringBuilder();
            StringBuilder body = new StringBuilder();
            string subjectTxt = "";
            string bodyTxt = "";

            if (type == "insert")
                subjectTxt = "Absence Added by ";
            else if (type == "update")
                subjectTxt = "Absence Updated by ";
            else if (type == "delete")
                subjectTxt = "Absence Deleted by ";

            subjectTxt = subjectTxt + sender[0]["Title"].ToString();
            bodyTxt = subjectTxt + ". <br/>";
            bodyTxt += "<b>Day: </b>" + CrtDate.ToShortDateString() + "<br/>";
            bodyTxt += "<b>Reason: </b>" + Comment;

            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
            subject.Append(subjectTxt);
            body.Append(bodyTxt);
            body.Append("<br><br>");
            //body.Append("<ul><li>Go to <a href='http://www.avaima.com/signin'>www.avaima.com/signin</a> and login to your account</li>");
            //body.Append("<li>Find and click \"Time & Attendance\" in the left menu</li>");
            //body.Append("<li>\"Clock-In\" when you start your day at work and \"Clock-Out\" when you are done</li>");
            //body.Append("</ul>");
            //body.Append("<br/>For further questions, comments or help contact support@avaima.com.");
            body.Append(Helper.AvaimaEmailSignature);
            body.Append("</div>");

            List<string> emails = new List<string>();

            //Filter users to check if Email Notifications are enabled
            dt = EmailNotifications_Enabled(dt, Notification_Type.Absent.ToInt32());

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ht = new Hashtable();
                ht["@userid"] = dt.Rows[i]["ID"].ToString();
                DataTable settings = dal.ExecuteSQLQueryData("Select * from ReportSettings where userid = @userid", ht);

                if (dt.Rows[i]["ID"].ToString() != userid)
                {
                    if (dt.Rows[i]["sendemail"].ToInt32() != -1) //If user has disabled email notifications
                        emails.Add(dt.Rows[i]["email"].ToString());

                    if (settings.Rows.Count > 0)
                    {
                        string[] ed = DataList(settings);
                        foreach (var val in ed)
                            emails.Add(val);
                    }
                }
            }

            foreach (var emailaddress in emails)
            {
                email.send_email(emailaddress, "Time & Attendance", "", body.ToString(), subject.ToString());
            }

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }


    public bool SendTimeEditNotification(string userid, Hashtable data)
    {
        try
        {
            Hashtable ht = new Hashtable();
            ht["@userid"] = userid;
            DataTable dt = dal.GetDataTable("sp_GetUserAccess", ht);
            DataRow[] sender = dt.Select("ID=" + userid);

            AvaimaEmailAPI email = new AvaimaEmailAPI();
            StringBuilder subject = new StringBuilder();
            StringBuilder body = new StringBuilder();
            string subjectTxt = "";
            string bodyTxt = "";
            string number = "";
            int count = Get_TimeinoutCount(0, data["rowid"].ToString(), DateTime.Now.ToString());
            count = (count == 0) ? 1 : count + 1;
            number = AddOrdinal(count);

            bodyTxt = "<b>" + sender[0]["Title"].ToString() + "</b> edited Clock-in/out time. <br/><br/>";
            bodyTxt += "<b>Date: " + Convert.ToDateTime(data["day"]).ToString("dddd, MMMM dd ,yyyy") + "</b><br/>";

            //bodyTxt += "<table style='width:400px; border-collapse: collapse; border: 1px solid #777;'><thead><tr><td></td><td>Old</td><td>New</td></tr><thead/>";
            //bodyTxt += "<tbody><tr><td>From</td><td>" + data["oldfrmTime"].ToString() + "</td><td class='change'>" + data["newfrmTime"].ToString() + "</td></tr>";
            //bodyTxt += "<tbody><tr><td>To</td><td>" + data["oldtoTime"].ToString() + "</td><td class='change'>" + data["newtoTime"].ToString() + "</td></tr>";
            //bodyTxt += "</tbody></table><br/>";

            bodyTxt += "<table style='width:400px; border-collapse: collapse; border: 1px solid #777;'><thead><tr><th style='border: 1px solid #999;padding:3px 5px; color:#000;text-align: left;'></th><th style='border: 1px solid #999;padding:3px 5px; color:#000;text-align: left;'>Clock In</th><th style='border: 1px solid #999;padding:3px 5px; color:#000;text-align: left;'>Clock Out</th></tr><thead/>";
            bodyTxt += "<tbody><tr><td style='border: 1px solid #999;padding:3px 5px; color:#000;text-align: left;font-weight:bold;'>Old</td><td style='border: 1px solid #999;padding:3px 5px; color:#000;text-align: left;'>" + Convert.ToDateTime(data["oldfrmTime"].ToString()).ToShortTimeString() + "</td><td style='border: 1px solid #999;padding:3px 5px; color:#000;text-align: left;'>";
            if ((!string.IsNullOrWhiteSpace(data["oldtoTime"].ToString())) && (!string.IsNullOrEmpty(data["oldtoTime"].ToString())))
                bodyTxt += Convert.ToDateTime(data["oldtoTime"].ToString()).ToShortTimeString();
            else
                bodyTxt += "Still Clocked-in";

            bodyTxt += "</td></tr>";
            bodyTxt += "<tbody><tr><td style='border: 1px solid #999;padding:3px 5px; color:#000;text-align: left;font-weight:bold;'>Edited</td><td style='border: 1px solid #999;padding:3px 5px; color:red !important;text-align: left;'>" + Convert.ToDateTime(data["newfrmTime"].ToString()).ToShortTimeString() + "</td><td style='border: 1px solid #999;padding:3px 5px; color:red !important;text-align: left;'>";
            if ((!string.IsNullOrWhiteSpace(data["newtoTime"].ToString())) && (!string.IsNullOrEmpty(data["newtoTime"].ToString())))
                bodyTxt += Convert.ToDateTime(data["newtoTime"].ToString()).ToShortTimeString();
            else
                bodyTxt += "Still Clocked-in";

            bodyTxt += "</td></tr>";
            bodyTxt += "</tbody></table><br/>";

            TimeSpan diff = new TimeSpan();
            string indicator = "";
            string suffix = "";

            if ((!string.IsNullOrEmpty(data["oldtoTime"].ToString())) && (!string.IsNullOrWhiteSpace(data["oldtoTime"].ToString())))
            {
                diff = (Convert.ToDateTime(data["oldtoTime"]).Subtract(Convert.ToDateTime(data["oldfrmTime"]))).Subtract((Convert.ToDateTime(data["newtoTime"]).Subtract(Convert.ToDateTime(data["newfrmTime"]))));
                TimeSpan olddiff = Convert.ToDateTime(data["oldtoTime"]).Subtract(Convert.ToDateTime(data["oldfrmTime"]));
                TimeSpan newdiff = Convert.ToDateTime(data["newtoTime"]).Subtract(Convert.ToDateTime(data["newfrmTime"]));
                if (newdiff > olddiff)
                { indicator = " (+)"; suffix = "Added"; }
                else if (newdiff < olddiff)
                { indicator = " (-)"; suffix = "Subtracted"; }
            }
            else
            {
                diff = (Convert.ToDateTime(data["oldfrmTime"]).Subtract(Convert.ToDateTime(data["newfrmTime"])));
                if (diff.CompareTo(TimeSpan.Zero) > 0)
                { indicator = " (+)"; suffix = "Added"; }
                else if (diff.CompareTo(TimeSpan.Zero) < 0)
                { indicator = " (-)"; suffix = "Subtracted"; }
            }

            if (diff.CompareTo(TimeSpan.Zero) < 0 || diff.CompareTo(TimeSpan.Zero) > 0)
            {
                bodyTxt += "<b>Difference: </b>" + ((Math.Abs(diff.Hours) != 0) ? Math.Abs(diff.Hours) + " Hours " : " ") + ((Math.Abs(diff.Minutes) != 0) ? Math.Abs(diff.Minutes) + " Minutes" : "") + " " + suffix + "<br/>";
                //indicator = " (+)";
            }
            //else if (diff.CompareTo(TimeSpan.Zero) > 0)
            //{
            //    bodyTxt += "<b>Difference: </b>" + ((Math.Abs(diff.Hours) != 0) ? Math.Abs(diff.Hours) + " Hours " : " ") + ((Math.Abs(diff.Minutes) != 0) ? Math.Abs(diff.Minutes) + " Minutes" : "") + " Subtracted<br/>";
            //    //indicator = " (-)";
            //}
            else
                bodyTxt += "<b>Difference: </b>0<br/>";

            subjectTxt = number + " Time Edit" + indicator + " by " + sender[0]["Title"].ToString();

            bodyTxt += "<b>Reason: </b>" + data["comment"].ToString();
            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
            subject.Append(subjectTxt);
            body.Append(bodyTxt);
            body.Append("<br/><br/>");
            body.Append(Helper.AvaimaEmailSignature);
            body.Append("</div>");

            List<string> emails = new List<string>();

            //Filter users to check if Email Notifications are enabled
            dt = EmailNotifications_Enabled(dt, Notification_Type.TimeEdit.ToInt32());

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //if (dt.Rows[i]["ID"].ToString() != userid)
                //{
                ht = new Hashtable();
                ht["@userid"] = dt.Rows[i]["ID"].ToString();
                DataTable settings = dal.ExecuteSQLQueryData("Select * from ReportSettings where userid = @userid", ht);

                if (dt.Rows[i]["sendemail"].ToInt32() != -1) //If user has disabled email notifications
                    emails.Add(dt.Rows[i]["email"].ToString());

                if (settings.Rows.Count > 0)
                {
                    string[] ed = DataList(settings);
                    foreach (var val in ed)
                        emails.Add(val);
                }
                //}
            }

            foreach (var emailaddress in emails)
            {
                email.send_email(emailaddress, "Time & Attendance", "", body.ToString(), subject.ToString());
            }

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public bool SendDeleteNotification(string userid, Hashtable data)
    {
        try
        {
            Hashtable ht = new Hashtable();
            ht["@userid"] = userid;
            DataTable dt = dal.GetDataTable("sp_GetUserAccess", ht);
            DataRow[] sender = dt.Select("ID=" + userid);

            AvaimaEmailAPI email = new AvaimaEmailAPI();
            StringBuilder subject = new StringBuilder();
            StringBuilder body = new StringBuilder();
            string subjectTxt = "";
            string bodyTxt = "";

            subjectTxt = "Time Deleted by " + sender[0]["Title"].ToString();
            bodyTxt = "<b>" + sender[0]["Title"].ToString() + "</b>, deleted time from recorded history. <br/>";
            bodyTxt += "<b>Date: " + Convert.ToDateTime(data["day"]).ToString("dddd, MMMM dd ,yyyy") + "</b><br/>";
            bodyTxt += "<b>Clock-in Time:</b> " + Convert.ToDateTime(data["frmTime"].ToString()).ToShortTimeString() + "<br/>";
            bodyTxt += "<b>Clock-out Time:</b> " + Convert.ToDateTime(data["toTime"].ToString()).ToShortTimeString() + "<br/>";
            bodyTxt += "<b>Reason: </b>" + data["comment"].ToString();
            body.Append("<style>.email-tem-attendance{width:400px; border-collapse: collapse; border: 1px solid #777;}.email-tem-attendance th, .email-tem-attendance td {border: 1px solid #999;padding:3px 5px; color:#000;text-align: left;}.email-tem-attendance .new-update{color:red !important;}</style>");
            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
            subject.Append(subjectTxt);
            body.Append(bodyTxt);
            body.Append("<br/><br/>");
            body.Append(Helper.AvaimaEmailSignature);
            body.Append("</div>");

            List<string> emails = new List<string>();

            //Filter users to check if Email Notifications are enabled
            dt = EmailNotifications_Enabled(dt, Notification_Type.TimeDelete.ToInt32());

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //if (dt.Rows[i]["ID"].ToString() != userid)
                //{
                ht = new Hashtable();
                ht["@userid"] = dt.Rows[i]["ID"].ToString();
                DataTable settings = dal.ExecuteSQLQueryData("Select * from ReportSettings where userid = @userid", ht);

                if (dt.Rows[i]["sendemail"].ToInt32() != -1) //If user has disabled email notifications
                    emails.Add(dt.Rows[i]["email"].ToString());

                if (settings.Rows.Count > 0)
                {
                    string[] ed = DataList(settings);
                    foreach (var val in ed)
                        emails.Add(val);
                }
                //}
            }

            foreach (var emailaddress in emails)
            {
                email.send_email(emailaddress, "Time & Attendance", "", body.ToString(), subject.ToString());
            }

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public string[] DataList(DataTable dt)
    {
        string[] list = dt.Rows[0]["AdditionalEmails"].ToString().Split(',');
        list = list.Where(s => !string.IsNullOrEmpty(s)).ToArray();
        return list;
    }

    /// <summary>
    /// Calculate user statistics and generates a yearly report 
    /// </summary>
    /// <param name="HoursPerDay"># of hours a user supposed to work</param>
    /// <param name="year"></param>
    /// <param name="userid"></param>
    /// <param name="instanceid"></param>
    /// <returns></returns>

    [WebMethod]
    public string CalculateStatistics(int ParentId, int year, int month, int userid, string instanceid, string report_type)
    {
        Hashtable ht = new Hashtable();
        DateTime startDate = new DateTime(year, 1, 1, 0, 0, 0);
        DateTime endDate = new DateTime();

        Hashtable userdetail = new Hashtable();
        //userdetail["@id"] = 0;
        //userdetail["@instanceid"] = instanceid;
        userdetail["@userid"] = userid;
        DataTable user_joindate = dal.ExecuteSQLQueryData("Select top 1 * from schema_6e815b00_6e13_4839_a57d_800a92809f21.workertrans where p_pid=@userid", userdetail);
        int UnmarkedDays = 0;

        if (startDate < DateTime.Now)
        {

            if (report_type == "monthly")
            {
                startDate = new DateTime(year, month, 1);
                endDate = startDate.AddMonths(1).AddDays(-1);
            }
            else if (report_type == "yearly")
            {
                endDate = new DateTime(year, 12, DateTime.DaysInMonth(startDate.Year, startDate.Month), 23, 59, 59);
                if (endDate > DateTime.Now)
                {
                    endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
                }
            }

            DataTable dtAbsences = GetYearlyAbsences(userid, startDate, endDate);
            DataTable dtAttendance = GetYearlyAttendance(userid, startDate, endDate);
            DataTable uniqueCols = dtAttendance.DefaultView.ToTable(true, "FormatedDate");

            string workedhours = WorkedHour(dtAttendance);

            int totalWeekends = TotalWeekendDays(ParentId, startDate, endDate);
            ht["TotalWorkedHours"] = workedhours;
            ht["TotalDaysWorked"] = uniqueCols.Rows.Count.ToString();
            ht["TotalAbsences"] = dtAbsences.Rows.Count.ToString();
            ht["TotalWeekends"] = totalWeekends.ToString();

            string days = "";
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(GetUserHolidaysWeekends(userid, "0", year));
            foreach (DataRow row in dt.Rows)
            {
                row["title"].ToString();
                days += row["title"].ToString() + ",";
            }

            ht["WeekendDays"] = days.TrimEnd(',');

            DataTable holidays = JsonConvert.DeserializeObject<DataTable>(GetUserHolidaysWeekends(ParentId, "1", year));
            string Filter = "date >='" + startDate.ToString("yyyy-MM-dd") + "' AND date <='" + endDate.ToString("yyyy-MM-dd") + "'";

            int totalHolidays = holidays.Select(Filter).Count();
            ht["TotalHolidays"] = totalHolidays.ToString();


            if (report_type == "monthly")
            {
                // Filter = "date >='" + startDate.ToString("yyyy-MM-dd") + "' AND date <='" + endDate.ToString("yyyy-MM-dd") + "'";
                //int totalHolidays = holidays.Select(Filter).Count();
                //ht["TotalHolidays"] = totalHolidays.ToString();

                //ht["MonthlyWorkingDays"] = (DateTime.DaysInMonth(year, month) - (totalHolidays + totalWeekends)).ToString();
                ht["MonthlyWorkingDays"] = YearlyWorkingDays(startDate, endDate, ParentId, year).ToString();
                //ht["UnmarkedDays"] = ht["MonthlyWorkingDays"].ToInt32() - (uniqueCols.Rows.Count + totalHolidays + totalWeekends);
                UnmarkedDays = ht["MonthlyWorkingDays"].ToInt32() - (uniqueCols.Rows.Count + dtAbsences.Rows.Count);
            }
            else if (report_type == "yearly")
            {
                startDate = Convert.ToDateTime(user_joindate.Rows[0]["lastin"]);

                //int totalHolidays = holidays.Select(Filter).Count();
                //ht["TotalHolidays"] = totalHolidays.ToString();
                int totaldays = 0;

                //for (int i = 1; i <= 12; i++)
                //{
                //    totaldays += DateTime.DaysInMonth(year, i);
                //}

                //var date1 = DateTime.ParseExact(startDate.ToString(), "MM-dd-yyyy", CultureInfo.InvariantCulture);
                //var date2 = DateTime.ParseExact(endDate.ToString(), "MM-dd-yyyy", CultureInfo.InvariantCulture);
                double diff = (endDate - startDate).TotalDays;


                //var date1 = Convert.ToDateTime(startDate.ToString("MM-dd-yyyy"));
                //var date2 = Convert.ToDateTime(endDate.ToString("MM-dd-yyyy"));
                //var lastdate = new DateTime(date1.Year, 12, 31);
                //double diff = (date2 - date1).TotalDays;

                //ht["YearlyWorkingDays"] = (YearlyWorkingDays(startDate, endDate, ParentId) - (totalHolidays + totalWeekends)).ToString();
                ht["YearlyWorkingDays"] = YearlyWorkingDays(startDate, endDate, ParentId, year).ToString();
                //ht["UnmarkedDays"] = ht["YearlyWorkingDays"].ToInt32() - (uniqueCols.Rows.Count + totalHolidays + totalWeekends);
                UnmarkedDays = ht["YearlyWorkingDays"].ToInt32() - (uniqueCols.Rows.Count + dtAbsences.Rows.Count);
            }
            ht["UnmarkedDays"] = UnmarkedDays > 0 ? UnmarkedDays : 0;
            ht["startDate"] = startDate;
            ht["endDate"] = endDate;
        }

        return JsonConvert.SerializeObject(ht);
    }

    public int TotalWeekendDays(int userid, DateTime start, DateTime end)
    {
        int totalweekendDays = 0;
        DataTable dt = JsonConvert.DeserializeObject<DataTable>(GetUserHolidaysWeekends(userid, "0"));

        foreach (DataRow row in dt.Rows)
        {
            DayOfWeek day = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), row["title"].ToString());
            start = start.Date.AddDays((7 + day - start.DayOfWeek) % 7);
            if (end < start)
                return 0;
            else
                totalweekendDays = totalweekendDays + ((int)(end - start).TotalDays) / 7 + 1;
        }

        return totalweekendDays;
    }

    [WebMethod]
    public static string DetailReport(int userid, DateTime startDate, DateTime endDate, string Appid)
    {
        Attendance atd = new Attendance();
        DataTable dtAttendance = atd.GetYearlyAttendance(userid, startDate, endDate);
        AvaimaTimeZoneAPI atp = new AvaimaTimeZoneAPI();
        string lastin;
        string lastout;

        string historyTable = "<h5>Detailed Report: " + startDate.ToString("dd MMM yyyy") + " to " + endDate.ToString("dd MMM yyyy") + "</h5>";
        historyTable += "<table class='table table-bordered table-hover'>";
        historyTable += "<thead><th>Day</th><th>Clock-in</th><th>Clock-out</th><th>Time Spent</th></thead><tbody>";

        if (dtAttendance.Rows.Count > 0)
        {
            foreach (DataRow row in dtAttendance.Rows)
            {
                lastin = atp.GetTime(row["lastin"].ToString(), Appid);
                lastout = atp.GetTime(row["lastout"].ToString(), Appid);

                historyTable += "<tr><td>" + Convert.ToDateTime(row["FormatedDate"]).ToString("dd-MMM-yyyy") + "</td><td>" + Convert.ToDateTime(lastin).ToShortTimeString() + "</td><td>" + Convert.ToDateTime(lastout).ToShortTimeString() + "</td><td>" + row["hours"] + "</td></tr>";
            }
        }

        historyTable += "</tbody></table>";

        return historyTable;

    }

    //BACKUP till 18th May 2018
    //public string AdvanceCalculateStatistics(int ParentId, int HoursPerDay, int year, int month, int userid, string instanceid, decimal breakHr, string startHour, string endHour, string Appid, string report_type, string additionalbreak, decimal additionalbreakHr, string customstartDate = "", string customendDate = "")
    //{
    //    Hashtable ht = new Hashtable();
    //    int monthI = DateTime.ParseExact("January", "MMMM", CultureInfo.InvariantCulture).Month;
    //    DateTime startDate = new DateTime(year, 1, 1, 0, 0, 0);
    //    DateTime endDate = new DateTime();

    //    Hashtable userdetail = new Hashtable();
    //    userdetail["@userid"] = userid;
    //    DataTable user_joindate = dal.ExecuteSQLQueryData("Select top 1 * from schema_6e815b00_6e13_4839_a57d_800a92809f21.workertrans where p_pid=@userid", userdetail);

    //    if (startDate < DateTime.Now)
    //    {
    //        if (report_type == "monthly")
    //        {
    //            startDate = new DateTime(year, month, 1);

    //            if (startDate.Month == DateTime.Now.Month)
    //                endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
    //            else
    //                endDate = startDate.AddMonths(1).AddDays(-1);
    //        }
    //        else if (report_type == "yearly")
    //        {
    //            if (user_joindate.Rows.Count > 0)
    //                startDate = Convert.ToDateTime(user_joindate.Rows[0]["lastin"]);

    //            endDate = new DateTime(year, 12, DateTime.DaysInMonth(startDate.Year, startDate.Month), 23, 59, 59);
    //            if (endDate > DateTime.Now)
    //            {
    //                endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
    //            }
    //        }
    //        else if (report_type == "custom")
    //        {
    //            startDate = Convert.ToDateTime(customstartDate);
    //            endDate = Convert.ToDateTime(customendDate);
    //        }

    //        if (additionalbreakHr > 0)
    //        {
    //            if (additionalbreak == "week")
    //            {
    //                TimeSpan ts = endDate.Subtract(startDate);
    //                int dateDiff = ts.Days;
    //                int totalWeeks = (int)dateDiff / 7;
    //                additionalbreakHr = additionalbreakHr * totalWeeks;
    //            }
    //            else
    //            {
    //                int monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
    //                additionalbreakHr = Math.Abs(monthsApart) * additionalbreakHr;
    //            }
    //        }

    //        DataTable dtAbsences = GetYearlyAbsences(userid, startDate, endDate);
    //        DataTable dtAttendance = GetYearlyAttendance(userid, startDate, endDate);
    //        DataTable uniqueCols = dtAttendance.DefaultView.ToTable(true, "FormatedDate");

    //        TimeSpan Whrs = AdvanceWorkedHour(dtAttendance, Appid, startHour, endHour);
    //        decimal workedhours = (Math.Round(Convert.ToDecimal(Whrs.TotalMinutes / 60), 2));
    //        ht["TotalWorkedHours"] = workedhours;
    //        ht["TotalDaysWorked"] = dtAttendance.Rows.Count.ToString();
    //        ht["TotalAbsences"] = dtAbsences.Rows.Count.ToString();
    //        int yearlyworkingdays = YearlyWorkingDays(startDate, endDate, ParentId);
    //        int yearlyworkinghours = yearlyworkingdays * HoursPerDay;
    //        decimal yearlybreakhours = Math.Round(yearlyworkingdays * breakHr) + additionalbreakHr;
    //        decimal yearlybreakdays = Math.Round(yearlybreakhours / 24);
    //        decimal totalbreakhours = Math.Round(dtAttendance.DefaultView.ToTable(true, "FormatedDate").Rows.Count * breakHr) + additionalbreakHr;
    //        decimal totalbreakdays = Math.Round(totalbreakhours / 24);

    //        ht["WorkingDays"] = yearlyworkingdays;
    //        ht["WorkingHours"] = yearlyworkinghours;
    //        ht["WorkingDaysB"] = Math.Abs(yearlyworkingdays - yearlybreakdays);
    //        ht["WorkingHoursB"] = Math.Abs(yearlyworkinghours - yearlybreakhours);

    //        //int UnmarkedDays = yearlyworkingdays - (uniqueCols.Rows.Count + dtAbsences.Rows.Count);
    //        //int UnmarkedDays = yearlyworkingdays - (dtAttendance.Rows.Count + dtAbsences.Rows.Count);
    //        DataTable dtHistory = new DataTable();
    //        dtHistory.Columns.Add("DateTime", typeof(DateTime));
    //        dtHistory.Columns.Add("Day");
    //        dtHistory.Columns.Add("Clock-in");
    //        dtHistory.Columns.Add("Clock-out");
    //        dtHistory.Columns.Add("Time Spent");
    //        dtHistory.Merge(FormatDataTable(dtAttendance, "attendance"));
    //        dtHistory.Merge(FormatDataTable(dtAbsences, "absence", Appid));


    //        string unmarked_dates = GetUnmarkedDays(dtHistory, startDate, endDate, ParentId);
    //        if (unmarked_dates != "")
    //            ht["UnmarkedDays"] = unmarked_dates.Split(',').Count();
    //        else
    //            ht["UnmarkedDays"] = 0;

    //        ht["Absences"] = dtAbsences.Rows.Count;

    //        //if (workedhours != 0)
    //        //{

    //        //ht["TotalWorkedHoursB"] = Math.Round(workedhours - totalbreakhours);
    //        ht["TotalWorkedHoursB"] = UtilityMethods.getFormatedTimeByMinutes(TimeSpan.FromHours(Convert.ToDouble(Math.Round(workedhours - totalbreakhours))).TotalMinutes.ToInt32());
    //        ht["TotalDaysWorkedB"] = Math.Round(dtAttendance.Rows.Count - totalbreakdays);

    //        //int _workedhours = ht["TotalWorkedHoursB"].ToInt32();
    //        decimal _workedhours = Math.Round(workedhours - totalbreakhours);
    //        //int _yearlyworkinghours = ht["WorkingHours"].ToInt32();
    //        int _yearlyworkinghours = ht["WorkingHoursB"].ToInt32();

    //        //test

    //        if (instanceid == "f425f912-8d79-4415-ba72-89a5309acb10" || userid == 10192)
    //        {
    //            decimal ut = GetExtratime(userid, startDate, endDate);
    //            ht["UnderTime"] = UtilityMethods.getFormatedTimeByMinutes(TimeSpan.FromHours(Convert.ToDouble(ut)).TotalMinutes.ToInt32());

    //            _workedhours = _workedhours - ut;
    //        }

    //        if (_workedhours > _yearlyworkinghours)
    //        {
    //            ht["ExtraHours"] = UtilityMethods.getFormatedTimeByMinutes(TimeSpan.FromHours(Convert.ToDouble(Math.Abs((_workedhours - _yearlyworkinghours)))).TotalMinutes.ToInt32());
    //            ht["UnderHours"] = 0;
    //        }
    //        else
    //        {
    //            ht["ExtraHours"] = 0;
    //            ht["UnderHours"] = UtilityMethods.getFormatedTimeByMinutes(TimeSpan.FromHours(Convert.ToDouble(Math.Abs((_workedhours - _yearlyworkinghours)))).TotalMinutes.ToInt32());

    //        }
    //        //}
    //        //else
    //        //{
    //        //    ht["ExtraHours"] = 0;
    //        //    ht["UnderHours"] = 0;
    //        //    ht["TotalWorkedHoursB"] = 0;
    //        //    ht["TotalDaysWorkedB"] = 0;
    //        //}

    //        ht["startDate"] = startDate;
    //        ht["endDate"] = endDate;
    //        ht["userid"] = userid;

    //        // ht["DetailReport"] = DetailReport(userid, startDate, endDate, Appid);
    //    }

    //    return JsonConvert.SerializeObject(ht);
    //}

    public string AdvanceCalculateStatistics(int ParentId, int HoursPerDay, int year, int month, int userid, string instanceid, int absences, decimal breakHr, string startHour, string endHour, string Appid, string report_type, string additionalbreak, decimal additionalbreakHr, string customstartDate = "", string customendDate = "")
    {
        Hashtable ht = new Hashtable();
        int monthI = DateTime.ParseExact("January", "MMMM", CultureInfo.InvariantCulture).Month;
        DateTime startDate = new DateTime(year, 1, 1, 0, 0, 0);
        DateTime endDate = new DateTime();
        Hashtable userdetail = new Hashtable();
        userdetail["@userid"] = userid;
        //DataTable user_joindate = dal.ExecuteSQLQueryData("Select top 1 * from schema_6e815b00_6e13_4839_a57d_800a92809f21.workertrans where p_pid=@userid", userdetail);
        DateTime user_joindate = SP.GetWorkerFirstSignInThisYear(userid.ToString(), year);

        DateTime currenttime = Convert.ToDateTime(App.GetTime(DateTime.UtcNow.TimeOfDay.ToString(), Appid));
        //DateTime currenttime = Convert.ToDateTime("10/14/2018 09:45 PM"); //TEST

        string WorkingHours = String.Empty;

        if (startDate < currenttime)
        {
            if (report_type == "monthly")
            {
                startDate = new DateTime(year, month, 1);

                if (user_joindate > startDate && (startDate.Month == user_joindate.Month))
                {
                    startDate = user_joindate;
                }

                if (startDate.Month == currenttime.Month)
                {
                    if (startDate.Year == currenttime.Year)
                        endDate = new DateTime(currenttime.Year, currenttime.Month, currenttime.Day, 23, 59, 59);
                    else
                        endDate = new DateTime(startDate.Year, startDate.Month, DateTime.DaysInMonth(startDate.Year, startDate.Month), 23, 59, 59);
                }
                else
                {
                    if (user_joindate > startDate && (startDate.Month == user_joindate.Month))
                    {
                        endDate = new DateTime(currenttime.Year, startDate.Month, DateTime.DaysInMonth(year, user_joindate.Month), 23, 59, 59);
                    }
                    else
                        //endDate = startDate.AddMonths(1).AddDays(-1);
                        endDate = new DateTime(startDate.Year, startDate.Month, DateTime.DaysInMonth(year, startDate.Month), 23, 59, 59);

                }
                //endDate = startDate.AddMonths(1).AddDays(-1);
                //endDate = new DateTime(currenttime.Year, startDate.Month, DateTime.DaysInMonth(year, user_joindate.Month), 23, 59, 59);
            }
            else if (report_type == "yearly")
            {
                startDate = SP.GetWorkerFirstSignInThisYear(userid.ToString(), year);
                endDate = SP.GetWorkerLastSignInThisYear(userid.ToString(), year);
            }
            else if (report_type == "custom")
            {
                if (Convert.ToDateTime(customstartDate) < user_joindate)
                    startDate = user_joindate;
                else
                    startDate = Convert.ToDateTime(customstartDate);

                endDate = Convert.ToDateTime(customendDate);
            }


            //endDate = Convert.ToDateTime("10/15/2018 02:45 AM");  //Test

            DateTime user_endDate = currenttime;
            Hashtable data = new Hashtable();
            data["@userid"] = userid;
            data["@lastin"] = endDate.ToShortDateString();
            DataTable dt = dal.ExecuteSQLQueryData("SELECT TOP(1) * FROM workertrans WHERE P_PID = @userid AND (Convert(date, lastin) = @lastin) ORDER BY lastin desc", data);
            if (dt.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(dt.Rows[0]["lastout"].ToString()))
                {
                    if (endDate.Date != currenttime.Date)
                        user_endDate = endDate;
                    else
                        user_endDate = endDate.AddDays(-1);
                }
                else
                {
                    user_endDate = endDate.AddDays(-1);
                }
            }
            else
                user_endDate = endDate.AddDays(-1);


            ht["info"] = "Until " + user_endDate.ToString("ddd, MMM dd yyyy");
            if (endDate.ToShortDateString() == currenttime.ToShortDateString())
                ht["info"] += " - today's time not included";

            if (startDate.ToShortDateString() == endDate.ToShortDateString())
                ht["info"] = "Statistics are calculated until previous day, current day is not included.";

            //DataTable dtAttendance = GetYearlyAttendance(userid, startDate, endDate); OLD
            DataTable dtAttendance = GetYearlyAttendance(userid, startDate, user_endDate);

            TimeSpan minutessss = new TimeSpan();

            DateTime abDate = new DateTime(year, 1, 1, 0, 0, 0); DataTable dtAbsences; DataTable dtAllAbsences = new DataTable();
            if (report_type == "yearly")
            {
                if (instanceid == "ae5eb924-def7-461e-8097-c503ec893bfc")
                    dtAllAbsences = GetAllYearlyAbsences(userid, abDate, endDate);

                dtAbsences = GetYearlyAbsences(userid, abDate, endDate);
            }
            else
            {
                if (instanceid == "ae5eb924-def7-461e-8097-c503ec893bfc")
                    dtAllAbsences = GetAllYearlyAbsences(userid, startDate, endDate);

                dtAbsences = GetYearlyAbsences(userid, startDate, endDate);
            }

            int TotalAbsences = dtAbsences.Rows.Count;
            ht["Absences"] = TotalAbsences;


            //Adjustments Start
            DataTable dtAdjustments;
            if (report_type == "yearly")
                dtAdjustments = GetYearlyAdjustments(userid, abDate, endDate);
            else
                dtAdjustments = GetYearlyAdjustments(userid, startDate, endDate);

            int TotalAdjustments = dtAdjustments.Rows.Count;
            ht["Adjustments"] = TotalAdjustments;
            //End


            //int yearlyworkingdays = 0;

            if (dtAttendance.Rows.Count > 0)
            {
                minutessss = GetTotalWorkedTime(dtAttendance, ParentId, Appid, instanceid, year);

                //ht["hd"] = minutessss.Days + "_" + minutessss.Hours + "_" + minutessss.Minutes + "_" + minutessss.TotalHours + "_" + minutessss;

                //if (TotalAbsences <= absences)
                //    yearlyworkingdays = YearlyWorkingDays(startDate, endDate, ParentId) - GetYearlyAbsences(userid, startDate, endDate).Rows.Count;
                //else
                //    yearlyworkingdays = YearlyWorkingDays(startDate, endDate, ParentId) - absences;

                //int yearlyworkinghours = yearlyworkingdays * HoursPerDay;
                //int yearlyworkinghours = WorkingHoursShouldBe(startDate, endDate, ParentId, userid, absences, instanceid, HoursPerDay);   OLD

                //Night Shift Instance 
                DataTable exclusive_settings = new DataTable();
                Hashtable ex = new Hashtable();
                ex["@userid"] = ParentId;

                if (instanceid == "ae5eb924-def7-461e-8097-c503ec893bfc")
                {
                    exclusive_settings = dal.ExecuteSQLQueryData("Select * from schema_6e815b00_6e13_4839_a57d_800a92809f21.ReportSettings where userid = @userid", ex);

                    if (exclusive_settings.Rows.Count > 0)
                        HoursPerDay = Convert.ToInt32(exclusive_settings.Rows[0]["working_hours"].ToString().Split(',')[0]);
                }

                int yearlyworkinghours = WorkingHoursShouldBe(startDate, user_endDate, ParentId, userid, year, absences, instanceid, HoursPerDay);
                //string ss= WorkingHoursShouldBe(startDate, user_endDate, ParentId, userid, year, absences, instanceid, HoursPerDay);
                //string[] s = ss.Split(',');
                //int yearlyworkinghours = Convert.ToInt32(s[0]);
                //ht["info"] = s[1];

                WorkingHours = String.Empty;
                TimeSpan _yearlyworkinghours = TimeSpan.FromHours(yearlyworkinghours);
                WorkingHours = GetTotalHours(_yearlyworkinghours);
                ht["WorkingHours"] = WorkingHours + " Hours " + ((_yearlyworkinghours.Minutes != 0) ? _yearlyworkinghours.Minutes + " Mins" : "");

                DataTable dtHistory = new DataTable();
                dtHistory.Columns.Add("DateTime", typeof(DateTime));
                dtHistory.Columns.Add("Day");
                dtHistory.Columns.Add("Clock-in");
                dtHistory.Columns.Add("Clock-out");
                dtHistory.Columns.Add("Time Spent");
                dtHistory.Merge(FormatDataTable(dtAttendance, "attendance", Appid));

                if (instanceid == "ae5eb924-def7-461e-8097-c503ec893bfc")
                    dtHistory.Merge(FormatDataTable(dtAllAbsences, "absence", Appid));
                else
                    dtHistory.Merge(FormatDataTable(dtAbsences, "absence", Appid));

                //string unmarked_dates = GetUnmarkedDays(dtHistory, startDate, endDate, ParentId); OLD
                string unmarked_dates = GetUnmarkedDays(dtHistory, startDate, user_endDate, ParentId, year);
                if (unmarked_dates != "")
                    ht["UnmarkedDays"] = unmarked_dates.Split(',').Count();
                else
                    ht["UnmarkedDays"] = 0;

                WorkingHours = String.Empty;
                //ht["TotalWorkedHoursB"] = Math.Round(minutessss.TotalHours) + " Hours " + ((minutessss.Minutes != 0) ? minutessss.Minutes + " Mins" : "");
                WorkingHours = GetTotalHours(minutessss);
                ht["TotalWorkedHoursB"] = WorkingHours + " Hours " + ((minutessss.Minutes != 0) ? minutessss.Minutes + " Mins" : "");
                //ht["TotalWorkedHoursB"] = string.Format("{0:d2} Hours {1:d2} Mins", (int)minutessss.TotalHours, (int)minutessss.Minutes);


                TimeSpan totalhours = _yearlyworkinghours.Subtract(minutessss).Duration();

                //if (totalhours.TotalHours < 0)
                //{
                //    ht["ExtraHours"] = Math.Round(totalhours.TotalHours) + " Hours " + ((totalhours.Minutes != 0) ? totalhours.Minutes + " Mins" : "");
                //    ht["UnderHours"] = 0;
                //}
                //else
                //{
                //    ht["ExtraHours"] = 0;
                //    ht["UnderHours"] = Math.Round(totalhours.TotalHours) + " Hours " + ((totalhours.Minutes != 0) ? totalhours.Minutes + " Mins" : "");
                //}

                WorkingHours = String.Empty;
                WorkingHours = GetTotalHours(totalhours);
                //if (totalhours.CompareTo(TimeSpan.Zero) < 0)
                if (TimeSpan.Compare(_yearlyworkinghours, minutessss) < 0)
                {
                    if (WorkingHours != "0" || totalhours.Minutes != 0)
                        ht["ExtraHours"] = WorkingHours + " Hours " + ((totalhours.Minutes != 0) ? totalhours.Minutes + " Mins" : "");
                    else
                        ht["ExtraHours"] = 0;
                    //totalhours.TotalHours.ToString("0") + " Hours " + ((totalhours.Minutes != 0) ? totalhours.Minutes + " Mins" : "");
                    //totalhours.ToString(@"h\.m").Split('.')[0] + " Hours " + totalhours.ToString(@"h\.m").Split('.')[1] + " Mins";
                    ht["UnderHours"] = 0;
                }
                else
                {
                    ht["ExtraHours"] = 0;
                    if (WorkingHours != "0" || totalhours.Minutes != 0)
                        ht["UnderHours"] = WorkingHours + " Hours " + ((totalhours.Minutes != 0) ? totalhours.Minutes + " Mins" : "");
                    else
                        ht["UnderHours"] = 0;
                    //totalhours.TotalHours.ToString("0") + " Hours " + ((totalhours.Minutes != 0) ? totalhours.Minutes + " Mins" : "");
                }
            }
            else
            {
                minutessss = new TimeSpan(0, 0, 0);
                ht["TotalWorkedHoursB"] = "0 hour";
                ht["WorkingHours"] = "0 hour";
                ht["ExtraHours"] = 0;
                ht["UnderHours"] = 0;
                ht["UnmarkedDays"] = 0;
            }

            ht["AdvanceSttings"] = (GetWorkingHours(ParentId).Rows.Count > 0) ? 1 : 0;
            ht["startDate"] = startDate;
            //ht["endDate"] = endDate;
            ht["endDate"] = user_endDate;
            ht["userid"] = userid;
        }

        return JsonConvert.SerializeObject(ht);
    }

    public string GetTotalHours(TimeSpan totalhours)
    {
        string WorkingHours = String.Empty;
        if (totalhours.Days > 0)
        {
            WorkingHours = ((totalhours.Days * 24) + totalhours.Hours).ToString();
        }
        else
        {
            WorkingHours = totalhours.Hours.ToString();
        }

        return WorkingHours;
    }

    public TimeSpan GetTotalWorkedTime(DataTable records, int userid, string Appid, string instanceid, int year)
    {
        TimeSpan start = new TimeSpan();
        TimeSpan end = new TimeSpan();
        Double minutes = 0;
        DataRow temp = null;

        DataTable workinghours = GetWorkingHours(userid);
        DateTime currenttime = Convert.ToDateTime(App.GetTime(DateTime.UtcNow.TimeOfDay.ToString(), Appid));
        List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(getParent(userid.ToString()).ToInt32(), instanceid);
        //List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(9845, "6752062f-f30f-4abe-b048-6cc2daee6d3d");

        Hashtable ht = new Hashtable();
        ht["@userid"] = userid;
        DataTable settings = dal.ExecuteSQLQueryData("Select TOP 1 * from schema_6e815b00_6e13_4839_a57d_800a92809f21.ReportSettings where userid = @userid And Chophours = 1", ht);
        //DateTime? ChophoursFrom = null;
        DateTime ChophoursFrom = new DateTime();
        if (settings.Rows.Count > 0)
            ChophoursFrom = Convert.ToDateTime(settings.Rows[0]["ChophoursFrom"]);


        if (workinghours.Rows.Count > 0)
        {
            DataTable setting_days = new DataTable();
            setting_days.Merge(JsonConvert.DeserializeObject<DataTable>(GetUserHolidaysWeekends(userid, "0"))); //weekend
            setting_days.Merge(JsonConvert.DeserializeObject<DataTable>(GetUserHolidaysWeekends(userid, "1", year))); //holidays

            foreach (DataRow dr in records.Rows)
            {
                DateTime date1 = Convert.ToDateTime(dr["lastin"]);
                DateTime date2 = (dr["lastout"] != DBNull.Value) ? Convert.ToDateTime(dr["lastout"]) : currenttime;
                Double Break = 0;
                Double totalminutes = (dr["minutes"] != DBNull.Value) ? dr["minutes"].ToInt32() : date2.Subtract(date1).TotalMinutes;
                string query = "title='" + date1.DayOfWeek + "' OR Convert(date, 'System.String') LIKE '" + date1.ToString("M/dd") + "%'";
                //if (setting_days.Rows.Count > 0 && setting_days.Select("title='" + date1.DayOfWeek + "' OR Convert(date, 'System.String') LIKE '%" + date1.ToString("M/dd") + "%'").Count() == 0)
                if (checkDataTable(setting_days, query) == 0)
                {
                    temp = workinghours.Select("DayTitle = '" + date1.DayOfWeek + "'").FirstOrDefault();
                    List<ExceptionalDay> exDay = exDays.Where(u => u.Date.Date.ToShortDateString() == date1.Date.ToShortDateString()).ToList();

                    if (temp != null && exDay.Count == 0)
                    {
                        start = date1.TimeOfDay;
                        end = date2.TimeOfDay;

                        //Subtract Break Time; if break taken
                        TimeSpan BreakStart = Convert.ToDateTime(temp["BreakStart"]).TimeOfDay;
                        TimeSpan BreakEnd = Convert.ToDateTime(temp["BreakEnd"]).TimeOfDay;

                        //if ((BreakStart >= start && BreakStart <= end) && (BreakEnd >= start && BreakEnd <= end))
                        //{
                        //    Break = BreakEnd.Subtract(BreakStart).TotalMinutes;
                        //}
                        //else if ((start >= BreakStart) && (start <= BreakEnd))
                        //{
                        //    Break = BreakEnd.Subtract(start).TotalMinutes;
                        //}
                        //else
                        //    Break = 0;

                        //9AM - 6PM
                        if (BreakStart >= start && BreakEnd <= end)
                        {
                            Break = BreakEnd.Subtract(BreakStart).TotalMinutes;
                        }
                        //9AM - 1.30PM
                        else if (BreakStart >= start && (BreakStart <= end && BreakEnd >= end))
                        {
                            Break = end.Subtract(BreakStart).TotalMinutes;
                        }
                        //1PM - 6PM
                        else if ((BreakStart <= start && BreakEnd >= start) && BreakEnd <= end)
                        {
                            Break = BreakEnd.Subtract(start).TotalMinutes;
                        }
                        //1PM - 2PM
                        else if (BreakStart <= start && BreakEnd >= end)
                        {
                            Break = end.Subtract(start).TotalMinutes;
                        }
                        else
                            Break = 0;

                        minutes = minutes + (totalminutes - Break);
                        //End

                        //Subtract Extra Morning Time; If Clocked-in Earlier 
                        TimeSpan DayStart = Convert.ToDateTime(temp["Clockin"]).TimeOfDay;
                        TimeSpan ClockedIn = date1.TimeOfDay;

                        if (ChophoursFrom != DateTime.MinValue)
                        {
                            if ((ClockedIn > Convert.ToDateTime(ChophoursFrom).TimeOfDay) && (ClockedIn < DayStart))
                            {
                                if (minutes != 0)
                                    minutes = minutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                            }
                        }
                        else
                        {
                            if (ClockedIn < DayStart)
                            {
                                if (minutes != 0)
                                    minutes = minutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                            }
                        }


                        //if ((ClockedIn > Convert.ToDateTime(ChophoursFrom).TimeOfDay && ChophoursFrom != DateTime.MinValue) && (ClockedIn < DayStart))
                        //{
                        //    if (minutes != 0)
                        //        minutes = minutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                        //}
                        //else if (ClockedIn < DayStart)
                        //{
                        //    if (minutes != 0)
                        //        minutes = minutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                        //}


                        //if (ClockedIn < DayStart)
                        //{                           
                        //        if (minutes != 0)
                        //            minutes = minutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                        //}
                        //End
                    }
                    else
                    {
                        if (exDay.Count != 0)
                        {
                            //Subtract Extra Morning Time; If Clocked-in Earlier 
                            TimeSpan DayStart = exDay[0].Clockin.TimeOfDay;
                            TimeSpan ClockedIn = date1.TimeOfDay;

                            if (ChophoursFrom != DateTime.MinValue)
                            {
                                if ((ClockedIn > Convert.ToDateTime(ChophoursFrom).TimeOfDay) && (ClockedIn < DayStart))
                                {
                                    minutes = minutes + (totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes);
                                }
                                else
                                    minutes += totalminutes;
                            }
                            else
                            {
                                if (ClockedIn < DayStart)
                                {
                                    minutes = minutes + (totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes);
                                }
                                else
                                    minutes += totalminutes;
                            }

                            //if ((ClockedIn > Convert.ToDateTime(ChophoursFrom).TimeOfDay && ChophoursFrom != DateTime.MinValue) && (ClockedIn < DayStart))
                            //{
                            //    minutes = minutes + (totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes);
                            //}
                            //else if (ClockedIn < DayStart)
                            //{
                            //    minutes = minutes + (totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes);
                            //}
                            //else
                            //    minutes += totalminutes;

                            //if (ClockedIn < DayStart )
                            //{
                            //    minutes = minutes + (totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes);
                            //}
                            //else
                            //    minutes += totalminutes;

                            //End
                        }
                        else
                            minutes += totalminutes;
                    }
                }
                else
                {
                    minutes += totalminutes;
                }
            }
        }
        else
        {
            foreach (DataRow dr in records.Rows)
            {
                DateTime date1 = Convert.ToDateTime(dr["lastin"]);
                DateTime date2 = (dr["lastout"] != DBNull.Value) ? Convert.ToDateTime(dr["lastout"]) : currenttime;
                Double totalminutes = 0;

                List<ExceptionalDay> exDay = exDays.Where(u => u.Date.Date.ToShortDateString() == date1.Date.ToShortDateString()).ToList();

                totalminutes = (dr["minutes"] != DBNull.Value) ? dr["minutes"].ToInt32() : date2.Subtract(date1).TotalMinutes;

                if (exDay.Count == 0)
                {
                    minutes += totalminutes;
                }
                else
                {
                    //Subtract Extra Morning Time; If Clocked-in Earlier 
                    TimeSpan DayStart = exDay[0].Clockin.TimeOfDay;
                    TimeSpan ClockedIn = date1.TimeOfDay;

                    if (ChophoursFrom != DateTime.MinValue)
                    {
                        if ((ClockedIn > Convert.ToDateTime(ChophoursFrom).TimeOfDay) && (ClockedIn < DayStart))
                        {
                            minutes = minutes + (totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes);
                        }
                        else
                            minutes += totalminutes;
                    }
                    else
                    {
                        if (ClockedIn < DayStart)
                        {
                            minutes = minutes + (totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes);
                        }
                        else
                            minutes += totalminutes;
                    }

                    //if ((ClockedIn > Convert.ToDateTime(ChophoursFrom).TimeOfDay && ChophoursFrom != DateTime.MinValue) && (ClockedIn < DayStart))
                    //{
                    //    minutes = minutes + (totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes);
                    //}
                    //else if (ClockedIn < DayStart)
                    //{
                    //    minutes = minutes + (totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes);
                    //}
                    //else
                    //    minutes += totalminutes;

                    //if (ClockedIn < DayStart)
                    //{
                    //    minutes = minutes + (totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes);
                    //}
                    //else
                    //    minutes += totalminutes;

                    //End
                }
            }
        }

        return TimeSpan.FromMinutes(minutes);
        //return minutes;
    }

    public DataTable GetWorkingHours(int userid)
    {
        DataTable workinghours = new DataTable();
        DataTable settings = new DataTable();
        Hashtable ht;
        bool check = false;

        ht = new Hashtable();
        ht["@userid"] = userid;
        settings = dal.ExecuteSQLQueryData("Select ApplyWorkingHours from schema_6e815b00_6e13_4839_a57d_800a92809f21.ReportSettings where userid = @userid", ht);

        if (settings.Rows.Count > 0)
            check = Convert.ToBoolean(settings.Rows[0]["ApplyWorkingHours"]);

        if (check)
        {
            //    ht = new Hashtable();
            //ht["@userid"] = userid;
            workinghours = new DataTable();
            workinghours = dal.ExecuteSQLQueryData("Select * from schema_6e815b00_6e13_4839_a57d_800a92809f21.WorkingHours where UserID = @userid and Active = 1", ht);
        }

        return workinghours;
    }

    public string AdvanceCalculateStatistics_newold(int ParentId, int HoursPerDay, int year, int month, int userid, string instanceid, int absences, decimal breakHr, string startHour, string endHour, string Appid, string report_type, string additionalbreak, decimal additionalbreakHr, string customstartDate = "", string customendDate = "")
    {
        Hashtable ht = new Hashtable();
        int monthI = DateTime.ParseExact("January", "MMMM", CultureInfo.InvariantCulture).Month;
        DateTime startDate = new DateTime(year, 1, 1, 0, 0, 0);
        DateTime endDate = new DateTime();

        Hashtable userdetail = new Hashtable();
        userdetail["@userid"] = userid;
        DataTable user_joindate = dal.ExecuteSQLQueryData("Select top 1 * from schema_6e815b00_6e13_4839_a57d_800a92809f21.workertrans where p_pid=@userid", userdetail);

        if (startDate < DateTime.Now)
        {
            if (report_type == "monthly")
            {
                startDate = new DateTime(year, month, 1);

                if (startDate.Month == DateTime.Now.Month)
                    endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
                else
                    endDate = startDate.AddMonths(1).AddDays(-1);
            }
            else if (report_type == "yearly")
            {
                if (user_joindate.Rows.Count > 0)
                {
                    if (Convert.ToDateTime(user_joindate.Rows[0]["lastin"]) > startDate)
                        startDate = Convert.ToDateTime(user_joindate.Rows[0]["lastin"]);
                }

                endDate = new DateTime(year, 12, DateTime.DaysInMonth(startDate.Year, startDate.Month), 23, 59, 59);
                if (endDate > DateTime.Now)
                {
                    endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
                }
            }
            else if (report_type == "custom")
            {
                if (Convert.ToDateTime(customstartDate) < Convert.ToDateTime(user_joindate.Rows[0]["lastin"]))
                    startDate = Convert.ToDateTime(user_joindate.Rows[0]["lastin"]);
                else
                    startDate = Convert.ToDateTime(customstartDate);

                endDate = Convert.ToDateTime(customendDate);
            }

            if (additionalbreakHr > 0)
            {
                if (additionalbreak == "week")
                {
                    TimeSpan ts = endDate.Subtract(startDate);
                    int dateDiff = ts.Days;
                    int totalWeeks = (int)dateDiff / 7;
                    additionalbreakHr = additionalbreakHr * totalWeeks;
                }
                else
                {
                    int monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
                    additionalbreakHr = Math.Abs(monthsApart) * additionalbreakHr;
                }
            }

            DateTime abDate = new DateTime(year, 1, 1, 0, 0, 0); DataTable dtAbsences;
            if (report_type == "yearly")
                dtAbsences = GetYearlyAbsences(userid, abDate, endDate);
            else
                dtAbsences = GetYearlyAbsences(userid, startDate, endDate);


            DataTable dtAttendance = GetYearlyAttendance(userid, startDate, endDate);
            DataTable uniqueCols = dtAttendance.DefaultView.ToTable(true, "FormatedDate");

            //TimeSpan Whrs = AdvanceWorkedHour(dtAttendance, Appid, startHour, endHour);
            //decimal workedhours = (Math.Round(Convert.ToDecimal(Whrs.TotalMinutes / 60), 2));

            TimeSpan Whrs = WorkedHourNew(dtAttendance);
            decimal workedhours = Convert.ToDecimal(Whrs.TotalHours);

            //TimeSpan Whrs = AWorkedHour(dtAttendance, dtAbsences, Appid, ParentId, breakHr, additionalbreakHr);
            //decimal workedhours = Convert.ToDecimal(Whrs.TotalHours);

            //ht["TotalWorkedHours"] = workedhours;
            //ht["TotalDaysWorked"] = dtAttendance.Rows.Count.ToString();
            //ht["TotalAbsences"] = dtAbsences.Rows.Count.ToString();
            int TotalAbsences = dtAbsences.Rows.Count;
            ht["Absences"] = TotalAbsences;
            int yearlyworkingdays = 0;

            if (TotalAbsences <= absences)
                yearlyworkingdays = YearlyWorkingDays(startDate, endDate, ParentId, year) - GetYearlyAbsences(userid, startDate, endDate).Rows.Count;
            else
                yearlyworkingdays = YearlyWorkingDays(startDate, endDate, ParentId, year) - absences;

            int yearlyworkinghours = yearlyworkingdays * HoursPerDay;

            breakHr = (breakHr != 0) ? breakHr : 1;

            decimal yearlybreakhours = Math.Round(yearlyworkingdays * breakHr) + additionalbreakHr;
            //decimal yearlybreakdays = Math.Round(yearlybreakhours / 24);
            decimal totalbreakhours = Math.Round(dtAttendance.DefaultView.ToTable(true, "FormatedDate").Rows.Count * breakHr) + additionalbreakHr;
            decimal totalbreakdays = Math.Round(totalbreakhours / 24);

            ht["WorkingDays"] = yearlyworkingdays;
            ht["WorkingHours"] = yearlyworkinghours;
            //ht["WorkingDaysB"] = Math.Abs(yearlyworkingdays - yearlybreakdays);
            //ht["WorkingHoursB"] = Math.Abs(yearlyworkinghours - yearlybreakhours);
            //ht["WorkingHoursB"] = yearlyworkinghours;


            //int UnmarkedDays = yearlyworkingdays - (uniqueCols.Rows.Count + dtAbsences.Rows.Count);
            //int UnmarkedDays = yearlyworkingdays - (dtAttendance.Rows.Count + dtAbsences.Rows.Count);
            DataTable dtHistory = new DataTable();
            dtHistory.Columns.Add("DateTime", typeof(DateTime));
            dtHistory.Columns.Add("Day");
            dtHistory.Columns.Add("Clock-in");
            dtHistory.Columns.Add("Clock-out");
            dtHistory.Columns.Add("Time Spent");
            dtHistory.Merge(FormatDataTable(dtAttendance, "attendance", Appid));
            dtHistory.Merge(FormatDataTable(dtAbsences, "absence", Appid));

            string unmarked_dates = GetUnmarkedDays(dtHistory, startDate, endDate, ParentId, year);
            if (unmarked_dates != "")
                ht["UnmarkedDays"] = unmarked_dates.Split(',').Count();
            else
                ht["UnmarkedDays"] = 0;


            //if (workedhours != 0)
            //{

            //ht["TotalWorkedHoursB"] = Math.Round(workedhours - totalbreakhours);
            ht["TotalWorkedHoursB"] = UtilityMethods.getFormatedTimeByMinutes(TimeSpan.FromHours(Convert.ToDouble(workedhours)).TotalMinutes.ToInt32());
            //ht["TotalDaysWorkedB"] = Math.Round(dtAttendance.Rows.Count - totalbreakdays);

            //int _workedhours = ht["TotalWorkedHoursB"].ToInt32();
            //decimal _workedhours = Math.Round(workedhours - totalbreakhours);

            decimal _workedhours = workedhours;
            int _yearlyworkinghours = ht["WorkingHours"].ToInt32();

            //test
            //if (instanceid == "f425f912-8d79-4415-ba72-89a5309acb10" || userid == 10192)
            //{
            //    decimal ut = GetExtratime(userid, startDate, endDate);
            //    ht["UnderTime"] = UtilityMethods.getFormatedTimeByMinutes(TimeSpan.FromHours(Convert.ToDouble(ut)).TotalMinutes.ToInt32());

            //    _workedhours = _workedhours - ut;
            //}

            if (_workedhours > _yearlyworkinghours)
            {
                ht["ExtraHours"] = UtilityMethods.getFormatedTimeByMinutes(TimeSpan.FromHours(Convert.ToDouble(Math.Abs((_workedhours - _yearlyworkinghours)))).TotalMinutes.ToInt32());
                ht["UnderHours"] = 0;
            }
            else
            {
                ht["ExtraHours"] = 0;
                ht["UnderHours"] = UtilityMethods.getFormatedTimeByMinutes(TimeSpan.FromHours(Convert.ToDouble(Math.Abs((_workedhours - _yearlyworkinghours)))).TotalMinutes.ToInt32());
            }

            ht["startDate"] = startDate;
            ht["endDate"] = endDate;
            ht["userid"] = userid;
        }

        return JsonConvert.SerializeObject(ht);
    }


    public string AdvanceCalculateStatistics_old(int ParentId, int HoursPerDay, int year, int month, int userid, string instanceid, decimal breakHr, string startHour, string endHour, string Appid, string report_type)
    {
        Hashtable ht = new Hashtable();
        int monthI = DateTime.ParseExact("January", "MMMM", CultureInfo.InvariantCulture).Month;
        DateTime startDate = new DateTime(year, 1, 1, 0, 0, 0);
        DateTime endDate = new DateTime();

        Hashtable userdetail = new Hashtable();
        //userdetail["@id"] = 0;
        //userdetail["@instanceid"] = instanceid;
        userdetail["@userid"] = userid;
        DataTable user_joindate = dal.ExecuteSQLQueryData("Select top 1 * from schema_6e815b00_6e13_4839_a57d_800a92809f21.workertrans where p_pid=@userid", userdetail);
        //DateTime user_joindate = Convert.ToDateTime(GetUserDetails(userdetail).Rows[0]["datefield"]);

        if (startDate < DateTime.Now)
        {
            if (report_type == "monthly")
            {
                startDate = new DateTime(year, month, 1);
                endDate = startDate.AddMonths(1).AddDays(-1);
            }
            else if (report_type == "yearly")
            {
                startDate = Convert.ToDateTime(user_joindate.Rows[0]["lastin"]);

                endDate = new DateTime(year, 12, DateTime.DaysInMonth(startDate.Year, startDate.Month), 23, 59, 59);
                if (endDate > DateTime.Now)
                {
                    endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
                }
            }

            DataTable dtAbsences = GetYearlyAbsences(userid, startDate, endDate);
            DataTable dtAttendance = GetYearlyAttendance(userid, startDate, endDate);
            DataTable uniqueCols = dtAttendance.DefaultView.ToTable(true, "FormatedDate");

            TimeSpan Whrs = AdvanceWorkedHour(dtAttendance, Appid, startHour, endHour);
            decimal workedhours = (Math.Round(Convert.ToDecimal(Whrs.TotalMinutes / 60), 2));
            ht["TotalWorkedHours"] = workedhours;
            ht["TotalDaysWorked"] = dtAttendance.Rows.Count.ToString();
            ht["TotalAbsences"] = dtAbsences.Rows.Count.ToString();
            int yearlyworkingdays = YearlyWorkingDays(startDate, endDate, ParentId, year);
            int yearlyworkinghours = yearlyworkingdays * HoursPerDay;
            //decimal yearlybreakdays = Math.Round(yearlyworkingdays * (breakHr / 1440));
            //decimal yearlybreakhours = Math.Round(yearlyworkingdays * ((breakHr % 1440) / 60));
            //decimal totalbreakdays = Math.Round(uniqueCols.Rows.Count * (breakHr / 1440));
            //decimal totalbreakhours = Math.Round(uniqueCols.Rows.Count * ((breakHr % 1440) / 60));
            decimal yearlybreakhours = Math.Round(yearlyworkingdays * breakHr);
            decimal yearlybreakdays = Math.Round(yearlybreakhours / 24);
            //decimal totalbreakhours = Math.Round(dtAttendance.Rows.Count * breakHr);
            decimal totalbreakhours = Math.Round(dtAttendance.DefaultView.ToTable(true, "FormatedDate").Rows.Count * breakHr);
            decimal totalbreakdays = Math.Round(totalbreakhours / 24);

            ht["WorkingDays"] = yearlyworkingdays;
            ht["WorkingHours"] = yearlyworkinghours;
            ht["WorkingDaysB"] = Math.Abs(yearlyworkingdays - yearlybreakdays);
            ht["WorkingHoursB"] = Math.Abs(yearlyworkinghours - yearlybreakhours);

            int UnmarkedDays = ht["TotalDaysWorked"].ToInt32() - (uniqueCols.Rows.Count + dtAbsences.Rows.Count);
            ht["UnmarkedDays"] = UnmarkedDays > 0 ? UnmarkedDays : 0;
            ht["Absences"] = dtAbsences.Rows.Count;

            if (workedhours != 0)
            {
                ht["TotalWorkedHoursB"] = Math.Round(workedhours - totalbreakhours);
                ht["TotalDaysWorkedB"] = Math.Round(dtAttendance.Rows.Count - totalbreakdays);

                int _workedhours = ht["TotalWorkedHoursB"].ToInt32();
                int _yearlyworkinghours = ht["WorkingHoursB"].ToInt32();

                if (_workedhours > _yearlyworkinghours)
                {
                    ht["ExtraHours"] = Math.Abs((_workedhours - _yearlyworkinghours));
                    //Math.Abs((workedhours - yearlyworkinghours));
                    ht["UnderHours"] = 0;
                }
                else
                {
                    ht["ExtraHours"] = 0;
                    ht["UnderHours"] = Math.Abs((_yearlyworkinghours - _workedhours));
                    //Math.Abs(yearlyworkinghours - workedhours);
                }


            }
            else
            {
                ht["ExtraHours"] = 0;
                ht["UnderHours"] = 0;
                ht["TotalWorkedHoursB"] = 0;
                ht["TotalDaysWorkedB"] = 0;
            }

        }

        return JsonConvert.SerializeObject(ht);
    }

    private int WorkingHoursShouldBe(DateTime start, DateTime stop, int parentid, int userid, int year, int AbsencesAllowed = 0, string instanceid = "0", int workingHours = 1)
    {
        DataTable weekends = JsonConvert.DeserializeObject<DataTable>(GetUserHolidaysWeekends(parentid, "0"));
        DataTable holidays = JsonConvert.DeserializeObject<DataTable>(GetUserHolidaysWeekends(parentid, "1", year));
        DataTable Absences = (instanceid == "ae5eb924-def7-461e-8097-c503ec893bfc") ? GetAllYearlyAbsences(userid, start, stop) : GetYearlyAbsences(userid, start, stop); 
        DateTime startDate = start;
        DateTime endDate = stop;
        string Filter = "date >='" + startDate.ToString("yyyy-MM-dd") + "' AND date <='" + endDate.ToString("yyyy-MM-dd") + "'";
        int check = 0;

        Hashtable ht = new Hashtable();
        ht["@userid"] = parentid;
        ht["@date"] = start.ToString("M-dd");
        ht["@fulldate"] = start.ToString("yyyy-MM-dd");
        int hours = 0;
        int count = 1;
        DataRow dr = null;
        //string time = "";
        DataTable workinghours = GetWorkingHours(parentid);
        //List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(9845, "6752062f-f30f-4abe-b048-6cc2daee6d3d");
        List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(parentid, instanceid);

        //Night Shift Instance check
        if (instanceid != "ae5eb924-def7-461e-8097-c503ec893bfc")
        {
            if (weekends.Rows.Count > 0)
            {
                while (start.Date <= stop.Date)
                {
                    List<ExceptionalDay> exDay = exDays.Where(u => u.Date.Date == start.Date).ToList();
                    //string query = "( Convert(date, 'System.String') LIKE '%" + start.ToString("/M/dd") + "%' AND  everyyear = 1) OR (  date = '" + start.Date + "')";
                    string query = "(  date = '" + start.Date + "')";

                    if (weekends.Select("title='" + start.DayOfWeek + "'").Count() == 0 &&
                     checkDataTable(holidays, query) == 0
                            //holidays.Select("( Convert(date, 'System.String') LIKE '%" + start.ToString("M/dd") + "%' AND  everyyear = 1) OR (  date = '" + start.Date + "')").Count() == 0
                            )

                    //Change query
                    //if (weekends.Select("title='" + start.DayOfWeek + "'").Count() == 0 &&
                    //    //dal.ExecuteSQLQueryData("Select * from tblholidays where userid = @userid and convert(varchar,date) like '%@date%'", ht).Rows.Count == 0
                    //    dal.ExecuteSQLQueryData("Select * from tblholidays where userid = @userid and type = 1 and ((convert(varchar, date) = '@fulldate') OR(convert(varchar, date) like '%@date%' AND everyyear = 1))", ht).Rows.Count == 0)
                    {
                        if (workinghours.Rows.Count > 0)
                        {
                            dr = workinghours.Select("DayTitle = '" + start.DayOfWeek + "'").FirstOrDefault();
                            if (dr.ItemArray.Count() > 0)
                            {
                                query = "Convert(CrtDate, 'System.String') LIKE '" + start.Date.ToShortDateString() + "%'";

                                if (instanceid == "f425f912-8d79-4415-ba72-89a5309acb10")
                                {
                                    if (checkDataTable(Absences, query) > 0)
                                    {
                                        check++;
                                    }
                                    else
                                    {
                                        if (exDay.Count == 0)
                                            hours += dr["TotalWorkingHours"].ToInt32();
                                        else
                                            hours += exDay[0].Clockout.Subtract(exDay[0].Clockin).TotalHours.ToInt32();
                                    }

                                }
                                else
                                {
                                    if (check <= AbsencesAllowed && (checkDataTable(Absences, query) > 0)
                                //(Absences.Select("Convert(CrtDate, 'System.String') LIKE '%" + start.Date.ToShortDateString() + "%'").Count() > 0)
                                )
                                    {
                                        //if (Absences.Select("Convert(CrtDate, 'System.String') LIKE '%" + start.Date + "%'").Count() > 0)
                                        //// if (Absences.Select("Convert(CrtDate, 'System.String') LIKE '" + start.ToString("yyyy-MM-dd") + "%'").Count() > 0)
                                        //{
                                        //    //hours += dr["TotalWorkingHours"].ToInt32();
                                        check++;
                                        //}
                                        //else
                                        //{
                                        //    hours += dr["TotalWorkingHours"].ToInt32();
                                        //    //check++;
                                        //}
                                    }
                                    else
                                    {
                                        if (exDay.Count == 0)
                                            hours += dr["TotalWorkingHours"].ToInt32();
                                        else
                                            hours += exDay[0].Clockout.Subtract(exDay[0].Clockin).TotalHours.ToInt32();
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (exDay.Count > 0)
                                hours += exDay[0].Clockout.Subtract(exDay[0].Clockin).TotalHours.ToInt32() * count;
                            else
                                hours += workingHours * count;

                            count++;
                        }
                    }

                    //time = time + "---" + hours.ToString() + " (" + start.ToString() + ")";
                    start = start.AddDays(1);
                    ht["@date"] = start.ToString("M-dd");
                    ht["@fulldate"] = start.ToString("yyyy-MM-dd");
                }
            }
        }
        else
        {
            while (start.Date <= stop.Date)
            {
                if (start.Day != 0)
                {
                    List<ExceptionalDay> exDay = exDays.Where(u => u.Date.Date == start.Date).ToList();
                    string query = "( Convert(date, 'System.String') LIKE '%" + start.ToString("/M/dd") + "%' AND  everyyear = 1) OR (  date = '" + start.Date + "')";

                    query = "Convert(CrtDate, 'System.String') LIKE '%" + start.Date.ToShortDateString() + "%'";
                    if (checkDataTable(Absences, query) > 0)
                    //if (check <= AbsencesAllowed && (checkDataTable(Absences, query) > 0))
                    {
                        check++;
                    }
                    else
                    {
                        if (exDay.Count > 0)
                            hours += exDay[0].Clockout.Subtract(exDay[0].Clockin).TotalHours.ToInt32();
                        else
                            hours += workingHours;
                    }

                    count++;
                }

                start = start.AddDays(1);
                ht["@date"] = start.ToString("M-dd");
                ht["@fulldate"] = start.ToString("yyyy-MM-dd");
            }
        }

        return hours;
    }

    public string WorkingHoursShouldBe_table(DateTime start, DateTime stop, int parentid, int userid, int year, int AbsencesAllowed = 0, string instanceid = "0", int workingHours = 1)
    {
        string table = "<table>";

        DataTable weekends = JsonConvert.DeserializeObject<DataTable>(GetUserHolidaysWeekends(parentid, "0"));
        DataTable holidays = JsonConvert.DeserializeObject<DataTable>(GetUserHolidaysWeekends(parentid, "1", year));
        DataTable Absences = GetYearlyAbsences(userid, start, stop);
        DateTime startDate = start;
        DateTime endDate = stop;
        string Filter = "date >='" + startDate.ToString("yyyy-MM-dd") + "' AND date <='" + endDate.ToString("yyyy-MM-dd") + "'";
        int check = 0;

        Hashtable ht = new Hashtable();
        ht["@userid"] = parentid;
        ht["@date"] = start.ToString("M-dd");
        ht["@fulldate"] = start.ToString("yyyy-MM-dd");
        int hours = 0;
        int count = 1;
        DataRow dr = null;

        DataTable workinghours = GetWorkingHours(parentid);
        //List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(9845, "6752062f-f30f-4abe-b048-6cc2daee6d3d");
        List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(parentid, instanceid);

        if (weekends.Rows.Count > 0)
        {
            while (start.Date <= stop.Date)
            {
                List<ExceptionalDay> exDay = exDays.Where(u => u.Date.Date == start.Date).ToList();
                string query = "( Convert(date, 'System.String') LIKE '%" + start.ToString("/M/dd") + "%' AND  everyyear = 1) OR (  date = '" + start.Date + "')";

                if (weekends.Select("title='" + start.DayOfWeek + "'").Count() == 0 &&
                 checkDataTable(holidays, query) == 0
                        //holidays.Select("( Convert(date, 'System.String') LIKE '%" + start.ToString("M/dd") + "%' AND  everyyear = 1) OR (  date = '" + start.Date + "')").Count() == 0
                        )

                //Change query
                //if (weekends.Select("title='" + start.DayOfWeek + "'").Count() == 0 &&
                //    //dal.ExecuteSQLQueryData("Select * from tblholidays where userid = @userid and convert(varchar,date) like '%@date%'", ht).Rows.Count == 0
                //    dal.ExecuteSQLQueryData("Select * from tblholidays where userid = @userid and type = 1 and ((convert(varchar, date) = '@fulldate') OR(convert(varchar, date) like '%@date%' AND everyyear = 1))", ht).Rows.Count == 0)
                {
                    if (workinghours.Rows.Count > 0)
                    {
                        dr = workinghours.Select("DayTitle = '" + start.DayOfWeek + "'").FirstOrDefault();
                        if (dr.ItemArray.Count() > 0)
                        {
                            query = "Convert(CrtDate, 'System.String') LIKE '%" + start.Date.ToShortDateString() + "%'";
                            if (check <= AbsencesAllowed && (checkDataTable(Absences, query) > 0)
                            //(Absences.Select("Convert(CrtDate, 'System.String') LIKE '%" + start.Date.ToShortDateString() + "%'").Count() > 0)
                            )
                            {
                                //if (Absences.Select("Convert(CrtDate, 'System.String') LIKE '%" + start.Date + "%'").Count() > 0)
                                //// if (Absences.Select("Convert(CrtDate, 'System.String') LIKE '" + start.ToString("yyyy-MM-dd") + "%'").Count() > 0)
                                //{
                                //    //hours += dr["TotalWorkingHours"].ToInt32();
                                check++;
                                //}
                                //else
                                //{
                                //    hours += dr["TotalWorkingHours"].ToInt32();
                                //    //check++;
                                //}
                            }
                            else
                            {
                                if (exDay.Count == 0)
                                    hours += dr["TotalWorkingHours"].ToInt32();
                                else
                                    hours += exDay[0].Clockout.Subtract(exDay[0].Clockin).TotalHours.ToInt32();
                            }
                        }
                    }
                    else
                    {
                        if (exDay.Count > 0)
                            hours += exDay[0].Clockout.Subtract(exDay[0].Clockin).TotalHours.ToInt32() * count;
                        else
                            hours += workingHours * count;

                        count++;
                    }
                }

                table += "<tr><td>" + start.Date.ToShortDateString() + "</td><td>" + hours.ToString() + "</td></tr>";

                start = start.AddDays(1);
                ht["@date"] = start.ToString("M-dd");
                ht["@fulldate"] = start.ToString("yyyy-MM-dd");


            }
        }

        table += "</table>";
        return table;
    }

    public int checkDataTable(DataTable dt, string query)
    {
        int count = 0;

        if (dt.Rows.Count > 0)
            count = dt.Select(query).Count();

        return count;
    }
    private int YearlyWorkingDays(DateTime start, DateTime stop, int userid, int year)
    {
        DataTable weekends = JsonConvert.DeserializeObject<DataTable>(GetUserHolidaysWeekends(userid, "0"));
        DataTable holidays = JsonConvert.DeserializeObject<DataTable>(GetUserHolidaysWeekends(userid, "1", year));
        DateTime startDate = start;
        DateTime endDate = stop;
        string Filter = "date >='" + startDate.ToString("yyyy-MM-dd") + "' AND date <='" + endDate.ToString("yyyy-MM-dd") + "'";

        Hashtable ht = new Hashtable();
        ht["@userid"] = userid;
        ht["@date"] = start.ToString("MM-dd");
        int days = 0;

        if (weekends.Rows.Count > 0)
        {
            while (start <= stop)
            {
                //foreach (DataRow ob in dt.Rows)
                //{
                //    if (ob["title"].ToString() != start.DayOfWeek.ToString())
                //    {
                //        ++days;
                //    }
                //}
                // if (weekends.Select("title='" + start.DayOfWeek + "'").Count() == 0 && holidays.Select("cast(date as VARCHAR) like '%" + start.ToString("MM-dd") + "%'").Count() == 0)
                if (weekends.Select("title='" + start.DayOfWeek + "'").Count() == 0 &&
                    dal.ExecuteSQLQueryData("Select * from tblholidays where userid = @userid and convert(varchar,date) like '%@date%'", ht).Rows.Count == 0)
                {
                    ++days;
                }



                //if (dal.ExecuteSQLQueryData("Select * from tblholidays where userid = @userid and convert(varchar,date) like '%@date%'", ht).Rows.Count == 0)
                //{
                //    ++days;
                //}


                //if (start.DayOfWeek != DayOfWeek.Saturday && start.DayOfWeek != DayOfWeek.Sunday)
                //{
                //    ++days;
                //}
                start = start.AddDays(1);
                ht["@date"] = start.AddDays(1).ToString("MM-dd");
            }
        }

        //    var filtered = holidays.AsEnumerable()
        //.Where(r => r.Field<String>("date").Contains(start.ToString("MM-dd")));

        // holidays.Select("date like '%" + startDate.Date + "%'");
        //if (holidays.Rows.Count > 0)
        //{
        //    return days - holidays.Select(Filter).Count();
        //}
        //else
        //{
        //    return days;
        //}

        //var results = holidays.AsEnumerable().Where(dr => dr.Field<DateTime>("date").ToShortDateString().Contains("2014-05-01")).ToList();


        return days;

    }

    private string WorkedHour(DataTable dt)
    {
        TimeSpan WorkedHours = new TimeSpan();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["lastout"].ToString() == null || dt.Rows[i]["lastout"].ToString() == "")
            {
                WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(DateTime.Now).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
            }
            else
            {
                WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastout"].ToString())).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
            }
        }

        return UtilityMethods.getFormatedTimeByMinutes(WorkedHours.TotalMinutes.ToInt32());
    }

    private TimeSpan WorkedHourNew(DataTable dt)
    {
        TimeSpan WorkedHours = new TimeSpan();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["lastout"].ToString() == null || dt.Rows[i]["lastout"].ToString() == "")
            {
                WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(DateTime.Now).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
            }
            else
            {
                WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastout"].ToString())).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
            }
        }

        return WorkedHours;
        //return UtilityMethods.getFormatedTimeByMinutes(WorkedHours.TotalMinutes.ToInt32());
    }



    private TimeSpan AdvanceWorkedHour(DataTable dt, string Appid, string StartHour, string EndHour)
    {
        AvaimaTimeZoneAPI atp = new AvaimaTimeZoneAPI();

        TimeSpan WorkedHours = new TimeSpan();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            //TimeSpan duration = DateTime.Parse(atp.GetTime(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"])).ToString(), Appid).ToString()).Subtract(DateTime.Parse(StartHour).AddHours(-6));
            if (StartHour != "" && EndHour != "")
            {
                TimeSpan duration = DateTime.Parse(atp.GetTime(dt.Rows[i]["lastin"].ToString(), Appid).ToString()).Subtract(DateTime.Parse(StartHour).AddHours(-6));

                if (duration.TotalMinutes < 0)
                {
                    dt.Rows[i]["lastin"] = dt.Rows[i]["lastin"].ToString().Split(' ')[0] + " " + StartHour;
                }
            }

            if (dt.Rows[i]["lastout"].ToString() == null || dt.Rows[i]["lastout"].ToString() == "")
            {
                WorkedHours = WorkedHours + DateTime.Now.Subtract(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString()));
            }
            else
            {
                WorkedHours = WorkedHours + Convert.ToDateTime(dt.Rows[i]["lastout"].ToString()).Subtract(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString()));
            }
            //if (dt.Rows[i]["lastout"].ToString() == null || dt.Rows[i]["lastout"].ToString() == "")
            //{
            //    WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(DateTime.Now).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
            //}
            //else
            //{
            //    WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastout"].ToString())).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dt.Rows[i]["lastin"].ToString())));
            //}
        }

        return WorkedHours;
        //return (Math.Round(Convert.ToDecimal(WorkedHours.TotalMinutes/ 60),2));

        // return UtilityMethods.getFormatedTimeByMinutes(WorkedHours.TotalMinutes.ToInt32());
    }

    private TimeSpan AWorkedHour(DataTable dtAttendance, DataTable dtAbsences, string Appid, int parentid, decimal breakHr, decimal additionalbreakHr, int year)
    {
        TimeSpan WorkedHours = new TimeSpan();

        DataTable setting_days = new DataTable();
        setting_days.Merge(JsonConvert.DeserializeObject<DataTable>(GetUserHolidaysWeekends(parentid, "0"))); //weekend
        setting_days.Merge(JsonConvert.DeserializeObject<DataTable>(GetUserHolidaysWeekends(parentid, "1", year))); //holidays

        for (int i = 0; i < dtAttendance.Rows.Count; i++)
        {
            //if Clock-in time doesnot lay in Weekend/Holidays and Absences -- Subtract break time
            if (setting_days.Select("title='" + Convert.ToDateTime(dtAttendance.Rows[i]["lastin"]).DayOfWeek + "' OR date='" + Convert.ToDateTime(dtAttendance.Rows[i]["lastin"]).ToShortDateString() + "'").Count() == 0
                && dtAbsences.Select("CrtDate='" + Convert.ToDateTime(dtAttendance.Rows[i]["lastin"]).ToShortDateString() + "'").Count() == 0)
            {
                if (dtAttendance.Rows[i]["lastout"].ToString() == null || dtAttendance.Rows[i]["lastout"].ToString() == "")
                {
                    WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(DateTime.Now).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dtAttendance.Rows[i]["lastin"].ToString())));
                }
                else
                {
                    WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dtAttendance.Rows[i]["lastout"].ToString())).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dtAttendance.Rows[i]["lastin"].ToString())));

                    //if (Convert.ToDateTime(dtAttendance.Rows[i]["lastin"]).TimeOfDay >= "4:00 AM")
                    WorkedHours = WorkedHours.Subtract(TimeSpan.FromHours(Convert.ToDouble(breakHr)));
                }
            }
            else
            {
                //if Clock-in time lay in Weekend/Holidays/Absence -- Do Not Subtract break time
                if (dtAttendance.Rows[i]["lastout"].ToString() == null || dtAttendance.Rows[i]["lastout"].ToString() == "")
                {
                    WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(DateTime.Now).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dtAttendance.Rows[i]["lastin"].ToString())));
                }
                else
                {
                    WorkedHours = WorkedHours + TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dtAttendance.Rows[i]["lastout"].ToString())).Subtract(TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(dtAttendance.Rows[i]["lastin"].ToString())));
                }
            }
        }

        return WorkedHours;
    }


    public DataTable GetYearlyAbsences(Int32 UserID, DateTime start, DateTime end)
    {
        DataTable dt = new DataTable();
        Hashtable ht = new Hashtable();
        ht["@userID"] = UserID;
        ht["@startDate"] = start;
        ht["@endDate"] = end;
        dt = dal.GetDataTable("sp_GetYearlyAbsences", ht);

        return dt;
    }

    public DataTable GetAllYearlyAbsences(Int32 UserID, DateTime start, DateTime end)
    {
        DataTable dt = new DataTable();
        Hashtable ht = new Hashtable();
        ht["@userID"] = UserID;
        ht["@startDate"] = start;
        ht["@endDate"] = end;
        dt = dal.GetDataTable("GetAllYearlyAbsences", ht);

        return dt;
    }


    public DataTable GetYearlyAdjustments(Int32 UserID, DateTime start, DateTime end)
    {
        DataTable dt = new DataTable();
        Hashtable ht = new Hashtable();
        ht["@userID"] = UserID;
        ht["@startDate"] = start;
        ht["@endDate"] = end;
        dt = dal.GetDataTable("GetYearlyAdjustments", ht);

        return dt;
    }

    //Office instance only
    //[WebMethod]
    public decimal GetExtratime(Int32 UserID, DateTime start, DateTime end)
    {
        DataTable dt = new DataTable();
        Hashtable ht = new Hashtable();
        TimeSpan Hours = new TimeSpan();
        ht["@userID"] = UserID;
        ht["@startDate"] = start;
        ht["@endDate"] = end;
        dt = dalAvaima.GetDataTable("sp_GetExtraTime", ht);

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            Hours = Hours + Convert.ToDateTime(dt.Rows[i]["ToTime"].ToString()).Subtract(Convert.ToDateTime(dt.Rows[i]["FromTime"].ToString()));
        }

        return (Math.Round(Convert.ToDecimal(Hours.TotalMinutes / 60), 2));
        //return UtilityMethods.getFormatedTimeByMinutes(Hours.TotalMinutes.ToInt32());
    }


    public DataTable FormatDataTagTable(DataTable dt, string type, string Appid, string instanceid = "0")
    {
        DataTable dtHistory = new DataTable();
        dtHistory.Columns.Add("DateTime", typeof(DateTime)); dtHistory.Columns.Add("Day"); dtHistory.Columns.Add("Clock-in"); dtHistory.Columns.Add("Clock-out"); dtHistory.Columns.Add("Tags"); dtHistory.Columns.Add("Time Spent"); dtHistory.Columns.Add("totalminutes");

        DataRow dr = null;
        AvaimaTimeZoneAPI atp = new AvaimaTimeZoneAPI();
        string lastin; string lastout;
        TimeSpan start = new TimeSpan();
        TimeSpan end = new TimeSpan();

        if (type == "attendance")
        {
            if (dt.Rows.Count > 0)
            {
                int parentid = getParent(dt.Rows[0]["p_pid"].ToString()).ToInt32();
                DataTable workinghours = GetWorkingHours(parentid);
                List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(parentid, instanceid);
                //List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(9845, "6752062f-f30f-4abe-b048-6cc2daee6d3d");

                Hashtable ht = new Hashtable();
                ht["@userid"] = parentid;
                DataTable settings = dal.ExecuteSQLQueryData("Select TOP 1 * from schema_6e815b00_6e13_4839_a57d_800a92809f21.ReportSettings where userid = @userid And Chophours = 1", ht);
                DateTime ChophoursFrom = new DateTime();
                if (settings.Rows.Count > 0)
                    ChophoursFrom = Convert.ToDateTime(settings.Rows[0]["ChophoursFrom"]);

                dt = dt.Select("", "lastin ASC").CopyToDataTable();

                foreach (DataRow row in dt.Rows)
                {
                    //lastin = atp.GetTime(row["lastin"].ToString(), Appid);
                    //lastout = atp.GetTime(row["lastout"].ToString(), Appid);
                    lastin = row["lastin"].ToString();
                    lastout = row["lastout"].ToString();

                    //dr = dtHistory.NewRow();
                    //dr["DateTime"] = Convert.ToDateTime(row["FormatedDate"]).ToString("dd-MMM-yyyy");
                    //dr["Day"] = Convert.ToDateTime(row["FormatedDate"]).ToString("dd-MMM-yyyy");
                    //dr["Clock-in"] = Convert.ToDateTime(lastin).ToShortTimeString();
                    //dr["Clock-out"] = (row["lastout"].ToString() != "") ? Convert.ToDateTime(lastout).ToShortTimeString() : " Still Clocked-in ";

                    //dr["Time Spent"] = (row["hours"].ToString() != "") ? row["hours"] : ConvertTimeSpan(DateTime.Now.Subtract(Convert.ToDateTime(lastin)));
                    //dtHistory.Rows.Add(dr);

                    DateTime currenttime = Convert.ToDateTime(App.GetTime(DateTime.UtcNow.TimeOfDay.ToString(), Appid));

                    dr = dtHistory.NewRow();
                    dr["DateTime"] = Convert.ToDateTime(row["FormatedDate"]).ToString("dd-MMM-yyyy");
                    dr["Day"] = Convert.ToDateTime(row["FormatedDate"]).ToString("dd-MMM-yyyy");
                    dr["Clock-in"] = Convert.ToDateTime(lastin).ToShortTimeString();
                    dr["Clock-out"] = (row["lastout"].ToString() != "") ? Convert.ToDateTime(lastout).ToShortTimeString() : " Still Clocked-in ";
                    dr["Time Spent"] = (row["hours"].ToString() != "") ? row["hours"] : ConvertTimeSpan(currenttime.Subtract(Convert.ToDateTime(lastin)));
                    dr["Tags"] = row["Tags"];
                    //Subtract Break Time; if break taken
                    DateTime date1 = Convert.ToDateTime(row["lastin"]);
                    DateTime date2 = (row["lastout"] != DBNull.Value) ? Convert.ToDateTime(row["lastout"]) : currenttime;
                    Double Break = 0;
                    Double totalminutes = (row["minutes"] != DBNull.Value) ? row["minutes"].ToInt32() : date2.Subtract(date1).TotalMinutes;


                    if (workinghours.Rows.Count > 0)
                    {
                        DataRow temp = workinghours.Select("DayTitle = '" + date1.DayOfWeek + "'").FirstOrDefault();
                        List<ExceptionalDay> exDay = exDays.Where(u => u.Date.Date.ToShortDateString() == date1.Date.ToShortDateString()).ToList();

                        if (temp != null && exDay.Count == 0)
                        {
                            start = TimeSpan.Parse(date1.TimeOfDay.ToString());
                            end = TimeSpan.Parse(date2.TimeOfDay.ToString());

                            TimeSpan BreakStart = TimeSpan.Parse(Convert.ToDateTime(temp["BreakStart"]).TimeOfDay.ToString());
                            TimeSpan BreakEnd = TimeSpan.Parse(Convert.ToDateTime(temp["BreakEnd"]).TimeOfDay.ToString());

                            //if ((BreakStart >= start && BreakStart <= end) && (BreakEnd >= start && BreakEnd <= end))
                            //{
                            //    Break = BreakEnd.Subtract(BreakStart).TotalMinutes;
                            //}
                            //else if ((start >= BreakStart) && (start <= BreakEnd))
                            //{
                            //    Break = BreakEnd.Subtract(start).TotalMinutes;
                            //}
                            //else
                            //    Break = 0;

                            //9AM - 6PM
                            if (BreakStart >= start && BreakEnd <= end)
                            {
                                Break = BreakEnd.Subtract(BreakStart).TotalMinutes;
                            }
                            //9AM - 1.30PM
                            else if (BreakStart >= start && (BreakStart <= end && BreakEnd >= end))
                            {
                                Break = end.Subtract(BreakStart).TotalMinutes;
                            }
                            //1PM - 6PM
                            else if ((BreakStart <= start && BreakEnd >= start) && BreakEnd <= end)
                            {
                                Break = BreakEnd.Subtract(start).TotalMinutes;
                            }
                            //1PM - 2PM
                            else if (BreakStart <= start && BreakEnd >= end)
                            {
                                Break = end.Subtract(start).TotalMinutes;
                            }
                            else
                                Break = 0;

                            totalminutes = totalminutes - Break;
                            //End

                            //Subtract Extra Morning Time; If Clocked-in Earlier 
                            TimeSpan DayStart = TimeSpan.Parse(Convert.ToDateTime(temp["Clockin"]).TimeOfDay.ToString());
                            TimeSpan ClockedIn = TimeSpan.Parse(date1.TimeOfDay.ToString());

                            if (ChophoursFrom != DateTime.MinValue)
                            {
                                if ((ClockedIn > Convert.ToDateTime(ChophoursFrom).TimeOfDay) && ClockedIn < DayStart)
                                {
                                    if (totalminutes != 0)
                                    {
                                        totalminutes = totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                                        dr["Clock-in"] = date1.ToShortTimeString();
                                    }
                                }
                            }
                            else
                            {
                                if (ClockedIn < DayStart)
                                {
                                    if (totalminutes != 0)
                                    {
                                        totalminutes = totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                                        dr["Clock-in"] = date1.ToShortTimeString();
                                    }
                                }
                            }

                            //if ((ClockedIn > Convert.ToDateTime(ChophoursFrom).TimeOfDay && ChophoursFrom != DateTime.MinValue) && ClockedIn < DayStart)
                            //{
                            //    if (totalminutes != 0)
                            //    {
                            //        totalminutes = totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                            //        dr["Clock-in"] = date1.ToShortTimeString();
                            //    }
                            //}

                            //if (ClockedIn < DayStart)
                            //{
                            //    if (totalminutes != 0)
                            //    {
                            //        totalminutes = totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                            //        dr["Clock-in"] = date1.ToShortTimeString();
                            //    }
                            //}                            
                            //End

                            dr["Time Spent"] = ConvertTimeSpan(TimeSpan.FromMinutes(totalminutes));
                        }
                        else
                        {
                            if (exDay.Count != 0)
                            {
                                //Subtract Extra Morning Time; If Clocked-in Earlier 
                                TimeSpan DayStart = TimeSpan.Parse(exDay[0].Clockin.TimeOfDay.ToString());
                                TimeSpan ClockedIn = TimeSpan.Parse(date1.TimeOfDay.ToString());

                                if (ChophoursFrom != DateTime.MinValue)
                                {
                                    if ((ClockedIn > Convert.ToDateTime(ChophoursFrom).TimeOfDay) && ClockedIn < DayStart)
                                    {
                                        if (totalminutes != 0)
                                        {
                                            totalminutes = totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                                            dr["Clock-in"] = date1.ToShortTimeString();
                                        }
                                    }
                                }
                                else
                                {
                                    if (ClockedIn < DayStart)
                                    {
                                        if (totalminutes != 0)
                                        {
                                            totalminutes = totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                                            dr["Clock-in"] = date1.ToShortTimeString();
                                        }
                                    }
                                }

                                //if (ClockedIn < DayStart)
                                //{
                                //    if (totalminutes != 0)
                                //    {
                                //        totalminutes = totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                                //        dr["Clock-in"] = date1.ToShortTimeString();
                                //    }
                                //}
                                //End

                                dr["Time Spent"] = ConvertTimeSpan(TimeSpan.FromMinutes(totalminutes));

                                //End
                            }
                        }
                    }
                    dr["totalminutes"] = totalminutes;
                    dtHistory.Rows.Add(dr);
                }
            }
        }

        if (type == "absence")
        {
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    dr = dtHistory.NewRow();
                    dr["DateTime"] = Convert.ToDateTime(row["CrtDate"]).ToString("dd-MMM-yyyy");
                    dr["Day"] = Convert.ToDateTime(row["CrtDate"]).ToString("dd-MMM-yyyy");
                    dr["Clock-in"] = "absent";
                    dr["Clock-out"] = row["Comment"].ToString();
                    dr["Time Spent"] = "";
                    dr["totalminutes"] = "0";
                    dtHistory.Rows.Add(dr);
                }
            }
        }

        return dtHistory;
    }


    public DataTable FormatDataTable(DataTable dt, string type, string Appid, string instanceid = "0")
    {
        DataTable dtHistory = new DataTable();
        dtHistory.Columns.Add("DateTime", typeof(DateTime)); dtHistory.Columns.Add("Day"); dtHistory.Columns.Add("Clock-in"); dtHistory.Columns.Add("Clock-out"); dtHistory.Columns.Add("Time Spent"); dtHistory.Columns.Add("totalminutes");

        DataRow dr = null;
        AvaimaTimeZoneAPI atp = new AvaimaTimeZoneAPI();
        string lastin; string lastout;
        TimeSpan start = new TimeSpan();
        TimeSpan end = new TimeSpan();

        if (type == "attendance")
        {
            if (dt.Rows.Count > 0)
            {
                int parentid = getParent(dt.Rows[0]["p_pid"].ToString()).ToInt32();
                DataTable workinghours = GetWorkingHours(parentid);
                List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(parentid, instanceid);
                //List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(9845, "6752062f-f30f-4abe-b048-6cc2daee6d3d");

                Hashtable ht = new Hashtable();
                ht["@userid"] = parentid;
                DataTable settings = dal.ExecuteSQLQueryData("Select TOP 1 * from schema_6e815b00_6e13_4839_a57d_800a92809f21.ReportSettings where userid = @userid And Chophours = 1", ht);
                DateTime ChophoursFrom = new DateTime();
                if (settings.Rows.Count > 0)
                    ChophoursFrom = Convert.ToDateTime(settings.Rows[0]["ChophoursFrom"]);

                dt = dt.Select("", "lastin ASC").CopyToDataTable();

                foreach (DataRow row in dt.Rows)
                {
                    //lastin = atp.GetTime(row["lastin"].ToString(), Appid);
                    //lastout = atp.GetTime(row["lastout"].ToString(), Appid);
                    lastin = row["lastin"].ToString();
                    lastout = row["lastout"].ToString();

                    //dr = dtHistory.NewRow();
                    //dr["DateTime"] = Convert.ToDateTime(row["FormatedDate"]).ToString("dd-MMM-yyyy");
                    //dr["Day"] = Convert.ToDateTime(row["FormatedDate"]).ToString("dd-MMM-yyyy");
                    //dr["Clock-in"] = Convert.ToDateTime(lastin).ToShortTimeString();
                    //dr["Clock-out"] = (row["lastout"].ToString() != "") ? Convert.ToDateTime(lastout).ToShortTimeString() : " Still Clocked-in ";

                    //dr["Time Spent"] = (row["hours"].ToString() != "") ? row["hours"] : ConvertTimeSpan(DateTime.Now.Subtract(Convert.ToDateTime(lastin)));
                    //dtHistory.Rows.Add(dr);

                    DateTime currenttime = Convert.ToDateTime(App.GetTime(DateTime.UtcNow.TimeOfDay.ToString(), Appid));

                    dr = dtHistory.NewRow();
                    dr["DateTime"] = Convert.ToDateTime(row["FormatedDate"]).ToString("dd-MMM-yyyy");
                    dr["Day"] = Convert.ToDateTime(row["FormatedDate"]).ToString("dd-MMM-yyyy");
                    dr["Clock-in"] = Convert.ToDateTime(lastin).ToShortTimeString();
                    dr["Clock-out"] = (row["lastout"].ToString() != "") ? Convert.ToDateTime(lastout).ToShortTimeString() : " Still Clocked-in ";
                    dr["Time Spent"] = (row["hours"].ToString() != "") ? row["hours"] : ConvertTimeSpan(currenttime.Subtract(Convert.ToDateTime(lastin)));

                    //Subtract Break Time; if break taken
                    DateTime date1 = Convert.ToDateTime(row["lastin"]);
                    DateTime date2 = (row["lastout"] != DBNull.Value) ? Convert.ToDateTime(row["lastout"]) : currenttime;
                    Double Break = 0;
                    Double totalminutes = (row["minutes"] != DBNull.Value) ? row["minutes"].ToInt32() : date2.Subtract(date1).TotalMinutes;


                    if (workinghours.Rows.Count > 0)
                    {
                        DataRow temp = workinghours.Select("DayTitle = '" + date1.DayOfWeek + "'").FirstOrDefault();
                        List<ExceptionalDay> exDay = exDays.Where(u => u.Date.Date.ToShortDateString() == date1.Date.ToShortDateString()).ToList();

                        if (temp != null && exDay.Count == 0)
                        {
                            start = TimeSpan.Parse(date1.TimeOfDay.ToString());
                            end = TimeSpan.Parse(date2.TimeOfDay.ToString());

                            TimeSpan BreakStart = TimeSpan.Parse(Convert.ToDateTime(temp["BreakStart"]).TimeOfDay.ToString());
                            TimeSpan BreakEnd = TimeSpan.Parse(Convert.ToDateTime(temp["BreakEnd"]).TimeOfDay.ToString());

                            //if ((BreakStart >= start && BreakStart <= end) && (BreakEnd >= start && BreakEnd <= end))
                            //{
                            //    Break = BreakEnd.Subtract(BreakStart).TotalMinutes;
                            //}
                            //else if ((start >= BreakStart) && (start <= BreakEnd))
                            //{
                            //    Break = BreakEnd.Subtract(start).TotalMinutes;
                            //}
                            //else
                            //    Break = 0;

                            //9AM - 6PM
                            if (BreakStart >= start && BreakEnd <= end)
                            {
                                Break = BreakEnd.Subtract(BreakStart).TotalMinutes;
                            }
                            //9AM - 1.30PM
                            else if (BreakStart >= start && (BreakStart <= end && BreakEnd >= end))
                            {
                                Break = end.Subtract(BreakStart).TotalMinutes;
                            }
                            //1PM - 6PM
                            else if ((BreakStart <= start && BreakEnd >= start) && BreakEnd <= end)
                            {
                                Break = BreakEnd.Subtract(start).TotalMinutes;
                            }
                            //1PM - 2PM
                            else if (BreakStart <= start && BreakEnd >= end)
                            {
                                Break = end.Subtract(start).TotalMinutes;
                            }
                            else
                                Break = 0;

                            totalminutes = totalminutes - Break;
                            //End

                            //Subtract Extra Morning Time; If Clocked-in Earlier 
                            TimeSpan DayStart = TimeSpan.Parse(Convert.ToDateTime(temp["Clockin"]).TimeOfDay.ToString());
                            TimeSpan ClockedIn = TimeSpan.Parse(date1.TimeOfDay.ToString());

                            if (ChophoursFrom != DateTime.MinValue)
                            {
                                if ((ClockedIn > Convert.ToDateTime(ChophoursFrom).TimeOfDay) && ClockedIn < DayStart)
                                {
                                    if (totalminutes != 0)
                                    {
                                        totalminutes = totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                                        dr["Clock-in"] = date1.ToShortTimeString();
                                    }
                                }
                            }
                            else
                            {
                                if (ClockedIn < DayStart)
                                {
                                    if (totalminutes != 0)
                                    {
                                        totalminutes = totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                                        dr["Clock-in"] = date1.ToShortTimeString();
                                    }
                                }
                            }

                            //if ((ClockedIn > Convert.ToDateTime(ChophoursFrom).TimeOfDay && ChophoursFrom != DateTime.MinValue) && ClockedIn < DayStart)
                            //{
                            //    if (totalminutes != 0)
                            //    {
                            //        totalminutes = totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                            //        dr["Clock-in"] = date1.ToShortTimeString();
                            //    }
                            //}

                            //if (ClockedIn < DayStart)
                            //{
                            //    if (totalminutes != 0)
                            //    {
                            //        totalminutes = totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                            //        dr["Clock-in"] = date1.ToShortTimeString();
                            //    }
                            //}                            
                            //End

                            dr["Time Spent"] = ConvertTimeSpan(TimeSpan.FromMinutes(totalminutes));
                        }
                        else
                        {
                            if (exDay.Count != 0)
                            {
                                //Subtract Extra Morning Time; If Clocked-in Earlier 
                                TimeSpan DayStart = TimeSpan.Parse(exDay[0].Clockin.TimeOfDay.ToString());
                                TimeSpan ClockedIn = TimeSpan.Parse(date1.TimeOfDay.ToString());

                                if (ChophoursFrom != DateTime.MinValue)
                                {
                                    if ((ClockedIn > Convert.ToDateTime(ChophoursFrom).TimeOfDay) && ClockedIn < DayStart)
                                    {
                                        if (totalminutes != 0)
                                        {
                                            totalminutes = totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                                            dr["Clock-in"] = date1.ToShortTimeString();
                                        }
                                    }
                                }
                                else
                                {
                                    if (ClockedIn < DayStart)
                                    {
                                        if (totalminutes != 0)
                                        {
                                            totalminutes = totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                                            dr["Clock-in"] = date1.ToShortTimeString();
                                        }
                                    }
                                }

                                //if (ClockedIn < DayStart)
                                //{
                                //    if (totalminutes != 0)
                                //    {
                                //        totalminutes = totalminutes - DayStart.Subtract(ClockedIn).TotalMinutes;
                                //        dr["Clock-in"] = date1.ToShortTimeString();
                                //    }
                                //}
                                //End

                                dr["Time Spent"] = ConvertTimeSpan(TimeSpan.FromMinutes(totalminutes));

                                //End
                            }
                        }
                    }
                    dr["totalminutes"] = totalminutes;
                    dtHistory.Rows.Add(dr);
                }
            }
        }

        if (type == "absence")
        {
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    dr = dtHistory.NewRow();
                    dr["DateTime"] = Convert.ToDateTime(row["CrtDate"]).ToString("dd-MMM-yyyy");
                    dr["Day"] = Convert.ToDateTime(row["CrtDate"]).ToString("dd-MMM-yyyy");
                    dr["Clock-in"] = "absent";
                    dr["Clock-out"] = (Convert.ToBoolean(row["Adjustment"]) == true) ? "Adjustment: " + row["Comment"].ToString() : row["Comment"].ToString();
                    dr["Time Spent"] = "";
                    dr["totalminutes"] = "0";
                    dtHistory.Rows.Add(dr);
                }
            }
        }

        return dtHistory;
    }
    public string GetUnmarkedDays(DataTable dtHistory, DateTime startDate, DateTime endDate, int userid, int year)
    {
        List<DateTime> existingDates = new List<DateTime>();
        string missingDates = "", query = "";
        Hashtable ht = new Hashtable();
        DataTable setting_days = new DataTable();
        setting_days.Merge(JsonConvert.DeserializeObject<DataTable>(GetUserHolidaysWeekends(userid, "0"))); //weekend
        setting_days.Merge(JsonConvert.DeserializeObject<DataTable>(GetUserHolidaysWeekends(userid, "1", year))); //holidays

        if (dtHistory.Rows.Count > 0)
        {
            foreach (DataRow row in dtHistory.Rows)
            {
                existingDates.Add(Convert.ToDateTime(row["Day"]));
            }
        }

        while (startDate <= endDate)
        {
            if (!existingDates.Contains(startDate.Date))
                if (setting_days.Rows.Count == 0)
                {
                    missingDates += startDate.ToShortDateString() + ",";
                }
                else
                {
                    //query = "title='" + startDate.DayOfWeek + "' OR Convert(date, 'System.String') LIKE '%" + startDate.ToString("M/dd") + "%'";

                    //On server date may appears like "2/5/19 OR 2/05/19 OR 02/05/19" - in order to cater variations we placed OR condition
                    query = "title='" + startDate.DayOfWeek + "' OR (Convert(date, 'System.String') LIKE '%" + startDate.ToString("M/dd") + "%' OR Convert(date, 'System.String') LIKE '%" + startDate.ToString("MM/dd") + "%' OR Convert(date, 'System.String') LIKE '%" + startDate.ToString("M/d") + "%') ";

                    //if (setting_days.Select("title='" + startDate.DayOfWeek + "' OR Convert(date, 'System.String') LIKE '%" + startDate.ToString("M/dd") + "%'").Count() == 0)
                    if (checkDataTable(setting_days, query) == 0)
                        missingDates += startDate.ToShortDateString() + ",";
                }

            //Increment date
            startDate = startDate.AddDays(1);
        }

        return missingDates.Trim(',');
    }
    public DataTable GetYearlyAttendance(Int32 UserID, DateTime start, DateTime end)
    {
        DataTable dt = new DataTable();
        Hashtable ht = new Hashtable();
        ht["@userID"] = UserID;
        ht["@startDate"] = start;
        ht["@endDate"] = end;
        dt = dal.GetDataTable("sp_GetYearlyHistory", ht);

        return dt;
    }

    public DataTable GetYearlyAttendance(string query, Int32 UserID, DateTime start, DateTime end)
    {
        DataTable dt = new DataTable();
        Hashtable ht = new Hashtable();
        ht["@userID"] = UserID;
        ht["@startDate"] = start;
        ht["@endDate"] = end;
        dt = dal.GetDataTable(query, ht);

        return dt;
    }

    /// <summary>
    /// Generates Web page accessible URL.
    /// </summary>
    /// <param name="type">'adduser' OR 'settings' OR 'statistics'</param>
    /// <param name="userid"> accessible User ID</param>
    /// <param name="instanceid">User Instance Id</param>
    /// <param name="email">User Email</param>
    /// <param name="password">User Password</param>
    /// <returns> string Web URL </returns>

    public List<TimeSpan> WorkedHoursAndBreak(int userid, DateTime startDate, DateTime endDate, string Appid, string instanceid)
    {
        Attendance atd = new Attendance();
        DataTable dtHistory = new DataTable();
        dtHistory.Columns.Add("DateTime", typeof(DateTime));
        dtHistory.Columns.Add("Day");
        dtHistory.Columns.Add("Clock-in");
        dtHistory.Columns.Add("Clock-out");
        dtHistory.Columns.Add("Break Time");
        dtHistory.Columns.Add("Time Spent");
        dtHistory.Columns.Add("totalminutes");

        DateTime user_joindate = SP.GetWorkerFirstSignInThisYear(userid.ToString(), DateTime.Now.Year);

        int parentid = atd.getParent(userid.ToString()).ToInt32();
        DataTable workingHours = new DataTable();
        DataRow tempRow = null;
        workingHours = atd.GetWorkingHours(parentid);

        dtHistory.Merge(atd.FormatDataTable(atd.GetYearlyAttendance(userid, startDate, endDate), "attendance", Appid, instanceid));

        DataView view = dtHistory.DefaultView;
        view.Sort = "DateTime ASC";
        dtHistory = view.ToTable();

        TimeSpan total_timespent = new TimeSpan();
        TimeSpan total_break = new TimeSpan();

        if (dtHistory.Rows.Count > 0)
        {
            List<ExceptionalDay> exDays = ExceptionalDay.GetExceptionalDays(atd.getParent(userid.ToString()).ToInt32(), instanceid);

            foreach (DataRow row in dtHistory.Rows)
            {
                string day = Convert.ToDateTime(row["Day"]).ToString("dddd");

                if (workingHours.Rows.Count > 0)
                {
                    tempRow = workingHours.Select("DayTitle = '" + day + "'").FirstOrDefault();
                    List<ExceptionalDay> exDay = exDays.Where(u => u.Date.Date.ToShortDateString() == Convert.ToDateTime(row["Day"]).ToShortDateString()).ToList();

                    if (tempRow != null && exDay.Count == 0)
                    {
                        DateTime bStart = Convert.ToDateTime(tempRow["BreakStart"]);
                        DateTime bEnd = Convert.ToDateTime(tempRow["BreakEnd"]);
                        DateTime clockin = Convert.ToDateTime(row["Clock-in"]);

                        DateTime clockout = Convert.ToDateTime(App.GetTime(DateTime.UtcNow.TimeOfDay.ToString(), Appid)); ;
                        if (!row["Clock-out"].ToString().Contains("Clocked-in"))
                            clockout = Convert.ToDateTime(row["Clock-out"]);

                        //9AM - 6PM
                        if (bStart.TimeOfDay >= clockin.TimeOfDay && bEnd.TimeOfDay <= clockout.TimeOfDay)
                        {
                            total_break += bEnd.Subtract(bStart);
                        }
                        //9AM - 1.30PM
                        else if (bStart.TimeOfDay >= clockin.TimeOfDay && (bStart.TimeOfDay <= clockout.TimeOfDay && bEnd.TimeOfDay >= clockout.TimeOfDay))
                        {
                            total_break += clockout.TimeOfDay.Subtract(bStart.TimeOfDay);
                        }
                        //1PM - 6PM
                        else if ((bStart.TimeOfDay <= clockin.TimeOfDay && bEnd.TimeOfDay >= clockin.TimeOfDay) && bEnd.TimeOfDay <= clockout.TimeOfDay)
                        {
                            total_break += bEnd.TimeOfDay.Subtract(clockin.TimeOfDay);
                        }
                        //1PM - 2PM
                        else if (bStart.TimeOfDay <= clockin.TimeOfDay && bEnd.TimeOfDay >= clockout.TimeOfDay)
                        {
                            total_break += clockout.TimeOfDay.Subtract(clockin.TimeOfDay);
                        }
                    }
                }

                total_timespent += TimeSpan.FromMinutes(Convert.ToDouble(row["totalminutes"]));
            }
        }

        List<TimeSpan> resultSet = new List<TimeSpan>();
        resultSet.Add(total_timespent);
        resultSet.Add(total_break);
        return resultSet;
    }


    public string getParent(string userid)
    {
        Hashtable ht = new Hashtable();
        ht["@ID"] = userid;

        //Check if User has a Parent
        DataTable dt = dal.ExecuteSQLQueryData("Select * from schema_6e815b00_6e13_4839_a57d_800a92809f21.attendence_management where ID = @ID", ht);
        return (dt.Rows[0]["parentid"].ToString() != "0") ? dt.Rows[0]["parentid"].ToString() : userid;

    }

    [WebMethod]
    public bool UserManaged(int userid, string instanceid, string action, bool managed = true)
    {
        bool response = false;
        Hashtable ht;
        ht = new Hashtable();
        ht["@InstanceId"] = instanceid;
        ht["@userid"] = userid;

        if (action == "get")
        {
            response = Convert.ToBoolean(dal.ExecuteSQLQueryData("Select isManaged from attendence_management where ID = @userid AND InstanceId = @InstanceId", ht).Rows[0]["isManaged"]);
        }
        else if (action == "set")
        {
            ht["@isManaged"] = managed;
            response = (dal.ExecuteIUDQuery("UPDATE attendence_management SET isManaged = @isManaged where ID = @userid AND InstanceId = @InstanceId", ht) > 0) ? true : false;
        }

        return response;
    }

    [WebMethod]
    public string SendEmail(string userid, string parentid)
    {
        try
        {
            string SQL = "Select * from cms_user c JOIN schema_6e815b00_6e13_4839_a57d_800a92809f21.tbluserrole r ON c.userid = r.ownerid where r.userid = @userid";
            Hashtable url_ht = new Hashtable();
            url_ht["userid"] = userid;
            DataTable dt = dalAvaima.ExecuteSQLQueryData(SQL, url_ht);
            DataRow dr = dt.Rows[0];
            string useremail = Decrypt(dr["email"].ToString(), true);
            string password = Decrypt(dr["password"].ToString(), true);

            string url = WebURL("myattendance", userid, dr["instanceid"].ToString(), useremail, password, userid);

            AvaimaEmailAPI email = new AvaimaEmailAPI();
            String subject = "Time & Attendance Sign-in Link!";
            StringBuilder body = new StringBuilder();
            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
            body.Append("Hi " + dr["firstname"] + " " + dr["lastname"] + "<br/>");
            body.Append("<p>&nbsp;&nbsp; Here's is your Time & Attendance Sign-in Link. Please click <a href='" + url + "'>here</a>.<br>");
            body.Append("&nbsp;&nbsp; To sign-in using avaima.com, following are your credentials<br>");
            body.Append("&nbsp;&nbsp; <b>Username:</b>" + useremail + "<br/>");
            body.Append("&nbsp;&nbsp; <b>Password:</b>" + password + "</p><br/>");

            body.Append(Helper.AvaimaEmailSignature);
            body.Append("</div>");
            email.send_email(useremail, "Time & Attendance", "", body.ToString(), subject.ToString());

            return "sent";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    public string WelcomeEmail(string userid)
    {
        try
        {
            string SQL = "Select * from cms_user c JOIN schema_6e815b00_6e13_4839_a57d_800a92809f21.tbluserrole r ON c.userid = r.ownerid where r.userid = @userid";
            Hashtable url_ht = new Hashtable();
            url_ht["userid"] = userid;
            DataTable dt = dalAvaima.ExecuteSQLQueryData(SQL, url_ht);
            DataRow dr = dt.Rows[0];
            string useremail = Decrypt(dr["email"].ToString(), true);
            string password = Decrypt(dr["password"].ToString(), true);

            string url = WebURL("myattendance", userid, dr["instanceid"].ToString(), useremail, password, userid);

            AvaimaEmailAPI email = new AvaimaEmailAPI();
            String subject = "Use your Time & Attendance Software";
            StringBuilder body = new StringBuilder();
            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
            body.Append("Hi " + dr["firstname"] + " " + dr["lastname"] + "<br/><br/>");
            body.Append("<p>You have successfully signed up for Time & Attendance and you can instantly access your software application by clicking the link below:<br/> <a href='" + url + "'>Sign In Link!</a>");
            body.Append("<br/><br/>Add your employees and start recording their work activities now. It is very simple and easy and we are here to help if you have any problems.<br/>");
            body.Append("<br/><br/>We look forward to working with you.");

            body.Append(Helper.AvaimaEmailSignature);
            body.Append("</div>");
            email.send_email(useremail, "Time & Attendance", "", body.ToString(), subject.ToString());

            return "sent";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(Description = "Generates Web page accessible URL. Here parameter type = 'home', 'adduser' , 'settings', 'statistics', 'myattendance','absences'")]
    public string WebURL(string type, string userid, string instanceid, string email, string password, string target_userid = "", string startDate = "", string endDate = "")
    {
        string url = "http://www.avaima.com/6e815b00-6e13-4839-a57d-800a92809f21/W/";

        if (type == "home")
        {
            url += "Default.aspx?";
        }
        else if (type == "adduser")
        {
            url += "Users.aspx?";
        }
        else if (type == "settings")
        {
            url += "settings.aspx?";
        }
        else if (type == "statistics")
        {
            url += "Statistics.aspx?";
        }
        else if (type == "myattendance")
        {
            url += "Add_worker.aspx?uid=" + target_userid + "&";
        }
        else if (type == "absences")
        {
            url += "Absence.aspx?uid=" + target_userid + "&";
        }
        else if (type == "unmarked")
        {
            url += "UnmarkedDays.aspx?uid=" + target_userid + "&sdate=" + startDate + "&edate=" + endDate + "&";
        }
        else if (type == "adjustments")
        {
            url += "Absence.aspx?uid=" + target_userid + "&isAdjustment=true&";
        }
        url += "id=" + userid + "&instanceid=" + instanceid + "&e=" + email.Encrypt() + "&p=" + password.Encrypt();

        return url;
    }


    public string Decrypt(string cipherString, bool useHashing)
    {
        byte[] keyArray;
        //get the byte code of the string

        byte[] toEncryptArray = Convert.FromBase64String(cipherString);

        System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
        //Get your key from config file to open the lock!
        string key = "aaaara1982";//(string)settingsReader.GetValue("SecurityKey", typeof(String));

        if (useHashing)
        {
            //if hashing was used get the hash code with regards to your key
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            //release any resource held by the MD5CryptoServiceProvider

            hashmd5.Clear();
        }
        else
        {
            //if hashing was not implemented get the byte code of the key
            keyArray = UTF8Encoding.UTF8.GetBytes(key);
        }

        TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
        //set the secret key for the tripleDES algorithm
        tdes.Key = keyArray;
        //mode of operation. there are other 4 modes. We choose ECB(Electronic code Book)

        tdes.Mode = CipherMode.ECB;
        //padding mode(if any extra byte added)
        tdes.Padding = PaddingMode.PKCS7;

        ICryptoTransform cTransform = tdes.CreateDecryptor();
        byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
        //Release resources held by TripleDes Encryptor                
        tdes.Clear();
        //return the Clear decrypted TEXT
        return UTF8Encoding.UTF8.GetString(resultArray);
    }

    [WebMethod]
    public string getStandardTime(string IpAddress)
    {
        //Default TimeZone
        string timezone = "Asia/Karachi";

        if (IpAddress == "::1" || IpAddress == "")
            IpAddress = "27.255.34.243";
        //IpAddress = "103.43.40.69";
        try
        {
            HttpWebRequest request = WebRequest.Create("http://ip-api.com/json/" + IpAddress) as HttpWebRequest;

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception(String.Format(
                    "Server error (HTTP {0}: {1}).",
                    response.StatusCode,
                    response.StatusDescription));

                string stringResponse = "";
                string[] data;
                using (System.IO.Stream resp = response.GetResponseStream())
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(resp);
                    stringResponse = sr.ReadToEnd();
                    data = stringResponse.Replace('\"', ' ').Replace("{", "").Replace("}", "").Replace("\n", "").Split(',');
                    timezone = data[12].Split(':')[1].Trim();
                }
            }
        }
        catch (Exception ex)
        {
            AvaimaEmailAPI email = new AvaimaEmailAPI();
            StringBuilder body = new StringBuilder();
            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
            body.Append("<p><b>API Used: </b>http://ip-api.com/json/ <br>");
            body.Append("<b>Error Message:</b> " + IpAddress + "<br>");
            body.Append("<b>Error Message:</b> " + ex.Message.ToString() + "<br>");
            body.Append("<b>Detail:</b> " + ex.StackTrace.ToString() + "</p>");
            body.Append(Helper.AvaimaEmailSignature);
            body.Append("</div>");
            try
            {
                //email.send_email(supportEmail, "AVAIMA", "", body.ToString(), "DST API Error");
                email.send_email(DevEmail, "AVAIMA", "", body.ToString(), "DST API Error");
            }
            catch (Exception exception)
            { }
        }

        return timezone;
    }

    public string getTimeZone(string IpAddress)
    {
        if (IpAddress == "::1" || IpAddress == "")
            IpAddress = "27.255.34.243";
        //IpAddress = "103.43.40.69";

        string avaimaTimeZone = "(UTC+05.00) Ekaterinburg, Islamabad, Karachi, Tashkent";
        //Test URL (India): http://gd.geobytes.com/GetCityDetails?ip=103.43.40.69
        HttpWebRequest request = WebRequest.Create("http://gd.geobytes.com/GetCityDetails?fqcn=" + IpAddress) as HttpWebRequest;

        using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
        {
            if (response.StatusCode != HttpStatusCode.OK)
                throw new Exception(String.Format(
                "Server error (HTTP {0}: {1}).",
                response.StatusCode,
                response.StatusDescription));

            string stringResponse = "";
            string[] data;
            using (System.IO.Stream resp = response.GetResponseStream())
            {
                System.IO.StreamReader sr = new System.IO.StreamReader(resp);
                stringResponse = sr.ReadToEnd();
                data = stringResponse.Replace('\"', ' ').Replace("{", "").Replace("}", "").Replace("\n", "").Split(',');
                string offset = data[19].Substring(data[19].IndexOf(':') + 1).Trim();
                if (!string.IsNullOrEmpty(offset))
                    avaimaTimeZone = TimeZone_offset.First(kvp => kvp.Key == offset).Value;
            }
        }

        return avaimaTimeZone;
    }

    public DataTable EmailNotifications_Enabled(DataTable UsersList, int notification_type)
    {
        Hashtable ht = new Hashtable();

        foreach (DataRow dr in UsersList.Rows)
        {
            ht["@UserID"] = dr["ID"].ToInt32();
            ht["@EmailNotifications_ID"] = notification_type;
            int i = dal.GetDataTable("sp_User_EmailNotifications_Enabled", ht).Rows[0]["response"].ToInt32();
            if (i == 0)
            {
                dr["sendemail"] = -1;
                //dr.Delete();
            }
        }

        UsersList.AcceptChanges();
        return UsersList;
    }

    public bool isAdmin(int userid)
    {
        Hashtable ht = new Hashtable();
        ht["@userid"] = userid;
        DataTable dt = dal.ExecuteSQLQueryData("Select * FROM schema_6e815b00_6e13_4839_a57d_800a92809f21.tbluserrole  where userid = @userid and role = 'admin'", ht);

        if (dt.Rows.Count > 0)
            return true;
        else
            return false;
    }


    //[WebMethod]
    //public string Getavaimauser()
    //{
    //    string html = "<table>";
    //    try
    //    {
    //        string SQL = "Select * from cms_user";
    //        Hashtable url_ht = new Hashtable();
    //        DataTable dt = dalAvaima.ExecuteSQLQueryData(SQL, url_ht);

    //        foreach (DataRow dr in dt.Rows) {
    //            if (dr["email"] != DBNull.Value)
    //                if (dr["email"].ToString() != "")
    //                    html += "<tr><td>"+ dr["firstname"]+" "+ dr["lastname"] + "</td><td>" + dr["phone"] + "</td><td>" + Decrypt(dr["email"].ToString(),true) + "</td></tr>";
    //        }

    //        html += "</table>";
    //        return html;
    //    }
    //    catch (Exception ex)
    //    {
    //        return ex.Message;
    //    }
    //}
}

public class WorkingHours
{
    public bool? Active { get; set; }
    public bool? AutoPresent { get; set; }
    public DateTime? Clockin { get; set; }
    public DateTime? Clockout { get; set; }
    public DateTime? BreakStart { get; set; }
    public DateTime? BreakEnd { get; set; }
    public int? TotalWorkingHours { get; set; }
    public DateTime? crtDate { get; set; }
    public string DayTitle { get; set; }
    public string InstanceID { get; set; }
    public int? UserID { get; set; }
    public int WorkHourID { get; set; }

    public WorkingHours() { }

}
