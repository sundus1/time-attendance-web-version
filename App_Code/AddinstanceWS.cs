﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.8943
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

// 
// This source code was auto-generated by wsdl, Version=2.0.50727.3038.
// 


/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Web.Services.WebServiceBindingAttribute(Name="AddinstanceWSSoap", Namespace="http://tempuri.org/")]
public partial class AddinstanceWS : System.Web.Services.Protocols.SoapHttpClientProtocol {
    
    private System.Threading.SendOrPostCallback AddInstanceOperationCompleted;
    
    private System.Threading.SendOrPostCallback GetUserIDOperationCompleted;
    
    private System.Threading.SendOrPostCallback DeleteinstanceOperationCompleted;
    
    /// <remarks/>
    public AddinstanceWS() {
        this.Url = "http://www.avaima.com/AddinstanceWS.asmx";
    }
    
    /// <remarks/>
    public event AddInstanceCompletedEventHandler AddInstanceCompleted;
    
    /// <remarks/>
    public event GetUserIDCompletedEventHandler GetUserIDCompleted;
    
    /// <remarks/>
    public event DeleteinstanceCompletedEventHandler DeleteinstanceCompleted;
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/AddInstance", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    public string AddInstance(string useremail, string appid, string instanceid, string olduserid, string firstname, string Lastname) {
        object[] results = this.Invoke("AddInstance", new object[] {
                    useremail,
                    appid,
                    instanceid,
                    olduserid,
                    firstname,
                    Lastname});
        return ((string)(results[0]));
    }
    
    /// <remarks/>
    public System.IAsyncResult BeginAddInstance(string useremail, string appid, string instanceid, string olduserid, string firstname, string Lastname, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("AddInstance", new object[] {
                    useremail,
                    appid,
                    instanceid,
                    olduserid,
                    firstname,
                    Lastname}, callback, asyncState);
    }
    
    /// <remarks/>
    public string EndAddInstance(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((string)(results[0]));
    }
    
    /// <remarks/>
    public void AddInstanceAsync(string useremail, string appid, string instanceid, string olduserid, string firstname, string Lastname) {
        this.AddInstanceAsync(useremail, appid, instanceid, olduserid, firstname, Lastname, null);
    }
    
    /// <remarks/>
    public void AddInstanceAsync(string useremail, string appid, string instanceid, string olduserid, string firstname, string Lastname, object userState) {
        if ((this.AddInstanceOperationCompleted == null)) {
            this.AddInstanceOperationCompleted = new System.Threading.SendOrPostCallback(this.OnAddInstanceOperationCompleted);
        }
        this.InvokeAsync("AddInstance", new object[] {
                    useremail,
                    appid,
                    instanceid,
                    olduserid,
                    firstname,
                    Lastname}, this.AddInstanceOperationCompleted, userState);
    }
    
    private void OnAddInstanceOperationCompleted(object arg) {
        if ((this.AddInstanceCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.AddInstanceCompleted(this, new AddInstanceCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetUserID", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    public string GetUserID(string olduserid) {
        object[] results = this.Invoke("GetUserID", new object[] {
                    olduserid});
        return ((string)(results[0]));
    }
    
    /// <remarks/>
    public System.IAsyncResult BeginGetUserID(string olduserid, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("GetUserID", new object[] {
                    olduserid}, callback, asyncState);
    }
    
    /// <remarks/>
    public string EndGetUserID(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((string)(results[0]));
    }
    
    /// <remarks/>
    public void GetUserIDAsync(string olduserid) {
        this.GetUserIDAsync(olduserid, null);
    }
    
    /// <remarks/>
    public void GetUserIDAsync(string olduserid, object userState) {
        if ((this.GetUserIDOperationCompleted == null)) {
            this.GetUserIDOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetUserIDOperationCompleted);
        }
        this.InvokeAsync("GetUserID", new object[] {
                    olduserid}, this.GetUserIDOperationCompleted, userState);
    }
    
    private void OnGetUserIDOperationCompleted(object arg) {
        if ((this.GetUserIDCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.GetUserIDCompleted(this, new GetUserIDCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Deleteinstance", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    public void Deleteinstance(string userid, string instanceid, string appid) {
        this.Invoke("Deleteinstance", new object[] {
                    userid,
                    instanceid,
                    appid});
    }
    
    /// <remarks/>
    public System.IAsyncResult BeginDeleteinstance(string userid, string instanceid, string appid, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("Deleteinstance", new object[] {
                    userid,
                    instanceid,
                    appid}, callback, asyncState);
    }
    
    /// <remarks/>
    public void EndDeleteinstance(System.IAsyncResult asyncResult) {
        this.EndInvoke(asyncResult);
    }
    
    /// <remarks/>
    public void DeleteinstanceAsync(string userid, string instanceid, string appid) {
        this.DeleteinstanceAsync(userid, instanceid, appid, null);
    }
    
    /// <remarks/>
    public void DeleteinstanceAsync(string userid, string instanceid, string appid, object userState) {
        if ((this.DeleteinstanceOperationCompleted == null)) {
            this.DeleteinstanceOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDeleteinstanceOperationCompleted);
        }
        this.InvokeAsync("Deleteinstance", new object[] {
                    userid,
                    instanceid,
                    appid}, this.DeleteinstanceOperationCompleted, userState);
    }
    
    private void OnDeleteinstanceOperationCompleted(object arg) {
        if ((this.DeleteinstanceCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.DeleteinstanceCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    public new void CancelAsync(object userState) {
        base.CancelAsync(userState);
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void AddInstanceCompletedEventHandler(object sender, AddInstanceCompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public partial class AddInstanceCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
    
    private object[] results;
    
    internal AddInstanceCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
            base(exception, cancelled, userState) {
        this.results = results;
    }
    
    /// <remarks/>
    public string Result {
        get {
            this.RaiseExceptionIfNecessary();
            return ((string)(this.results[0]));
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void GetUserIDCompletedEventHandler(object sender, GetUserIDCompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public partial class GetUserIDCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
    
    private object[] results;
    
    internal GetUserIDCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
            base(exception, cancelled, userState) {
        this.results = results;
    }
    
    /// <remarks/>
    public string Result {
        get {
            this.RaiseExceptionIfNecessary();
            return ((string)(this.results[0]));
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void DeleteinstanceCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
