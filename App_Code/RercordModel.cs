﻿using MyAttSys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RercordModel
/// </summary>
public class BreakModel
{
    public int? Id { get; set; }
    public string BreakTitle { get; set; }
    public TimeSpan? BreakStartTime { get; set; }
    public TimeSpan? BreakEndTime { get; set; }
    public object IsModified { get; set; }
    public double? Time { get; set; }

}

public class SubRecordModel
{
    public int Id { get; set; }
    public String LoginTime { get; set; }
    public string LoginStatus { get; set; }
    public String LogoutTime { get; set; }
    public string LogoutStatus { get; set; }
    public string InComment { get; set; }
    public string OutComment { get; set; }
    public string Time { get; set; }
    public int RowTime { get; set; }
    public string InLocation { get; set; }
    public string OutLocation { get; set; }
    public DateTime? LoginTimeDT { get; set; }
    public DateTime? LogoutTimeDT { get; set; }
    public String InstanceID { get; set; }
    public TimeSpan? OvertimeAuto
    {
        get
        {
            if (LogoutTimeDT != null && LoginTimeDT != null) {
                //MyAttSys.ExceptionalDay ed = MyAttSys.ExceptionalDay.Find(u => u.DayTitle == LoginTimeDT.Value.DayOfWeek.ToString() && u.InstanceID == InstanceID).ToList().Where(u => LoginTimeDT.Value.Date == u.Date.Value.Date).SingleOrDefault();
                //if (ed != null)
                //{
                //    return new TimeSpan(0);
                //// If WorkedTime is greater than defined working hours
                //TimeSpan? edTimeDiff = ed.Clockout.Value.TimeOfDay - ed.Clockin.Value.TimeOfDay;
                //TimeSpan? loginoutTimeDiff = LogoutTimeDT - LoginTimeDT;
                ////if (edTimeDiff.Value.TotalMinutes < WorkedTime.Value.TotalMinutes)
                ////{
                ////DateTime? clockinTime, clockoutTime;
                //// When clockin is early and clockout is on time or greater
                //if (LoginTimeDT.Value.TimeOfDay < ed.Clockin.Value.TimeOfDay && LogoutTimeDT.Value.TimeOfDay >= ed.Clockout.Value.TimeOfDay)
                //{
                //    // If logout is greater than exceptional days + 15 minutes then return overtime
                //    if (LogoutTimeDT.Value.TimeOfDay > ed.Clockout.Value.AddMinutes(15).TimeOfDay)
                //    { return LogoutTimeDT.Value.TimeOfDay - ed.Clockout.Value.TimeOfDay; }
                //    // If logout is greater than clockout but in 15 minutes timeline
                //    else if (LogoutTimeDT.Value.TimeOfDay > ed.Clockout.Value.TimeOfDay && LogoutTimeDT.Value.TimeOfDay <= ed.Clockout.Value.AddMinutes(15).TimeOfDay)
                //    {
                //        return new TimeSpan(0);
                //    }
                //    else
                //    { return new TimeSpan(0); }
                //}
                //// if clockin is late and clockout is late
                //else if (LoginTimeDT > ed.Clockin && LogoutTimeDT > ed.Clockout)
                //{
                //    TimeSpan? clockInDiff = LoginTimeDT.Value.TimeOfDay - ed.Clockin.Value.TimeOfDay;
                //    // If clockout is within 15 minutes
                //    if (LogoutTimeDT.Value.TimeOfDay <= ed.Clockout.Value.AddMinutes(15).TimeOfDay && LogoutTimeDT.Value.TimeOfDay > ed.Clockout.Value.TimeOfDay)
                //    {
                //        TimeSpan? clockoutDiff = LogoutTimeDT.Value.TimeOfDay - ed.Clockout.Value.TimeOfDay;
                //        if (clockoutDiff.Value.TotalMinutes >= clockInDiff.Value.TotalMinutes)
                //        {
                //            // if clokout diff is greater than clockin diff and clockout within 15 minutes
                //            return new TimeSpan(0);
                //        }
                //        else
                //        {
                //            // if clokout diff is greater than clockin diff and clockout within 15 minutes
                //            // Undertime
                //            return -(clockInDiff - clockoutDiff);
                //        }
                //    }
                //    else if (LogoutTimeDT.Value.TimeOfDay >= ed.Clockout.Value.AddMinutes(15).TimeOfDay)
                //    {
                //        // If clockout is greater than 15 minute plus and clock in is late than return overtime
                //        TimeSpan? clockoutDiff = LogoutTimeDT.Value.TimeOfDay - ed.Clockout.Value.TimeOfDay;
                //        return clockoutDiff - clockInDiff;
                //    }
                //    else
                //    { return new TimeSpan(0); }
                //}
                //else
                //{
                //    return new TimeSpan(0);
                //}

                //else if (edTimeDiff.Value.TotalMinutes > WorkedTime.Value.TotalMinutes)
                //{
                //    if (LoginTimeDT > ed.Clockin && LogoutTimeDT > ed.Clockout)
                //    {

                //    }
                //    else { clockinTime = LoginTimeDT; }
                //}
                //}
                //else
                //{                
                //MyAttSys.WorkingHour wh = MyAttSys.WorkingHour.SingleOrDefault(u => u.DayTitle == LoginTimeDT.Value.DayOfWeek.ToString() && u.Active == true);
                if (HttpContext.Current.Session["UsersWorkingHours"] == null) {
                    avaimaTest0001DB db = new avaimaTest0001DB();
                    List<MyAttSys.WorkingHour> workingHours = db.GetAllWorkingHourOfUser(HttpContext.Current.Session["UserID_"].ToInt32(), InstanceID).ToWorkingHourList();
                    HttpContext.Current.Session["UsersWorkingHours"] = workingHours;
                }

                MyAttSys.WorkingHour wh = ((List<MyAttSys.WorkingHour>)HttpContext.Current.Session["UsersWorkingHours"]).SingleOrDefault(u => u.DayTitle == LoginTimeDT.Value.DayOfWeek.ToString() && u.Active == true);
                if (wh != null) {
                    // If WorkedTime is greater than defined working hours
                    TimeSpan? edTimeDiff = wh.Clockout.Value.TimeOfDay - wh.Clockin.Value.TimeOfDay;
                    TimeSpan? loginoutTimeDiff = LogoutTimeDT - LoginTimeDT;
                    //if (edTimeDiff.Value.TotalMinutes < WorkedTime.Value.TotalMinutes)
                    //{
                    //DateTime? clockinTime, clockoutTime;
                    // When clockin is early and clockout is on time or greater
                    if (LoginTimeDT.Value.TimeOfDay < wh.Clockin.Value.TimeOfDay && LogoutTimeDT.Value.TimeOfDay >= wh.Clockout.Value.TimeOfDay) {
                        // If logout is greater than exceptional days + 15 minutes then return overtime
                        if (LogoutTimeDT.Value.TimeOfDay > wh.Clockout.Value.AddMinutes(15).TimeOfDay) { return LogoutTimeDT.Value.TimeOfDay - wh.Clockout.Value.TimeOfDay; }
                        // If logout is greater than clockout but in 15 minutes timeline
                        else if (LogoutTimeDT.Value.TimeOfDay > wh.Clockout.Value.TimeOfDay && LogoutTimeDT.Value.TimeOfDay <= wh.Clockout.Value.AddMinutes(15).TimeOfDay) {
                            return new TimeSpan(0);
                        }
                        else { return new TimeSpan(0); }
                    }
                    // if clockin is late and clockout is late
                    else if (LoginTimeDT > wh.Clockin && LogoutTimeDT > wh.Clockout) {
                        TimeSpan? clockInDiff = LoginTimeDT.Value.TimeOfDay - wh.Clockin.Value.TimeOfDay;
                        // If clockout is within 15 minutes
                        if (LogoutTimeDT.Value.TimeOfDay <= wh.Clockout.Value.AddMinutes(15).TimeOfDay && LogoutTimeDT.Value.TimeOfDay > wh.Clockout.Value.TimeOfDay) {
                            TimeSpan? clockoutDiff = LogoutTimeDT.Value.TimeOfDay - wh.Clockout.Value.TimeOfDay;
                            if (clockoutDiff.Value.TotalMinutes >= clockInDiff.Value.TotalMinutes) {
                                // if clokout diff is greater than clockin diff and clockout within 15 minutes
                                return new TimeSpan(0);
                            }
                            else {
                                // if clokout diff is greater than clockin diff and clockout within 15 minutes
                                // Undertime
                                return -(clockInDiff - clockoutDiff);
                            }
                        }
                        else if (LogoutTimeDT.Value.TimeOfDay >= wh.Clockout.Value.AddMinutes(15).TimeOfDay) {
                            // If clockout is greater than 15 minute plus and clock in is late than return overtime
                            TimeSpan? clockoutDiff = LogoutTimeDT.Value.TimeOfDay - wh.Clockout.Value.TimeOfDay;
                            return clockoutDiff - clockInDiff;
                        }
                    }
                }
            }
            return new TimeSpan(0);
            //}
            //else
            //{
            //    return new TimeSpan(0);
            //}

        }
    }
    public TimeSpan? WorkedTime
    {
        get
        {
            try { return LogoutTimeDT - LoginTimeDT; }
            catch (Exception) { return DateTime.Now - LoginTimeDT; }

        }
    }
}

public class RecordModel
{
    public DateTime Date { get; set; }
    public List<SubRecordModel> Records = new List<SubRecordModel>();
    public List<BreakModel> Breaks = new List<BreakModel>();
    public List<UnderTime> Undertimes = new List<UnderTime>();
    public List<OverTime> Overtimes = new List<OverTime>();
    private avaimaTest0001DB db = new avaimaTest0001DB();
    public TimeSpan WorkingTimeMinutes { get; set; }
    public MyAttSys.WorkingHour workingHour { get; set; }
    
    public TimeSpan? OvertimeAuto
    {
        get
        {
            if (DayTitle == null && Records.Count > 0) {
                if (Records[0].LogoutTimeDT != null && Records[0].LoginTimeDT != null) {
                    if (Date.Year > 2014) {
                        if (HttpContext.Current.Session["UsersWorkingHours"] == null) {
                            avaimaTest0001DB db = new avaimaTest0001DB();
                            List<MyAttSys.WorkingHour> workingHours = db.GetAllWorkingHourOfUser(UserID, InstanceID).ToWorkingHourList();
                            HttpContext.Current.Session["UsersWorkingHours"] = workingHours;
                        }
                        if (HttpContext.Current.Session["UserID_"] == null) {
                            HttpContext.Current.Session["UserID_"] = UserID;
                        }
                        TimeSpan tsOvertime = TotalWorkedHoursOrgTS - WorkingHours;
                        return tsOvertime;
                    }
                    else {
                        MyAttSys.ExceptionalDay ed = MyAttSys.ExceptionalDay.Find(u => u.DayTitle == Date.DayOfWeek.ToString() && InstanceID == InstanceID).ToList().Where(u => Date.Date == u.Date.Value.Date && u.InstanceID == this.InstanceID).SingleOrDefault();
                        if (ed == null) {
                            if (UtilityMethods.Namazi2014Avaib.Where(u => u.Value == UserID).Count() > 0) {
                                TimeSpan ts = new TimeSpan(0, Records.Sum(u => u.OvertimeAuto.Value.Minutes), 0);
                                return ts.Subtract(new TimeSpan(0, 10, 0));
                            }
                            else {
                                return new TimeSpan(0, Records.Sum(u => u.OvertimeAuto.Value.Minutes), 0);
                            }
                        }
                    }
                }
            }
            return new TimeSpan(0);
        }
    }

    public TimeSpan TotalUnderTime
    {
        get
        {
            TimeSpan tsUnderTime = new TimeSpan(0);
            foreach (UnderTime undertime in Undertimes) {
                if (undertime.FromTime != null && undertime.ToTime != null) {
                    TimeSpan ts = (TimeSpan)(undertime.ToTime - undertime.FromTime);
                    tsUnderTime += ts;
                }
            }
            return tsUnderTime;
        }
    }

    public TimeSpan TotalUnderTimeWithoutLate
    {
        get
        {
            TimeSpan tsUnderTime = new TimeSpan(0);
            foreach (UnderTime undertime in Undertimes) {
                if (undertime.FromTime != null && undertime.ToTime != null) {
                    TimeSpan ts = (TimeSpan)(undertime.ToTime - undertime.FromTime);
                    tsUnderTime += ts;
                }
            }
            return tsUnderTime;
        }
    }

    public TimeSpan TotalOverTime
    {
        get
        {
            TimeSpan tsOverTime = new TimeSpan(0);
            foreach (OverTime Overtime in Overtimes) {
                if (Overtime.FromTime != null && Overtime.ToTime != null) {
                    TimeSpan ts = (TimeSpan)(Overtime.ToTime - Overtime.FromTime);
                    tsOverTime += ts;
                }
            }
            return tsOverTime;
        }
    }

    public string TotalTime
    {
        get
        {
            int RecordTime = (int)Records.Sum(x => x.RowTime);
            int BreakTime = (int)Breaks.Sum(x => x.Time);
            if (workingHour != null) {
                TimeSpan tsWorkHour = workingHour.Clockout.Value.TimeOfDay - workingHour.Clockin.Value.TimeOfDay;
                if (Date.Year > 2014) { return UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsWorkHour.TotalMinutes) - Convert.ToInt32(TotalUnderTime.TotalMinutes)); }

            }
            return UtilityMethods.getFormatedTimeByMinutes(RecordTime);

            //
            //return UtilityMethods.getFormatedTimeByMinutes(RecordTime - BreakTime);
        }
    }
    public string TotalWorkedHours
    {
        get
        {
            int RecordTime = (int)Records.Sum(x => x.RowTime);
            int BreakTime = (int)Breaks.Sum(x => x.Time);

            if (Date.Year <= 2014) { return UtilityMethods.getFormatedTimeByMinutes(RecordTime - BreakTime + Convert.ToInt32(TotalOverTime.TotalMinutes) - Convert.ToInt32(TotalUnderTime.TotalMinutes)); }
            else if (workingHour != null) {
                if (Records.Count > 0) {
                    if (Records[0].LogoutTimeDT != null) {
                        BreakTime = 0;
                        // Estimate break time
                        foreach (BreakModel _break in Breaks) {
                            // If break falls in work hours
                            //if (((_break.BreakStartTime.toServerTimeTs() >= Records[0].LoginTimeDT.Value.TimeOfDay) && (_break.BreakStartTime.toServerTimeTs() <= Records[0].LogoutTimeDT.Value.TimeOfDay))
                            //    && ((_break.BreakEndTime.toServerTimeTs() >= Records[0].LoginTimeDT.Value.TimeOfDay) && (_break.BreakEndTime.toServerTimeTs() <= Records[0].LogoutTimeDT.Value.TimeOfDay)))
                            //{ BreakTime += _break.Time.ToInt32(); }
                            // If user clocked out before break
                            if (Records[0].LogoutTimeDT.Value.TimeOfDay <= _break.BreakStartTime.toServerTimeTs() && Records[0].LoginTimeDT.Value.TimeOfDay <= _break.BreakStartTime.toServerTimeTs()) { BreakTime += 0; }
                            // If user clocked out after break
                            else if (Records[0].LoginTimeDT.Value.TimeOfDay >= _break.BreakEndTime.toServerTimeTs() && Records[0].LogoutTimeDT.Value.TimeOfDay >= _break.BreakEndTime.toServerTimeTs()) { BreakTime += 0; }
                            // If user clocked out between break
                            else if (Records[0].LogoutTimeDT.Value.TimeOfDay < _break.BreakEndTime.toServerTimeTs() && Records[0].LogoutTimeDT.Value.TimeOfDay > _break.BreakStartTime.toServerTimeTs()) { BreakTime += (Records[0].LogoutTimeDT.Value.TimeOfDay - _break.BreakStartTime.toServerTimeTs()).TotalMinutes.ToInt32(); }
                            // If break falls in work hours
                            else { BreakTime += _break.Time.ToInt32(); }
                        }

                        //usman TimeSpan tsResult = Records[0].LogoutTimeDT.Value - Convert.ToDateTime(Records[0].LoginTimeDT.Value.ToShortDateString() + " " + workingHour.Clockin.Value.ToShortTimeString());

                        TimeSpan tsResult = Records[0].LogoutTimeDT.Value - Convert.ToDateTime(Records[0].LoginTimeDT.Value);

                        //usman TimeSpan tsEndResult = tsResult - TotalUnderTime;
                        TimeSpan tsEndResult = tsResult;
                        TimeSpan tsWorkingDuration = (workingHour.Clockout.Value.TimeOfDay - workingHour.Clockin.Value.TimeOfDay);
                        if (tsWorkingDuration < tsEndResult) { tsEndResult = tsWorkingDuration; }

                        //usman return "" + UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsEndResult.TotalMinutes) - BreakTime + Convert.ToInt32(TotalOverTime.TotalMinutes));
                        return "" + UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsEndResult.TotalMinutes) - BreakTime);

                        //return "" + UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsEndResult.TotalMinutes) - BreakTime);
                        //return UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsEndResult.TotalMinutes)) + "." + "wh" + UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsWorkingDuration.TotalMinutes));
                    }
                }
                //TimeSpan tsWorkHour = workingHour.Clockout.Value.TimeOfDay - workingHour.Clockin.Value.TimeOfDay;
                //TimeSpan tsClockHour = new TimeSpan(0, (int)Records.Sum(x => x.RowTime), 0);
                ////return UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(ts.TotalMinutes));
                //int result = Convert.ToInt32(tsWorkHour.TotalMinutes) - BreakTime - Convert.ToInt32(TotalUnderTime.TotalMinutes);
                //TimeSpan tsResult = new TimeSpan(0, result, 0);
                //if (tsResult < tsWorkHour)
                //{
                //    //tsResult = tsResult + (tsClockHour - tsResult);
                //    if (tsResult >  )
                //    {

                //    }
                //}
                //return UtilityMethods.getFormatedTimeByMinutes();
            }
            //usman return "" + UtilityMethods.getFormatedTimeByMinutes(RecordTime - BreakTime + Convert.ToInt32(TotalOverTime.TotalMinutes) - Convert.ToInt32(TotalUnderTime.TotalMinutes));
            return "" + UtilityMethods.getFormatedTimeByMinutes(RecordTime - BreakTime);

        }
    }

    public string TotalWorkedHoursflex
    {
        get
        {
            int RecordTime = (int)Records.Sum(x => x.RowTime);
            int BreakTime = (int)Breaks.Sum(x => x.Time);

            if (Date.Year <= 2014) { return UtilityMethods.getFormatedTimeByMinutes(RecordTime - BreakTime + Convert.ToInt32(TotalOverTime.TotalMinutes) - Convert.ToInt32(TotalUnderTime.TotalMinutes)); }
            else if (Records.Count > 0)
            {
                TimeSpan tsEndResult = new TimeSpan();
                BreakTime = 0;
                foreach (var item in Records)
                {
                    if (item.LogoutTimeDT != null)
                    {                       
                        // Estimate break time
                        foreach (BreakModel _break in Breaks)
                        {
                            // If user clocked out before break
                            if (item.LogoutTimeDT.Value.TimeOfDay <= _break.BreakStartTime.toServerTimeTs() && item.LoginTimeDT.Value.TimeOfDay <= _break.BreakStartTime.toServerTimeTs()) { BreakTime += 0; }
                            // If user clocked out after break
                            else if (item.LoginTimeDT.Value.TimeOfDay >= _break.BreakEndTime.toServerTimeTs() && item.LogoutTimeDT.Value.TimeOfDay >= _break.BreakEndTime.toServerTimeTs()) { BreakTime += 0; }
                            // If user clocked out between break
                            else if (item.LogoutTimeDT.Value.TimeOfDay < _break.BreakEndTime.toServerTimeTs() && item.LogoutTimeDT.Value.TimeOfDay > _break.BreakStartTime.toServerTimeTs()) { BreakTime += (item.LogoutTimeDT.Value.TimeOfDay - _break.BreakStartTime.toServerTimeTs()).TotalMinutes.ToInt32(); }
                            // If break falls in work hours
                            else { BreakTime += _break.Time.ToInt32(); }
                        }

                        TimeSpan tsResult = item.LogoutTimeDT.Value - Convert.ToDateTime(item.LoginTimeDT.Value);

                        //usman TimeSpan tsEndResult = tsResult - TotalUnderTime;
                        tsEndResult += tsResult;

                    }
                }
                return "" + UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsEndResult.TotalMinutes) - BreakTime);

                //return "" + UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsEndResult.TotalMinutes) - BreakTime);
                //return UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsEndResult.TotalMinutes)) + "." + "wh" + UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsWorkingDuration.TotalMinutes));
            }
            return "" + UtilityMethods.getFormatedTimeByMinutes(RecordTime - BreakTime);

        }
    }
    public TimeSpan TotalWorkedHoursTS
    {
        get
        {
            int RecordTime = (int)Records.Sum(x => x.RowTime);
            int BreakTime = (int)Breaks.Sum(x => x.Time);

            if (Date.Year <= 2014) { return new TimeSpan(0, RecordTime - BreakTime + Convert.ToInt32(TotalOverTime.TotalMinutes) - Convert.ToInt32(TotalUnderTime.TotalMinutes), 0); }
            else if (workingHour != null) {
                if (Records.Count > 0) {
                    if (Records[0].LogoutTimeDT != null) {
                        BreakTime = 0;
                        // Estimate break time
                        foreach (BreakModel _break in Breaks) {
                            // If break falls in work hours
                            //if (((_break.BreakStartTime.toServerTimeTs() >= Records[0].LoginTimeDT.Value.TimeOfDay) && (_break.BreakStartTime.toServerTimeTs() <= Records[0].LogoutTimeDT.Value.TimeOfDay))
                            //    && ((_break.BreakEndTime.toServerTimeTs() >= Records[0].LoginTimeDT.Value.TimeOfDay) && (_break.BreakEndTime.toServerTimeTs() <= Records[0].LogoutTimeDT.Value.TimeOfDay)))
                            //{ BreakTime += _break.Time.ToInt32(); }
                            // If user clocked out before break
                            if (Records[0].LogoutTimeDT.Value.TimeOfDay <= _break.BreakStartTime.toServerTimeTs() || Records[0].LoginTimeDT.Value.TimeOfDay >= _break.BreakStartTime.toServerTimeTs()) { BreakTime += 0; }
                            // If user clocked out between break
                            else if (Records[0].LogoutTimeDT.Value.TimeOfDay < _break.BreakEndTime.toServerTimeTs() && Records[0].LogoutTimeDT.Value.TimeOfDay > _break.BreakStartTime.toServerTimeTs()) { BreakTime += (Records[0].LogoutTimeDT.Value.TimeOfDay - _break.BreakStartTime.toServerTimeTs()).TotalMinutes.ToInt32(); }
                            // If break falls in work hours
                            else { BreakTime += _break.Time.ToInt32(); }
                        }

                        TimeSpan tsResult = Records[0].LogoutTimeDT.Value - Convert.ToDateTime(Records[0].LoginTimeDT.Value.ToShortDateString() + " " + workingHour.Clockin.Value.ToShortTimeString());
                        TimeSpan tsEndResult = tsResult - TotalUnderTime;
                        TimeSpan tsWorkingDuration = (workingHour.Clockout.Value.TimeOfDay - workingHour.Clockin.Value.TimeOfDay);
                        if (tsWorkingDuration < tsEndResult) { tsEndResult = tsWorkingDuration; }
                        return new TimeSpan(0, Convert.ToInt32(tsEndResult.TotalMinutes) - BreakTime + Convert.ToInt32(TotalOverTime.TotalMinutes), 0);
                        //return UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsEndResult.TotalMinutes)) + "." + "wh" + UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsWorkingDuration.TotalMinutes));
                    }
                }
                //TimeSpan tsWorkHour = workingHour.Clockout.Value.TimeOfDay - workingHour.Clockin.Value.TimeOfDay;
                //TimeSpan tsClockHour = new TimeSpan(0, (int)Records.Sum(x => x.RowTime), 0);
                ////return UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(ts.TotalMinutes));
                //int result = Convert.ToInt32(tsWorkHour.TotalMinutes) - BreakTime - Convert.ToInt32(TotalUnderTime.TotalMinutes);
                //TimeSpan tsResult = new TimeSpan(0, result, 0);
                //if (tsResult < tsWorkHour)
                //{
                //    //tsResult = tsResult + (tsClockHour - tsResult);
                //    if (tsResult >  )
                //    {

                //    }
                //}
                //return UtilityMethods.getFormatedTimeByMinutes();
            }
            return new TimeSpan(0, RecordTime - BreakTime + Convert.ToInt32(TotalOverTime.TotalMinutes) - Convert.ToInt32(TotalUnderTime.TotalMinutes), 0);


        }
    }

    public TimeSpan TotalWorkedHoursOrgTS
    {
        get
        {
            int RecordTime = (int)Records.Sum(x => x.RowTime);
            int BreakTime = (int)Breaks.Sum(x => x.Time);

            if (Date.Year <= 2014) { return new TimeSpan(0, RecordTime - BreakTime + Convert.ToInt32(TotalOverTime.TotalMinutes) - Convert.ToInt32(TotalUnderTime.TotalMinutes), 0); }
            else if (workingHour != null) {
                if (Records.Count > 0) {
                    if (Records[0].LogoutTimeDT != null) {
                        BreakTime = 0;
                        // Estimate break time
                        foreach (BreakModel _break in Breaks) {
                            // If break falls in work hours
                            //if (((_break.BreakStartTime.toServerTimeTs() >= Records[0].LoginTimeDT.Value.TimeOfDay) && (_break.BreakStartTime.toServerTimeTs() <= Records[0].LogoutTimeDT.Value.TimeOfDay))
                            //    && ((_break.BreakEndTime.toServerTimeTs() >= Records[0].LoginTimeDT.Value.TimeOfDay) && (_break.BreakEndTime.toServerTimeTs() <= Records[0].LogoutTimeDT.Value.TimeOfDay)))
                            //{ BreakTime += _break.Time.ToInt32(); }
                            // If user clocked out before break
                            if (Records[0].LogoutTimeDT.Value.TimeOfDay <= _break.BreakStartTime.toServerTimeTs() || Records[0].LoginTimeDT.Value.TimeOfDay >= _break.BreakStartTime.toServerTimeTs()) { BreakTime += 0; }
                            // If user clocked out between break
                            else if (Records[0].LogoutTimeDT.Value.TimeOfDay < _break.BreakEndTime.toServerTimeTs() && Records[0].LogoutTimeDT.Value.TimeOfDay > _break.BreakStartTime.toServerTimeTs()) { BreakTime += (Records[0].LogoutTimeDT.Value.TimeOfDay - _break.BreakStartTime.toServerTimeTs()).TotalMinutes.ToInt32(); }
                            // If break falls in work hours
                            else { BreakTime += _break.Time.ToInt32(); }
                        }

                        TimeSpan tsResult = Records[0].LogoutTimeDT.Value - Records[0].LoginTimeDT.Value;
                        return new TimeSpan(0, Convert.ToInt32(tsResult.TotalMinutes) - BreakTime + Convert.ToInt32(TotalOverTime.TotalMinutes), 0);
                        //return UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsEndResult.TotalMinutes)) + "." + "wh" + UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsWorkingDuration.TotalMinutes));
                    }
                }
                //TimeSpan tsWorkHour = workingHour.Clockout.Value.TimeOfDay - workingHour.Clockin.Value.TimeOfDay;
                //TimeSpan tsClockHour = new TimeSpan(0, (int)Records.Sum(x => x.RowTime), 0);
                ////return UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(ts.TotalMinutes));
                //int result = Convert.ToInt32(tsWorkHour.TotalMinutes) - BreakTime - Convert.ToInt32(TotalUnderTime.TotalMinutes);
                //TimeSpan tsResult = new TimeSpan(0, result, 0);
                //if (tsResult < tsWorkHour)
                //{
                //    //tsResult = tsResult + (tsClockHour - tsResult);
                //    if (tsResult >  )
                //    {

                //    }
                //}
                //return UtilityMethods.getFormatedTimeByMinutes();
            }
            return new TimeSpan(0, RecordTime - BreakTime + Convert.ToInt32(TotalOverTime.TotalMinutes) - Convert.ToInt32(TotalUnderTime.TotalMinutes), 0);


        }
    }

    public string TotalBreak
    {
        get
        {
            int BreakTime = (int)Breaks.Sum(x => x.Time);
            if (Date.Year > 2014 && workingHour != null) {
                if (Records.Count > 0) {
                    if (Records[0].LogoutTimeDT != null) {
                        BreakTime = 0;
                        // Estimate break time
                        foreach (BreakModel _break in Breaks) {
                            // If user clocked out before break
                            if (Records[0].LogoutTimeDT.Value.TimeOfDay <= _break.BreakStartTime.toServerTimeTs() || Records[0].LoginTimeDT.Value.TimeOfDay >= _break.BreakStartTime.toServerTimeTs()) { BreakTime += 0; }
                            // If user clocked out between break
                            else if (Records[0].LogoutTimeDT.Value.TimeOfDay < _break.BreakEndTime.toServerTimeTs() && Records[0].LogoutTimeDT.Value.TimeOfDay > _break.BreakStartTime.toServerTimeTs()) { BreakTime += (Records[0].LogoutTimeDT.Value.TimeOfDay - _break.BreakStartTime.toServerTimeTs()).TotalMinutes.ToInt32(); }
                            // If break falls in work hours
                            else { BreakTime += _break.Time.ToInt32(); }
                        }
                        return "" + UtilityMethods.getFormatedTimeByMinutes(BreakTime);
                        //return UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsEndResult.TotalMinutes)) + "." + "wh" + UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsWorkingDuration.TotalMinutes));
                    }
                }
            }
            else {
                if (Records.Count > 0) {
                    BreakTime = 0;
                    foreach (SubRecordModel subRecord in Records) {
                        if (subRecord.LogoutTimeDT != null) {
                            // Estimate break time
                            foreach (BreakModel _break in Breaks) {
                                // If user clocked out before break
                                if (subRecord.LogoutTimeDT.Value.TimeOfDay <= _break.BreakStartTime.toServerTimeTs() || subRecord.LoginTimeDT.Value.TimeOfDay >= _break.BreakStartTime.toServerTimeTs()) { BreakTime += 0; }
                                // If user clocked out between break
                                else if (subRecord.LogoutTimeDT.Value.TimeOfDay < _break.BreakEndTime.toServerTimeTs() && subRecord.LogoutTimeDT.Value.TimeOfDay > _break.BreakStartTime.toServerTimeTs()) { BreakTime += (subRecord.LogoutTimeDT.Value.TimeOfDay - _break.BreakStartTime.toServerTimeTs()).TotalMinutes.ToInt32(); }
                                // If break falls in work hours
                                else { BreakTime += _break.Time.ToInt32(); }
                            }
                            return "" + UtilityMethods.getFormatedTimeByMinutes(BreakTime);
                            //return UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsEndResult.TotalMinutes)) + "." + "wh" + UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsWorkingDuration.TotalMinutes));
                        }
                    }

                }
            }
            return UtilityMethods.getFormatedTimeByMinutes(BreakTime);
            //return UtilityMethods.getFormatedTimeByMinutes(RecordTime - BreakTime);
        }
    }

    public TimeSpan TotalBreakTS
    {
        get
        {
            int BreakTime = (int)Breaks.Sum(x => x.Time);
            if (Date.Year > 2014 && workingHour != null) {
                if (Records.Count > 0) {
                    if (Records[0].LogoutTimeDT != null) {
                        BreakTime = 0;
                        // Estimate break time
                        foreach (BreakModel _break in Breaks) {
                            // If user clocked out before break
                            if (Records[0].LogoutTimeDT.Value.TimeOfDay <= _break.BreakStartTime.toServerTimeTs() || Records[0].LoginTimeDT.Value.TimeOfDay >= _break.BreakStartTime.toServerTimeTs()) { BreakTime += 0; }
                            // If user clocked out between break
                            else if (Records[0].LogoutTimeDT.Value.TimeOfDay < _break.BreakEndTime.toServerTimeTs() && Records[0].LogoutTimeDT.Value.TimeOfDay > _break.BreakStartTime.toServerTimeTs()) { BreakTime += (Records[0].LogoutTimeDT.Value.TimeOfDay - _break.BreakStartTime.toServerTimeTs()).TotalMinutes.ToInt32(); }
                            // If break falls in work hours
                            else { BreakTime += _break.Time.ToInt32(); }
                        }
                        return new TimeSpan(0, BreakTime, 0);
                        //return UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsEndResult.TotalMinutes)) + "." + "wh" + UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsWorkingDuration.TotalMinutes));
                    }
                }
            }
            else {
                if (Records.Count > 0) {
                    BreakTime = 0;
                    foreach (SubRecordModel subRecord in Records) {
                        if (subRecord.LogoutTimeDT != null) {
                            // Estimate break time
                            foreach (BreakModel _break in Breaks) {
                                // If user clocked out before break
                                if (subRecord.LogoutTimeDT.Value.TimeOfDay <= _break.BreakStartTime.toServerTimeTs() || subRecord.LoginTimeDT.Value.TimeOfDay >= _break.BreakStartTime.toServerTimeTs()) { BreakTime += 0; }
                                // If user clocked out between break
                                else if (subRecord.LogoutTimeDT.Value.TimeOfDay < _break.BreakEndTime.toServerTimeTs() && subRecord.LogoutTimeDT.Value.TimeOfDay > _break.BreakStartTime.toServerTimeTs()) { BreakTime += (subRecord.LogoutTimeDT.Value.TimeOfDay - _break.BreakStartTime.toServerTimeTs()).TotalMinutes.ToInt32(); }
                                // If break falls in work hours
                                else { BreakTime += _break.Time.ToInt32(); }
                            }
                            return new TimeSpan(0, BreakTime, 0);
                            //return UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsEndResult.TotalMinutes)) + "." + "wh" + UtilityMethods.getFormatedTimeByMinutes(Convert.ToInt32(tsWorkingDuration.TotalMinutes));
                        }
                    }

                }
            }
            return new TimeSpan(0, BreakTime, 0);
            //return UtilityMethods.getFormatedTimeByMinutes(RecordTime - BreakTime);
        }
    }

    public TimeSpan TotalBreakShouldBeTS
    {
        get
        {
            int BreakTime = (int)Breaks.Sum(x => x.Time);
            return new TimeSpan(0, BreakTime, 0);
            //return UtilityMethods.getFormatedTimeByMinutes(RecordTime - BreakTime);
        }
    }

    /// <summary>
    /// Get Total working hours that should be completed by user
    /// </summary>
    public TimeSpan WorkingHours
    {
        get
        {
            db = new avaimaTest0001DB();
            int? iwh;
            try {
                iwh = db.GetTodayUserWorkHours(UserID, InstanceID, Date.Date, Date.Date.DayOfWeek.ToString()).ExecuteScalar<int>();
                if (iwh != null) { return new TimeSpan((int)iwh, 0, 0); }
            }
            catch (Exception) { return new TimeSpan(0); }
            return new TimeSpan(0);
        }
    }
    public int? DayType { get; set; }
    public string DayTitle { get; set; }

    public int UserID { get; set; }

    public string InstanceID { get; set; }
}