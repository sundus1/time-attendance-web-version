﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for contractwebservice
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class contractwebservice : System.Web.Services.WebService
{

    public contractwebservice()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public void Emailcontract()
    {
        try
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                
                
                con.Open();
                using (SqlCommand cmd = new SqlCommand("getallcontractorforemail", con))
                {
                    //if (dt.Columns[""].ToString() == "")
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.SelectCommand.CommandType = CommandType.StoredProcedure;
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            AvaimaEmailAPI objemail = new AvaimaEmailAPI();
                            string message = "";
                            string title = "Contract Reminder alert.";

                            List<string> emailto = new List<string>();
                            
                            MailMessage mail = new MailMessage();
                            mail.From = new MailAddress("alimumtaz@gmail.com");
                            //mail.To.Add("alimumtaz108@gmail.com");
                            mail.Subject = "Contract Reminder.";
                            mail.Body = "Test content";
                            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                            smtp.Credentials = new NetworkCredential("qcvc.test@gmail.com", "qcvctest$10");

                            AvaimaUserProfile obj = new AvaimaUserProfile();
                            DataTable dtcontractors = obj.getcompanyuser(dr["p_contID"].ToString());
                            if (dtcontractors.Rows.Count > 0)
                            {
                                foreach (DataRow drcontract in dtcontractors.Rows)
                                {

                                    switch (dr["remdate"].ToString())
                                    {
                                        case "30":
                                            //getemailbody
                                            DataTable dtemail30 = Getemailformat(Convert.ToString(dr["p_contID"]), "30");
                                            if (dtemail30.Rows.Count > 0)
                                            {
                                                message = dtemail30.Rows[0]["_message"].ToString();
                                                mail.Body = dtemail30.Rows[0]["_message"].ToString();
                                            }
                                            else
                                            {
                                                message = "Reminder Mail for 30 days for contract end";
                                                mail.Body = "Reminder Mail for 30 days for contract end";
                                            }

                                            emailto.Add(drcontract["email"].ToString());
                                            mail.To.Add(drcontract["email"].ToString());

                                            break;
                                        case "15":
                                            //getemailbody
                                            DataTable dtemail15 = Getemailformat(Convert.ToString(dr["p_contID"]), "15");
                                            if (dtemail15.Rows.Count > 0)
                                            {
                                                message = dtemail15.Rows[0]["_message"].ToString();
                                                mail.Body = dtemail15.Rows[0]["_message"].ToString();
                                            }
                                            else
                                            {
                                                mail.Body = "Reminder Mail for 15 days for contract end";
                                                message= "Reminder Mail for 15 days for contract end";
                                            }

                                            emailto.Add(drcontract["email"].ToString());
                                            mail.To.Add(drcontract["email"].ToString());

                                            break;
                                        case "7":
                                            //getemailbody
                                            DataTable dtemail7 = Getemailformat(Convert.ToString(dr["p_contID"]), "7");
                                            if (dtemail7.Rows.Count > 0)
                                            {
                                                message = dtemail7.Rows[0]["_message"].ToString();
                                                mail.Body = dtemail7.Rows[0]["_message"].ToString();
                                            }
                                            else
                                            {
                                                message = "Reminder Mail for 7 days for contract end";
                                                mail.Body = "Reminder Mail for 7 days for contract end";
                                            }
                                            emailto.Add(drcontract["email"].ToString());                                       
                                            mail.To.Add(drcontract["email"].ToString());

                                            break;
                                        case "1":
                                            //getemailbody
                                            DataTable dtemail1 = Getemailformat(Convert.ToString(dr["p_contID"]), "1");
                                            if (dtemail1.Rows.Count > 0)
                                            {
                                                message = dtemail1.Rows[0]["_message"].ToString();
                                                mail.Body = dtemail1.Rows[0]["_message"].ToString();
                                            }
                                            else
                                            {
                                                message = "Reminder Mail for 1 days for contract end";
                                                mail.Body = "Reminder Mail for 1 days for contract end";
                                            }
                                            emailto.Add(drcontract["email"].ToString());                                            
                                            mail.To.Add(drcontract["email"].ToString());

                                            break;
                                        case "0":
                                            //getemailbody
                                            DataTable dtemail0 = Getemailformat(Convert.ToString(dr["p_contID"]), "0");
                                            if (dtemail0.Rows.Count > 0)
                                            {
                                                message= dtemail0.Rows[0]["_message"].ToString();
                                                mail.Body = dtemail0.Rows[0]["_message"].ToString();
                                            }
                                            else
                                            {
                                                message = "Reminder Mail for 1 days for contract end";
                                                mail.Body = "Reminder Mail for 1 days for contract end";
                                            }
                                            emailto.Add(drcontract["email"].ToString());
                                            mail.To.Add(drcontract["email"].ToString());

                                            break;
                                        default:
                                            //if (Convert.ToInt32(dr["remdate"]) > 0 && Convert.ToInt32(dr["remdate"]) < 31)
                                            //{
                                            //    mail.Body = "Reminder Mail for " + dr["remdate"] + " for contract end";

                                            //}
                                            break;

                                    }
                                }
                            }
                            using (SqlCommand cmdemailtras = new SqlCommand("insertemailtrans", con))
                            {

                                cmdemailtras.CommandType = CommandType.StoredProcedure;
                                cmdemailtras.Parameters.AddWithValue("@id", dr["p_contID"]);
                                cmdemailtras.ExecuteNonQuery();
                            }
                            //if (mail.To.Count > 0)
                            if(emailto.Count>0)
                            {
                                string email = string.Join(",", emailto.ToArray());
                                
                                objemail.send_email(email,"Contract Management","",message,title);
                                smtp.EnableSsl = true;
                                smtp.Send(mail);

                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataTable Getemailformat(string id, string time)
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("getmailformatbyid", con))
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@time", time);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }
        }
        return dt;
    }
}