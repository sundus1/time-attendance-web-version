﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for holidayReminder
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class holidayReminder : System.Web.Services.WebService {

    public holidayReminder () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public void SendHolidyReminder() {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand
                (@"SELECT h.title, h.date, h.day, h.type, h.everyyear, a.* FROM tblHolidays h INNER JOIN tbluserrole a
                   ON h.instanceId = a.instanceId
                   WHERE ((h.date = @tdate and h.type = 1) or
                   (DATEPART(day,h.date) = DATEPART(day,@tdate) and DATEPART(month,h.date) = DATEPART(month,@tdate) and h.type = 1 and h.everyyear = 1))
                ", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@tdate", DateTime.Now.AddDays(-1));
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    AvaimaUserProfile obj1 = new AvaimaUserProfile();
                    AvaimaEmailAPI email = new AvaimaEmailAPI();
                    foreach(DataRow dr in dt.Rows)
                    {
                        string messagebody = "";
                        DataTable dtcontractors = obj1.getuserbyInstanceId(dr["userid"].ToString(), dr["instanceId"].ToString());
                        if (dtcontractors.Rows.Count > 0)
                        {
                            if (dr["title"].ToString().ToLower() == "not assigned")
                            {
                                messagebody = string.Format("Dear {0} {1},\r\nTomorrow {2} is a holiday.\r\nThank you",
                                                            dtcontractors.Rows[0]["FirstName"].ToString(),
                                                            dtcontractors.Rows[0]["LastName"].ToString(),
                                                            dr["date"].ToString());
                            }
                            else
                            {
                                messagebody =
                                    string.Format("Dear {0} {1},\r\nTomorrow {2} is a holiday for {3}.\r\nThank you",
                                                  dtcontractors.Rows[0]["FirstName"].ToString(),
                                                  dtcontractors.Rows[0]["LastName"].ToString(), dr["date"].ToString(),
                                                  dr["title"].ToString());
                            }
                            email.send_email(dtcontractors.Rows[0]["email"].ToString(), "Attendance Management", "",
                                             messagebody, "Holiday Alert");
                        }
                    }
                }
            }
        }
    }
    
}
