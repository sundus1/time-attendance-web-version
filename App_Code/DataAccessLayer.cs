﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for DataAccessLayer
/// </summary>
public class DataAccessLayer
{
    SqlConnection con;
    // SqlConnection avaimaLogin;
    public DataAccessLayer(bool avaima)
    {
        //
        // TODO: Add constructor logic here
        //
        if (avaima == true)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["aviamaConn"].ConnectionString);
        }
        else
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString);
        }
    }
    public DataTable GetDataTable(string spName, Hashtable hts)
    {
        DataTable dt = new DataTable();
        SqlCommand cmd = new SqlCommand(spName, con);
        ConnectionClose(cmd);
        con.Open();
        foreach (DictionaryEntry de in hts)
        {
            cmd.Parameters.Add(new SqlParameter(de.Key.ToString(), de.Value.ToString()));
        }
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandTimeout = 0;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        return dt;
    }
    public bool ExecuteNonQuery(string spName, Hashtable hts)
    {
        SqlCommand cmd = new SqlCommand(spName, con);
        ConnectionClose(cmd);
        con.Open();
        foreach (DictionaryEntry de in hts)
        {
            cmd.Parameters.Add(new SqlParameter(de.Key.ToString(), de.Value.ToString()));
        }
        cmd.CommandType = CommandType.StoredProcedure;
        if (cmd.ExecuteNonQuery() > 0)
            return true;
        else
            return false;
    }

    public bool ExecuteScalar(string spName, Hashtable hts)
    {
        SqlCommand cmd = new SqlCommand(spName, con);
        ConnectionClose(cmd);
        con.Open();
        foreach (DictionaryEntry de in hts)
        {
            cmd.Parameters.Add(new SqlParameter(de.Key.ToString(), de.Value.ToString()));
        }
        cmd.CommandType = CommandType.StoredProcedure;
        if ((Int32)cmd.ExecuteScalar() > 0)
            return true;
        else
            return false;
    }

    public int ExecuteQuery(string spName, Hashtable hts)
    {
        SqlCommand cmd = new SqlCommand(spName, con);
        ConnectionClose(cmd);
        con.Open();
        foreach (DictionaryEntry de in hts)
        {
            cmd.Parameters.Add(new SqlParameter(de.Key.ToString(), de.Value.ToString()));
        }
        cmd.CommandType = CommandType.StoredProcedure;
        //Int32 ReturnId = (Int32)cmd.ExecuteScalar();
        int ReturnId = (int)(decimal)cmd.ExecuteScalar();

        if (ReturnId > 0)           
            return ReturnId;
        else
            return 0;
    }

    public int ExecuteSQLQuery(string query, Hashtable hts)
    {
        SqlCommand cmd = new SqlCommand(query, con);
        ConnectionClose(cmd);
        con.Open();
        foreach (DictionaryEntry de in hts)
        {
            cmd.Parameters.Add(new SqlParameter(de.Key.ToString(), de.Value.ToString()));
        }

        if ((int)cmd.ExecuteScalar() > 0)
            return (int)cmd.ExecuteScalar();
        else
            return 0;
    }

    public DataTable ExecuteSQLQueryData(string query, Hashtable hts)
    {
        DataTable dt = new DataTable();
        SqlCommand cmd = new SqlCommand(query, con);
        ConnectionClose(cmd);
        con.Open();
        foreach (DictionaryEntry de in hts)
        {
            cmd.Parameters.Add(new SqlParameter(de.Key.ToString(), de.Value.ToString()));
        }

        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        return dt;
    }


    public int ExecuteIUDQuery(string query, Hashtable hts)
    {
        SqlCommand cmd = new SqlCommand(query, con);
        ConnectionClose(cmd);
        con.Open();
        foreach (DictionaryEntry de in hts)
        {
            cmd.Parameters.Add(new SqlParameter(de.Key.ToString(), de.Value.ToString()));
        }

        if (cmd.ExecuteNonQuery() > 0)
            return cmd.ExecuteNonQuery();
        else
            return 0;
    }


    public void ConnectionClose(SqlCommand cmd)
    {
        if (cmd.Connection.State == ConnectionState.Open)
        {
            cmd.Connection.Close();
        }

    }

}