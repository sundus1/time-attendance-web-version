﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for App
/// </summary>
public static class App
{
    public static String ApplicationID { get { return "714fcd17-288b-4725-a0c2-7641c997eda4"; } }
    public static String InstanceID { get { return "de368dcf-3084-4159-8d5c-4637c1238902"; } }

    /// <summary>
    /// Get AVA_UserID according to user role
    /// Returns current AVA_UserID on AVAIMA (server)
    /// Returns Hardcoded AVA_UserID on local 
    /// </summary>
    public static String GetAVAIMAUserID(Roles role)
    {
        AddinstanceWS objinst = new AddinstanceWS();
        String AVA_UserID = "";
        String localAVA_UserID;
        switch (role)
        {
            case Roles.SUPER_ADMIN:
                // Sir. Fahad
                localAVA_UserID = "e8a92994-352a-4319-838d-882e8dc1bdeb";
                break;
            case Roles.ADMIN:
                // Muhammad Faizan
                localAVA_UserID = "632351f2-6bb1-4e99-845c-fb2e75341d59";
                break;
            case Roles.USER:
                // User
                localAVA_UserID = "522af1b9-2675-482e-a5be-168d5d7b59a2";
                break;
            default:
                localAVA_UserID = "";
                break;
        }

        try
        {
            // Get AVA_UserID from AVAIMA
            AVA_UserID = objinst.GetUserID(HttpContext.Current.User.Identity.Name);
            if (AVA_UserID == "")
            {
                AVA_UserID = localAVA_UserID; // SignUp in Avaima Account and use your AVA_UserID
            }
        }
        catch (Exception)
        {
            AVA_UserID = localAVA_UserID; // SignUp in Avaima Account and use your AVA_UserID
        }

        return AVA_UserID;
    }

    public enum Roles
    {
        SUPER_ADMIN = 1,
        ADMIN,
        USER
    }

    public static string GetDate(string DateTime, string ownerID)
    {
        if (App.IsLocal)
        {
            if (!String.IsNullOrEmpty(DateTime)) { return Convert.ToDateTime(DateTime).ToString(); }
            else { return ""; }
        }
        else
        {
            AvaimaTimeZoneAPI at = new AvaimaTimeZoneAPI();
            return at.GetDate(DateTime, ownerID);
        }
    }

    public static string GetTime(string DateTime, string ownerID)
    {
        if (App.IsLocal)
        {
            if (!String.IsNullOrEmpty(DateTime)) { return Convert.ToDateTime(DateTime).ToString(); }
            else { return ""; }
        }
        else
        {
            AvaimaTimeZoneAPI at = new AvaimaTimeZoneAPI();
            return at.GetTime(DateTime, ownerID);
        }
    }

    public static string GetDateTime(string DateTime1, string ownerID)
    {
        if (App.IsLocal)
        {
            if (!String.IsNullOrEmpty(DateTime1)) { return Convert.ToDateTime(DateTime1).ToString(); }
            else { return ""; }
        }
        else
        {
            AvaimaTimeZoneAPI at = new AvaimaTimeZoneAPI();
            if (!at.GetDate(DateTime1, ownerID).Contains("Today"))
            {
                return at.GetDate(DateTime1, ownerID) + " " + at.GetTime(DateTime1, ownerID);
            }
            else
            {
                return DateTime.Now.Date.ToShortDateString() + " " + at.GetTime(DateTime1, ownerID);
            }
        }
    }

    public static bool IsLocal
    {
        get;
        set;
    }

    /// <summary>
    /// 15 minutes
    /// </summary>
    public static int LateMinutes
    {
        get
        {
            return 15;
        }
    }
}