﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Statistics : System.Web.UI.Page
{
    int userId = 0;
    string instanceID = "";
    Attendance atd = new Attendance();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
            userId = Convert.ToInt16(Request.QueryString["id"]);
        }
        if (Request.QueryString["instanceid"] != null)
        {
            instanceID = Request.QueryString["instanceid"];
        }

        if (!Page.IsPostBack)
        {
            LoadData();
        }
    }

    public void LoadData()
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_GetUsers", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", userId);
                cmd.Parameters.AddWithValue("@instanceid", instanceID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    ddlUsers.DataValueField = "ID";
                    ddlUsers.DataTextField = "Title";
                    ddlUsers.DataSource = dt;
                    ddlUsers.DataBind();

                    ddlUsers.Items.Insert(0, new ListItem("Select User", "-1"));
                    //     ddlUsers.Items.Insert(1, new ListItem("Select User", userId.ToString()));

                }
                else
                {

                }
            }
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
    //    int id = ddlUsers.SelectedValue.ToInt32();
    //    int hours = txtHours.Text.ToInt32();
    //    int year = DateTime.Now.Year;

    //    Hashtable stats = JsonConvert.DeserializeObject<Hashtable>(atd.CalculateStatistics(hours, year, id, instanceID));

    //    string table = "<h3>Report</h3><table>";
    //    table += "<tr><td>Absences:  </td><td>"+stats["TotalAbsences"].ToString()+"</td></tr>";
    //    table += "<tr><td>Worked Days:  </td><td>" + stats["TotalDaysWorked"].ToString() + " (Should be: "+stats["WorkingDays"].ToString()+" days)</td></tr>";
    //    table += "<tr><td>Worked Hours:  </td><td>" + stats["TotalWorkedHours"].ToString() + " (Should be: " + stats["WorkingHours"].ToString() + " hours)</td></tr>";
    //    table += "<tr><td>Extra Worked Hours:  </td><td>" + stats["ExtraHours"].ToString() + "</td></tr>";
    //    table += "<tr><td>Under Worked Hours:  </td><td>" + stats["UnderHours"].ToString() + "</td></tr>";
    //    table += "</table>";

    //    result.InnerHtml = table;
    }

    protected void ddlUsers_SelectedIndexChanged(object sender, EventArgs e)
    {
        int user = ddlUsers.SelectedValue.ToInt32();

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("Select * from tblholidays where userid = @id", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@id", user);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    rptHolidayWeekend.DataSource = dt;
                    rptHolidayWeekend.DataBind();

                    rptHolidayWeekend.Visible = true;
                    lblText.Visible = false;
                }
                else
                {
                    rptHolidayWeekend.Visible = false;
                    lblText.Text = "No data available!";
                    lblText.Visible = true;
                }
            }
        }
    }

    protected void rptHolidayWeekend_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label type = e.Item.FindControl("lblType") as Label;
            Label date = e.Item.FindControl("lblDate") as Label;

            if (type.Text == "0")
            {
                type.Text = "Weekend";
                date.Text = "weekly";
            }
            else
            {
                type.Text = "Holiday";
                date.Text = Convert.ToDateTime(date.Text).ToString("dd-MM-yyyy");
            }
        }

    }
}