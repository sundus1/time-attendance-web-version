﻿using MyAttSys;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Collections;

public partial class settings : AvaimaThirdpartyTool.AvaimaWebPage
{
    public string[] TimeZone = { "(UTC+00.00) Western Europe Time, London, Lisbon, Casablanca, Monrovia",
                                        "(UTC-12.00) Eniwetok, Kwajalein",
                                        "(UTC-11.00) Midway Island, Samoa",
                                        "(UTC-10.00) Hawaii",
                                        "(UTC-09.00) Alaska",
                                        "(UTC-08.00) Pacific Time (US &amp; Canada)",
                                        "(UTC-07.00) Mountain Time (US &amp; Canada)",
                                        "(UTC-06.00) Central Time (US &amp; Canada), Mexico City",
                                        "(UTC-05.00) Eastern Time (US &amp; Canada), Bogota, Lima, Quito",
                                        "(UTC-04.00) Atlantic Time (Canada), Caracas, La Paz",
                                        "(UTC-03.50) Newfoundland",
                                        "(UTC-03.00) Brazil, Buenos Aires, Georgetown",
                                        "(UTC-02.00) Mid-Atlantic",
                                        "(UTC-01.00) Azores, Cape Verde Islands",
                                        "(UTC+01.00) CET(Central Europe Time), Brussels, Copenhagen, Madrid, Paris",
                                        "(UTC+02.00) EET(Eastern Europe Time), Kaliningrad, South Africa",
                                        "(UTC+03.00) Baghdad, Kuwait, Riyadh, Moscow, St. Petersburg, Volgograd, Nairobi",
                                        "(UTC+03.50) Tehran",
                                        "(UTC+04.00) Abu Dhabi, Muscat, Baku, Tbilisi",
                                        "(UTC+04.50) Kabul",
                                        "(UTC+05.00) Ekaterinburg, Islamabad, Karachi, Tashkent",
                                        "(UTC+05.50) Bombay, Calcutta, Madras, New Delhi",
                                        "(UTC+06.00) Almaty, Dhaka, Colombo",
                                        "(UTC+07.00) Bangkok, Hanoi, Jakarta",
                                        "(UTC+08.00) Beijing, Perth, Singapore, Hong Kong, Chongqing, Urumqi, Taipei",
                                        "(UTC+09.00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk",
                                        "(UTC+09.50) Adelaide, Darwin",
                                        "(UTC+10.00) EAST(East Australian Standard), Guam, Papua New Guinea, Vladivostok",
                                        "(UTC+11.00) Magadan, Solomon Islands, New Caledonia",
                                        "(UTC+12.00) Auckland, Wellington, Fiji, Kamchatka, Marshall Island",
                                        "(UTC+13.00) Nuku'alofa"};

    private string userid = "";
    private string emailId = "";
    private string password = "";
    private string AppId = "";

    Attendance atd = new Attendance();

    protected void Page_Load(object sender, EventArgs e)
    {
        userid = Request.QueryString["id"].ToString();
        emailId = Request.QueryString["e"].ToString().Decrypt();
        password = Request.QueryString["p"].ToString().Decrypt();

        if (!IsPostBack)
        {
            FillData();
        }
    }

    public void FillData()
    {
        //Load TimeZone   
        int maxlength = 0;
        foreach (string str in TimeZone)
        {
            ddlTimzone.Items.Add(str);
            if (maxlength < str.Length)
            {
                maxlength = str.Length;
            }
        }

        DataTable userSettings = JsonConvert.DeserializeObject<DataTable>(atd.GetSettingInfo(emailId, password));
        DataTable Userprofile = JsonConvert.DeserializeObject<DataTable>(atd.GetLoginInfo(emailId));

        ddlTimzone.SelectedValue = userSettings.Rows[0]["timezone"].ToString();
        AppId = userSettings.Rows[0]["userid"].ToString();
        txtTitle.Text = Userprofile.Rows[0]["Title"].ToString();

        LoadWeekends();

        //Load Holidays
        DataTable dt = new DataTable();
        dt = new DataTable();
        dt = JsonConvert.DeserializeObject<DataTable>(atd.GetUserHolidaysWeekends(userid.ToInt32(), "1"));

        if (dt.Rows.Count > 0)
        {
            rpthd.DataSource = dt;
            rpthd.DataBind();
            hdmsg.Visible = false;
        }
        else
        {
            hdmsg.Visible = true;
            rpthd.Visible = false;
        }
    }

    public void LoadWeekends()
    {
        List<Weekends> listToBind = new List<Weekends>
            { new Weekends("Sunday", "Sunday"), new Weekends("Monday", "Monday") , new Weekends("Tuesday", "Tuesday"), new Weekends("Wednesday", "Wednesday")  , new Weekends("Thursday", "Thursday"), new Weekends("Friday", "Friday"), new Weekends("Saturday", "Saturday")   };
        ddlWeekend.DataTextField = "Text";
        ddlWeekend.DataValueField = "Id";
        ddlWeekend.DataSource = listToBind;
        ddlWeekend.DataBind();

        DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.GetUserHolidaysWeekends(userid.ToInt32(), "0"));

        foreach (DataRow row in dt.Rows)
        {
            string curItem = row["title"].ToString();
            if (ddlWeekend.Items.FindByText(curItem) != null)
                ddlWeekend.Items.FindByText(curItem).Selected = true;

        }
    }

    protected void rpthd_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label hddate = e.Item.FindControl("hddate") as Label;
            hddate.Text = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "date")).ToString("ddd, MMM dd yyyy");
        }
    }

    protected void hAdd_Click(object sender, EventArgs e)
    {
        bool result = atd.Holidays(userid, "insert", htitle.Text, hdate.Text, cye.Checked, 0);
        htitle.Text = "";
        hdate.Text = "";
        FillData();
    }

    protected void rpthd_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            HiddenField d = e.Item.FindControl("hdnID") as HiddenField;
            atd.Holidays("0", "del", "", "", false, Convert.ToInt32(d.Value));
            FillData();
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string fullname = txtTitle.Text;
        string timezone = ddlTimzone.SelectedItem.ToString();
        Hashtable ht = new Hashtable();
        String weekendDays = "";

        foreach (ListItem item in ddlWeekend.Items)
        {
            if (item.Selected)
            {
                weekendDays += item + ",";
            }
        }

        weekendDays = weekendDays.TrimEnd(',');

        // Save Timzone
        //atd.SetTimezone(AppId, timezone);

        bool result = atd.UpdateUserSetting(weekendDays, Convert.ToInt32(userid), fullname);
    }

    public class Weekends
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public Weekends(string id, string text)
        {
            this.Id = id;
            this.Text = text;
        }
    }
}