﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class treeview : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       // TreeNode parentNode = new TreeNode(String.Format("<input type='radio' name='rbtncat' value='Root' onclick='setvalue(0)'/>&nbsp; Root"));
        TreeNode parentNode = new TreeNode(String.Format("<input type='radio' name='rbtncat' value='Root' onclick='setvalue(0)' />&nbsp; Root"));
        parentNode.SelectAction = TreeNodeSelectAction.Expand;
        Filltreeview(parentNode.ChildNodes, "0");
        TreeView1.Nodes.Add(parentNode);
    }
    public void Filltreeview(TreeNodeCollection tree, string id)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("gettreeviewid", con))
            {
                cmd.Parameters.AddWithValue("@ID", id);
                cmd.Parameters.AddWithValue("@InstanceId", 0);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        TreeNode node = new TreeNode(String.Format("<input type='radio' name='rbtncat' value='{0}' onclick='setvalue({0})'/>&nbsp;{1}", dr["ID"].ToString(), dr["Title"].ToString()));
                        //TreeNode node = new TreeNode(dr["Title"].ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        Filltreeview(node.ChildNodes, dr["ID"].ToString());
                        tree.Add(node);

                    }
                }
            }
        }
    }
}