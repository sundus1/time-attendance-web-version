﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Reports.aspx.cs" Inherits="Reports" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" media="screen" href="_assets/style.css" />
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <title>Reports</title>
    <style>
        table td {
            padding: 20px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: center">

            <table style="text-align: center">
                <tr>
                    <td>
                        <figure>
                            <asp:Image ID="imgMonthly" runat="server" src="images/Controls/Monthly.png" Style="width: 64px" />
                            <figcaption style="text-align: center">
                                <asp:LinkButton runat="server" ID="lblMonthlyReport" Text="Monthly Report" OnClick="lblMonthlyReport_Click">
                                    
                                </asp:LinkButton>
                            </figcaption>
                        </figure>
                    </td>
                </tr>
                <tr >
                    <td>
                        <figure>
                            <img src="images/Controls/Yearly.png" style="width: 64px" />
                            <figcaption style="text-align: center">
                                <asp:LinkButton runat="server" ID="lblYearlyReport" Text="Yearly Report" OnClick="lblYearlyReport_Click"></asp:LinkButton>
                            </figcaption>
                        </figure>
                    </td>
                </tr>
                <tr style="display: none">
                    <td>
                        <figure>
                            <img src="images/Controls/late.png" style="width: 64px" />
                            <figcaption style="text-align: center">
                                <asp:LinkButton runat="server" ID="lblLateReport" Text="Late Report" OnClick="lblLateReport_Click"></asp:LinkButton>
                            </figcaption>
                        </figure>
                    </td>
                </tr>
                <tr style="display: none">
                    <td>
                        <figure>
                            <img src="images/Controls/EDAL.png" style="width: 64px" />
                            <figcaption style="text-align: center">
                                <asp:LinkButton runat="server" ID="lnkEDAL" Text="Leaves & Extra Work Report" OnClick="lnkEDAL_Click"></asp:LinkButton>
                            </figcaption>
                        </figure>
                    </td>
                </tr>

            </table>

        </div>
    </form>
</body>
</html>
