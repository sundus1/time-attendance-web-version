﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default"
    EnableEventValidation="false" %>

<%@ Register Src="~/WebApplication1/PagerControl.ascx" TagPrefix="uc1" TagName="PagerControl" %>
<%@ Register Src="~/WebApplication1/userprofile.ascx" TagName="workeruserprofile"
    TagPrefix="uc2" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" media="screen" href="_assets/style.css" />
    <link rel="stylesheet" href="_assets/jquery-ui.css" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="_assets/css/tooltipster.css" />
    <link href="_assets/tablestyles.css" rel="stylesheet" />
    <script type="text/javascript" src="_assets/js/jquery.tooltipster.js"></script>
    <link rel="stylesheet" href="_assets/jquery.timepicker.css" />
    <script src="_assets/jquery.timepicker.min.js"></script>
    <link href="_assets/jquery.switchButton.css" rel="stylesheet" />
    <script src="_assets/jquery.switchButton.js"></script>
    <script src="_assets/DateTimeHelper.js"></script>
    <link href="_assets/jquery-ui-timepicker-addon.css" rel="stylesheet" />
    <script src="_assets/jquery-ui-timepicker-addon.js"></script>
    <script src="_assets/otf.js"></script>
    <style>
        div.main2 a
        {
            color: blue;
        }
    </style>
    <script>

        function GetParameterValues(param) {
            var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                if (urlparam[0] == param) {
                    return urlparam[1];
                }
            }
        }
        $(document).ready(function () {
            var employeelink = GetParameterValues('employeelink');
            if (employeelink == "1") {
                openAdd();
            }
        });

        function showFirstTime() {
            try {
                $('.main2').dialog({
                    height: 261,
                    width: 892,
                    modal: true,
                    hide: {
                        effect: 'blind'
                    },
                    buttons: {
                        "OK": function () {
                            $(this).dialog('close');
                        }
                    }
                });
            } catch (e) {
             //   alert(e.message);
            }

        }

        var mousePos;
        function handleMouseMove(event) {
            event = event || window.event; // IE-ism
            mousePos = {
                x: event.clientX,
                y: event.clientY
            };
        }

        $(document).ready(function () {
            //1800000
            setInterval(function () {
                //alert('auto reloading');
                location.reload(true);
            }, 1800000);


            try {
                window.onmousemove = handleMouseMove;
            } catch (e) {
                alert(e.message);
            }

            $('.tooltip').tooltipster({
                interactive: true
            });
            $('body').click(function (event) {
                if (event.target.nodeName != 'IMG') {
                    $('#menu').hide(200);
                }
            })
            $('.imgAction').click(function () {
                try {
                    //showCursor($(this));
                    var dataabid = $(this).attr('data-ab-id');
                    var datauserid = $(this).attr('data-ab-userid');
                    var datadate = $(this).attr('data-ab-date');
                    var dataactive = $(this).attr('data-ab-active');
                    var dataclockin = $(this).attr('data-clock');
                    var datainaddress = $(this).attr('data-inaddress');
                    var datauseractive = $(this).attr('data-useractive');

                    var dataapid = $(this).attr('data-ap-autopresentid');
                    var dataapci = $(this).attr('data-ap-ci');
                    var dataapco = $(this).attr('data-ap-co');
                    var dataapactive = $(this).attr('data-ap-active');

                    var pos = mousePos;
                    if (!pos) {
                        $('#menu').css('left', pos.x);
                        $('#menu').css('top', pos.y);
                        $('#menu').css('position', 'absolute');
                    }
                    else {
                        $('#menu').css('left', pos.x);
                        $('#menu').css('top', pos.y);
                        $('#menu').css('position', 'absolute');
                    }

                    var dataexid = $(this).attr('data-exday-id');
                    var dataextypeid = $(this).attr('data-exday-typeid');
                    var dataexactive = $(this).attr('data-exday-active');
                    var dataexdate = $(this).attr('data-exday-date');

                    $('#menu').show(200);
                    GenerateMenu(dataabid, datauserid, datadate, dataactive, dataclockin, datainaddress, dataapid, dataapci, dataapco, dataapactive, datauseractive, dataexid, dataextypeid, dataexactive, dataexdate);
                    $('#menu').menu();
                } catch (e) {
                    alert(e.message);
                }
            });

            $(document).on('click', '.lnkActive', function () {
                var userid = $('.lnkActive').attr('data-userid');
                var datauseractive = $('.lnkActive').attr('data-useractive');
                if (confirm('Are you sure?')) {
                    SetUserStatus(userid, datauseractive, true);
                }
            });

            function GenerateMenu(abid, userid, abdate, abactive, dataclockin, datainaddress, dataapid, dataapci, dataapco, dataapactive, datauseractive, dataexid, dataextypeid, dataexactive, dataexdate) {
                //$('.lnkActive').attr('data-useractive', datauseractive);
                //$('.lnkActive').attr('data-userid', userid);
                //if (datauseractive == 'True') {
                //    $('.lnkActive').text('Deactivate');
                //}
                //else {
                //    $('.lnkActive').text('Activate');
                //}
                //$('.lnkEdit').attr('data-useractive', datauseractive);
                var instanceId = GetParameterValues("instanceid");
                $('.lnkEdit').attr('href', "EditTime.aspx?id=" + userid + "&instanceid=" + instanceId);
                $('.lnkEdit').text('Edit');

                // ExDay Partial Working
                //                $('.lnkPW').attr('data-exday-id', dataexid);
                //                $('.lnkPW').attr('data-exday-userid', userid);
                //                $('.lnkPW').attr('data-exday-typeid', dataextypeid);
                //                $('.lnkPW').attr('data-exday-date', dataexdate);
                //                $('.lnkPW').attr('data-exday-active', dataexactive);

                //                if (dataexactive == "false") {
                //                    $('.lnkPW').text("Unmark as Partial Working");
                //                }
                //                else {
                //                    $('.lnkPW').text("Mark as Partial Working");
                //                }

                if (dataclockin == 'True') {
                    $('.lnkClock').attr('clockin', 'false');
                    $('.lnkClock').text("Clock in");
                }
                else {
                    $('.lnkClock').attr('clockin', 'true');
                    $('.lnkClock').text("Clock out");
                }
                $('.lnkClock').attr('data-userid', userid);
                $('.lnkClock').attr('data-inaddress', datainaddress);

                /* $('.lnkAttendance').attr('data-id', abid);
                 $('.lnkAttendance').attr('data-userid', userid);
                 $('.lnkAttendance').attr('data-date', abdate);
                 $('.lnkAttendance').attr('data-active', abactive);
                 $('.lnkAttendance').attr('data-inaddress', datainaddress);
                 if (parseInt(abid) > 0) {
                     $('.lnkAttendance').text("Remove absence");
                 }
                 else {
                     $('.lnkAttendance').text("Mark as absent");
                 }*/
            }

            //            $(document).on('click', '.lnkAttendance', function () {
            //                var text = $('.lnkAttendance').text();
            //                showDialog(text, "<p>Are you sure you want to " + text + "?</p>", $(this));
            //                return false;
            //            });

            //            $(document).on('click', '.lnkPW', function () {
            //                var text = $('.lnkPW').text();
            //                $('#divClockedIn').attr("title", text);
            //                $('#divClockedIn').html("Are you sure you want to " + text);
            //                $('#divClockedIn').dialog({
            //                    modal: true,
            //                    buttons: {
            //                        "Yes": function () {
            //                            // Manage
            //                            AddUpdateExDaysD($('.lnkPW'), 'Partial working', true);
            //                            $(this).dialog("close");
            //                        },
            //                        "No": function () {
            //                            $(this).dialog("close");
            //                        },
            //                        "Cancel": function () {
            //                            $(this).dialog("close");
            //                        }
            //                    }
            //                });
            //                return false;
            //            });
            $(function () {
                setInterval(function () { var d1 = $('#hdnCITime').val().split(' '); var d = $('#hdnCITime').val().split(' ')[0].split(':'); if (d[1] == "59") { d[1] = "00"; if (d[0] == "12") d[0] = "01"; else d[0] = parseInt(d[0]) + 1; } else d[1] = parseInt(d[1]) + 1; if (parseInt(d[1]) < 10) d[1] = "0" + d[1]; $('#hdnCITime').val(d[0] + ":" + d[1] + " " + d1[1]) }, 60000);
            });
            //$(document).on('click', '.lnkAutoPresent', function () {
            //    //alert('autopresent');
            //    $('#divAutoPresent').dialog({
            //        modal: true,
            //        buttons: {
            //            "Save": function () {
            //                try {
            //                    var AutoPresentID = $('.lnkAutoPresent').attr('data-ap-autopresentid');
            //                    var UserID = $('.lnkAutoPresent').attr('data-userid');
            //                    var ClockInTime = $('#txtOPStartTime').val();
            //                    var ClockOutTime = $('#txtOPEndTime').val();
            //                    var Active = 'False';
            //                    if ($('.trAPC').css('display') == 'none') {
            //                        Active = 'False';
            //                    }
            //                    else {
            //                        Active = 'True';
            //                    }
            //                    AddUpdateAutoPresent(AutoPresentID, UserID, ClockInTime, ClockOutTime, Active);
            //                    $(this).dialog("close");
            //                } catch (e) {

            //                }
            //                //alert('save');
            //                //$(this).dialog("close");
            //            },
            //            "Cancel": function () {
            //                //alert('close');
            //                $(this).dialog("close");
            //            }
            //        },
            //        show: {

            //        }
            //    });
            //    return false;
            //});

            $(document).on('change', '#chkAP', function () {
                //alert('change');
                if ($('#chkAP').is(":checked")) {
                    $('.trAPC').show(200);
                    //$
                }
                else {
                    $('.trAPC').hide(200);
                }
            });

            $('.datetime').timepicker({
                timeFormat: 'hh:mm tt',
                controlType: 'select'
            });


            $("#chkAP").switchButton({
                on_label: 'On',
                off_label: 'Off'
            });



            function showDialog(title, message, obj) {
                $('#divClockedIn').attr("title", title);
                $('#divClockedIn').html(message);
                $('#divClockedIn').dialog({
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            showAbsCommentBox(obj);
                        },
                        "No": function () {
                            $(this).dialog("close");
                        },
                        "Cancel": function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }

            function showAbsCommentBox(obj) {
                $('#absComment').dialog({
                    width: 375,
                    modal: true,
                    buttons: {
                        "Save": function () {
                            addAbsenceToday(obj, $('#txtAbsComment').val(), true);
                            $(this).dialog('close');
                            $('#divClockedIn').dialog("close");
                        },
                        "Cancel": function () {
                            $(this).dialog('close');
                        }
                    }
                });
            }
            $('.lnkWorkingHours').click(function () {
                var dlg = $('#divWorkingHours').dialog({
                    modal: true,
                    width: 400,
                    hide: {
                        effect: "blind",
                    }

                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;
            });
            $('.btnSaveWH').click(function () {
                $('#divWorkingHours').dialog('close');
            });
            $('.btnCanceWH').click(function () {
                $('#divWorkingHours').dialog('close');
                return false;
            });
            $('#btnApplyToAll').click(function () {
                var CI = $('.txtMonCI').val();
                var CO = $('.txtMonCO').val();
                $('.datetimeCI').val(CI);
                $('.datetimeCO').val(CO);
                return false;
            });

            $('#lnkClock').click(function () {
                if ($(this).attr('clockin') == 'false') {
                    Clockin($(this));
                }
                else if ($(this).attr('clockin') == 'true') {
                    Clockout($(this));
                }
            });
        });

        function Workinghour() {
            var dlg = $('#divWorkingHours').dialog({
                modal: true,
                width: 400,
                hide: {
                    effect: "blind",
                }

            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            return false;
        }

        function openAdd() {
            $('#txttitle').val('');
            var dlg = $("#divAddWorker").dialog({
                show: {
                },
                top: 80,
                left: 330,
                modal: true,
                hide: {
                    effect: "blind",
                }
                //,
                //beforeClose: function (event, ui) {
                //    var id = $("#id").val();
                //    $.get("Default.aspx?id=" + id + "&do=del");
                //}
            });
            $('.txtEmail').focus();
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            dlg.parent().css("width", "455px");
        }
//        function openHowToAddEmployee() {
//            var dlg = $("#HAddEmployee").dialog({
//                show: {
//                },
//                top: 0,
//                left: 0,
//                // modal: true,
//                hide: {
//                    effect: "blind",
//                }
//            });
//            //dlg.parent().appendTo($("form:first"));
//            //dlg.parent().css("z-index", "1000");
//            dlg.parent().css("width", "455px");
//        }
        function alreadyExists() {
            var dlg = $("#divError").dialog({
                show: {
                },
                modal: true,
                hide: {
                    effect: "blind",
                },
                buttons: {
                    "Ok": function () {
                        $(this).dialog('close');
                    }
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            dlg.parent().css("width", "455px");
        }

        function ren() {
            $('#lblrerror').text("");
            $('#txtrtitle').val('');
            var dlg = $("#divrenamedepartment").dialog({
                show: {


                    //effect: "blind",
                    //duration: 1000
                },
                modal: true,
                hide: {


                    effect: "blind",
                    //duration: 1000
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            dlg.parent().css("width", "247px");
            return false;
        }

        $(document).on('click', '#btnsendcontrorinfo', function () {
            if ($('#txtEmail').val() == '') {
                $('.txtEmail').attr('placeholder', 'Please provide email address');
                return false;
            }
            $('#divAddWorker').hide(200);
        });



        function settings() {
            var dlg = $("#categorySettings").dialog({
                show: {
                    //effect: "blind",
                    //duration: 1000
                },
                modal: true,
                hide: {


                    effect: "blind",
                    //duration: 1000
                }
                //,
                //beforeClose: function (e) {
                //    e.preventDefault();
                //    var t = $("a[id^='rptBreadCrum_lbtnBreadCrum_']");
                //    var hrf = t.last().attr("href");
                //    if (hrf == null || hrf == undefined || hrf == "")
                //        hrf = $("#root").attr("href");
                //    var tmpFunc = new Function(hrf);
                //    tmpFunc();
                //}
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            return false;
        }

        function removeConfirmation(control_uniqueid, name_deleted) {
            $("#b_name_to_be_deleted").html(name_deleted);
            var r;
            $("input[id^='Rptrattendence_chkselect']").each(function () {
                if ($(this).is(":checked")) {
                    r = $(this).is(":checked");
                }
            });
            if (r) {
                var dlg = $("#removemail").dialog({
                    resizable: false,
                    height: 200,
                    width: 400,
                    modal: true,
                    autoOpen: true,
                    buttons: {
                        "Yes": function () {
                            $(this).dialog("close");
                            __doPostBack('lbtn_del', 'OnClick');
                            //__doPostBack(control_uniqueid, 'OnClick');
                        },
                        "No": function () {
                            $(this).dialog("close");
                            return false;
                        }
                    }
                });
            } else {
                alert("No item(s) selected to delete.");
            }
            return false;
        }

        function setvalue(id) {
            var x = document.getElementById("hftreenodevalue").value = id;
            return false;
        }


        //------ script for check all
        $(function () {
            setInterval(function () {
                $('span[data-date]').each(function (index) {
                    //alert($(this).text());
                    var $target = '#' + $(this).attr('id');
                    var signintime = $($target).attr('data-date');
                    getDateTimeDifference(signintime, $target, new Date(new Date().toLocaleDateString() + ' ' + $('#hdnCITime').val()));
                });
            }, 1000);


            $('.cb_select_all input').change(function () {
                if (this.checked)
                    $($(this).parents("table")[0]).find(".cb_select input").each(function () {
                        this.checked = true;
                    });
                else {
                    $($(this).parents("table")[0]).find(".cb_select input").each(function () {
                        this.checked = false;
                    });
                }
            });

            $('.lnkadd_worker, .lnkAdd_worker1').click(function () {
                openAdd();
            });

            $("#btnupdate").click(function () {
                if ($('#txtrtitle').val() == "") {
                    $('#lblrerror').text('Required');
                    return false;
                }
            });

            $("#btnusercontrol").click(function () {
                var dlg = $("#divworkeradd").dialog({
                    height: 450,
                    width: 800,
                    show: {
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                var iframe = window.parent.document.getElementById("MainContent_ifapp");
                if (iframe != null)
                    iframe.height = "600px";
                $("#divAddWorker").dialog("close");
                return false;
            });

            $('#btncancel').click(function () {

            });


            //--- date picker
            //$("#txtstartdate").datepicker();
            $("#txtenddate").datepicker();


            //--- empty search click funtion
            $('#btnserachdate').click(function () {
                if ($('#txtsearch').val() == "") {
                    return false;
                }
            });

            //--------- script for add department

            $("#lnkAdd_department").click(function () {
                $('#txttitle').val('');
                var dlg = $("#divadddepartment").dialog({
                    show: {



                        //effect: "blind",
                        //duration: 1000
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                        //duration: 1000
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;
                //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
            });

            $("#lnkAdd_department1").click(function () {
                $('#txttitle').val('');
                var dlg = $("#divadddepartment").dialog({
                    show: {



                        //effect: "blind",
                        //duration: 1000
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                        //duration: 1000
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;
                //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
            });

            //---- for show hide searchbox
            //for toogle div search
            //            $('#lnksearch').click(function (e) {
            //                e.preventDefault();
            //                $('#searchdate').toggle();
            //                $('#divfuntions').toggle();
            //                $('#categorySettings').hide();
            //                resizeiframepage();
            //                $('#txtsearch').focus();
            //                return false;
            //            });
            $('#btn_cancel').click(function (e) {
                e.preventDefault();
                $('#searchdate').toggle();
                $('#divfuntions').toggle();
                document.getElementById("txtsearch").value = "";
                resizeiframepage();
                return false;
            });

            $("#lnkcategorySettings").click(function (e) {
                e.preventDefault();
                $('#categorySettings').toggle();
                $('#searchdate').hide();
                try {
                    resizeiframepage();
                } catch (e) {

                }
                return false;
            });

            $("#clsbtn").click(function (e) {
                e.preventDefault();
                $('#categorySettings').hide();
                try {
                    resizeiframepage();
                } catch (e) {

                }
                return false;
            });

            //----for move item
            $("#lnkmove").click(function () {
                var r = false;
                $('#lblerror').text("");
                $("input[id^='Rptrattendence_chkselect']").each(function () {
                    if ($(this).is(":checked")) {
                        r = $(this).is(":checked");
                    }
                });
                if (r) {
                    var dlg = $("#divtreeview").dialog({
                        show: {
                            height: 100
                            //effect: "blind",
                            //duration: 1000
                        },
                        modal: true,
                        hide: {
                            effect: "blind",
                            //duration: 1000
                        }
                    });
                    dlg.parent().appendTo($("form:first"));
                    dlg.parent().css("z-index", "1000");
                } else {
                    alert("No item(s) selected to move.");
                }
                return false;
                //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
            });

        });

        function resizeiframepage() {
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
            if (innerDoc.body.offsetHeight) {
                iframe.height = innerDoc.body.offsetHeight + 32 + "px";
            } else if (iframe.Document && iframe.Document.body.scrollHeight) {
                iframe.style.height = iframe.Document.body.scrollHeight + "px";
            }
        }

        function chkRF() {
            var title = $("#<%=txttitle.ClientID%>").val();
                if (title.trim() == "" || title == null) {
                    $("#<%=lblerror.ClientID%>").css("display", "block");
                    return false;
                }
            }

    </script>
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css"
        media="screen" type="text/css" rel="stylesheet" />
    <title>Time & Attendance</title>
    <style>
        .ui-menu
        {
            width: 150px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hdnAdminEmail" runat="server" />
    <asp:HiddenField ID="hdnAdminName" runat="server" />
    <asp:ScriptManager ID="scmang" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblDate" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lbl" runat="server" Visible="false"></asp:Label>
    <table>
        <tr id="trmainfunction" runat="server">
            <td>
                <div style="margin-left: 7px;" runat="server" id="topDiv">
                    <div id="divfuntions" runat="server">
                        <%--OnClick="LnkAddContractClick"--%>
                        <%--<asp:LinkButton ID="lnkadd_worker" Text="Add Worker" runat="server" style="color:red"></asp:LinkButton>--%>
                        <a href="#" id="Add Worker" class="lnkadd_worker">Add Employee</a> |
                        <asp:LinkButton ID="lnkAdd_department" Text="Add Department" runat="server" OnClick="LnkAddDepartmentClick"></asp:LinkButton>
                        |
                        <asp:LinkButton ID="lnkmove" Text="Move" runat="server"></asp:LinkButton>
                        |
                        <asp:LinkButton ID="lbtn_del" Text="Delete" runat="server" OnClientClick="return removeConfirmation(this)"
                            OnClick="LbtndelClick"></asp:LinkButton>
                        <%--<asp:LinkButton ID="lbtn_del" Text="Delete" runat="server" OnClientClick="return confirm('Are you sure you want to delete')" OnClick="LbtndelClick"></asp:LinkButton>--%>
                        <%--|
                        <asp:LinkButton ID="lnksearch" Text="Search" runat="server"></asp:LinkButton>
                        --%>
                        <span runat="server" id="foldrOp">|
                            <asp:LinkButton ID="lnkcategorySettings" Text="Folder Settings" runat="server"></asp:LinkButton></span>
                        <span runat="server" id="MyAttendance" visible="False">|
                            <asp:LinkButton ID="lnkMyAttendance" Text="My Attendance" runat="server" OnClick="lnkMyAttendance_Click"></asp:LinkButton></span>
                        <%--|
                        <asp:LinkButton ID="lnkLateReport" Text="Reports" runat="server" OnClick="lnkLateReport_Click"></asp:LinkButton>--%></span>
                    </div>
                </div>
            </td>
        </tr>
        <tr id="trmainbc" runat="server">
            <td colspan="2">
                <div style="display: inline-block; margin: 12px 0 0 7px; float: left" id="breadcrumb">
                    <asp:Label ID="lblexception" Text="" runat="server" ForeColor="red"></asp:Label>
                    <div style="display: inline-block;">
                        <asp:LinkButton ID="root" Text="Root" runat="server" OnClick="RootClick"></asp:LinkButton>
                        <asp:Repeater runat="server" ID="rptBreadCrum" OnItemDataBound="Rptbreadcrumitemdatabound"
                            OnItemCommand="RptrbreadcrumitemCommand">
                            <HeaderTemplate>
                                /
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lbtnBreadCrum" CommandName="breadcrum"></asp:LinkButton>
                                /
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <%--<asp:DropDownList runat="server" ID="statusList" AutoPostBack="true" OnSelectedIndexChanged="statusList_SelectedIndexChanged"
                    Style="float: right; margin-top: 12px;">
                    <asp:ListItem Text="Show All" Value="0" Selected="true"></asp:ListItem>
                    <asp:ListItem Text="Show All Signed In" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Show All Not Signed In" Value="2"></asp:ListItem>
                </asp:DropDownList>--%>
            </td>
        </tr>
        <tr>
            <td>
                <div class="space" id="main" runat="server" style="margin-left: 7px;">
                    <div id="removemail" style="display: none;" title="Confirmation">
                        Are you sure you want to delete the following<b id="b_name_to_be_deleted"></b>?
                        <asp:Label ID="lblrem" runat="server"></asp:Label>
                    </div>
                    <div id="categorySettings" style="display: none">
                        <table class="smallGrid">
                            <tr>
                                <td>
                                    Folder Admins:
                                </td>
                                <td>
                                    <ul style="list-style: none">
                                        <asp:Repeater runat="server" ID="admins" OnItemCommand="adminItemCommand">
                                            <ItemTemplate>
                                                <li>
                                                    <%#Eval("Title") %>(<asp:LinkButton ID="delAdmin" CommandArgument='<%# Eval("ID") %>'
                                                        CommandName="delAdmin" runat="server" Text="Remove"></asp:LinkButton>)</li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <li style="color: red;" runat="server" id="noAdmin" visible="False">No admin found.</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr class="smallGrid">
                                <td>
                                    Add Admin:
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="userList">
                                    </asp:DropDownList>
                                    <asp:HiddenField runat="server" ID="fid" />
                                </td>
                            </tr>
                            <tr class="smallGrid">
                                <td>
                                </td>
                                <td>
                                    <asp:Button ID="addAdmin" Text="Add" runat="server" OnClick="addAdmin_Click" />
                                    <asp:Button ID="clsbtn" Text="Close" runat="server" />
                                </td>
                            </tr>
                        </table>
                        <br />
                    </div>
                    <div id="searchdate" style="display: none">
                        <table class="smallGrid">
                            <tr>
                                <td>
                                    Search By Name or Email :
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlsel_daterange" runat="server" Visible="False">
                                        <asp:ListItem Text="Start Date" Value="Start Date"></asp:ListItem>
                                        <asp:ListItem Text="End Date" Value="End Date"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="smallGrid">
                                <td>
                                    Search :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtsearch" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="smallGrid">
                                <td>
                                </td>
                                <td>
                                    <asp:Button ID="btnserachdate" Text="Search" OnClick="BtnsearchClick" runat="server" />
                                    <asp:Button ID="btn_cancel" Text="Clear" runat="server" />
                                </td>
                            </tr>
                        </table>
                        <br />
                    </div>
                    <div>
                        <asp:Repeater ID="Rptrattendence" runat="server" OnItemCommand="RptrattendenceItemcommand"
                            OnItemDataBound="Rprteattendenceitemdatabound">
                            <HeaderTemplate>
                                <table id="tblconctract" class="smallGrid">
                                    <tr class="smallGridHead">
                                        <td>
                                            <asp:CheckBox ID="chkselectall" runat="server" CssClass="cb_select_all" />
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            Title
                                        </td>
                                        <%--<td>
                                    Email Address
                                </td>--%>
                                        <td style="display: none">
                                            User Name
                                        </td>
                                        <td>
                                            Date
                                        </td>
                                        <td>
                                            Signin
                                        </td>
                                        <td style="display: none;">
                                            Signout
                                        </td>
                                        <td>
                                            Workstatus
                                        </td>
                                        <td>
                                            Worked
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <%--<td colspan="2">Modification
                                </td>--%>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="smallGrid" id="trdata" runat="server">
                                    <td>
                                        <asp:CheckBox ID="chkselecteditem" runat="server" CssClass="cb_select" />
                                        <asp:HiddenField runat="server" ID="hf_ID" Value='<%# Eval("ID") %>' />
                                        <asp:HiddenField runat="server" ID="hf_userrole" Value='<%# Eval("role") %>' />
                                    </td>
                                    <td class="folderLink">
                                        <asp:Image ID="imgfolder" ImageUrl="images/Folder.png" runat="server" />
                                        <asp:Label ID="lblcatcheck" Text='<%# Eval("category") %>' Visible="False" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <div style="float: left">
                                            <asp:LinkButton ID="lnktitle" CommandArgument='<%# Eval("ID") %>' CommandName="search"
                                                runat="server" Style=""></asp:LinkButton>
                                            <asp:Label runat="server" ID="lblrole"></asp:Label>
                                            <asp:Label runat="server" ID="lblcount"></asp:Label>
                                        </div>
                                        <div style="float: right; padding-left: 10px">
                                            <asp:ImageButton runat="server" CommandArgument='<%# Eval("ID") %>' CommandName="ren"
                                                ID="btnRename" Style="float: right; font-size: 8px; display: block; vertical-align: sub;
                                                margin: 0px 0 0 0px;" OnClientClick="" ImageUrl="images/pencil.png"></asp:ImageButton>
                                            <asp:ImageButton runat="server" CommandArgument='<%# Eval("ID") %>' CommandName="settings"
                                                ID="btnSettings" Style="float: right; font-size: 8px; display: block; vertical-align: sub;
                                                margin: 0px 0 0 0px;" OnClientClick="" ImageUrl="images/settings.png"></asp:ImageButton>
                                        </div>
                                    </td>
                                    <%--<td>
                                <asp:Label runat="server" ID="lblemail" Text='<%# Eval("email") %>'></asp:Label>
                            </td>--%>
                                    <td style="display: none">
                                        <asp:Label runat="server" ID="lblusername" Text='<%# Eval("userid") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lbltimein"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lbltimeforin"></asp:Label>
                                    </td>
                                    <%--<td>
                                <asp:Label runat="server" ID="lbltimeout"></asp:Label>
                            </td>--%>
                                    <td style="display: none;">
                                        <asp:Label runat="server" ID="lbltimeforout"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblworkstatus"></asp:Label>
                                        <%--<label>- </label>
                                <asp:LinkButton ID="lnkSignIn" CssClass="lnkSignIn" CommandName="SignIn" runat="server" Visible="false" Style="margin-left: 6px" Text="Clock-In"></asp:LinkButton>
                                <asp:LinkButton ID="lnkSignOut" CssClass="lnkSignOut" CommandName="SignOut" runat="server" Visible="false" Text="Clock-Out"></asp:LinkButton>--%>
                                    </td>
                                    <%--<td>
                                <asp:Label ID="lbldatefield" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbltimefield" runat="server"></asp:Label>
                            </td>--%>
                                    <td>
                                        <asp:Label runat="server" ID="lblworkhours"></asp:Label>
                                    </td>
                                    <td>
                                        <image src="images/controls/icon_info.png" style="display: none; padding-right: 5px;
                                            float: left;" runat="server" id="imgInfo" title="" tooltip="" alt="Info" class="imgInfo"></image>
                                    </td>
                                    <td>
                                        <image src="images/controls/gear.png" style="visibility: hidden; float: right" runat="server"
                                            id="imgAction" title="" tooltip="" alt="Menu" class="imgAction"></image>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:Button ID="Button1" runat="server" Text="Send Email Test" Visible="false" OnClick="Button1_Click" />
                        <asp:Label ID="lblemptygrid" ForeColor="red" Text="No Record Found" Visible="False"
                            runat="server"></asp:Label>
                        <uc1:PagerControl runat="server" ID="pg_Control" PageSize="20" />
                    </div>
                    <div id="divrenamedepartment" title="Rename Department" style="display: none">
                        <asp:Panel ID="Panel1" DefaultButton="btnupdate" runat="server">
                            <br />
                            <asp:TextBox ID="txtrtitle" runat="server"></asp:TextBox>
                            <asp:Label ID="lblrerror" Text="" ForeColor="red" runat="server"></asp:Label>
                            <br />
                            <asp:Button ID="btnupdate" Text="submit" runat="server" OnClick="btnupdate_Click" />
                            <asp:HiddenField ID="deptId" runat="server" />
                        </asp:Panel>
                    </div>
                    <div id="divtreeview" style="display: none" title="Move">
                        <asp:TreeView ID="TreeView1" runat="server" ImageSet="Arrows" CollapseImageToolTip="">
                        </asp:TreeView>
                        <br />
                        <asp:Button ID="btntreeview" runat="server" Text="Move" OnClick="BtntreeviewClick" />
                        <asp:Button ID="btncancel" Text="Cancel" runat="server" />
                        <asp:HiddenField ID="hftreenodevalue" Value="" runat="server" />
                        <asp:HiddenField ID="hfOldNode" Value="" runat="server" />
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
    <div id="divworkeradd" style="display: none; width: 600px">
        <uc2:workeruserprofile ID="workeruserprofile" runat="server" />
    </div>
    <div id="divAddWorker" title="Add Employee" style="width: 440px; display: none">
        <asp:Panel ID="pnlAddDept" DefaultButton="btnsendcontrorinfo" runat="server">
            <table class="profile" style="margin-top: 5px;">
                <tr>
                    <td colspan="2">
                        When you fill out the form below, Avaima will send them an email and ask the user
                        to sign up just like how you did when you had signed up on Avaima. They can then
                        go to avaima.com, sign-in and clock-in/out and their data will automatically show
                        in the grid below as a new user. Each user will have to go to avaima.com and sign
                        in to clock-in/out using their respective accounts through which they sign up.<br />
                    </td>
                </tr>
                <tr>
                    <td class="captionauto">
                        First Name:
                    </td>
                    <td>
                        <asp:TextBox ID="txtFirstName" runat="server" EnableViewState="false" CssClass="standardField txtFirstName required"
                            placeholder="First Name..."></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="captionauto">
                        Last Name:
                    </td>
                    <td>
                        <asp:TextBox ID="txtLastName" runat="server" CssClass="standardField txtLastName required"
                            EnableViewState="false" placeholder="Last Name..."></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="captionauto">
                        Email:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtEmail" placeholder="Email Address" CssClass="standardField txtEmail"></asp:TextBox><asp:RequiredFieldValidator
                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="Required"
                            ForeColor="Red" ValidationGroup="gg" Display="Dynamic">Required</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtEmail"
                            Display="Dynamic" ErrorMessage="Invalid Email" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ValidationGroup="gg"></asp:RegularExpressionValidator>
                        <asp:HiddenField runat="server" ID="id" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: right">
                        <asp:Button runat="server" ID="btnsendcontrorinfo" Text="Send Email" CssClass="ui-state-default btnsendcontrorinfo"
                            ValidationGroup="gg" OnClick="BtnsendcontrorinfoClick" ClientIDMode="Static" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Avaima collects and updates information from the User
                    </td>
                </tr>
                <%--<tr>
                        <td colspan="2">
                            <asp:LinkButton ID="btnusercontrol" Text="I want to provide employee information myself" runat="server" Style="color: blue"></asp:LinkButton>
                        </td>
                    </tr>--%>
            </table>
        </asp:Panel>
    </div>
    <div id="divError" class="divError" title="Error" style="display: none">
        <p style="padding: 10px">
            <b>ERROR:</b> The user already exists
        </p>
        <p style="padding: 10px">
            Path: Root /
        </p>
    </div>
    <div>
        <div id="divadddepartment" title=" Add Department" style="display: none">
            <asp:Panel ID="pnladd" DefaultButton="btndeptinsert" runat="server">
                <br />
                <asp:TextBox ID="txttitle" runat="server"></asp:TextBox>
                <asp:Label ID="lblerror" Text="Required" ForeColor="red" runat="server" Style="display: none;
                    width: 70px"></asp:Label>
                <br />
                <asp:Button ID="btndeptinsert" CssClass="ui-state-default" Text="submit" runat="server"
                    OnClick="BtndeptinsertClick" OnClientClick="return chkRF();" />
                <asp:HiddenField ID="hfparentid" runat="server" />
            </asp:Panel>
        </div>
    </div>
    <ul id="menu">
        <li><a class="lnkEdit menuitem" id="lnkEdit" href="#"></a></li>
        <li><a href="#" class="lnkClock menuitem" id="lnkClock"></a></li>
        <%--   <li>
                <a href="#" class="lnkAttendance menuitem" id="lnkAttendance"></a>
            </li>--%>
        <%--<li><a href="#" class="lnkActive menuitem hide" id="lnkActive"></a></li>--%>
        <%--<li><a href="#" class="lnkPW menuitem hide" id="lnkPW" style="width: 140px"></a>--%>
        <%--</li>--%>
    </ul>
    <div id="divAutoPresent" title="Auto Present" style="display: none">
        <table>
            <tr>
                <td colspan="2">
                    <p>
                        Use this option to mark present automatically by specifying clock in and clock out
                        time.
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    Auto Present:
                </td>
                <td>
                    <input type="checkbox" id="chkAP" value="1" />
                </td>
            </tr>
            <tr class="trAPC">
                <td>
                    Clock in Time:
                </td>
                <td>
                    <input id="txtOPStartTime" type="text" class="txtOPStartTime datetime" value="Select" />
                </td>
            </tr>
            <tr class="trAPC">
                <td>
                    Clock out Time:
                </td>
                <td>
                    <input id="txtOPEndTime" type="text" class="txtOPStartTime datetime" value="Select" />
                </td>
            </tr>
        </table>
    </div>
    <div id="main2" title="Welcome!" class="main2" runat="server" style="margin-left: 4px;
        display: none">
        <div class="breadCrumb" style="margin-left: 4px;">
            Welcome to Time & Attendance Management
        </div>
        <table class="" id="tblproducts" cellpadding="0" cellspacing="2" style="margin-left: 10px;">
            <tbody>
                <tr id="MainContent_tr_dmenu">
                    <td colspan="9">
                        <div>
                            <p>
                                Start by Adding Employee or Department. You can create as many employees and organize
                                them in department/sub department as you want.
                            </p>
                            <ul style="margin-left: 18px; padding: 11px">
                                <li>
                                    <%--OnClick="LnkAddContractClick"--%>
                                    <a href="#" id="lnkAdd_worker1" class="lnkAdd_worker1" style="color: red">Add Employee</a>
                                    OR
                                    <%--<asp:LinkButton ID="lnkAdd_worker1" Text="Add Worker"  runat="server"></asp:LinkButton>--%>
                                </li>
                                <li>
                                    <asp:LinkButton ID="lnkClockin" Text="Start by recording your clock-in/out time"
                                        runat="server"></asp:LinkButton>
                                    <%--<asp:LinkButton ID="lnkAdd_department1" Text="Add Department" runat="server" OnClick="LnkAddDepartmentClick"></asp:LinkButton>--%>
                                </li>
                            </ul>
                            <p>
                                For learning more about Time & Attendance please go to <a href="About.aspx">About</a>
                                or <a href="appinstruction.aspx">Help</a>.
                            </p>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div id="divClockedIn" title="Clocked In Successfully!" style="display: none">
        <p>
            You have successfully clocked in.
        </p>
        <br />
        <p>
            Please verify your location by clicking the link in your verification email sent
            to
            <asp:Label runat="server" ID="lblInEmail"></asp:Label>
        </p>
    </div>
    <div id="absComment" class="hide" title="Comment">
        <table>
            <tr>
                <td>
                    <textarea id="txtAbsComment" class="txtAbsComment" style="width: 333px; height: 57px;"
                        placeholder="Please add your comments here..."></textarea>
                </td>
            </tr>
        </table>
    </div>
    <div id="divWorkingHours" title="Working hours" style="display: none">
        <table class="profile" style="margin-top: 10px; padding: 0 13px">
            <tr>
                <td colspan="3" style="text-align: center">
                    <p>
                        Provide your working hours here
                    </p>
                </td>
            </tr>
            <tr>
                <th>
                </th>
                <th>
                    Clock in
                </th>
                <th>
                    Clock out
                </th>
                <th>
                    Active
                </th>
            </tr>
            <tr>
                <td class="captionauto">
                    Monday
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hdnMon" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnMonID" Value="0" />
                    <asp:TextBox ID="txtMonCI" runat="server" CssClass="txtMonCI datetime datetimeCI"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtMonCO" runat="server" CssClass="txtMonCO datetime datetimeCO"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkMon" runat="server" />
                </td>
                <td>
                    <img id="btnApplyToAll" class="btnApplyToAll" src="" alt="Apply to all" style="float: right;
                        margin-left: 5px; color: blue; cursor: pointer" />
                </td>
            </tr>
            <tr class="trAPC1">
                <td class="captionauto">
                    Tuesday
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hdnTue" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnTueID" Value="0" />
                    <asp:TextBox ID="txtTuesCI" runat="server" CssClass="txtTuesCI datetime datetimeCI"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTuesCO" runat="server" CssClass="txtTuesCO datetime datetimeCO"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkTue" runat="server" />
                </td>
            </tr>
            <tr class="trAPC1">
                <td class="captionauto">
                    Wednesday
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hdnWed" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnWedID" Value="0" />
                    <asp:TextBox ID="txtWedCI" runat="server" CssClass="txtWedCI datetime datetimeCI"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtWedCO" runat="server" CssClass="txtWedCO datetime datetimeCO"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkWed" runat="server" />
                </td>
            </tr>
            <tr class="trAPC1">
                <td class="captionauto">
                    Thursday
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hdnThu" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnThuID" Value="0" />
                    <asp:TextBox ID="txtThuCI" runat="server" CssClass="txtThuCI datetime datetimeCI"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtThuCO" runat="server" CssClass="txtThuCO datetime datetimeCO"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkThur" runat="server" />
                </td>
            </tr>
            <tr class="trAPC1">
                <td class="captionauto">
                    Friday
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hdnFri" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnFriID" Value="0" />
                    <asp:TextBox ID="txtFriCI" runat="server" CssClass="txtFriCI datetime datetimeCI"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtFriCO" runat="server" CssClass="txtFriCO datetime datetimeCO"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkFri" runat="server" />
                </td>
            </tr>
            <tr class="trAPC1">
                <td class="captionauto">
                    Saturday
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hdnSat" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnSatID" Value="0" />
                    <asp:TextBox ID="txtSatCI" runat="server" CssClass="txtSatCI datetime datetimeCI"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtSatCO" runat="server" CssClass="txtSatCO datetime datetimeCO"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkSat" runat="server" />
                </td>
            </tr>
            <tr class="trAPC1">
                <td class="captionauto">
                    Sunday
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hdnSun" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnSunID" Value="0" />
                    <asp:TextBox ID="txtSunCI" runat="server" CssClass="txtSunCI datetime datetimeCI"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtSunCO" runat="server" CssClass="txtSunCO datetime datetimeCO"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkSun" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="captionauto">
                    Autopresent:
                </td>
                <td>
                    <asp:CheckBox ID="chkAutoPresent" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: right">
                    <asp:Button ID="btnSaveWH" CssClass="btnSaveWH ui-state-default raisesevent" runat="server"
                        Text="Save" OnClick="btnSaveWH_Click" />
                    <asp:Button ID="btnCanceWH" CssClass="btnCanceWH ui-state-default" runat="server"
                        Text="Cancel" />
                </td>
            </tr>
        </table>
    </div>
    <asp:HiddenField ID="hdnCITime" runat="server" ClientIDMode="Static" />
    <asp:LinkButton runat="server" Text="Define working hours" ID="lnkWorkingHours" CssClass="lnkWorkingHours"
        ClientIDMode="Static" Style="display: none;"></asp:LinkButton>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    </form>
</body>
</html>
