﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LateReport.aspx.cs" Inherits="LateReport" %>
<%@ Register Src="~/Controls/LateReport.ascx" TagPrefix="uc1" TagName="LateReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Late report</title>
    <link rel="stylesheet" type="text/css" media="screen" href="_assets/style.css" />
    <link rel="stylesheet" href="_assets/jquery-ui.css" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="_assets/css/tooltipster.css" />
    <link href="_assets/tablestyles.css" rel="stylesheet" />
    <script type="text/javascript" src="_assets/js/jquery.tooltipster.js"></script>
    <link rel="stylesheet" href="_assets/jquery.timepicker.css" />
    <script src="_assets/jquery.timepicker.min.js"></script>
    <script src="_assets/DateTimeHelper.js"></script>
    <link href="_assets/jquery-ui-timepicker-addon.css" rel="stylesheet" />
    <script src="_assets/jquery-ui-timepicker-addon.js"></script>
    <script src="_assets/otf.js"></script>
    <link href="_assets/jquery.switchButton.css" rel="stylesheet" />
    <script src="_assets/jquery.switchButton.js"></script>
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:LateReport runat="server" id="LateReport1" />        
    </div>
    </form>
</body>
</html>
