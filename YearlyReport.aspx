﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="YearlyReport.aspx.cs" Inherits="YearlyReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" media="screen" href="_assets/style.css" />
    <link rel="stylesheet" href="_assets/jquery-ui.css" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="_assets/css/tooltipster.css" />
    <script type="text/javascript" src="_assets/js/jquery.tooltipster.js"></script>
    <link rel="stylesheet" href="_assets/jquery.timepicker.css" />
    <script src="_assets/jquery.timepicker.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <span>Select Year: </span>
            <div>
                <asp:DropDownList ID="year" runat="server"></asp:DropDownList>
                <asp:Button ID="btnGenRpt" runat="server" Text="Generate Report" OnClick="btnGenRpt_Click" />
            </div>
        </div>
        <div runat="server" id="rmaindiv" visible="false">
            <div style="text-align:center;font-size:16px; font-weight:bold;height:18px">
                <asp:Label ID="name" runat="server"></asp:Label> 's Report For Year <asp:Label ID="y" runat="server"></asp:Label>
            </div>
            <div>
                <asp:Repeater ID="rptReport" runat="server" OnItemDataBound="rptReport_ItemDataBound" OnItemCommand="rptReport_ItemCommand">
                    <HeaderTemplate>
                        <table class="smallGrid" style="width:795px; height:100%; margin:0 auto">
                            <tr class="smallGridHead">
                                <td>Month</td>
                                <td>Working Days</td>
                                <td>Absences</td>
                                <td>Holidays</td>
                                <td>Extra Days Worked</td>
                                <td></td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr>
                                <td><asp:Label ID="m" runat="server"></asp:Label></td>
                                <td><asp:Label ID="w" runat="server"></asp:Label></td>
                                <td><asp:Label ID="a" runat="server"></asp:Label></td>
                                <td><asp:Label ID="h" runat="server"></asp:Label></td>
                                <td><asp:Label ID="e" runat="server"></asp:Label></td>
                                <td><asp:LinkButton ID="d" runat="server" Text="Details" CommandName="det"></asp:LinkButton></td>
                            </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
