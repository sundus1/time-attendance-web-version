﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class workerverification : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Request["rowid"])) &&
                    !string.IsNullOrEmpty(Convert.ToString(Request["pid"])) && !string.IsNullOrEmpty(Convert.ToString(Request["iid"])))
                {
                    using (
                        SqlConnection con =
                            new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                    {
                        con.Open();
                        if (Request["action"].ToString() == "in")
                        {
                            using (SqlCommand cmd = new SqlCommand("setinstatusbyemail", con))
                            {
                                cmd.Parameters.AddWithValue("@id", Request["rowid"]);
                                cmd.Parameters.AddWithValue("@pid", Request["pid"]);
                                cmd.Parameters.AddWithValue("@inaddress", Request.UserHostAddress);
                                cmd.Parameters.AddWithValue("@instanceId", Request["iid"]);
                                cmd.CommandType = CommandType.StoredProcedure;
                                Response.Write(cmd.ExecuteScalar());
                            }
                        }
                        else if (Request["action"].ToString() == "out")
                        {
                            using (SqlCommand cmd = new SqlCommand("setoutstatusbyemail", con))
                            {
                                cmd.Parameters.AddWithValue("@id", Request["rowid"]);
                                cmd.Parameters.AddWithValue("@pid", Request["pid"]);
                                cmd.Parameters.AddWithValue("@outaddress", Request.UserHostAddress);
                                cmd.Parameters.AddWithValue("@instanceId", Request["iid"]);
                                cmd.CommandType = CommandType.StoredProcedure;

                                Response.Write(cmd.ExecuteScalar());

                            }
                        }
                    }

                }
                if (!IsPostBack)
                {
                    //&& !string.IsNullOrEmpty(Convert.ToString(Request["upid"]))
                    if (!string.IsNullOrEmpty(Convert.ToString(Request["uid"])) && !string.IsNullOrEmpty(Convert.ToString(Request["action"])) && !string.IsNullOrEmpty(Convert.ToString(Request["iid"])))
                    {
                        List<Absence> abs = Absence.GetAbsences(Convert.ToInt32(Request["uid"])).Where(u => u.Active == true).ToList();
                        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                        {
                            con.Open();
                            if (Request["action"].ToString() == "absent")
                            {
                                if (abs.Where(u => u.CrtDate.Date == DateTime.Now.Date).Count() > 0)
                                {
                                    Response.Write("<span style='font-size:18px;margin-top:5px'>You are already marked as absent on " + DateTime.Now.ToString("ddd, MMM dd yyyy") + "</span>");
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "", "AddLeave();", true);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("http://www.avaima.com/");
            }
        }
        catch (Exception ex)
        {

            throw;
        }
    }
    //SP.AddLeave("0",Convert.ToString(Request["userid"]), DateTime.Now.ToShortDateString(), "True", )
    protected void btnAddLeave_Click(object sender, EventArgs e)
    {
        SP.AddLeave("0", Convert.ToString(Request["uid"]), DateTime.Now.ToShortDateString(), "True", txtAbsComments.Text);
        AvaimaEmailAPI email = new AvaimaEmailAPI();
        StringBuilder subject = new StringBuilder();
        StringBuilder body = new StringBuilder();
        string workerName = SP.GetWorkerName(Convert.ToString(Request["uid"]));
        subject.Append(workerName + " requested for a leave on " + DateTime.Now.ToString("ddd, MMM dd yyyy"));
        body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
        body.Append("<b>" + workerName + "</b> requested a leave on " + DateTime.Now.ToString("ddd, MMM dd yyyy") + ".");
        body.Append(" Below is the description of his/her leave");
        body.Append("<ul style='list-style-type:circle'>");
        body.Append("<li>Leave Day: " + DateTime.Now.ToString("ddd, MMM dd yyyy") + "</li>");
        body.Append("<li>Reason: " + txtAbsComments.Text + "</li>");
        body.Append("</ul>");
        body.Append("Waiting for your approval");
        body.Append(Helper.AvaimaEmailSignature);
        body.Append("</div>");
        string strbody = body.ToString();
        List<int> adminIds = SP.GetAdminIDByUserID(Convert.ToInt32(Request["uid"]), Convert.ToString(Request["iid"]));
        foreach (int adminID in adminIds)
        {
            email.send_email(SP.GetEmailByUserID(adminID), "Attendence Management", SP.GetEmailByUserID(Convert.ToInt32(Convert.ToString(Request["uid"]))), body.ToString(), subject.ToString());
        }
        Response.Write("<span style='font-size:18px;margin-top:5px'>you have been marked as abscent today.</span>");
    }
}