﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AvaimaThirdpartyTool;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Text;
using MyAttSys;


public partial class _Default : AvaimaWebPage
{
    private Dictionary<string, string> _breadcrumdict = new Dictionary<string, string>();
    private bool IsSimpleClock = true;
    private bool IsAdmin = false;
    private string OwnerId = "";
    private int appAssignedId = 0;
    private Boolean isNew = false;
    public String _InstanceID { get; set; }
    //public bool firstTime = true;
    //private bool AllowSplashScreen(int userId, string instanceId)
    //{
    //    //Session["ContinueUrl"] = HttpContext.Current.Request.Url.AbsoluteUri;
    //    DataTable dt = new DataTable();
    //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
    //    {
    //        using (SqlCommand cmd = new SqlCommand("sp_GetUserSettings", con))
    //        {
    //            con.Open();
    //            cmd.Parameters.AddWithValue("@UserId", userId);
    //            cmd.Parameters.AddWithValue("@InstanceId", instanceId);
    //            cmd.CommandType = CommandType.StoredProcedure;
    //            SqlDataAdapter adp = new SqlDataAdapter(cmd);
    //            adp.Fill(dt);
    //        }
    //    }
    //    if (dt == null)
    //    { return false; }
    //    else
    //    {
    //        if (dt.Rows.Count > 0)
    //        {
    //            return Convert.ToBoolean(dt.Rows[0]["SplashScreen"].ToString());//False==show screen
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }
    //}
    private void UpdateFirstTime(int userId, string instanceId)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_UpdateUserSettings", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@InstanceId", instanceId);
                cmd.Parameters.AddWithValue("@popup", false);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }
    }
    private bool UserFirstTime(int userId, string instanceId)
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_GetUserSettings", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@InstanceId", instanceId);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }
        }
        if (dt == null)
        { return true; }
        else
        {
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["popup"] == null || dt.Rows[0]["popup"].ToString() == "")
                {
                    return true;
                }
                else
                {
                    return Convert.ToBoolean(dt.Rows[0]["popup"].ToString());//False==show screen
                }
            }
            else
            {
                return true;
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
       // ScriptManager.RegisterStartupScript(this, GetType(), "", "openHowToAddEmployee();", true);
        hdnCITime.Value = App.GetTime(DateTime.Now.TimeOfDay.ToString(), HttpContext.Current.User.Identity.Name);
		
        if (_InstanceID == "0") { _InstanceID = App.InstanceID; }
        else { _InstanceID = this.InstanceID; }

        lblDate.Text = DateTime.Now.ToString();

        AddinstanceWS objinst = new AddinstanceWS();
        OwnerId = App.GetAVAIMAUserID(App.Roles.SUPER_ADMIN);
        lbl.Text = OwnerId;


        //List<WorkingHour> whList = WorkingHour.GetWorkingHours(0);
        //whList = whList.Where(u => u.InstanceID == this.InstanceID).ToList();
        //if (whList.Count <= 0)
        //{
        //    this.Redirect("Welcome.aspx?InstanceID=" + InstanceID);
        //    ScriptManager.RegisterStartupScript(this, GetType(), "Opentheworkingdiv", "Workinghour();", true);
        //}


        workeruserprofile.InstanceID = _InstanceID;
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("getuserrole", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@userid", OwnerId.Trim());
                cmd.Parameters.AddWithValue("@instanceid", _InstanceID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    hdnAdminName.Value = SP.GetWorkerName(dt.Rows[0][1].ToString());
                    hdnAdminEmail.Value = SP.GetEmailByUserID(Convert.ToInt32(dt.Rows[0][1].ToString()));

                    if (Convert.ToString(dt.Rows[0]["role"]) == "worker" || Convert.ToString(dt.Rows[0]["role"]) == "admin")
                    {
                        if (Convert.ToString(dt.Rows[0]["role"]) == "admin")
                        {

                            IsAdmin = true;
                            appAssignedId = Convert.ToInt32(dt.Rows[0]["userid"]);
                            MyAttendance.Visible = true;
                            hfparentid.Value = SP.GetAdminCatID(Convert.ToInt32(dt.Rows[0]["userid"].ToString())).ToString();
                            if (UserFirstTime(appAssignedId, _InstanceID) == true)
                            {
                                //if (AllowSplashScreen(appAssignedId, _InstanceID) == false)
                                //{
                                //    Response.Redirect("StartUp.aspx?UserId=" + appAssignedId + "&InstanceId=" + _InstanceID);
                                //}
                                ScriptManager.RegisterStartupScript(this, GetType(), "", "showFirstTime();", true);
                                UpdateFirstTime(appAssignedId, _InstanceID);

                            }
                        }
                        else
                        {
                            attendancetype objattendancetype = attendancetype.SingleOrDefault(u => u.userid == dt.Rows[0]["userid"] && u.instanceid == this.InstanceID);
                            if (objattendancetype != null)
                            {
                                if (objattendancetype.userattendType == 2)
                                {
                                    this.Redirect("Flexibleworker.aspx?id=" + dt.Rows[0]["userid"]);
                                }
                                else
                                {
                                    this.Redirect("Add_worker.aspx?id=" + dt.Rows[0]["userid"]);
                                }
                            }
                            else
                            {
                                this.Redirect("Add_worker.aspx?id=" + dt.Rows[0]["userid"]);
                            }
                        }
                    }
                    else
                    {
                        //IsAdmin = true;
                        appAssignedId = Convert.ToInt32(dt.Rows[0]["userid"]);
                        if (UserFirstTime(appAssignedId, _InstanceID) == true)
                        {

                            //    if (AllowSplashScreen(appAssignedId, _InstanceID) == false)
                            //    {
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "showFirstTime();", true);
                            UpdateFirstTime(appAssignedId, _InstanceID);
                            //        Response.Redirect("StartUp.aspx?UserId=" + appAssignedId + "&InstanceId=" + _InstanceID);
                            //    }
                        }
                    }
                }
                // Intance Dependent Code - UnCommented
                else
                {
                    AvaimaUserProfile objwebser = new AvaimaUserProfile();
                    //string[] user = objwebser.Getonlineuserinfo(this.Context.User.Identity.Name).Split('#');
                    string[] user = objwebser.Getonlineuserinfo(OwnerId).Split('#');
                    using (SqlCommand cmd1 = new SqlCommand("insertworker", con))
                    {
                        string title = user[0] + " " + user[1];
                        cmd1.Parameters.AddWithValue("@title", title);
                        cmd1.Parameters.AddWithValue("@parentid", 0);
                        cmd1.Parameters.AddWithValue("@InstanceId", _InstanceID);
                        cmd1.Parameters.AddWithValue("@datefield", Convert.ToDateTime(DateTime.Now));
                        cmd1.Parameters.AddWithValue("@category", 0);
                        cmd1.Parameters.AddWithValue("@email", user[2]);
                        cmd1.Parameters.AddWithValue("@active", true);

                        cmd1.CommandType = CommandType.StoredProcedure;
                        string id = cmd1.ExecuteScalar().ToString();
                        Insertuserrole(OwnerId, "superuser", user[2], id);
                        try
                        {
                            objwebser.insertUserFull(OwnerId, user[3], user[2], "", "", "", "", "", "", user[0], user[1], "", "", "", "", "", "", "", "", id, testInstanceID, "");
                        }
                        catch (Exception ex)
                        {

                        }
                        isNew = true;
                    }
                }
                ////end

            }
            if (!IsPostBack)
            {
                try
                {
                    if (!String.IsNullOrEmpty(Request["allowed"].ToString()))
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('You are not allowed to view this page');", true);
                    }
                }
                catch (Exception)
                { }

            }
            //ScriptManager.RegisterStartupScript(this, GetType(), "", "showFirstTime();", true);
        }

        pg_Control.OnPageIndex_Changed += pg_rptrcontract_OnPageIndex_Changed;
        if (!IsPostBack)
        {
            FillAttendance();
        }
        if (_breadcrumdict.Count == 0)
        {
            root.Visible = false;
        }
        else
        {
            root.Visible = true;
        }
    }
    private Boolean ValidateDayText(TextBox txtCI, TextBox txtCO)
    {
        if (String.IsNullOrEmpty(txtCI.Text) || String.IsNullOrEmpty(txtCO.Text))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    protected void btnSaveWH_Click(object sender, EventArgs e)
    {
        AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
        WorkingHour wh = new WorkingHour();
        if (ValidateDayText(txtMonCI, txtMonCO))
        {
            wh = new WorkingHour();
            wh.WorkHourID = Convert.ToInt32(hdnMonID.Value);
            wh.Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtMonCI.Text, this.InstanceID));
            wh.Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtMonCO.Text, this.InstanceID));
            wh.Active = chkMon.Checked;
            wh.AutoPresent = chkAutoPresent.Checked;
            wh.crtDate = DateTime.Now;
            wh.DayTitle = "Monday";
            wh.UserID = 0;
            wh.InstanceID = this.InstanceID;
            wh.Save();
        }

        if (ValidateDayText(txtTuesCI, txtTuesCO))
        {
            wh = new WorkingHour()
            {
                WorkHourID = Convert.ToInt32(hdnTueID.Value),
                Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtTuesCI.Text, InstanceID)),
                Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtTuesCO.Text, InstanceID)),
                Active = chkTue.Checked,
                AutoPresent = chkAutoPresent.Checked,
                crtDate = DateTime.Now,
                DayTitle = "Tuesday",
                InstanceID = this.InstanceID,
                UserID = 0
            };
            wh.Save();
        }

        if (ValidateDayText(txtWedCI, txtWedCO))
        {
            wh = new WorkingHour()
            {
                WorkHourID = Convert.ToInt32(hdnWedID.Value),
                Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtWedCI.Text, InstanceID)),
                Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtWedCO.Text, InstanceID)),
                Active = chkWed.Checked,
                AutoPresent = chkAutoPresent.Checked,
                crtDate = DateTime.Now,
                DayTitle = "Wednesday",
                InstanceID = this.InstanceID,
                UserID = 0

            };
            wh.Save();
        }

        if (ValidateDayText(txtThuCI, txtThuCO))
        {
            wh = new WorkingHour()
            {
                WorkHourID = Convert.ToInt32(hdnThuID.Value),
                Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtThuCI.Text, InstanceID)),
                Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtThuCO.Text, InstanceID)),
                Active = chkThur.Checked,
                AutoPresent = chkAutoPresent.Checked,
                crtDate = DateTime.Now,
                DayTitle = "Thursday",
                InstanceID = this.InstanceID,
                UserID = 0
            };
            wh.Save();
        }

        if (ValidateDayText(txtFriCI, txtFriCO))
        {
            wh = new WorkingHour()
            {
                WorkHourID = Convert.ToInt32(hdnFriID.Value),
                Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtFriCI.Text, InstanceID)),
                Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtFriCO.Text, InstanceID)),
                Active = chkFri.Checked,
                AutoPresent = chkAutoPresent.Checked,
                crtDate = DateTime.Now,
                DayTitle = "Friday",
                InstanceID = this.InstanceID,
                UserID = 0
            };
            wh.Save();
        }

        if (ValidateDayText(txtSatCI, txtSatCO))
        {
            wh = new WorkingHour()
            {
                WorkHourID = Convert.ToInt32(hdnSatID.Value),
                Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtSatCI.Text, InstanceID)),
                Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtSatCO.Text, InstanceID)),
                Active = chkSat.Checked,
                AutoPresent = chkAutoPresent.Checked,
                crtDate = DateTime.Now,
                DayTitle = "Saturday",
                InstanceID = this.InstanceID,
                UserID = 0
            };
            wh.Save();

        }

        if (ValidateDayText(txtSunCI, txtSunCO))
        {
            wh = new WorkingHour()
            {
                WorkHourID = Convert.ToInt32(hdnSunID.Value),
                Clockin = Convert.ToDateTime(atz.GetTimeReverse(txtSunCI.Text, InstanceID)),
                Clockout = Convert.ToDateTime(atz.GetTimeReverse(txtSunCO.Text, InstanceID)),
                Active = chkSun.Checked,
                AutoPresent = chkAutoPresent.Checked,
                crtDate = DateTime.Now,
                DayTitle = "Sunday",
                InstanceID = this.InstanceID,
                UserID = 0
            };
            wh.Save();
        }
        if (chkAutoPresent.Checked)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "", "StartAutoPresent();", true);
        }
        //SetWorkingHours();
    }
    private void FillAttendance()
    {
        RptrAttendanceFill(0, IsAdmin);
        hfparentid.Value = "0";
        TreeNode parentNode = new TreeNode(String.Format("<input type='radio' name='rbtncat' value='Root' onclick='setvalue(0)'/>&nbsp; Root"));
        parentNode.SelectAction = TreeNodeSelectAction.Expand;
        FillTreeView(parentNode.ChildNodes, "0");
        TreeView1.Nodes.Add(parentNode);
        pg_Control.CurrentPage = 0;
    }

    protected void RptrAttendanceFill(int pid, bool isAdmin = false, bool ShowFiltered = false, int status = 1)
    {
        DataTable dtstudentall = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            string q = "getallattendencerecordbyid";
            if (ShowFiltered)
            {
                if (status == 1)
                    q = "getallactiveusers";
                else
                {
                    q = "getallinactiveusers";
                }
            }
            else
            {
                if (isAdmin)
                {
                    q = "getfilteredattendancerecordbyid";
                }
            }

            using (SqlCommand cmd = new SqlCommand(q, con))
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", pid);
                cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                if (isAdmin)
                {
                    if (q == "getfilteredattendancerecordbyid")
                    {
                        cmd.Parameters.AddWithValue("@userid", appAssignedId);
                    }
                    foldrOp.Visible = false;
                }
                fid.Value = pid.ToString();
                if (pid == 0)
                {
                    foldrOp.Visible = false;
                }
                else
                {
                    foldrOp.Visible = true;
                    FolderSettings(pid);
                }
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dtstudentall);
                cmd.CommandText = "getclocktype";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@instanceId", _InstanceID);
                bool i = false;

                try
                {
                    i = (bool)cmd.ExecuteScalar();
                }
                catch (Exception)
                { }

                if (i)
                {
                    IsSimpleClock = false;
                }

                PagedDataSource pds = new PagedDataSource();
                pds.AllowPaging = true;
                pds.DataSource = dtstudentall.DefaultView;
                pds.CurrentPageIndex = pg_Control.CurrentPage;
                pds.PageSize = pg_Control.PageSize;
                pg_Control.TotalRecords = dtstudentall.Select().Where(x => x.Field<bool>("category") != true).Count();
                if (dtstudentall.Rows.Count > 0)
                {
                    Rptrattendence.DataSource = pds;
                    autopresences = AutoPresent.GetAll();
                    Rptrattendence.DataBind();

                    trmainbc.Visible = trmainfunction.Visible = main.Visible = true;
                    if (isNew)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "showFirstTime();", true);
                    }
                    divfuntions.Visible = true;
                    Rptrattendence.Visible = true;
                    pg_Control.LoadStatus();
                }
                else
                {
                    Rptrattendence.DataSource = pds;
                    autopresences = AutoPresent.GetAll();
                    Rptrattendence.DataBind();

                    if (ShowFiltered)
                    {
                        trmainbc.Visible = trmainfunction.Visible = main.Visible = true;
                        //main2.Visible = isNew;
                        if (isNew)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "showFirstTime();", true);
                        }
                        lblemptygrid.Visible = true;
                    }
                    else
                    {
                        lblemptygrid.Visible = false;
                        //trmainbc.Visible = trmainfunction.Visible = main.Visible = false;
                        //main2.Visible = true;
                        if (isNew)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "showFirstTime();", true);
                        }
                    }
                    //divfuntions.Visible = false;
                    if (pid == 0)
                    {
                        topDiv.Visible = false;
                    }
                    Rptrattendence.Visible = false;
                    pg_Control.LoadStatus();
                }
            }
        }
    }

    private void FolderSettings(int id)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("getinstanceusers", con))
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CatId", id);
                cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                userList.DataSource = dt;
                userList.DataTextField = "Title";
                userList.DataValueField = "ID";
                userList.DataBind();
                userList.Items.Insert(0, "Select Employee");
                clsbtn.Style.Remove("display");
                cmd.Parameters.Clear();
                cmd.CommandText = "getadminsbycategoryId";
                cmd.Parameters.AddWithValue("@Id", id);
                cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                adp = new SqlDataAdapter(cmd);
                dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    admins.DataSource = dt;
                    admins.DataBind();
                    noAdmin.Visible = false;
                }
                else
                {
                    admins.DataSource = dt;
                    admins.DataBind();
                    noAdmin.Visible = true;
                }
            }
        }
    }

    private bool UserExists(string email)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("select count(*) from attendence_management a inner join tbluserrole b on a.Id=b.userid where a.email = @email and b.instanceid = @insId", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@insId", _InstanceID);
                return ((int)cmd.ExecuteScalar() > 0);
            }
        }
    }

    public void Insertuserrole(string userid, string role, string email, string ids = "")
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("adduserrole", con))
                {
                    con.Open();
                    if (ids == "")
                    {
                        cmd.Parameters.AddWithValue("@id", Request["id"]);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@id", ids);
                    }

                    cmd.Parameters.AddWithValue("@role", role);
                    cmd.Parameters.AddWithValue("@userid", userid);
                    cmd.Parameters.AddWithValue("@email", email);
                    cmd.Parameters.AddWithValue("@instanceid", _InstanceID);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
        }
        catch (Exception ex)
        {

            throw;
        }
    }

    protected void BtnsendcontrorinfoClick(object sender, EventArgs e)
    {
        Attendance atd = new Attendance();
        string userid = "0";

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("Select * from attendence_management  where email = @email and InstanceId =  @instanceid", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@email", hdnAdminEmail.Value);
                cmd.Parameters.AddWithValue("@instanceid", _InstanceID);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    userid = dt.Rows[0]["ID"].ToString();
                }
            }
        }

        atd.AddEmployee(txtFirstName.Text, txtLastName.Text, txtEmail.Text, userid, HttpContext.Current.User.Identity.Name, _InstanceID, true);
        //if (!UserExists(txtEmail.Text))
        //{
        //    LnkAddContractClick(sender, e);

        //    Hashtable objuser = new Hashtable();
        //    AddinstanceWS objaddinst = new AddinstanceWS();
        //    string userid = objaddinst.AddInstance(txtEmail.Text, "6e815b00-6e13-4839-a57d-800a92809f21", _InstanceID, HttpContext.Current.User.Identity.Name);
        //    AvaimaUserProfile objwebser = new AvaimaUserProfile();
        //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        //    {
        //        using (SqlCommand cmd = new SqlCommand("select count(*) from tbluserrole where ownerid = @id and instanceid = @insId", con))
        //        {
        //            con.Open();
        //            cmd.Parameters.AddWithValue("@id", userid);
        //            cmd.Parameters.AddWithValue("@insId", _InstanceID);
        //            int i = (int)cmd.ExecuteScalar();
        //            if (i > 0)
        //            {
        //                cmd.Parameters.Clear();
        //                cmd.CommandText = "del_records";
        //                cmd.Connection = con;
        //                cmd.Parameters.AddWithValue("@Pid", workeruserprofile.wid);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.ExecuteNonQuery();

        //                // to be continued
        //                ScriptManager.RegisterStartupScript(this, GetType(), "", "alreadyExists();", true);
        //            }
        //            else
        //            {
        //                objuser.Add("email", txtEmail.Text);
        //                objwebser.insertUserFull(userid, "", objuser["email"].ToString(), "", "", "",
        //                                     "", "", "", txtFirstName.Text, txtLastName.Text, "", "", "", "", "", "", "", "", workeruserprofile.wid, _InstanceID, "");
        //                Insertuserrole(userid, "worker", objuser["email"].ToString(), id.Value);
        //                AvaimaEmailAPI email = new AvaimaEmailAPI();
        //                //email.Adduseremail(HttpContext.Current.User.Identity.Name, userid, objuser["email"].ToString(), "Attendance Management");
        //                StringBuilder subject = new StringBuilder();
        //                StringBuilder body = new StringBuilder();
        //                subject.Append(hdnAdminName.Value + " wants you to use Time & Attendance");
        //                body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
        //                body.Append("<b>" + hdnAdminName.Value + "</b> wants you to use \"Time & Attendance\" software application on Avaima. You can use this application to clock-in / clock-out on a daily basis using your own user-account. Follow the steps below to start using the application:");
        //                //body.Append("</br> He added you in this application and wants you to clock-in / clock-out on a daily basis.");
        //                //body.Append("</br> Follow the steps below to start using the application:");
        //                body.Append("<ul>");
        //                body.Append("<li>In another email you should have received request to sign up on Avaima.com. Follow the instructions and sign up on Avaima &ndash; sign up is <b>FREE</b>. "
        //                    + "If you did not receive this email, go to Avaima.com and sign up using " + objuser["email"].ToString() + " as your email address</li>");
        //                body.Append("<li>Go to <a href='http://www.avaima.com/signin'>www.avaima.com/signin</a> and login to your account</li>");
        //                body.Append("<li>Find and click \"Time & Attendance\" in the left menu</li>");
        //                body.Append("<li>\"Clock-In\" when you start your day at work and \"Clock-Out\" when you are done</li>");
        //                body.Append("</ul>");
        //                body.Append("For further questions, comments or help contact support@avaima.com.");
        //                body.Append(Helper.AvaimaEmailSignature);
        //                body.Append("</div>");
        //                email.send_email(objuser["email"].ToString(), "Attendance Management", "", body.ToString(), subject.ToString());
        //                if (string.IsNullOrEmpty(hfparentid.Value) || string.IsNullOrWhiteSpace(hfparentid.Value))
        //                {
        //                    hfparentid.Value = "0";
        //                }
        //                RptrAttendanceFill(Convert.ToInt32(hfparentid.Value), IsAdmin);
        //                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Worker added successfully.');", true);
        //            }
        //        }
        //    }
        //    txtEmail.Text = "";
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "", "alreadyExists()", true);
        //    txtEmail.Text = "";
        //    //ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('User already exist in this instance.');", true);
        //}
    }

    protected void Breadcrum(int id)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("getidforbreadcrum", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                cmd.CommandType = CommandType.StoredProcedure;
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    _breadcrumdict.Add(Convert.ToString(dt.Rows[0]["ID"]), Convert.ToString(dt.Rows[0]["title"]));
                    if (Convert.ToInt32(dt.Rows[0]["P_ID"]) != 0)
                    {
                        Breadcrum(Convert.ToInt32(dt.Rows[0]["P_ID"]));
                    }
                }
            }
        }
    }

    protected void Bredcrumbind()
    {
        var revbreadcrum = _breadcrumdict.Reverse();
        rptBreadCrum.DataSource = revbreadcrum;
        rptBreadCrum.DataBind();
        if (revbreadcrum.Count() == 0)
        {
            rptBreadCrum.Visible = false;
        }
        else
        {
            rptBreadCrum.Visible = true;
            root.Visible = true;
        }
    }

    public void FillTreeView(TreeNodeCollection treenode, string parentid)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlDataAdapter adp = new SqlDataAdapter("gettreeviewid", con))
            {
                con.Open();
                adp.SelectCommand.Parameters.AddWithValue("@id", parentid);
                adp.SelectCommand.Parameters.AddWithValue("@InstanceId", _InstanceID);
                adp.SelectCommand.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        TreeNode node = new TreeNode(String.Format("<input type='radio' name='rbtncat' value='{0}' onclick='setvalue({0})'/>&nbsp;{1}", dr["ID"].ToString(), dr["Title"].ToString()));
                        //TreeNode node = new TreeNode(dr["Title"].ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        FillTreeView(node.ChildNodes, dr["ID"].ToString());
                        treenode.Add(node);
                    }
                }
            }
        }
    }

    protected void LnkAddContractClick(object sender, EventArgs e)
    {
        try
        {
            string s = hfparentid.Value;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                //using (SqlCommand cmd = new SqlCommand("insertworker", con))  //Old SP without parent-userid
                using (SqlCommand cmd = new SqlCommand("sp_insertworker", con))
                {
                    string title = txtFirstName.Text + " " + txtLastName.Text;
                    con.Open();
                    cmd.Parameters.AddWithValue("@title", title);
                    cmd.Parameters.AddWithValue("@parentid", s);
                    cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                    cmd.Parameters.AddWithValue("@datefield", Convert.ToDateTime(DateTime.Now));
                    cmd.Parameters.AddWithValue("@category", 0);
                    cmd.Parameters.AddWithValue("@email", txtEmail.Text);
                    cmd.Parameters.AddWithValue("@active", true);
                    cmd.CommandType = CommandType.StoredProcedure;
                    id.Value = cmd.ExecuteScalar().ToString();
                    workeruserprofile.wid = id.Value;
                    workeruserprofile.InstanceID = _InstanceID;
                    RptrAttendanceFill(Convert.ToInt32(s), IsAdmin);
                    //ScriptManager.RegisterStartupScript(this, GetType(), "", "openAdd();", true);
                }
            }
        }
        catch (Exception ex)
        {
            lblexception.Text = ex.Message;
        }


    }

    protected void LnkAddDepartmentClick(object sender, EventArgs e)
    {

    }

    protected void LbtndelClick(object sender, EventArgs e)
    {
        var ids = (from r in Rptrattendence.Items.Cast<RepeaterItem>()
                   let selected = (r.FindControl("chkselecteditem") as CheckBox).Checked
                   let id = (r.FindControl("hf_ID") as HiddenField).Value
                   let userrole = (r.FindControl("hf_userrole") as HiddenField).Value
                   where selected && userrole != "superuser"
                   select id).ToList();
        bool showError = false;
        bool showSupAderr = false;
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            con.Open();
            foreach (var id in ids)
            {
                if (appAssignedId == Convert.ToInt32(id))
                {
                    showError = true;
                    continue;
                }
                //continue;
                //return;
                AvaimaUserProfile obj = new AvaimaUserProfile();
                using (SqlCommand cmd1 = new SqlCommand("select ownerid from tbluserrole where userid=@id", con))
                {
                    cmd1.Parameters.AddWithValue("@id", id);
                    obj.Deletecompanyuser(Convert.ToString(cmd1.ExecuteScalar()));
                    AddinstanceWS objinst = new AddinstanceWS();
                    objinst.Deleteinstance(Convert.ToString(cmd1.ExecuteScalar()), _InstanceID, "6e815b00-6e13-4839-a57d-800a92809f21");
                    SqlCommand cmd = new SqlCommand("del_records", con);
                    cmd.Parameters.AddWithValue("@Pid", id);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    cmd.CommandText = "delete from tbluserrole where userid=@id and instanceid=@instanceid";
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@instanceid", _InstanceID);
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                    MyAttSys.avaimaTest0001DB db = new MyAttSys.avaimaTest0001DB();
                    db.DeleteEmployeeCompletely(Convert.ToInt32(id), InstanceID);
                }
            }
        }
        if (showError)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('You could not delete yourself.');", true);
        }
        if (showSupAderr)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Super admin cannot be deleted.');", true);
        }
        RptrAttendanceFill(Convert.ToInt32(hfparentid.Value), IsAdmin);
        Breadcrum(Convert.ToInt32(hfparentid.Value));
        Bredcrumbind();
    }

    protected void RootClick(object sender, EventArgs e)
    {
        divfuntions.Style.Add("display", "");
        _breadcrumdict.Clear();
        Bredcrumbind();
        RptrAttendanceFill(0, IsAdmin);
    }

    protected void BtndeptinsertClick(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("insertdepartment", con))
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@parentid", hfparentid.Value);
                cmd.Parameters.AddWithValue("@title", txttitle.Text);
                cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                cmd.Parameters.AddWithValue("@datefield", DateTime.Now);
                cmd.Parameters.AddWithValue("@category", "1");
                int a = Convert.ToInt32(cmd.ExecuteScalar());
            }
        }
        RptrAttendanceFill(Convert.ToInt32(hfparentid.Value), IsAdmin);
        TreeView1.Nodes.Clear();
        TreeNode parentNode = new TreeNode(String.Format("<input type='radio' name='rbtncat' value='Root' onclick='setvalue(0)'/>&nbsp; Root"));
        parentNode.SelectAction = TreeNodeSelectAction.Expand;
        FillTreeView(parentNode.ChildNodes, "0");
        TreeView1.Nodes.Add(parentNode);
        TreeView1.ExpandAll();
    }

    protected void RptrattendenceItemcommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "openpopup")
        {
            LinkButton lnk = e.Item.FindControl("lnktitle") as LinkButton;

            attendancetype objattendancetype = attendancetype.SingleOrDefault(u => u.userid == lnk.CommandArgument && u.instanceid == this.InstanceID);
            if (objattendancetype != null)
            {
                if (objattendancetype.userattendType == 2)
                {
                    this.Redirect("Flexibleworker.aspx?id=" + lnk.CommandArgument);
                }
                else
                {
                    this.Redirect("Add_worker.aspx?id=" + lnk.CommandArgument);
                }
            }
            else
            {
                this.Redirect("Add_worker.aspx?id=" + lnk.CommandArgument);
            }
        }
        else if (e.CommandName == "search")
        {
            LinkButton lnk = e.Item.FindControl("lnktitle") as LinkButton;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (!string.IsNullOrEmpty(lnk.Text))
                    hfparentid.Value = e.CommandArgument.ToString();
                RptrAttendanceFill(Convert.ToInt32(e.CommandArgument.ToString()), IsAdmin);
                Breadcrum(Convert.ToInt32(e.CommandArgument.ToString()));
                Bredcrumbind();
            }
        }

        else if (e.CommandName == "settings")
        {
            ImageButton btnSettings = e.Item.FindControl("btnSettings") as ImageButton;
            fid.Value = btnSettings.CommandArgument;
            FolderSettings(Convert.ToInt32(fid.Value));
            clsbtn.Style.Add("display", "none");
            ScriptManager.RegisterStartupScript(this, GetType(), "", "settings();", true);
        }

        else if (e.CommandName == "ren")
        {
            ImageButton btnRename = e.Item.FindControl("btnRename") as ImageButton;
            deptId.Value = btnRename.CommandArgument;
            ScriptManager.RegisterStartupScript(this, GetType(), "", "ren();", true);
        }
        else if (e.CommandName == "SignIn")
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("insertuserintime", con))
                    {
                        con.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ppid", e.CommandArgument);
                        cmd.Parameters.AddWithValue("@userid", "uid");
                        cmd.Parameters.AddWithValue("@lastin", Convert.ToDateTime(DateTime.Now));
                        cmd.Parameters.AddWithValue("@inaddress", Request.UserHostAddress);
                        cmd.Parameters.AddWithValue("@comment", "Clock-In by admin");
                        cmd.Parameters.AddWithValue("@location", "2");
                        int rowid = Convert.ToInt32(cmd.ExecuteScalar());
                        //Session["siID"] = rowid;
                        //Session["soID"] = null;
                        //string messagebody = "<a href='http://www.avaima.com/714fcd17-288b-4725-a0c2-7641c997eda4/workerverification.aspx?action=in&rowid=" + rowid + "&pid=" + UserId + "&iid=" + _InstanceID + "'>Click here </a> to verify signin time for" + DateTime.Now.ToString("MM/dd/yyyy") + ". This email is valid for 15 min after the time of delieverd";


                        using (SqlCommand cmd1 = new SqlCommand("UPDATE attendence_management SET datefield=@date WHERE ID=@id", con))
                        {
                            cmd1.Parameters.AddWithValue("@date", Convert.ToDateTime(DateTime.Now));
                            cmd1.Parameters.AddWithValue("@id", e.CommandArgument);
                            cmd1.ExecuteNonQuery();
                        }

                        FillAttendance();
                        //AvaimaEmailAPI email = new AvaimaEmailAPI();
                        //email.send_email(dtcontractors.Rows[0]["email"].ToString(), "Worker", "", messagebody, "Verification signin email");

                        //btnsignin.Attributes.Add("Disabled", "Disabled");
                    }
                }

            }
            catch (Exception ex)
            {
                //this.Redirect("Add_worker.aspx?id=" + UserId);
            }
        }
        else if (e.CommandName == "SignOut")
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("insertuserouttime", con))
                    {
                        con.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ppid", e.CommandArgument);
                        cmd.Parameters.AddWithValue("@rowid", "0");
                        cmd.Parameters.AddWithValue("@lastout", Convert.ToDateTime(DateTime.Now));
                        cmd.Parameters.AddWithValue("@outaddress", Request.UserHostAddress);
                        cmd.Parameters.AddWithValue("@comment", "Clock-Out by admin");
                        cmd.Parameters.AddWithValue("@location", "2");
                        int rowid = Convert.ToInt32(cmd.ExecuteScalar());
                        using (SqlCommand cmd1 = new SqlCommand("UPDATE attendence_management SET datefield=@date WHERE ID=@id", con))
                        {
                            cmd1.Parameters.AddWithValue("@date", Convert.ToDateTime(DateTime.Now));
                            cmd1.Parameters.AddWithValue("@id", e.CommandArgument);
                            cmd1.ExecuteNonQuery();
                        }
                        FillAttendance();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }

    protected void adminItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "delAdmin" && !string.IsNullOrEmpty(hfparentid.Value))
        {
            LinkButton lnk = e.Item.FindControl("delAdmin") as LinkButton;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("delcategoryadmin", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CatId", hfparentid.Value);
                cmd.Parameters.AddWithValue("@UserId", lnk.CommandArgument);
                cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                cmd.ExecuteNonQuery();
                this.Redirect("Default.aspx");

                SendRoleUpdateEmail(false);
            }
        }
    }

    protected void btnupdate_Click(object sender, EventArgs e)
    {
        string s = deptId.Value;
        int id = 0;
        if (!string.IsNullOrEmpty(s) && !string.IsNullOrWhiteSpace(s) && int.TryParse(s, out id))
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("update attendence_management set Title = @title where Id = @Id and category = @category and InstanceId = @InstanceId", con);
                cmd.Parameters.AddWithValue("@title", (!string.IsNullOrEmpty(txtrtitle.Text) && !string.IsNullOrWhiteSpace(txtrtitle.Text)) ? txtrtitle.Text : "not assigned");
                cmd.Parameters.AddWithValue("@Id", s);
                cmd.Parameters.AddWithValue("@category", true);
                cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                cmd.ExecuteNonQuery();
                txtrtitle.Text = "";
                RptrAttendanceFill(Convert.ToInt32(hfparentid.Value), IsAdmin);
            }
        }
    }

    protected void Rprteattendenceitemdatabound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
            object container = e.Item.DataItem;
            string userId = DataBinder.Eval(e.Item.DataItem, "ID").ToString();
            Boolean userStatus = SP.IsWorkerActive(Convert.ToInt32(userId));
            //for hours
            LinkButton title = e.Item.FindControl("lnktitle") as LinkButton;
            Label lblcount = e.Item.FindControl("lblcount") as Label;
            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(container, "Title"))))
            {
                string t = Convert.ToString(DataBinder.Eval(container, "Title"));
                title.Text = t;
                if (!userStatus)
                {
                    lblcount.Text = " - Deactivated on " + SP.GetWorkerLastAMEdit(Convert.ToInt32(userId)).ToString("ddd, MMM dd yyyy");
                }
            }

            Label lblrole = e.Item.FindControl("lblrole") as Label;
            if (DataBinder.Eval(e.Item.DataItem, "role").ToString() == "worker")
            {
                lblrole.Text = "";
            }
            else
            {
                if (DataBinder.Eval(e.Item.DataItem, "role").ToString() == "superuser")
                {
                    lblrole.Text = " - superAdmin";
                }
                else
                {
                    lblrole.Text = " - " + DataBinder.Eval(e.Item.DataItem, "role").ToString();
                }

            }
            Label lblworkhours = e.Item.FindControl("lblworkhours") as Label;
            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(container, "hours"))))
            {
                lblworkhours.Text = DataBinder.Eval(container, "hours").ToString();
            }


            HtmlControl trdata = e.Item.FindControl("trdata") as HtmlControl;
            List<Absence> absences = Absence.GetAbsences(Convert.ToInt32(userId.ToString()));
            List<ExDay> exdays = ExDay.GetExDays(Convert.ToInt32(userId.ToString()));

            Label lblcategory = e.Item.FindControl("lblcatcheck") as Label;
            if (Convert.ToString(lblcategory.Text) != "True")
            {
                LinkButton lnktitle = e.Item.FindControl("lnktitle") as LinkButton;
                lnktitle.CommandName = "openpopup";
                Image imgcategory = e.Item.FindControl("imgfolder") as Image;
                imgcategory.Visible = false;
                //---- for status
                Label lblwork = e.Item.FindControl("lblworkstatus") as Label;
                String strMenuHtml = "";
                strMenuHtml = "<ul id='menu'>";

                bool ClockedIn = false;
                if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "lastout"))))
                {
                    lblwork.Text = "";
                    lblwork.ForeColor = System.Drawing.Color.Red;
                    ClockedIn = true;
                }
                else
                {
                    lblwork.Text = "Present";
                    lblwork.ForeColor = System.Drawing.Color.Green;
                    strMenuHtml += "<li> <a href='#' id='lnkSignIn' class='lnkSignIn' data-id='" + DataBinder.Eval(e.Item.DataItem, "ID").ToString() + "'>Clock out</a></li>";
                    String localDate = objATZ.GetDate(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "lastin")), HttpContext.Current.User.Identity.Name);
                    try
                    {

                        String localTime = objATZ.GetTime(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "lastin")), HttpContext.Current.User.Identity.Name);
                        DateTime tempDateTime1;
                        if (localDate.Contains("Today"))
                        {
                            tempDateTime1 = DateTime.Now;
                        }
                        else
                        {
                            tempDateTime1 = Convert.ToDateTime(localDate);
                        }
                        //new DateTime(tempDateTime1.Year, tempDateTime1.Month, tempDateTime1.Day, Convert.ToInt32(lblSigninTime.Text.Split(' ')[0].Split(':')[0]), Convert.ToInt32(lblSigninTime.Text.Split(' ')[0].Split(':')[1]), 0);
                        DateTime tempDateTime;
                        tempDateTime = Convert.ToDateTime(tempDateTime1.ToShortDateString() + " " + Convert.ToDateTime(localTime).ToShortTimeString());
                        lblworkhours.Attributes.Add("data-date", tempDateTime.ToString());
                        ClockedIn = false;
                    }
                    catch (Exception)
                    { }
                }



                if (!String.IsNullOrEmpty(DataBinder.Eval(e.Item.DataItem, "lastin").ToString()))
                {
                    List<Absence> tempabs = absences.Where(u => u.CrtDate.Date == DateTime.Now.Date && u.Active == true).ToList();
                    List<ExDay> tempExDays = exdays.Where(u => u.CrtDate.Date == DateTime.Now.Date && u.Active == true && u.ExDayTypeID == ExDayTypes.PARTIAL_WORKING).ToList();
                    HtmlControl imgAction = e.Item.FindControl("imgAction") as HtmlControl;
                    if (tempabs.Count > 0)
                    {
                        lblwork.Text = "Absent";
                        trdata.Attributes.Add("class", "pinkRow");
                        trdata.Attributes.Add("data-tr-id", "trdata" + userId);

                        if (!String.IsNullOrEmpty(DataBinder.Eval(container, "lastin").ToString()) || DataBinder.Eval(container, "lastin") != "")
                        {
                            imgAction.Visible = true;
                            imgAction.Style.Add("visibility", "visible");
                            imgAction.Attributes.Add("data-ab-id", tempabs.Last().AbsLogID.ToString());
                            imgAction.Attributes.Add("data-ab-userid", DataBinder.Eval(container, "ID").ToString());
                            imgAction.Attributes.Add("data-ab-date", DataBinder.Eval(container, "lastin").ToString());
                            imgAction.Attributes.Add("data-ab-active", "false");
                            imgAction.Attributes.Add("data-ab-tr", ("trdata" + userId));
                            imgAction.Attributes.Add("data-clock", ClockedIn.ToString());
                            imgAction.Attributes.Add("data-inaddress", Request.UserHostAddress);
                            imgAction.Attributes.Add("data-useractive", userStatus.ToString());
                            AutoPresent userAP = autopresences.SingleOrDefault(u => u.UserID == Convert.ToInt32(DataBinder.Eval(container, "ID").ToString()));
                            if (userAP != null)
                            {
                                imgAction.Attributes.Add("data-ap-autopresentid", userAP.AutoPresentID.ToString());
                                imgAction.Attributes.Add("data-ap-ci", userAP.ClockInTime.ToShortTimeString());
                                imgAction.Attributes.Add("data-ap-co", userAP.ClockOutTime.ToShortTimeString());
                                imgAction.Attributes.Add("data-ap-active", userAP.Active.ToString());
                            }
                            else
                            {
                                imgAction.Attributes.Add("data-ap-autopresentid", "0");
                                imgAction.Attributes.Add("data-ap-ci", "Select");
                                imgAction.Attributes.Add("data-ap-co", "Select");
                                imgAction.Attributes.Add("data-ap-active", "False");
                            }


                            string MarkAsAttendance = "<a href='#' class='lnkMarkAbsence' style='color:white' data-id='" + tempabs.Last().AbsLogID + "' data-userid='" + DataBinder.Eval(container, "ID").ToString() + "' data-date='" + DataBinder.Eval(container, "lastin").ToString() + "' data-active='false' data-tr='" + ("trdata" + userId) + "'>Mark as present</a>";
                            strMenuHtml += "<li>" + MarkAsAttendance + "</li>";
                        }
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(DataBinder.Eval(container, "lastin").ToString()) || DataBinder.Eval(container, "lastin") != "")
                        {
                            trdata.ID = "trdata" + userId;
                            imgAction.Style.Add("visibility", "visible");
                            imgAction.Visible = true;
                            imgAction.Attributes.Add("data-ab-id", "0");
                            imgAction.Attributes.Add("data-ab-userid", DataBinder.Eval(container, "ID").ToString());
                            imgAction.Attributes.Add("data-ab-date", DataBinder.Eval(container, "lastin").ToString());
                            imgAction.Attributes.Add("data-ab-active", "true");
                            imgAction.Attributes.Add("data-ab-tr", ("trdata" + userId));
                            imgAction.Attributes.Add("data-clock", ClockedIn.ToString());
                            imgAction.Attributes.Add("data-inaddress", Request.UserHostAddress);
                            imgAction.Attributes.Add("data-useractive", userStatus.ToString());
                            AutoPresent userAP = autopresences.SingleOrDefault(u => u.UserID == Convert.ToInt32(DataBinder.Eval(container, "ID").ToString()));
                            if (userAP != null)
                            {
                                imgAction.Attributes.Add("data-ap-autopresentid", userAP.AutoPresentID.ToString());
                                imgAction.Attributes.Add("data-ap-ci", userAP.ClockInTime.ToShortTimeString());
                                imgAction.Attributes.Add("data-ap-co", userAP.ClockOutTime.ToShortTimeString());
                                imgAction.Attributes.Add("data-ap-active", userAP.Active.ToString());
                            }
                            else
                            {
                                imgAction.Attributes.Add("data-ap-autopresentid", "0");
                                imgAction.Attributes.Add("data-ap-ci", "Select");
                                imgAction.Attributes.Add("data-ap-co", "Select");
                                imgAction.Attributes.Add("data-ap-active", "False");
                            }
                        }
                    }

                    if (tempExDays.Count > 0)
                    {
                        trdata.Attributes.Add("class", "eggshellRow");
                        imgAction.Visible = true;
                        imgAction.Attributes.Add("data-exday-id", tempExDays.Last().ExDayID.ToString());
                        imgAction.Attributes.Add("data-exday-typeid", "1");
                        imgAction.Attributes.Add("data-exday-date", tempExDays.Last().CrtDate.ToString());
                        imgAction.Attributes.Add("data-exday-active", "false");
                    }
                    else
                    {
                        imgAction.Visible = true;
                        imgAction.Attributes.Add("data-exday-id", "0");
                        imgAction.Attributes.Add("data-exday-typeid", "1");
                        imgAction.Attributes.Add("data-exday-date", DateTime.Now.ToString());
                        imgAction.Attributes.Add("data-exday-active", "true");
                    }
                }
                strMenuHtml += "</ul>";

                if (string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "lastout"))) && string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "lastin"))))
                {
                    lblwork.Text = "";
                    lblwork.ForeColor = System.Drawing.Color.Red;
                }
                #region Sign in - out grid when present or working
                Label lblintime = e.Item.FindControl("lbltimein") as Label;
                lblintime.Text = objATZ.GetDate(DataBinder.Eval(container, "lastin").ToString(), HttpContext.Current.User.Identity.Name);

                string time = objATZ.GetTime(DataBinder.Eval(container, "lastin").ToString(), HttpContext.Current.User.Identity.Name);
                Label lbltimeforin = e.Item.FindControl("lbltimeforin") as Label;
                if (!string.IsNullOrEmpty(DataBinder.Eval(container, "lastin").ToString()) && !string.IsNullOrWhiteSpace(DataBinder.Eval(container, "lastin").ToString()))
                {
                    lbltimeforin.Text = ((!IsSimpleClock) ? Convert.ToDateTime(time).ToString("HH:mm") : time);
                }
                //Label lblouttime = e.Item.FindControl("lbltimeout") as Label;            
                time = objATZ.GetTime(DataBinder.Eval(container, "lastout").ToString(), HttpContext.Current.User.Identity.Name);
                Label lbltimeforout = e.Item.FindControl("lbltimeforout") as Label;
                if (!string.IsNullOrEmpty(DataBinder.Eval(container, "lastout").ToString()) && !string.IsNullOrWhiteSpace(DataBinder.Eval(container, "lastout").ToString()))
                    lbltimeforout.Text = ((!IsSimpleClock) ? Convert.ToDateTime(time).ToString("HH:mm") : time);

                String dtOut = DataBinder.Eval(container, "lastout").ToString();
                String dtIn = DataBinder.Eval(container, "lastin").ToString();
                if (!String.IsNullOrEmpty(dtIn) && !String.IsNullOrEmpty(dtOut))
                {
                    if (Convert.ToDateTime(dtIn).Date != Convert.ToDateTime(dtOut).Date)
                    {
                        lbltimeforout.Text = ((!IsSimpleClock) ? Convert.ToDateTime(time).ToString("HH:mm") : time) + " - " + Convert.ToDateTime(dtOut).ToString("ddd MMM dd");
                    }
                }
                #endregion
                if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "lastout"))))
                {

                    #region Fetch last record
                    lblintime.Visible = false;
                    lbltimeforin.Visible = false;
                    lbltimeforout.Visible = false;
                    #endregion
                    string lastIn = DataBinder.Eval(container, "lastin").ToString();
                    if (!String.IsNullOrEmpty(lastIn) || lastIn != "")
                    {
                        if (objATZ.GetDate(lastIn, HttpContext.Current.User.Identity.Name) != "Today")
                        {
                            if (!trdata.Attributes["class"].ToString().Contains("pinkRow"))
                            {
                                trdata.Attributes.Add("class", "buttermilkRow");
                            }
                        }
                        else
                        {
                            lastIn = DateTime.Now.ToShortDateString() + " " + objATZ.GetTime(lastIn, HttpContext.Current.User.Identity.Name);
                        }
                    }

                    GetHistoryRecord(userId, Convert.ToDateTime(lastIn).Date, Convert.ToDateTime(lastIn).Date);
                    RecordModel record = records[0];

                    string strDetails = "<table id=\"tblDetail\">";
                    strDetails += "<tr><td colspan='2' style='text-align:center'><b>Last Clocked Out Details</b></td></tr>";
                    strDetails += "<tr><td colspan='2' style='text-align:center'>" + lblintime.Text + "</td><tr>";
                    strDetails += "<tr><td class='caption'>Clock in: </td><td>" + lbltimeforin.Text + "</td><tr>";
                    strDetails += "<tr><td class='caption'>Clock Out: </td><td>" + lbltimeforout.Text + "</td><tr>";
                    if (record.Records.Count > 0)
                    {
                        strDetails += "<tr><td class='caption'>Clock in location: </td><td>" + record.Records[0].InLocation;
                        if (record.Records[0].LoginStatus == "Verified")
                        {
                            strDetails += "<image src=\"images/Controls/tick.png\" width=\"12px\" style='padding-left:3px;' /></td></tr>";
                        }
                        else
                        {
                            strDetails += "<image src=\"images/Controls/cross.png\" width=\"12px\" style='padding-left:3px;' /></td></tr>";
                        }

                        strDetails += "<tr><td class='caption'>Clock out location: </td><td>" + record.Records[0].OutLocation;

                        if (record.Records[0].LogoutStatus == "Verified")
                        {
                            strDetails += "<image src=\"images/Controls/tick.png\" width=\"12px\" style='padding-left:3px;' /></td></tr>";
                        }
                        else
                        {
                            strDetails += "<image src=\"images/Controls/cross.png\" width=\"12px\" style='padding-left:3px;' /></td></tr>";
                        }
                        strDetails += "<tr><td class='caption'>Total Worked: </td><td>" + record.TotalWorkedHours + "</td><tr>";

                    }

                    lblworkhours.Visible = false;
                    #region When not accounted for
                    HtmlControl imgInfo = e.Item.FindControl("imgInfo") as HtmlControl;
                    imgInfo.Style.Add("display", "inline-block");
                    imgInfo.Visible = true;
                    imgInfo.Attributes.Add("class", "tooltip");

                    strDetails += "</table>";
                    imgInfo.Attributes.Add("Title", strDetails);
                    imgInfo.Attributes.Add("Tooltip", strDetails);
                    #endregion
                }
                else
                {
                    DateTime lastIn = SP.GetWorkerLastSignInDateTime(userId);
                    GetHistoryRecord(userId, lastIn.Date, lastIn.Date);
                    RecordModel record = records[0];

                    string strDetails = "<table id=\"tblDetail\">";
                    strDetails += "<tr><td colspan='2' style='text-align:center'><b>Last Clocked Out Details</b></td></tr>";
                    strDetails += "<tr><td colspan='2' style='text-align:center'>" + lblintime.Text + "</td><tr>";
                    strDetails += "<tr><td class='caption'>Clock in: </td><td>" + lbltimeforin.Text + "</td><tr>";
                    strDetails += "<tr><td class='caption'>Clock Out: </td><td>" + lbltimeforout.Text + "</td><tr>";
                    if (record.Records.Count > 0)
                    {
                        strDetails += "<tr><td class='caption'>Clock in location: </td><td>" + record.Records[0].InLocation;
                        if (record.Records[0].LoginStatus == "Verified")
                        {
                            strDetails += "<image src=\"images/Controls/tick.png\" width=\"12px\" style='padding-left:3px;' /></td></tr>";
                        }
                        else
                        {
                            strDetails += "<image src=\"images/Controls/cross.png\" width=\"12px\" style='padding-left:3px;' /></td></tr>";
                        }

                        strDetails += "<tr><td class='caption'>Clock out location: </td><td>" + record.Records[0].OutLocation;

                        if (record.Records[0].LogoutStatus == "Verified")
                        {
                            strDetails += "<image src=\"images/Controls/tick.png\" width=\"12px\" style='padding-left:3px;' /></td></tr>";
                        }
                        else
                        {
                            strDetails += "<image src=\"images/Controls/cross.png\" width=\"12px\" style='padding-left:3px;' /></td></tr>";
                        }
                        strDetails += "<tr><td class='caption'>Total Worked: </td><td>" + record.TotalWorkedHours + "</td><tr>";

                    }

                    //lblworkhours.Visible = false;
                    #region When not accounted for
                    HtmlControl imgInfo = e.Item.FindControl("imgInfo") as HtmlControl;
                    imgInfo.Style.Add("display", "inline-block");
                    imgInfo.Visible = true;
                    imgInfo.Attributes.Add("class", "tooltip");

                    strDetails += "</table>";
                    imgInfo.Attributes.Add("Title", strDetails);
                    imgInfo.Attributes.Add("Tooltip", strDetails);
                    #endregion
                }

                ImageButton btnRename = e.Item.FindControl("btnRename") as ImageButton;
                btnRename.Visible = false;

                ImageButton btnSettings = e.Item.FindControl("btnSettings") as ImageButton;
                btnSettings.Visible = false;
            }
            else
            {
                ImageButton btnSettings = e.Item.FindControl("btnSettings") as ImageButton;
                if (IsAdmin)
                    btnSettings.Visible = false;
                lblcount.Text = "(" + DataBinder.Eval(container, "t_records").ToString() + ")";
            }
        }
    }

    private void GetHistoryRecord(string userID, DateTime start, DateTime end)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("getworkerrecordbydate", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@sDate", start.Date);
                cmd.Parameters.AddWithValue("@eDate", start.Date);
                cmd.Parameters.AddWithValue("@Id", userID);
                cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                records = new List<RecordModel>();
                records = UtilityMethods.getRecords(ds);
                //rptHistory.DataSource = records;
                //rptHistory.DataBind();            
            }
        }

    }

    protected void Rptbreadcrumitemdatabound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton lnk = e.Item.FindControl("lbtnBreadCrum") as LinkButton;
            lnk.Text = DataBinder.Eval(e.Item.DataItem, "value").ToString();
            lnk.CommandArgument = DataBinder.Eval(e.Item.DataItem, "key").ToString();
        }
    }

    protected void RptrbreadcrumitemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton lnk = e.Item.FindControl("lbtnBreadCrum") as LinkButton;
            hfparentid.Value = e.CommandArgument.ToString();
            Breadcrum(Convert.ToInt32(e.CommandArgument));
            Bredcrumbind();
            RptrAttendanceFill(Convert.ToInt32(e.CommandArgument), IsAdmin);
        }
    }

    protected void BtntreeviewClick(object sender, EventArgs e)
    {
        var ids = (from r in Rptrattendence.Items.Cast<RepeaterItem>()
                   let selected = (r.FindControl("chkselecteditem") as CheckBox).Checked
                   let id = (r.FindControl("hf_ID") as HiddenField).Value
                   where selected
                   select id).ToList();
        foreach (string id in ids)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("updateparentidtreeview", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@p_id", hftreenodevalue.Value);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@fid", fid.Value);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        RptrAttendanceFill(Convert.ToInt32(hfparentid.Value), IsAdmin);
        Breadcrum(Convert.ToInt32(hfparentid.Value));
        Bredcrumbind();
        TreeView1.Nodes.Clear();
        TreeNode parentNode = new TreeNode(String.Format("<input type='radio' name='rbtncat' value='Root' onclick='setvalue(0)'/>&nbsp; Root"));
        FillTreeView(parentNode.ChildNodes, "0");
        TreeView1.Nodes.Add(parentNode);
    }

    protected void BtnsearchClick(object sender, EventArgs e)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                divfuntions.Style.Add("display", "none");
                using (SqlCommand cmd = new SqlCommand("getsearchrecord", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", hfparentid.Value);
                    cmd.Parameters.AddWithValue("@search", txtsearch.Text);
                    cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        Rptrattendence.Visible = true;
                        Rptrattendence.DataSource = dt;
                        autopresences = AutoPresent.GetAll();
                        Rptrattendence.DataBind();
                    }
                    else
                    {
                        Rptrattendence.Visible = false;
                        Rptrattendence.DataSource = dt;
                        Rptrattendence.DataBind();
                        lblemptygrid.Text = "No Record Found";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            lblexception.Text = ex.Message;
        }
        ScriptManager.RegisterStartupScript(this, GetType(), "", "document.getElementById('searchdate').style.display=''", true);
    }

    public void pg_rptrcontract_OnPageIndex_Changed(object sender, EventArgs e)
    {
        RptrAttendanceFill(Convert.ToInt32(hfparentid.Value), IsAdmin);
    }

    protected void addAdmin_Click(object sender, EventArgs e)
    {
        if (userList.SelectedIndex != 0)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("addcategoryadmin", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CatId", fid.Value);
                    cmd.Parameters.AddWithValue("@UserId", userList.SelectedValue);
                    cmd.Parameters.AddWithValue("@InstanceId", _InstanceID);
                    cmd.ExecuteNonQuery();
                    RptrAttendanceFill(Convert.ToInt32(hfparentid.Value));
                    clsbtn.Style.Remove("display");
                    ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Admin added successfully.');", true);

                    SendRoleUpdateEmail(true);
                }
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Please select a user from a list first');", true);
        }
    }

    private void SendRoleUpdateEmail(Boolean admin)
    {
        AvaimaEmailAPI email = new AvaimaEmailAPI();
        StringBuilder subject = new StringBuilder();
        StringBuilder body = new StringBuilder();
        if (admin)
        {
            subject.Append(hdnAdminName.Value + " made you Admin in Time & Attendance");
            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
            body.Append("<b><p style='line-height:32px'>" + hdnAdminName.Value + "</b> assigned you as <b>Admin</b> in \"Time & Attendance\" software application on Avaima.");
            body.Append("</br> You can now see and manage the time and attendance of all the employees under your supervision. Your dashboard will give you an overview of the statuses of all the employees.");
            //body.Append("</br>For questions email <a href='mailto:support@avaima.com'>support@avaima.com</a></p>");
            //body.Append("</br></br><a href='http://avaima.com'>AVAIMA.COM</a>");
            //body.Append("</br><i>Organize your work!</i>");
            //body.Append("</br></br>For information contact: <a href='mailto:info@avaima.com'>info@avaima.com</a>");
            //body.Append("</br>For information contact: <a href='mailto:support@avaima.com '>support@avaima.com </i>");
            body.Append(Helper.AvaimaEmailSignature);
            body.Append("</div>");
        }
        else
        {
            subject.Append(hdnAdminName.Value + " removed your admin status");
            body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
            body.Append("<b><p style='line-height:32px'>" + hdnAdminName.Value + "</b> removed your status as an \"Admin\" in \"Time & Attendance\" software application on Avaima.com.");
            body.Append("</br> You can only clock-in / clock-out and view your own statistics only.");
            //body.Append("</br>For questions email <a href='mailto:support@avaima.com'>support@avaima.com</a></p>");
            //body.Append("</br></br><a href='http://avaima.com'>AVAIMA.COM</a>");
            //body.Append("</br><i>Organize your work!</i>");
            //body.Append("</br></br>For information contact: <a href='mailto:info@avaima.com'>info@avaima.com</a>");
            //body.Append("</br>For information contact: <a href='mailto:support@avaima.com '>support@avaima.com </i>");
            body.Append(Helper.AvaimaEmailSignature);
            body.Append("</div>");
        }
        string str = body.ToString();
        //hdnAdminEmail.Value.ToString()
        email.send_email(SP.GetEmailByUserID(Convert.ToInt32(appAssignedId.ToString())), "Attendance Management", "", body.ToString(), subject.ToString());
    }

    protected void lnkMyAttendance_Click(object sender, EventArgs e)
    {
        attendancetype objattendancetype = attendancetype.SingleOrDefault(u => u.userid == appAssignedId.ToString() && u.instanceid == this.InstanceID);
        if (objattendancetype != null)
        {
            if (objattendancetype.userattendType == 2)
            {
                this.Redirect("Flexibleworker.aspx?id=" + appAssignedId.ToString());
            }
            else
            {
                this.Redirect("Add_worker.aspx?id=" + appAssignedId.ToString());
            }
        }
        else
        {
            this.Redirect("Add_worker.aspx?id=" + appAssignedId.ToString());
        }
    }

    //protected void statusList_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (statusList.SelectedValue != "0")
    //        RptrAttendanceFill(Convert.ToInt32(hfparentid.Value), IsAdmin, true, Convert.ToInt32(statusList.SelectedValue));
    //    else
    //    {
    //        RptrAttendanceFill(Convert.ToInt32(hfparentid.Value), IsAdmin);
    //    }
    //}

    public string testInstanceID { get { return "0"; } }

    public List<AutoPresent> autopresences { get; set; }
    protected void Button1_Click(object sender, EventArgs e)
    {
        AvaimaEmailAPI email = new AvaimaEmailAPI();
        StringBuilder subject = new StringBuilder();
        StringBuilder body = new StringBuilder();
        subject.Append(hdnAdminName.Value + " wants you to use Time & Attendance");
        body.Append("<div style='line-height:22px;font-family:\"Helvetica Neue\",\"Segoe UI\",Helvetica,Arial,\"Lucida Grande\",sans-serif;font-size:14px'>");
        body.Append("<b>" + hdnAdminName.Value + "</b> uses <b>\"Time & Attendance\"</b> software application on Avaima.");
        body.Append("</br> He added you in this application and wants you to clock-in / clock-out on a daily basis.");
        body.Append("</br> Follow the steps below to start using the application:");
        body.Append("<ul>");
        body.Append("<li>In another email you should have received request to sign up on Avaima.com. Follow the instructions and sign up on Avaima. Sign up is <b>FREE</b></br>"
            + "If you did not receive this email, go to Avaima.com and sign up using xyz@hotmail.com as your email address</li>");
        body.Append("<li>Go to <a href='http://www.avaima.com/signin'>www.avaima.com/signin</a> and login to your account</li>");
        body.Append("<li>Find and click \"Time & Attendance\" in the left menu</li>");
        body.Append("<li>\"Clock-In\" when you start your day at work and \"Clock-Out\" when you are done</li>");
        //body.Append("<li></li>");
        //body.Append("<li></li>");        
        body.Append("</ul>");
        //body.Append("</br>For questions email support@avaima.com");
        //body.Append("</br><a href='http://avaima.com'>AVAIMA.COM</a>");
        //body.Append("</br><i>Organize your work!</i>");
        //body.Append("</br></br>For information contact: <a href='mailto:info@avaima.com'>info@avaima.com</a>");
        //body.Append("</br>For information contact: <a href='mailto:support@avaima.com '>support@avaima.com </i>");
        body.Append(Helper.AvaimaEmailSignature);
        body.Append("</div>");
        string strbody = body.ToString();

        email.send_email("sbmuhammdfaizan@hotmail.com", hdnAdminName.Value.ToString(), hdnAdminEmail.Value.ToString(), body.ToString(), subject.ToString());
    }

    public List<RecordModel> records { get; set; }
    //protected void lnkLateReport_Click(object sender, EventArgs e)
    //{
    //    string page = "Reports.aspx?id=" + appAssignedId.ToString() + "&instanceid=" + _InstanceID;
    //    this.Redirect(page);
    //}
}