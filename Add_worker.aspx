﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Add_worker.aspx.cs" Inherits="attendence.Add_worker" MasterPageFile="~/MasterPage.master" %>

<%@ Register Src="~/WebApplication1/userprofile.ascx" TagName="contractuserprofile" TagPrefix="uc1" %>

<asp:Content runat="server" ContentPlaceHolderID="head" ID="Content1">
     
    <script type="text/javascript">

        function openPausedDialog() {
            if ($('#hdnPauseTimeID').val() != "0") {
                var dlg = $('#divPauseDialog').dialog({
                    modal: true,
                    hide: { effect: "blind" },
                    width: 'auto',
                    height: 'auto'
                });

                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                var iframe = window.parent.document.getElementById("MainContent_ifapp");
                $('.ui-widget-overlay').css("opacity", ".8");
                $('.lnkPlay').click(function () {
                    $(this).fadeOut();
                });
            }
        }

        jQuery(document).ready(function ($) {
            $('#btnApplyToAll').click(function () {
                var CI = $('.txtMonCI').val();
                var CO = $('.txtMonCO').val();
                $('.datetimeCI').val(CI);
                $('.datetimeCO').val(CO);
                return false;
            });
            $('.btnCanceWH').click(function () {
                $('#divWorkingHours').dialog('close');
                return false;
            });

            $('.imgDeleteAbs').click(function () {
                var $obj = $(this);
                var userid = $(this).attr('data-userid');
                var id = $(this).attr('data-id');
                var date = $(this).attr('data-date');
                $('#divDialog').attr('title', 'Delete leave?');
                $('#divDialog').html('Are you sure?');
                $('#divDialog').dialog({
                    modal: true,
                    hide: {
                        effect: "blind"
                    },
                    buttons: {
                        "Yes": function () {
                            addNewAbsence(id, userid, date, "False", "Deleting");
                            $obj.closest('tr').hide(200);
                            $(this).dialog('close');
                        },
                        "No": function () {
                            $(this).dialog('close');
                        },
                        "Cance": function () {
                            $(this).dialog('close');
                        }
                    }
                });
            });

            $('.btnLeaveCancel').click(function () {
                $('.divAbs').dialog('close');
                return false;
            });
            $('.btnAddLeave').click(function () {
                $('.divAbs').dialog('close');
            });

            $('.lnkAddAbs').click(function () {
                var userid = $(this).attr('data-userid');
                var date = new Date();
                $('.txtAbsDateFrom').val('');
                $('.txtAbsDateTo').val('');
                $('.txtAbsComments').val('');
                var dlg = $('.divAbs').dialog({
                    width: 380,
                    modal: true,
                    hide: {
                        effect: "blind"
                    }
                    //buttons: {
                    //    "Save": function () {
                    //        addNewAbsence('0', $('#hdnUserID').val(), $('.txtAbsDate').val(), "True", $('.txtAbsComments').val());
                    //        $(this).dialog('close');
                    //    },
                    //    "Cancel": function () {
                    //        $(this).dialog('close');
                    //    }
                    //}
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;
            });

            $('.lnkMarkNPW').click(function () {
                var dataid = $(this).attr('data-id');
                var datatype = $(this).attr('data-ex-type');
                var datauserid = $(this).attr('data-userid');
                var datadate = $(this).attr('data-date');
                var dataactive = $(this).attr('data-active');
                var $obj = $(this);

                $('#Dialog').attr('title', $(this).attr('tooltip'));
                $('#Dialog').html('Are you sure you want to ' + $(this).attr('tooltip') + "?");
                $('#Dialog').dialog({
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            AddUpdateExDays(dataid, datatype, datauserid, datadate, dataactive, 'Partial Working', true);
                            $(this).dialog('close');
                            if (dataactive == "False") {
                                $obj.closest('tr').removeClass('eggshellRow');
                            }
                            else {
                                $obj.closest('tr').addClass('eggshellRow');
                            }

                        },
                        "No": function () {
                            $(this).dialog('close');
                        },
                        "Cancel": function () {
                            $(this).dialog('close');
                        }
                    }
                });
                return false;
            });


            //$("#chkAP").switchButton({
            //    on_label: 'On',
            //    off_label: 'Off'
            //});

            jQuery(document).on('change', '#chkAP', function ($) {
                //alert('change');
                if ($('#chkAP').is(":checked")) {
                    $('.trAPC').show(200);
                    //$
                }
                else {
                    $('.trAPC').hide(200);
                }


                $('.tooltip').tooltipster({
                    interactive: true
                });


            });

            //$('.datetime').timepicker({
            //    timeFormat: 'hh:mm tt',
            //    controlType: 'select'
            //});


            $('.lnkWorkingHours').click(function () {
                //var AutoPresentID = $(this).attr('data-ap-id');
                //var UserID = $(this).attr('data-ap-userid');
                //var ClockInTime = $(this).attr('data-ap-clockin');
                //var ClockOutTime = $(this).attr('data-ap-clockout');
                //$('.txtWClockIn').val(ClockInTime);
                //$('.txtWClockOut').val(ClockOutTime);
                //var Active = $(this).attr('data-ap-active');
                var dlg = $('#divWorkingHours').dialog({
                    modal: true,
                    width: 400,
                    hide: {
                        effect: "blind",
                    }

                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;
            });

            $('.lblShowExDays').click(function () {
                //var AutoPresentID = $(this).attr('data-ap-id');
                //var UserID = $(this).attr('data-ap-userid');
                //var ClockInTime = $(this).attr('data-ap-clockin');
                //var ClockOutTime = $(this).attr('data-ap-clockout');
                //$('.txtWClockIn').val(ClockInTime);
                //$('.txtWClockOut').val(ClockOutTime);
                //var Active = $(this).attr('data-ap-active');
                var dlg = $('.ExpectionalDays').dialog({
                    modal: true,
                    width: 'auto',
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;
            });

            $('.lbltimesetting').click(function () {
                //var AutoPresentID = $(this).attr('data-ap-id');
                //var UserID = $(this).attr('data-ap-userid');
                //var ClockInTime = $(this).attr('data-ap-clockin');
                //var ClockOutTime = $(this).attr('data-ap-clockout');
                //$('.txtWClockIn').val(ClockInTime);
                //$('.txtWClockOut').val(ClockOutTime);
                //var Active = $(this).attr('data-ap-active');
                var dlg = $('.TimeSetting').dialog({
                    modal: true,
                    width: 'auto',
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;
            });


            $('.timesettings').click(function () {
                $('.TimeSetting').dialog('close');
            });

            $('.timesettingcancel').click(function () {
                $('.TimeSetting').dialog('close');
                return false;
            });


            $('.btnSaveWH').click(function () {
                $('#divWorkingHours').dialog('close');
            });

            $('.btnAddExceptionalDays').click(function () {
                $('.ExpectionalDays').dialog('close');
            });

            $('.btnCancelExDays').click(function () {
                $('.ExpectionalDays').dialog('close');
                return false;
            });

            $('.lnkExNew').click(function () {
                $('.txtExTitle').val('');
                $('.txtExFrom').val('');
                $('.txtExTo').val('');
                $('.txtExClockIn').val('');
                $('.txtExClockOut').val('');
                $('.chkEliminateBreaks').find('input').prop('checked', false);
                $('#hdnGroupID').val('0');
                $('.btnAddExceptionalDays').val('Add');
                return false;
            });

            $('.lnkEdit').click(function () {
                try {
                    $('.txtExTitle').val($(this).parent().parent().find('.lblGroupName').text());
                    $('.txtExFrom').val($(this).parent().parent().find('.lblStartDate').text());
                    $('.txtExTo').val($(this).parent().parent().find('.lblEndDate').text());
                    $('.txtExClockIn').val($(this).parent().parent().find('.lblClockIn').text());
                    $('.txtExClockOut').val($(this).parent().parent().find('.lblClockOut').text());
                    if ($(this).parent().parent().find('.lblClockOut').val() == 'No') {
                        $('.chkEliminateBreaks').find('input').prop('checked', false);
                    }
                    else {
                        $('.chkEliminateBreaks').find('input').prop('checked', true);
                    }
                    $('#hdnGroupID').val($(this).attr('data-groupid'));
                    $('.btnAddExceptionalDays').val('Save');
                    return false;
                } catch (e) {
                    alert(e.message);
                }
            });


            $('.lblAutoPresent').click(function () {
                var AutoPresentID = $(this).attr('data-ap-id');
                var UserID = $(this).attr('data-ap-userid');
                var ClockInTime = $(this).attr('data-ap-clockin');
                var ClockOutTime = $(this).attr('data-ap-clockout');
                $('.txtOPStartTime').val(ClockInTime);
                $('.txtOPEndTime').val(ClockOutTime);
                var Active = $(this).attr('data-ap-active');
                $('#divAutoPresent').dialog({
                    modal: true,
                    buttons: {
                        "Save": function () {
                            ClockInTime = $('.txtOPStartTime').val();
                            ClockOutTime = $('.txtOPEndTime').val();
                            try {
                                if ($('.trAPC').css('display') == 'none') {
                                    Active = 'False';
                                }
                                else {
                                    Active = 'True';
                                }
                                AddUpdateAutoPresent(AutoPresentID, UserID, ClockInTime, ClockOutTime, Active);
                                $('.lblAutoPresent').attr('data-ap-clockin', $('.txtOPStartTime').val());
                                $('.lblAutoPresent').attr('data-ap-clockout', $('.txtOPEndTime').val());
                                $('.lblAutoPresent').attr('data-ap-active', Active);
                                if (Active == "True") {
                                    $('.lblAutoPresent').text('On');
                                }
                                else {
                                    $('.lblAutoPresent').text('Off');
                                }

                                $(this).dialog("close");
                            } catch (e) {

                            }
                        },
                        "Cancel": function () {
                            //alert('close');
                            $(this).dialog("close");
                        }
                    },
                    show: {

                    }
                });
            })

            $('.lblactive').click(function () {
                var userid = $(this).attr('data-userid');
                var status = $(this).attr('data-active');
                $('#divActive').attr('title', "Set status");
                if (status == "True") {
                    $('#divActive').html("<p>Do you want to deactivate employee?</p>");
                    $('#divActive').dialog({
                        modal: true,
                        buttons: {
                            "Yes": function () {
                                SetUserStatus(userid, status, false);
                                $('.lblactive').text("Inactive");
                                $('.lblactive').attr("data-active", "False");
                                $(this).dialog('close');
                            },
                            "No": function () {
                                $(this).dialog('close');
                            },
                            "Cancel": function () {
                                $(this).dialog('close');
                            }
                        }
                    });
                }
                else {
                    $('#divActive').html("<p>Do you want to activate employee?</p>");
                    $('#divActive').dialog({
                        modal: true,
                        buttons: {
                            "Yes": function () {
                                SetUserStatus(userid, status, false);
                                $('.lblactive').text("Active");
                                $('.lblactive').attr("data-active", "True");
                                $(this).dialog('close');
                            },
                            "No": function () {
                                $(this).dialog('close');
                            },
                            "Cancel": function () {
                                $(this).dialog('close');
                            }
                        }
                    });
                }
            });

            $('.chkAutoPresent').click(function () {
                if ($(this).is(":checked")) {
                    var returnVal = confirm("Are you sure?");
                    $(this).attr("checked", returnVal);
                    alert(returnVal);
                }
            });
            $('#btnUpdateTime').click(function () {
                if ($('.txtEFrmTime').val() == 'Select' || $('.txtEToTime').val() == 'Select') {
                    alert('Please select valid date time range');
                    return false;
                }
                $('#divEditTime').dialog().hide(200);
            });

            //$('#imgEditRole1').click(function () {
            //    var dlg = $('#divEditRole').dialog({
            //        modal: true,
            //        hide: {
            //            effect: "blind",
            //        }
            //    });
            //    dlg.parent().appendTo($("form:first"));
            //    dlg.parent().css("z-index", "1000");
            //});
            //jQuery(document).on('click', '.btnRole', function ($) {
            //    $('#divEditRole').dialog().hide(200);
            //});
            $('#btnDeleteTime').click(function () {
                var frmTime = $('.txtfrmTime').val();
                var toTime = $('.txtToTime').val();
                if (frmTime == 'Select' || toTime == 'Select') {
                    alert('Please select a time range');
                    return false;
                }
                //$('#revHr').dialog('close');
                getTimeDifference(frmTime, toTime, '#lblHoursToDel');
                var dlg = $('#divConfirmDeleteTime').dialog({
                    model: true
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "2000");
                var iframe = window.parent.document.getElementById("MainContent_ifapp");
                //if (iframe != null)
                //    iframe.height = "600px";
                return false;
            });
            $('#btnNo, #btnCancel').click(function () {
                //$('#divConfirmDeleteTime').hide(200);
                $('#divConfirmDeleteTime').dialog("close");
                return false;
            });
            // Refresh Worked hour
            $('.lblWorkedhours').attr('data-incdate', $($('span[data-incdate]')[0]).attr('data-incdate'))
            setInterval(function () {
                $('span[data-incdate],label[data-incdate]').each(function (index) {
                    //alert($(this).text());
                    var $target = '#' + $(this).attr('id');
                    var signintime = $($target).attr('data-incdate');

                    //getworkedhours
                    getDateTimeDifference(signintime, $target, new Date(new Date().toLocaleDateString() + ' ' + $('#hdnCITime').val()));
                });
            }, 1000);

            //string AbsLogID, string UserID, string Date, string Active
            $('.lnkMarkAbsence').click(function () {
                var label;
                $('.lnkMarkAbsence').each(function () {
                    label = $(this).text();
                });
                showDialog("Are you sure?", "<p>Are you sure you want to " + label + "?</p>", $(this));
                return false;
            });

            function showDialog(title, message, obj) {
                $('#divClockedIn').attr("title", title);
                $('#divClockedIn').html(message);
                $('#divClockedIn').dialog({
                    model: true,
                    buttons: {
                        "Yes": function () {
                            showAbsCommentBox(obj);
                        },
                        "No": function () {
                            $(this).dialog("close");
                        },
                        "Cancel": function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }

            function showAbsCommentBox(obj) {
                $('#absComment').dialog({
                    width: 375,
                    modal: true,
                    buttons: {
                        "Save": function () {
                            addAbsence(obj, $('#txtAbsComment').val(), false);
                            $(this).dialog('close');
                            $('#divClockedIn').dialog("close");
                        },
                        "Cancel": function () {
                            $(this).dialog('close');
                        }
                    }
                });
            }

            $('.imgDeleteHours').click(function () {
                //alert('delete hours');
                var $obj = $(this);
                var frmTime = $obj.attr('data-frmTime');
                var toTime = $obj.attr('data-toTime');
                var recordID = $obj.attr('data-recordID');
                var breakTime = "Break Time: " + frmTime + " - " + toTime;
                var dataday = $obj.attr('data-day');
                var datadaytime = $obj.attr('data-daytime');
                $('#lbldataday').text(dataday);
                $('#lblsWorked').text(datadaytime);


                $('.txtfrmTime').val('Select');
                $('.txtToTime').val('Select');
                $('#rID').val(recordID);

                var dlg = $("#revHr").dialog({
                    height: 270,
                    width: 380,
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                var iframe = window.parent.document.getElementById("MainContent_ifapp");
                //if (iframe != null)
                //    iframe.height = "600px";
                return false;
            });

            $('.imgEditHours').click(function () {
                var $obj = $(this);

                var frmTime = $obj.attr('data-frmTime');
                var toTime = $obj.attr('data-toTime');
                var frmDate = $obj.attr('data-frmdate');
                var toDate = $obj.attr('data-todate');
                var recordID = $obj.attr('data-recordID');
                var breakTime = "Clock in time: " + frmTime + " - " + toTime;

                if ($(this).hasClass('imgEditInHour')) {
                    $('.trout').hide();
                }
                else {
                    $('.trout').show();
                }
                $('#txteComments').val('');
                $('#hdnOFrmTime').val(frmTime);
                $('#hdnOToTime').val(toTime);
                $('.txtEFrmTime').val(frmTime);
                $('.txtEToTime').val(toTime);
                $('#hdnerid').val(recordID);

                var dlg = $("#divEditTime").dialog({
                    width: 393,
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });

        });

        function showVerifiedSignedOut() {
            $('.pverifyout').show();
            $('#divClockedOut').dialog({
                modal: true,
                buttons: {
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }

        function showSignedOut() {
            $('.pverifyout').hide();
            $('#divClockedOut').dialog({
                modal: true,
                buttons: {
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
        function showVerificationSignedIn() {
            $('.pverifyin').show();
            $('#divClockedIn').dialog({
                modal: true,
                buttons: {
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
        function showSignedIn() {
            $('.pverifyin').hide();
            $('#divClockedIn').dialog({
                modal: true,
                buttons: {
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }


        jQuery(document).ready(function ($) {
            //$('.btnSignNow').click(function () {
            //    $('#divSigninProcess').slideUp(800);
            //});

            // Sets hdnSomeEvent val to noevent to provide page leave confirmation
            $('#hdnSomeEvent').val("noevent");
            // Sets hdnSomeEvent val to event in order to avoid page leave confirmation
            $('.raisesevent').click(function () {
                $('#hdnSomeEvent').val("event")
            });
            // Checks if user leaves the page without clocking out
            <%--window.onbeforeunload = function (e) {
                e = e || window.event;
                var someevent = $('#hdnSomeEvent').val();
                var btnsignin = $("#<%=btnsignin.ClientID%>").hasClass('hide');
                // if admin or superuser
                //&& !(adminval == "admin")
                if (btnsignin && (someevent != "event")) {
                    // For IE and Firefox prior to version 4
                    if (e) {
                        e.returnValue = 'You are still clocked in!';
                    }

                    // For Safari
                    return 'You are still clocked in!';
                }
            };--%>

            $('.btnSignoutProcess').click(function () {
                $("#<%=btnsignin.ClientID%>").removeClass('btnSignInDisable');
            });

            $('#rpth_tr_0').css("display", "none");

            function startClock() {
                setInterval(function () {
                    var currentDate = new Date();
                    var tAMPM = formatAMPM(currentDate);
                    $(".lblCurrentTime").text(tAMPM);
                }, 1000)
            }


            setInterval(function () {
                //getWorkedHours
                //getDateTimeDifference($('.lblSigninDateTime').text(), '.lblWorkedhours');
            }, 2000);



            $('.btnsignout').click(function () {
                //if (confirm('Are you sure you want to clock out?')) {
                //    return true;
                //}
                return true;
            });

            $("#lnkcomment").click(function () {
                $('#lblerror').text("");

                var dlg = $("#addcomment").dialog({
                    height: 160,
                    width: 450,
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                var textbox = document.getElementById('txtcomment');
                textbox.focus();
                textbox.value = textbox.value;
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                var iframe = window.parent.document.getElementById("MainContent_ifapp");
                //if (iframe != null)
                //    iframe.height = "600px";
                return false;
            });

            $("#<%=btnsignin.ClientID%>").click(function () {
                //alert(updateMsg);
                $(this).attr("onclick", "");
            });

            $(".undo").click(function (e) {
                if (confirm("Are you sure you want to undo?")) {
                    $(this).attr("onclick", "");
                } else {
                    e.preventDefault();
                }
            });

            var signoutclickable = false;
            $("#<%=btnsignout.ClientID%>").click(function () {


                if (!signoutclickable) {
                    var btnObj = $(this);
                    var dlg = $('#divDialogUOT').dialog({
                        modal: true,
                        hide: {
                            effect: "blind"
                        },
                        buttons: {
                            "Clockout": function () {
                                signoutclickable = true;
                                $(btnObj).click();
                                $(this).hide('blind');
                            },

                            "Cancel": function () {
                                $('#hdnIsUnderTime').val('false');
                                $(this).dialog('close');
                            }
                        }
                    });
                    dlg.parent().appendTo($("form:first"));
                    dlg.parent().css("z-index", "1000");
                    var iframe = window.parent.document.getElementById("MainContent_ifapp");

                    return false;
                }


                //else if (clockoutTime > ) {
                //    console.log('hello')
                //}              
            });

         
         
            $('.btnAddPause').click(function () {
                $('.divPauseAsk').hide('blind');
            })

            $('.lnkAddOvertime, .lnkAddUndertime').click(function () {
                $('#lblerrormsg').text("");
                var dialogTitle = "";
                var UOTitle = "";
                if ($(this).hasClass('lnkAddOvertime')) {
                    $('#hdnUOTime').val('true');
                    //dialogTitle = "Overtime" + " " + (new Date()).toLocaleDateString();
                    dialogTitle = "Overtime" + " ";
                    UOTitle = "Add overtime";
                }
                else {
                    $('#hdnUOTime').val('false');
                    //dialogTitle = "Undertime" + " " + (new Date()).toLocaleDateString();
                    dialogTitle = "Undertime" + " ";
                    UOTitle = "Add undetime";
                }
                var dlg = $('#divAddOvertime').dialog({
                    title: dialogTitle, modal: true, hide: { effect: "blind" }
                });
                $('.btnCancelOvertime').click(function () {
                    $('#divAddOvertime').dialog('close');
                    return false;
                });
                //$('.btnAddOvertime').click(function () {
                //    if ($('#lblerrormsg').text() != "Error: Range Invalid")
                //       
                //});

                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                var iframe = window.parent.document.getElementById("MainContent_ifapp");

                return false;
            });

            $(window).load(function () {
                if ($("#hdnAdmin").val() != "admin") {
                    $('.lnkeditO').remove();
                    $('.lnkdeleteO').remove();
                }
            });


            $('.lnkShowOvertime').click(function () {
                $('.trOvertime').toggle();
                return false;
            });
            $('.lnkShowUndertime').click(function () {
                $('.trUndertime').toggle();
                return false;
            });

            $('.lnkeditO').click(function () {
                $(this).parent().parent().find('span').hide();
                $(this).parent().parent().find('input[type="text"], textarea').show();
                $(this).parent().find('.lnkupdateO').show();
                $(this).parent().find('.lnkcancelO').show();
                $(this).hide();
                $(this).parent().parent().find('input[type="text"]').first().focus();
                return false;
            });

            $('.lnkcancelO').click(function () {
                $(this).parent().parent().find('span').show();
                $(this).parent().parent().find('input[type="text"], textarea').hide();
                $(this).parent().find('.lnkupdateO').hide();
                $(this).hide();
                $(this).parent().find('.lnkeditO').show();
                return false;
            });

            var deleteoflag = false;

            //$('.tooltip').tooltip({
            //    offset: [45, 170],
            //    delay: 4000,
            //    effect: 'slide'
            //});

            $("#link").click(function () {
                openpopup();
                return false;
            });

            $(".DatePick").datepicker({
                dateFormat: 'mm/dd/yy',
                changeMonth: true,
                changeYear: true
            });
            $('.txtdatetime').datepicker();

            //$('.txttime').timepicker({
            //    timeFormat: 'hh:mm tt',
            //    controlType: 'select'
            //});

            $('.bedit').click(function () {
                var frmTime = $(this).attr('data-bfrom');
                var toTime = $(this).attr('data-bto');
                setTimePicker(frmTime, toTime, $(this));
            });

            $('.imgDeleteHours').click(function () {
                var frmTime = $(this).attr('data-frmTime');
                var toTime = $(this).attr('data-toTime');
                setTimePicker(frmTime, toTime);
            });

            function setTimePicker(from, to) {
                var f = $("#hftimeFormat").val();
                var fromTime = parseInt(getTimeFromAMPM(from).split(':')[0]);
                var toTime = parseInt(getTimeFromAMPM(to).split(':')[0]);
               <%-- $(".TimePick").timepicker({
                    //'timeFormat': f, 'step': 1, 'forceRoundTime': true,
                    //'minTime': '<%=f%>',
                    //'maxTime': '<%=t%>',
                    hourMin: fromTime,
                    hourMax: toTime,
                    //controlType: 'select',
                    //'minTime': from,
                    //'maxTime': to,
                    timeFormat: 'hh:mm tt',
                    controlType: 'select'
                    //'showDuration': true
                });--%>
            }

           <%-- $(".TimePick").timepicker({
                //'timeFormat': f, 'step': 1, 'forceRoundTime': true,
                //'minTime': '<%=f%>',
                //'maxTime': '<%=t%>',
                //hourMin: fromTime,
                //hourMax: toTime,
                //controlType: 'select',
                //'minTime': from,
                //'maxTime': to,
                timeFormat: 'hh:mm tt',
                controlType: 'select'
                //'showDuration': true
            });--%>

            function setTimePicker(from, to, $obj) {
                //var f = $("#hftimeFormat").val();
                var fromTime = parseInt(getTimeFromAMPM(from).split(':')[0]);
                var toTime = parseInt(getTimeFromAMPM(to).split(':')[0]);
             <%--   $obj.timepicker({
                    //'timeFormat': f, 'step': 1, 'forceRoundTime': true,
                    //'minTime': '<%=f%>',
                    //'maxTime': '<%=t%>',
                    hourMin: fromTime,
                    hourMax: toTime,
                    //controlType: 'select',
                    //'minTime': from,
                    //'maxTime': to,
                    timeFormat: 'hh:mm tt',
                    controlType: 'select'
                    //'showDuration': true
                });--%>
            }

            //$(".DateTimePick").datetimepicker({
            //    timeFormat: "hh:mm tt",
            //    controlType: 'select'
            //});

            $(function () {
                setInterval(function () { var d1 = $('#hdnCITime').val().split(' '); var d = $('#hdnCITime').val().split(' ')[0].split(':'); if (d[1] == "59") { d[1] = "00"; if (d[0] == "12") d[0] = "01"; else d[0] = parseInt(d[0]) + 1; } else d[1] = parseInt(d[1]) + 1; if (parseInt(d[1]) < 10) d[1] = "0" + d[1]; $('#hdnCITime').val(d[0] + ":" + d[1] + " " + d1[1]) }, 60000);

                //$('#hdnCITime').val(formatAMPM(new Date(new Date().toLocaleDateString() + ' ' + $('#hdnCITime').val())));
            });

            if ($("#<%=btnsignin.ClientID%>").attr("disabled") == "disabled") {

                //alert(updateMsg);
                $("#<%=btnsignin.ClientID%>").removeClass("btnSignInEnable");
                $("#<%=btnsignin.ClientID%>").addClass("btnSignInDisable");


                $("#<%=btnsignout.ClientID%>").addClass("btnSignOutEnable");
                $("#<%=btnsignout.ClientID%>").removeClass("btnSignOutDisable");
                $("#<%=btnsignin.ClientID%>").addClass("hide");
                $("#<%=btnsignout.ClientID%>").removeClass("hide");
                $('.trProfile').each(function () {
                    $(this).addClass('uncheck');
                    $(this).removeClass('hide');
                });


            } else {
                $("#<%=btnsignin.ClientID%>").addClass("btnSignInEnable");
                $("#<%=btnsignin.ClientID%>").removeClass("btnSignInDisable");

                $("#<%=btnsignout.ClientID%>").removeClass("btnSignOutDisable");
                $("#<%=btnsignout.ClientID%>").addClass("btnSignOutEnable");
                $("#<%=btnsignout.ClientID%>").addClass("hide");
                $("#<%=btnsignin.ClientID%>").removeClass("hide");
                $('.trProfile').each(function () {
                    $(this).addClass('check');
                    $(this).addClass('hide');
                });
            }

            setTimeout("try{document.getElementById('UndoMsg').style.display = 'none';}catch(e){}", 60000);
        });

        jQuery(document).on('click', 'img.bedit', function ($) {
            try {
                var $edit = $(this);
                var $parenttr = $edit.parent();
                var dataflag = $edit.attr("data-flag");
                var databid = $edit.attr("data-bid");
                var bfrom = $edit.attr("data-bfrom");
                var bto = $edit.attr("data-bto");
                var bdate = $edit.attr("data-bdate");
                $('.bfrmTime').val(bfrom);
                $('.btoTime').val(bto);
                $('#beDate').val(bdate);
                $('#bID').val(databid);
                $('#bModified').val(dataflag);
                $('.lblBreakTime').text('Break Time: ' + bfrom + " - " + bto);
                var dlg = $("#edtHr").dialog({
                    height: 226,
                    width: 291,
                    top: 60,
                    show: {
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

            } catch (e) {
                alert(e.message);
            }
            return false;
        });

        function editTime() {
            var bID = "";
            var bModified = "";
            var bfrmTime = $('.bfrom').val();
            var btoTime = $('.bto').val();
            var bDate = $('#bDate').val();


            var dlg = $("#edtHr").dialog({
                height: 226,
                width: 291,
                show: {
                },
                modal: true,
                hide: {
                    effect: "blind",
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            //if (iframe != null)
            //    iframe.height = "600px";
            return false;
        }

        function openpopup() {
            $('#lblerror').text("");
            var dlg = $("#divworkeradd").dialog({
                height: 450,
                width: 800,
                show: {
                },
                modal: true,
                hide: {
                    effect: "blind",
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            //if (iframe != null)
            //    iframe.height = "600px";
            return false;
        }

        function editdiv() {
            var dlg = $("#editworker").dialog({
                height: 255,
                width: 450,
                modal: true,
                hide: {
                    effect: "blind",
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            //if (iframe != null)
            //    iframe.height = "600px";
            return false;

        }

        function delTime() {
            var dlg = $("#revHr").dialog({
                height: 140,
                width: 200,
                modal: true,
                hide: {
                    effect: "blind",
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            //if (iframe != null)
            //    iframe.height = "600px";
            return false;
        }

        function Checkovertimehours() {
            var fromDate = new Date((new Date()).toDateString() + ' ' + $('.divAddOvertime .txtOverTimeFrom').val());
            var toDate = new Date((new Date()).toDateString() + ' ' + $('.divAddOvertime .txtOverTimeTo').val());
            if (toDate < fromDate) {
                $('#divAddOvertime').show('blind');
                $('#lblerrormsg').text("Error: Range Invalid");
                return false;

            }
            else {
                if ((((toDate.getTime() - fromDate.getTime()) / 60) / 1000) < $('#hfotlimit').val() && $('#hdnUOTime').val() != "false") {
                    $('#lblerrormsg').text("Error: You cannot enter overtime less than " + $('#hfotlimit').val() + " minutes. Contact the relevant person for details.");
                    return false;
                }
                else {
                    $('#divAddOvertime').hide('blind');
                    $('#lblerrormsg').text("");
                    return true;
                }
            }
        }


        function InstructNewUser() {
            $('#divNewUser').dialog({
                modal: true,
                hide: {
                    effect: "blind",
                },
                buttons: {
                    "Ok": function () {
                        $(this).dialog('close');
                    }
                }
            });
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            iframe.height = "500px";
            iframe.onload = null;
            //iframe.style.height = "600px !important";
        }
    </script>
    <style type="text/css">
        .ui-widget-content a {
            color: blue;
            cursor: pointer;
        }

        .auto-style2 {
            width: 62px;
        }

        #tblDetail {
            font-size: 14px;
            text-align: left;
        }

            #tblDetail td {
                padding: 5px 1px 0 4px;
            }

                #tblDetail td:first-child {
                    text-align: right;
                    font-weight: bold;
                }

        .bedit {
            display: none;
        }

            .bedit:last-of-type {
                display: inline;
            }

        h1.username {
            font: 32px/20px arial, helvetica, sans-serif;
            color: #333;
            margin: 6px 0 0 0;
        }


        .comment {
            line-height: 12px;
            margin-bottom: 0px;
        }

        .btnsign {
            float: left;
            background-repeat: no-repeat;
            width: 132px;
            height: 28px;
            padding: 0px 0 0 14px;
            color: #fff;
            font: 16px/ 16px arial, helvetica, sans-serif;
            text-align: left;
        }

        td.btnNeighbor {
            padding: 2px 0 0 0;
            vertical-align: middle;
        }

        table.tblBreaks td {
            padding: 0px;
            margin: 0px;
        }

            table.tblBreaks td:last-child {
                padding: 0 0 0 10px;
            }

        big {
            color: green;
            font-weight: bold;
        }

        #secprofile {
            padding: 1px 0 0 8px;
            line-height: 16px;
        }

        #tblDlg td, #tblDlg th {
            padding: 2px;
        }

        #tblDlg th {
            padding: 2px;
            text-align: right;
            font-weight: bold;
        }

        input[type='radio'] label {
            padding: 5px;
        }
    </style>

    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <link href="_assets/InfobarStyle.css" rel="stylesheet" />
    <%--</head>
<body>
    <form id="form1" runat="server">--%>
    <asp:HiddenField ID="hdnAdminEmail" runat="server" />
    <asp:HiddenField ID="hdnAdminName" runat="server" />
    <asp:HiddenField ID="hdnUserID" runat="server" />
    <div id="divMain" runat="server">
        <section id="secprofile">
            <div class="isa_info" id="divInfo" runat="server" visible="false">
                <%--<i class="fa fa-info-circle"></i>--%>
                    Replace this text with your own text.
            </div>
            <div class="isa_success" id="divSuccess" runat="server" visible="false">
                <%--<i class="fa fa-check"></i>--%>
                    Replace this text with your own text.
            </div>
            <div class="isa_warning" id="divWarning" runat="server" visible="false">
                <%--<i class="fa fa-warning"></i>--%>
                    Replace this text with your own text.
            </div>
            <div class="isa_error" id="divError" runat="server" visible="false">
                <%--<i class="fa fa-times-circle"></i>--%>
                <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
            </div>
            <br />
            <div>
                <asp:HiddenField runat="server" ID="hfdate" />
                <div class="space" runat="server" id="showProfile">
                    <asp:ScriptManager ID="scmang" runat="server"></asp:ScriptManager>
                    <asp:HiddenField runat="server" ID="HiddenField1" />
                    <div id="divworkeradd" style="display: none; width: 600px">
                        <uc1:contractuserprofile ID="workeruserprofile" runat="server" />
                    </div>
                    <a runat="server" id="anctab" target="_blank" style="display: none">a</a>
                </div>
                <%--background-color: #1a87cc;--%>
                <h1 class="username">
                    <asp:Label ID="lblUserName" runat="server" Text="My Profile"></asp:Label>
                    <%--<asp:Label CssClass="lblUserName"  Text=""></asp:Label>--%>
                    <%--<span style="font-size: 14px; vertical-align: middle;">[<asp:LinkButton runat="server" Text="My Profile" ID="link" Style="vertical-align: middle;"></asp:LinkButton>]
                            &nbsp;
                         
                            <span id="spWH" runat="server" class="trProfile"></span>

                        </span>--%>

                </h1>

                <table class="profile" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="caption">
                            <asp:Button ID="btnsignin" CssClass="btnsignin btnsign raisesevent" runat="server" Text="Clock In" OnClick="BtnsigninClick" />
                            <asp:Button ID="btnsignout" CssClass="btnsignout btnsign" Text="Clock Out" runat="server" OnClick="BtnSignoutClick" />
                            <%--<asp:Button ID="lnkAddOvertime" CssClass="lnkAddOvertime lnkAddOvertimeButton btnsign" runat="server" OnClick="lnkAddOvertime_Click" Text="+ Overtime" Visible="false"></asp:Button>--%>
                        </td>
                        <td class="btnNeighbor" id="divWorkedTime" style="padding: 0px" runat="server">
                            <asp:Label ID="lblSigninStatus" runat="server" Text="Clocked in at "></asp:Label>
                            <big>
                            <asp:Label ID="lblSigninTime" CssClass="lblSigninTime" runat="server">00:00 am</asp:Label>
                                </big>
                            <asp:Label ID="lblSignDate" runat="server"></asp:Label>
                            <asp:Label ID="lblSigninDateTime" CssClass="lblSigninDateTime hide" runat="server">00:00 am</asp:Label>
                            <asp:Image ID="imgEditInHour" runat="server" Width="12px" ImageUrl="images/Controls/pencil.png" ToolTip="Edit Clock-in time" title="Edit Clock-in time" Visible="false" CssClass="tooltip imgEditInHour" Style="margin-left: 5px" />

                        </td>
                    </tr>

                    <tr>
                        <td>&nbsp;
                        </td>
                        <td style="padding: 0px; min-height: 10px;">

                            <div runat="server" id="UndoMsg" visible="false">
                                You have been signed
                        <asp:Label runat="server" ID="ttype"></asp:Label>
                                successfully
                    <asp:LinkButton runat="server" CssClass="event" ID="lnkUndo" Text="Undo" OnClick="lnkUndo_Click"></asp:LinkButton><asp:HiddenField runat="server" ID="hrid" />
                            </div>

                        </td>
                    </tr>

                    <tr class="trProfile" id="trVerifStatus" runat="server">
                        <td class="caption">Verified:
                        </td>
                        <td style="padding-bottom: 6px">
                            <asp:Label ID="lblVerStatus" runat="server"></asp:Label>
                        </td>
                    </tr>

                    <tr class="trProfile" style="display: none">
                        <td></td>
                        <td>
                            <asp:LinkButton runat="server" PostBackUrl="#" ID="lnkDeleteHours" CssClass="lnkDeleteHours">I took additional break</asp:LinkButton>
                        </td>
                    </tr>
                    <tr class="trProfile" id="comments" runat="server" style="display: none">
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td class="caption">Comments:
                                    </td>
                                    <td>
                                        <%--Comments Repeater--%>
                                        <asp:Repeater ID="rptComments" runat="server" OnItemDataBound="rptComments_ItemDataBound">
                                            <HeaderTemplate>
                                                <table>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr id="trComment" runat="server">
                                                    <td>
                                                        <div class="comment">
                                                            <asp:Label runat="server" ID="lblComments" Text='<%# DataBinder.Eval(Container.DataItem, "Comments") %>'></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>

                                                <tfooter>                                                        
                            </tfooter>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:LinkButton runat="server" ID="lnkcomment" Text="Add Comment" Style="text-decoration: none;"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="caption">History:</td>
                        <td style="width: 100%" id="tdHistory" runat="server">
                            <asp:Table ID="tblHistory" runat="server" EnableViewState="false">
                                <asp:TableRow>
                                    <asp:TableCell>
                                    Empty
                                    </asp:TableCell>
                                    <asp:TableCell>
                                    &nbsp;
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Reports:
                        </td>
                        <td>
                            <asp:LinkButton runat="server" ID="rpt" OnClick="rpt_Click" CssClass="raisesevent">Monthly Report</asp:LinkButton>
                            | 
                            <asp:LinkButton ID="lnkGetYearStats" runat="server" OnClick="yrpt_Click">Yearly Report</asp:LinkButton>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <tr class="trdataR" id="tr1" runat="server" visible="false">
                        <td class="caption">Absences <%=DateTime.Now.Year %>:
                        </td>
                        <td>
                            <asp:Label ID="lblTotalAbsencesR" runat="server" Text="0"></asp:Label>&nbsp;<asp:LinkButton ID="lnkAddAbs" runat="server" CssClass="lnkAddAbs" Text="Add leave(s)" Style="padding-left: 0px"></asp:LinkButton>
                        </td>
                    </tr>

                    <tr class="trdataR">
                        <td class="caption">Working Hour <%=DateTime.Now.Year %>:
                        </td>
                        <td>
                            <asp:Label ID="lblTotalWorkedHourY" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                    <%-- <tr class="trdataR" id="tr2" runat="server" visible="false">
                            <td class="caption">Overtime 2015: 
                            </td>
                            <td>
                                <asp:Label ID="lblTotalOvertimeR" runat="server" Text="0"></asp:Label>
                            </td>
                        </tr>--%>
                    <%-- <tr class="trdataR" id="tr3" runat="server" visible="false">
                            <td class="caption" style="text-align: right">
                                <asp:Label ID="lbl12" CssClass="lblOvertimeLabel" runat="server" Text="Undertime 2015: "></asp:Label>
                            </td>

                            <td style="">
                                <asp:Label ID="lblTotalUndertimeR" CssClass="divworker defaulte" runat="server" data-text="As the time is recorded" data-label="" Style="float: left"></asp:Label>
                                <div style="background-color: black; width: 20px">
                                </div>
                            </td>
                        </tr>--%>
                    <%-- <tr class="trdataR" id="tr4" runat="server" visible="false">
                            <td class="caption" style="text-align: right; display: block; margin: 6px 0 0 0;">
                                <asp:Label ID="Label3" CssClass="lblOvertimeLabel" runat="server" Text="Balance Time 2015: "></asp:Label>
                            </td>
                            <td style="">
                                <asp:Label ID="lblbalancetime" CssClass="divworker defaulte" runat="server" data-text="As the time is recorded" data-label="" Style="float: left; border: 1px; border-top-style: solid; border-top-color: #000; padding-top: 5px;"></asp:Label>
                                <div style="background-color: black; width: 20px">
                                </div>
                            </td>
                        </tr>--%>

                    <%-- <tr id="imgEditRole" runat="server">
                            <td class="caption">Role:
                            </td>
                            <td>
                                <asp:Label ID="lblUserRole" runat="server">
                            
                                </asp:Label>
                                <img src="images/Controls/edit.png" runat="server" id="imgEditRole1" title="Use this option to change role of employee" alt="Edit" class="imgEditRole tooltip" tooltip="Use this option to change role of employee" style="cursor: pointer; width: 12px;" />
                            </td>
                        </tr>--%>
                    <tr style="display: none">
                        <td class="caption">Autopresent
                        </td>
                        <td>
                            <asp:Label ID="lblAutoPresent" runat="server" CssClass="lblAutoPresent" ForeColor="Blue" Style="cursor: pointer"></asp:Label>
                            <asp:Label ID="lblAutoPresentU" runat="server" CssClass="lblAutoPresentU"></asp:Label>
                        </td>
                    </tr>
                    <tr id="trStatus" runat="server">
                        <td class="caption">Deactive/active Status
                        </td>
                        <td>
                            <asp:Label ID="lblactive" CssClass="lblactive" runat="server" Style="color: blue; cursor: pointer"></asp:Label>
                            <asp:Label ID="lblActiveU" CssClass="lblActiveU" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">
                            <asp:Label ID="lblLeaves" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Repeater ID="rptAbscenses" runat="server">
                                <HeaderTemplate>
                                    <table class="smallGrid" id="tblAdAbs">
                                        <thead>
                                            <tr class="smallGridHead">
                                                <td>Date
                                                </td>
                                                <td style="padding-left: 10px">Comments
                                                </td>
                                                <td style="padding-left: 10px">Action
                                                </td>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr data-id="">
                                        <td>
                                            <asp:Label ID="lblAbsences" runat="server" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "CrtDate")).ToString("ddd, MMM dd yyyy") %>'></asp:Label>
                                        </td>
                                        <td style="padding-left: 10px">
                                            <asp:Label ID="lblComments" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Comment") %>'></asp:Label>
                                        </td>
                                        <td style="padding-left: 10px">
                                            <img style="cursor: pointer; color: blue" src="images/Controls/cross.png" alt="Delete" id="imgDeleteAbs" class="imgDeleteAbs" width="12px" data-id="<%# DataBinder.Eval(Container.DataItem, "AbsLogID") %>" data-userid="<%# DataBinder.Eval(Container.DataItem, "UserID") %>" data-date="<%# DataBinder.Eval(Container.DataItem, "CrtDate") %>" data-active="<%# DataBinder.Eval(Container.DataItem, "Active") %>" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                </table>


            </div>
            <%--  <div id="divEditRole" title="Employee role" class="hide" runat="server">
                    <table class='profile' style='margin-top: 10px'>
                        <tr>
                            <td style='padding: 5px;'>Select role:
                            </td>
                            <td style='padding: 5px;'>
                                <asp:RadioButtonList ID="rblRole" runat="server">
                                    <asp:ListItem Text="Admin" Value="admin" />
                                    <asp:ListItem Text="Employee" Value="worker" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style='padding: 5px; text-align: right'>
                                <asp:Button ID="btnRole" CssClass="btnRole ui-state-default raisesevent" runat="server" Text="Update" OnClick="btnRole_Click" />

                            </td>
                        </tr>
                    </table>


                </div>--%>
            <div id="divSigninProcess" title="Clock" class="hide">
                <center>
                <table style="margin-top:20px;" cellpadding="0px" cellspacing="0px">
                    <tr runat="server" id="LocationContainer">
                        <td class="caption" style="min-width:0px">
                            <span style="margin-left: 10px;">Location:</span>
                        </td>
                        <td >
                            <asp:DropDownList runat="server" ID="location"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption" style="min-width:0px">Current time:
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblCurrentTime" CssClass="lblCurrentTime">00:00 am</asp:Label>
                        </td>                        
                    </tr>
                    <tr id="trLateClockIn" runat="server" visible="false">
                        <td class="caption" style="min-width:0px">Undertime:
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblUnderTime" CssClass="lblUnderTime"></asp:Label>
                        </td>                                                
                    </tr>
                    <tr id="trOverClockIn" runat="server" visible="false">
                        <td class="caption" style="min-width:0px">Overtime:
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblOvertime" CssClass="lblOvertime"></asp:Label>
                        </td>                                                
                    </tr>
                    <tr id="trLateComment" runat="server" visible="false">
                        <td class="caption" style="min-width:0px">
                            <asp:Label ID="lblUndetimeReason" text="Undertime Reason" runat="server" />                            
                        </td>
                        <td>
                            <asp:TextBox TextMode="MultiLine" runat="server" ID="txtUndertimeComment" CssClass="txtUndertimeComment"></asp:TextBox>
                        </td>                                                
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:center">
                            <asp:Button ID="btnSigninProcess" CssClass="btnSigninProcess btnSignNow btnSignInEnable" runat="server" Text="Clock-In Now" OnClick="BtnsigninClick" style="padding:0 0 0 4px;height:28px;width:130px;text-align:left"/>
                            <asp:Button ID="btnSignoutProcess" CssClass="btnSignoutProcess btnSignNow btnSignOutEnable" runat="server" Text="Clock-Out Now" OnClick="BtnSignoutClick"  style="padding:0 0 0 4px;height:28px;width:130px;text-align:left"/>
                        </td>
                    </tr>
                </table>
                    </center>
            </div>
        </section>
        <asp:HiddenField runat="server" ID="hftimeFormat" />
        <div style="margin-left: 11px;" class="BreakTimeDiv">
            <asp:Label runat="server" ID="noBreak" ForeColor="Red" Text="No record found." Visible="false"></asp:Label>
            <div id="edtHr" title="Edit" style="display: none; padding-left: 10px;">
                <asp:HiddenField runat="server" ID="bID" />
                <asp:HiddenField runat="server" ID="bModified" />
                <asp:HiddenField runat="server" ID="beDate" />
                <div style="padding: 10px 0 10px 28px">
                    <asp:Label ID="lblBreakTime" CssClass="lblBreakTime" runat="server" Style="margin-bottom: 12px;"></asp:Label>
                    <br />
                    <br />
                    <asp:Label runat="server" ID="h" Text="I took break from:" Width="120px"></asp:Label>
                    <asp:TextBox ID="bfrmTime" runat="server" CssClass="TimePick bfrmTime" ValidationGroup="bt" Width="65px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="bfrmTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="bt">Required</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="bfrmTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="(0?[1-9]|(10|11|12))(:[0-5][0-9])( )*(AM|PM|am|pm)" ValidationGroup="bt">Invalid Time</asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="bfrmTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="([01]?[0-9]|2[0-3]):[0-5][0-9]" Visible="False" ValidationGroup="bt">Invalid Time</asp:RegularExpressionValidator>
                </div>
                <div style="padding: 5px 0 10px 28px">
                    <asp:Label runat="server" ID="m" Text="to:" Width="120px"></asp:Label>
                    <asp:TextBox ID="btoTime" runat="server" CssClass="TimePick btoTime" ValidationGroup="bt" Width="65px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="btoTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="bt">Required</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="btoTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="(0?[1-9]|(10|11|12))(:[0-5][0-9])( )*(AM|PM|am|pm)" ValidationGroup="bt">Invalid Time</asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="btoTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="([01]?[0-9]|2[0-3]):[0-5][0-9]" Visible="False" ValidationGroup="bt">Invalid Time</asp:RegularExpressionValidator>
                </div>
                <div style="text-align: right; border-top: 1px solid gray; padding: 10px 0 10px 0px; margin-top: 10px">
                    <asp:Button runat="server" ID="editBreak" CssClass="raisesevent" Text="Save" OnClick="editBreak_Click" ValidationGroup="bt" />
                </div>
            </div>
            <asp:Label ID="bmsgs" runat="server" Text="No breaks" Visible="false"></asp:Label>
        </div>
        <div style="margin-left: 11px;" class="hide">
            <h3 class="SevenDayHHEading">Seven Days History</h3>
        </div>

        <div>
        </div>
        <div id="divActive" class="hide">
        </div>
        <div id="revHr" title="Additional break" style="display: none">
            <asp:HiddenField runat="server" ID="rID" />
            <p>
                Use this option to delete time from your recorded history in order to fix any excess time you recorded by mistake.
            </p>
            <p style="padding: 12px 32px; font-weight: bold; text-align: center">
                <label id="lbldataday"></label>
                <br />
                <label id="lblsWorked"></label>
            </p>
            <div class="divDelHours">
                <asp:Label runat="server" ID="Label1" CssClass="Label1" Text="From:" Width="57px"></asp:Label>
                <asp:TextBox ID="txtfrmTime" runat="server" CssClass="TimePick txtfrmTime" ValidationGroup="dt" Width="65px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtfrmTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="dt">Required</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtfrmTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="(0?[1-9]|(10|11|12))(:[0-5][0-9])( )*(AM|PM|am|pm)" ValidationGroup="dt">Invalid Time</asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtfrmTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="([01]?[0-9]|2[0-3]):[0-5][0-9]" Visible="False" ValidationGroup="dt">Invalid Time</asp:RegularExpressionValidator>
            </div>
            <div class="divDelHours">
                <asp:Label runat="server" ID="Label2" Text="To:" Width="57px"></asp:Label>
                <asp:TextBox ID="txtToTime" runat="server" CssClass="TimePick txtToTime" ValidationGroup="dt" Width="65px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtToTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="dt">Required</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtToTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="(0?[1-9]|(10|11|12))(:[0-5][0-9])( )*(AM|PM|am|pm)" ValidationGroup="dt">Invalid Time</asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtToTime" Display="Dynamic" ErrorMessage="Invalid Time" ForeColor="Red" ValidationExpression="([01]?[0-9]|2[0-3]):[0-5][0-9]" Visible="False" ValidationGroup="dt">Invalid Time</asp:RegularExpressionValidator>
            </div>
            <div class="divDialogButton">
                <button value="Add Break Time" id="btnDeleteTime" class="btnDeleteTime ui-state-default">Add Break Time</button>
            </div>
        </div>
        <div id="divEditTime" title="Edit Time" style="display: none">
            <asp:HiddenField runat="server" ID="hdnerid" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hdnOFrmTime" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hdnOToTime" ClientIDMode="Static" />
            <table class="tblDlg" id="tblDlg">
                <tr>
                    <th style="text-align: left" colspan="2">Use this option to edit time from your recorded history.
                    </th>
                </tr>
                <tr>
                    <td>&nbsp;
                            <asp:Label ID="lblRowID" CssClass="lblRowID" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <th>From:
                    </th>
                    <td>
                        <asp:TextBox ID="txtEFrmTime" runat="server" CssClass="DateTimePick txtEFrmTime" ValidationGroup="edittime" Width="160px" ClientIDMode="Static"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEFrmTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="edittime">Required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="trout">
                    <th>To:
                    </th>
                    <td>
                        <asp:TextBox ID="txtEToTime" runat="server" CssClass="DateTimePick txtEToTime" ValidationGroup="edittime" Width="160px" ClientIDMode="Static"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtEToTime" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="edittime">Required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <th>Comments:
                    </th>
                    <td>
                        <asp:TextBox ID="txteComments" runat="server" TextMode="MultiLine" ClientIDMode="Static" ValidationGroup="edittime">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="edittime" ControlToValidate="txteComments" ErrorMessage="Required" SetFocusOnError="true" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>

            </table>
            <div class="divDialogButton">
                <asp:Button ID="btnUpdateTime" runat="server" Text="Update Time" CssClass="ui-state-default btnUpdateTime raisesevent" OnClick="btnUpdateTime_Click" ValidationGroup="edittime"  />
            </div>
        </div>
        <div id="divConfirmDeleteTime" class="hide" title="Are you sure?">
            <p style="margin-bottom: 5px">
                <b>Warning:</b> Once the hours are deleted you will not be able to restore them.
            </p>
            <p style="margin-bottom: 5px">
                You are deleting <b>
                    <label id="lblHoursToDel" class="lblHoursToDel">#</label></b> from your work log. 
            </p>
            <p style="margin-bottom: 5px">
                Are you sure you want to do delete this time?
            </p>
            <div class="divDialogButton">
                <asp:Button runat="server" ID="deleHrs" Text="Yes" OnClick="deleHrs_Click" CssClass="raisesevent ui-state-default" />
                <button id="btnNo" class="btnclosedlg ui-state-default" value="No">No</button>
                <button id="btnCancel" class="btnclosedlg ui-state-default" value="Cancel">Cancel</button>
            </div>
        </div>
        <div id="editworker" style="display: none" title="Edit employee time" runat="server">
            <table class="smallGrid">
                <tr>
                    <td style="width: 100px">Signin </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hfeditid" />
                        <input type="text" value="" id="j" style="width: 0px; height: 0px; margin-left: -10px; border-color: transparent; color: transparent" />
                        <asp:TextBox runat="server" ID="txteditsignin" Width="150px" CssClass="DatePick" TabIndex="1"></asp:TextBox>
                        <asp:TextBox runat="server" ID="txteditintime" Width="100px" CssClass="TimePick"></asp:TextBox>
                        <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator9" runat="server" ControlToValidate="txteditsignin" Display="Dynamic" ErrorMessage="Invalid Date" ValidationExpression="(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}" ValidationGroup="ghj">Invalid Date</asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator10" runat="server" ControlToValidate="txteditintime" Display="Dynamic" ErrorMessage="Invalid Time" ValidationExpression="(0?[1-9]|(10|11|12))(:[0-5][0-9])( )*(AM|PM|am|pm)">Invalid Time</asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator11" runat="server" ControlToValidate="txteditintime" Display="Dynamic" ErrorMessage="Invalid Time" ValidationExpression="([01]?[0-9]|2[0-3]):[0-5][0-9]" Visible="False">Invalid Time</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlsigninstatus">
                            <asp:ListItem Text="" Value=""></asp:ListItem>
                            <asp:ListItem Text="Verified" Value="Verified"></asp:ListItem>
                            <asp:ListItem Text="Not Verified" Value="Not Verified"></asp:ListItem>
                        </asp:DropDownList>

                    </td>
                </tr>
                <tr>
                    <td>Signout</td>
                    <td>
                        <input type="text" value="" id="q" style="width: 0px; height: 0px; margin-left: -10px; border-color: transparent; color: transparent" />
                        <asp:TextBox runat="server" ID="txteditsignout" CssClass="DatePick" Width="150px" TabIndex="2"></asp:TextBox>
                        <asp:TextBox runat="server" ID="txteditoutitme" CssClass="TimePick" Width="100px"></asp:TextBox>
                        <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator12" runat="server" ControlToValidate="txteditsignout" Display="Dynamic" ErrorMessage="Invalid Date" ValidationExpression="(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}" ValidationGroup="ghj">Invalid Date</asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator13" runat="server" ControlToValidate="txteditoutitme" Display="Dynamic" ErrorMessage="Invalid Time" ValidationExpression="(0?[1-9]|(10|11|12))(:[0-5][0-9])( )*(AM|PM|am|pm)">Invalid Time</asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator14" runat="server" ControlToValidate="txteditoutitme" Display="Dynamic" ErrorMessage="Invalid Time" ValidationExpression="([01]?[0-9]|2[0-3]):[0-5][0-9]" Visible="False">Invalid Time</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlsignoutstatus">
                            <asp:ListItem Text="" Value=""></asp:ListItem>
                            <asp:ListItem Text="Verified" Value="Verified"></asp:ListItem>
                            <asp:ListItem Text="Not Verified" Value="Not Verified"></asp:ListItem>
                        </asp:DropDownList>

                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:TextBox runat="server" TextMode="MultiLine" ID="txteditcomment" Width="247px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button runat="server" Text="Update" ID="btnupdate" class="buttonstyle" OnClick="BtnupdateClick" ValidationGroup="ghj" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divClockedIn" title="Clocked In Successfully!" style="display: none">
            <p>
                You have successfully clocked in. 
            </p>
            <br />
            <p class="pverifyin">
                Please verify your location by clicking the link in your verification email sent to 
                <asp:Label runat="server" ID="lblInEmail"></asp:Label>
            </p>
        </div>
        <div id="divClockedOut" title="Clocked Out Successfully!" style="display: none">
            <p>
                You have successfully clocked out. 
            </p>
            <br />
            <p class="pverifyout">
                Please verify your location by clicking the link in your verification email sent to 
                <asp:Label runat="server" ID="lblOutEmail"></asp:Label>
            </p>
        </div>

        <div id="divDialog" title="" style="display: none">
        </div>
        <div id="addcomment" title="Comment" style="display: none">
            <asp:HiddenField runat="server" ID="hfcommentstatus" />
            <asp:TextBox runat="server" ID="txtcomment" TextMode="MultiLine" Height="50px"></asp:TextBox>
            <br />
            <br />

            <asp:Button runat="server" ID="btnin" CssClass="raisesevent ui-state-default" Text="Post Comment" placeholder="Please provide your comments..." Style="float: right" />
        </div>
        <div id="addcomentout" title="Signout Comment" style="display: none">
            <asp:TextBox runat="server" ID="txtcommentout" TextMode="MultiLine" Height="50px"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="btnout" runat="server" Text="Post Comment" Style="float: right" />
        </div>
        <div id="absComment" class="hide" title="Comment">
            <table>
                <tr>
                    <td>
                        <textarea id="txtAbsComment" class="txtAbsComment" style="width: 333px; height: 57px;" placeholder="Please add your comments here..."></textarea>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divAutoPresent" title="Auto Present" style="display: none">
            <table>
                <tr>
                    <td colspan="2">
                        <p>
                            Use this option to mark present automatically by specifying clock in and clock out time.
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>Auto Present:
                    </td>
                    <td>
                        <input type="checkbox" id="chkAP" value="1" />
                    </td>
                </tr>
                <tr class="trAPC">
                    <td>Clock in Time: 
                    </td>
                    <td>
                        <input id="txtOPStartTime" type="text" class="txtOPStartTime datetime" value="Select" />
                    </td>
                </tr>
                <tr class="trAPC">
                    <td>Clock out Time: 
                    </td>
                    <td>
                        <input id="txtOPEndTime" type="text" class="txtOPEndTime datetime" value="Select" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divWorkingHours" title="Working hours" style="display: none">
            <center>
                <h3>
                    <asp:Label ID="lblWorkingHourMessage" CssClass="lblWorkingHourMessage" runat="server"></asp:Label>
                </h3>   
                    </center>
            <table class="profile" id="tblWorkingHours" runat="server" style="margin-top: 10px; padding: 0 13px">
                <tr>
                    <td colspan="3" style="text-align: center">
                        <p>
                            Provide your working hours here
                        </p>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <th>Clock in 
                    </th>
                    <th>Clock out
                    </th>
                </tr>
                <tr>
                    <td class="captionauto">Monday
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hdnMon" Value="0" />
                        <asp:HiddenField runat="server" ID="hdnMonID" Value="0" />
                        <asp:TextBox ID="txtMonCI" runat="server" CssClass="txtMonCI datetime datetimeCI"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtMonCO" runat="server" CssClass="txtMonCO datetime datetimeCO"></asp:TextBox>
                    </td>
                    <td>
                        <img id="btnApplyToAll" class="btnApplyToAll" src="" alt="Apply to all" style="float: right; margin-left: 5px; color: blue; cursor: pointer" />
                    </td>
                </tr>
                <tr class="trAPC">
                    <td class="captionauto">Tuesday
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hdnTue" Value="0" />
                        <asp:HiddenField runat="server" ID="hdnTueID" Value="0" />
                        <asp:TextBox ID="txtTuesCI" runat="server" CssClass="txtTuesCI datetime datetimeCI"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtTuesCO" runat="server" CssClass="txtTuesCO datetime datetimeCO"></asp:TextBox>
                    </td>
                </tr>
                <tr class="trAPC">
                    <td class="captionauto">Wednesday
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hdnWed" Value="0" />
                        <asp:HiddenField runat="server" ID="hdnWedID" Value="0" />
                        <asp:TextBox ID="txtWedCI" runat="server" CssClass="txtWedCI datetime datetimeCI"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtWedCO" runat="server" CssClass="txtWedCO datetime datetimeCO"></asp:TextBox>
                    </td>
                </tr>
                <tr class="trAPC">
                    <td class="captionauto">Thursday
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hdnThu" Value="0" />
                        <asp:HiddenField runat="server" ID="hdnThuID" Value="0" />
                        <asp:TextBox ID="txtThuCI" runat="server" CssClass="txtThuCI datetime datetimeCI"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtThuCO" runat="server" CssClass="txtThuCO datetime datetimeCO"></asp:TextBox>
                    </td>
                </tr>
                <tr class="trAPC">
                    <td class="captionauto">Friday
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hdnFri" Value="0" />
                        <asp:HiddenField runat="server" ID="hdnFriID" Value="0" />
                        <asp:TextBox ID="txtFriCI" runat="server" CssClass="txtFriCI datetime datetimeCI"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFriCO" runat="server" CssClass="txtFriCO datetime datetimeCO"></asp:TextBox>
                    </td>
                </tr>
                <tr class="trAPC">
                    <td class="captionauto">Saturday
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hdnSat" Value="0" />
                        <asp:HiddenField runat="server" ID="hdnSatID" Value="0" />
                        <asp:TextBox ID="txtSatCI" runat="server" CssClass="txtSatCI datetime datetimeCI"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtSatCO" runat="server" CssClass="txtSatCO datetime datetimeCO"></asp:TextBox>
                    </td>
                </tr>
                <tr class="trAPC">
                    <td class="captionauto">Sunday
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hdnSun" Value="0" />
                        <asp:HiddenField runat="server" ID="hdnSunID" Value="0" />
                        <asp:TextBox ID="txtSunCI" runat="server" CssClass="txtSunCI datetime datetimeCI"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtSunCO" runat="server" CssClass="txtSunCO datetime datetimeCO"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="captionauto">Autopresent: 
                    </td>
                    <td>
                        <asp:CheckBox ID="chkAutoPresent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right">
                        <asp:Button ID="btnSaveWH" CssClass="btnSaveWH ui-state-default raisesevent" runat="server" Text="Update" OnClick="btnSaveWH_Click" />
                        <asp:Button ID="btnCanceWH" CssClass="btnCanceWH ui-state-default" runat="server" Text="Cancel" />
                    </td>
                </tr>
            </table>
            <asp:LinkButton ID="lblShowExDays" runat="server" CssClass="lblShowExDays" Text="Days with different working hours"></asp:LinkButton><br />
            <br />
            <asp:LinkButton ID="lbltimesetting" runat="server" CssClass="lbltimesetting" Text="Use flexible hours for this worker (Ex: 7hrs /day)"></asp:LinkButton><br />
            <br />

        </div>
        <div class="Tab ExpectionalDays" title="Days with different working hours" id="ExpectionalDays" runat="server" style="display: none; margin-top: 5px !important">
            <asp:LinkButton ID="lnkExNew" CssClass="lnkExNew" runat="server" Text="Add new"></asp:LinkButton>

            <table style="width: 314px">
                <tr class="smallGrid">
                    <td class="auto-style3">Title </td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtExTitle" CssClass="txtExTitle" runat="server" MaxLength="150" ValidationGroup="l" Width="124px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtExTitle" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="5">Required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="smallGrid">
                    <td class="auto-style3">Start Date</td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtExFrom" CssClass="txtExFrom txtdatetime" runat="server" MaxLength="150" ValidationGroup="l" Width="124px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtExFrom" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="5">Required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="smallGrid">
                    <td class="auto-style3">End Date</td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtExTo" CssClass="txtExTo txtdatetime" runat="server" MaxLength="150" ValidationGroup="l" Width="124px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtExTo" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="5">Required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="smallGrid">
                    <td class="auto-style3">Clock In</td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtExClockIn" CssClass="txtExClockIn txttime" runat="server" MaxLength="150" ValidationGroup="l" Width="124px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtExTo" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="5">Required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="smallGrid">
                    <td class="auto-style3">Clock Out</td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtExClockOut" CssClass="txtExClockOut txttime" runat="server" MaxLength="150" ValidationGroup="l" Width="124px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtExTo" Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="5">Required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="smallGrid">
                    <td class="auto-style3">Eliminate Breaks</td>
                    <td class="auto-style1">
                        <asp:CheckBox ID="chkEliminateBreaks" CssClass="chkEliminateBreaks" runat="server" Text="" />
                    </td>
                </tr>
                <tr class="smallGrid">
                    <td class="auto-style3"></td>
                    <td class="auto-style1">
                        <asp:Button ID="btnAddExceptionalDays" CssClass="btnAddExceptionalDays" runat="server" Text="Add" ValidationGroup="5" Width="50px" OnClick="btnAddExceptionalDays_Click" />
                        <%--<asp:Button ID="btnClearExDays" runat="server" Text="Use standard only" ValidationGroup="5" Width="50px" OnClick="btnClearExDays_Click" />--%>
                        <asp:Button ID="btnCancelExDays" CssClass="btnCancelExDays" runat="server" Text="Cancel" ValidationGroup="5" Width="50px" />
                        <asp:HiddenField ID="hdnGroupID" ClientIDMode="Static" runat="server" Value="0" />
                    </td>
                </tr>
            </table>
            <asp:Repeater runat="server" ID="rptExceptionalDays" OnItemCommand="rptExceptionalDays_ItemCommand">
                <HeaderTemplate>
                    <table class="smallGrid">
                        <tr class="smallGridHead">
                            <td>Title</td>
                            <td>From</td>
                            <td>To</td>
                            <td>Clockin</td>
                            <td>Clockout</td>
                            <td>CrtDate</td>
                            <td>ModDate</td>
                            <td>Avoid Breaks</td>
                            <td></td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="smallGrid">
                        <td>
                            <asp:Label runat="server" ID="lblGroupName" Text='<%# DataBinder.Eval(Container.DataItem, "GroupName").ToString() %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblStartDate" Text='<%# GetDate(DataBinder.Eval(Container.DataItem, "StartDate").ToString()) %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblEndDate" Text='<%# GetDate(DataBinder.Eval(Container.DataItem, "EndDate").ToString()) %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblClockIn" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "ClockIn")).ToShortTimeString() %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblClockOut" Text='<%# Convert.ToDateTime((DataBinder.Eval(Container.DataItem, "ClockOut"))).ToShortTimeString()%>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblCrtDate" Text='<%# GetDate(DataBinder.Eval(Container.DataItem, "CrtDate").ToString()) %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblModDate" Text='<%# GetDate(DataBinder.Eval(Container.DataItem, "ModDate").ToString())%>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="chkAvoidBreaks" Text='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "AvoidBreaks").ToString())?"Yes" : "No" %>'></asp:Label>
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" data-groupid='<%# DataBinder.Eval(Container.DataItem, "GroupID").ToString()%>' CommandName="Edit" CssClass="raisesevent lnkEdit" Visible='<%# GetExVisiblity(DataBinder.Eval(Container.DataItem, "UserID").ToString()) %>'></asp:LinkButton>
                            <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" data-groupid='<%# DataBinder.Eval(Container.DataItem, "GroupID").ToString()%>' CommandArgument='<%# DataBinder.Eval(Container.DataItem, "GroupID").ToString()%>' CommandName="Delete" CssClass="raisesevent" Visible='<%# GetExVisiblity(DataBinder.Eval(Container.DataItem, "UserID").ToString()) %>'></asp:LinkButton>
                            <asp:Label ID="lblExInfo" runat="server" CssClass="raisesevent" Text='<%# GetExInfo(DataBinder.Eval(Container.DataItem, "UserID").ToString()) %>'></asp:Label>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <div class="Tab TimeSetting" title="Use flexible hours for this worker (Ex: 7hrs /day)" id="Div1" runat="server" style="display: none; margin-top: 5px !important">
            <p>To Save this Setting, also change this user fixed time setting to flexible time setting </p>
            <table style="width: 405px;">
                <tr class="smallGrid">
                    <td>Hours per day (Ex: 8 hrs /day):
                    </td>
                    <td>
                        <asp:TextBox ID="txthours" runat="server" CssClass="standardField"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server" ErrorMessage="Only Number" ControlToValidate="txthours" ForeColor="Red" ValidationExpression="^\d$" ValidationGroup="flex"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr class="smallGrid">
                    <td>Break:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlflexbreak" runat="server">
                            <asp:ListItem Value="False">No</asp:ListItem>
                            <asp:ListItem Value="True">Yes</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr class="smallGrid">
                    <td></td>
                    <td>
                        <asp:Button ID="btnflexsave" runat="server" CssClass="standardButton timesettings" Text="Save" OnClick="btnflexsave_Click" />&nbsp;
                                <asp:Button ID="Button1" CssClass="timesettingcancel" runat="server" Text="Cancel" ValidationGroup="5" Width="50px" />

                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField runat="server" ID="hdnAdmin" />
        <asp:HiddenField runat="server" ID="hdnSomeEvent" />
    </div>
    <h1 id="h1Inactive" runat="server" style="padding-top: 10px;" visible="false">
        <span style="color: red">Sorry!</span> your account has been deactivated by admin.
    </h1>
    <div id="divAbs" class="divAbs" style="display: none" title="Add leaves">
        <asp:HiddenField runat="server" ID="hdfAbsLogID" />
        <asp:HiddenField runat="server" ID="hdfActive" />
        <asp:HiddenField runat="server" ID="hdfUserID" />
        <table class="profile" style="margin-top: 10px">
            <tr>
                <td colspan="2" style="text-align: center"><b>Please add a leave</b>
                </td>
            </tr>
            <tr>
                <td class="captionauto">Taking leave from: 
                </td>
                <td>
                    <asp:TextBox type="text" ID="txtAbsDateFrom" runat="server" CssClass="txtAbsDateFrom DateTimePick"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="captionauto">To: 
                </td>
                <td>
                    <asp:TextBox type="text" ID="txtAbsDateTo" runat="server" CssClass="txtAbsDateTo DateTimePick"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="captionauto">Comment: 
                </td>
                <td>
                    <asp:TextBox ID="txtAbsComments" CssClass="txtAbsComments" runat="server" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center"><b>Your request will be emailed to your administrators for approval</b>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: right">
                    <asp:Button ID="btnAddLeave" CssClass="btnAddLeave raisesevent ui-state-default" runat="server" Text="Add" OnClick="btnAddLeave_Click" />
                    <asp:Button ID="btnLeaveCancel" CssClass="btnLeaveCancel ui-state-default" runat="server" Text="Cancel" />
                </td>
            </tr>
        </table>
    </div>
    <div id="Dialog" title="">
    </div>
    <div id="divNewUser" class="hide" title="Welcome">
        <div>
            <p>
                Welcome to avaima Time & Attendance.
                    <br />
                <br />
                Start using Time & Attendance by clocking in before you start your start. 
                    <br />
                <br />
                After work, you can clock out to record your working statistics.                    
            </p>
        </div>
    </div>
    <div id="divDialogUOT" title="" style="display: none">
        <div id="divmsg">
        </div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblUOTTitle" runat="server" CssClass="lblUOTTitle"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblUOTDelay" runat="server" CssClass="lblUOTDelay"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Reason:
                </td>
                <td>
                    <asp:TextBox ID="txtUOTReason" runat="server" TextMode="MultiLine" CssClass="txtUOTReason"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="divDialogIN" title="" style="display: none">
        <div id="divmsg1">
        </div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" CssClass="lblUOTTitle"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label4" runat="server" CssClass="lblUOTDelay"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Reason:
                </td>
                <td>
                    <asp:TextBox ID="TXTINReason" runat="server" TextMode="MultiLine" CssClass="txtUOTReason"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    
    <asp:HiddenField ID="hdnClockinTime" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnClockinServerTime" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnClockoutTime" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnIsUnderTime" ClientIDMode="Static" runat="server" Value="false" />
    <asp:HiddenField ID="hdnIsOverTime" ClientIDMode="Static" runat="server" Value="false" />
    <asp:HiddenField ID="hdnCOTime" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnCITime" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnCOServerTime" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnUndertimeLateClockIn" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnAutoCO" runat="server" ClientIDMode="Static" Value="false" />
    <asp:HiddenField ID="hdnAnotherCO" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnMessageFlag" runat="server" ClientIDMode="Static" Value="." />
    <asp:HiddenField ID="hdnIsWeekend" runat="server" ClientIDMode="Static" Value="false" />

    <div>
    </div>

    <script>
        function OpenDialog(id) {
            $(id).dialog({
                width: 400,
                modal: true,
                hide: {
                    effect: "blind",
                },
                buttons: {
                    'Ok': function () {
                        $(this).dialog('close');
                    }
                }
            });
        }



        function OpenDialogWB(id) {
            var dlg = $(id).dialog({
                width: 400,
                modal: true,
                hide: { effect: "blind", }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");

            $('.btnClose').click(function () {
                $(id).dialog('close');
                return false;
            });
        }

        function OpenEditUOT() {
            var dlg = $('#divUOT').dialog({
                width: 360,
                modal: true,
                hide: { effect: "blind" },
                buttons: {
                    'Cancel': function () {
                        $(this).dialog('close');
                    }
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
        }

        function OpenEditOvertime() {

            $('#divOvertimeEdit').attr('title', 'Edit Over-time');
            var dlg = $('#divOvertimeEdit').dialog({
                width: 360,
                modal: true,
                hide: { effect: "blind" },
                buttons: {
                    'Cancel': function () {
                        $(this).dialog('close');
                    }
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
        }

        $(function () {
            $('#lblClockintime').text($('#hdnClockinTime').val());

            var div = $('#' + $('#hdnMessageFlag').val());
            if (div != "") {
                OpenDialog(div);
            }

            $('.divAddOvertime .txtOverTimeTo, .divAddOvertime  .txtOverTimeFrom').change(function () {
                var fromDate = new Date((new Date()).toDateString() + ' ' + $('.divAddOvertime .txtOverTimeFrom').val());
                var toDate = new Date((new Date()).toDateString() + ' ' + $('.divAddOvertime .txtOverTimeTo').val());
                var duration = GetDHMs((toDate - fromDate) / 60000);
                $('.lblUOTDuration').text(duration);
            })

            var signin = false;
            $('.btnsignin').click(function () {


                if (!signin) {

                    var btnObj = $(this);
                    var dlg = $('#divDialogIN').dialog({
                        modal: true,
                        hide: {
                            effect: "blind"
                        },
                        buttons: {
                            "Clockin": function () {
                                signin = true;
                                $(btnObj).click();
                                $(this).hide('blind');
                            },
                            "Cancel": function () {
                                $('#hdnIsUnderTime').val('false');
                                $(this).dialog('close');
                            }
                        }
                    });
                    dlg.parent().appendTo($("form:first"));
                    dlg.parent().css("z-index", "1000");
                    var iframe = window.parent.document.getElementById("MainContent_ifapp");

                    return false;

                }
            });

        });
    </script>
    <div id="divEarlyClockIn" title="Early Clock-In" class="hide">
        <p>
            You signed in earlier than your start time defined in your working hours
        </p>
        <br />
        <p>
            If you are intentionally doing over-time then use the over-time feature to manually record time.
                You can do this anytime whether you are clocked-in or out.
        </p>
    </div>
    <div id="divLateClockIn" title="Late Clock-In" class="hide">
        <%-- <p>
                <b>Subject:</b> You will automatically CLOCK-IN at
                <label id="lblClockintime"></label>
            </p>--%>
        <%--<br />--%>
        <p>
            Your start time is
                <asp:Label ID="lblcitime1" runat="server"></asp:Label>
            and you are
                <asp:Label ID="lblMinutes" runat="server"></asp:Label>
            late!
        </p>
    </div>
    <div id="divAutoClockoutSimple" title="Auto Clock-out" class="hide">
        <p>
            Your working hours end at
                <asp:Label ID="lblClockouttime" runat="server"></asp:Label>
            and hence you have been clocked out automatically.
        </p>
        <br />
        <p>
            You have to manually add overtime in case you are working extra.
        </p>
    </div>
    <div id="divAutoClockoutAfter" title="Auto Clock-out" class="hide">
        <p>
            Your working hours are complete. Any extra work can be manually recorded using over-time feature.
        </p>
        <br />
        <p>
            You have to manually add overtime in case you are working extra.
        </p>
    </div>
    <div id="divAlreadyClockedIn" title="Already Clocked-in" class="hide">
        <p>
            You are already clocked-in!
        </p>
    </div>
    <div id="divClockinAfterClockoutTimeWithNoClockIn" title="Already Clocked-in" class="hide">
        <p>
            You can only clock-in during your work hours. You can add Over-time if you are working 
        </p>
    </div>
    <div id="divErr" title="Error" class="hide">
        <p>
            An error has occurred.
        </p>
        <br />
        <p>
            <b>Error:</b>
            <asp:Label ID="lblErrTitle" runat="server"></asp:Label>
        </p>
        <br />
        <p>
            <b>Detail:</b>
            <asp:Label ID="lblErrDetail" runat="server"></asp:Label>
        </p>
        <br />
        <p>
            <asp:Button ID="btnEmailToDev" runat="server" CssClass="btnAddPause raisesevent ui-state-default" Text="Email Support" OnClick="btnEmailToDev_Click" />
            <asp:Button ID="btnClose" CssClass="btnClose ui-state-default" runat="server" Text="Close" />
        </p>
        <%-- <p>
                <b>Stack Overflow:</b> <asp:Label ID="lblErrStack" runat="server"></asp:Label>
            </p>--%>
    </div>
    <div id="divErrUOT" title="Invalid Attempt" class="hide">
        <br />
        <p>
            <b>Error:</b>
            <asp:Label ID="lblUOTMessage" runat="server"></asp:Label>
        </p>
        <%-- <p>
                <b>Stack Overflow:</b> <asp:Label ID="lblErrStack" runat="server"></asp:Label>
            </p>--%>
    </div>
    <div id="divAutoClockIn" class="divAutoClockIn hide" title="Auto Clock-in">
        <p>
            You are set to automatically clock-in at
                <asp:Label runat="server" ID="lblAutoCITime" CssClass="lblAutoCITime"></asp:Label>
            <br />
            <br />
            Time to clock-in:
                <asp:Label ForeColor="#cc0000" Font-Size="Large" ID="lblTimeToCI" CssClass="lblTimeToCI" runat="server"></asp:Label>
        </p>
    </div>
    <%--<div id="divOnClockin" class="hide" title="NOTE">
            <p>
                If you dont clock in within your office work hours. You will be undertimed with late minutes which you have to compensate afterwards in order to complete your work hours.
            </p>
        </div>
        <div id="divOnClockout" class="hide" title="NOTE">
            <p>
                Make sure you compensate your undertime before clocking out. If you clock out before completing your work hours, you will be unable to complete your work hours.
            </p>
        </div>
        <div id="divOnAfterClockin" class="hide" title="Time & Attendance Updates">
            <h3>Time & Attendance Updates:</h3>
            <p>
                Six important Updates (features) to have a look before proceeding:
            </p>
            <ol>
                <li>
                    <b>Pause Time: </b>You can now pause and play your time on while taking certain breaks like Smoking, taking a long break, attending a phone call etc. Depending on your organization's policy. This time slice will also be undertimed from working hours
                </li>
                <li>
                    <b>Add Overtime: </b>You can add overtime during working hour. i.e. Working during break timing/s.
                </li>
                <li>
                    <b>Add Under-time: </b>You can add undertime during working hour while taking any kind of extra breaks like Smoking, taking a long break, attending a phone call etc. Depending on your organization's policy. This time slice will also be undertimed from total work hours.
                    <ul>
                        <li>Pause time is a type of undertime.</li>
                        <li>Every undertime will increase working hours i.e. employee needs to compensate his/her undertime by either adding overtime or by working more than defined working hours</li>
                        <li>Employee has to compensate undertime to complete his working hours. Or he can add overtime afterwards to balance working hours.</li>
                    </ul>
                </li>
                <li>
                    <b>Automated Clock-out: </b>Employee will be automatically clocked out once after he completes his defined work hours. System is intelligent enough to calculate your clock out time through compensating your undertime automatically.
                </li>
                <li>
                    <b>Total Work Hours: </b>are calculation by using three parameters. Working hours, Undertime and overtime. 
                </li>
                <li>
                    <b>Late Clock-in: </b>If employee clocks in after defined clock-out time. Certain time slice will be undertimed which he has to compensate by extra work (late clock-in) and his automatic clock-out time will increase.
                </li>
            </ol>
        </div>
        <div id="divOnAfterAutoClockout" class="hide" title="NOTE">
            You have completed your work hours. You can now add overtime if you are doing overtime.
        </div>--%>


    <%--<div>
            Time & Attendance has been updated. Following are the list of feature that we are changing in Time & Attendance Application.

            <section>
                <h3>Automatic Overtime/Undertime Calculation: </h3>                
                In early version we were calculating Employee's Overtime/Undertime automatically according to their working hours.
                    However, Organizations face the following problems:
                    <ol>
                        <li>Employees that come late, used to clockout after certain duration of time that was compensating their late hours without any permission of administrator.
                        </li>
                        <li>There was an advantage for employees to clockout every day after 5 or 10 minutes later which was appearing as overtime in reports.
                        </li>
                        <li>There were very less amount of traces to track the Undertime and Overtime of employees accurately.
                        </li>
                    </ol>
                <br />
                We came up with the solution of Manual Overtime/Undertime in order to provide accurate statistics.<br />

            </section>

            face. You may be experiencing some hard time figuring out what's gone wrong with the work hour calculation. 

        </div>--%>
    <asp:HiddenField runat="server" ID="hfbfrom" Value="0" />
    <asp:HiddenField runat="server" ID="hfbto" Value="0" />
    <asp:HiddenField runat="server" ID="hfotlimit" Value="0" ClientIDMode="Static" />

    <%--   </form>
</body>
</html>--%>
</asp:Content>

