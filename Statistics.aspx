﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Statistics.aspx.cs" Inherits="Statistics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        $(document).ready(function () {


        });
    </script>
    <style>
        table.myTable {
            border: none;
            background-color: #EEEEEE;
            width: 100%;
            text-align: left;
            border-collapse: collapse;
        }

        table.blueTable td, table.blueTable th {
            border: none;
            padding: 3px 2px;
        }

        table.blueTable tbody td {
            font-size: 13px;
            padding: 10px 10px 10px 10px;
        }
    </style>

    <asp:ScriptManager runat="server" ID="scriptmanager1"></asp:ScriptManager>

    <table id="tblMain" class="myGrid">
        <tr>
            <td>
                <table class="tblDlg" id="tbl_stats">
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="lbluser" Text="Select User"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlUsers" OnSelectedIndexChanged="ddlUsers_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="How many hours a day user suppossed to work?"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtHours"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button runat="server" ID="btnGenerateReport" OnClick="btnGenerateReport_Click" Text="Generate Report" />
                        </td>
                    </tr>
                </table>
            </td>
            <td style="padding: 4px 7px 4px 60px;">
                <asp:UpdatePanel runat="server" ID="list_panel">
                    <ContentTemplate>
                        <div id="divUserHolidays">
                            <asp:Label runat="server" ID="lblText" Text="Select user to see his defined weekend and holidays." Font-Bold="true"></asp:Label>
                            <asp:Repeater runat="server" ID="rptHolidayWeekend" Visible="false" OnItemDataBound="rptHolidayWeekend_ItemDataBound">
                                <HeaderTemplate>
                                    <table class="myGrid">
                                        <tr class="myGridHead">
                                            <td>Title</td>
                                            <td>Date</td>
                                            <td>Type</td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <!--class="myGrid"-->
                                        <td>
                                            <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("title") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblDate" Text='<%# Eval("date") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblType" Text='<%# Eval("type") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger EventName="SelectedIndexChanged" ControlID="ddlUsers" />
                    </Triggers>
                </asp:UpdatePanel>

            </td>
        </tr>
    </table>

    <asp:UpdatePanel runat="server" ID="stats_panel">
        <ContentTemplate>
            <div id="result" runat="server">
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnGenerateReport"/>
        </Triggers>
    </asp:UpdatePanel>


</asp:Content>

